<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCtliMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ctli_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150);
            $table->string('alias', 150)->nullable();
            $table->string('slug', 150)->unique();
            $table->unsignedInteger('padre')->default(0);
            $table->smallInteger('orden')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ctli_menus');
    }
}
