<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_documentos_archivos
  ALCANCE: Para el sistema SICODOC
*/



class AddForeignSdocDocumentosArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('sdoc_documentos_archivos', function (Blueprint $table) {
        $table->foreign('documento_id')->references('id')->on('sdoc_documentos');
        $table->foreign('archivo_id')->references('id')->on('sdoc_archivos');
        $table->foreign('usuario_id')->references('id')->on('usuarios');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('sdoc_documentos_archivos', function (Blueprint $table) {
        $table->dropForeign(['documento_id']);
        $table->dropForeign(['archivo_id']);
        $table->dropForeign(['usuario_id']);
        $table->dropIndex('sdoc_documentos_archivos_documento_id_foreign');
        $table->dropIndex('sdoc_documentos_archivos_archivo_id_foreign');
        $table->dropIndex('sdoc_documentos_archivos_usuario_id_foreign');
      });
    }
}
