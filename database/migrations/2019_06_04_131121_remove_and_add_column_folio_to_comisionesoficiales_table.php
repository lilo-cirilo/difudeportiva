<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Problema al tener el folio solo en viaticos, debe ser para todos los tipos
    ALCANCE: Fondo Rotatorio
*/

class RemoveAndAddColumnFolioToComisionesoficialesTable extends Migration
{
    /**
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropColumn('folio');
        });
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->string('folio', 30)->after('programa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->string('folio', 30);
        });
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->dropColumn('folio');
        });
    }
}
