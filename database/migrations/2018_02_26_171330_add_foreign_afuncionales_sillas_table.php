<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAfuncionalesSillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('afuncionales_sillas', function (Blueprint $table) {
            $table->foreign('afuncionalespersona_id')->references('id')->on('afuncionalespersonas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('afuncionales_sillas', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['afuncionalespersona_id']);
        });
    }
}
