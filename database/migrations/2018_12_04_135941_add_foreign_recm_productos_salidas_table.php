<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmProductosSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_productos_salidas
        Schema::table('recm_productos_salidas', function (Blueprint $table) {
            $table->foreign('salida_id')->references('id')->on('recm_salidas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('recm_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_productos_salidas', function (Blueprint $table) {
            $table->dropForeign(['salida_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropIndex('recm_productos_salidas_salida_id_foreign');
            $table->dropIndex('recm_productos_salidas_presentacion_producto_id_foreign');
            
        });
    }
}
