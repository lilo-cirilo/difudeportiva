<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatDependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_dependencias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('calle')->nullable();
            $table->integer('numero')->nullable();
            $table->string('colonia')->nullable();
            $table->string('telefono')->nullable();
            $table->string('encargado')->nullable();
            $table->string('cargoencargado')->nullable();
            $table->string('tipo')->nullable();
            $table->integer('entidad_id')->unsigned()->nullable();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_dependencias');
    }
}
