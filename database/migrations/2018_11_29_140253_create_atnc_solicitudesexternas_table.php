<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Captura de datos
  ALCANCE: Atención Ciudadana
*/

class CreateAtncSolicitudesexternasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('atnc_solicitudesexternas', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('solicitud_id')->unsigned();
        $table->integer('dependencia_id')->unsigned();
        $table->unsignedInteger('usuario_id');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('atnc_solicitudesexternas');
    }
}
