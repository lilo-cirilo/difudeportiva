<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian y Jhony
  MOTIVO: Reestructura del funcionamiento del sistema
  ALCANCE: DIF te lleva
*/

class CreateDtllServiciosTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('dtll_servicios', function (Blueprint $table) {
				$table->increments('id');

				$table->unsignedInteger('controlruta_id');
				$table->unsignedInteger('pasajero_id');
				$table->datetime('subida');
				$table->string('latitud');
				$table->string('longitud');

				$table->unsignedInteger('usuario_id');
				$table->timestamps();
				$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('dtll_servicios');
	}
}
