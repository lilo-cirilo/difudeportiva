<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmCatPartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_cat_partidas', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_cat_partidas', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_cat_partidas_usuario_id_foreign');
        });
    }
}
