<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class AddForeignAnioProgramaIdToBeneficiosprogramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->foreign('anio_programa_id')->references('id')->on('anios_programas');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('beneficiosprogramas', function (Blueprint $table) {
      $table->dropForeign(['anio_programa_id']);
      $table->dropIndex('beneficiosprogramas_anio_programa_id_foreign');
    });
  }
}
