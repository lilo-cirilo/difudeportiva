<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSiafViaticonacionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_viaticonacional', function (Blueprint $table) {
            $table->foreign('viatico_id')->references('id')->on('siaf_comisionesoficiales');
            $table->foreign('origenentidad_id')->references('id')->on('cat_municipios');
            $table->foreign('destinoentidad_id')->references('id')->on('cat_municipios');
            $table->foreign('origenregion_id')->references('id')->on('cat_regiones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_viaticonacional', function (Blueprint $table) {
            $table->dropForeign(['viatico_id']);
            // $table->dropIndex('siaf_viaticonacional_viatico_id_foreign');
            $table->dropForeign(['origenentidad_id']);
            $table->dropIndex('siaf_viaticonacional_origenentidad_id_foreign');
            $table->dropForeign(['destinoentidad_id']);
            $table->dropIndex('siaf_viaticonacional_destinoentidad_id_foreign');
            $table->dropForeign(['origenregion_id']);
            $table->dropIndex('siaf_viaticonacional_origenregion_id_foreign');
        });
    }
}
