<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignEscuelasProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('escuelas_programas', function (Blueprint $table) {
            $table->foreign('escuela_id')->references('id')->on('cat_escuelas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('escuelas_programas', function (Blueprint $table) {
            $table->dropForeign(["escuela_id"]);
            $table->dropForeign(["programa_id"]);
        });
    }
}
