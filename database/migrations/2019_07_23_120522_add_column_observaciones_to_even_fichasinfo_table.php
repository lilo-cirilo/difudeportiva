<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para ingresar datos de antecedentes
    ALCANCE: Eventos
*/

class AddColumnObservacionesToEvenFichasinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_fichasinfo', function (Blueprint $table) {
            $table->text('antecedentes')->nullable()->after('fila_honor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_fichasinfo', function (Blueprint $table) {
            $table->dropcolumn('antecedentes');
        });
    }
}
