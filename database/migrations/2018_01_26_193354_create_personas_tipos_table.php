<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas_tipos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipospersona_id')->unsigned()->unsigned();
            $table->integer('persona_id')->unsigned()->unsigned();
            $table->string('tabla');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas_tipos');
    }
}
