<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSoportedictamenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soportedictamenes', function (Blueprint $table) {
            $table->foreign('respuesta_id')->references('id')->on('respuestassoportesolicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('dirigidoa_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('realizo_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('autorizo_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soportedictamenes', function (Blueprint $table) {
            $table->dropForeign(["respuesta_id"]);
            $table->dropForeign(["dirigidoa_id"]);
            $table->dropForeign(["realizo_id"]);
            $table->dropForeign(["autorizo_id"]);
        });
    }
}
