<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class AddForeignToComisionesoficialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->string('folio', 30);
            $table->enum('tipo', ['ESTATAL', 'NACIONAL']);
            $table->unsignedInteger('clavepresupuestal_id');
            $table->unsignedInteger('transporte_id');
        });
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->foreign('clavepresupuestal_id')->references('id')->on('siaf_clavespresupuestales');
            $table->foreign('transporte_id')->references('id')->on('siaf_transportes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropForeign(['transporte_id']);
            $table->dropForeign(['clavepresupuestal_id']);
            $table->dropIndex('siaf_comisionesoficiales_transporte_id_foreign');  
            $table->dropIndex('siaf_comisionesoficiales_clavepresupuestal_id_foreign');
        });
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropColumn('folio');
            $table->dropColumn('tipo');
            $table->dropColumn('clavepresupuestal_id');
            $table->dropColumn('transporte_id');
        });
    }
}
