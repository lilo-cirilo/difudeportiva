<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian 
  MOTIVO: Se presentó en la necesidad de desarrollo en el sistema DIF te lleva
  ALCANCE: Para todos
*/

class AlterAddcolsCirculaciontarjetasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    //Hacemos la crorrección de cómo queremos nuestro tipo de dato
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->date('vigencia')->nullable()->change();
    });
    //Le cambiamos el valor a forma numérica
    DB::statement('UPDATE circulaciontarjetas SET vigencia=null');
    

    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      // $table->increments('id');
      // $table->unsignedInteger('unidadtransporte_id');
      $table->dropColumn('clase');
      $table->dropColumn('tipo');
      $table->string('oficina_expedidora', 50)->nullable()->change();
      $table->dropColumn('origen_vehiculo');
      $table->dropColumn('clave_vehicular');
      $table->date('fecha_expedicion')->after('oficina_expedidora')->nullable(); 
      $table->boolean('vigencia')->tinyInteger('vigencia')->unsigned()->change()->nullable();             //como va a pasar de tipo date a tinyInteger, para que no nos de error de compatibilidad, primero lo vamos a volver boolean
      $table->dropColumn('tipo_servicio');
      $table->dropColumn('cilindro');
      $table->dropColumn('numero_motor');
      $table->dropColumn('combustible');
      $table->dropColumn('numero_deatificacion');
      $table->dropColumn('placa');
      $table->dropColumn('numero_serie');
      $table->string('clave_repuve', 10)->nullable()->after('vigencia');
      $table->string('reg_ent', 2)->nullable()->after('clave_repuve');
      $table->string('verificacion_vehicular', 17)->after('reg_ent');

      $table->unsignedInteger('usuario_id')->after('verificacion_vehicular')->nullable();
    });
 
    //Le cambiamos el valor a forma numérica
    DB::statement('UPDATE circulaciontarjetas SET vigencia=0,  fecha_expedicion=now(), usuario_id=1 ');
    //Hacemos la crorrección de cómo queremos nuestro tipo de dato
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->unsignedInteger('usuario_id')->change();
      $table->date('fecha_expedicion')->change();
      $table->boolean('vigencia')->tinyInteger('vigencia')->unsigned()->change();  //Forzosamente se debe poner boolean y luego tinyInteger, si no, marca error
     
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->string('clase')->after('unidad_id')->nullable();
      $table->string('tipo')->after('clase')->nullable();
      $table->string('origen_vehiculo')->after('oficina_expedidora')->nullable();
      $table->string('clave_vehicular')->after('origen_vehiculo')->nullable();
      $table->dropcolumn('fecha_expedicion');
      $table->string('vigencia')->nullable()->change();                               //como va a pasar de tipo date a date, para que no nos de error de compatibilidad, primero lo vamos a volver string
      $table->string('tipo_servicio')->after('vigencia')->nullable();
      $table->integer('cilindro')->after('tipo_servicio')->nullable();
      $table->string('numero_motor')->after('cilindro')->nullable();
      $table->integer('combustible')->after('numero_motor')->nullable();
      $table->string('numero_deatificacion')->after('combustible')->nullable();
      $table->string('placa')->after('numero_deatificacion')->nullable();
      $table->string('numero_serie')->after('placa')->nullable();
      $table->dropColumn('clave_repuve');
      $table->dropColumn('reg_ent');
      $table->dropColumn('verificacion_vehicular');
      $table->dropColumn('usuario_id');
    });

    
    //Le cambiamos el valor a forma numérica
    DB::statement('UPDATE circulaciontarjetas SET vigencia=DATE_FORMAT( NOW(), "%Y-%m-%d" ) ');
    //Hacemos la crorrección de cómo queremos nuestro tipo de dato
    Schema::table('circulaciontarjetas', function (Blueprint $table) {
      $table->date('vigencia')->change();
    });
  }
}
