<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolEtniaPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('personas', function (Blueprint $table) {
        $table->integer('etnia_id')->after('huella')->unsigned()->default(1);
      });

      Schema::table('personas', function (Blueprint $table) {
        $table->foreign('etnia_id')->references('id')->on('cat_etnias')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('personas', function (Blueprint $table) {
        $table->dropForeign(['etnia_id']);
        $table->dropColumn('etnia_id');
      });
    }
}
