<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: Eliminacíón en columnas innecesarias e inserción de foránea tipoaccion_id
    ALCANCE: Alimentarios
*/

class AlterAddcolTipoaccionDropcolProgamaIdSubprogramaIdToAlimDotaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['subprograma_id']);
            $table->dropIndex('alim_dotaciones_subprograma_id_foreign');
            $table->dropUnique(['programa_id','subprograma_id']);
        });

        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->dropColumn('programa_id');
            $table->dropColumn('subprograma_id');
        });

        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->unsignedInteger('tipoaccion_id')->after('id');
            $table->foreign('tipoaccion_id')->references('id')->on('alim_tiposaccion');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->dropForeign(['tipoaccion_id']);
            $table->dropIndex('alim_dotaciones_tipoaccion_id_foreign');
        });

        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->unsignedInteger('programa_id')->after('id');
            $table->unsignedInteger('subprograma_id')->after('programa_id');
            $table->unique(['programa_id','subprograma_id']);
            $table->dropColumn('tipoaccion_id');
        });

        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('subprograma_id')->references('id')->on('programas');
        });
    }
}
