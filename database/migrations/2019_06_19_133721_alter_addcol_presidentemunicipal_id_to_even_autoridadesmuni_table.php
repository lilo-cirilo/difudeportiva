<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los presidentes municipales.
    ALCANCE: Eventos
*/

class AlterAddcolPresidentemunicipalIdToEvenAutoridadesmuniTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->unsignedInteger('presidentemunicipal_id')->after('fichainfo_id');
        });

        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->dropForeign(['cargo_id']);
            $table->dropForeign(['partidopolitico_id']);
            $table->dropIndex('even_autoridadesmuni_cargo_id_foreign');
            $table->dropIndex('even_autoridadesmuni_partidopolitico_id_foreign');
        });

        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->dropColumn('nombre');
			$table->dropColumn('cargo_id');
			$table->dropColumn('partidopolitico_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->dropColumn('presidentemunicipal_id');
            $table->string('nombre')->after('fichainfo_id');
			$table->unsignedInteger('cargo_id')->after('nombre');
			$table->unsignedInteger('partidopolitico_id')->after('cargo_id');
        });

        Schema::table('even_autoridadesmuni', function (Blueprint $table) {
            $table->dropForeign(['cargo_id']);
            $table->dropForeign(['partidopolitico_id']);

            $table->dropIndex('even_autoridadesmuni_cargo_id_foreign');
            $table->dropIndex('even_autoridadesmuni_partidopolitico_id_foreign');
        });
    }
}
