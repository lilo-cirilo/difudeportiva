<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtncEventosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('atnc_eventos_solicitudes', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('evento_id');
        $table->unsignedInteger('solicitud_id');
        $table->unsignedInteger('usuario_id');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('atnc_eventos_solicitudes');
    }
}
