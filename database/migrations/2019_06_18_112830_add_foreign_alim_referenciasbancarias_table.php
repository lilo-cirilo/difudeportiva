<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimReferenciasbancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_referenciasbancarias', function (Blueprint $table) {
            $table->foreign('programacion_id')->references('id')->on('alim_programacion');
            $table->foreign('banco_id')->references('id')->on('cat_bancos');
            $table->foreign('recibo_id')->references('id')->on('alim_recibos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_referenciasbancarias', function (Blueprint $table) {
            $table->dropForeign(['programacion_id']);
            $table->dropForeign(['banco_id']);
            $table->dropForeign(['recibo_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_referenciasbancarias_programacion_id_foreign');
            $table->dropIndex('alim_referenciasbancarias_banco_id_foreign');
            $table->dropIndex('alim_referenciasbancarias_recibo_id_foreign');
            $table->dropIndex('alim_referenciasbancarias_usuario_id_foreign');
        });
    }
}
