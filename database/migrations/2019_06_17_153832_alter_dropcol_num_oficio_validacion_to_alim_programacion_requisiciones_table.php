<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Se quita la columna num_oficio_validacion porque no es usable.
    ALCANCE: Alimentarios
*/

class AlterDropcolNumOficioValidacionToAlimProgramacionRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->dropColumn('num_oficio_validacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->tinyInteger('num_oficio_validacion')->unsigned()->after('programacion_id');
        });
    }
}
