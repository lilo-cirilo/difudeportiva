<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Se presentó en la necesidad de desarrollo para el sistema DIF te lleva
  ALCANCE: Para DIF te lleva
*/

class AlterColnullFechahoraSesionFinToControlrutasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('controlrutas', function (Blueprint $table) {
      $table->datetime('fechahora_sesion_fin')->nullable()->change();
    });

    DB::statement("UPDATE controlrutas SET fechahora_sesion_fin=NULL WHERE fechahora_sesion_fin='1900-01-01'");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    DB::statement("UPDATE controlrutas SET fechahora_sesion_fin='1900-01-01' WHERE fechahora_sesion_fin IS NULL");

    Schema::table('controlrutas', function (Blueprint $table) {
      $table->datetime('fechahora_sesion_fin')->change();
    });
  }
}
