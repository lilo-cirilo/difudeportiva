<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiSolicitudProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mopi_solicitudes_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->float('precio_unitario');
            $table->float('precio_total');
            $table->mediumText('descripcion');
            $table->unsignedInteger('estatus_id');
            $table->unsignedInteger('presentacion_producto_id');
            $table->unsignedInteger('solicitud_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mopi_solicitudes_productos');
    }
}
