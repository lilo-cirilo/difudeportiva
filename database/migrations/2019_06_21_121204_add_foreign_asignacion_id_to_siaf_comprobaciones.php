<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAsignacionIdToSiafComprobaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->foreign('asignacion_id')->references('id')->on('siaf_asignaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobaciones', function (Blueprint $table) {
            $table->dropForeign(['asignacion_id']);
            $table->dropIndex('siaf_comprobaciones_asignacion_id_foreign');
        });
    }
}
