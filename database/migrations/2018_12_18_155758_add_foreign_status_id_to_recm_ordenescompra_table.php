<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignStatusIdToRecmOrdenescompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->unsignedInteger('status_id');
            $table->foreign('status_id')->references('id')->on('recm_cat_estatuses')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->dropForeign(['status_id']);
            $table->dropIndex('recm_ordenescompra_status_id_foreign');
        });
    }
}
