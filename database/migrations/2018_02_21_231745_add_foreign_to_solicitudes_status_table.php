<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSolicitudesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_solicitudes', function (Blueprint $table) {
            $table->foreign('statusproceso_id')->references('id')->on('cat_statusprocesos')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('programas_solicitud_id')->references('id')->on('programas_solicitudes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('motivo_id')->references('id')->on('cat_motivos')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['statusproceso_id']);
            $table->dropForeign(['programas_solicitud_id']);
            $table->dropForeign(['motivo_id']);

        });
    }
}
