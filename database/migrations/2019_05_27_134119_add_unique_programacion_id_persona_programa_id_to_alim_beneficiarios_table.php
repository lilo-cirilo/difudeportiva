<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Se agregan restricciones unique para no repetir información
  ALCANCE: Alimentarios
*/

class AddUniqueProgramacionIdPersonaProgramaIdToAlimBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->unique(['programacion_id','persona_programa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->dropUnique(['programacion_id','persona_programa_id']);
        });
    }
}
