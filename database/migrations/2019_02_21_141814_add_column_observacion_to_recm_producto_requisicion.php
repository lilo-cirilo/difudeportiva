<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna para que almacene la observacion del producto
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnObservacionToRecmProductoRequisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_producto_requisicions', function (Blueprint $table) {
            $table->string('observacion')->after('precio'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_producto_requisicions',function(Blueprint $table){
            $table->dropColumn('observacion');
        });
    }
}
