<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_entradas
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('recm_cat_proveedors')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->dropForeign(['proveedor_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('recm_entradas_usuario_id_foreign');
            $table->dropIndex('recm_entradas_proveedor_id_foreign');
            
        });
    }
}
