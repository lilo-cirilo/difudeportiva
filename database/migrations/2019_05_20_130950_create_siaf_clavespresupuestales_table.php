<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class CreateSiafClavespresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_clavespresupuestales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('unidadejecutora_id');
            $table->unsignedInteger('area_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_clavespresupuestales');
    }
}
