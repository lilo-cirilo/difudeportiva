<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para capturar en alim_pagosrecibos los datos del pago de recibo y ya no capturarlos en la tabla alim_recibos
    ALCANCE: Alimentarios y Caja
*/

class CreateAlimPagosrecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_pagosrecibos', function (Blueprint $table) {
            $table->increments('id');
						$table->unsignedInteger('recibo_id');
						$table->string('folio_caja',20);
            $table->date('fecha_pago');
						$table->decimal('importe_total', 12, 2);
            $table->smallInteger('total_dotaciones');
            $table->text('texto_pago');
						$table->boolean('iscancelado')->default(0);
						$table->varchar('nombre_usuario')->nullable();
						$table->text('motivo_cancelacion')->nullable();

						$table->unsignedInteger('usuario_id')->nullable();
            $table->timestamps();
						$table->softDeletes();

						$table->unique(['folio_caja','iscancelado']);
        });

				DB::statement('INSERT INTO alim_pagosrecibos (recibo_id, folio_caja, fecha_pago, importe_total, total_dotaciones, texto_pago, usuario_id, created_at, updated_at, deleted_at) '.
											'SELECT id, folio_caja, fecha_pago, importe_total, total_dotaciones, texto_pago, usuario_id, created_at, updated_at, deleted_at '.
											'FROM alim_recibos '.
											'WHERE folio_caja IS NOT NULL'
										);

				/*Schema::create('alim_pagosrecibos', function (Blueprint $table) {
					$table->dropColumn(['folio_caja', 'fecha_pago', 'importe_total', 'total_dotaciones', 'texto_pago']);
				}*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			/*Schema::table('alim_recibos', function (Blueprint $table) {
						$table->string('folio_caja',20)->nullable()->after('referenciabancaria_id');
            $table->date('fecha_pago')->nullable()->after('folio_caja');
						$table->decimal('importe_total', 12, 2)->nullable()->after('fecha_pago');
            $table->smallInteger('total_dotaciones')->nullable()->after('importe_total');
            $table->text('texto_pago')->nullable()->after('total_dotaciones');
        });*/

				/*DB::statement('UPDATE alim_pagosrecibos pr INNER JOIN alim_recibos r ON (pr.recibo_id=r.id) '.
											'SET r.folio_caja=pr.folio_caja, r.fecha_pago=pr.fecha_pago, r.importe_total=pr.importe_total, '.
													'r.total_dotaciones=pr.total_dotaciones, r.texto_pago=pr.texto_pago '.
											'WHERE pr.iscancelado=0'
										);*/

        Schema::dropIfExists('alim_pagosrecibos');
    }
}
