<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony, Emanuel.
    MOTIVO: Para ingresar datos de personas que tienen un beneficios sin generar una solicitud
    ALCANCE: Solicitudes
*/

class AddForeignBeneficiosprogPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiosprog_personas', function (Blueprint $table) {
            $table->foreign('beneficioprograma_id')->references('id')->on('beneficiosprogramas');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiosprog_personas', function (Blueprint $table) {
            $table->dropForeign(['beneficioprograma_id']);
            $table->dropForeign(['persona_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('beneficiosprog_personas_beneficioprograma_id_foreign');
            $table->dropIndex('beneficiosprog_personas_persona_id_foreign');
            $table->dropIndex('beneficiosprog_personas_usuario_id_foreign');
        });
    }
}
