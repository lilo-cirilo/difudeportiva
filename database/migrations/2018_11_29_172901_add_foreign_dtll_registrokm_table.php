<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDtllRegistrokmTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_registrokm', function (Blueprint $table) {
      $table->foreign('controlruta_id')->references('id')->on('controlrutas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_registrokm', function (Blueprint $table) {
      $table->dropForeign(['controlruta_id']);
      $table->dropIndex('dtll_registrokm_controlruta_id_foreign');
    });
  }
}
