<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar los pagos con el folio de comprobacion
    ALCANCE: fondorotatorio
*/

class CreateSiafComprobacionesEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_comprobaciones_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('comprobacion_id');
            $table->unsignedInteger('pago_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_comprobaciones_estados');
    }
}
