<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos que nos proporiconan las áreas de montos invertidos en programas
  ALCANCE: Para estadísticas en sistema Eventos
*/

class CreateInveCatRubrosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('inve_cat_rubros', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->timestamps();
      $table->softDeletes();
    });

    DB::table('inve_cat_rubros')->insert([
      ['nombre'=>'ALIMENTARIOS', 'created_at'=>NOW()],
      ['nombre'=>'DISCAPACIDAD', 'created_at'=>NOW()],
      ['nombre'=>'DESARROLLO FAMILIAR COMUNITARIO', 'created_at'=>NOW()],
      ['nombre'=>'POBLACIÓN VULNERABLE', 'created_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('inve_cat_rubros');
  }
}
