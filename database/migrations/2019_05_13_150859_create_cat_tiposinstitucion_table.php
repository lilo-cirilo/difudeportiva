<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se crea cat_tipos_institucion
  ALCANCE: Base Principal
*/

class CreateCatTiposinstitucionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('cat_tiposinstitucion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('cat_tiposinstitucion')->insert([
            ['nombre'=>'INSTITUCIÓN', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre'=>'DEPENDENCIA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_tiposinstitucion');
    }
}
