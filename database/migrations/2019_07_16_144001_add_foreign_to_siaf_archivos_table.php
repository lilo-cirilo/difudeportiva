<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSiafArchivosTable extends Migration
{
    /**
     * PETICIÓN: Luis V.
     * MOTIVO: Agregar llave foranea para la comprobacion en la tabla archivos
     * ALCANCE: FONDO ROTATORIO
     * 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->foreign('detallespago_id')->references('id')->on('siaf_detallespago');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->dropForeign(['detallespago_id']);
            $table->dropIndex('siaf_archivos_detallespago_id_foreign');
        });
    }
}
