<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenActividadesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_actividades', function (Blueprint $table) {
      $table->foreign('fichainfo_id')->references('id')->on('even_fichasinfo')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_actividades', function (Blueprint $table) {
      $table->dropForeign(['fichainfo_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('even_actividades_fichainfo_id_foreign');
      $table->dropIndex('even_actividades_usuario_id_foreign');
    });
  }
}
