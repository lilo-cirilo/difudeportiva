<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Relaciones de la tabla recm_requisiciones con la tabla de recm_cat_tipos
  ALCANCE: Para el sistema de recursos materiales
*/

class AddForeignTipoToRecmRequisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->foreign('tipo')->references('id')->on('recm_cat_tipos');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->dropForeign(['tipo']);
            $table->dropIndex('recm_requisicions_tipo_foreign');
          });
    }
}
