<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignInstitucionesrubrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('instituciones_rubros', function(Blueprint $table){
        $table->foreign('institucion_id')->references('id')->on('cat_instituciones')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('rubro_id')->references('id')->on('cat_rubros')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('instituciones_rubros', function(Blueprint $table){
        $table->dropForeign(['institucion_id']);
        $table->dropForeign(['rubro_id']);
      });
    }
}
