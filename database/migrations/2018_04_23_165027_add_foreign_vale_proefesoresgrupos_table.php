<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeProefesoresgruposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vale_proefesoresgrupos', function (Blueprint $table) {
            $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('grupodisponible_id')->references('id')->on('vale_gruposdisponibles')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vale_proefesoresgrupos', function (Blueprint $table) {
            $table->dropForeign(["empleado_id"]);
            $table->dropForeign(["grupodisponible_id"]);
        });
    }
}
