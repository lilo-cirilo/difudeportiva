<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de alimentarios
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateCargosProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('cargos_programas', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('cargo_id');
        $table->unsignedInteger('programa_id');
        $table->timestamps();
        $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('cargos_programas');
  }
}
