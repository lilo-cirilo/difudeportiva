<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimPresentacionesProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('recm_cat_productos');
            $table->foreign('presentacion_id')->references('id')->on('recm_cat_presentacions');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['presentacion_id']);
            $table->dropForeign(['usuario_id']);
            // $table->dropIndex('alim_presentaciones_productos_producto_id_presentacion_id_unique'); // Ya no es necesario porque se vuelve unique
            $table->dropIndex('alim_presentaciones_productos_presentacion_id_foreign');
            $table->dropIndex('alim_presentaciones_productos_usuario_id_foreign');
        });
    }
}
