<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oficios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio', 25)->unique();
            $table->date('fecha');
            $table->boolean('interno');
            $table->string('nombre_destinatario', 100);
            $table->string('puesto_destinatario', 100);
            $table->text('asunto');
            $table->string('nombre_solicitante', 100);
            // $table->date('fecha_acuse')->nullable();
            // $table->text('observaciones')->nullable();

            $table->unsignedInteger('id_tipooficio');
            $table->unsignedInteger('id_areasolicitante');

            $table->foreign('id_tipooficio')->references('id')->on('cat_tiposoficios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('id_areasolicitante')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('oficios', function(Blueprint $table){
        $table->dropForeign(['id_tipooficio']);
        $table->dropForeign(['id_areasolicitante']);
      });
      
      Schema::dropIfExists('oficios');
    }
}
