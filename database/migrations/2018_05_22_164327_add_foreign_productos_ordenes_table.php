<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignProductosOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos_ordenes', function (Blueprint $table) {
            $table->foreign('orden_id')->references('id')->on('ordenes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('producto_requisicion_id')->references('id')->on('productos_requisiciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos_ordenes', function (Blueprint $table) {
            $table->dropForeign(['orden_id']);
            $table->dropForeign(['producto_requisicion_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
