<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de riesgos de temas sísmicos, inundaciones, vientos fuertes, heladas, deslizamientos e incendios forestales
  ALCANCE: Riesgos
*/

class CreateRiesCatCorredoressismicosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('ries_cat_corredoressismicos', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre',50);
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['nombre','deleted_at']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('ries_cat_corredoressismicos');
  }
}
