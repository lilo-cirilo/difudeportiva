<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignControlrutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('controlrutas', function (Blueprint $table) {
            // $table->foreign('recorrido_id')->references('id')->on('recorridos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('unidad_id')->references('id')->on('unidades')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('conductor_id')->references('id')->on('conductores')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('controlrutas', function (Blueprint $table) {
            // $table->dropForeign(["recorrido_id"]);
            $table->dropForeign(["unidad_id"]);
            $table->dropForeign(["conductor_id"]);
        });
    }
}
