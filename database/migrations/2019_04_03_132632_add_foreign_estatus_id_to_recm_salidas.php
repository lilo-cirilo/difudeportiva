<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Relacion de estatus_id con recm_cat_estatuses
  ALCANCE: Para el sistema de recursos materiales
*/

class AddForeignEstatusIdToRecmSalidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $estatus = DB::table('recm_cat_estatuses')->first();
        if ($estatus)
          DB::table('recm_salidas')->update(['estatus_id'=>$estatus->id]);

        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->foreign('estatus_id')->references('id')->on('recm_cat_estatuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->dropForeign(['estatus_id']);
            $table->dropIndex('recm_salidas_estatus_id_foreign');
        });
    }
}
