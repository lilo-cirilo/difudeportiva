<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los diputados locales.
    ALCANCE: Eventos
*/

class AddForeignEvenAutoridadesdipuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_autoridadesdipu', function (Blueprint $table) {
            $table->foreign('fichainfo_id')->references('id')->on('even_fichasinfo');
            $table->foreign('diputadolocal_id')->references('id')->on('diputadoslocales');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_autoridadesdipu', function (Blueprint $table) {
            $table->dropForeign(['fichainfo_id']);
            $table->dropForeign(['diputadolocal_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('even_autoridadesdipu_fichainfo_id_foreign');
            $table->dropIndex('even_autoridadesdipu_diputadolocal_id_foreign');
            $table->dropIndex('even_autoridadesdipu_usuario_id_foreign');
        });
    }
}
