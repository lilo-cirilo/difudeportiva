<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class AddForeignBeneficiosprogramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiosprogramas', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiosprogramas', function (Blueprint $table) {
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('beneficiosprogramas_programa_id_foreign');
            $table->dropIndex('beneficiosprogramas_usuario_id_foreign');
        });
    }
}
