<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class AddForeignPetaClinicassaludUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peta_clinicassalud_usuarios', function (Blueprint $table) {
            $table->foreign('institucion_usuario_id')->references('id')->on('peta_instituciones_usuarios');
            $table->foreign('clinicasalud_id')->references('id')->on('cat_clinicassalud');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peta_clinicassalud_usuarios', function (Blueprint $table) {
            $table->dropForeign(['institucion_usuario_id']);    // Ya no genra index por el unique
            $table->dropForeign(['clinicasalud_id']);
            $table->dropForeign(['usuario_id']);
            
            $table->dropIndex('peta_clinicassalud_usuarios_clinicasalud_id_foreign');
            $table->dropIndex('peta_clinicassalud_usuarios_usuario_id_foreign');
        });
    }
}
