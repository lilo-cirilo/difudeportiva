<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllPasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dtll_pasajeros', function (Blueprint $table) 
      {
        $table->increments('id');
        $table->integer('tiposervicio_id')->unsigned();
        $table->integer('tipopasajero_id')->unsigned();
        $table->date('fechaIniServ');
        $table->date('fechaFinServ');
        $table->integer('timestampFinServ');
        //$table->index:"B0009"
        $table->tinyInteger('numMesesServ')->unsigned();
        $table->string('folioPago',15);
        $table->integer('lugarPago_id')->unsigned();
        $table->string('numPasajero',7);
        $table->string('numPasajeroAmbos',7)->nullable();
        $table->integer('persona_id')->unsigned();
        //$table->string('region',20),
        //$table->integer('estado_id')->unsigned(),
        //$table->string('estado',20),
        // $table->unsignedInteger('pais_id');
        //$table->string('pais',20),
        $table->string('persona_emergencias',120)->nullable();
        $table->string('telefono_emergencias',20)->nullable();
        $table->integer('usuario_id')->unsigned();
        $table->datetime('fechaInsert')->default(NOW());
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_pasajeros');
    }
}
