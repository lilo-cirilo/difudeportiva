<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class CreateAlimChoferespreveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_choferesproveedor', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('modulo_proveedor_id');
            $table->string('nombre',30);
            $table->string('primer_apellido',40);
            $table->string('segundo_apellido',40)->nullable();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['modulo_proveedor_id','nombre','primer_apellido','segundo_apellido'],'alim_choferesproveedor_modulo_proveedor_id_nombrecompleto_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_choferesproveedor');
    }
}
