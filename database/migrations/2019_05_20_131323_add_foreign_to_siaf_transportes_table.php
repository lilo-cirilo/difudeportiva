<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class AddForeignToSiafTransportesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_transportes', function (Blueprint $table) {
            $table->foreign('tipotransporte_id')->references('id')->on('siaf_cat_tipostransportes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_transportes', function (Blueprint $table) {
            $table->dropForeign(['tipotransporte_id']);
            $table->dropIndex('siaf_transportes_tipotransporte_id_foreign');  
        });
    }
}
