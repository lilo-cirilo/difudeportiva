<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega el campo folio y estatus para que pueda cancelar la entrada
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnFolioAndEstatusidToRecmEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->string('folio')->after('fecha_hora');
            $table->unsignedInteger('estatus_id')->after('fecha_hora');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas',function(Blueprint $table){
            $table->dropColumn('folio');
            $table->dropColumn('estatus_id');
        });
    }
}
