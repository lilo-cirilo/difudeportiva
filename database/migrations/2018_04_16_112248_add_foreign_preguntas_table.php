<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPreguntasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntas', function (Blueprint $table) {
            $table->foreign('encuesta_id')->references('id')->on('programas_encuestas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('tipospregunta_id')->references('id')->on('cat_tipospreguntas')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preguntas', function (Blueprint $table) {
            $table->dropForeign(["encuesta_id"]);
             $table->dropForeign(["tipospregunta_id"]);
        });
    }
}
