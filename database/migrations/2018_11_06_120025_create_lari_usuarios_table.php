<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel 
  MOTIVO: Se presentó en la necesidad de desarrollo para el sistema Lari_DIF
  ALCANCE: Para Lari_DIF
*/

class CreateLariUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('lari_usuarios', function (Blueprint $table) {
      $table->increments('id');
      $table->string('usuario');
      $table->string('contrasenia');
      $table->unsignedInteger('municipio_id');
      $table->unsignedInteger('localidad_id')->nullable();
      $table->smallInteger('num_beneficiarios');
      $table->unsignedInteger('rol_id')->nullable();
      $table->unsignedInteger('programa_solicitud_id')->nullable();
      $table->timestamps();
      $table->softDeletes();
    });

    DB::statement('ALTER TABLE lari_usuarios MODIFY COLUMN created_at TIMESTAMP DEFAULT NOW() ');
    DB::statement('ALTER TABLE lari_usuarios MODIFY COLUMN updated_at TIMESTAMP NULL ON UPDATE NOW() ');
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('lari_usuarios');
  }
}
