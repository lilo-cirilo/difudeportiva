<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignActivosMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activos_medicamentos', function (Blueprint $table) {
            $table->foreign('activo_id')->references('id')->on('cat_ingredientesactivos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('medicamento_id')->references('id')->on('cat_medicamentos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activos_medicamentos', function (Blueprint $table) {
            $table->dropForeign(["activo_id"]);
            $table->dropForeign(["medicamento_id"]);
        });
    }
}
