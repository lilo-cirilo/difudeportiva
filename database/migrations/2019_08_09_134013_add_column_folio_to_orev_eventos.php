<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFolioToOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('orev_eventos', function (Blueprint $table) {
				$table->string('folio_peticion')->nullable()->after('folio');
				$table->unsignedInteger('peticion_id')->nullable()->after('folio');
			}); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('orev_eventos', function (Blueprint $table) {
				$table->dropColumn('folio_peticion');
				$table->dropColumn('peticion_id');
			});
    }
}
