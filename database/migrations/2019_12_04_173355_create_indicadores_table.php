<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para ingresar datos de indicadores
    ALCANCE: General
*/

class CreateIndicadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicadores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('indicador_id');
            $table->unsignedInteger('municipio_id');
            $table->unsignedInteger('poblacion');
            $table->decimal('porcentaje',9,6);
            $table->smallinteger('periodo_ini')->unsigned()->nullable();
            $table->smallinteger('periodo_fin')->unsigned()->nullable();
            $table->unsignedInteger('fuente_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['indicador_id','municipio_id','periodo_ini','fuente_id'],'indicadores_indic_id_mun_id_per_ini_fuente_id_unique');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indicadores');
    }
}
