<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToCycConveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cyc_convenios', function (Blueprint $table) {
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
        });

        Schema::table('sdoc_responsables', function (Blueprint $table) {
            $table->dropForeign(['instituciones_responsables_id']);
            $table->dropIndex('sdoc_responsables_instituciones_responsables_id_foreign');  
            $table->foreign('instituciones_responsables_id')->references('id')->on('cat_instituciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('cyc_convenios', function (Blueprint $table) {
            $table->dropForeign(['institucion_id']);   
            $table->dropIndex('cyc_convenios_institucion_id_foreign');     
            });
        
        Schema::table('sdoc_responsables', function (Blueprint $table) {
            $table->dropForeign(['instituciones_responsables_id']);
            $table->dropIndex('sdoc_responsables_instituciones_responsables_id_foreign');  
            $table->foreign('instituciones_responsables_id')->references('id')->on('sdoc_instituciones_responsables');
            });
    }
}
