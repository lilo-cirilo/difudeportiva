<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Juan.
    MOTIVO: Para ingresar datos extra de cuotas de recuperación
    ALCANCE: Alimentarios
*/

class CreateAlimRefbancariapagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_refbancariapagos', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('referenciabancaria_id');
            $table->date('fecha_cuota');
            $table->string('descripcion');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_refbancariapagos');
    }
}
