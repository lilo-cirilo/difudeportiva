<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_historial_documentos
  ALCANCE: Para el sistema SICODOC
*/



class AddForeignSdocHistorialDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
  {
    Schema::table('sdoc_seguimiento_documentos', function (Blueprint $table) {
      $table->foreign('documento_id')->references('id')->on('sdoc_documentos');
      $table->foreign('estatus_id')->references('id')->on('sdoc_cat_estatus');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
      $table->foreign('areaturnado_id')->references('id')->on('cat_areas');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('sdoc_seguimiento_documentos', function (Blueprint $table) {
      $table->dropForeign(['documento_id']);
      $table->dropForeign(['estatus_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropForeign(['areaturnado_id']);
      
      $table->dropIndex('sdoc_seguimiento_documentos_documento_id_foreign');
      $table->dropIndex('sdoc_seguimiento_documentos_estatus_id_foreign');
      $table->dropIndex('sdoc_seguimiento_documentos_usuario_id_foreign');
      $table->dropIndex('sdoc_seguimiento_documentos_areaturnado_id_foreign');
    });
  }
}
