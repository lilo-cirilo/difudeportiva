<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar los pagos con el fondo
    ALCANCE: fondorotatorio
*/

class AddColumnFondoIdToSiafPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->unsignedInteger('fondo_id')->after('programa_id');
        });

        $registro = DB::table('siaf_fondorotatorioareas')->first();
        if ($registro)
            DB::table('siaf_pagos')->update(['fondo_id'=>$registro->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->dropColumn('fondo_id');
        });
    }
}
