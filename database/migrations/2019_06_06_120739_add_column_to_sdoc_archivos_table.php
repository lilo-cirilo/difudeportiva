<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSdocArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_archivos', function (Blueprint $table) {
            $table->string('tamanio', 30)->after('url');
            $table->string('mime', 100)->after('tamanio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_archivos', function (Blueprint $table) {
            $table->dropColumn('tamanio');
            $table->dropColumn('mime');
        });
    }
}
