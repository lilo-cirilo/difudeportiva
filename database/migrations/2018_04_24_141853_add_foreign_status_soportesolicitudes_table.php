<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignStatusSoportesolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_soportesolicitudes', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('soportesolicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('status_id')->references('id')->on('soporteestados')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_soportesolicitudes', function (Blueprint $table) {
            $table->dropForeign(["solicitud_id"]);
            $table->dropForeign(["status_id"]);
        });
    }
}
