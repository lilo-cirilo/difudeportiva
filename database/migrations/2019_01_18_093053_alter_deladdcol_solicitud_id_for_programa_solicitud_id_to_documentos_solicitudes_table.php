<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/*
  PETICIÓN: Orlando Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AlterDeladdcolSolicitudIdForProgramaSolicitudIdToDocumentosSolicitudesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    
    $documentos_solicitudes = DB::table('documentos_solicitudes')->get()->count();
    if ($documentos_solicitudes > 0) {
      $programa = DB::table('programas')->where('nombre','ASISTENCIA ALIMENTARIA')->first();
      if ($programa === null){
        $programa = DB::table('programas')->insert(
          ['nombre' => 'ASISTENCIA ALIMENTARIA', 'activo' => 1, 'descripcion'=>'ALIMENTARIOS', 'tipo'=>'PROGRAMA', 'oficial'=>1, 'puede_agregar_benef'=>0, 'usuario_id'=>1]
        );
      }

      $estados_programas = DB::table('estados_programas')->where('programa_id',$programa->id)->count();
      if ($estados_programas === 0){
        DB::table('estados_programas')->insert([
          ['estado_id' => DB::table('cat_estados')->where('nombre','ALTA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>1, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','ACTIVA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>2, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','CANCELADA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>3, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','BAJA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>4, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','REVISION')->first()->id, 'programa_id'=>$programa->id, 'orden'=>5, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','ENVIADA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>6, 'usuario_id'=>1, 'created_at'=>NOW()],
          ['estado_id' => DB::table('cat_estados')->where('nombre','RECIBIDA')->first()->id, 'programa_id'=>$programa->id, 'orden'=>7, 'usuario_id'=>1, 'created_at'=>NOW()]
        ]);
      }
    }
    

    Schema::table('documentos_solicitudes', function (Blueprint $table) {
      $table->dropForeign(['solicitud_id']);
      $table->dropIndex('documentos_solicitudes_solicitud_id_foreign');
      $table->dropColumn('solicitud_id');
      
      $table->unsignedInteger('programa_solicitud_id')->after('documentos_persona_id');
    });
    
    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('documentos_solicitudes', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('documentos_solicitudes')->get();
      if ($temp->count()>0)
        DB::statement('UPDATE documentos_solicitudes SET programa_solicitud_id=(SELECT MIN(id) FROM programas_solicitudes)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('programa_solicitud_id')->references('id')->on('programas_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
    });


  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('documentos_solicitudes', function (Blueprint $table) {
      $table->dropForeign(['programa_solicitud_id']);
      $table->dropIndex('documentos_solicitudes_programa_solicitud_id_foreign');
      $table->dropColumn('programa_solicitud_id');
      
      $table->unsignedInteger('solicitud_id')->after('documentos_persona_id');
    });

    //Establecemos la relación foránea, forzosamente tenemos que hacerla como otro Schema, para que la reconosca el DB::table
    Schema::table('documentos_solicitudes', function (Blueprint $table) {
      //--------- Si ya hay datos en la tabla que voy a relacionar, le clavamos un id por default de la que se va a amarrar ---------
      $temp = DB::table('documentos_solicitudes')->count();
      if ($temp>0)
        DB::statement('UPDATE documentos_solicitudes SET solicitud_id=(SELECT MIN(id) FROM solicitudes)');
      //-----------------------------------------------------------------------------------------------------------------------------
      $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
