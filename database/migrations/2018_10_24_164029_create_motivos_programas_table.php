<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

/*
  PETICIÓN: Jerza 
  MOTIVO: Se presentó la necesidad en el desarrollo del sistema Peticiones
  ALCANCE: Se creó la tabla para todos
*/

class CreateMotivosProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('motivos_programas', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('motivo_id');
      $table->unsignedInteger('programa_id');
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });

    // Respaldamos los datos de la tabla cat_motivos
    // DB::statement('CREATE TABLE z_cat_motivos SELECT * FROM cat_motivos');
    // Borramos los repetidos, sólo para demo y prueba
    // DB::statement('delete from cat_motivos where id not in (select min(id)  from cat_motivos group by motivo order by min(id))');

    // DB::statement('INSERT INTO motivos_programas (motivo_id, programa_id, usuario_id, created_at) SELECT id AS motivo_id, (SELECT id FROM programas WHERE nombre=cat_motivos.tipo) AS programa_id, usuario_id, created_at FROM cat_motivos');
    /*DB::table('motivos_programas')->insert(
      (array) 
    );*/
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('motivos_programas');
    Schema::dropIfExists('z_cat_motivos');
  }
}
