<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBinestarpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bienestarpersonas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prefolio');
            $table->integer('foliounico')->nullable();
            $table->integer('posicion')->nullable();
            $table->integer('discapacidad_id')->unsigned()->nullable();
            $table->integer('persona_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bienestarpersonas');
    }
}
