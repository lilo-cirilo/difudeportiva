<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se crea relacion con requisicion
  ALCANCE: Para el sistema de recursos materiales
*/

class AddForeignRequisicionIdToRecmEntrdas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->unsignedInteger('requisicion_id')->after('fecha_hora');
        });
        
        $requisicion = DB::table('recm_requisicions')->first();
        if ($requisicion)
            DB::table('recm_entradas')->update(['requisicion_id'=>$requisicion->id]);

        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('recm_requisicions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropIndex('recm_entradas_requisicion_id_foreign');
            $table->dropColumn('requisicion_id');
        });
    }
}
