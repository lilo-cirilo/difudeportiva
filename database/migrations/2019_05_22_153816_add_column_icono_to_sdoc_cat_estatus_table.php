<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agregan columnas para guardar información adicional de los estatus
  ALCANCE: Para el sistema SICODOC
*/

class AddColumnIconoToSdocCatEstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_cat_estatus', function (Blueprint $table) {
            $table->string('icono')->after('color');
            $table->string('descripcion')->after('icono');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_cat_estatus', function (Blueprint $table) {
            $table->dropColumn('icono');
            $table->dropColumn('descripcion');
        });
    }
}
