<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCredCertificadomedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cred_certificadomedico', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('persona_id')->unsigned();
        $table->string('ruta_imagen');
        $table->integer('institucion_rubro_id')->unsigned();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cred_certificadomedico');
    }
}
