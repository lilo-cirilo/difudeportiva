<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Se agrega tabla para asignar las personas a un evento
  ALCANCE: Para el sistema de eventos
*/

class AddForeignEvenEventosPersonasTable extends Migration
{
  /**
    * Run the migrations.
    *
    * @return void
    */
  public function up()
  {
    Schema::table('even_eventos_personas', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('even_eventos');
      $table->foreign('persona_id')->references('id')->on('personas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
    * Reverse the migrations.
    *
    * @return void
    */
  public function down()
  {
    Schema::table('even_eventos_personas', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['persona_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('even_eventos_personas_evento_id_foreign');
      $table->dropIndex('even_eventos_personas_persona_id_foreign');
      $table->dropIndex('even_eventos_personas_usuario_id_foreign');
    });
  }
}
