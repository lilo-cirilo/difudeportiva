<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCertificadomedicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cred_certificadomedico', function(Blueprint $table){
        $table->foreign('institucion_rubro_id')->references('id')->on('instituciones_rubros')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cred_certificadomedico', function(Blueprint $table){
        $table->dropForeign(['institucion_rubro_id']);
      });
    }
}
