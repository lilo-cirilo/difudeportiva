<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alex.
  MOTIVO: Para ingresar datos de atención ciudadana
  ALCANCE: Atención ciudadana.
*/

class RenameAtncGiraspreregisProgramasToAtncGiraspreregisBeneficiosprogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->dropForeign(['girapreregis_id']);
            $table->dropForeign(['programa_id']);

            $table->dropIndex('atnc_giraspreregis_programas_girapreregis_id_foreign');
            $table->dropIndex('atnc_giraspreregis_programas_programa_id_foreign');
        });

        Schema::rename('atnc_giraspreregis_programas', 'atnc_giraspreregis_beneficiosprog');

        Schema::table ('atnc_giraspreregis_beneficiosprog', function(Blueprint $table){
            $table->dropColumn('programa_id');
            $table->unsignedInteger('beneficioprograma_id')->after('girapreregis_id');
            $table->unsignedInteger('usuario_id')->after('beneficioprograma_id');
        });

        $registro = DB::table('beneficiosprogramas')->first();
        if ($registro)
            DB::table('atnc_giraspreregis_beneficiosprog')->update(['beneficioprograma_id'=>$registro->id]);
        DB::table('atnc_giraspreregis_beneficiosprog')->update(['usuario_id'=>1]);

        Schema::table ('atnc_giraspreregis_beneficiosprog', function(Blueprint $table){
            $table->foreign('girapreregis_id')->references('id')->on('atnc_giraspreregis');
            $table->foreign('beneficioprograma_id')->references('id')->on('beneficiosprogramas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table ('atnc_giraspreregis_beneficiosprog', function(Blueprint $table)
        {
            $table->dropForeign(['girapreregis_id']);
            $table->dropForeign(['beneficioprograma_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('atnc_giraspreregis_beneficiosprog_girapreregis_id_foreign');
            $table->dropIndex('atnc_giraspreregis_beneficiosprog_beneficioprograma_id_foreign');
            $table->dropIndex('atnc_giraspreregis_beneficiosprog_usuario_id_foreign');
        });

        Schema::rename('atnc_giraspreregis_beneficiosprog', 'atnc_giraspreregis_programas');

        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->dropColumn('beneficioprograma_id');
            $table->dropColumn('usuario_id');
            $table->unsignedInteger('programa_id')->after('girapreregis_id');
            
            $table->foreign('girapreregis_id')->references('id')->on('atnc_giraspreregis');
            $table->foreign('programa_id')->references('id')->on('programas');
        });
    }
}
