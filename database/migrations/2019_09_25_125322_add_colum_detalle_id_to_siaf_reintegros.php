<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guardar la relacion con la partida.
    ALCANCE: SIAF
*/

class AddColumDetalleIdToSiafReintegros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('siaf_reintegros', function (Blueprint $table) {
				$table->unsignedInteger('detalle_id')->nullable()->after('cantidad');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('siaf_reintegros', function (Blueprint $table) {
				$table->dropColumn('detalle_id');
			});
    }
}
