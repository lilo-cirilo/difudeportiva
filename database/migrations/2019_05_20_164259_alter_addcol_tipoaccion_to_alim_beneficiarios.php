<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para identificar acciones no oficiales a los que pertenecen los beneficiarios de alimentarios
  ALCANCE: modulo de Alimentarios
*/

class AlterAddcolTipoaccionToAlimBeneficiarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->unsignedInteger('tipoaccion_id')->after('esta_embarazada_lactando');
        });

        $registro = DB::table('alim_tiposaccion')->first();
        if ($registro)
            DB::table('alim_beneficiarios')->update(['tipoaccion_id'=>$registro->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_beneficiarios', function (Blueprint $table) {
            $table->dropColumn('tipoaccion_id');
        });
    }
}
