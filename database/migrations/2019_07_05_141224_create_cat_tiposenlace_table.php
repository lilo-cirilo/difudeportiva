<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para los enlaces que tendrán los directivos
    ALCANCE: General
*/

class CreateCatTiposenlaceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_tiposenlace', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',20);

            $table->timestamps();
            $table->softDeletes();

            $table->unique('nombre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_tiposenlace');
    }
}
