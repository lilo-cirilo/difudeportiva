<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SICODOC
  ALCANCE: Para el sistema SICODOC
*/


class CreateSdocResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_responsables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('areas_responsables_id')->nullable();
            $table->unsignedInteger('instituciones_responsables_id')->nullable();
            $table->string('tipo',25);
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_responsables');
    }
}
