<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Manuel
  MOTIVO: Se agregan columna para relacionar al usuario de rm que atendera esa requisicion
  ALCANCE: para el sistema de requisiciones recmat
*/

class RenameColumProveedorIdToRecmEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->dropForeign(['proveedor_id']);
            $table->dropIndex('recm_entradas_proveedor_id_foreign');
        });

        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->renameColumn('proveedor_id', 'orden_compra_id');
        });

        $idOD = DB::table('recm_ordenescompra')->first();
            if ($idOD)
              DB::table('recm_entradas')->update(['orden_compra_id'=>$idOD->id]);

        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->foreign('orden_compra_id')->references('id')->on('recm_ordenescompra');                       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->dropForeign(['orden_compra_id']);
            $table->dropIndex('recm_entradas_orden_compra_id_foreign');
        });

        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->renameColumn('orden_compra_id', 'proveedor_id');
        });
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->foreign('proveedor_id')->references('id')->on('recm_cat_proveedors');                       
        });
    }
}
