<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeprofesoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_Profesores', function(Blueprint $table){
        $table->foreign('empleado_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('grado_academico_id')->references('id')->on('vale_cat_GradosAcademicos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('especialidad_id')->references('id')->on('vale_cat_Especialidades')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_Profesores', function(Blueprint $table){
        $table->dropForeign(['empleado_id']);
        $table->dropForeign(['grado_academico_id']);
        $table->dropForeign(['especialidad_id']);
      });
    }
}
