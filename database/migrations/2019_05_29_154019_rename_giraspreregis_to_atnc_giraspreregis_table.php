<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alex.
  MOTIVO: Para ingresar datos de atención ciudadana
  ALCANCE: Atención ciudadana.
*/

class RenameGiraspreregisToAtncGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Quitamos foráneas exportadas
        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->dropForeign(['girapreregis_id']);
            $table->dropIndex('atnc_giraspreregis_programas_girapreregis_id_foreign');
        });
        //Quitamos foráneas importadas
        Schema::table('giraspreregis', function(Blueprint $table){
            $table->dropForeign(['evento_id']);
            $table->dropForeign(['localidad_id']);
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('giraspreregis_evento_id_foreign');
            $table->dropIndex('giraspreregis_localidad_id_foreign');
            $table->dropIndex('giraspreregis_municipio_id_foreign');
            $table->dropIndex('giraspreregis_usuario_id_foreign');
        });
        //Renombramos
        Schema::rename('giraspreregis', 'atnc_giraspreregis');
        //Volvemos a amarrar las foráneas importadas
        Schema::table ('atnc_giraspreregis', function(Blueprint $table){
            $table->foreign('evento_id')->references('id')->on('even_eventos');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
        // Amarramos con las foráneas exportadas
        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->foreign('girapreregis_id')->references('id')->on('atnc_giraspreregis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Quitamos foráneas exportadas
        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->dropForeign(['girapreregis_id']);
            $table->dropIndex('atnc_giraspreregis_programas_girapreregis_id_foreign');
        });
        //Quitamos foráneas importadas
        Schema::table('atnc_giraspreregis', function(Blueprint $table){
            $table->dropForeign(['evento_id']);
            $table->dropForeign(['localidad_id']);
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('atnc_giraspreregis_evento_id_foreign');
            $table->dropIndex('atnc_giraspreregis_localidad_id_foreign');
            $table->dropIndex('atnc_giraspreregis_municipio_id_foreign');
            $table->dropIndex('atnc_giraspreregis_usuario_id_foreign');
        });
        
        //Renombramos
        Schema::rename('atnc_giraspreregis', 'giraspreregis');
        //Volvemos a amarrar las foráneas importadas
        Schema::table ('giraspreregis', function(Blueprint $table){
            $table->foreign('evento_id')->references('id')->on('even_eventos');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
        // Amarramos con las foráneas exportadas
        Schema::table('atnc_giraspreregis_programas', function(Blueprint $table){
            $table->foreign('girapreregis_id')->references('id')->on('giraspreregis');
        });
    }
}
