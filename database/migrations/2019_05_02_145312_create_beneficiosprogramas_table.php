<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Para ingresar datos de las nuevas áreas 2019
  ALCANCE: Para todos los sistemas
*/

class CreateBeneficiosprogramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('beneficiosprogramas', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('programa_id');
      $table->string('nombre');

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('beneficiosprogramas');
  }
}
