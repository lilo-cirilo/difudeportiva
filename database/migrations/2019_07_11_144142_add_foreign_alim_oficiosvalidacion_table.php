<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para ingresar los datos de oficios validados por alimentarios
    ALCANCE: Alimentarios
*/

class AddForeignAlimOficiosvalidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_oficiosvalidacion', function (Blueprint $table) {
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('programa_id')->references('id')->on('programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_oficiosvalidacion', function (Blueprint $table) {
            $table->dropForeign(['ejercicio_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_oficiosvalidacion_ejercicio_id_foreign');
            $table->dropIndex('alim_oficiosvalidacion_programa_id_foreign');
            $table->dropIndex('alim_oficiosvalidacion_usuario_id_foreign');
        });
    }
}
