<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se elimina tabla cat_instituciones
  ALCANCE: Base Principal
*/

class DeleteCatInstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_instituciones_responsables', function($table)
        {
            $table->dropForeign(['institucion_id']);
            $table->dropIndex('sdoc_instituciones_responsables_institucion_id_foreign');
            //$table->dropColumn('institucion_id');
        });

        Schema::table('cyc_convenios', function($table)
        {
            $table->dropForeign(['institucion_id']);
            $table->dropIndex('cyc_convenios_institucion_id_foreign');
            //$table->dropColumn('institucion_id');
        });

        Schema::dropIfExists('cat_instituciones');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::create('cat_instituciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('sdoc_instituciones_responsables', function (Blueprint $table) {
            //$table->unsignedInteger('institucion_id')->after('id');
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            });

        Schema::table('cyc_convenios', function (Blueprint $table) {
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            });
    }
}
