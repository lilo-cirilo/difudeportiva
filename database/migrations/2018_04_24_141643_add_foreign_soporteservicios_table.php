<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSoporteserviciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soporteservicios', function (Blueprint $table) {
            $table->foreign('tiposervicio_id')->references('id')->on('tipossoporteservicios')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soporteservicios', function (Blueprint $table) {
            $table->dropForeign(["tiposervicio_id"]);
        });
    }
}
