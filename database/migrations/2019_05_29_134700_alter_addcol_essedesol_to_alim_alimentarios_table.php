<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Bryan.
  MOTIVO: Para indicar qué cocina es sedesol
  ALCANCE: Alimentarios.
*/

class AlterAddcolEssedesolToAlimAlimentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_alimentarios', function (Blueprint $table) {
            $table->tinyInteger('essedesol')->unsigned()->default(0)->after('programa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_alimentarios', function (Blueprint $table) {
            $table->dropColumn('essedesol');
        });
    }
}
