<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSolicitudanexosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudanexos', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudanexos', function (Blueprint $table) {
            $table->dropForeign(["solicitud_id"]);
           
        });    
    }
}
