<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeGruposdiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vale_gruposdias', function (Blueprint $table) {
            $table->foreign('grupodisponible_id')->references('id')->on('vale_gruposdisponibles')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vale_gruposdias', function (Blueprint $table) {
            $table->dropForeign(["grupodisponible_id"]);
        });
    }
}
