<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel.
    MOTIVO: Para registrar observaciones
    ALCANCE: Atención ciudadana
*/

class AlterAddcolObservacionesToAtncGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('atnc_giraspreregis', function (Blueprint $table) {
            $table->string('observaciones')->after('fecha_registro')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atnc_giraspreregis', function (Blueprint $table) {
            $table->dropColumn('observaciones');
        });
    }
}
