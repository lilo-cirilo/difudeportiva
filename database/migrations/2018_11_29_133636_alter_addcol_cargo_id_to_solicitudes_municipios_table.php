<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Captura de datos
  ALCANCE: Atención Ciudadana
*/
class AlterAddcolCargoIdToSolicitudesMunicipiosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('solicitudes_municipios', function (Blueprint $table) {
      $table->unsignedInteger('cargo_id')->nullable()->after('persona_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('solicitudes_municipios', function (Blueprint $table) {
      $table->dropColumn('cargo_id');
    });
  }
}
