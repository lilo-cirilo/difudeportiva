<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos la observacion adicional al momento de generar la orden de compra
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumObservacionToRecmOrdenescompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->text('observacion')->nullable()->after('folio_interno');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra',function(Blueprint $table){
            $table->dropColumn('observacion');
        });
    }
}
