<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para clasificar los productos
    ALCANCE: General
*/

class AddForeignModulosProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modulos_productos', function (Blueprint $table) {
            $table->foreign('producto_id')->references('id')->on('recm_cat_productos');
            $table->foreign('modulo_id')->references('id')->on('cat_modulos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modulos_productos', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['modulo_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('modulos_productos_producto_id_foreign');
						$table->dropIndex('modulos_productos_modulo_id_foreign');
						$table->dropIndex('modulos_productos_usuario_id_foreign');
        });
    }
}
