<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListasDispersionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listas_dispersiones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_listado');
            $table->integer('bimestre');
            $table->float('importe');
            $table->datetime('fecha');
            $table->string ('status');
            $table->integer('usuario_id')->unsigned(); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listas_dispersiones');
    }
}
