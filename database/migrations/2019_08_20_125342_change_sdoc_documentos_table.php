<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Quitando columna contenido
    ALCANCE: SICODOC
*/

class ChangeSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('sdoc_documentos', function (Blueprint $table) {
				$table->text('observaciones')->nullable()->change();
				$table->dropColumn('contenido');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('sdoc_documentos', function (Blueprint $table) {
				$table->string('observaciones',1500)->nullable()->change();
				$table->string('contenido')->nullable();
			});
    }
}
