<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_documentos
  ALCANCE: Para el sistema SICODOC
*/


class AddForeignSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('remitente_id')->references('id')->on('sdoc_responsables');
            $table->foreign('destinatario_id')->references('id')->on('sdoc_responsables');
            $table->foreign('tipodocumento_id')->references('id')->on('sdoc_cat_tiposdocumento');
            $table->foreign('estatus_id')->references('id')->on('sdoc_cat_estatus');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_documentos', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['remitente_id']);
            $table->dropForeign(['destinatario_id']);
            $table->dropForeign(['tipodocumento_id']);
            $table->dropForeign(['estatus_id']);

            $table->dropIndex('sdoc_documentos_usuario_id_foreign');
            $table->dropIndex('sdoc_documentos_remitente_id_foreign');
            $table->dropIndex('sdoc_documentos_destinatario_id_foreign');
            $table->dropIndex('sdoc_documentos_tipodocumento_id_foreign');
            $table->dropIndex('sdoc_documentos_estatus_id_foreign');
        });
    }
}
