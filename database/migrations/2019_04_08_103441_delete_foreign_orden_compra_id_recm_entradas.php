<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se elimina la relacion, se agrega columna para relacionar con la requisicion
  ALCANCE: Para el sistema de recursos materiales
*/

class DeleteForeignOrdenCompraIdRecmEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->dropForeign(['orden_compra_id']);
            $table->dropIndex('recm_entradas_orden_compra_id_foreign');
            // $table->dropColumn('orden_compra_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas', function (Blueprint $table) {
            $table->foreign('orden_compra_id')->references('id')->on('recm_ordenescompra');
        });
    }
}
