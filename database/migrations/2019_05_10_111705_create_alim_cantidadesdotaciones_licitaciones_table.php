<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de licitaciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimCantidadesDotacionesLicitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_cantidadesdotaciones_licitaciones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('licitacion_id');
            $table->unsignedInteger('dotacion_id');
            $table->tinyInteger('cantidad')->unsigned();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['licitacion_id','dotacion_id'],'alim_requis_licit_liciid_dotaid_unique'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_cantidadesdotaciones_licitaciones');
    }
}
