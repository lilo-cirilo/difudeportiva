<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Mantener el saldo disponible en cada area
    ALCANCE: Fondo Rotatorio
*/

class AddColumnToSiafFondorotatorioareasTable extends Migration
{
    /**
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_fondorotatorioareas', function (Blueprint $table) {
            $table->double('saldo_disponible')->after('techo_presupuestal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_fondorotatorioareas', function (Blueprint $table) {
            $table->dropColumn('saldo_disponible');
        });
    }
}
