<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de riesgos de temas sísmicos, inundaciones, vientos fuertes, heladas, deslizamientos e incendios forestales
  ALCANCE: Riesgos
*/

class CreateRiesCorredoressisMunicipiosriesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('ries_corredoressis_municipiosries', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('corredor_id');
      $table->unsignedInteger('municipio_riesgo_id');

      $table->unsignedInteger('usuarios_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('ries_corredoressis_municipiosries');
  }
}
