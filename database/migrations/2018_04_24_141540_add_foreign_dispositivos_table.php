<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDispositivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dispositivos', function (Blueprint $table) {
            $table->foreign('marca_id')->references('id')->on('cat_marcasequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('modelo_id')->references('id')->on('cat_modelosequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dispositivos', function (Blueprint $table) {
            $table->dropForeign(["marca_id"]);
            $table->dropForeign(["modelo_id"]);
        });
    }
}
