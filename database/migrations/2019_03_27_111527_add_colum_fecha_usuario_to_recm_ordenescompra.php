<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se guarda la fecha de la captura enviada por el usuario
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumFechaUsuarioToRecmOrdenescompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->date('fecha_captura')->after('subtotal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra',function(Blueprint $table){
            $table->dropColumn('fecha_captura');
        });
    }
}
