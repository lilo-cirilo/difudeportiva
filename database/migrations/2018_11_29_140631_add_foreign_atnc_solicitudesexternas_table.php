<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Captura de datos
  ALCANCE: Atención Ciudadana
*/

class AddForeignAtncSolicitudesexternasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('atnc_solicitudesexternas', function (Blueprint $table) {
      $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('dependencia_id')->references('id')->on('cat_dependencias')->onUpdate('CASCADE')->onDelete('CASCADE');        
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('atnc_solicitudesexternas', function (Blueprint $table) {
      $table->dropForeign(["solicitud_id"]);
      $table->dropForeign(["dependencia_id"]);

      $table->dropIndex('atnc_solicitudesexternas_solicitud_id_foreign');
      $table->dropIndex('atnc_solicitudesexternas_dependencia_id_foreign');
    });
  }
}
