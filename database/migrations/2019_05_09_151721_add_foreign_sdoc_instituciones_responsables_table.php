<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega foreign keys para tabla sdoc_instituciones_responsables
  ALCANCE: Para el sistema SICODOC
*/


class AddForeignSdocInstitucionesResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_instituciones_responsables', function (Blueprint $table) {
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_instituciones_responsables', function (Blueprint $table) {
            $table->dropForeign(['institucion_id']);
            $table->dropForeign(['usuario_id']);      
            
            $table->dropIndex('sdoc_instituciones_responsables_institucion_id_foreign');    
            $table->dropIndex('sdoc_instituciones_responsables_usuario_id_foreign');    
        });
    }
}
