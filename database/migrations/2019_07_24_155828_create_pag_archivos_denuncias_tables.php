<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagArchivosDenunciasTables extends Migration
{
    /**
     * PETICIÓN: Beto
     * MOTIVO: Guardar los archivos de una denuncia
     * ALCANCE: Pagina DIF
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pag_archivos_denuncias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('tamanio');
            $table->string('url');
            $table->string('tipo');
            $table->unsignedInteger('buzon_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pag_archivos_denuncias');
    }
}