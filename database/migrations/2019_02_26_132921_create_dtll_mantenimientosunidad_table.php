<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllMantenimientosunidadTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dtll_mantenimientosunidad', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('unidad_id');
      $table->decimal('costo',7,2);
      $table->string('descripcion', 100);
      $table->unsignedInteger('mantenimiento_id');
      $table->date('fecha_mantenimiento');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dtll_mantenimientosunidad');
  }
}
