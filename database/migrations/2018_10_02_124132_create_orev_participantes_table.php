<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrevParticipantesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('orev_participantes', function (Blueprint $table) {
      $table->increments('id');
      
      $table->string('nombre');
      $table->string('primerApellido');
      $table->string('segundoApellido');
      $table->enum('genero',['H','M']);
      $table->date('fecha_nacimiento');
      $table->string('telefono')->nullable();
      $table->string('curp')->unique();
      $table->string('correo')->nullable();

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('orev_participantes');
  }
}
