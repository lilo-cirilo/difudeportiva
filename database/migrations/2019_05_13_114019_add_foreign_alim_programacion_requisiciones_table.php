<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimProgramacionRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('alim_requisiciones');
            $table->foreign('programacion_id')->references('id')->on('alim_programacion');
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['programacion_id']);
            $table->dropForeign(['estado_programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_programacion_requisiciones_requisicion_id_foreign');
            $table->dropIndex('alim_programacion_requisiciones_programacion_id_foreign');
            $table->dropIndex('alim_programacion_requisiciones_estado_programa_id_foreign');
            $table->dropIndex('alim_programacion_requisiciones_usuario_id_foreign');
        });
    }
}
