<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class AddForeignAlimChoferespreveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_choferesproveedor', function (Blueprint $table) {
            $table->foreign('modulo_proveedor_id')->references('id')->on('modulos_proveedores');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_choferesproveedor', function (Blueprint $table) {
            $table->dropForeign(['modulo_proveedor_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_choferesproveedor_usuario_id_foreign');
        });
    }
}
