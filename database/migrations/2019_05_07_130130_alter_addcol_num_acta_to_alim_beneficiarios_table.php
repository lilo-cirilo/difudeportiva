<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para ingresar datos de número de actas de los beneficiarios de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class AlterAddcolNumActaToAlimBeneficiariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_beneficiarios', function (Blueprint $table) {
      $table->string('num_acta_nac',20)->nullable()->after('persona_programa_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_beneficiarios', function (Blueprint $table) {
      $table->dropColumn('num_acta_nac');
    });
  }
}
