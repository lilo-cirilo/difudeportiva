<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSiafViaticoestatalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_viaticoestatal', function (Blueprint $table) {
            $table->foreign('viatico_id')->references('id')->on('siaf_comisionesoficiales');
            $table->foreign('origenmunicipio_id')->references('id')->on('cat_municipios');
            $table->foreign('destinomunicipio_id')->references('id')->on('cat_municipios');
            $table->foreign('destinolocalidad_id')->references('id')->on('cat_localidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_viaticoestatal', function (Blueprint $table) {
            $table->dropForeign(['viatico_id']);
            // $table->dropIndex('siaf_viaticoestatal_viatico_id_foreign');
            $table->dropForeign(['origenmunicipio_id']);
            $table->dropIndex('siaf_viaticoestatal_origenmunicipio_id_foreign');  
            $table->dropForeign(['destinomunicipio_id']);
            $table->dropIndex('siaf_viaticoestatal_destinomunicipio_id_foreign');  
            $table->dropForeign(['destinolocalidad_id']);
            $table->dropIndex('siaf_viaticoestatal_destinolocalidad_id_foreign');
        });
    }
}
