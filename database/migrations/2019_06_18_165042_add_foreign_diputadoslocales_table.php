<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los diputados locales.
    ALCANCE: General
*/

class AddForeignDiputadoslocalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diputadoslocales', function (Blueprint $table) {
            $table->foreign('cargo_id')->references('id')->on('cat_cargos');
            $table->foreign('partidopolitico_id')->references('id')->on('cat_partidospoliticos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diputadoslocales', function (Blueprint $table) {
            $table->dropForeign(['cargo_id']);
            $table->dropForeign(['partidopolitico_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('diputadoslocales_cargo_id_foreign');
            $table->dropIndex('diputadoslocales_partidopolitico_id_foreign');
            $table->dropIndex('diputadoslocales_usuario_id_foreign');
        });
    }
}
