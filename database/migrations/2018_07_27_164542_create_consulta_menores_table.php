<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaMenoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta_menores', function (Blueprint $table) {
            $table->integer('consulta_id')->unsigned();
            $table->tinyInteger('ninio_sano');
            $table->tinyInteger('cancer')->nullable();
            $table->string('tipo_edi',50)->nullable();
            $table->string('res_edi')->nullable();
            $table->string('res_battelle')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta_menores');
    }
}
