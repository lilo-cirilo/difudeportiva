<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para filtrar los proveedores por módulos.
    ALCANCE: General
*/

class AddForeignModulosProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('modulos_proveedores', function (Blueprint $table) {
            $table->foreign('modulo_id')->references('id')->on('cat_modulos');
            $table->foreign('proveedor_id')->references('id')->on('recm_cat_proveedors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modulos_proveedores', function (Blueprint $table) {
            $table->dropForeign(['modulo_id']);
            $table->dropForeign(['proveedor_id']);
            $table->dropIndex('modulos_proveedores_modulo_id_foreign');
            $table->dropIndex('modulos_proveedores_proveedor_id_foreign');
        });
    }
}
