<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmHistorialRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_historial_requisicions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('usuario_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_historial_requisicions');
    }
}
