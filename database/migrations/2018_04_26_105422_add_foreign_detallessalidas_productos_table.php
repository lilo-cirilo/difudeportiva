<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDetallessalidasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detallessalidas_productos', function (Blueprint $table) {
            $table->foreign('salidas_producto_id')->references('id')->on('salidas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('areas_producto_id')->references('id')->on('areas_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detallessalidas_productos', function (Blueprint $table) {
            $table->dropForeign(["salidas_producto_id"]);
            $table->dropForeign(["areas_producto_id"]);
        });
    }
}
