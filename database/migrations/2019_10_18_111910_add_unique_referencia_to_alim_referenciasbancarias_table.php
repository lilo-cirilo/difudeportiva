<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de tutores de una persona que recibe un beneficio
    ALCANCE: General
*/

class AddUniqueReferenciaToAlimReferenciasbancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_referenciasbancarias', function (Blueprint $table) {
            $table->unique(['referencia']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_referenciasbancarias', function (Blueprint $table) {
           $table->dropUnique(['referencia']);
        });
    }
}
