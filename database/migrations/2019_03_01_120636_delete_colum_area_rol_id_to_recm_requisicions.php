<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se elimina columna 
  ALCANCE: ya no es necesario en el sistema de recursos materiales
*/

class DeleteColumAreaRolIdToRecmRequisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->dropForeign(['area_rol_id']);
            $table->dropColumn(['area_rol_id']);
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->integer('area_rol_id')->unsigned()->after('usuario_id');
            // $table->foreign('area_rol_id')->references('id')->on('recm_area_rols');
        });
        
         $idrol = DB::table('recm_area_rols')->first();
            if ($idrol)
              DB::table('recm_requisicions')->update(['area_rol_id'=>$idrol->id]);

        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->foreign('area_rol_id')->references('id')->on('recm_area_rols');
        });
    }
}
