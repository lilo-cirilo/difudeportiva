<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSdocDetallesdocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_detallesdocumentos', function (Blueprint $table) {
            $table->foreign('documento_id')->references('id')->on('sdoc_documentos');
            $table->foreign('institucionresponsable_id')->references('id')->on('sdoc_responsables');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_detallesdocumentos', function (Blueprint $table) {
            $table->dropForeign(['documento_id']);
            $table->dropForeign(['institucionresponsable_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('sdoc_detallesdocumentos_documento_id_foreign');
            $table->dropIndex('sdoc_detallesdocumentos_institucionresponsable_id_foreign');
            $table->dropIndex('sdoc_detallesdocumentos_usuario_id_foreign');
        });
    }
}
