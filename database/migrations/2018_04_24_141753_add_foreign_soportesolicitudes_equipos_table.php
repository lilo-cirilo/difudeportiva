<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSoportesolicitudesEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soportesolicitudes_equipos', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('soportesolicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('inventario_id')->references('id')->on('inventario')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('tecnico_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soportesolicitudes_equipos', function (Blueprint $table) {
            $table->dropForeign(["solicitud_id"]);
            $table->dropForeign(["inventario_id"]);
            $table->dropForeign(["tecnico_id"]);
        });
    }
}
