<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmPresentacionProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_presentacion_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('precio')->unsigned();
            $table->unsignedInteger('presentacion_id');
            $table->unsignedInteger('producto_id');
            $table->unsignedInteger('partida_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_presentacion_productos');
    }
}
