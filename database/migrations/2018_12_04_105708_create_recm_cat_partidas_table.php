<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmCatPartidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_cat_partidas', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('tipo')->unsigned();
            $table->string('objetivo',5);
            $table->integer('capitulo')->unsigned();
            $table->integer('concep')->unsigned()->nullable();
            $table->integer('partida_generica')->unsigned();
            $table->integer('partida_especifica_c')->unsigned()->nullable();
            $table->string('idp',5)->nullable();
            $table->string('capitulo_d')->nullable();
            $table->string('concep_d')->nullable();
            $table->string('partida_generica_d')->nullable();
            $table->string('partida_especifica');
            $table->string('concepto');
            $table->unsignedInteger('usuario_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_cat_partidas');
    }
}
