<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSoportesolicitudesEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soportesolicitudes_equipos', function(Blueprint $table){
          $table->dropForeign(['inventario_id']);
          $table->dropColumn('inventario_id');
          $table->string('estado_equipo',100)->nullable()->after('tecnico_id');
          $table->string('observaciones')->nullable()->after('estado_equipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soportesolicitudes_equipos', function(Blueprint $table){
          $table->dropColumn('observaciones');
          $table->dropColumn('estado_equipo');
          $table->integer('inventario_id')->unsigned()->after('solicitud_id');
          $table->foreign('inventario_id')->references('id')->on('inventario')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
}
