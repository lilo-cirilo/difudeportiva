<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony.
  MOTIVO: Para relacionar lo beneficios con una persona.
  ALCANCE: Atención ciudadana y Apoyos funcionales.
*/

class CreateBeneficiosprogsolPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiosprogsol_personas', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('beneficiosprogramas_solicitud_id');
            $table->unsignedInteger('persona_id');
            $table->boolean('entregado');
            $table->string('observacion_entrega')->nullable();
            $table->boolean('evaluado');
            $table->string('observacion_evaluado')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiosprogsol_personas');
    }
}
