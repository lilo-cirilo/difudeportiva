<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class CreateSiafComisionesoficialesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('siaf_comisionesoficiales', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('pago_id')->unique();
      $table->unsignedInteger('origen_municipio_id');
      $table->unsignedInteger('destino_municipio_id')->nullable();
      $table->date('fecha_salida');
      $table->date('fecha_regreso');
      $table->unsignedInteger('localidad_id')->nullable();
      $table->string('localidad');
      $table->tinyInteger('gastos_alimentarios');
      $table->unsignedInteger('num_comidas');
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('siaf_comisionesoficiales');
  }
}
