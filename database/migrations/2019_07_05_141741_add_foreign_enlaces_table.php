<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para los enlaces que tendrán los directivos
    ALCANCE: General
*/

class AddForeignEnlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enlaces', function (Blueprint $table) {
            $table->foreign('empleado_id')->references('id')->on('empleados');
            $table->foreign('area_id')->references('id')->on('cat_areas');
            $table->foreign('tipoenlace_id')->references('id')->on('cat_tiposenlace');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enlaces', function (Blueprint $table) {
            $table->dropForeign(['empleado_id']);
            $table->dropForeign(['area_id']);
            $table->dropForeign(['tipoenlace_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('enlaces_area_id_foreign');
            $table->dropIndex('enlaces_empleado_id_foreign');
            $table->dropIndex('enlaces_tipoenlace_id_foreign');
            $table->dropIndex('enlaces_usuario_id_foreign');
        });
    }
}
