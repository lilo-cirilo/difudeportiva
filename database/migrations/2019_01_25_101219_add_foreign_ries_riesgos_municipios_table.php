<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Para trabajar con el sistema de riesgos de temas sísmicos, inundaciones, vientos fuertes, heladas, deslizamientos e incendios forestales
  ALCANCE: Riesgos
*/

class AddForeignRiesRiesgosMunicipiosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('ries_municipios_riesgos', function (Blueprint $table) {
      $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('riesgo_id')->references('id')->on('ries_cat_riesgos')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('ries_municipios_riesgos', function (Blueprint $table) {
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['riesgo_id']);
      
      $table->dropIndex('ries_municipios_riesgos_municipio_id_foreign');
      $table->dropIndex('ries_municipios_riesgos_riesgo_id_foreign');
    });
  }
}
