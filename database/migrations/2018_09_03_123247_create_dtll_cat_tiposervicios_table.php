<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllCatTiposerviciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dtll_cat_tiposervicios', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->timestamps();
        $table->softDeletes();
      });

      DB::table('dtll_cat_tiposervicios')->insert([
        ['nombre'=>'URBAN', 'created_at'=>NOW()],
        ['nombre'=>'TAXI', 'created_at'=>NOW()],
        ['nombre'=>'AMBOS', 'created_at'=>NOW()]
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_cat_tiposervicios');
    }
}
