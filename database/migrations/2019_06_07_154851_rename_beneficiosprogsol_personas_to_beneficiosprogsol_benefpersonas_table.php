<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony, Emanuel.
    MOTIVO: Para que se relaicione con la tabla beneficiosprogramas
    ALCANCE: Solicitudes
*/

class RenameBeneficiosprogsolPersonasToBeneficiosprogsolBenefpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Quitamos foráneas importadas
        Schema::table('beneficiosprogsol_personas', function(Blueprint $table){
           $table->dropForeign('benefprogsol_personas_benefprog_solicitud_id_foreign');
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["usuario_id"]);

            $table->dropIndex('benefprogsol_personas_benefprog_solicitud_id_foreign');
            $table->dropIndex('beneficiosprogsol_personas_persona_id_foreign');
            $table->dropIndex('beneficiosprogsol_personas_usuario_id_foreign');
        });
        //Renombramos
        Schema::rename('beneficiosprogsol_personas', 'beneficiosprogsol_benefpersonas');
        //Volvemos a amarrar las foráneas importadas
        Schema::table ('beneficiosprogsol_benefpersonas', function(Blueprint $table){
            $table->foreign('beneficiosprogramas_solicitud_id', 'benefprogsol_benefpersonas_benefprog_solicitud_id_foreign')->references('id')->on('beneficiosprogramas_solicitudes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Quitamos foráneas importadas
        Schema::table('beneficiosprogsol_benefpersonas', function(Blueprint $table){
            $table->dropForeign('benefprogsol_benefpersonas_benefprog_solicitud_id_foreign');
            $table->dropForeign(["persona_id"]);
            $table->dropForeign(["usuario_id"]);

            $table->dropIndex('benefprogsol_benefpersonas_benefprog_solicitud_id_foreign');
            $table->dropIndex('beneficiosprogsol_benefpersonas_persona_id_foreign');
            $table->dropIndex('beneficiosprogsol_benefpersonas_usuario_id_foreign');
        });
        //Renombramos
        Schema::rename('beneficiosprogsol_benefpersonas', 'beneficiosprogsol_personas');
        //Volvemos a amarrar las foráneas importadas
        Schema::table ('beneficiosprogsol_personas', function(Blueprint $table){
            $table->foreign('beneficiosprogramas_solicitud_id', 'benefprogsol_personas_benefprog_solicitud_id_foreign')->references('id')->on('beneficiosprogramas_solicitudes');
            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }
}
