<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO:
  ALCANCE:
*/

class DeletecolumnCredImpresion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Table('cred_impresion', function(Blueprint $table){
            $table->dropForeign(['personas_tipos_id']);
            $table->dropColumn(['personas_tipos_id']);
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Table('cred_impresion', function(Blueprint $table){
            $table->integer('personas_tipos_id')->unsigned()->after('id');
            $table->foreign('personas_tipos_id')->references('id')->on('personas_tipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }
}
