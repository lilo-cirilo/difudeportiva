<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para guardar el empleado responsable del area cuando se genero la requisicion
    ALCANCE: recmat
*/

class AddColumnEmpleadoResponsableToRecmRecquisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('recm_requisicions', function (Blueprint $table) {
				$table->unsignedInteger('areas_responsable_id')->nullable()->after('area_id');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('recm_requisicions', function (Blueprint $table) {
				$table->dropColumn('areas_responsable_id');
			});
    }
}
