<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder indicar si una relación laboral es oficial o no de acuerdo al cambio de organigrama en 2018-2019
  ALCANCE: Para todos los sitemas
*/

class AlterAddcolOficialToCatTiposempleadosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    // Creamos un campo nullable
    Schema::table('cat_tiposempleados', function (Blueprint $table) {
      $table->tinyInteger('oficial')->after('tipo');
    });

    //Actualizamos el campo
    DB::table('cat_tiposempleados')->update(['oficial'=>1]);
    
    //Insertamos los nuevos datos que necesitamos
    DB::table('cat_tiposempleados')->insert([
      ['tipo'=>'CONTRATO CONFIANZA ADICIONAL', 'oficial'=>0, 'created_at'=>NOW()],
      ['tipo'=>'CONTRATO CONFIANZA UNIDADES MOVILES', 'oficial'=>0, 'created_at'=>NOW()],
      ['tipo'=>'NOMBRAMIENTO CONFIANZA UNIDADES MOVILES', 'oficial'=>0, 'created_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cat_tiposempleados', function (Blueprint $table) {
      $table->dropColumn('oficial');
    });
  }
}
