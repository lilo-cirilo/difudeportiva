<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDtllRegistrogasolinaTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_registrogasolina', function (Blueprint $table) {
      $table->foreign('tipocarga_id')->references('id')->on('dtll_cat_tiposcarga')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('controlruta_id')->references('id')->on('controlrutas')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_registrogasolina', function (Blueprint $table) {
      $table->dropForeign(['controlruta_id']);
      $table->dropForeign(['tipocarga_id']);
      $table->dropIndex('dtll_registrogasolina_controlruta_id_foreign');
      $table->dropIndex('dtll_registrogasolina_tipocarga_id_foreign');
    });
  }
}
