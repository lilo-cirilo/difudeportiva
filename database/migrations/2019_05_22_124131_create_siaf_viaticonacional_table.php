<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiafViaticonacionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_viaticonacional', function (Blueprint $table) {
            $table->unsignedInteger('viatico_id')->unique();
            $table->unsignedInteger('origenentidad_id');
            $table->unsignedInteger('destinoentidad_id');
            $table->unsignedInteger('origenregion_id');
            $table->string('destinociudad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_viaticonacional');
    }
}
