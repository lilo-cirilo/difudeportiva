<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de oficios de aturización de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimOficiosautorizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->increments('id');

            $table->tinyInteger('num_oficio')->unsigned();
            $table->tinyInteger('num_referencia')->unsigned()->nullable();
            $table->unsignedInteger('tipo_oficio_id');
            $table->datetime('fecha_emision');
            $table->unsignedInteger('region_id');
            $table->unsignedInteger('dotacion_id');
            $table->decimal('costo_unitario')->unsigned();
            $table->tinyInteger('num_dotaciones')->unsigned();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_oficiosautorizacion');
    }
}
