<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('programas_solicitud_id')->unsigned();
            $table->integer('statusproceso_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->integer('motivo_id')->unsigned()->nullable();
            $table->string('observacion')->nullable();
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_solicitudes');
    }
}
