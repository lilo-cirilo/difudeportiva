<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class AddForeignToSiafClavespresupuestalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('siaf_clavespresupuestales', function (Blueprint $table) {
            $table->foreign('unidadejecutora_id')->references('id')->on('siaf_clavespresupuestales');
            $table->foreign('area_id')->references('id')->on('cat_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_clavespresupuestales', function (Blueprint $table) {
            $table->dropForeign(['unidadejecutora_id']);
            $table->dropIndex('siaf_clavespresupuestales_unidadejecutora_id_foreign');  
            $table->dropForeign(['area_id']);
            $table->dropIndex('siaf_clavespresupuestales_area_id_foreign');  
        });
    }
}
