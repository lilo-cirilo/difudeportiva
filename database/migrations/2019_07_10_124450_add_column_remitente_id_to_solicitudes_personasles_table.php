<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Migue.
    MOTIVO: Para quitar la relación con la tabla personas y ahora ingresar los datos de remitente en la tabla atnc_remitentes
    ALCANCE: Atención ciudadana
*/

class AddColumnRemitenteIdToSolicitudesPersonaslesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Agregamos el campo remitente_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->unsignedInteger('remitente_id')->after('persona_id');
        });

        //Volcamos la información relacionada con persona_id a la tabla atnc_remitentes
        $solicitudes = DB::table('solicitudes_personales AS s')
                                    ->join('personas AS p','p.id','=','s.persona_id')
                                    ->leftJoin('cat_localidades AS l','l.id','=','p.localidad_id')
                                    ->leftJoin('cat_municipios AS m','m.id','=','p.municipio_id')
                                    ->select ('s.id','p.nombre', 'p.primer_apellido', 'p.segundo_apellido','p.calle','p.numero_exterior','p.colonia','l.nombre AS localidad','m.nombre AS municipio','p.email','p.numero_celular')
                                    ->get();
        
        for ($i=0; $i<count($solicitudes); $i++){
            $domicilio = $solicitudes[$i]->calle;
            $domicilio = $domicilio . " " . $solicitudes[$i]->numero_exterior;
            $domicilio = trim($domicilio) . ($domicilio && $solicitudes[$i]->colonia?", ":"") . $solicitudes[$i]->colonia;
            $domicilio = $domicilio . (($domicilio && $solicitudes[$i]->localidad)?", ":"") . $solicitudes[$i]->localidad;
            $domicilio = $domicilio . (($domicilio && $solicitudes[$i]->municipio)?", ":"") . $solicitudes[$i]->municipio;

            $remitente_id = DB::table('atnc_remitentes')->insertGetId([
                        'nombre'=>$solicitudes[$i]->nombre,
                        'primer_apellido'=>$solicitudes[$i]->primer_apellido,
                        'segundo_apellido'=>$solicitudes[$i]->segundo_apellido,
                        'domicilio'=>$domicilio,
                        'email'=>$solicitudes[$i]->email,
                        'telefono'=>$solicitudes[$i]->numero_celular,
                        'usuario_id'=>91,
                        'created_at'=>NOW(),
                        'updated_at'=>NOW()
                    ]);
            
            DB::table('solicitudes_personales')
                    ->where('id',$solicitudes[$i]->id)
                    ->update(['remitente_id'=>$remitente_id]);
            
        }

        // Creamos la relación remitente_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->foreign('remitente_id')->references('id')->on('atnc_remitentes');
        });

        //Eliminamos la relación con persona_id y cargo_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->dropForeign(['persona_id']);
            $table->dropIndex('solicitudes_personales_persona_id_foreign');
        });
        //Eliminamos las columnas persona_id y cargo_id pues ya no se usarán más
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->dropColumn('persona_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Creamos las columnas persona_id y cargo_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->unsignedInteger('persona_id')->after('solicitud_id');
        });

        $registro = DB::table('personas')->first();
        if ($registro)
            DB::table('solicitudes_personales')->update(['persona_id'=>$registro->id]);

        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
        });

        //Quitamos la relación remitente_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->dropForeign(['remitente_id']);
            $table->dropIndex('solicitudes_personales_remitente_id_foreign');
        });
        //Quitamos la columna remitente_id
        Schema::table('solicitudes_personales', function (Blueprint $table) {
            $table->dropColumn('remitente_id');
        });
    }
}
