<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos que requiere la api de google calendar
  ALCANCE: Para el sistema de eventos
*/

class AddForeignEvenCalendariosPersonasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_calendarios_personas', function (Blueprint $table) {
      $table->foreign('calendario_id')->references('id')->on('even_cat_tiposevento');
      $table->foreign('persona_id')->references('id')->on('personas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_calendarios_personas', function (Blueprint $table) {
      $table->dropForeign(['calendario_id']);
      $table->dropForeign(['persona_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('even_calendarios_personas_calendario_id_foreign');
      $table->dropIndex('even_calendarios_personas_persona_id_foreign');
      $table->dropIndex('even_calendarios_personas_usuario_id_foreign');
    });
  }
}
