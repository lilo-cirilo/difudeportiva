<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: 
    ALCANCE: fondorotatorio
*/

class AddForeignSiafComprobacionesEstados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobaciones_estados', function (Blueprint $table) {
            $table->foreign('comprobacion_id')->references('id')->on('siaf_comprobaciones');
            $table->foreign('pago_id')->references('id')->on('siaf_pagos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobaciones_estados', function (Blueprint $table) {
            $table->dropForeign(['comprobacion_id']);
            $table->dropForeign(['pago_id']);

            $table->dropIndex('siaf_comprobaciones_estados_comprobacion_id_foreign');
            $table->dropIndex('siaf_comprobaciones_estados_pago_id_foreign');
        });
    }
}
