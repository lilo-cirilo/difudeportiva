<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar datos referentes a bienestar
  ALCANCE: Bienestar
*/

class AddForeignBienRefrendosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->foreign('anio_programa_id')->references('id')->on('anios_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('estado_programa_id')->references('id')->on('cat_estados_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('bien_refrendos', function (Blueprint $table) {
      $table->dropForeign(['anio_programa_id']);
      $table->dropForeign(['estado_programa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('bien_refrendos_anio_programa_id_foreign');
      $table->dropIndex('bien_refrendos_estado_programa_id_foreign');
      $table->dropIndex('bien_refrendos_usuario_id_foreign');
    });
  }
}
