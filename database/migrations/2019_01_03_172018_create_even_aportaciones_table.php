<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenAportacionesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_aportaciones', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('fichainfo_id');
      $table->unsignedInteger('dependencia_id');
      $table->unsignedInteger('producto_id');
      $table->smallInteger('cantidad');
      $table->decimal('monto',11,2);

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_aportaciones');
  }
}
