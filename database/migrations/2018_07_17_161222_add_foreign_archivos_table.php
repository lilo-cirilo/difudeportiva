<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignArchivosTable extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->table('archivos', function (Blueprint $table) {
                    $table->foreign('tablabd_id')->references('id')->on('cat_tablasbd')->onUpdate('CASCADE')->onDelete('CASCADE');
                    $table->foreign('modulo_id')->references('id')->on('cat_modulos')->onUpdate('CASCADE')->onDelete('CASCADE');
                });
            }
        }
        else
        {
            Schema::table('archivos', function (Blueprint $table) {
                $table->foreign('tablabd_id')->references('id')->on('cat_tablasbd')->onUpdate('CASCADE')->onDelete('CASCADE');
                $table->foreign('modulo_id')->references('id')->on('cat_modulos')->onUpdate('CASCADE')->onDelete('CASCADE');
            });
        }
    }

    function up2($table)
    {
        $table->foreign('tablabd_id')->references('id')->on('cat_tablasbd')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('modulo_id')->references('id')->on('cat_modulos')->onUpdate('CASCADE')->onDelete('CASCADE');
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        if(env('DB_ISADMIN'))
        {
            $conexiones = ['mysql_maai', 'mysql_server_real', 'mysql_server_test', 'mysql_server_demo'];

            for($i = 0; $i < count($conexiones); $i++)
            {
                Schema::connection($conexiones[$i])->table('archivos', function(Blueprint $table) {
                    $table->dropForeign(['tablabd_id']);
                    $table->dropForeign(['modulo_id']);
                });
            }
        }
        else
        {
            Schema::table('archivos', function(Blueprint $table) {
                $table->dropForeign(['tablabd_id']);
                $table->dropForeign(['modulo_id']);
            });
        }
    }

    public function down2 ($table)
    {
        $table->dropForeign(['tablabd_id']);
        $table->dropForeign(['modulo_id']);
    }
}
