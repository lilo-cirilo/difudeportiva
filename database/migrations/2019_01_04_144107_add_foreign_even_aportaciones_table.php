<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenAportacionesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_aportaciones', function (Blueprint $table) {
      $table->foreign('fichainfo_id')->references('id')->on('even_fichasinfo')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('dependencia_id')->references('id')->on('cat_dependencias')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('producto_id')->references('id')->on('even_cat_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_aportaciones', function (Blueprint $table) {
      $table->dropForeign(['fichainfo_id']);
      $table->dropForeign(['dependencia_id']);
      $table->dropForeign(['producto_id']);
      $table->dropForeign(['usuario_id']);

      $table->dropIndex('even_aportaciones_fichainfo_id_foreign');
      $table->dropIndex('even_aportaciones_dependencia_id_foreign');
      $table->dropIndex('even_aportaciones_producto_id_foreign');
      $table->dropIndex('even_aportaciones_usuario_id_foreign');
    });
  }
}
