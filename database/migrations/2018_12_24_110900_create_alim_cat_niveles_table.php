<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimCatNivelesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('alim_cat_niveles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->timestamps();
      $table->softDeletes();
    });

    DB::table('alim_cat_niveles')->insert([
      ['nombre' => 'NO DEFINIDO', 'created_at'=>NOW(), 'updated_at'=>NOW()],
      ['nombre' => 'PREESCOLAR', 'created_at'=>NOW(), 'updated_at'=>NOW()],
      ['nombre' => 'PRIMARIA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('alim_cat_niveles');
  }
}
