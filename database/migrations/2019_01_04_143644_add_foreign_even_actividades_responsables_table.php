<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AddForeignEvenActividadesResponsablesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('even_actividades_responsables', function (Blueprint $table) {
      $table->foreign('actividad_id')->references('id')->on('even_actividades')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('responsable_id')->references('id')->on('even_responsables')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_actividades_responsables', function (Blueprint $table) {
      $table->dropForeign(['actividad_id']);
      $table->dropForeign(['responsable_id']);

      $table->dropIndex('even_ar_activ_resp_delat');  //Es el index UNIQUE, y el index even_actividades_responsables_actividad_id_foreign no se crea, no se porqué
      $table->dropIndex('even_actividades_responsables_responsable_id_foreign');
    });
  }
}
