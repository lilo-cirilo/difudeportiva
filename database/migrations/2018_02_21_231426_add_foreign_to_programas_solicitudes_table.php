<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToProgramasSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programas_solicitudes', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('solicitud_id')->references('id')->on('solicitudes')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programas_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['solicitud_id']);
        });
    }
}
