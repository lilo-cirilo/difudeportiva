<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('requisiciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('proveedor_id')->references('id')->on('proveedores')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('recibe_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('autoriza_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['proveedor_id']);
            $table->dropForeign(['recibe_id']);
            $table->dropForeign(['autoriza_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
