<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiPresentacionProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mopi_presentacion_productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('marca');
            $table->float('precio_estimado');
            $table->mediumText('descripcion');
            $table->unsignedInteger('producto_id');
            $table->unsignedInteger('presentacion_id');
            $table->unsignedInteger('categoria_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mopi_presentacion_productos');
    }
}
