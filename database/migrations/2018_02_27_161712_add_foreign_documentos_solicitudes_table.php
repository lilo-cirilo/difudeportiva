<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDocumentosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('documentos_solicitudes', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('solicitudes')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('documentos_persona_id')->references('id')->on('documentos_personas')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('documentos_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['documentos_persona_id']);
            $table->dropForeign(['solicitud_id']);
        });
    }
}
