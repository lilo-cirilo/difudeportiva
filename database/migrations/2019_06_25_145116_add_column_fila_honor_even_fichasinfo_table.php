<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para indicar si va a haber fila de honor o no
    ALCANCE: Eventos
*/

class AddColumnFilaHonorEvenFichasinfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('even_fichasinfo', function (Blueprint $table) {
            $table->boolean('fila_honor')->default(0)->after('director_participacion_id')->comments('Para indicar si va a haber fila de honor o no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('even_fichasinfo', function (Blueprint $table) {
            $table->dropColumn('fila_honor');
        });
    }
}
