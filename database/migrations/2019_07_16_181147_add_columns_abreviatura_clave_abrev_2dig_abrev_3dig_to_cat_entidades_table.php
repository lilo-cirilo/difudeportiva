<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Para tener los datos abreviados de las entidades
    ALCANCE: General
*/

class AddColumnsAbreviaturaClaveAbrev2digAbrev3digToCatEntidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_entidades', function (Blueprint $table) {
            $table->string('clave_renapo')->after('entidad')->nullable();
            $table->string('abreviatura')->after('clave_renapo')->nullable();
            $table->string('abrev_2dig')->after('abreviatura')->nullable();
            $table->string('abrev_3dig')->after('abrev_2dig')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_entidades', function (Blueprint $table) {
            $table->dropColumn('clave_renapo');
            $table->dropColumn('abreviatura');
            $table->dropColumn('abrev_2dig');
            $table->dropColumn('abrev_3dig');
        });
    }
}
