<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Corrección relación de llaves foráneas
    ALCANCE: Alimentarios
*/

class AlterChangeForeignKeysReferencesToAlimOficiosautorizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Desligamos
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->dropForeign(['tipo_oficio_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['dotacion_id']);
            $table->dropIndex('alim_oficiosautorizacion_tipo_oficio_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_region_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_dotacion_id_foreign');
        });

        // Relacionamos
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->foreign('tipo_oficio_id')->references('id')->on('alim_cat_tiposoficio');
            $table->foreign('region_id')->references('id')->on('cat_regiones');
            $table->foreign('dotacion_id')->references('id')->on('alim_dotaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Desligamos
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->dropForeign(['tipo_oficio_id']);
            $table->dropForeign(['region_id']);
            $table->dropForeign(['dotacion_id']);
            $table->dropIndex('alim_oficiosautorizacion_tipo_oficio_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_region_id_foreign');
            $table->dropIndex('alim_oficiosautorizacion_dotacion_id_foreign');
        });

        // Relacionamos
        Schema::table('alim_oficiosautorizacion', function (Blueprint $table) {
            $table->foreign('tipo_oficio_id')->references('id')->on('cat_ejercicios');
            $table->foreign('region_id')->references('id')->on('proveedores');
            $table->foreign('dotacion_id')->references('id')->on('proveedores');
        });
    }
}
