<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimCostosProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_costos_productos', function (Blueprint $table) {
            $table->foreign('presentacion_producto_id')->references('id')->on('alim_presentaciones_productos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_costos_productos', function (Blueprint $table) {
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_costos_productos_presentacion_producto_id_foreign');
            $table->dropIndex('alim_costos_productos_usuario_id_foreign');
        });
    }
}
