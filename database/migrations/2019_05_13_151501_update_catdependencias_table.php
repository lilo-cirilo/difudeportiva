<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCatdependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_dependencias', function($table)
        {
            $table->unsignedInteger('tipoinstitucion_id')->after('cargoencargado')->nullable();
            $table->dropColumn('tipo');
        });

        $registro = DB::table('cat_tiposinstitucion')->first();
        if ($registro)
            DB::table('cat_dependencias')->update(['tipoinstitucion_id'=>$registro->id]);

        Schema::table('cat_dependencias', function($table)
        {
            $table->unsignedInteger('tipoinstitucion_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_dependencias', function (Blueprint $table) {
            $table->dropColumn('tipoinstitucion_id');
            $table->string('tipo')->after('cargoencargado');
        });
    }
}
