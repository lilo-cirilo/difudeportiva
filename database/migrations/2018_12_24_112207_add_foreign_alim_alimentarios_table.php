<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimAlimentariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_alimentarios', function (Blueprint $table) {
      $table->foreign('localidad_id')->references('id')->on('cat_localidades');
      $table->foreign('municipio_id')->references('id')->on('cat_municipios');
      $table->foreign('programa_id')->references('id')->on('programas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_alimentarios', function (Blueprint $table) {
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_alimentarios_localidad_id_foreign');
      $table->dropIndex('alim_alimentarios_municipio_id_foreign');
      $table->dropIndex('alim_alimentarios_programa_id_foreign');
      $table->dropIndex('alim_alimentarios_usuario_id_foreign');
    });
  }
}
