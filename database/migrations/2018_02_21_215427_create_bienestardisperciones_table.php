<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBienestardispercionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bienestardispersiones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('personas_programa_id')->unsigned();
            $table->integer('lista_id')->unsigned();
            $table->boolean('pagado');
            $table->string('observacion')->nullable();
            $table->integer('usuario_id')->unsigned(); 
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['personas_programa_id', 'lista_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bienestardispersiones');
    }
}
