<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class CreateAlimRecibosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_recibos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('programacion_requisicion_id');
            $table->string('folio_caja',20)->nullable();
            $table->decimal('cantidad', 12, 2)->nullable();
            $table->date('fecha_pago')->nullable();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_recibos');
    }
}
