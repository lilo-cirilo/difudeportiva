<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class CreateCatClinicassaludTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_clinicassalud', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',50);
            $table->string('clave',50)->nullable();
            $table->unsignedInteger('localidad_id')->nullable();
            $table->unsignedInteger('municipio_id')->nullable();
            
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['nombre','clave']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_clinicassalud');
    }
}
