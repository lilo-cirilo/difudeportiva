<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: se guarda la suma de los totales de los productos que no generan iva
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumSubtotalNoIvaToRecmOrdenescompra extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_ordenescompra', function (Blueprint $table) {
            $table->float('subtotal_no_iva',8,2)->after('subtotal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_ordenescompra',function(Blueprint $table){
            $table->dropColumn('subtotal_no_iva');
        });
    }
}
