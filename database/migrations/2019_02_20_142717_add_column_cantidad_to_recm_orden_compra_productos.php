<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna para que almacene la cantidad del producto solicitado
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnCantidadToRecmOrdenCompraProductos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_orden_compra_productos', function (Blueprint $table) {
            $table->unsignedInteger('cantidad')->after('subtotal');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_orden_compra_productos',function(Blueprint $table){
            $table->dropColumn('cantidad');
        });
    }
}
