<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel 
  MOTIVO: Se presentó en la necesidad de desarrollo para el sistema Lari_DIF
  ALCANCE: Para Lari_DIF
*/

class AddForeignLariUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('lari_usuarios', function (Blueprint $table) {
      $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('localidad_id')->references('id')->on('cat_localidades')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('rol_id')->references('id')->on('cat_roles')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_solicitud_id')->references('id')->on('programas_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('lari_usuarios', function (Blueprint $table) {
      $table->dropForeign(['municipio_id']);
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['rol_id']);
      $table->dropForeign(['programa_solicitud_id']);
      
      $table->dropIndex('lari_usuarios_municipio_id_foreign');
      $table->dropIndex('lari_usuarios_localidad_id_foreign');
      $table->dropIndex('lari_usuarios_rol_id_foreign');
      $table->dropIndex('lari_usuarios_programa_solicitud_id_foreign');
    });
  }
}
