<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiSalidasSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mopi_salidas_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solicitud_id');
            $table->unsignedInteger('estatus_id');
            $table->string('folio')->nullable();
            $table->string('subtotal');
            $table->string('total');
            $table->string('nombre_recibe');
            $table->string('primer_apellido_recibe');
            $table->string('segundo_apellido_recibe');
            $table->string('nombre_transporta');
            $table->string('primer_apellido_transporta');
            $table->string('segundo_apellido_transporta');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mopi_salidas_solicitudes');
    }
}
