<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSdocInstitucionesResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_instituciones_responsables', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);        
            $table->dropIndex('sdoc_instituciones_responsables_usuario_id_foreign');    
        });

        Schema::dropIfExists('sdoc_instituciones_responsables');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('sdoc_instituciones_responsables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('institucion_id');
            $table->string('nombre_responsable');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('sdoc_instituciones_responsables', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }
}
