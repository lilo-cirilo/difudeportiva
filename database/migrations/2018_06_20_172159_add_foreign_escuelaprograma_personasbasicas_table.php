<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignEscuelaprogramaPersonasbasicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('escuelaprograma_personasbasicas', function (Blueprint $table) {
            $table->foreign('personabasica_id')->references('id')->on('personasbasicas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('escuela_programa_id')->references('id')->on('escuelas_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('escuelaprograma_personasbasicas', function (Blueprint $table) {
            $table->dropForeign(["personabasica_id"]);
            $table->dropForeign(["escuela_programa_id"]);
        });
    }
}
