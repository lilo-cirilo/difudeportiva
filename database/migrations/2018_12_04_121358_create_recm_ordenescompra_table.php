<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmOrdenescompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_ordenescompra', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->float('subtotal');
            $table->float('iva');
            $table->float('total');
            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('proveedor_id');
            $table->unsignedInteger('usuario_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_ordenescompra');
    }
}
