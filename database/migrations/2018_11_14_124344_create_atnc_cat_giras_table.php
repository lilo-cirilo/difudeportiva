<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos de Dirección General
  ALCANCE: Para todos
*/

class CreateAtncCatGirasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('atnc_cat_giras', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre',100);
        $table->timestamps();
        $table->softDeletes();
    });
      
    DB::statement('INSERT INTO atnc_cat_giras (nombre) SELECT distinct(nombre) FROM atnc_eventos');
    DB::statement('UPDATE atnc_eventos SET nombre = (SELECT id FROM atnc_cat_giras WHERE nombre=atnc_eventos.nombre)');
    
    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->renameColumn('nombre', 'gira_id');
    });

    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->unsignedInteger('gira_id')->change();
    });


  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->renameColumn('gira_id', 'nombre');
    });

    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->string('nombre')->change();
    });

    DB::statement('UPDATE atnc_eventos SET nombre = (SELECT nombre FROM atnc_cat_giras WHERE id=atnc_eventos.nombre)');

    Schema::dropIfExists('atnc_cat_giras');
  }
}
