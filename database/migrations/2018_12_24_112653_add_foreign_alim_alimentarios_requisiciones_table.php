<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimAlimentariosRequisicionesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
      $table->foreign('programacion_id')->references('id')->on('alim_programacion');
      $table->foreign('requisicion_id')->references('id')->on('alim_requisiciones');
      $table->foreign('estado_id')->references('id')->on('alim_cat_estados');
      $table->foreign('programa_id')->references('id')->on('programas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_alimentarios_requisiciones', function (Blueprint $table) {
      $table->dropForeign(['programacion_id']);
      $table->dropForeign(['requisicion_id']);
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_alimentarios_requisiciones_programacion_id_foreign');
      $table->dropIndex('alim_alimentarios_requisiciones_requisicion_id_foreign');
      $table->dropIndex('alim_alimentarios_requisiciones_estado_id_foreign');
      $table->dropIndex('alim_alimentarios_requisiciones_programa_id_foreign');
      $table->dropIndex('alim_alimentarios_requisiciones_usuario_id_foreign');
    });
  }
}
