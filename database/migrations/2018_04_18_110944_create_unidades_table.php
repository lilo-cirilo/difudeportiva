<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricula');
            $table->string('modelo');
            $table->string('marca');
            $table->integer('anio');
            $table->string('condiciones');
            $table->integer('kilometraje_inicial');
            $table->string('tipo_servicio');
            $table->string('observaciones')->nullable();
            $table->string('poliza_garantia');
            $table->string('numero_unidad')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades');
    }
}
