<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos extra a la tabla personas
  ALCANCE: Para todos los sitemas
*/

class CreateExtradatospersonasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('extradatospersonas', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('persona_id');
      $table->tinyInteger('grado')->unsigned()->nullable();
      $table->float('talla')->unsigned()->nullable();
      $table->date('fecha_talla')->nullable();
      $table->float('peso')->unsigned()->nullable();
      $table->date('fecha_peso')->nullable();
      $table->unsignedInteger('tiporopa_id')->unsigned()->nullable();
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::dropIfExists('extradatospersonas');
  }
}
