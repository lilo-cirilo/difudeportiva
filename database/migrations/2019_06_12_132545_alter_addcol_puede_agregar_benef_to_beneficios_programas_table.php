<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Miguel, Jhony.
    MOTIVO: Para saber si atención ciudadana puede o no registrar la petición del beneficiario.
    ALCANCE: Solicitudes
*/

class AlterAddcolPuedeAgregarBenefToBeneficiosProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiosprogramas', function (Blueprint $table) {
            $table->boolean('puede_agregar_benef')->after('predeterminado')->default(0)->comment('Para saber si atención ciudadana puede o no registrar la petición del beneficiario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiosprogramas', function (Blueprint $table) {
            $table->dropColumn('puede_agregar_benef');
        });
    }
}
