<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeAsistencias2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_Asistencias', function(Blueprint $table){
        $table->foreign('grupo_alumno_id')->references('id')->on('vale_GruposAlumnos')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_Asistencias', function(Blueprint $table){
        $table->dropForeign(['grupo_alumno_id']);
      });
    }
}
