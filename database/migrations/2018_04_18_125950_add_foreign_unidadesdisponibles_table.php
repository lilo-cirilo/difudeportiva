<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUnidadesdisponiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unidadesdisponibles', function (Blueprint $table) {
            $table->foreign('unidad_id')->references('id')->on('unidades')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unidadesdisponibles', function (Blueprint $table) {
            $table->dropForeign(["unidad_id"]);
        });
    }
}
