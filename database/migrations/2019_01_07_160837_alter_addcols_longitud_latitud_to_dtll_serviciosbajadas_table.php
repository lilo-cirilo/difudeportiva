<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class AlterAddcolsLongitudLatitudToDtllServiciosbajadasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_serviciosbajadas', function (Blueprint $table) {
      $table->string('latitud',50)->after('bajada')->nullable();
      $table->string('longitud',50)->after('latitud')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_serviciosbajadas', function (Blueprint $table) {
      $table->dropColumn('latitud');
      $table->dropColumn('longitud');
    });
  }
}
