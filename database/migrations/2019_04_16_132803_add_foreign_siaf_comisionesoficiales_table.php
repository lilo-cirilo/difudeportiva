<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafComisionesoficialesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
      $table->foreign('pago_id')->references('id')->on('siaf_pagos');
      $table->foreign('origen_municipio_id')->references('id')->on('cat_municipios');
      $table->foreign('destino_municipio_id')->references('id')->on('cat_municipios');
      $table->foreign('localidad_id')->references('id')->on('cat_localidades');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
      $table->dropForeign(['pago_id']);
      $table->dropForeign(['origen_municipio_id']);
      $table->dropForeign(['destino_municipio_id']);
      $table->dropForeign(['localidad_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_comisionesoficiales_pago_id_unique');
      $table->dropIndex('siaf_comisionesoficiales_origen_municipio_id_foreign');
      $table->dropIndex('siaf_comisionesoficiales_destino_municipio_id_foreign');
      $table->dropIndex('siaf_comisionesoficiales_localidad_id_foreign');
      $table->dropIndex('siaf_comisionesoficiales_usuario_id_foreign');
    });
  }
}
