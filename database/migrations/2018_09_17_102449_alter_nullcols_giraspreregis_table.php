<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNullcolsGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('giraspreregis', function (Blueprint $table) {
        $table->string('primer_apellido',30)->nullable()->change();
        $table->date('fecha_nacimiento')->nullable()->change();
        $table->string('calle')->nullable()->change();
        $table->string('numero_exterior',10)->nullable()->change();
        $table->string('colonia',100)->nullable()->change();
        $table->integer('localidad_id')->unsigned()->nullable()->change();
        $table->string('folio',20)->nullable()->change();
        $table->date('fecha_registro')->nullable()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('giraspreregis',function(Blueprint $table){
        $table->string('primer_apellido',30)->change();
        $table->date('fecha_nacimiento')->change();
        $table->string('calle')->change();
        $table->string('numero_exterior',10)->change();
        $table->string('colonia',100)->change();
        $table->integer('localidad_id')->unsigned()->change();
        $table->string('folio',20)->change();
        $table->date('fecha_registro')->change();
      });
    }
}
