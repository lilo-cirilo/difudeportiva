<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar estatus que usan cada programa que lo requiera
  ALCANCE: General
*/

class AddForeignCatEstadosProgramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('cat_estados_programas', function (Blueprint $table) {
      $table->foreign('estado_id')->references('id')->on('cat_estados')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdte('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('cat_estados_programas', function (Blueprint $table) {
      $table->dropForeign(['estado_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      // $table->dropIndex('cat_estados_programas_estado_id_foreign');  //Por el index unique este index ya no se crea
      $table->dropIndex('cat_estados_programas_programa_id_foreign');
      $table->dropIndex('cat_estados_programas_usuario_id_foreign');
    });
  }
}
