<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jerza 
  MOTIVO: Se presentó en la necesidad en el desarrollo del sistema Peticiones, se modificó para poderse usar la tabla motivo_programas
  ALCANCE: Modificción para todos
*/

class AlterDelcolMotivoIdAddcolMotivoprogramaIdStatussolicitudesStatusbienestarpersonasPersonasprogramasTables extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    /* Agregamos la columna motivoprograma_id y quitamos las relaciones a cat_motivos*/
    Schema::table('status_solicitudes', function (Blueprint $table) {
      $table->unsignedInteger('motivoprograma_id')->after('motivo_id')->nullable();
      $table->dropForeign(['motivo_id']);
      $table->dropIndex('status_solicitudes_motivo_id_foreign');
    });

    Schema::table('status_bienestarpersonas', function (Blueprint $table) {
      $table->unsignedInteger('motivoprograma_id')->after('motivo_id')->nullable();
      $table->dropForeign(['motivo_id']);
      $table->dropIndex('status_bienestarpersonas_motivo_id_foreign');
    });

    Schema::table('personas_programas', function (Blueprint $table) {
      $table->unsignedInteger('motivoprograma_id')->after('motivo_id')->nullable();
      $table->dropForeign(['motivo_id']);
      $table->dropIndex('personas_programas_motivo_id_foreign');
    });

    DB::statement('UPDATE status_solicitudes SET motivoprograma_id = (SELECT id FROM motivos_programas WHERE motivo_id=status_solicitudes.motivo_id) ');
    DB::statement('UPDATE status_bienestarpersonas SET motivoprograma_id = (SELECT id FROM motivos_programas WHERE motivo_id=status_bienestarpersonas.motivo_id) ');
    DB::statement('UPDATE personas_programas SET motivoprograma_id = (SELECT id FROM motivos_programas WHERE motivo_id=personas_programas.motivo_id) ');
    
    /* Eliminamos la columna motivo_id*/
    $tablas = ['status_solicitudes', 'status_bienestarpersonas', 'personas_programas'];
    for($i = 0; $i < count($tablas); $i++){
      Schema::table($tablas[$i], function (Blueprint $table) {
        $table->foreign('motivoprograma_id')->references('id')->on('motivos_programas')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->dropColumn('motivo_id');
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    /* Agregamos la columna motivo_id y quitamos las relaciones a motivos_programas*/
    Schema::table('status_solicitudes', function (Blueprint $table) {
      $table->unsignedInteger('motivo_id')->nullable()->after('motivoprograma_id');
      $table->dropForeign(['motivoprograma_id']);
      $table->dropIndex('status_solicitudes_motivoprograma_id_foreign');
    });

    Schema::table('status_bienestarpersonas', function (Blueprint $table) {
      $table->unsignedInteger('motivo_id')->nullable()->after('motivoprograma_id');
      $table->dropForeign(['motivoprograma_id']);
      $table->dropIndex('status_bienestarpersonas_motivoprograma_id_foreign');
    });

    Schema::table('personas_programas', function (Blueprint $table) {
      $table->unsignedInteger('motivo_id')->nullable()->after('motivoprograma_id');
      $table->dropForeign(['motivoprograma_id']);
      $table->dropIndex('personas_programas_motivoprograma_id_foreign');
    });

    DB::statement('UPDATE status_solicitudes SET motivo_id = (SELECT motivo_id FROM motivos_programas WHERE id=status_solicitudes.motivoprograma_id LIMIT 1)');
    DB::statement('UPDATE status_bienestarpersonas SET motivo_id = (SELECT motivo_id FROM motivos_programas WHERE id=status_bienestarpersonas.motivoprograma_id LIMIT 1)');
    DB::statement('UPDATE personas_programas SET motivo_id = (SELECT motivo_id FROM motivos_programas WHERE id=personas_programas.motivoprograma_id LIMIT 1)');

    /* Eliminamos la columna motivoprograma_id*/
    $tablas = ['status_solicitudes', 'status_bienestarpersonas', 'personas_programas'];
    for($i = 0; $i < count($tablas); $i++){
      Schema::table($tablas[$i], function (Blueprint $table) {
        $table->foreign('motivo_id')->references('id')->on('cat_motivos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->dropColumn('motivoprograma_id');
      });
    }
  }
}
