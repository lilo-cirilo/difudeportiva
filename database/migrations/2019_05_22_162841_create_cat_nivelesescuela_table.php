<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para uso de datos en la tabla cat_escuelas
  ALCANCE: General
*/

class CreateCatNivelesescuelaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_nivelesescuela', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('cat_nivelesescuela')->insert([
            ['nombre' => 'NO DEFINIDO', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => 'PREESCOLAR', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => 'PRIMARIA', 'created_at'=>NOW(), 'updated_at'=>NOW()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_nivelesescuela');
    }
}
