<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaOdontologicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta_odontologica', function (Blueprint $table) {
            $table->integer('consulta_id')->unsigned();
            $table->integer('servicio_id')->unsigned();
            $table->integer('pieza_dental_id')->unsigned();
            $table->integer('cantidad')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta_odontologica');
    }
}
