<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony.
  MOTIVO: Para relacionar indicar el estado en el que está una solicitud
  ALCANCE: Atención ciudadana y Apoyos Funcionales.
*/

class CreateEstadosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_solicitudes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('beneficioprograma_solicitud_id');
            $table->unsignedInteger('statusproceso_id');
            $table->unsignedInteger('motivo_programa_id')->nullable();
            $table->string('observacion')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_solicitudes');
    }
}
