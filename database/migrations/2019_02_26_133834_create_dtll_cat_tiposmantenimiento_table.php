<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllCatTiposmantenimientoTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dtll_cat_mantenimientos', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre', 50);
      $table->timestamps();
      $table->softDeletes();
    });

    DB::table('dtll_cat_mantenimientos')->insert([
      ['nombre'=>'REPARACIÓN', 'created_at'=>NOW()],
      ['nombre'=>'SERVICIO ELÉCTRICO', 'created_at'=>NOW()],
      ['nombre'=>'SERVICIO MECÁNICO', 'created_at'=>NOW()],
      ['nombre'=>'SERVICIO DE GARANTÍA', 'created_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dtll_cat_mantenimientos');
  }
}
