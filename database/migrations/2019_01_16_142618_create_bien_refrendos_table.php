<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar datos referentes a bienestar
  ALCANCE: Bienestar
*/

class CreateBienRefrendosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bien_refrendos', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('anio_programa_id');
        $table->unsignedInteger('estado_programa_id');
        
        $table->unsignedInteger('usuario_id');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bien_refrendos');
    }
}
