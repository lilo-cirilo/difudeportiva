<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenActividadesResponsablesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_actividades_responsables', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('actividad_id');
      $table->unsignedInteger('responsable_id');
      
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['actividad_id', 'responsable_id', 'deleted_at'],'even_ar_activ_resp_delat'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
    });

    // DB::statement("ALTER TABLE even_actividades_responsables ADD UNIQUE even_ar_activ_resp_delat (actividad_id, responsable_id, deleted_at)");  // Agregamos manualmente un unique, porque poreloquent crea un nombre muy largo y no lo acepta mysql
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_actividades_responsables');
  }
}
