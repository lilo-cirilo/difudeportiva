<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para el ingreso de una entrega de proveedor
    ALCANCE: Alimentarios
*/

class CreateAlimRequisicionesDatoscargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_requisiciones_datoscarga', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('programacion_requisicion_id');
            $table->unsignedInteger('datocarga_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_requisiciones_datoscarga');
    }
}
