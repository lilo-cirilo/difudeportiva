<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Bryan.
    MOTIVO: Para registrar un motivo de cancelación de la requisición
    ALCANCE: Alimentarios
*/

class AddColumnMotivoCancelacionToAlimProgramacionRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->string('motivo_cancelacion')->nullable()->after('estado_programa_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_programacion_requisiciones', function (Blueprint $table) {
            $table->dropColumn('motivo_cancelacion');
        });
    }
}
