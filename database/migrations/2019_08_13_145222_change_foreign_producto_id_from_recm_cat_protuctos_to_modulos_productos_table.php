<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignProductoIdFromRecmCatProtuctosToModulosProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
				// Quitamos la relación
        Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
            $table->dropForeign(['producto_id']);
						// $table->dropIndex('alim_presentaciones_productos_producto_id_foreign');
						$table->dropUnique('alim_presentaciones_productos_producto_id_presentacion_id_unique');
        });

			// Agregamos la información a modulos_productos
			$bids = DB::table('alim_presentaciones_productos')
										->select('producto_id',	
															DB::raw('(SELECT id FROM cat_modulos WHERE nombre="ASISTENCIA ALIMENTARIA") as modulo_id'), 
															DB::raw('91 AS usuario_id'),
															DB::raw('now() as created_at'),
															DB::raw('now() as updated_at')
														)
										->get();
			$inserts = [];
			foreach($bids as $bid) {
					$inserts[] =  [
						'producto_id' => $bid->producto_id,
						'modulo_id' => $bid->modulo_id,
						'usuario_id' => $bid->usuario_id,
						'created_at' => $bid->created_at,
						'updated_at' => $bid->updated_at
					]; 
			}
			DB::table('modulos_productos')->insert($inserts);

			// Redireccionamos el producto_id de alim_presentaciones_productos
			Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
            $table->renameColumn('producto_id', 'modulo_producto_id');
			});

			DB::statement('update alim_presentaciones_productos pp inner join modulos_productos mp on (pp.modulo_producto_id=mp.producto_id) set pp.modulo_producto_id = mp.id');

			// Establecemos la nueva relación
			Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
					$table->unique(['modulo_producto_id','presentacion_id'],'alim_presentaciones_productos_mod_prod_id_present_id_unique');
          $table->foreign('modulo_producto_id')->references('id')->on('modulos_productos');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
				$table->dropForeign(['modulo_producto_id']);
				$table->dropUnique('alim_presentaciones_productos_mod_prod_id_present_id_unique');
			});

			// Redireccionamos el producto_id de alim_presentaciones_productos
			DB::statement ('update alim_presentaciones_productos pp '.
										 'set modulo_producto_id=(select mp.producto_id '.
																							'from modulos_productos mp, cat_modulos m '.
																							'where m.id=mp.modulo_id and m.nombre="ASISTENCIA ALIMENTARIA" and mp.id=pp.modulo_producto_id)');

			Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
				$table->renameColumn('modulo_producto_id', 'producto_id');
			});

			// Quitamos la información a modulos_productos
			DB::table('modulos_productos')->where('modulo_id',DB::raw('(SELECT id FROM cat_modulos WHERE nombre="ASISTENCIA ALIMENTARIA")'))->delete();

			// Agregamos la relación
			Schema::table('alim_presentaciones_productos', function (Blueprint $table) {
					$table->unique(['producto_id','presentacion_id']);
					$table->foreign('producto_id')->references('id')->on('recm_cat_productos');
			});
    }
}
