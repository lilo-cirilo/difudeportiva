<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class CreateAlimVehiculosproveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_vehiculosproveedor', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('vehiculo_id');
            $table->string('placas',10);
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['vehiculo_id','placas']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_vehiculosproveedor');
    }
}
