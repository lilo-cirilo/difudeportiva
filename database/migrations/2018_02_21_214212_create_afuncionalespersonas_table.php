<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAfuncionalespersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afuncionalespersonas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('persona_id')->unsigned();
            $table->integer('limitacion_id')->unsigned()->nullable();
            $table->integer('discapacidad_id')->unsigned();
            $table->integer('tiempodiscapacidad')->nullable();
            $table->string('otrocontacto')->nullable();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afuncionalespersonas');
    }
}
