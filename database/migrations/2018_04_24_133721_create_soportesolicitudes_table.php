<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoportesolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soportesolicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_solicitud');
            $table->integer('solicitante_id')->unsigned();
            $table->integer('servicio_id')->unsigned();
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->string('asunto');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soportesolicitudes');
    }
}
