<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignSalidasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_salidas_productos', function (Blueprint $table) {
            $table->foreign('salida_id')->references('id')->on('mopi_salidas_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('mopi_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_salidas_productos', function (Blueprint $table) {
            $table->dropForeign(['salida_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('mopi_salidas_productos_salida_id_foreign');
            $table->dropIndex('mopi_salidas_productos_usuario_id_foreign');
            $table->dropIndex('mopi_salidas_productos_presentacion_producto_id_foreign');
        });
    }
}
