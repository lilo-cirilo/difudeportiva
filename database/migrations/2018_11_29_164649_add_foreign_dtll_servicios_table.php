<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian y Jhony
  MOTIVO: Reestructura del funcionamiento del sistema
  ALCANCE: DIF te lleva
*/

class AddForeignDtllServiciosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_servicios', function (Blueprint $table) {
      $table->foreign('controlruta_id')->references('id')->on('controlrutas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('pasajero_id')->references('id')->on('dtll_pasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_servicios', function (Blueprint $table) {
      $table->dropForeign(['controlruta_id']);
      $table->dropForeign(['pasajero_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('dtll_servicios_controlruta_id_foreign');
      $table->dropIndex('dtll_servicios_pasajero_id_foreign');
      $table->dropIndex('dtll_servicios_usuario_id_foreign');
    });
  }
}
