<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPeticionidToOrevEventos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('orev_eventos', function (Blueprint $table) {
				$table->foreign('peticion_id')->references('id')->on('beneficiosprogramas_solicitudes');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('orev_eventos', function (Blueprint $table) {
				$table->dropForeign(['peticion_id']);
				$table->dropIndex('orev_eventos_peticion_id_foreign');
			});
    }
}
