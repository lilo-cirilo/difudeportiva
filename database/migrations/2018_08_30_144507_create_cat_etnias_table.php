<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatEtniasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cat_etnias', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->integer('clave_inegi')->nullable();
        $table->timestamps();
        $table->softDeletes();
      });

      DB::table('cat_etnias')->insert([
        'nombre' => 'NO APLICA'
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cat_etnias');
    }
}
