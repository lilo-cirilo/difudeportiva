<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar estatus que usan cada programa que lo requiera
  ALCANCE: General
*/

class CreateCatEstadosProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cat_estados_programas', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('estado_id');
        $table->unsignedInteger('programa_id');
        $table->tinyInteger('orden')->unsigned();
        
        $table->unsignedInteger('usuario_id');
        $table->timestamps();
        $table->softDeletes();

        $table->unique(['estado_id', 'programa_id','deleted_at']);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cat_estados_programas');
    }
}
