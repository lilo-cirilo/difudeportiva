<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolPrecioDetallesentradasProductosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('detallesentradas_productos', function (Blueprint $table) {
      $table->decimal('precio', 8, 2)->nullable()->after('cantidad');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('detallesentradas_productos', function (Blueprint $table) {
      $table->dropColumn('precio');
    });
  }
}
