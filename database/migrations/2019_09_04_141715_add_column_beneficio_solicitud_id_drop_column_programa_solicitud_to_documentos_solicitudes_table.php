<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: 
    ALCANCE: 
*/

class AddColumnBeneficioSolicitudIdDropColumnProgramaSolicitudToDocumentosSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('documentos_solicitudes', function (Blueprint $table) {
				$table->unsignedInteger('beneficio_solicitud_id')->after('documentos_persona_id');
				$table->foreign('beneficio_solicitud_id')->references('id')->on('beneficiosprogramas_solicitudes');
			});

			Schema::table('documentos_solicitudes', function (Blueprint $table) {
				$table->dropForeign(['programa_solicitud_id']);
				$table->dropIndex('documentos_solicitudes_programa_solicitud_id_foreign');
				$table->dropColumn('programa_solicitud_id');
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('documentos_solicitudes', function (Blueprint $table) {
				$table->unsignedInteger('programa_solicitud_id')->after('documentos_persona_id');
				$table->foreign('programa_solicitud_id')->references('id')->on('programas_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
			});

			Schema::table('documentos_solicitudes', function (Blueprint $table) {
				$table->dropForeign(['beneficio_solicitud_id']);
				$table->dropIndex('documentos_solicitudes_beneficio_solicitud_id_foreign');
				$table->dropColumn('beneficio_solicitud_id');
			});
    }
}
