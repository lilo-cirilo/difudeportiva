<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan, jefe del departamento
  MOTIVO: Para poder indicar qué nombre de área es oficial
  ALCANCE: General
*/

class AlterAddcolOficialToCatAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
      Schema::table('cat_areas', function (Blueprint $table) {
        $table->tinyInteger('oficial')->unsigned()->nullable()->after('siglas');
      });

      DB::statement('UPDATE cat_areas SET oficial=1');

      Schema::table('cat_areas', function (Blueprint $table) {
        $table->boolean('oficial')->tinyInteger('oficial')->unsigned()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cat_areas',function(Blueprint $table){
        $table->dropColumn('oficial');
      });
    }
}
