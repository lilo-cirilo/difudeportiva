<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega columna campos 
  ALCANCE: Para guardas la sumatoria de los compremetido por categoria del sistema de recursos materiales
*/

class AddColumnCamposToMopiSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_solicitudes', function (Blueprint $table) {
            $table->text('campos')->after('justificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_solicitudes',function(Blueprint $table){
            $table->dropColumn('campos');
        });
    }
}
