<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class AddForeignPetaInstitucionesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peta_instituciones_usuarios', function (Blueprint $table) {
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            $table->foreign('access_usuario_id')->references('id')->on('usuarios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peta_instituciones_usuarios', function (Blueprint $table) {
            $table->dropForeign(['institucion_id']);      // Ya no genra index por el unique
            $table->dropForeign(['access_usuario_id']);
            $table->dropForeign(['usuario_id']);
            
            $table->dropIndex('peta_instituciones_usuarios_access_usuario_id_foreign');
            $table->dropIndex('peta_instituciones_usuarios_usuario_id_foreign');
        });
    }
}
