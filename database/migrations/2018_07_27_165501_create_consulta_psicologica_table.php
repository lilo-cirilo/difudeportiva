<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsultaPsicologicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('consulta_psicologica', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('consulta_id')->unsigned();
        $table->string('adiccion',50);
        $table->string('entrevista')->nullable();
        $table->string('e_psicometrico')->nullable();
        $table->string('p_individual')->nullable();
        $table->string('p_grupo')->nullable();
        $table->string('p_pareja')->nullable();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta_psicologica');
    }
}
