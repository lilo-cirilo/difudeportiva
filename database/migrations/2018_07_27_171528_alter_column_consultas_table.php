<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterColumnConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('consultas',function(Blueprint $table){
        $table->datetime('horainicio')->nullable()->change();
        $table->datetime('horafin')->nullable()->change();
        $table->float('peso')->nullable()->change();
        $table->float('talla')->nullable()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('consultas', function(Blueprint $table){
        $table->datetime('horainicio')->change();
        $table->datetime('horafin')->change();
        $table->float('peso')->change();
        $table->float('talla')->change();
      });
    }
}
