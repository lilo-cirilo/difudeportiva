<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class CreateSiafEnlacesfinancierosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('siaf_enlacesfinancieros', function (Blueprint $table) {
      $table->increments('id');

      $table->string('nombre_cuenta',50);
      $table->string('clabe',50);
      $table->unsignedInteger('banco_id');
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('siaf_enlacesfinancieros');
  }
}
