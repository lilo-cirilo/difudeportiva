<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafHistorialAreasEnlacesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_historial_areas_enlaces', function (Blueprint $table) {
      $table->foreign('fondorotatorioarea_id')->references('id')->on('siaf_fondorotatorioareas');
      $table->foreign('enlacefinanciero_id')->references('id')->on('siaf_enlacesfinancieros');
      $table->foreign('empleado_id')->references('id')->on('empleados');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_historial_areas_enlaces', function (Blueprint $table) {
      $table->dropForeign(['fondorotatorioarea_id']);
      $table->dropForeign(['enlacefinanciero_id']);
      $table->dropForeign(['empleado_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_historial_areas_enlaces_fondorotatorioarea_id_foreign');
      $table->dropIndex('siaf_historial_areas_enlaces_enlacefinanciero_id_foreign');
      $table->dropIndex('siaf_historial_areas_enlaces_empleado_id_foreign');
      $table->dropIndex('siaf_historial_areas_enlaces_usuario_id_foreign');
    });
  }
}
