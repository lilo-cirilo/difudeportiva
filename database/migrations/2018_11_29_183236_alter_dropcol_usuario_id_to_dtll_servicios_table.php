<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony
  MOTIVO: Se presentó en la necesidad de desarrollo para el sistema DIF te lleva
  ALCANCE: Para DIF te lleva
*/

class AlterDropcolUsuarioIdToDtllServiciosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_servicios', function (Blueprint $table) {
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('dtll_servicios_usuario_id_foreign');
      $table->dropColumn('usuario_id');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_servicios', function (Blueprint $table) {
      $table->unsignedInteger('usuario_id')->after('longitud');
    });

    DB::statement("UPDATE dtll_servicios SET usuario_id=1");

    Schema::table('dtll_servicios', function (Blueprint $table) {
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }
}
