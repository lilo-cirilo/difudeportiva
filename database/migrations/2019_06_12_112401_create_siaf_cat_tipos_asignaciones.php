<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
    PETICIÓN: Manuel.
    MOTIVO: Para guardar los tipos de asiganaciones que se realizan
    ALCANCE: fondorotatorio
*/

class CreateSiafCatTiposAsignaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_cat_tipos_asignaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',100);
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('siaf_cat_tipos_asignaciones')->insert([
            ['nombre' => 'ASIGNACION', 'created_at'=>NOW(), 'updated_at'=>NOW()]
          ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_cat_tipos_asignaciones');
    }
}
