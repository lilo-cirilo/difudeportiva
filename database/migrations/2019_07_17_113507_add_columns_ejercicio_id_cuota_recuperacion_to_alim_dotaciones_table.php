<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: Para clasificar la inforamcion de dotaciones por ejercicio
    ALCANCE: Alimentarios
*/

class AddColumnsEjercicioIdCuotaRecuperacionToAlimDotacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->unsignedInteger('ejercicio_id')->after('id');
            $table->decimal('cuota_recuperacion',6,2)->after('tipoaccion_id');
        });

        $registro=DB::table('cat_ejercicios')->orderby('id','desc')->first();
        if($registro)
            DB::table('alim_dotaciones')->update(['ejercicio_id'=>$registro->id]);

        Schema::table('alim_dotaciones', function(Blueprint $table){
            $table->foreign('ejercicio_id')->references('id')->on('cat_ejercicios');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_dotaciones', function(Blueprint $table){
            $table->dropForeign(['ejercicio_id']);
            $table->dropIndex('alim_dotaciones_ejercicio_id_foreign');
        });
        Schema::table('alim_dotaciones', function (Blueprint $table) {
            $table->dropColumn('ejercicio_id');
            $table->dropColumn('cuota_recuperacion');
        });
    }
}
