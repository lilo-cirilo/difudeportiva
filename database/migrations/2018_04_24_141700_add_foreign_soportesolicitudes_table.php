<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSoportesolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soportesolicitudes', function (Blueprint $table) {
            $table->foreign('servicio_id')->references('id')->on('soporteservicios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('solicitante_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soportesolicitudes', function (Blueprint $table) {
            $table->dropForeign(["servicio_id"]);
            $table->dropForeign(["solicitante_id"]);
        });
    }
}
