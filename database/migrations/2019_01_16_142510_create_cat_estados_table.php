<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar nombres de estatus
  ALCANCE: General
*/

class CreateCatEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('cat_estados', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->timestamps();
        $table->softDeletes();

        $table->unique(['nombre', 'deleted_at']);
      });

      DB::table('cat_estados')->insert([
        ['nombre' => 'ALTA', 'created_at'=>NOW()],
        ['nombre' => 'ACTIVA', 'created_at'=>NOW()],
        ['nombre' => 'CANCELADA', 'created_at'=>NOW()],
        ['nombre' => 'BAJA', 'created_at'=>NOW()],
        ['nombre' => 'REVISION', 'created_at'=>NOW()],
        ['nombre' => 'ENVIADA', 'created_at'=>NOW()],
        ['nombre' => 'RECIBIDA', 'created_at'=>NOW()]
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('cat_estados');
    }
}
