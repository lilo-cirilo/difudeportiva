<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony, Alex y Miguel
  MOTIVO: Para ingresar solicitudes
  ALCANCE: General
*/

class AddForeignBeneficiossolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('beneficiosprogramas_solicitudes', function (Blueprint $table) {
            $table->foreign('solicitud_id')->references('id')->on('solicitudes');
            $table->foreign('beneficioprograma_id')->references('id')->on('beneficiosprogramas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('beneficiosprogramas_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['beneficioprograma_id']);
            $table->dropForeign(['usuario_id']);

            $table->dropIndex('beneficiosprogramas_solicitudes_solicitud_id_foreign');  
            $table->dropIndex('beneficiosprogramas_solicitudes_beneficioprograma_id_foreign');
            $table->dropIndex('beneficiosprogramas_solicitudes_usuario_id_foreign');  
        });
    }
}
