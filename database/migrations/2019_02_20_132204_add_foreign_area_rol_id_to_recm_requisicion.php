<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna area_rol_id
  ALCANCE: Para filtrar las solicitudes por usuario
*/

class AddForeignAreaRolIdToRecmRequisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->foreign('area_rol_id')->references('id')->on('recm_area_rols')->onUpdate('CASCADE')->onDelete('CASCADE');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->dropForeign(['area_rol_id']);
            $table->dropIndex('recm_requisicions_area_rol_id_foreign');
        });
    }
}
