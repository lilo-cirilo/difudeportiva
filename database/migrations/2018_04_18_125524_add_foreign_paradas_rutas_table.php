<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignParadasRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paradas_rutas', function (Blueprint $table) {
            $table->foreign('ruta_id')->references('id')->on('rutas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('parada_id')->references('id')->on('paradas')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paradas_rutas', function (Blueprint $table) {
            $table->dropForeign(["ruta_id"]);
            $table->dropForeign(["parada_id"]); 
        });
    }
}
