<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Anthony.
    MOTIVO: Volver nullable el campo de numero de documento
    ALCANCE: SICODOC
*/

class AlterColnullNumerodocumentoToSdocDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
		public function up()
    {
			Schema::table('sdoc_documentos', function (Blueprint $table) {
				$table->string('numerodocumento')->nullable()->change();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('sdoc_documentos', function (Blueprint $table) {
				$table->string('numerodocumento')->change();
			});
    }
}