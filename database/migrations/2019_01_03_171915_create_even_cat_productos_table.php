<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenCatProductosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('even_cat_productos', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['nombre','deleted_at']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('even_cat_productos');
  }
}
