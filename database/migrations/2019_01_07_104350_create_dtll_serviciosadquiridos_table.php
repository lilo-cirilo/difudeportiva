<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class CreateDtllServiciosadquiridosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dtll_serviciosadquiridos', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('pasajero_id')->unsigned();
      $table->integer('tiposervicio_id')->unsigned();
      $table->integer('tipopasajero_id')->unsigned();
      $table->string('numPasajero',7);

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dtll_serviciosadquiridos');
  }
}
