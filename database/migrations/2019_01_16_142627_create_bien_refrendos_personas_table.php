<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Alexander
  MOTIVO: Para poder almacenar datos referentes a bienestar
  ALCANCE: Bienestar
*/

class CreateBienRefrendosPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('bien_refrendos_personas', function (Blueprint $table) {
        $table->increments('id');
        $table->unsignedInteger('refrendo_id');
        $table->unsignedInteger('bien_persona_id');
        $table->unsignedInteger('estado_programa_id');
        
        $table->unsignedInteger('usuario_id');
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bien_refrendos_personas');
    }
}
