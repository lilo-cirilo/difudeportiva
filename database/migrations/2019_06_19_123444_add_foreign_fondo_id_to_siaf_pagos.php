<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para la relacion de fondo con el pago
    ALCANCE: fondorotatorio
*/

class AddForeignFondoIdToSiafPagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->foreign('fondo_id')->references('id')->on('siaf_fondorotatorioareas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_pagos', function (Blueprint $table) {
            $table->dropForeign(['fondo_id']);
         
            $table->dropIndex('siaf_pagos_fondo_id_foreign');

        });
    }
}
