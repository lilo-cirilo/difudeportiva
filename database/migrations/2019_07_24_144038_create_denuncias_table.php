<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDenunciasTable extends Migration
{
    /**
     * PETICIÓN: Beto
     * MOTIVO: Para guardar las denuncias de la pagina dif
     * ALCANCE: Pagina DIF
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pag_denuncias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_completo')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('email')->nullable();
            $table->string('telefono')->nullable();
            $table->enum('asunto', ['QUEJA', 'SUGERENCIA', 'DENUNCIA']);	
            $table->text('comentario');
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pag_denuncias');
    }
}
