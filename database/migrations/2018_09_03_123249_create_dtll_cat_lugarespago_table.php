<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllCatLugarespagoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dtll_cat_lugarespago', function (Blueprint $table) {
        $table->increments('id');
        $table->string('nombre');
        $table->timestamps();
        $table->softDeletes();
      });

      DB::table('dtll_cat_lugarespago')->insert([
        ['nombre'=>'BANCO', 'created_at'=>NOW()],
        ['nombre'=>'CAJA', 'created_at'=>NOW()]
      ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_cat_lugarespago');
    }
}
