<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Yo (beto)
  MOTIVO: Faltan columnas en las tablas de viaticos
  ALCANCE: Módulo de BancaDIF
*/

class CreateSiafUnidadesejecutorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_unidadesejecutoras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ur', 3);
            $table->string('ue', 3);
            $table->string('unidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_unidadesejecutoras');
    }
}
