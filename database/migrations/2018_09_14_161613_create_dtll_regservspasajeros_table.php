<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDtllRegservspasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dtll_regservspasajeros', function (Blueprint $table) {
        $table->increments('id');
        $table->tinyInteger('acompanante');
        $table->string('clave',7);
        $table->integer('pasajero_id')->unsigned();
        $table->datetime('fecha');
        $table->integer('conductor_id')->unsigned();
        $table->integer('tiposervicio_id')->unsigned();
        $table->integer('usuario_id')->unsigned();
        $table->timestamps();
        $table->softDeletes();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dtll_regservspasajeros');
    }
}
