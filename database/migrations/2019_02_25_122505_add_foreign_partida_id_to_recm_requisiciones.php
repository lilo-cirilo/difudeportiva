<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para relacionar la requisicion con la partida
  ALCANCE: Para el sistema de recursos materiales
*/

class AddForeignPartidaIdToRecmRequisiciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->foreign('partida_id')->references('id')->on('recm_cat_partidas');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->dropForeign(['partida_id']);
            $table->dropIndex('recm_requisicions_partida_id_foreign');
          });
    }
}
