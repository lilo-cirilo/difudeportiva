<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de requisiciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimCantidadesdotacionesRequisisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_cantidadesdotaciones_requisisiones', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('programacion_requisicion_id');
            $table->unsignedInteger('dotacion_id');
            $table->smallInteger('cantidad_dotaciones')->unsigned();
            $table->smallInteger('cantidad_beneficiarios')->unsigned();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_cantidadesdotaciones_requisisiones');
    }
}
