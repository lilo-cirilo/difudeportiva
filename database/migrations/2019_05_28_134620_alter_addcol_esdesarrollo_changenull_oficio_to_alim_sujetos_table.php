<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jony.
  MOTIVO: Para ingresar especificaciones a sujetos
  ALCANCE: Alimentarios
*/

class AlterAddcolEsdesarrolloChangenullOficioToAlimSujetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_sujetos', function (Blueprint $table) {
            $table->string('oficio',30)->nullable()->change();
            $table->tinyInteger('esdesarrollo')->after('alimentario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_sujetos', function (Blueprint $table) {
            $table->string('oficio',30)->change();
            $table->dropColumn('esdesarrollo');
        });
    }
}
