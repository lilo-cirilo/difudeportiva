<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class AlterRenametableAddcolsNombreGoogleidToCatEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::rename('cat_eventos', 'even_cat_tiposevento');

    Schema::table('even_cat_tiposevento', function (Blueprint $table) {
      $table->string('nombre',100)->after('id');
      $table->string('descripcion',150)->nullable()->change();
      $table->string('id_calendario_google')->after('descripcion')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('even_cat_tiposevento', function (Blueprint $table) {
      $table->dropColumn('nombre');
      $table->dropColumn('id_calendario_google');
      $table->string('descripcion',150)->change();
    });

    Schema::rename('even_cat_tiposevento', 'cat_eventos');
  }
}
