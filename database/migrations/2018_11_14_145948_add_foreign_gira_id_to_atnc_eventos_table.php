<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos de Dirección General
  ALCANCE: Para todos
*/

class AddForeignGiraIdToAtncEventosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->foreign('gira_id')->references('id')->on('atnc_cat_giras')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('atnc_eventos', function (Blueprint $table) {
      $table->dropForeign(['gira_id']);
      $table->dropIndex('atnc_eventos_gira_id_foreign');
    });
  }
}
