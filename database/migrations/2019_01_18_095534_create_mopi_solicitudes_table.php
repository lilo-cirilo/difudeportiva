<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mopi_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio')->nullable();
            $table->float('total');
            $table->date('fecha_entrega');
            $table->mediumText('justificacion');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('estatus_id');
            $table->unsignedInteger('area_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mopi_solicitudes');
    }
}
