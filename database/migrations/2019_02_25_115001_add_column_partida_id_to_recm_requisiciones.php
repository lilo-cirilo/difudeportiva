<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para relacionar la requisicion con la partida
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnPartidaIdToRecmRequisiciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->unsignedInteger('partida_id')->after('area_id'); 
        });

        $registro = DB::table('recm_cat_partidas')->first();
        if ($registro)
          DB::table('recm_requisicions')->update(['partida_id'=>$registro->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions',function(Blueprint $table){
            $table->dropColumn('partida_id');
        });
    }
}
