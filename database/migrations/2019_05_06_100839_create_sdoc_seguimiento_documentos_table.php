<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SICODOC
  ALCANCE: Para el sistema SICODOC
*/

class CreateSdocSeguimientoDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_seguimiento_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('documento_id');
            $table->unsignedInteger('estatus_id');
            $table->unsignedInteger('areaturnado_id');
            $table->text('observaciones')->nullable()->default(null);
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_seguimiento_documentos');
    }
}
