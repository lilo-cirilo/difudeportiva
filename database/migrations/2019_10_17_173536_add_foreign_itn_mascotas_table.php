<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de las mascotas de las personas que asisten a una caravana
    ALCANCE: Itinerantes
*/

class AddForeignItnMascotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itn_mascotas', function (Blueprint $table) {
            $table->foreign('beneficiopersona_caravana_id')->references('id')->on('itn_beneficiospersonas_caravana');	
            $table->foreign('especie_id')->references('id')->on('itn_cat_especiesmascotas');	
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itn_mascotas', function (Blueprint $table) {
            $table->dropForeign(['beneficiopersona_caravana_id']);
            $table->dropForeign(['especie_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('itn_mascotas_beneficiopersona_caravana_id_foreign');
						$table->dropIndex('itn_mascotas_especie_id_foreign');
						$table->dropIndex('itn_mascotas_usuario_id_foreign');
        });
    }
}
