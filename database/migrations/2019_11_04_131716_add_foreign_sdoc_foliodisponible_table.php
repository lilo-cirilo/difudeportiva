<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar folio y número de oficio de dirección general
    ALCANCE: SICODOC
*/

class AddForeignSdocFoliodisponibleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sdoc_foliodisponible', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);	
						$table->dropIndex('sdoc_foliodisponible_usuario_id_foreign');
        });
    }
}
