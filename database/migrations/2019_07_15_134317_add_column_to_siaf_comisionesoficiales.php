<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToSiafComisionesoficiales extends Migration
{
    /**
     * PETICIÓN: Luis V.
     * MOTIVO: Agregar columna faltante {PRESENTARSE} para viaticos y gastos en comision
     * ALCANCE: FONDO ROTATORIO
     *
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->string('presentarse')->after('usuario_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropColumn('presentarse');
        });
    }
}
