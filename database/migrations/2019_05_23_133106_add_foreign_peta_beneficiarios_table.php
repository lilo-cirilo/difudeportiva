<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class AddForeignPetaBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peta_beneficiarios', function (Blueprint $table) {
            $table->foreign('persona_programa_id')->references('id')->on('personas_programas');
            $table->foreign('institucion_usuario_id')->references('id')->on('peta_instituciones_usuarios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peta_beneficiarios', function (Blueprint $table) {
            $table->dropForeign(['persona_programa_id']);  // Ya no genra index por el unique
            $table->dropForeign(['institucion_usuario_id']);
            $table->dropForeign(['usuario_id']);
            
            $table->dropIndex('peta_beneficiarios_institucion_usuario_id_foreign');
            $table->dropIndex('peta_beneficiarios_usuario_id_foreign');
        });
    }
}
