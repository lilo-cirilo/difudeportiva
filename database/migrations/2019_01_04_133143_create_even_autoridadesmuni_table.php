<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Gerzain
  MOTIVO: Para poder almacenar datos referentes a eventos y giras
  ALCANCE: Eventos de Dirección General
*/

class CreateEvenAutoridadesmuniTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('even_autoridadesmuni', function (Blueprint $table) {
			$table->increments('id');

			$table->unsignedInteger('fichainfo_id');
			$table->string('nombre');
			$table->unsignedInteger('cargo_id');
			$table->unsignedInteger('partidopolitico_id');

			$table->unsignedInteger('usuario_id');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('even_autoridadesmuni');
	}
}
