<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar los usuarios que tendrá una escuela del programa desayunos
  ALCANCE: Para el sistema de alimentarios
*/

class CreateAlimDesayunosUsuariosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('alim_desayunos_usuarios', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('usuario_id');
      $table->unsignedInteger('desayuno_id');
      $table->unsignedInteger('usuariosis_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('alim_desayunos_usuarios');
  }
}
