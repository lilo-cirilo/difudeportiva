<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Anthony
  MOTIVO: Se agregan columnas para guardar el municipio_id de las instituciones
  ALCANCE: Todos los sistemas
*/

class AddColumnMunicipioIdToCatInstitucionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_instituciones', function (Blueprint $table) {
            $table->unsignedInteger('municipio_id')->after('tipoinstitucion_id')->nullable();
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_instituciones', function (Blueprint $table) {
            $table->dropForeign(['municipio_id']);
            $table->dropIndex('cat_instituciones_municipio_id_foreign');
            $table->dropColumn('municipio_id');
        });
    }
}
