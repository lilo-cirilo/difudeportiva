<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimCatTiposdistribucionTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('alim_cat_tiposdistribucion', function (Blueprint $table) {
      $table->increments('id');
      $table->string('nombre');
      $table->timestamps();
      $table->softDeletes();
    });

    DB::table('alim_cat_tiposdistribucion')->insert([
      ['nombre' => 'MUNICIPAL', 'created_at'=>NOW()],
      ['nombre' => 'ESPECIAL CAM', 'created_at'=>NOW()],
      ['nombre' => 'AMPLIACION', 'created_at'=>NOW()],
      ['nombre' => 'EXTENSION', 'created_at'=>NOW()],
      ['nombre' => 'AMPLIACION PROGRAMACION', 'created_at'=>NOW()]
    ]);
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('alim_cat_tiposdistribucion');
  }
}
