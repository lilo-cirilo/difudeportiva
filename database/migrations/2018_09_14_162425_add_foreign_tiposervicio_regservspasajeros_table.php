<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignTiposervicioRegservspasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('dtll_regservspasajeros', function(Blueprint $table){
        $table->foreign('pasajero_id')->references('id')->on('dtll_pasajeros')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('tiposervicio_id')->references('id')->on('dtll_cat_tiposervicios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('conductor_id')->references('id')->on('conductores')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('dtll_regservspasajeros', function(Blueprint $table){
        $table->dropForeign(['pasajero_id']);
        $table->dropForeign(['tiposervicio_id']);
        $table->dropForeign(['conductor_id']);
        $table->dropForeign(['usuario_id']);
      });
    }
}
