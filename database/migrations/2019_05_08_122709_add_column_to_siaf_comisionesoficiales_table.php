<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Beto
  MOTIVO: Para ingresar tabuladores de costos de viáticos
  ALCANCE: Para el sistema SIAF
*/

class AddColumnToSiafComisionesoficialesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->unsignedInteger('cuota_id')->after('destino_municipio_id')->nullable();
        });
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->foreign('cuota_id')->references('id')->on('siaf_tabuladorviaticos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comisionesoficiales', function (Blueprint $table) {
            $table->dropForeign(['cuota_id']);
            $table->dropColumn('cuota_id');
        });
    }
}
