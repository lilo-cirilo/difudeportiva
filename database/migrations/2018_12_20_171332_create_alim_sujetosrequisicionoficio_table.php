<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimSujetosrequisicionoficioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_sujetosrequisicionoficio', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('programacion_id');
            $table->unsignedInteger('programa_id');
            $table->datetime('fecha_solicitud');
            $table->unsignedInteger('numero_oficio_solicitud');
            $table->unsignedInteger('numero_oficio_requisicion');
            $table->tinyInteger('pertenece_decsegego')->unsigned();
            $table->datetime('fecha_oficio_requisicion');
            $table->tinyInteger('bimestre')->unsigned();
            $table->unsignedInteger('numero_beneficiarios');
            $table->tinyInteger('se_cobra')->unsigned();
            $table->string('quien_recibe',100);
            $table->unsignedInteger('cargo_id');
            $table->string('observaciones')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_sujetosrequisicionoficio');
    }
}
