<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos referentes a pasajeros del programa Dif te lleva
  ALCANCE: DIF te lleva
*/

class CreateDtllPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('dtll_pagos', function (Blueprint $table) {
      $table->increments('id');

      $table->unsignedInteger('servicioadquirido_id');
      $table->date('fechaIniServ');
      $table->date('fechaFinServ');
      $table->tinyInteger('numMesesServ')->unsigned();
      $table->string('folioPago',15);
      $table->integer('lugarPago_id')->unsigned();

      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('dtll_pagos');
  }
}
