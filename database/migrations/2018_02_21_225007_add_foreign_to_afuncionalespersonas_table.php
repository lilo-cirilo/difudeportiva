<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToAfuncionalespersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('afuncionalespersonas', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('discapacidad_id')->references('id')->on('cat_discapacidades')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
             $table->foreign('limitacion_id')->references('id')->on('cat_limitaciones')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('afuncionalespersonas', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['persona_id']);
            $table->dropForeign(['discapacidad_id']);
            $table->dropForeign(['limitacion_id']);
        });
    }
}
