<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBitacoraEmpleadoAreaTable extends Migration
{
    /**
     * PETICIÓN: Anthony
     * MOTIVO: Tabla para guardar los cambios en el área de un empleado
     * ALCANCE: GENERAL
     * 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bitacora_empleado_area', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('empleado_id');
            $table->integer('area_id');
            $table->integer('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bitacora_empleado_area');
    }
}
