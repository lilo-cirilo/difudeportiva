<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_areas', function (Blueprint $table) {
            $table->foreign('tipoarea_id')->references('id')->on('cat_tipoareas');
            $table->foreign('padre_id')->references('id')->on('cat_areas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_areas', function (Blueprint $table) {
            $table->dropForeign(['tipoarea_id']);
            $table->dropForeign(['padre_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
