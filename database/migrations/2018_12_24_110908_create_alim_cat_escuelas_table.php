<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class CreateAlimCatEscuelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_cat_escuelas', function (Blueprint $table) {
            $table->increments('id');

            $table->string('nombre');
            $table->string('clave',12);
            $table->unsignedInteger('nivel_id');
            $table->tinyInteger('turno')->unsigned();
            $table->unsignedInteger('padre_id')->nullable();
            $table->tinyInteger('extension');
            $table->unsignedInteger('municipio_id');
            $table->unsignedInteger('localidad_id')->nullable();
            $table->string('latitud',50)->nullable();
            $table->string('longitud',50)->nullable();
            $table->tinyInteger('oficial')->unsigned();
            $table->unsignedInteger('usuario_id');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_cat_escuelas');
    }
}
