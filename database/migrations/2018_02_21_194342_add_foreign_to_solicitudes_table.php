<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tiposrecepcion_id')->references('id')->on('cat_tiposrecepciones')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('tiposremitente_id')->references('id')->on('cat_tiposremitentes')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['tiposrecepcion_id']);
            $table->dropForeign(['tiposremitente_id']);
        });
    }
}
