<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolsUsuarioTablasvaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_Alumnos', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('matricula');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_Asistencias', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('asistio');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_Evaluaciones', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('valoracion');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_Grupos', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('capacidad');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_GruposAlumnos', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('alumno_id');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_GruposHorarios', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('dia');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_GruposProfesores', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('status');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_Niveles', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('descripcion');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
      Schema::table('vale_Profesores', function (Blueprint $table) {
        $table->integer('usuario_id')->unsigned()->after('cedula');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_alumnos', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_asistencias', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_evaluaciones', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_grupos', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_gruposalumnos', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_gruposhorarios', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_gruposprofesores', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_niveles', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
      Schema::table('vale_profesores', function (Blueprint $table) {
        $table->dropForeign(['usuario_id']);
        $table->dropColumn('usuario_id');
      });
    }
}
