<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPreguntasopcionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntasopciones', function (Blueprint $table) {
            $table->foreign('pregunta_id')->references('id')->on('preguntas')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('preguntasopciones', function (Blueprint $table) {
            $table->dropForeign(["pregunta_id"]);
             
        });
    }
}
