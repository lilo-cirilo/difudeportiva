<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampoBienestardispersionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->integer('cuenta_id')->unsigned()->nullable();
            $table->foreign('cuenta_id')->references('id')->on('cuentasbancos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bienestardispersiones', function (Blueprint $table) {
            $table->dropForeign(["cuenta_id"]);
        });
    }
}
