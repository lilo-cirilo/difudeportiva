<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de oficios de aturización de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimCatTiposoficioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_cat_tiposoficio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['nombre']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_cat_tiposoficio');
    }
}
