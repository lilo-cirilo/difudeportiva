<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos que nos proporiconan las áreas de montos invertidos en programas
  ALCANCE: Para estadísticas en sistema Eventos
*/

class CreateInveInversionesprogramasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('inve_inversionesprogramas', function (Blueprint $table) {
      $table->increments('id');
      $table->unsignedInteger('anio_programa_id');
      $table->unsignedInteger('rubro_id');
      $table->unsignedInteger('municipio_id');
      $table->unsignedInteger('numbeneficiarios');
      $table->decimal('inversion',20,6)->nullable();
      $table->timestamps();
      $table->softDeletes();

      $table->unique(['anio_programa_id', 'municipio_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('inve_inversionesprogramas');
  }
}
