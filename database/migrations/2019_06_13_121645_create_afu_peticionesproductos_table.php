<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para registrar los productos que solicita una persona.
    ALCANCE: Apoyos Funcionales
*/

class CreateAfuPeticionesproductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afu_peticionesproductos', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('salidas_producto_id');
            $table->unsignedInteger('beneficioprogsol_benefpersona_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afu_peticionesproductos');
    }
}
