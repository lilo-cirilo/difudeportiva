<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Corrección de nombre y relación de llaves foráneas
    ALCANCE: Alimentarios
*/

class AlterRenameAlimCantidadesdotacionesRequisisionesToAlimCantidadesdotacionesRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Desligamos
        Schema::table('alim_cantidadesdotaciones_requisisiones', function (Blueprint $table) {
            $table->dropForeign('alim_cantdotaciones_requisisiones_prog_req_id_foreign');
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_cantdotaciones_requisisiones_prog_req_id_foreign');
            $table->dropIndex('alim_cantidadesdotaciones_requisisiones_dotacion_id_foreign');
            $table->dropIndex('alim_cantidadesdotaciones_requisisiones_usuario_id_foreign');
        });

        // Renombramos
        Schema::rename('alim_cantidadesdotaciones_requisisiones','alim_cantidadesdotaciones_requisiciones');

        // Relacionamos
        Schema::table('alim_cantidadesdotaciones_requisiciones', function (Blueprint $table) {
            $table->foreign('programacion_requisicion_id', 'alim_cantdotaciones_requisiciones_prog_req_id_foreign')->references('id')->on('alim_programacion_requisiciones');
            $table->foreign('dotacion_id')->references('id')->on('alim_dotaciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Desligamos
        Schema::table('alim_cantidadesdotaciones_requisiciones', function (Blueprint $table) {
            $table->dropForeign('alim_cantdotaciones_requisiciones_prog_req_id_foreign');
            $table->dropForeign(['dotacion_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_cantdotaciones_requisiciones_prog_req_id_foreign');
            $table->dropIndex('alim_cantidadesdotaciones_requisiciones_dotacion_id_foreign');
            $table->dropIndex('alim_cantidadesdotaciones_requisiciones_usuario_id_foreign');
        });

        // Renombramos
        Schema::rename('alim_cantidadesdotaciones_requisiciones','alim_cantidadesdotaciones_requisisiones');

        // Relacionamos
        Schema::table('alim_cantidadesdotaciones_requisisiones', function (Blueprint $table) {
            $table->foreign('programacion_requisicion_id', 'alim_cantdotaciones_requisisiones_prog_req_id_foreign')->references('id')->on('cat_ejercicios');
            $table->foreign('dotacion_id')->references('id')->on('proveedores');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }
}
