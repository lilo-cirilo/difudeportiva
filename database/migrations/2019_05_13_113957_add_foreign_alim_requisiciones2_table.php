<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAlimRequisiciones2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->foreign('licitacion_id')->references('id')->on('alim_licitaciones');
            $table->foreign('tipo_entrega_id')->references('id')->on('alim_cat_tiposentrega');
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['licitacion_id']);
            $table->dropForeign(['tipo_entrega_id']);
            $table->dropForeign(['estado_programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_requisiciones_licitacion_id_foreign');
            $table->dropIndex('alim_requisiciones_tipo_entrega_id_foreign');
            $table->dropIndex('alim_requisiciones_estado_programa_id_foreign');
            $table->dropIndex('alim_requisiciones_usuario_id_foreign');
        });
    }
}
