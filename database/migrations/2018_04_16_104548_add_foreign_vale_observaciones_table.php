<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeObservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vale_observaciones', function (Blueprint $table) {
            $table->foreign('grupos_alumno_id')->references('id')->on('vale_grupos_alumnos')->onUpdate('CASCADE')->onDelete('CASCADE');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vale_observaciones', function (Blueprint $table) {
            $table->dropForeign(["grupos_alumno_id"]);
        });
    }
}
