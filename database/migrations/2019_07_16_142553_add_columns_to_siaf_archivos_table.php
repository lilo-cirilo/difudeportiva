<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSiafArchivosTable extends Migration
{
    /**
     * PETICIÓN: Luis V.
     * MOTIVO: Agregar columnas faltanes para la comprobacion en la tabla archivos
     * ALCANCE: FONDO ROTATORIO
     * 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->string('beneficiario')->after('size');
            $table->string('folio_comprobante', 50)->after('beneficiario');
            $table->date('fecha')->after('folio_comprobante')->nullable()->default(null);
            $table->float('importe')->after('fecha');
            $table->float('isr')->after('importe');
            $table->unsignedInteger('detallespago_id')->after('isr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->dropColumn('beneficiario');
            $table->dropColumn('folio_comprobante');
            $table->dropColumn('fecha');
            $table->dropColumn('importe');
            $table->dropColumn('isr');
            $table->dropColumn('detallespago_id');
        });
    }
}
