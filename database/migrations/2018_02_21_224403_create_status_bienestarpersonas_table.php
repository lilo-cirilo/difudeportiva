<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusBienestarpersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_bienestarpersonas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bienestarpersona_id')->unsigned();
            $table->integer('statusproceso_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->integer('motivo_id')->unsigned()->nullable();
            $table->string('observacion')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_bienestarpersonas');
    }
}
