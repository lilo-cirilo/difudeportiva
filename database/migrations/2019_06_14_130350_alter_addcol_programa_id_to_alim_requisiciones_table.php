<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para relaiconar una requisición a un programa.
    ALCANCE: Alimentarios
*/

class AlterAddcolProgramaIdToAlimRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->unsignedInteger('programa_id')->after('id');
        });

        $registro = DB::table('programas')->first();
        if ($registro)
            DB::table('alim_requisiciones')->update(['programa_id'=>$registro->id]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->dropColumn('programa_id');
        });
    }
}
