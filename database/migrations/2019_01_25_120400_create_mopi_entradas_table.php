<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class CreateMopiEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mopi_entradas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->string('nombre_entrega');
            $table->string('primer_apellido_entrega');
            $table->string('segundo_apellido_entrega');
            $table->float('total');
            $table->date('fecha_recepcion');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('solicitud_id');
            $table->unsignedInteger('area_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mopi_entradas');
    }
}
