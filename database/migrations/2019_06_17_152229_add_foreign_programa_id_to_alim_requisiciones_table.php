<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jhony.
    MOTIVO: Para relaiconar una requisición a un programa.
    ALCANCE: Alimentarios
*/

class AddForeignProgramaIdToAlimRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['programa_id']);
            $table->dropIndex('alim_requisiciones_programa_id_foreign');
        });
    }
}
