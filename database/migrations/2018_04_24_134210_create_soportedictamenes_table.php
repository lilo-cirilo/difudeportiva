<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoportedictamenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soportedictamenes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('respuesta_id')->unsigned();
            $table->integer('dirigidoa_id')->unsigned();
            $table->integer('realizo_id')->unsigned();
            $table->integer('autorizo_id')->unsigned();
            $table->string('leyenda');
            $table->string('detalles');
            $table->string('diagnostico');
            $table->string('nota');
            $table->string('tipo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soportedictamenes');
    }
}
