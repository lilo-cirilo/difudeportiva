<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony
  MOTIVO: Se agrega tabla para ingresar datos de licitaciones de alimentarios
  ALCANCE: Para el sistema de Alimentarios
*/

class CreateAlimDotacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_dotaciones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('programa_id');
            $table->unsignedInteger('subprograma_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['programa_id','subprograma_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_dotaciones');
    }
}
