<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
    PETICIÓN: Manuel.
    MOTIVO: Para relacionar la comprobacion en caja con el estatus y usuario que realiza la modificacion
    ALCANCE: BancaDIF
*/

class AddForeignSiafComprobacionCajaHistorial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_comprobacion_caja_historial', function (Blueprint $table) {
            $table->foreign('comprobacioncaja_id')->references('id')->on('siaf_comprobacion_caja');
            $table->foreign('estado_modulo_id')->references('id')->on('estados_modulos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_comprobacion_caja_historial', function (Blueprint $table) {
            $table->dropForeign(['comprobacioncaja_id']);
            $table->dropForeign(['estado_modulo_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('siaf_comprobacion_caja_historial_comprobacioncaja_id_foreign');
            $table->dropIndex('siaf_comprobacion_caja_historial_estado_modulo_id_foreign');
            $table->dropIndex('siaf_comprobacion_caja_historial_usuario_id_foreign');
        });
    }
}
