<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se agrega la columna recibe_id y folio
  ALCANCE: Para el sistema de recursos materiales
*/
class AddColumnRecibeIdFolioToRecmSalidas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_salidas', function (Blueprint $table) {
            $table->unsignedInteger('recibe_id')->after('requisicion_id');
            $table->string('folio')->nullable()->after('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_salidas',function(Blueprint $table){
            $table->dropColumn('recibe_id');
            $table->dropColumn('folio');
        });
    }
}
