<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignValeGruposhorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('vale_GruposHorarios', function(Blueprint $table){
        $table->foreign('grupo_id')->references('id')->on('vale_Grupos')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('nivel_id')->references('id')->on('vale_Niveles')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('vale_GruposHorarios', function(Blueprint $table){
        $table->dropForeign(['grupo_id']);
        $table->dropForeign(['nivel_id']);
      });
    }
}
