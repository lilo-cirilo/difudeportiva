<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian
  MOTIVO: Para trabajar con el módulo de peso y talla
  ALCANCE: Peso y talla
*/

class CreatePetaBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peta_beneficiarios', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('persona_programa_id');
            $table->unsignedInteger('institucion_usuario_id');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['persona_programa_id','institucion_usuario_id'],'peta_beneficiarios_persona_prog_id_institucion_usuario_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peta_beneficiarios');
    }
}
