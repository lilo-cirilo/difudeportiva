<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Orlando Herzain
  MOTIVO: Para captura de datos
  ALCANCE: Para Atención Ciudadana
*/

class AlterAddcolComprimisoToSolicitudesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('solicitudes', function (Blueprint $table) {
      $table->boolean('compromiso')->after('observaciones');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('solicitudes', function (Blueprint $table) {
      $table->dropColumn('compromiso');
    });
  }
}
