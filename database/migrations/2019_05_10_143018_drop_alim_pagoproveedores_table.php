<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAlimPagoproveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_pagoproveedores', function (Blueprint $table) {
            $table->dropForeign(['alimentarios_requisicion_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_pagoproveedores_alimentarios_requisicion_id_foreign');
            $table->dropIndex('alim_pagoproveedores_programa_id_foreign');
            $table->dropIndex('alim_pagoproveedores_usuario_id_foreign');
        });

        Schema::dropIfExists('alim_pagoproveedores');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('alim_pagoproveedores', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('alimentarios_requisicion_id');
            $table->unsignedInteger('programa_id');
            $table->unsignedInteger('numero_factura');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('alim_pagoproveedores', function (Blueprint $table) {
      $table->foreign('alimentarios_requisicion_id')->references('id')->on('alim_alimentarios_requisiciones')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('programa_id')->references('id')->on('programas')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
    }
}
