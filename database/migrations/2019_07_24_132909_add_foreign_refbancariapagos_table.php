<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Juan.
    MOTIVO: Para ingresar datos extra de cuotas de recuperación
    ALCANCE: Alimentarios
*/

class AddForeignRefbancariapagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_refbancariapagos', function (Blueprint $table) {
            $table->foreign('referenciabancaria_id')->references('id')->on('alim_referenciasbancarias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_refbancariapagos', function (Blueprint $table) {
            $table->dropForeign(['referenciabancaria_id']);
            $table->dropIndex('alim_refbancariapagos_referenciabancaria_id_foreign');
        });
    }
}
