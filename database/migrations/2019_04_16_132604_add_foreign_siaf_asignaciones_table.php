<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafAsignacionesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_asignaciones', function (Blueprint $table) {
      $table->foreign('fondorotatorioarea_id')->references('id')->on('siaf_fondorotatorioareas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_asignaciones', function (Blueprint $table) {
      $table->dropForeign(['fondorotatorioarea_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_asignaciones_fondorotatorioarea_id_foreign');
      $table->dropIndex('siaf_asignaciones_usuario_id_foreign');
    });
  }
}
