<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class CreateAlimEntregasproveedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_entregasproveedor', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('recibo_id');
            $table->date('fecha_entrega');
            $table->unsignedInteger('recibe_comite_id')->nullable();
            $table->unsignedInteger('datocarga_id');
            $table->unsignedInteger('estado_programa_id');
            $table->string('observaciones')->nullable();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_entregasproveedor');
    }
}
