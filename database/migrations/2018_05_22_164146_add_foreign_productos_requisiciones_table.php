<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignProductosRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('productos_requisiciones', function (Blueprint $table) {
            $table->foreign('requisicion_id')->references('id')->on('requisiciones')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('producto_id')->references('id')->on('cat_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('productos_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['requisicion_id']);
            $table->dropForeign(['producto_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
