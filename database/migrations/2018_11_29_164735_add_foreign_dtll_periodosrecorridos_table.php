<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Brian y Jhony
  MOTIVO: Reestructura del funcionamiento del sistema
  ALCANCE: DIF te lleva
*/

class AddForeignDtllPeriodosrecorridosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_periodosrecorridos', function (Blueprint $table) {
      $table->foreign('recorrido_id')->references('id')->on('recorridos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_periodosrecorridos', function (Blueprint $table) {
      $table->dropForeign(['recorrido_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('dtll_periodosrecorridos_recorrido_id_foreign');
      $table->dropIndex('dtll_periodosrecorridos_usuario_id_foreign');
    });
  }
}
