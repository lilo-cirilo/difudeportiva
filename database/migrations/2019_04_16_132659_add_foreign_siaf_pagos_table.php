<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_pagos', function (Blueprint $table) {
      $table->foreign('solicitante_empleado_id')->references('id')->on('empleados');
      $table->foreign('metodopago_id')->references('id')->on('siaf_cat_metodospago');
      $table->foreign('tipopago_id')->references('id')->on('siaf_cat_tipospago');
      $table->foreign('autorizo_empleado_id')->references('id')->on('empleados');
      $table->foreign('vobo_empleado_id')->references('id')->on('empleados');
      $table->foreign('programa_id')->references('id')->on('programas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_pagos', function (Blueprint $table) {
      $table->dropForeign(['solicitante_empleado_id']);
      $table->dropForeign(['metodopago_id']);
      $table->dropForeign(['tipopago_id']);
      $table->dropForeign(['autorizo_empleado_id']);
      $table->dropForeign(['vobo_empleado_id']);
      $table->dropForeign(['programa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_pagos_solicitante_empleado_id_foreign');
      $table->dropIndex('siaf_pagos_metodopago_id_foreign');
      $table->dropIndex('siaf_pagos_tipopago_id_foreign');
      $table->dropIndex('siaf_pagos_autorizo_empleado_id_foreign');
      $table->dropIndex('siaf_pagos_vobo_empleado_id_foreign');
      $table->dropIndex('siaf_pagos_programa_id_foreign');
      $table->dropIndex('siaf_pagos_usuario_id_foreign');
    });
  }
}
