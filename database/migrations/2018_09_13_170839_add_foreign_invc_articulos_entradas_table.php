<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignInvcArticulosEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invc_articulos_entradas', function(Blueprint $table){
            $table->foreign('entrada_id')->references('id')->on('invc_entradas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('articulo_id')->references('id')->on('invc_categorias_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('bodega_id')->references('id')->on('cat_bodegas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invc_articulos_entradas', function(Blueprint $table){
            $table->dropForeign(['entrada_id']);
            $table->dropForeign(['articulo_id']);
            $table->dropForeign(['usuario_id']);
        });
    }
}
