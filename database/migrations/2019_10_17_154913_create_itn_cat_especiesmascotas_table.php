<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de las mascotas de las personas que asisten a una caravana
    ALCANCE: Itinerantes
*/

class CreateItnCatEspeciesmascotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itn_cat_especiesmascotas', function (Blueprint $table) {
            $table->increments('id');
						$table->string('nombre',20);

						$table->timestamps();
						$table->softDeletes();

						$table->unique(['nombre']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itn_cat_especiesmascotas');
    }
}
