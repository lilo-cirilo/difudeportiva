<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar la localidad
  ALCANCE: DIF te lleva
*/

class AlterUniqueNumPasajeroPasajeroIdDtllServiciosadquiridosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('dtll_serviciosadquiridos', function (Blueprint $table) {
      $table->unique(['pasajero_id','numPasajero','deleted_at'],'dtll_servadq_pasajeroid_numpas_delat'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('dtll_serviciosadquiridos', function (Blueprint $table) {
      $table->dropUnique('dtll_servadq_pasajeroid_numpas_delat');
    });
  }
}
