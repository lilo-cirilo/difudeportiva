<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAreasResponsableIdToRecmRecquisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			Schema::table('recm_requisicions', function (Blueprint $table) {
				$table->foreign('areas_responsable_id')->references('id')->on('areas_responsables');
				
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
			Schema::table('recm_requisicions', function (Blueprint $table) {
				$table->dropForeign(['areas_responsable_id']);
				$table->dropIndex('recm_requisicions_areas_responsable_id_foreign');
			});
    }
}
