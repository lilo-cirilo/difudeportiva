<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Brenda.
    MOTIVO: Para capturar datos que van relacionados con el pedido y entrega de un proveedor.
    ALCANCE: Alimentarios
*/

class CreateAlimDatoscargaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alim_datoscarga', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('vehiculoproveedor_id');
            $table->unsignedInteger('choferproveedor_id');
            $table->string('ruta');
            $table->date('fecha');
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alim_datoscarga');
    }
}
