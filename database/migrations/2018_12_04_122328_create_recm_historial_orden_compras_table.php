<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmHistorialOrdenComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_historial_orden_compras', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('orden_compra_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_historial_orden_compras');
    }
}
