<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Alex.
    MOTIVO: Para capturar datos que tienen que ver con los presidentes municipales.
    ALCANCE: General
*/

class AddForeignPresidentesmunicipalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presidentesmunicipales', function (Blueprint $table) {
            $table->foreign('cargo_id')->references('id')->on('cat_cargos');
            $table->foreign('metodo_eleccion_id')->references('id')->on('cat_metodoseleccion');
            $table->foreign('municipio_id')->references('id')->on('cat_municipios');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('presidentesmunicipales', function (Blueprint $table) {
            $table->dropForeign(['cargo_id']);
            $table->dropForeign(['metodo_eleccion_id']);
            $table->dropForeign(['municipio_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('presidentesmunicipales_cargo_id_foreign');
            $table->dropIndex('presidentesmunicipales_metodo_eleccion_id_foreign');
            $table->dropIndex('presidentesmunicipales_municipio_id_foreign');
            $table->dropIndex('presidentesmunicipales_usuario_id_foreign');
        });
    }
}
