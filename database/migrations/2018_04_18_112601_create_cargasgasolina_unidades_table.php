<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargasgasolinaUnidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargasgasolina_unidades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estadounidad_id')->unsigned();
            $table->integer('cargagasolina_id')->unsigned();
            $table->integer('litros');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargasgasolina_unidades');
    }
}
