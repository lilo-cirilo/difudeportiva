<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecmProductoRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recm_producto_requisicions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('requisicion_id');
            $table->unsignedInteger('presentacion_producto_id');
            $table->unsignedInteger('status_id');
            $table->unsignedInteger('cantidad');
            $table->float('precio');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recm_producto_requisicions');
    }
}
