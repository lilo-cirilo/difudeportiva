<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignCatEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_equipos', function(Blueprint $table){
          $table->foreign('marca_id')->references('id')->on('cat_marcasequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
          $table->foreign('modelo_id')->references('id')->on('cat_modelosequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
          $table->foreign('tipoequipo_id')->references('id')->on('cat_tiposequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
          $table->foreign('resguardo_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_equipos', function(Blueprint $table){
          $table->dropForeign(['marca_id']);
          $table->dropForeign(['modelo_id']);
          $table->dropForeign(['tipoequipo_id']);
          $table->dropForeign(['resguardo_id']);

          $table->dropIndex('cat_equipos_marca_id_foreign');
          $table->dropIndex('cat_equipos_modelo_id_foreign');
          $table->dropIndex('cat_equipos_tipoequipo_id_foreign');
          $table->dropIndex('cat_equipos_resguardo_id_foreign');
        });
    }
}
