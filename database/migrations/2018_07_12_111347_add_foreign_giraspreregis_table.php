<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table ('giraspreregis', function(Blueprint $table){
        $table->foreign('localidad_id')->references('id')->on('cat_localidades')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('municipio_id')->references('id')->on('cat_municipios')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('giraspreregis', function(Blueprint $table){
        $table->dropForeign(['localidad_id']);
        $table->dropForeign(['municipio_id']);
        $table->dropForeign(['usuario_id']);
      });
    }
}
