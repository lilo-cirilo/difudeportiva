<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para poder almacenar datos referentes al sistema de Monte de piedad
  ALCANCE:Monte de piedad
*/

class AddForeignMopiEntradas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mopi_entradas', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('solicitud_id')->references('id')->on('mopi_solicitudes')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mopi_entradas', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['area_id']);
            $table->dropForeign(['solicitud_id']);
            $table->dropIndex('mopi_entradas_area_id_foreign');
            $table->dropIndex('mopi_entradas_usuario_id_foreign');
            $table->dropIndex('mopi_entradas_solicitud_id_foreign');
        });
    }
}
