<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Campo extra para guardar el beneficiario en archivos.
    ALCANCE: SIAF
*/

class ChangeColumnsToBeNullableOnSiafArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->string('beneficiario')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_archivos', function (Blueprint $table) {
            $table->string('beneficiario')->nullable(false)->change();
        });
    }
}
