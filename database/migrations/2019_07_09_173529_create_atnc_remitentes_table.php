<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Migue.
    MOTIVO: Para quitar la relación con la tabla personas y ahora ingresar los datos de remitente en la tabla atnc_remitentes
    ALCANCE: Atención ciudadana
*/

class CreateAtncRemitentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atnc_remitentes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',40);
            $table->string('primer_apellido',30);
            $table->string('segundo_apellido',30)->nullable();
            $table->unsignedInteger('cargo_id')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('email',100)->nullable();
            $table->string('telefono',30)->nullable();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atnc_remitentes');
    }
}
