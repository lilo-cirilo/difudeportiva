<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToRecmCatProveedorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->text('giro')->after('telefono');
            $table->unsignedInteger('estado_id')->after('giro')->default(20);
            $table->date('vigencia')->nullable()->default(null)->after('estado_id');
            $table->foreign('estado_id')->references('id')->on('cat_entidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_cat_proveedors', function (Blueprint $table) {
            $table->dropForeign(['estado_id']);
            $table->dropColumn('giro');
            $table->dropColumn('estado_id');
            $table->dropColumn('vigencia');
        });
    }
}
