<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmEntradasProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_entradas_productos', function (Blueprint $table) {
            $table->foreign('entrada_id')->references('id')->on('recm_entradas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('presentacion_producto_id')->references('id')->on('recm_presentacion_productos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_entradas_productos', function (Blueprint $table) {
            $table->dropForeign(['entrada_id']);
            $table->dropForeign(['presentacion_producto_id']);
            $table->dropIndex('recm_entradas_productos_presentacion_producto_id_foreign');
            $table->dropIndex('recm_entradas_productos_entrada_id_foreign');
            
        });
    }
}
