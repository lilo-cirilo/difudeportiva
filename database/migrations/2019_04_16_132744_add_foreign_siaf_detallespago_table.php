<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class AddForeignSiafDetallespagoTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('siaf_detallespago', function (Blueprint $table) {
      $table->foreign('pago_id')->references('id')->on('siaf_pagos');
      $table->foreign('partida_id')->references('id')->on('recm_cat_partidas');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('siaf_detallespago', function (Blueprint $table) {
      $table->dropForeign(['pago_id']);
      $table->dropForeign(['partida_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('siaf_detallespago_pago_id_foreign');
      $table->dropIndex('siaf_detallespago_partida_id_foreign');
      $table->dropIndex('siaf_detallespago_usuario_id_foreign');
    });
  }
}
