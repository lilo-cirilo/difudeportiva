<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Maai.
    MOTIVO: Se agrega columna para indicar qué localidad es oficial. Oficial: Aparece en el catálogo inegi, No oficial: Agregada según las necesidades del sistema
    ALCANCE: General
*/

class AlterAddcolOficialToCatLocalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cat_localidades', function (Blueprint $table) {
            $table->boolean('oficial')->after('cve_localidad');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cat_localidades', function (Blueprint $table) {
            $table->dropColumn('oficial');
        });
    }
}
