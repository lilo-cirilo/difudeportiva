<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToProgramasResponsablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('programas_responsables', function (Blueprint $table) {
            $table->foreign('programa_id')->references('id')->on('programas')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('usuario_id')->references('id')->on('usuarios')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('areas_responsable_id')->references('id')->on('areas_responsables')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('programas_responsables', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['programa_id']);
            $table->dropForeign(['areas_responsable_id']);
        });
    }
}
