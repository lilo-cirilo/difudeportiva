<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de las mascotas de las personas que asisten a una caravana
    ALCANCE: Itinerantes
*/

class CreateItnMascotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itn_mascotas', function (Blueprint $table) {
            $table->increments('id');

						$table->unsignedInteger('beneficiopersona_caravana_id');
						$table->string('nombre',20);
						$table->smallInteger('edad')->unsigned();
						$table->unsignedInteger('especie_id');
            
						$table->unsignedInteger('usuario_id');
            $table->timestamps();
						$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itn_mascotas');
    }
}
