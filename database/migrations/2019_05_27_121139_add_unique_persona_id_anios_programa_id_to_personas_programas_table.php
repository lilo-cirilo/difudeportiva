<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Se agregan restricciones unique para no repetir información
  ALCANCE: Todos los sistemas
*/

class AddUniquePersonaIdAniosProgramaIdToPersonasProgramasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('personas_programas', function (Blueprint $table) {
            $table->unique(['persona_id','anios_programa_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('personas_programas', function (Blueprint $table) {
            $table->dropUnique(['persona_id','anios_programa_id']);
        });
    }
}
