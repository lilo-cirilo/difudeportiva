<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Jhony, Alex y Miguel
  MOTIVO: Para ingresar solicitudes
  ALCANCE: General
*/

class CreateBeneficiosprogramasSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiosprogramas_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('solicitud_id')->unsigned();
            $table->integer('beneficioprograma_id')->unsigned();
            $table->smallInteger('cantidad')->unsigned();
            $table->string('folio',20);
            
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiosprogramas_solicitudes');
    }
}
