<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignInventarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventario', function (Blueprint $table) {
            $table->foreign('equipo_id')->references('id')->on('tiposequipos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('dispositivo_id')->references('id')->on('dispositivos')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('resguardo_id')->references('id')->on('empleados')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventario', function (Blueprint $table) {
            $table->dropForeign(["equipo_id"]);
            $table->dropForeign(["dispositivo_id"]);
            $table->dropForeign(["resguardo_id"]);
        });
    }
}
