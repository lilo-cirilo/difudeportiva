<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueIndexOnSiafViaticoestatalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_viaticoestatal', function (Blueprint $table) {
            $table->dropForeign(['viatico_id']);
            $table->dropUnique('siaf_viaticoestatal_viatico_id_unique');
            $table->foreign('viatico_id')->references('id')->on('siaf_comisionesoficiales');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_viaticoestatal', function (Blueprint $table) {
            $table->dropForeign(['viatico_id']);
            $table->foreign('viatico_id')->references('id')->on('siaf_comisionesoficiales');
        });
    }
}
