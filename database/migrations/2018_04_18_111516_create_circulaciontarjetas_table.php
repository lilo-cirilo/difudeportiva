<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCirculaciontarjetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('circulaciontarjetas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unidad_id')->unsigned();
            $table->string('clase');
            $table->string('tipo');
            $table->string('oficina_expedidora');
            $table->string('origen_vehiculo');
            $table->string('clave_vehicular');
            $table->date('vigencia');
            $table->string('tipo_servicio');
            $table->integer('cilindro');
            $table->string('numero_motor');
            $table->integer('combustible');
            $table->string('numero_deatificacion');
            $table->string('placa');
            $table->string('numero_serie');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('circulaciontarjetas');
    }
}
