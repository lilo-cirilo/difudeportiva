<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignOrevEventosParticipantesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('orev_eventos_participantes', function (Blueprint $table) {
      $table->foreign('evento_id')->references('id')->on('orev_eventos')->onUpdate('CASCADE')->onDelete('CASCADE');
      $table->foreign('participante_id')->references('id')->on('orev_participantes')->onUpdate('CASCADE')->onDelete('CASCADE');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('orev_eventos_participantes', function (Blueprint $table) {
      $table->dropForeign(['evento_id']);
      $table->dropForeign(['participante_id']);

      $table->dropIndex('orev_eventos_participantes_evento_id_foreign');
      $table->dropIndex('orev_eventos_participantes_participante_id_foreign');
    });
  }
}
