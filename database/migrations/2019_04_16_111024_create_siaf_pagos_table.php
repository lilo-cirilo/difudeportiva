<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Luis Miguel
  MOTIVO: Se agrega tabla para ingresar datos para el sistema SIAF
  ALCANCE: Para el sistema SIAF
*/

class CreateSiafPagosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('siaf_pagos', function (Blueprint $table) {
      $table->increments('id');

      $table->date('fecha_elaboracion');
      $table->unsignedInteger('solicitante_empleado_id');
      $table->decimal('monto_total',20,6)->nullable();
      $table->string('descripcion')->nullable();
      $table->unsignedInteger('metodopago_id');
      $table->unsignedInteger('tipopago_id');
      $table->unsignedInteger('autorizo_empleado_id');
      $table->unsignedInteger('vobo_empleado_id');
      $table->unsignedInteger('programa_id');
      
      $table->unsignedInteger('usuario_id');
      $table->timestamps();
      $table->softDeletes();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('siaf_pagos');
  }
}
