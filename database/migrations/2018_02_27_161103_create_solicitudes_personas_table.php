<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesPersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes_personas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('programas_solicitud_id')->unsigned();
            $table->integer('persona_id')->unsigned();
            $table->boolean('entregado');
            $table->string('observacionentrega');
            $table->boolean('evaluado');
            $table->string('observacionevaluado');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_personas');
    }
}
