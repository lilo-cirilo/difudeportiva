<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Para saber que tipo de requisicion es (material o de mantenimiento)
  ALCANCE: Para el sistema de recursos materiales
*/

class AddColumnTipoToRecmRequisicions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->unsignedInteger('tipo')->nullable()->after('partida_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions',function(Blueprint $table){
            $table->dropColumn('tipo');
        });
    }
}
