<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignCatdependenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cat_dependencias', function(Blueprint $table){
        $table->dropForeign(['entidad_id']);
        $table->dropForeign(['usuario_id']);
        //$table->dropColumn('entidad_id');
        //$table->dropColumn('usuario_id');
        $table->dropIndex('cat_dependencias_entidad_id_foreign');
        $table->dropIndex('cat_dependencias_usuario_id_foreign');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('cat_dependencias', function(Blueprint $table){
        //$table->unsignedInteger('entidad_id')->after('tipoinstitucion_id');
        //$table->unsignedInteger('usuario_id')->after('entidad_id');
        $table->foreign('entidad_id')->references('id')->on('cat_entidades');
        $table->foreign('usuario_id')->references('id')->on('usuarios');

      });
    }
}
