<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAfuncionalesSillasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('afuncionales_sillas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('afuncionalespersona_id')->unsigned();
            $table->float('altura');
            $table->float('peso');
            $table->string('terreno');
            $table->string('tiposilla');
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('afuncionales_sillas');
    }
}
