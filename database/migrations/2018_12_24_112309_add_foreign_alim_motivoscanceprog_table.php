<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimMotivoscanceprogTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_motivoscanceprog', function (Blueprint $table) {
      $table->foreign('programacion_id')->references('id')->on('alim_programacion');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_motivoscanceprog', function (Blueprint $table) {
      $table->dropForeign(['programacion_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_motivoscanceprog_programacion_id_foreign');
      $table->dropIndex('alim_motivoscanceprog_usuario_id_foreign');
    });
  }
}
