<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignSolicitudesmodifdatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('solicitudesmodifdatos', function(Blueprint $table){
        $table->foreign('persona_id')->references('id')->on('personas')->onUpdate('CASCADE')->onDelete('CASCADE');
        $table->foreign('status_solicitud_id')->references('id')->on('cred_impresion')->onUpdate('CASCADE')->onDelete('CASCADE');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('solicitudesmodifdatos', function(Blueprint $table){
        $table->dropForeign(['persona_id']);
        $table->dropForeign(['status_solicitud_id']);
      });
    }
}
