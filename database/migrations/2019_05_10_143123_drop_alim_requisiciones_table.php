<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropAlimRequisicionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->dropForeign(['estado_programa_id']);
            $table->dropForeign(['tiporequisicion_id']);
            $table->dropForeign(['usuario_id']);
        });

        Schema::dropIfExists('alim_requisiciones');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('alim_requisiciones', function (Blueprint $table) {
            $table->increments('id');

            $table->string('oficio_requisicion');
            $table->timestamp('fecha_oficio');
            $table->string('titulo',50);
            $table->tinyInteger('bimestre')->unsigned();
            $table->tinyInteger('entrega')->unsigned();
            $table->unsignedInteger('estado_programa_id');
            $table->unsignedInteger('tiporequisicion_id');
            $table->unsignedInteger('licitacion_id');
            $table->string('observacion')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['oficio_requisicion', 'deleted_at']);
            $table->unique(['oficio_requisicion', 'bimestre', 'entrega', 'deleted_at'],'alim_requisiciones_ofireq_bim_entrega_del_at_unique'); // Especificamos un nombre porque eloquent crea un nombre muy largo y no lo acepta mysql
        });

        Schema::table('alim_requisiciones', function (Blueprint $table) {
            $table->foreign('estado_programa_id')->references('id')->on('estados_programas');
            $table->foreign('tiporequisicion_id')->references('id')->on('alim_cat_tiposrequisicion');
            // $table->foreign('licitacion_id')->references('id')->on('alim_licitaciones');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }
}
