<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para uso de datos en la tabla cat_escuelas
  ALCANCE: General
*/

class CreateCatTurnosescuelaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_turnosescuela', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('cat_turnosescuela')->insert([  //Por el momento desconosco los valores que toma cada turno
            ['nombre' => 'MATUTINO', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '2', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '3', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '4', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '5', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '6', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '7', 'created_at'=>NOW(), 'updated_at'=>NOW()],
            ['nombre' => '8', 'created_at'=>NOW(), 'updated_at'=>NOW()]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_turnosescuela');
    }
}
