<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignRecmRequisicionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //recm_requisicions
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('area_id')->references('id')->on('cat_areas')->onUpdate('CASCADE')->onDelete('CASCADE');
          });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recm_requisicions', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['area_id']);
            $table->dropIndex('recm_requisicions_usuario_id_foreign');
            $table->dropIndex('recm_requisicions_area_id_foreign');
        });
        
    }
}
