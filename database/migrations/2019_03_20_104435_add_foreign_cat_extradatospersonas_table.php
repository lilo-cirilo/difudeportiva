<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Maai
  MOTIVO: Para poder almacenar datos extra a la tabla personas
  ALCANCE: Para todos los sitemas
*/

class AddForeignCatExtradatospersonasTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('extradatospersonas', function (Blueprint $table) {
      $table->foreign('persona_id')->references('id')->on('personas');            
      $table->foreign('tiporopa_id')->references('id')->on('cat_tiposropa');            
      $table->foreign('usuario_id')->references('id')->on('usuarios');            
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('extradatospersonas', function (Blueprint $table) {
      $table->dropForeign(['persona_id']);
      $table->dropForeign(['tiporopa_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('extradatospersonas_persona_id_foreign');
      $table->dropIndex('extradatospersonas_tiporopa_id_foreign');
      $table->dropIndex('extradatospersonas_usuario_id_foreign');
    });
  }
}
