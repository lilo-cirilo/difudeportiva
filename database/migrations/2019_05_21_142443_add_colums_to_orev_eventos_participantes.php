<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsToOrevEventosParticipantes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orev_eventos_participantes', function (Blueprint $table) {
            $table->unsignedInteger('inscrito')->after('persona_id')->nullable();
            $table->unsignedInteger('evaluado')->after('persona_id')->nullable();
            $table->unsignedInteger('constancia')->after('persona_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orev_eventos_participantes', function (Blueprint $table) {
            $table->dropColumn('inscrito');
            $table->dropColumn('evaluado');
            $table->dropColumn('constancia');
        });
    }
}
