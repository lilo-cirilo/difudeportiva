<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParadasRutasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paradas_rutas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ruta_id')->unsigned();
            $table->integer('parada_id')->unsigned();
            $table->string('tipo');
            $table->integer('numero_estacion');
            $table->char('tipoparada',1);
            $table->string('icono',50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paradas_rutas');
    }
}
