<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolsGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('giraspreregis', function (Blueprint $table) {
        $table->integer('evento_id')->after('id')->unsigned();
        $table->string('telefono',15)->after('curp')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('giraspreregis',function(Blueprint $table){
        $table->dropColumn('telefono');
        $table->dropColumn('evento_id');
      });
    }
}
