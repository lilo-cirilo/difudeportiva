<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSdocObservacionesAdmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_observaciones_adm', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('seguimiento_id');
            $table->string('observaciones', 512)->nullable()->default(null);
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
        
        Schema::table('sdoc_observaciones_adm', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('seguimiento_id')->references('id')->on('sdoc_seguimiento_documentos');
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   
        Schema::table('sdoc_observaciones_adm', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['seguimiento_id']);

            $table->dropIndex('sdoc_observaciones_adm_usuario_id_foreign');
            $table->dropIndex('sdoc_observaciones_adm_seguimiento_id_foreign');
        });

        Schema::dropIfExists('sdoc_observaciones_adm');
    }
}
