<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Manuel
  MOTIVO: Se elimina la columna de fecha_plazo ya no es necesario
  ALCANCE: Para el sistema de recursos materiales
*/

class DeleteColumnFechaPlazoToRecmRequisicion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->dropColumn(['fecha_plazo']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->date('fecha_plazo')->after('justificacion')->nullable();
        });

        DB::table('recm_requisicions')->update(['fecha_plazo'=>'1900-01-01']);

        Schema::Table('recm_requisicions', function(Blueprint $table){
            $table->date('fecha_plazo')->change();
        });
    }
}
