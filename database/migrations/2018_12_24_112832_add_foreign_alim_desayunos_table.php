<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Juan
  MOTIVO: Petición a la Unidad de Informática por parte de dirección para dibujar la ubicación de Cocinas, sujetos  y desayunos
  ALCANCE: Alimentarios (Cocinas, sujetos  y desayunos)
*/

class AddForeignAlimDesayunosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('alim_desayunos', function (Blueprint $table) {
      $table->foreign('alimentario_id')->references('id')->on('alim_alimentarios');
      $table->foreign('escuela_id')->references('id')->on('alim_cat_escuelas');
      $table->foreign('tipodistribucion_id')->references('id')->on('alim_cat_tiposdistribucion');
      $table->foreign('usuario_id')->references('id')->on('usuarios');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('alim_desayunos', function (Blueprint $table) {
      $table->dropForeign(['alimentario_id']);
      $table->dropForeign(['escuela_id']);
      $table->dropForeign(['tipodistribucion_id']);
      $table->dropForeign(['usuario_id']);
      $table->dropIndex('alim_desayunos_alimentario_id_foreign');
      $table->dropIndex('alim_desayunos_escuela_id_foreign');
      $table->dropIndex('alim_desayunos_tipodistribucion_id_foreign');
      $table->dropIndex('alim_desayunos_usuario_id_foreign');
    });
  }
}
