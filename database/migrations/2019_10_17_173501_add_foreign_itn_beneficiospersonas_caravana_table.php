<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar datos de las personas que asisten a una caravana
    ALCANCE: Itinerantes
*/

class AddForeignItnBeneficiospersonasCaravanaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('itn_beneficiospersonas_caravana', function (Blueprint $table) {
            $table->foreign('caravana_id')->references('id')->on('itn_caravana');	
            $table->foreign('beneficioprog_persona_id')->references('id')->on('beneficiosprog_personas');	
            $table->foreign('usuario_id')->references('id')->on('usuarios');	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('itn_beneficiospersonas_caravana', function (Blueprint $table) {
            $table->dropForeign(['caravana_id']);
            $table->dropForeign(['beneficioprog_persona_id']);
            $table->dropForeign(['usuario_id']);
						$table->dropIndex('itn_beneficiospersonas_caravana_caravana_id_foreign');
						$table->dropIndex('itn_beneficiospersonas_caravana_beneficioprog_persona_id_foreign');
						$table->dropIndex('itn_beneficiospersonas_caravana_usuario_id_foreign');
        });
    }
}
