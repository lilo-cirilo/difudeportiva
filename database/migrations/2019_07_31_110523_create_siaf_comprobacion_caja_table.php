<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Manuel.
    MOTIVO: Para registrar las comprobaciones que se van a visualizar y pagar en caja
    ALCANCE: BancaDIF
*/

class CreateSiafComprobacionCajaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siaf_comprobacion_caja', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio',50);
            $table->unsignedInteger('comprobacion_id');
            $table->decimal('monto', 8, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siaf_comprobacion_caja');
    }
}
