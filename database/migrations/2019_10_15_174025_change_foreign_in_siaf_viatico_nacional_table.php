<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Beto.
    MOTIVO: Modificación de la referencia de las llaves foraneas en la tabla siaf_viaticonacional.
    ALCANCE: SIAF
*/

class ChangeForeignInSiafViaticoNacionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('siaf_viaticonacional', function (Blueprint $table) {
            //Elimar la llave foranea
            $table->dropForeign(['origenentidad_id']);
            $table->dropIndex('siaf_viaticonacional_origenentidad_id_foreign');
            $table->dropForeign(['destinoentidad_id']);
            $table->dropIndex('siaf_viaticonacional_destinoentidad_id_foreign');

            $table->foreign('origenentidad_id')->references('id')->on('cat_entidades');
            $table->foreign('destinoentidad_id')->references('id')->on('cat_entidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('siaf_viaticonacional', function (Blueprint $table) {
            $table->dropForeign(['origenentidad_id']);
            $table->dropIndex('siaf_viaticonacional_origenentidad_id_foreign');
            $table->dropForeign(['destinoentidad_id']);
            $table->dropIndex('siaf_viaticonacional_destinoentidad_id_foreign');
            $table->foreign('origenentidad_id')->references('id')->on('cat_municipios');
            $table->foreign('destinoentidad_id')->references('id')->on('cat_municipios');
        });
    }
}
