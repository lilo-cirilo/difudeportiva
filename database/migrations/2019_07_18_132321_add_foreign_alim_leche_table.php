<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Jony.
    MOTIVO: Para ingresar la información de beneficiarios de leche
    ALCANCE: Alimentarios
*/

class AddForeignAlimLecheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alim_leche', function (Blueprint $table) {
            $table->foreign('alimentario_id')->references('id')->on('alim_alimentarios');
            $table->foreign('caic_id')->references('id')->on('cat_caics');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alim_leche', function (Blueprint $table) {
            $table->dropForeign(['alimentario_id']);
            $table->dropForeign(['caic_id']);
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('alim_leche_alimentario_id_foreign');
            $table->dropIndex('alim_leche_caic_id_foreign');
            $table->dropIndex('alim_leche_usuario_id_foreign');
        });
    }
}
