<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
     * PETICIÓN: Beto.
     * MOTIVO: Tabla intermedia de los estatus con los modulos
     * ALCANCE: Universal
*/

class CreateAddForeignToEstadosModulosTable extends Migration
{
    /**
     *
     * @return void
     */
     public function up()
     {
        Schema::table('estados_modulos', function (Blueprint $table) {
            $table->foreign('estado_id')->references('id')->on('cat_estados');
            $table->foreign('modulo_id')->references('id')->on('cat_modulos');
            $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('estados_modulos', function (Blueprint $table) {
            $table->dropForeign(['estado_id']);
            $table->dropForeign(['modulo_id']);
            $table->dropForeign(['usuario_id']);
            // $table->dropIndex('cat_estados_programas_estado_id_foreign');  //Por el index unique este index ya no se crea
            $table->dropIndex('estados_modulos_modulo_id_foreign');
            $table->dropIndex('estados_modulos_usuario_id_foreign');
        });
    }
}
