<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('diagnosticos', function (Blueprint $table) {
            $table->foreign('consulta_id')->references('id')->on('consultas')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('cie10_id')->references('id')->on('cat_cie10')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('discapacidad_id')->references('id')->on('cat_discapacidades')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('suive_id')->references('id')->on('cat_suive')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('clasificacion_id')->references('id')->on('cat_clasificacionenfermedades')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnosticos', function (Blueprint $table) {
            $table->dropForeign(["consulta_id"]);
            $table->dropForeign(["cie10_id"]);
            $table->dropForeign(["discapacidad_id"]);
            $table->dropForeign(["suive_id"]);
            $table->dropForeign(["clasificacion_id"]);
        });
    }
}
