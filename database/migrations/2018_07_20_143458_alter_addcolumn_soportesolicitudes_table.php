<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAddcolumnSoportesolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soportesolicitudes', function(Blueprint $table)
        {
          $table->integer('equipo_id')->unsigned()->after('servicio_id');
          $table->foreign('equipo_id')->references('id')->on('cat_equipos')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soportesolicitudes',function(Blueprint $table){
          $table->dropForeign(['equipo_id']);
          $table->dropColumn('equipo_id');
        });
    }
}
