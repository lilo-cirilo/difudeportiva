<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiraspreregisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giraspreregis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',40);
            $table->string('primer_apellido',30);
            $table->string('segundo_apellido',30)->nullable();
            $table->date('fecha_nacimiento');
            $table->string('curp',18)->nullable();
            $table->string('calle');
            $table->string('numero_exterior',10);
            $table->string('numero_interior',10)->nullable();
            $table->string('colonia',100);
            $table->integer('localidad_id')->unsigned();
            $table->integer('municipio_id')->unsigned();
            $table->string('folio',20)->nullable();
            $table->string('fotografia')->nullable();
            $table->date('fecha_registro');
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('giraspreregis');
    }
}
