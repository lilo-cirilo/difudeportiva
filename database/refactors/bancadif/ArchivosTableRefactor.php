<?php
use \Excel as Excel;

use Modules\BancaDIF\Entities\Archivo;
use Modules\BancaDIF\Entities\Proveedor;

class ArchivosTableRefactor
{
    /**
     * Run the database refactoring.
     *
     * @return void
     */
    public function run()
    {

        Archivo::get()->each(function ($archivo) {
            \Log::debug($archivo);
            dd($archivo);
            $proveedor = Proveedor::firstOrCreate(
                [
                    'name' => 'Flight 10'
                ], 
                [
                    'delayed' => 1
                ]);
            $archivo = $proveedor->id;
            $archivo->save();
        });
    }
}