<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RegionesSeeder::class);
        $this->call(DistritosSeeder::class);
        $this->call(MunicipiosSeeder::class);
        $this->call(LocalidadesSeeder::class);
        $this->call(EjerciciosSeeder::class);
        $this->call(PersonasSeeder::class);
        $this->call(UsuariosSeeder::class);
        $this->call(TiposempleadosSeeder::class);
        $this->call(NivelesSeeder::class);
        $this->call(TipoareasSeeder::class);
        $this->call(AreasSeeder::class);
        $this->call(EmpleadosSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(ModulosSeeder::class);
        $this->call(UsuariosrolesSeeder::class);
        $this->call(EntidadesSeeder::class);
        $this->call(OficiosSeeder::class);
        $this->call(CatIndicadoresSeeder::class);

	    Model::reguard();
    }
}
