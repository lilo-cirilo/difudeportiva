<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ejercicio;

class EjerciciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		Ejercicio::create( [ 'anio' => '2011','created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2012', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2013', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2014', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2015', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2016', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2017', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Ejercicio::create( [ 'anio' => '2018', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
    }
}
