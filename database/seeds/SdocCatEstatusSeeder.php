<?php

use Illuminate\Database\Seeder;

class SdocCatEstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sdoc_cat_estatus')->insert([
            ['nombre' => 'NUEVO',
             'color' => 'info',
             'icono' => 'fa-sign-in',
             'descripcion' => 'Documento Nuevo', 
             'created_at' => date("Y-m-d H:i:s"),
             'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'REVISIÓN',
             'color' => 'warning',
             'icono' => 'fa-clipboard',
             'descripcion' => 'Documento en Revisión',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'PENDIENTE',
             'color' => 'warning',
             'icono' => 'fa-hourglass-o',
             'descripcion' => 'Documento Pendiente',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'CORRECCIÓN',
             'color' => 'danger',
             'icono' => 'fa-arrow-circle-left',
             'descripcion' => 'Documento para Corrección',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'VINCULADO',
             'color' => 'success',
             'icono' => 'fa-chain',
             'descripcion' => 'Documento Vinculado',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'ATENDIDO',
             'color' => 'atended',
             'icono' => 'fa-check-square',
             'descripcion' => 'Documento Atendido',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'FINALIZADO',
             'color' => 'finished',
             'icono' => 'fa-check-square-o',
             'descripcion' => 'Documento Finalizado',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'CANCELADO',
             'color' => 'cancel',
             'icono' => 'fa-times-circle',
             'descripcion' => 'Documento Cancelado',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],

            ['nombre' => 'CONOCIMIENTO',
             'color' => 'success',
             'icono' => 'fa-cc',
             'descripcion' => 'Copia de Conocimiento',
						 'created_at' => date("Y-m-d H:i:s"),
						 'updated_at' => date("Y-m-d H:i:s")],
						 
						 ['nombre' => 'VISTO BUENO',
							'color' => 'vobo',
							'icono' => 'fa-eye',
							'descripcion' => 'Visto Bueno',
							'created_at' => date("Y-m-d H:i:s"),
							'updated_at' => date("Y-m-d H:i:s")],
        ]);
    }
}
