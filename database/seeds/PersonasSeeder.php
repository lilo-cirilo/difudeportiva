<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Persona;

class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();
    	Persona::create( [
    		'titulo'=>'ING',
			'nombre'=>'ADMINISTRADOR',
			'primer_apellido'=>'DEL',
			'segundo_apellido'=>'SISTEMA',
			'calle'=>'13 DE SEPTIEMBRE',
			'numero_exterior'=>'313',
			'numero_interior'=>'5',
			'colonia'=>'EX-MARQUESADO',
			'codigopostal'=>'68030',
			'localidad_id'=>'1',
			'municipio_id'=>'67',
			'curp'=>'ABCD123456MOCRJL03',
			'genero'=>'F',
			'fecha_nacimiento'=>'1992-07-15',
			'clave_electoral'=>'123456789',
			'numero_celular'=>'121221212',
			'numero_local'=>'1221212',
			'email'=>'administrador@gmail.com',
			'referencia_domicilio'=>'ENTRE UNA CALLE Y OTRA CALLE',
			'created_at'=>date('Y-m-d H:m:s'),
			'updated_at'=>date('Y-m-d H:m:s'),
    	] );
        


    }
}
