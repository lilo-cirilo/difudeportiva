<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\RecmCatTipos;

class RecmCatTiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RecmCatTipos::create( [ 'tipo' => 'REQUISICION', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s')] );
        RecmCatTipos::create( [ 'tipo' => 'MANTENIMIENTO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s')] );
    }
}
