<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Region;

class RegionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		Model::unguard();
		Region::create( [ 'nombre' => 'CAÑADA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'COSTA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'ISTMO', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'MIXTECA', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'PAPALOAPAM', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'SIERRA NORTE', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'SIERRA SUR', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
		Region::create( [ 'nombre' => 'VALLES CENTRALES', 'created_at'=> date('Y-m-d H:m:s'), 'updated_at'=>date('Y-m-d H:m:s'), ] );
     
		
    }
}
