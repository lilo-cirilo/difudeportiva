<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tiposempleado;

class TiposempleadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
    
    	Tiposempleado::create( [
		'tipo'=>'BASE',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'CONFIANZA NOMBRAMIENTO',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'CONTRATO ESPECIAL',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'MANDOS MEDIOS',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'CONTRATO',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'CONFIANZA CONTRATO',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'GUARDIAS',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'SERVICIO SOCIAL',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'BASE CREE',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'DIF NACIONAL',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'HONORARIOS ALBERGUE 1',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'HONORARIOS ALBERGUE 2',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'BASE ALBERGUE 1',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'BASE ALBERGUE 2',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );
		Tiposempleado::create( [
		'tipo'=>'HONORARIOS',
		'oficial'=>'1',
		'created_at'=>date('Y-m-d H:m:s'),
		'updated_at'=>date('Y-m-d H:m:s'),
		] );

    }
}
