<?php

use Illuminate\Database\Seeder;
use App\Models\Sicodoc\TipoOficio;
use App\Models\Sicodoc\EstatusSeguimiento;
use App\Models\Sicodoc\Oficio;
use App\Models\Sicodoc\Seguimiento;
use App\Models\Sicodoc\Historial;

class OficiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		TipoOficio::insert([
			['tipo' => 'Oficio'],
			['tipo' => 'Memorandum'],
			['tipo' => 'Circular'],
			['tipo' => 'Ficha informativa']
		]);

		EstatusSeguimiento::insert([
			['estatus' => 'Capturado'],
			['estatus' => 'Recibido'],
			['estatus' => 'En proceso'],
			['estatus' => 'Cancelado'],
			['estatus' => 'Servido']
		]);

		Oficio::create([
			'folio' => 'DG/UI0001/2018',
			'fecha' => date('Y-m-d'),
			'interno' => true,
			'nombre_destinatario' => 'Juanito Escarcha',
			'puesto_destinatario' => 'Jefe de la Unidad de Informática',
			'asunto' => 'Oficio de prueba',
			'nombre_solicitante' => 'Julio Morales',
			'id_tipooficio' => 1,
			'id_areasolicitante' => 1
		]);

		Seguimiento::create([
			'fecha' => date('Y-m-d'),
			'observaciones' => 'Se captura el oficio de prueba',
			'id_oficio' => 1,
			'id_estatusseguimiento' => 1
		]);
    }
}
