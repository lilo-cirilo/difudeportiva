@extends('vendor.admin-lte.layouts.main')

@if(auth()->check())
  @section('user-avatar')
    @php
        $userimg = auth()->user()->persona->get_url_fotografia();
        $userimg = $userimg != 'images/no-image.png'? $userimg : 'images/serv med/usuario.png';
    @endphp
    {{ asset($userimg) }}
  @endsection
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('sidebar-menu')
  <ul class="sidebar-menu" data-widget="tree" id="sidebar">
    <li>
    		<a href="{{ route('home') }}">
    			<i class="fa fa-home"></i><span>IntraDIF</span>
    		</a>
    	</li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('ubr.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
        <li><a href="{{ route('ubr.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
      </ul>
    </li>
  </ul>
@endsection

@section('content-title', 'Unidades básicas de rehabilitación')

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
  <script type="text/javascript">
    $('input[type="text"]').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    $('#sidebar').find('a').each(function (){
      //auto seleccionar elemento activos en la barra lateral
      if($(this).attr('href')==window.location){
        $(this).parent().addClass('active');//primer nivel
        $(this).parent().parent().parent('li').addClass('active');//segundo nivel
        $(this).parent().parent().parent().parent().parent('li').addClass('active');//tercer nivel
      }
    })
    function block() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '0px',
                backgroundColor: 'none',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#fff'
            },
            baseZ: 10000,
            message: '<div align="center"><img style="width:168px; height:168px;" src="{{ asset('images/preloader/loading.gif') }}"><br /><p style=" color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">Procesando...</p></div>',
        });

        function unblock_error() {
            if($.unblockUI())
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
        }

        setTimeout(unblock_error, 120000);
    }

    function unblock() {
        $.unblockUI();
    }
  </script>
@endpush
