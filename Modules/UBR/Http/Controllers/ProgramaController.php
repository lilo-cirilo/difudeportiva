<?php

namespace Modules\UBR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\ProgramaBaseController;

class ProgramaController extends ProgramaBaseController{
  function __construct(){
    parent::__construct('UBR','ubr');
  }
}