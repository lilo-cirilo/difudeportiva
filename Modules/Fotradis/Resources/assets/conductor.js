	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	var licencias = (function() {

		function select2_licencias() {
			$('#clave_licencia').select2({
		      language: 'es',
		      ajax: {
		        //url: '{{ route('licencias.fotradis.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.numero_licencia,
		                text: item.numero_licencia,
		                slug: item.numero_licencia,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function crear_licencia() {
			if($("#form_modal_licencia").valid()) {
				swal({
				title: '¿Estas seguro?',
				text: '¡Nueva licencia!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});

 						$.ajax({
							url:  "{{ route('fotradis.crear.licencia') }}",
							type: 'POST',
							dataType: 'JSON',
							success: function(response) {
								swal({
									title: '¡Selección exitosa!',
									text: 'Registro encontrado...',
									type: 'success'
								});
							},
							error: function(response) {
								swal({
									title: '¡Selección cancelada!',
									text: 'Registro no encontrado...',
									type: 'error'
								});
							}
						});

					}
				});
			}
		};

		return {
			select2_licencias: select2_licencias,
			crear_licencia: crear_licencia
		};
	})();

	var empleados = (function() {
		var form = $("#form_modal_agregar_conductor");

		function select2_empleados() {
			$('#conductor').select2({
		      language: 'es',
		      ajax: {
		        //url: '{{ route('empleados.fotradis.select') }}',
		        dataType: 'JSON',
		        type: 'GET',
		        data: function(params) {
		          return {
		            search: params.term
		          };
		        },
		        processResults: function(data, params) {
		          params.page = params.page || 1;
		          return {
		            results: $.map(data, function(item) {
		              return {
		                id: item.rfc,
		                text: item.rfc,
		                slug: item.rfc,
		                results: item
		              }
		            })
		          };
		        },
		        cache: true
		      }
	  		});
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					conductor: {
						required: true
					}
				}
			});
		};

		function seleccionar_empleado() {
			if(form.valid()) {
				swal({
				title: '¿Estas seguro?',
				text: '¡Empleado seleccionado!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						$.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							}
						});

						var rfc = $("#conductor").select2('data')[0].results.rfc;

 						$.ajax({
							url:  "{{ URL::to('fotradis/conductor/create') }}" + "?rfc=" + rfc,
							type: 'GET',
							success: function(response) {
								swal({
									title: '¡Selección exitosa!',
									text: 'Registro encontrado...',
									type: 'success'
								});

								window.location.href = "{{ URL::to('fotradis/conductor/show') }}" + "?empleado=" + response.id;
							},
							error: function(response) {
								swal({
									title: '¡Selección cancelada!',
									text: 'Registro no encontrado...',
									type: 'error'
								});
							}
						});

					}
				});
			}
		};

		function crear_editar_conductor(persona, conductor) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url:  "{{ URL::to('fotradis/conductor/show') }}" + "?persona=" + persona,
				type: 'GET', 
				success: function(response) {
					
				},
				error: function(response) {
					swal({
						title: 'Error inesperado',
						text: 'Su registro no ha sido encontrado..',
						type: 'error'
					});
				}
			});
		};

		return {
			select2_empleados: select2_empleados,
			agregar_validacion: agregar_validacion,
			seleccionar_empleado: seleccionar_empleado,
			crear_editar_conductor: crear_editar_conductor
		};
	})();

	function elegir_licencia() {
		$("#licencia").val($("#select2-clave_licencia-container").text());
	}

	function mostrar_modal_nueva_licencia() {
		$("#modal_nueva_licencia").modal("show");
	}

	function cambiar_modal_nueva_licencia() {
    modal_body_nueva_licencia.html(
      '<div class="form-group">' +
          '<label for="tipo_licencia_nueva">Tipo de licencia:</label>' +
          '<select id="tipo_licencia_nueva" name="tipo_licencia_nueva" class="form-control">' +
              '<option>A</option>' +
              '<option>B</option>' +
              '<option>C</option>' +
              '<option>D</option>' +
          '</select>'+ 
      
          '<label for="numero_licencia">Número de licencia:</label>' +
          '<input type="text" class="form-control" id="numero_licencia" name="numero_licencia" value="" />' +
      
          '<label for="fecha_expedicion">Fecha de expedición:</label>' +
          '<input type="text" class="form-control" id="fecha_expedicion" name="fecha_expedicion" value="" />' +
      
          '<label for="oficina_expedicion">Oficina de expedición:</label>' +
          '<input type="text" class="form-control" id="oficina_expedicion" name="oficina_expedicion" value="" />' +
      
          '<label for="entidad">Entidad federativa:</label>' +
          '<select id="entidad" name="entidad" class="form-control">' +
            entidades() +
          '</select>' +
      '</div>'
    );

    $('#fecha_expedicion').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd-mm-yyyy'
    }).change(function(event) {
      $("#fecha_expedicion").valid();  
    });

    //button_confirmar_licencia.attr("onclick", "crear_nueva_licencia()");
    modal_footer_nueva_licencia.html(
      '<button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="reiniciar_modal_nueva_licencia()">Cerrar</button>' +
      '<button id="confirmar_licencia" type="button" class="btn btn-primary pull-rigth" data-toggle="modal" title="Finalizar confirmación"data-target="#confirmar" onclick="licencias.crear_licencia()">Confirmar</button>'
    );
  }

  function reiniciar_modal_nueva_licencia() {
    modal_body_nueva_licencia.html(
      '<div class="">' +
        '<div class="form-group">' +
          '<label for="clave_licencia">Elegir número de licencia existente</label>' +
            '<select id="clave_licencia" name="clave_licencia" class="form-control select2" style="width: 100%;">' +
                '</select>' +
        '</div>' +
      '</div>'
    );

    modal_footer_nueva_licencia.html(
      '<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>' +
      '<button id="nueva_licencia" type="button" class="btn btn-warning pull-rigth" data-toggle="modal" title="Solicitud nueva licencia" onclick="cambiar_modal_nueva_licencia()">Nueva licencia</button>' +
      '<button id="confirmar_licencia" type="button" class="btn btn-primary pull-rigth" data-toggle="modal" title="Finalizar confirmación" data-target="#confirmar" data-dismiss="modal" onclick="licencias.crear_licencia()">Confirmar</button>'
    );
  }

  function entidades() {
    return '<option>Aguascalientes, Aguascalientes</option>' +
            '<option>Baja California, Mexicali</option>' +
            '<option>Baja California Sur, La Paz</option>' +
            '<option>Campeche, San Francisco de Campeche</option>' +
            '<option>Chihuahua, Chihuahua</option>' +
            '<option>Chiapas, Tuxtla Gutiérrez</option>' +
            '<option>Coahuila, Saltillo</option>' +
            '<option>Colima, Colima</option>' +
            '<option>Durango, Victoria de Durango</option>' +
            '<option>Guanajuato, Guanajuato</option>' +
            '<option>Guerrero, Chilpancingo de los Bravo</option>' +
            '<option>Hidalgo, Pachuca de Soto</option>' +
            '<option>Jalisco, Guadalajara</option>' +
            '<option>México, Toluca de Lerdo</option>' +
            '<option>Michoacán, Morelia</option>' +
            '<option>Morelos, Cuernavaca</option>' +
            '<option>Nayarit, Tepic</option>' +
            '<option>Nuevo León, Monterrey</option>' +
            '<option>Oaxaca, Oaxaca de Juárez</option>' +
            '<option>Puebla, Puebla de Zaragoza</option>' +
            '<option>Querétaro, Santiago de Querétaro</option>' +
            '<option>Quintana Roo, Chetumal</option>' +
            '<option>San Luis Potosí, San Luis Potosí</option>' +
            '<option>Sinaloa, Culiacán Rosales</option>' +
            '<option>Sonora, Hermosillo</option>' +
            '<option>Tabasco, Villahermosa</option>' +
            '<option>Tamaulipas, Ciudad Victoria</option>' +
            '<option>Tlaxcala, Tlaxcala de Xicohténcatl</option>' +
            '<option>Veracruz, Xalapa-Enríquez</option>' +
            '<option>Yucatán, Mérida</option>' +
            '<option>Zacatecas, Zacatecas</option>';
  }