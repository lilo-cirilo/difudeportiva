@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@section('content-title', 'DIF TE LLEVA')
@section('content-subtitle', 'Fondo para la Accesibilidad en el Transporte Público para las Personas con Discapacidad')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Monitoreo</a></li>
        @yield('li-breadcrumbs')
    </ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
	<li class="header active">DIF TE LLEVA</li>

	<li>
		<a href="{{ route('home') }}">
			<i class="fa fa-home"></i><span>IntraDIF</span>
		</a>
	</li>

    <li class="active">
		<a href="#" onclick="abrirModalReporte();">
        <i class="fa fa-file-pdf-o text-red"></i><span>Generar reporte</span>
		</a>
    </li>

    <li {{ ($ruta === 'fotradis.registroBenefs.index') ? 'class=active' : '' }}>
		<a href="{{ route('registrobenef.index') }}">
			<i class="fa fa-fw fa-user text-green"></i><span>Registro de beneficiarios</span>
		</a>
	</li>

	<li {{ ($ruta === 'fotradis.index') ? 'class=active' : '' }}>
		<a href="{{ route('fotradis.index') }}">
			<i class="fa fa-dashboard"></i><span>Monitoreo</span>
		</a>
	</li>
  @if(auth()->user()->hasRolesModulo(['ADMINISTRADOR','MONITOR'],6))  
    <li {{ ($ruta === 'periodorecorridos.index') ? 'class=active' : '' }}>
		<a href="{{ route('periodorecorridos.index') }}">
        <i class="fa fa-fw fa-edit text-blue"></i><span>Recorridos</span>
		</a>
    </li>
    
    <li {{ ($ruta === 'fotradis.historial') ? 'class=active' : '' }}>
		<a href="{{ route('fotradis.historial') }}">
        <i class="fa fa-fw fa-history text-purple"></i><span>Historial conductores</span>
		</a>
	</li>

    <li {{ ($ruta === 'unidad.index') ? 'class=active' : '' }}>
		<a href="{{ route('unidad.index') }}">
        <i class="fa fa-fw fa-bus text-red"></i><span>Transporte</span>
		</a>
	</li>

    <li {{ ($ruta === 'conductor.index') ? 'class=active' : '' }}>
		<a href="{{ route('conductor.index') }}">
        <i class="fa fa-fw fa-user text-yellow"></i><span>Conductores</span>
		</a>
	</li>

    <li {{ ($ruta === 'rutas.index') ? 'class=active' : '' }}>
		<a href="{{ route('rutas.index') }}">
        <i class="fa fa-fw fa-map-signs text-green"></i><span>Rutas</span>
		</a>
    </li>
    
    <li class="treeview">
        <a href="#">
            <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="{{ route('fotradis.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
            <li><a href="{{ route('fotradis.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
        </ul>
    </li>
    
    <li >
		<a href="{{ route('rutas.incompletas') }}">
        <i class="fa fa-config"></i><span>Rutas Incompletas</span>
		</a>
    </li>
</ul>  
@endif
@endsection

@section('styles')
<!-- Bootstrap 3.3.7 -->
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<!-- Ionicons -->
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<!-- daterange picker -->
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<!-- bootstrap datepicker -->
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- iCheck for checkboxes and radio inputs -->
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap Color Picker -->
<link href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap time Picker -->
<link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Select2 -->
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<!-- Theme style -->
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    input {
        text-transform: uppercase;
    }

    select{
        width: 100%;
    }
    select[readonly] {
	    background: #eee;
	    cursor: no-drop;
    }
    select[readonly] option {
        display: none;
    }

    .modal-body {
        overflow-y: auto;
        max-height: calc(100vh - 210px);
    }
</style>
@stop

@if(auth()->check())
  @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('scripts')
@include('fotradis::reporte')
<!-- jQuery 3 -->
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{ asset('bower_components/select2/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<!-- InputMask -->
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap datepicker locales -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<!-- bootstrap color picker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script type="text/javascript" src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- blockui -->
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
{{-- <script src="https://www.gstatic.com/firebasejs/5.0.4/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.10.1/firebase-database.js"></script> --}}

<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJY1fM_1PhRMuqn6eu9-2Cra1LRdnoqg&libraries=geometry,visualization"></script>

@include('fotradis::js.general')
{{-- @include('fotradis::unity.js.unidades')
@include('fotradis::routes.js.rutas')
@include('fotradis::driver.js.conductor') --}}

<script type="text/javascript">
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } })

    $('#fechaIni').datepicker({
  		autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: moment('2018-01-01').format('DD/MM/YYYY'),
        endDate: moment().format('DD/MM/YYYY')
  	}).on('changeDate',function(event) {
        $('#fechaFin').datepicker('setDate','')
        $('#fechaFin').datepicker('setStartDate', $(this).datepicker('getDate'))   
  	})

    $("#fechaFin").datepicker({
      autoclose: true,
      language: 'es',
      format: 'dd/mm/yyyy',
      startDate: moment('2018-01-01').format('DD/MM/YYYY'),
      endDate: moment().format('DD/MM/YYYY')
    }).on('changeDate',function(event) {
    })

    $("#tipo-reporte").on('change',function() {
        console.log(this.value)
        if(this.value == 'heatmapdata') {
            $("#tipo-mapa-calor-div").attr("style","")
            $("#tipo-servicios-div").attr("style","display:none;")
        } else if(this.value == 'info') {
            $("#tipo-servicios-div").attr("style","")
            $("#tipo-mapa-calor-div").attr("style","display:none;")
        } else {
            $("#tipo-mapa-calor-div").attr("style","display:none;")
            $("#tipo-servicios-div").attr("style","display:none;")
        }
    })
  	
    function block() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: 'none',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#fff',
            },
            baseZ: 10000,
            message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
        });

        function unblock_error() {
            if($.unblockUI())
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
        }

        setTimeout(unblock_error, 120000);
    }

    function unblock() {
        $.unblockUI();
    }

    function abrirModalReporte() {
        $('#form-reporte').trigger("reset")
        $('#modal-reporte').modal('show')
    }

    function filtroHeatMapData(array) {
      let data = []
      
      array["coordenadas"].forEach(function(a,i,c) {
          data.push(new google.maps.LatLng(parseFloat(a.latitud),parseFloat(a.longitud)))
          
      })
      console.log(data)
      return data
    }

    function generarReporte() {
        $('#form-reporte').validate({
            rules: {
                fechaIni: {
                    required: true
                },
                fechaFin: {
                    required: true
                }
            }
        })

        if($('#form-reporte').valid()) {
            let tipoReporte = $("#tipo-reporte").val()
            block()
            $.ajax({
                url: "{{ URL::to('fotradis') }}"+"/"+tipoReporte,//"http://intradif.developersoax.com.mx:8080/dtll/api/"
                type: "POST",
                data: $('#form-reporte').serialize(),
                success: (response) => {
                    if(tipoReporte != "heatmapdata") {
                        jsreport.serverUrl = 'http://intradif.developersoax.com.mx:3001'
                        var request = {
                            template:{
                                shortid: tipoReporte == "info" ? ($("#tipo-servicio").val() == "general" ? "S1WrR9bIN" : "ryyEzKzO4") : (tipoReporte == "mantenimiento" ? "SJSV2ZvvV" : "ry3jSB3LN"),
                                phantom: {
                                    header: `<img src='{#asset Oaxaca_DIF.png @encoding=dataURI}' width="135" height="25"/> <span>My Great Report</span>`,
                                    footer: '<span>{#pageNum}/{#numPages}</span>'
                                }
                            },
                            data:  tipoReporte == "info" ? Registro_Servicios(response,$("#tipo-servicio").val()) : (tipoReporte == "mantenimiento" ? response : Registro_Bitacora(response))
                        }
                        //jsreport.render('_blank', request)
                        //add custom headers to ajax calls
                        jsreport.headers['Authorization'] = "Basic " + btoa("admin:password")
                        //render through AJAX request and return promise with array buffer response
                        jsreport.renderAsync(request).then(function(res) {
                            unblock()

                            //open in new window
                            window.open(res.toDataURI())

                            //get the content as string
                            //res.toString()

                            //open download dialog
                            var title = ''
                            switch (tipoReporte) {
                                case 'info':
                                    t = $("#tipo-servicio").val() == "general de servicios" ? "general" : "servicios por conductor y pasajero"
                                    title =  'reporte dtll '+t+' registrados del '+response.fechaIni+' al '+response.fechaFin
                                    break;
                                case 'mantenimiento':
                                    title = 'reporte dtll de mantenimiento de unidades registrados del '+response.fechaIni+' al '+response.fechaFin
                                    break;
                                case 'bitacora':
                                    title = 'reporte dtll de kilometraje y gasolina registrados del '+response.fechaIni+' al '+response.fechaFin
                                    break;
                            
                                default:
                                    break;
                            }
                            res.download(title)
                        })
                    } else {
                        console.log(response)
                        var heatMapData = filtroHeatMapData(response)
      
                        map1 = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: 17.027557, lng: -96.710725},
                            zoom: 12,
                            mapTypeControl: false,
                            styles: [
                                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                                {
                                featureType: 'administrative.locality',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#d59563'}]
                                },
                                {
                                featureType: 'poi',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#d59563'}]
                                },
                                {
                                featureType: 'poi.park',
                                elementType: 'geometry',
                                stylers: [{color: '#263c3f'}]
                                },
                                {
                                featureType: 'poi.park',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#6b9a76'}]
                                },
                                {
                                featureType: 'road',
                                elementType: 'geometry',
                                stylers: [{color: '#38414e'}]
                                },
                                {
                                featureType: 'road',
                                elementType: 'geometry.stroke',
                                stylers: [{color: '#212a37'}]
                                },
                                {
                                featureType: 'road',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#9ca5b3'}]
                                },
                                {
                                featureType: 'road.highway',
                                elementType: 'geometry',
                                stylers: [{color: '#746855'}]
                                },
                                {
                                featureType: 'road.highway',
                                elementType: 'geometry.stroke',
                                stylers: [{color: '#1f2835'}]
                                },
                                {
                                featureType: 'road.highway',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#f3d19c'}]
                                },
                                {
                                featureType: 'transit',
                                elementType: 'geometry',
                                stylers: [{color: '#2f3948'}]
                                },
                                {
                                featureType: 'transit.station',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#d59563'}]
                                },
                                {
                                featureType: 'water',
                                elementType: 'geometry',
                                stylers: [{color: '#17263c'}]
                                },
                                {
                                featureType: 'water',
                                elementType: 'labels.text.fill',
                                stylers: [{color: '#515c6d'}]
                                },
                                {
                                featureType: 'water',
                                elementType: 'labels.text.stroke',
                                stylers: [{color: '#17263c'}]
                                }
                            ]
                        });
                
                        var heatmap = new google.maps.visualization.HeatmapLayer({
                            data: heatMapData
                        });
                        heatmap.setMap(map1);
                        
                        if($("#tipo-mapa-calor").val() == "subidas")
                            $("#mapacalor-titulo").text("Mapa de calor de donde se suben pasajeros")
                        else if($("#tipo-mapa-calor").val() == "bajadas")
                            $("#mapacalor-titulo").text("Mapa de calor de donde se bajan pasajeros")

                        $('#modal-mapacalor').modal('show')
                        unblock()                    
                    }
                },
                error: (response) => {
                    unblock()
                    console.log(response);
                    
                    swal(
                        'Error',
                        'No se pudo obtener datos para reporte ',
                        'error'
                    )
                }
            });
        }
    }

    var Registro_Bitacora = (obj) => {             
        var items = {}
        items["fechaIni"] = obj["fechaIni"]
        items["fechaFin"] = obj["fechaFin"]
        items["fechaImp"] = moment().format("DD/MM/YYYY HH:MM:SS")  
        items["km"] = {}
        items["gas"] = {}
        items["km"]["recorridos"] = obj["kms_por_unidad"]
        items["gas"]["acumule"] = obj["cargas_de_gas_acumulada"]
        items["gas"]["todo"] = obj["registros_de_carga_de_gas"]
        items["graficas"] = {}

        let a1 = obj["kms_por_unidad"].filter(o => (o.kmFin - o.kmIni) > 0 && o.kmIni != null)
        let aN = []
        a1.forEach(a => {
            aN.push({value: (a.kmFin - a.kmIni),name: a.numero_unidad})
        })

        items["graficas"]["kmrecorridos"] = aN

        a1 = obj["cargas_de_gas_acumulada"]
        aN = []
        a1.forEach(a => {
            aN.push({value: a.litros,name: a.numero_unidad})
        })

        items["graficas"]["gasacumule"] = aN

        console.log(items)
        return items
    }

    var Registro_Servicios = (obj,tipo) => {             
        var items = {}
        items["fechaIni"] = obj["fechaIni"]
        items["fechaFin"] = obj["fechaFin"]
        items["fechaImp"] = moment().format("DD/MM/YYYY HH:MM:SS")
        items["servicios"] = {}

        if(tipo=="general") {
            items["inscritos"] = {}
            items["inscritos"]["hombres"] = {}
            items["inscritos"]["mujeres"] = {}
            items["inscritos"]["pormes"] = mesNombre(obj["pasajeros_inscritos_por_mes"].filter(o => o.mesAnio != "Jul 2018"))

            obj["servicios_por_pasajero"] = obj["servicios_por_pasajero"].filter(o => (o.numRuta != null || o.numTaxi != null))

            items["inscritos"]["hombres"]["ruta"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "H").filter(o => (o.numTaxi == null && o.numRuta != null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["mujeres"]["ruta"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "M").filter(o => (o.numTaxi == null && o.numRuta != null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["hombres"]["taxi"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "H").filter(o => (o.numTaxi != null && o.numRuta == null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["mujeres"]["taxi"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "M").filter(o => (o.numTaxi != null && o.numRuta == null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["hombres"]["ambos"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "H").filter(o => (o.numTaxi != null && o.numRuta != null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["mujeres"]["ambos"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "M").filter(o => (o.numTaxi != null && o.numRuta != null)).filter(o => moment(o.fechaInsert).format('DD/MM/YYYY') <= moment(obj["fechaFin"]).format('DD/MM/YYYY')).length
            items["inscritos"]["hombres"]["activos"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "H").filter(o => o.totalServ != 0).length
            items["inscritos"]["mujeres"]["activos"] = obj["servicios_por_pasajero"].filter(o => o.generoCurp == "M").filter(o => o.totalServ != 0).length
            
            items["servicios"]["hombres"] = {}
            items["servicios"]["mujeres"] = {}
            items["servicios"]["rutas"] = obj["servicios_por_recorrido"]
            

            items["servicios"]["hombres"]["ruta"] = obj["servicios_por_genero"].filter(o => o.generoCurp == "H" && o.tipo == "ruta")[0].servicios
            items["servicios"]["mujeres"]["ruta"] = obj["servicios_por_genero"].filter(o => o.generoCurp == "M" && o.tipo == "ruta")[0].servicios
            items["servicios"]["hombres"]["taxi"] = obj["servicios_por_genero"].filter(o => o.generoCurp == "H" && o.tipo == "taxi")[0].servicios
            items["servicios"]["mujeres"]["taxi"] = obj["servicios_por_genero"].filter(o => o.generoCurp == "M" && o.tipo == "taxi")[0].servicios
            items["graficas"] = {}
            
            tipoServicio = ["ambos","taxi","urban"]

            //
            obj["meses_anios"] = obj["meses_anios"].filter(o => o.mesAnio != "Jul 2018")
            obj["inscritos_por_mes_servicio"] = obj["inscritos_por_mes_servicio"].filter(o => o.mesAnio != "Jul 2018")
            //
            tipoServicio.forEach(a => {
                obj["meses_anios"].forEach(b => {
                    if(obj["inscritos_por_mes_servicio"].filter(o => o.mesAnio == b.mesAnio && a == o.servicio).length == 0)
                        obj["inscritos_por_mes_servicio"].push({servicio: a,mesAnio:b.mesAnio,rep:0,mesAnioN:b.mesAnioN})
                })
            })
            items["graficas"]["progresioninscritos"] = {}
            items["graficas"]["progresioninscritos"]["ambos"] = mapMesAnio("value",mesAnioNShort(obj["inscritos_por_mes_servicio"].filter(o => o.servicio == "ambos")))
            items["graficas"]["progresioninscritos"]["taxi"] = mapMesAnio("value",mesAnioNShort(obj["inscritos_por_mes_servicio"].filter(o => o.servicio == "taxi")))
            items["graficas"]["progresioninscritos"]["ruta"] = mapMesAnio("value",mesAnioNShort(obj["inscritos_por_mes_servicio"].filter(o => o.servicio == "urban")))
            items["graficas"]["mesesanios"] = mapMesAnio("key",obj["meses_anios"])
            items["graficas"]["recorridosdias"] = {}
            items["graficas"]["recorridosdias"]["keys"] = keyValueGrafica(obj["dias_de_servicio_por_recorrido"],"key")
            items["graficas"]["recorridosdias"]["values"] = keyValueGrafica(obj["dias_de_servicio_por_recorrido"],"value")
            items["graficas"]["diassemanasservicios"] = graficaDiasSemanaServicios(obj["servicios_por_dia_semana"],"dias")
            items["graficas"]["horassemanasservicios"] = graficaDiasSemanaServicios(obj["servicios_por_hora_semana"],"hrs")
            items["graficas"]["serviciostipospasajero"] = mapBarras(obj["servicios_por_tipopasajero"])
            items["graficas"]["inscritostipospasajero"] = mapBarras(obj["inscritos_por_tipopasajero"])

        } else {
            items["servicios"]["conductores"] = obj["servicios_por_conductor"]
            items["servicios"]["pasajeros"] = obj["servicios_por_pasajero"].filter(o => o.totalServ > 0)
        }
        
        console.log(items)
        return items
    }

    function mapBarras(array) {
        mHeaders= []
        mSeries= []
        mA = []
        array.forEach(b => {
            mHeaders.push(b.tipo)            
            mSeries.push(b.cont)
        })
        mA.push(mHeaders)
        mA.push(mSeries)
        
        return mA
    }

    function graficaDiasSemanaServicios(array,type) {
        var semanas = []
        var semana = []
        var legends = []
        var semTemp = ""
        array.forEach(function(a,i,c) {
            if(a.islegend != semTemp && semTemp != "") {
                semTemp = a.islegend
                legends.push(a.islegend)
                semanas.push(semana)
                semana = []
                semana.push(a)
                if(i == (array.length-1))
                    semanas.push(semana)
            } else if(a.islegend == semTemp && i == (array.length-1))  {
                semana.push(a)
                semanas.push(semana)
            } else {
                if(semTemp == "") {
                    semTemp = a.islegend
                    legends.push(a.islegend)
                }
                semana.push(a)
                if(array.length == 1)
                    semanas.push(semana)
            }
        })
        var format = []
        if(type == "dias")
            format = ["lunes","martes","miércoles","jueves","viernes","sábado"]
        else {
            for (let i = 5; i < 19; i++) {
                let text = i+" hrs"
                if(i < 10)
                    text = "0"+i+" hrs"
                format.push(text)
            }
        }

        format.forEach(function(a,i,c) {
            semanas.forEach(b => {
                if(b.filter(o => o.format == a).length == 0)
                    b.push({iskey:i,format:a,islegend:null,isvalue:0})
            })
        })
        
        var series = []
        semanas.forEach(e => {
          s = arrayShort(e)          
          v = []
          for (let i = 0; i < s.length; i++) {
            v.push(s[i].isvalue)
          }
          series.push(
            {
                name: s.filter(o => o.islegend != null)[0].islegend,
                type:'line',
                label: {
                    normal: {
                        show: true,
                        position: 'inside',
                        fontStyle: 'bold'
                    }
                },
                data:v,
                markPoint: {
                    data: [
                        {type: 'max'}
                        //{type: 'min'}
                    ]
                },
                markLine: {
                    data: [
                        {type: 'average'}
                    ]
                }
            }
          )
        })

        
        return [series,legends,format]
    }

    function keyValueGrafica(array,type) {
        mapr = []
        array.forEach(b => {
            if(type == "key")
                mapr.push(b.iskey)
            else if(type == "value")
                mapr.push(b.isvalue)
            else if(type == "legend")
                mapr.push(b.islegend)
        })
        return mapr
    }

    function mapMesAnio(type,array) {
        mA = []
        mesNombre(array).forEach(b => {
            if(type == "key")
                mA.push(b.mesAnio)
            else
                mA.push(b.rep)
        })
        return mA
    }

    function mesNombre(array) {
        array.forEach(element => {
            var sp = element.mesAnio.split(' ')
            switch (sp[0].toLowerCase()) {
                case "jan":
                    element.mesAnio = 'Ene '+sp[1]
                    break;
                case "apr":
                    element.mesAnio = 'Abr '+sp[1]
                    break;
                case "aug":
                    element.mesAnio = 'Ago '+sp[1]
                    break;
                case "dec":
                    element.mesAnio = 'Dic '+sp[1]
                    break;
                default:
                    break;
            }
        })
        return array
    }

    function mesAnioNShort(array) {
        array.sort(function (a, b) {
            if (a.mesAnioN > b.mesAnioN) {
                return 1;
            }
            if (a.mesAnioN < b.mesAnioN) {
                return -1;
            }
            return 0;
        })
        return array
    }

    function arrayShort(array) {
        array.sort(function (a, b) {
            if (a.iskey > b.iskey) {
                return 1;
            }
            if (a.iskey < b.iskey) {
                return -1;
            }
            return 0;
        })
        return array
    }

    /* function ubicacionCoord(array) {
        console.log(array)
        
        let arrayLength = array.length
        array.forEach(function(a,i,c) {
            mL = new google.maps.LatLng(parseFloat(a.latitud),parseFloat(a.longitud))
            addressGeocoder(a,mL)
        })     
    }

    function addressGeocoder(mL,myLatLng) {
        var geocoder = new google.maps.Geocoder()
        geocoder.geocode({'latLng': myLatLng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var address=results[0]['formatted_address']
                console.log(address.split(',')[1]+" "+mL)
            } else
            console.log("fail "+mL.latitud+" "+mL.longitud)
        })
    }
    
    function reduce(bloque) {
        var i = {}
        const r = bloque.reduce((i,nombre) => {
            i[nombre] = (i[nombre] || 0) + 1
            return i
        },{})
        console.log(r);
        
        return r
        for (const key in bloque) {
            const element = object[key]
        }
    } */
</script>
@stop