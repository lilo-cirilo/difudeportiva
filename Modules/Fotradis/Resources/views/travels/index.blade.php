
@extends('fotradis::layouts.master')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('li-breadcrumbs')
    <li class="active">Recorridos actuales</li>
@endsection

@section('content')

    <section class="content">
    	<div class="box box-primar shadow">
			<div class="box-header with-border">
				<h3 class="box-title">Recorridos</h3>
        <div class="box-tools pull-right">
          <button class="btn btn-box-tool" data-toggle="modal" title="Elegir" data-target="#modal-recorrido" style="color: #d12654"><i class="fa fa-fw fa-plus-square"></i> Agregar Recorrido</button>
        </div>
      </div>

			<div class="box-body">
				<!-- /.box-header -->
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
					<input type="text" id="search" name="search" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
					</span>
				</div>
				<!-- table start -->
				{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'recorridos', 'name' => 'recorridos', 'style' => 'width: 100%']) !!}
			</div>
		</div>
		</section>
		
	
	<div class="modal fade" id="modal-recorrido">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header btn-info">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times</span></button>
					<center><h4 class="modal-title">Registrar nuevo recorrido</h4></center>
				</div>
				<div class="modal-body">
					<form id="form-recorrido">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="nombre">Nombre del recorrido</label>
								<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Ingresar nombre del recorrido">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
					<button type="button" class="btn btn-primary pull-rigth" data-toggle="modal" onclick="guardarRecorrido()">Guardar</button>
				</div>
			</div>
		</div>
	</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	})

	var form_recorrido = $('#form-recorrido')

	$(document).ready(function() {
		form_recorrido.validate({
			rules: {
				nombre: {
					required: true
				}
			}
		})

		block()
		var table = (function() {
			var tabla = undefined,
			btn_buscar = $('#btn_buscar'),
			search = $('#search')

			function init() {			
				search.keypress(function(e) {
					if(e.which === 13) {
						tabla.DataTable().search(search.val()).draw()
					}
				})

				btn_buscar.on('click', function() {
					tabla.DataTable().search(search.val()).draw()
				})
			}

			function set_table(valor) {
				tabla = $(valor)
			}

			function get_table() {
				return tabla
			}

			return {
				init: init,
				set_table: set_table,
				get_table: get_table
			}
		})()

		table.set_table($('#recorridos').dataTable())
		table.init()
	    unblock()
	})

	function habilitarRecorrido(key,type) {
		swal({
			title: '¿Estas seguro?',
			text:  `¡Se ${ type ? 'inhabilitara' : 'habilitara' }!`,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'SI',
			cancelButtonText: 'NO'
			}).then((result) => {
				if(result.value) {				
						$.ajax({
						url:  "{{ URL::to('fotradis/periodorecorridos') }}" + "/" + key + "?in=" + type,
						type: 'DELETE',
						success: function(response) {
							swal({
								title: '¡Transacción exitosa!',
								text: `Recorrido ${ type ? 'inhabilitado' : 'habilitado'}`,
								type: 'success'
							})
              window.LaravelDataTables['recorridos'].ajax.reload(null, false)
						},
						error: function(response) {
							swal({
								title: '¡Transacción rechazada!',
								text: 'No se pudo cambiar el estatus...',
								type: 'error'
							})
						}
					})
				}
		})
	}

	function guardarRecorrido() {
		if(form_recorrido.valid()) {
			swal({
			title: '¿Estas seguro?',
			text:  '¡Actualizando recorridos!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'SI',
			cancelButtonText: 'NO'
			}).then((result) => {
				if(result.value) {
						var formData = new FormData(form_recorrido[0])

						$.ajax({
						url:  "{{route('periodorecorridos.store')}}",
						type: 'POST',
						data: formData,
						dataType: 'JSON',
						processData: false, 
						contentType: false,
						success: function(response) {
							swal({
									title: '¡Transacción exitosa!',
									text: 'Se ha modificado los conductores...',
									type: 'success'
								})
							window.LaravelDataTables['recorridos'].ajax.reload(null, false)
						},
						error: function(response) {
							swal({
								title: '¡Transacción rechazada!',
								text: 'Registro no almacenado...',
								type: 'error'
							})
						}
					})
				}
			})
		}
	}
</script>
@endpush