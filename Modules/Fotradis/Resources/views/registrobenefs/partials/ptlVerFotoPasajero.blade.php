<div class="modal fade" id="modal_ver_fotopasajero" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <center><h4 class="modal-title">Visualizando fotografía del pasajero</h4></center>
      </div>
      <div class="modal-body">
        <div class="hovereffect"><img src="" class="img-thumbnail center-block" id="imagen" name="imagen"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div 