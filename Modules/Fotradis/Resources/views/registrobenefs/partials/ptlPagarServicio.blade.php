<div class="row">

  <h4>Período de servicio pagado:</h4>
  <hr>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="backupFechaIniServ">Fecha inicial actual:</label>
        <div class="input-group date">
          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
          <input type="text" class="datepicker form-control" id="backupFechaIniServ" name="backupFechaIniServ" value="{{ $pago->fechaIniServ or '' }}" />
        </div>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="backupFechaFinServ">Fecha término actual:</label>
        <div class="input-group date">
          <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
          <input type="text" class="datepicker form-control" id="backupFechaFinServ" name="backupFechaFinServ" value="{{ $pago->fechaFinServ or '' }}" />
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <hr>
    <label id="lblDiasRestantes" name="lblDiasRestantes" style="color: #58b9b0;"></label>
    <hr>
  </div>


  <form id="form_pagarServicio" enctype="multipart/form-data" action="{{ route('registrobenef.setstorepagoserv') }}" method="POST">
    
    <input type="hidden" id="servicioadquirido_id" name="servicioadquirido_id" value="{{ $pago->servicioadquirido_id or '' }}">
    
    <h4>Perído de servicio a pagar:</h4>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label>Fecha inicio de servicio*:</label>
          <div class="input-group date">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" id="fechaIniServ" name="fechaIniServ" class="datepicker form-control pull-right" value="" placeholder="dd/MM/aaaa" >
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="numMesesServ">Cantidad de meses a pagar*:</label>
            <select id="numMesesServ" name="numMesesServ" class="form-control select2">
              @for ($i = 1; $i < 12; $i++)
                <option value="{{ $i }}">{{ $i }}</option>
              @endfor
            </select>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="folioPago">Folio/No. pago*:</label>
          <input type="text" id="folioPago" name="folioPago" class="form-control" placeholder="Folio o número de pago" value="">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="fechaFinServ">Fecha de término de servicio</label>
          <div class="input-group date">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <input type="text" id="fechaFinServ" name="fechaFinServ" class="form-control" placeholder="Fecha en la que se vencerá el servicio" value="">
          </div>
        </div>
      </div>
    </div>
  </form>
</div>