<div class="row">
  
  <form id="form_eliminarServicio" enctype="multipart/form-data" action="{{ route('registrobenef.setdeleteservicio') }}" method="POST">

    @if(isset($servicioAmbos))
      <h4>Seleccione el servicio a eliminar:</h4>
    @else
      <h4>El siguiente servicio se eliminará junto con el beficiario:</h4>
    @endif
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label>
            <div class="icheckbox_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative">
            <input type="checkbox" id="numPasajero" name="numPasajero" value="{{ $servicio->id }}" class="flat-red" checked="" style="position: absolute opacity: 0"><ins class="iCheck-helper" style="position: absolute top: 0% left: 0% display: block width: 100% height: 100% margin: 0px padding: 0px background: rgb(255, 255, 255) border: 0px opacity: 0"></ins></div>
          </label>
          <label>
              {{ $servicio->servicio }} ({{ $servicio->numPasajero }})
          </label>
        </div>
      </div>

      @if(isset($servicioAmbos))
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="form-group">
            <label>
              <div class="icheckbox_flat-green checked" aria-checked="true" aria-disabled="false" style="position: relative">
              <input type="checkbox" id="numPasajeroAmbos" name="numPasajeroAmbos" value="{{ $servicioAmbos->id }}" class="flat-red" checked="" style="position: absolute opacity: 0"><ins class="iCheck-helper" style="position: absolute top: 0% left: 0% display: block width: 100% height: 100% margin: 0px padding: 0px background: rgb(255, 255, 255) border: 0px opacity: 0"></ins></div>
            </label>
            <label>
                {{ $servicioAmbos->servicio }} ({{ $servicioAmbos->numPasajero }})
            </label>
          </div>
        </div>
      @endif
    </div>

  </form>

</div>