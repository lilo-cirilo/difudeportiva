<script type="text/javascript">

var PagarServicio = (function()
{
  var erFechaDiaMesAnio = Globaljs.erFechaDiaMesAnio;
  var backupFechaIniServ=null, backupFechaFinServ=null, fechaIniServ=null, fechaFinServ=null, lblDiasRestantes=null, numMesesServ = null;
  var form_pagarServicio;

  function init() 
  {
    form_pagarServicio = $("#form_pagarServicio");
    backupFechaIniServ = $("#modal-pagarServicio #backupFechaIniServ");
    backupFechaFinServ = $("#modal-pagarServicio #backupFechaFinServ");
    fechaIniServ = $("#modal-pagarServicio #fechaIniServ");
    fechaFinServ = $("#modal-pagarServicio #fechaFinServ");
    numMesesServ = $("#modal-pagarServicio #numMesesServ");
    lblDiasRestantes = $("#modal-pagarServicio #lblDiasRestantes");

    backupFechaIniServ.val(Globaljs.reverseDate (backupFechaIniServ.val(),'-','/'));
    backupFechaFinServ.val(Globaljs.reverseDate (backupFechaFinServ.val(),'-','/'));
    
    var fechaIni = (Globaljs.compareDate(Globaljs.getFechaHoy(false),"<=",backupFechaIniServ.val())) ? backupFechaIniServ.val() : Globaljs.getFechaHoy(false); //Por si se diera el caso que el tiempo de contrato todavía no ha iniciado, tomamos el dia en que inicará, si ya inició tomamos la fecha de hoy
    var diasRestantes = Globaljs.getDatesDif ("DAYS",fechaIni, backupFechaFinServ.val());
    lblDiasRestantes.text(diasRestantes>0?'Quedan '+diasRestantes+' días para que se termine el pago del servicio, estos días se agregarán al nuevo periodo de pago que realice.':'El servicio ha caducado');

    fechaIniServ.datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01/01/2019'
        // endDate: '0d'
    }).change(function(event) {
        calcFechaFin();
        //fechaIniServ.valid();
    });
    
    $("#modal-pagarServicio #fechaIniServ").keyup(function(e) {
      calcFechaFin();
    });

    Globaljs.object_setEnabled(false,'backupFechaIniServ');
    Globaljs.object_setEnabled(false,'backupFechaFinServ');
    Globaljs.object_setEnabled(false,'fechaFinServ');

    setFormValidations();
  }

  function guardarPago()
  {
    if(form_pagarServicio.valid() && validarEntradas ()) 
    {
      swal({ title: '¿Está seguro?', text:'¿Confirma que desea guardar las modificaciones al registro?', type: 'warning', showCancelButton: true, 
          confirmButtonColor: '#28a745', cancelButtonColor: '#dc3545', confirmButtonText: 'SI', cancelButtonText: 'NO'
      }).then((result) => 
      {
        if(result.value) 
        {
          block();
          /*$.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });*/
          var formDataPagarServicio = new FormData(form_pagarServicio[0]);
					console.log(formDataPagarServicio);
          formDataPagarServicio.append('fechaFinServ', fechaFinServ.val());
          $.ajax({
            url: form_pagarServicio.attr('action'),
            type: form_pagarServicio.attr('method'),
            data: formDataPagarServicio,  //form.serialize(),
            dataType: 'JSON',
            processData: false, 
            contentType: false,
            success: function(response) {
              unblock();
              if (response.success){
                swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600});
                window.location.href = "{{ route('registrobenef.index') }}"; //"{{ URL::to('atnciudadana/solicitudes') }}"
              }else
                swal({ title: '¡Transacción rechazada!', html:'Registro no almacenado.<br><br>'+response.mensaje, type:'error'});
              //pasajero_create_edit_success(response, method);
            },
            error: function(response) {
              unblock();
              swal({ title: '¡Transacción rechazada!', text:'Registro no almacenado...', type:'error'});
              //pasajero_create_edit_error(response);
            }
          });
        }
      });
    }
  }

  function validarEntradas ()
  {
    if (Globaljs.compareDate(fechaIniServ.val(),"<=",backupFechaFinServ.val()))
      return mostrarWarning("La fecha inicial ("+fechaIniServ.val()+") tiene que ser posterior a la fecha de término del servicio anterior ("+backupFechaFinServ.val()+").");
    if (Globaljs.compareDate(fechaIniServ.val(),"<",Globaljs.getFechaHoy(false)))
      return mostrarWarning("La fecha inicial tiene que ser igual o posterior al día de hoy.");

    if (Globaljs.compareDate(fechaFinServ.val(),"<=",backupFechaFinServ.val()))
            return mostrarWarning("El periodo de pago que intenta establecer ya está cubierto.");
    
    return true;
  }

  function setFormValidations() 
  {
    $.validator.addMethod('pattern_fecha', function (value, element) {
		        return this.optional(element) || erFechaDiaMesAnio.test(value);
    }, 'Formato de fecha incorrecta');

    return form_pagarServicio.validate({
      rules: {
        fechaIniServ: {
          required:true,
          pattern_fecha: true
        },
        folioPago: {
          required:true
        },
      }
    });
  }

  function calcFechaFin()
  {
    var fechaFin = "";
    var fechaIni = "";
    
    if (erFechaDiaMesAnio.test(fechaIniServ.val())){ //formato dd/MM/yyyy o dd-MM-yyyy
      /*HACEMOS CÁLCULOS CON LAS FECHAS DE INICIO Y FIN DE CONTRATO DE SERVICIO*/
      fechaFin = Globaljs.addDaysToDate (fechaIniServ.val(), parseInt(numMesesServ.val())*30);
      fechaIni = (Globaljs.compareDate(Globaljs.getFechaHoy(false),"<=",backupFechaIniServ.val())) ? backupFechaIniServ.val() : Globaljs.getFechaHoy(false); //Por si se diera el caso que el tiempo de contrato todavía no ha iniciado, tomamos el dia en que inicará, si ya inició tomamos la fecha de hoy
      if (Globaljs.compareDate(fechaIni,"<=",backupFechaFinServ.val())){ //Si todavía le faltan días para que termine su contrato
        //let difDias = this.getDatesDif ("DAYS",this.Globaljs.getFechaHoy(false), this.backupFechaFinServ);
        let difDias = Globaljs.getDatesDif ("DAYS",fechaIni, backupFechaFinServ.val());
        fechaFin = Globaljs.addDaysToDate (fechaFin, difDias); //Agregamos los días que le faltan para término de su contrato
      }
    }
    fechaFinServ.val(fechaFin);
    return fechaFin;
  }

  function mostrarWarning (mensaje)
  {
    swal({ title:'¡Precaución!', text: mensaje, type: 'warning' });
    return false;
  }

  return {
    init: init,
    guardarPago: guardarPago
  };
})();

</script>