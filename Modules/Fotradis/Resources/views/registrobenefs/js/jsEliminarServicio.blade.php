<script type="text/javascript">

var EliminarServicio = (function()
{
  var form_eliminarServicio;

  function init(numServicios) 
  {
    form_eliminarServicio = $("#form_eliminarServicio");
    numPasajero = $("#modal-eliminarServicio #numPasajero");
    numPasajeroAmbos = $("#modal-eliminarServicio #numPasajeroAmbos");

    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
    }).on('ifChanged', function (e) {
        
    })

    if (numServicios === 1)
      Globaljs.object_setEnabled (false, 'numPasajero');
    else if (numServicios === 2){
      $('input[type="checkbox"]').prop('checked', false);
      //$(".icheckbox_flat-green").attr("aria-checked","false");
      $(".icheckbox_flat-green").removeClass("checked");
    }
    
  }

  function eliminarServicio(pasajero_id) 
  {
    swal({title:'¿Está seguro de eliminar el registro?', text:'¡El registro se eliminará de forma permanente!', type:'warning',
      showCancelButton: true, confirmButtonColor:'#3085d6', cancelButtonColor:'#d33', confirmButtonText: 'Sí, eliminar', cancelButtonText: 'No, regresar', reverseButtons: true
    }).then((result) => {
      if (result.value) {
        if (validarEntradas ())
        {
          /*$.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
          });*/
          var formDataEliminarServicio = new FormData(form_eliminarServicio[0]);
          formDataEliminarServicio.append('servicioadquirido_id', numPasajero.val());
          formDataEliminarServicio.append('servicioadquiridoAmbos_id', numPasajeroAmbos.val());
          formDataEliminarServicio.append('numPasajero_isChecked', numPasajero.is(':checked'));
          formDataEliminarServicio.append('numPasajeroAmbos_isChecked', typeof(numPasajeroAmbos !== "undefined") ? numPasajeroAmbos.is(':checked') : null);
          
          $.ajax({
            url: form_eliminarServicio.attr('action'),  //"{{ URL::to('regisbenef/destroyservicio') }}" + '/' + pasajero_id,
            type: form_eliminarServicio.attr('method'),
            data: formDataEliminarServicio,  //form.serialize(),
            dataType: 'JSON',
            processData: false, 
            contentType: false,
            success: function(response) {
              /*table.get_table().DataTable().ajax.reload(null, false);
              swal({title: '¡Eliminado!', text: 'El registro ha sido eliminado.', type: 'success', timer: 3000 });*/
              unblock();
              if (response.success){
                swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600});
                window.location.href = "{{ route('registrobenef.index') }}"; //"{{ URL::to('atnciudadana/solicitudes') }}"
              }else
                swal({ title: '¡Transacción rechazada!', html:'Registro no eliminado.<br><br>'+response.mensaje, type:'error'});
            },
            error: function(response) {
              unblock();
              swal({ title: '¡Transacción rechazada!', text:'Registro no eliminado...', type:'error'});
            }
          });
        }
      }
    });
  }

  function validarEntradas ()
  {
    if (numPasajero.is(':checked') === false && numPasajeroAmbos!=="undefined" && numPasajeroAmbos.is(':checked') === false)
      return mostrarWarning("Debe seleccionar almenos un servicio.");
    return true;
  }

  function mostrarWarning (mensaje)
  {
    swal({ title:'¡Precaución!', text: mensaje, type: 'warning' });
    return false;
  }

  return {
    init: init,
    eliminarServicio: eliminarServicio
  };

})();

</script>