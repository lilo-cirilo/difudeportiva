<script type="text/javascript">

$(document).ready(() => 
{
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
	});

  CrearPasajero.init ();
});

var CrearPasajero = (function() 
{
  var config = {
    apiKey: "AIzaSyB7jM0WfuaZdwqfRlE5t-CJ3zfDz11xSUw",
    authDomain: "reginet-7c5b2.firebaseapp.com",
    databaseURL: "https://reginet-7c5b2.firebaseio.com",
    projectId: "reginet-7c5b2",
    storageBucket: "reginet-7c5b2.appspot.com",
    messagingSenderId: "770654201194"
  };

  var form_pasajero = $('#form_pasajero');
  var action = form_pasajero.attr('action');
  var method = form_pasajero.attr('method');

  var mAuth, storageRef;

  function init() 
  {
    form = $('#form_pasajero'),
    action = form_pasajero.attr('action'),
    method = form_pasajero.attr('method');

    $('.urban').hide();

    $('#tipoServicio').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getTiposServicio') }}", 
        dataType: "json",
        data: params => {
          return {
            search: params.term
            //tipo: $('#tipo').val()
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(function(event){
      ocultarDatosTaxi (event);
    });

    $('#tipoPasajero').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getTiposPasajero') }}", 
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    });

    $('#fechaIniServ').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01/01/2019',
        orientation: 'bottom',
        currentText: "Now"
    }).change(function(event) {
      //$(".datepicker").valid();
    });

    $('#lugarPago').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getLugaresPago') }}", 
        dataType: "json",
        data: params => {
          return {
            search: params.term
            //tipo: $('#tipo').val()
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    });

    $('#pais').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getPaises') }}", 
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    }).change(function(event){
      if ($('#pais').select2('data')[0].text !== "MÉXICO"){
        $('#select2-estado-container').html("").trigger('change.select2');
        Globaljs.object_setEnabled(false,'estado');
      }else
        Globaljs.object_setEnabled(true,'estado');
    });

    $('#estado').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getEntidades') }}", //entidades.select
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.entidad,
                slug: item.entidad,
                results: item
              }
            })
          };
        },
        cache: true
      },
       /*placeholder: {
        id: '20', // the value of the option
        text: 'OAXACA'
      }*/
    });

    $('#municipio').select2({
      language: 'es',
      ajax: {
        type: "GET",
        url: "{{ route('registrobenef.getMunicipios') }}", //municipios.select
        dataType: "json",
        data: params => {
          return {
            search: params.term
          };
        },
        processResults: (data, params) => {
          params.page = params.page || 1;
          return {
            results: $.map(data, item => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          };
        },
        cache: true
      }
    });

    $('#pais').append(new Option("MÉXICO", 165, true, true)).trigger('change');
    $('#estado').append(new Option("OAXACA", 20, true, true)).trigger('change');

    setFormValidations();
    
    app.to_upper_case();

    // Para la conexión a firebase
    firebase.initializeApp(config);
    mAuth = firebase.auth().signInWithEmailAndPassword('angelbrian.inc@gmail.com', 'scodelario1').catch(function(error) {
      // Encargarse de errores aquí
      var errorCode = error.code;
      var errorMessage = error.message;
    });
    var storage = firebase.storage();
    storageRef = storage.ref();
  }

  function setFormValidations()
  {
    var erFechaAnioMesDia= /^(19|20)([0-9]{2})(\/)(0[1-9]|1[0-2])\3([0-2][0-9]|3[0-1])$/;
    var erFechaDiaMesAnio= /^([0-2][0-9]|3[0-1])(\/)(0[1-9]|1[0-2])\2(19|20)([0-9]{2})$/;


    $.extend($.validator.messages, {
      required: 'Este campo es obligatorio.',
      remote: 'Por favor, rellena este campo.',
      email: 'Por favor, escribe una dirección de correo válida.',
      url: 'Por favor, escribe una URL válida.',
      date: 'Por favor, escribe una fecha válida.',
      dateISO: 'Por favor, escribe una fecha (ISO) válida.',
      number: 'Por favor, escribe un número válido.',
      digits: 'Por favor, escribe sólo dígitos.',
      creditcard: 'Por favor, escribe un número de tarjeta válido.',
      equalTo: 'Por favor, escribe el mismo valor de nuevo.',
      extension: 'Por favor, escribe un valor con una extensión aceptada.',
      maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
      minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
      rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
      range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
      max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
      min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
      nifES: 'Por favor, escribe un NIF válido.',
      nieES: 'Por favor, escribe un NIE válido.',
      cifES: 'Por favor, escribe un CIF válido.'
    });

    $.validator.setDefaults({
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorPlacement: function(error, element) {
        $(element).parents('.form-group').append(error);
      }
    });


    $.validator.addMethod('pattern_fechaIniServ', function(value, element) {
      return this.optional(element) || erFechaDiaMesAnio.test(value);
    }, 'El formato de fecha no es correcto');

    $.validator.addMethod('pattern_numPasajero', function(value, element) {
      return this.optional(element) || /^[A-Ca-c][0-9]{3,4}$/.test(value);
    }, 'Una letra (A, B o C) y tres o cuatro dígitos');

    $.validator.addMethod('pattern_nombre', function(value, element) {
      return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]{2,})(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
    }, 'Solo letras y espacios entre palabras');
    
    $.validator.addMethod('pattern_apellidoRequerido', function(value, element) {
      return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{2,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
    }, 'El apellido no es válido.');

    $.validator.addMethod('pattern_apellido', function(value, element) {
      return this.optional(element) || /(?:^$)|(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{2,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
    }, 'El apellido no es válido.');

    $.validator.addMethod('pattern_curp',function(value, element) {
      return this.optional(element) || /(?:^$)|[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/i.test(value);
    }, 'El formato de la CURP no es válido.');
    
    $.validator.addMethod('pattern_telefono',function(value, element) {
      return this.optional(element) || /(^[0-9]{7}$)|(^[0-9]{10}$)/.test(value);
    }, 'El teléfono es de 7 ó 10 dígitos.');

    $.validator.addMethod('pattern_numero',function(value, element) {
      return this.optional(element) || /(^[0-9]{0,}$)/.test(value);
    }, 'El número no es válido.');


    return form_pasajero.validate({
      rules: {
        tipoServicio:{ required: true },
        tipoPasajero:{ required: true },
        fechaIniServ:{ required: true, pattern_fechaIniServ:true },
        numMesesServ:{ required: true },
        folioPago:{ required: true },
        lugarPago:{ required: true },
        numPasajero:{ required: true, pattern_numPasajero:true },
        numPasajeroAmbos:{ required: true, pattern_numPasajero:true },
        nombre:{ required: true, pattern_nombre:true },
        primerApe:{ required: true, pattern_apellidoRequerido:true },
        segundoApe:{ pattern_apellido:true },
        curp:{ required: true, pattern_curp:true },
        telefono:{ required: true, pattern_telefono:true },
        pais:{ required: true },
        estado:{ required: true },
        municipio:{ required: true },
        numero:{ pattern_numero:true },
        persona_emergencias:{ pattern_apellido:true },
        telefono_emergencias:{ pattern_telefono:true },
        cantidad: { required: true, minlength: 1, pattern_folios: 'solo numeros, X si no dispone de uno.' }
      }
    });
  }

  function guardarPasajero() 
  {
    if(form_pasajero.valid() && validarEntradas()) 
    {
      swal({ title: '¿Está seguro?', text:'¿Confirma que desea guardar las modificaciones al registro?', type: 'warning', showCancelButton: true, 
          confirmButtonColor: '#28a745', cancelButtonColor: '#dc3545', confirmButtonText: 'SI', cancelButtonText: 'NO'
      }).then((result) => 
      {
        if(result.value) {
          var formDataPasajero = new FormData(form_pasajero[0]);
          formDataPasajero.append('foto', '');
          
          block();
          $.ajax({
            url:  form_pasajero.attr('action'),
            type: form_pasajero.attr('method'),
            data: formDataPasajero, //form.serialize()
            dataType: 'JSON',
            processData: false, 
            contentType: false,
            success: function(response) 
            {
              unblock();
              if (response.success){
                if (response.yaTieneFoto === 'no')
                  guardarFotoPasajeroEnFirebase($('#numPasajero').val(), response.rutaPeticion);
                else {
                  swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600 });
                  window.location.href = "{{ route('registrobenef.index') }}"; //"{{ URL::to('atnciudadana/solicitudes') }}"
                }
              }else
                swal({ title: '¡Transacción rechazada!', html:'Registro no almacenado.<br><br>'+response.mensaje, type:'error' });
              //showMessageSuccess(response, method);
            },
            error: function(response) {
              unblock();
              if (typeof(response.responseJSON) !== "undefined" && (response.responseJSON.message !== null || typeof(response.responseJSON.message) !== "undefined")  && response.responseJSON.message !== "")
                swal({ title: '¡Transacción rechazada!', html:'Registro no almacenado.<br><br>'+response.responseJSON.message, type:'error' });
              else
              swal({ title: '¡Transacción rechazada!', text:'Registro no almacenado...', type:'error' });
              //showMessageError(response);
            }
          });
        }
      });
    }
  }

  function guardarFotoPasajeroEnFirebase(numPasajero, rutaPeticion) 
  {
    var image64 = document.getElementById('imagen').src;

    new Promise ( (resolve, rejected) =>
    {
      block();
      if (image64 !== null)
      {
        var uploadTask = storageRef.child(`fotos/${numPasajero}`).putString(image64, 'data_url', {contentType:'image/jpg'});
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // // Escuchamos el estado de los cambios, errores, y completado de la actualización o 'state_changed'
          function(snapshot) {
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100; // Obtenemos el progreso de la tarea, incluyendo el número de bytes subidos y el total de númberos de bytes a ser actualizados
            //console.log(progress +'% actualizado');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // o 'pausado'
                console.log('Actualización pausada');
                break;
              case firebase.storage.TaskState.RUNNING: // o 'subiendo'
                // console.log('Actualización en proceso');
                break;
            }
          }, function(error) {
            console.log('Error al guardar la foto: '+error.code);
            switch (error.code) {
              case 'storage/unauthorized': console.log('¡Error! No hay permiso para acceder al objeto');// El usuario no tiene permiso de acceder al objeto
                break;
              case 'storage/canceled': console.log('¡Error! Actualización cancelada');// El usuario canceló la actualización
                break;
              case 'storage/unknown': console.log('Error desconocido');// Error desconocido, error inseperado error.serverResponse
                break;
            }
            resolve("");
          }, function() { // Actualización completada satisfactoriamente, ahora podemos obtener la url de descarga
            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
              resolve(downloadURL);
            });
          });
      } else
        resolve("");
    }).then((downloadURL)=>{
      unblock();
      console.log(downloadURL);
      actualizarFotoEnBD (downloadURL, rutaPeticion)
    }).catch(function(err) {
      unblock();
      swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600 });
      window.location.href = "{{ route('registrobenef.index') }}";
      console.log("Error al guardar la foto:", err);
    });

  }

  function actualizarFotoEnBD (rutaImagen, rutaPeticion)
  {
    block();
    $.ajax({
      url:  rutaPeticion,
      type: 'PUT',
      data: {foto:rutaImagen}, //form.serialize()
      dataType: 'JSON',
      //processData: false, 
      //contentType: false,
      success: function(response) 
      {
        unblock();
        if (response.success){
          swal({ title:'¡Transacción exitosa!', text: 'Registro almacenado...', type: 'success', timer: 2600 });
          window.location.href = "{{ route('registrobenef.index') }}"; //"{{ URL::to('atnciudadana/solicitudes') }}"
        }else
          swal({ title: '¡Precaución!', html:'La fotografía no se pudo actualizar.<br><br>'+response.mensaje, type:'error' });
      },
      error: function(response) {
        unblock();
        swal({ title: '¡Precaución!', html:'La fotografía no se pudo actualizar.', type:'error', timer: 3600  });
      }
    });
  }

  function ocultarDatosTaxi (event)
  {
    $('#numMesesServ').val('1').trigger('change.select2');
    $("#folioPago").val('');
    $("#numPasajeroAmbos").val('');

    if($('#tipoServicio').select2('data')[0].text === 'URBAN')
    {
      $('.urban').hide();
      $('#pnlTipoServicio').removeClass("col-lg-4"); $('#pnlTipoServicio').addClass("col-lg-6");
      $('#pnlTipoServicio').removeClass("col-md-4"); $('#pnlTipoServicio').addClass("col-md-6");
      $('#pnlNumPasajero').removeClass("col-lg-4"); $('#pnlNumPasajero').addClass("col-lg-6");
      $('#pnlNumPasajero').removeClass("col-md-4"); $('#pnlNumPasajero').addClass("col-md-6");
    }else if( $('#tipoServicio').select2('data')[0].text === 'TAXI' )
    {
      $('.urban').show();
      $('.taxi').hide();
      $('#pnlTipoServicio').removeClass("col-lg-4"); $('#pnlTipoServicio').addClass("col-lg-6");
      $('#pnlTipoServicio').removeClass("col-md-4"); $('#pnlTipoServicio').addClass("col-md-6");
      $('#pnlNumPasajero').removeClass("col-lg-4"); $('#pnlNumPasajero').addClass("col-lg-6");
      $('#pnlNumPasajero').removeClass("col-md-4"); $('#pnlNumPasajero').addClass("col-md-6");
    }else if($('#tipoServicio').select2('data')[0].text === 'AMBOS'){
      $('.urban').show();
      $('#pnlTipoServicio').removeClass("col-lg-6"); $('#pnlTipoServicio').addClass("col-lg-4");
      $('#pnlTipoServicio').removeClass("col-md-6"); $('#pnlTipoServicio').addClass("col-md-4");
      $('#pnlNumPasajero').removeClass("col-lg-6"); $('#pnlNumPasajero').addClass("col-lg-4");
      $('#pnlNumPasajero').removeClass("col-md-6"); $('#pnlNumPasajero').addClass("col-md-4");
    }
  }

  function validarEntradas() 
  {
    if ( $('#tipoServicio').select2('data')[0].text === 'AMBOS' )
      if ($('#numPasajero').val().substring(0, 1) === $('#numPasajeroAmbos').val().substring(0, 1)){
        swal({ title: '¡Error!', html:'No puede repetir la letra para los dos números de pasajero.', type:'error' });
        return false
      }
    return true;
  }

  /*function showMessageSuccess (response, method) 
  {
    var mensaje = (method === "POST") ? 'registrado' : 'actualizado';

    swal({ title:'¡Correcto!', text:'Pasajero ' + mensaje + '.', type: 'success', timer: 2000 });
    table.get_table().DataTable().ajax.reload(null, false);
    app.set_bloqueo(false);
    unblock();
    cerrar_modal('modal-regispasajero');
  }

  function showMessageError(response) 
  {
      unblock();
      if(response.status === 422) {
        swal({
          title: 'Error al registrar el pasajero.',				
          text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
          type: 'error',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Regresar',
          allowEscapeKey: false,
          allowOutsideClick: false
        });
      }
  }*/

  return {
    init: init,
    guardarPasajero: guardarPasajero
  }
  
})();
   
var fotoPasajero = (function()
{
  var foto = $("#foto");
  var imagen = $('#imagen');

  function init() 
  {
    foto = $("#foto");
    imagen = $('#imagen');
  }

  function ver() 
  {
    init();
    $("#imagen").attr('src', imagen.attr('src'));
    $("#modal_ver_fotopasajero").modal("show");
  };

  function agregar() 
  {
    init();
    foto.click();
    editar();
  };

  function editar() 
  {
    init();
    foto.change(function() {
      var file = this.files[0];
      var reader = new FileReader();
      reader.onloadend = function() {
        imagen.attr('src', reader.result);
      };
      reader.onerror = function() {
      };
      if(file)
        reader.readAsDataURL(file);
      else {  }
    });
  };

  function eliminar() 
  {
    init();
    imagen.attr('src', "{{ asset('images/no-image.png') }}");
  };

  return { ver:ver, agregar:agregar, editar: editar, eliminar: eliminar };
})();


</script>