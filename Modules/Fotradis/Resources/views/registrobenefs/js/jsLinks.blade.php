<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<!--script type="text/javascript" src="vendor/datatables/buttons.server-side.js"></script-->

<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>

{{--<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-app.js"></script>--}}
{{--<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-auth.js"></script>--}}
{{--<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-database.js"></script>--}}
{{--<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-storage.js"></script>--}}
<script src="https://www.gstatic.com/firebasejs/5.8.2/firebase.js"></script>

{{--<script src="{{asset('js/firebase-app.js')}}"></script>-}}
{{--<script src="{{asset('js/firebase-auth.js')}}"></script>-}}
{{--<script src="{{asset('js/firebase-database.js')}}"></script>-}}
{{--<script src="{{asset('js/firebase-storage.js')}}"></script>--}}