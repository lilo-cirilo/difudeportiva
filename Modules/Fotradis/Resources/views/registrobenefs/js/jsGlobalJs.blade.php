<script type="text/javascript">

var Globaljs = (function()
{
  var erFechaDiaMesAnio = /^([0-2][0-9]|3[0-1])(\/)(0[1-9]|1[0-2])\2(19|20)([0-9]{2})$/;
  var erFechaAnioMesDia = /^(19|20)([0-9]{2})(\/)(0[1-9]|1[0-2])\3([0-2][0-9]|3[0-1])$/;
  var erFechaDiaMesAnio_gion = /^([0-2][0-9]|3[0-1])(-)(0[1-9]|1[0-2])\2(19|20)([0-9]{2})$/;
  var erFechaDiaMesAnio_diag = /^([0-2][0-9]|3[0-1])(\/)(0[1-9]|1[0-2])\2(19|20)([0-9]{2})$/;
  var erFechaAnioMesDia_gion = /^(19|20)([0-9]{2})(-)(0[1-9]|1[0-2])\3([0-2][0-9]|3[0-1])$/;
  var erFechaAnioMesDia_diag = /^(19|20)([0-9]{2})(\/)(0[1-9]|1[0-2])\3([0-2][0-9]|3[0-1])$/;
	
  //Le suma el número de días a una fecha proporcionada en formato dd/MM/yyyy o dd-MM-yyyy
  function addDaysToDate (strfecha, numDias)
  {
    var dateValues=strfecha.split(strfecha.substr(2,1));
    
    //la fecha
    var newDate = new Date(dateValues[2]+"/"+dateValues[1]+"/"+dateValues[0]); //Transformamos la fecha en MM/dd/yyyy, pues así la usa new Date
    //dias a sumar
    var dias = parseInt(numDias);
    //nueva fecha sumada
    newDate.setDate(newDate.getDate() + dias);

    /* Agregamos cero a la izquierda si lo amerita el día y el mes */
    var dd = newDate.getDate();
    var mm = newDate.getMonth()+1; //Enero es 0!
    if(dd<10)
        dd='0'+dd;
    if(mm<10)
        mm='0'+mm;

    //formato de salida para la fecha
    return dd + '/' + mm + '/' + newDate.getFullYear();
  }


  /**
     * Funcion que dadas dos fechas, determina si la inicial es IGUAL, MENOR, MAYOR, MENOR_IGUAL, MAYOR_IGUAL con respecto a la segunda. Si se define una tercera fecha, determina si esta tercera está entre la primera y la segunda.
     * Tiene que recibir las fechas en formato español dd/mm/yyyy
     * No valida que las fechas sean correctas
     * Devuelve true o false, según el caso que se le indique a verificar
     *
     * Para validar si una fecha es correcta, utilizar la función:
     * http://www.lawebdelprogramador.com/codigo/JavaScript/1757-Validar_una_fecha.html
  */
  function compareDate(fechaInicial,comparacion_O_fechaIntermedia, fechaFinal)
  {
    var valuesStart=fechaInicial.split(fechaInicial.substr(2,1));
    var valuesEnd=fechaFinal.split(fechaFinal.substr(2,1));
    // Verificamos que la fecha no sea posterior a la actual
    var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]).getTime();
    var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]).getTime();
    
    if (erFechaDiaMesAnio.test(comparacion_O_fechaIntermedia)) //Si está definida la variable fechaIntermedia calculamos si esa fecha es intermedia entre fechaInicial y fechaFinal
    {
      var valuesMiddle=fechaIntermedia.split(strfecha.substr(2,1));
      var middleDate=new Date(valuesMiddle[2],(valuesMiddle[1]-1),valuesMiddle[0]).getTime();

      switch(tipoComparacion){
        case "INTERMEDIA": return dateStart<=middleDate && middleDate<=dateEnd; break;
      }
    }else{
      switch(comparacion_O_fechaIntermedia){
        case "===": return dateStart===dateEnd; break;
        case "<": return dateStart<dateEnd; break;
        case ">": return dateStart>dateEnd; break;
        case "<=": return dateStart<=dateEnd; break;
        case ">=": return dateStart>=dateEnd; break;
      }
    }
    return false;
  }

  function getFechaHoy(incluirHora)
  {
    var today = new Date();

    var dd = today.getDate();
    var mm = today.getMonth()+1; //Enero es 0!
    var yyyy = today.getFullYear();
    var hour = today.getHours();
    var minute = today.getMinutes();
    var seconds = today.getSeconds();

    if(dd<10)
        dd='0'+dd;
    if(mm<10)
        mm='0'+mm;
    if(hour<10)
        hour='0'+hour;
    if(minute<10)
        minute='0'+minute;
    if(seconds<10)
        seconds='0'+seconds;

    if (typeof(incluirHora)!=="undefined" && incluirHora===true )
      return dd+'/'+mm+'/'+yyyy+' '+hour+':'+minute+':'+seconds;
    else
      return dd+'/'+mm+'/'+yyyy;
  }

    /*Invierte una fecha. Si está en formato dd/MM/yyyy la pone como yyyy/MM/dd */
  function  reverseDate (fecha, separador, nuevoSeparador)
  {
      var nuevaFecha="";
      
      if (separador===null || typeof(separador)==="undefined" || separador === "")   //Si no se especifica un separador el default será '/'
          separador = "/";
      if (fecha==="")
          return "";
      var values = fecha.split(separador);
      if (nuevoSeparador!==null && typeof(nuevoSeparador)!=="undefined" && nuevoSeparador !== "")   //Si se desea cambiar el separador
          separador = nuevoSeparador;
      //Verificamos que haya un formato correcto
      if (fecha.length===10 && values.length===3)
          nuevaFecha = values[2]+separador+values[1]+separador+values[0];
      return nuevaFecha;
  }

  /*Devuelve la diferencia de fechas según el caso especificado dadas dos fechas en formato dd/MM/yyyy*/
  function getDatesDif (caso,iniDate, endDate)
  {
    var valoresFechaIni = iniDate.split(iniDate.substr(2,1));
    var valoresFehaFin = endDate.split(endDate.substr(2,1));
    var fechaInicio = new Date(valoresFechaIni[2], valoresFechaIni[1]-1, valoresFechaIni[0]);
    var fechaFin = new Date(valoresFehaFin[2], valoresFehaFin[1]-1, valoresFehaFin[0]);

    var minutes = 1000 * 60; //Milisegundos por segundos
    var hours = minutes * 60;
    var days = hours * 24;
    var years = days * 365;

    var diffFechas = fechaFin - fechaInicio;

    switch (caso){
      case "DAYS": return Math.trunc(diffFechas/days); break;
      case "YEARS": return Math.trunc(diffFechas/years); break;
      default: return Math.trunc(diffFechas/days); break; //Por default retorna en días
    }
  }

  //Los elementos en general que aceptan enabled o disabled son: button, input, optgroup, option, select, y textarea.
  function object_setEnabled (enabled, nombreObjeto)
  {
      if (enabled === true)
          $("#"+nombreObjeto).removeAttr("disabled");
      else
          $("#"+nombreObjeto).attr("disabled", "disabled");
          
  }
  function object_setVisible (enabled, nombreObjeto)
  {
      if (enabled === true)
          $("#"+nombreObjeto).css("display","");
      else
          $("#"+nombreObjeto).css("display","none");
  }

	function object_exists (nombreObjeto) {
		if ($('#'+nombreObjeto).length)
			return true;
		else
			false;
	}

	// Convierte una imágen que es cargada a través de una url o base 64
	//   url: La url donde está la imagen a cargar.
	//   content_type: El formato de imagen ('image/png', etc). Png es el default por lo tanto content_type podría ir en blanco, si la imagen no es formato png hay que especificar.
	//   callback: Una función o una promesa.
	//   ejemplo de uso: Globaljs.image_link_to_b64('url...', 'image/png', function(b64) { /* acciones...; */ });
	var image_url_to_b64 = function(url, content_type, callback) {
			var image = new Image();
			image.crossOrigin = 'Anonymous';
			image.onload = function() {
					var canvas = document.createElement('CANVAS');
					var context = canvas.getContext('2d');
					var data_url;
					canvas.height = this.naturalHeight; // or this.height;  if you want a special/scaled size. This se refiere a la imagen
					canvas.width =  this.naturalWidth;  // or this.width; if you want a special/scaled size. This se refiere a la imagen
					context.drawImage(this, 0, 0);
					data_url = canvas.toDataURL(content_type); //
					
					// get as Data URI
					callback(data_url);  
					// ... or Get raw image data
					// callback(data_url.replace(/^data:image\/(png|jpg);base64,/, ''));
					canvas = null;
			};
			image.src = url;
	};

  return {
    erFechaDiaMesAnio : erFechaDiaMesAnio,
    erFechaAnioMesDia : erFechaAnioMesDia,
    erFechaDiaMesAnio_gion : erFechaDiaMesAnio_gion,
    erFechaDiaMesAnio_diag : erFechaDiaMesAnio_diag,
    erFechaAnioMesDia_gion : erFechaAnioMesDia_gion,
    erFechaAnioMesDia_diag : erFechaAnioMesDia_diag,

    addDaysToDate: addDaysToDate,
    compareDate: compareDate,
    getFechaHoy: getFechaHoy,
    reverseDate: reverseDate,
    getDatesDif: getDatesDif,
    object_setEnabled: object_setEnabled,
    object_setVisible: object_setVisible,
		object_exists: object_exists,
		image_url_to_b64: image_url_to_b64
  };

})();

var app = (function() 
{
  var bloqueo = false;

  function to_upper_case() 
  {
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  };

  function set_bloqueo(valor) {
    bloqueo = valor;
  };

  function get_bloqueo() {
    return bloqueo;
  };

  function agregar_bloqueo_pagina() {
    $('form :input').change(function() {
      bloqueo = true;
    });

    window.onbeforeunload = function(e) {
      if(bloqueo)
        return '¿Estás seguro de salir?';
    };
  };

  return {
    to_upper_case: to_upper_case,
    set_bloqueo: set_bloqueo,
    agregar_bloqueo_pagina: agregar_bloqueo_pagina
  };
})();

</script>