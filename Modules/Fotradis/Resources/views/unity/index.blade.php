@extends('fotradis::layouts.master')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('li-breadcrumbs')
    <li class="active">Unidades actuales</li>
@endsection

@section('content')

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box box-primary shadow">
        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title">Unidades Actuales</h3>
            <div class="box-tools pull-right">
              <a href="{{ route('unidad.show', ['null', 'accion' => 'add']) }}" class="btn btn-box-tool" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar unidad</a>
            </div>
					</div>
					<div class="box-body">
						<!-- /.box-header -->
						<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
							<input type="text" id="search" name="search" class="form-control">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
							</span>
						</div>
				   	{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'unidades_actuales', 'name' => 'unidades_actuales', 'style' => 'width: 100%']) !!}
				  </div>
    		</div>
			</div>
    </div>
	</div>
	<div id="prueba"></div>
</section>

@include('fotradis::unity.partials.ver_unidad')
@include('fotradis::unity.partials.agregar_mantenimiento')
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

@include('fotradis::unity.js.unidades')
<script type="text/javascript">
	var modal_paradas = $("#modal_nueva_unidad");
	swal({
			title: 'Unidades asignadas a ruta y conductor',
			text: '¡No se pueden eliminar!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});

	$(document).ready(function() {
		block();
		var table = (function() {
			var tabla = undefined,
			btn_buscar = $('#btn_buscar'),
			search = $('#search');

			function init() {			
				search.keypress(function(e) {
					if(e.which === 13) {
						tabla.DataTable().search(search.val()).draw();
					}
				});

				btn_buscar.on('click', function() {
					tabla.DataTable().search(search.val()).draw();
				});
			};

			function set_table(valor) {
				tabla = $(valor);
			};

			function get_table() {
				return tabla;
			};

			return {
				init: init,
				set_table: set_table,
				get_table: get_table
			};
		})();
		
		$('.datepicker').datepicker({
  		autoclose: true,
        language: 'es',
        format: 'yyyy-mm-dd'
  	}).change(function(event) {
  	})

		table.set_table($('#unidades_actuales').dataTable());
		table.init();
	    unblock();
	});

	function borrar_unidad(control_rutas_id) {
		swal({
			title: '¿Estas seguro?',
			text: '¡Usted no podrá recuperar este registró!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			confirmButtonText: 'SI',
			cancelButtonText: 'NO'
		}).then((result) => {
			if(result.value) {

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: "{{ URL::to('fotradis/rutas') }}" + '/' + control_rutas_id,
					type: 'DELETE',
					success: function(response) {
						tabla.ajax.reload();

						unblock();

						swal({
							title: '¡Eliminado!',
							text: 'Su registro ha sido eliminado..',
							type: 'success',
							timer: 1500
						});
					},
					error: function(response) {
						swal({
							title: '¡No se pudo realizar el registro!',
							text: 'Su registro no ha sido eliminado..',
							type: 'error',
							timer: 1500
						});
						unblock();
					}
				});

			}
		});
	}

	function editar_unidad(matricula) {

	}

	function show_stops_route(ruta_id) 
	{
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			url: "{{ URL::to('fotradis/rutas') }}" + '/' + ruta_id,
			type: 'GET',
			success: function(response) {
				unblock();
				$("#modal_titulo").text("Ruta " + response.nombre_ruta);
				modal_paradas.modal("show");
			},
			error: function(response) {
				swal({
					title: '¡No se pudo obtener datos de ruta!',
					text: '',
					type: 'error',
					timer: 1500
				});
				unblock();
			}
		});
	}
	
	var unidadMant
	
	function addMantenimiento(unidad, accion) {
		var form = $('#form_mantenimiento')
		if(accion == 0) {
			form[0].reset()
			$('#modal_agregar_mantenimiento').modal('show')
			unidadMant = unidad
		} else if(accion == 1) {
			$.validator.addMethod('pattern_numeros', function (value, element) {
		        return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value)
			}, 'Solo números')

			form.validate({
				rules: {
					descripcion: {
						required:true
					},
					tipo: {
						required:true
					},
					costo: {
						required:true,
						pattern_numeros: 'Sólo números'
					},
					fecha: {
						required:true
					}
				}
			})

			if(form.valid()) {
				swal({
					title: '¿Estas seguro?',
					text:  '¡Se registrará un nuevo mantenimiento/servicio!',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#28a745',
					cancelButtonColor: '#dc3545',
					confirmButtonText: 'SI',
					cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						var formData = new FormData(form[0])
						formData.append('unidad_id',unidadMant)
						$.ajax({
							url:  'mantenimiento',
							type: 'POST',
							data: formData,
							dataType: 'JSON',
							processData: false, 
							contentType: false,
							success: function(response) {
								$('#modal_agregar_mantenimiento').modal('hide')
								swal({
									title: '¡Exito!',
									text: "Se añadio un registro de mantenimiento para la unidad " + unidadMant,
									type: 'info',
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'OK'
								})
							},
							error: function(response) {
								swal({
									title: '¡Transacción rechazada!',
									text: 'Registro no almacenado...',
									type: 'error'
								})
							}
						})
					}
				})
			}
		}

	}

	function listMantenimiento(unidad) {
		$.get('mantenimiento/'+unidad, function(data) {
		})
		.done(function(data) {
			//console.log(data.dt)
			$('#prueba').html(data.dt)
			$("#modal_lista_mantenimiento").modal('show')
			$('#modal_lista_mantenimiento').on('shown.bs.modal', function (event) {
				if ( ! $.fn.DataTable.isDataTable( '#lista_mantenimiento' ) ) {
					tabla_mantenimiento_unidad()
				}
			})
			
		})
		.fail(function(data) {			
            console.log(0)
		})
	}

	function tabla_mantenimiento_unidad() {
		$("#lista_mantenimiento").DataTable({
			language: {
				url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			},
			dom: 'itp',
			lengthMenu: [ [3], [3] ],
			responsive: true,
			autoWidth: true,
			processing: true,
			columnDefs: [
				{ className: 'text-center', 'targets': '_all' }
			]
		});            
	}
</script>
@endpush