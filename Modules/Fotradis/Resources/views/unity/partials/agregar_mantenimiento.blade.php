<div class="modal fade" id="modal_agregar_mantenimiento" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-info">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="input_enable()">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Agregar datos de servicio o mantenimiento</h4></center>
            </div>
            <div class="modal-body">
                <form id="form_mantenimiento" action="#" method="#">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="fecha_mantenimiento">Fecha de entrega para reparación*</label>
                            <input type="text" class="form-control datepicker" id="fecha_mantenimiento" name="fecha_mantenimiento" placeholder="Fecha">
                        </div>
                        </div>
                    
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="costo">Costo (en pesos)*</label>
                            <input type="text" class="form-control" id="costo" name="costo" placeholder="Costo">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="mantenimiento_id">Tipo*</label>
                            <select id="mantenimiento_id" name="mantenimiento_id" class="form-control select2" style="width: 100%;">
                                <option value="" disabled selected>SELECCIONE UN MOTIVO...</option>
                                <option value="1">REPARACIÓN</option>
                                <option value="2">SERVICIO ELÉCTRICO</option>
                                <option value="3">SERVICIO MECÁNICO</option>
                                <option value="4">SERVICIO DE GARANTÍA</option>
                            </select>
                        </div>
                        </div>
                    
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="descripcion">Descripción*</label>
                                <textarea type="text" class="form-control" id="descripcion" name="descripcion" placeholder="DESCRIPCIÓN"></textarea>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" onclick="input_enable();" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-default pull-rigth" onclick="addMantenimiento(0,1);">Agregar</button>
            </div>
        </div>
    </div>
</div>