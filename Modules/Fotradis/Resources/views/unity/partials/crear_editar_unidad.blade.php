{{-- Formulario unidad --}}
<form id="form_unidad" enctype="multipart/form-data" action="{{ isset($unidad) ? route('unidad.update', $unidad->id) : route('unidad.store') }}" method="POST">
  <input id="archivo_poliza" name="_token" type="hidden" value="{{ csrf_token() }}" />
  
  <hr>

  <h5>Datos Generales:</h5>

  <hr>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_unidad">Número de unidad*</label>
      <input type="text" class="form-control" id="numero_unidad" name="numero_unidad" placeholder="Número unidad" value="{{isset($unidad->numero_unidad) ? $unidad->numero_unidad : ''}}" {{isset($en_asignacion) == true || isset($ver) == true ? 'disabled' : ''}}>
    </div>
    </div>
    <div id="html_clave_vehicular" class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="matricula">Matrícula*</label>
      <input type="text" class="form-control" id="matricula" name="matricula" placeholder="Matrícula" value="{{isset($unidad->matricula) ? $unidad->matricula : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="modelo">Modelo*</label>
        <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Modelo" value="{{isset($unidad->modelo) ? $unidad->modelo : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="marca">Marca*</label>
        <input type="text" class="form-control" id="marca" name="marca" placeholder="Marca" value="{{isset($unidad->marca) ? $unidad->marca : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group" id="mostrar_anio">
        <label for="anio">Año*</label>
          <select class="form-control" id="anio" name="anio" {{isset($ver) ? 'disabled' : ''}}>
            <option value="" disabled selected>Seleccione año...</option>
            @for ($i = 1990; $i <= $anio_actual; $i++)
              @if(isset($unidad->anio))
                @if($unidad->anio == $i)
                  <option value="{{ $i }}" selected="true">{{ $i }}</option>
                @else
                  <option value="{{ $i }}">{{ $i }}</option>
                @endif
              @else
                <option value="{{ $i }}">{{ $i }}</option>
              @endif
            @endfor

        </select>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="condiciones">Condiciones físicas</label>
        <textarea class="form-control" rows="3" id="condiciones" name="condiciones" {{isset($ver) ? 'disabled' : ''}}>{{isset($unidad->condiciones) ? $unidad->condiciones : ''}}</textarea>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="observaciones">Observaciones de funcionamiento</label>
        <textarea class="form-control" rows="3" id="observaciones" name="observaciones" {{isset($ver) ? 'disabled' : ''}}>{{isset($unidad->observaciones) ? $unidad->observaciones : ''}}</textarea>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="kilometraje_inicial">Kilometraje inicial</label>
        <input type="text" class="form-control" id="kilometraje_inicial" name="kilometraje_inicial" placeholder="Kilometraje inicial" value="{{isset($unidad->kilometraje_inicial) ? $unidad->kilometraje_inicial : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group" id="mostrar_tipo_servicio">
        <label>Tipo de servicio*</label>
        <select class="form-control" id="tipo_servicio" name="tipo_servicio" {{isset($ver) == true || isset($en_asignacion) == true ? 'disabled' : ''}}>
              <option value="" disabled selected>Seleccione un servicio...</option>
              <option value="taxi">Taxi</option>
              <option value="urban">Urban</option>
              @if(isset($tipo_servicio))
                  <script>
                    var servicio = "{{ $unidad->tipo_servicio }}";
                    $("#tipo_servicio option[value=" + servicio + "]").attr("selected",true);
                  </script>
              @endif
        </select>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
      <div class="form-group">
        <label for="poliza_garantia">Poliza de garantía</label>
        <div class="form-group" >
            <div class="hovereffect">
                <img src="{{ isset($unidad->poliza_garantia) ? asset($unidad->poliza_garantia) :  asset('images/no-image.png')}}" class="center-block" id="imagen" name="imagen" height="200dp" width="400dp" {{isset($ver) ? 'disabled' : ''}}>
                <div class="overlay">
                    <h2>Acciones</h2>
                    @if(isset($ver) == false)
                      <button type="button" class="btn btn-success" onclick="poliza_fotografia.agregar();"><i class="fa fa-database"></i> Agregar</button>
                      <button type="button" class="btn btn-danger" onclick="poliza_fotografia.eliminar()"><i class="fa fa-trash"></i> Eliminar</button>
                    @endif
                    <button type="button" class="btn btn-info" onclick="poliza_fotografia.ver()"><i class="fa fa-eye"></i> Ver</button>
                    <input id="poliza_garantia" name="archivo" style="display: none;" type="file">
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <h5>Datos Específicos:</h5>

  <hr>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_serie">Número de serie*</label>
      <input type="text" class="form-control" id="numero_serie" name="numero_serie" placeholder="Número de serie" value="{{isset($unidad->numero_serie) ? $unidad->numero_serie : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_motor">Número de motor*</label>
      <input type="text" class="form-control" id="numero_motor" name="numero_motor" placeholder="Número de motor" value="{{isset($unidad->numero_motor) ? $unidad->numero_motor : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_cilindros">Número de cilindros*</label>
      <input type="text" class="form-control" id="numero_cilindros" name="numero_cilindros" placeholder="Número cilindros" value="{{isset($unidad->numero_cilindros) ? $unidad->numero_cilindros : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="clave_vehicular">Clave vehicular*</label>
        <input type="text" class="form-control" id="clave_vehicular" name="clave_vehicular" placeholder="Clave vehicular" value="{{isset($unidad->clave_vehicular) ? $unidad->clave_vehicular : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="tipo">Tipo*</label>
        <input type="text" class="form-control" id="tipo" name="tipo" placeholder="Tipo" value="{{isset($unidad->tipo) ? $unidad->tipo : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="combustible">Combustible*</label>
      <input type="text" class="form-control" id="combustible" name="combustible" placeholder="Combustible" value="{{isset($unidad->combustible) ? $unidad->combustible : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="litros">Litros</label>
      <input type="text" class="form-control" id="litros" name="litros" placeholder="Litros" value="{{isset($unidad->litros) ? $unidad->litros : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="toneladas">Toneladas</label>
      <input type="text" class="form-control" id="toneladas" name="toneladas" placeholder="Toneladas" value="{{isset($unidad->toneladas) ? $unidad->toneladas : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="uso_vehiculo">Uso del vehículo</label>
        <input type="text" class="form-control" id="uso_vehiculo" name="uso_vehiculo" placeholder="Uso del vehículo" value="{{isset($unidad->uso_vehiculo) ? $unidad->uso_vehiculo : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="clase">Clase</label>
      <input type="text" class="form-control" id="clase" name="clase" placeholder="Clase" value="{{isset($unidad->clase) ? $unidad->clase : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="origen_vehiculo">Orígen del vehículo</label>
      <input type="text" class="form-control" id="origen_vehiculo" name="origen_vehiculo" placeholder="Orígen del vehículo" value="{{isset($unidad->origen_vehiculo) ? $unidad->origen_vehiculo : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="fabricante">Fabricante</label>
      <input type="text" class="form-control" id="fabricante" name="fabricante" placeholder="Fabricante" value="{{isset($unidad->fabricante) ? $unidad->fabricante : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="procedencia">Procedencia</label>
      <input type="text" class="form-control" id="procedencia" name="procedencia" placeholder="Procedencia" value="{{isset($unidad->procedencia) ? $unidad->procedencia : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="color_exterior">Color exterior</label>
      <input type="text" class="form-control" id="color_exterior" name="color_exterior" placeholder="Color exterior" value="{{isset($unidad->color_exterior) ? $unidad->color_exterior : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="color_interior">Color interior</label>
      <input type="text" class="form-control" id="color_interior" name="color_interior" placeholder="Color interior" value="{{isset($unidad->color_interior) ? $unidad->color_interior : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="capacidad_pasajeros">Capacidad de pasajeros</label>
      <input type="text" class="form-control" id="capacidad_pasajeros" name="capacidad_pasajeros" placeholder="Capacidad pasajeros" value="{{isset($unidad->capacidad_pasajeros) ? $unidad->capacidad_pasajeros : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_puertas">Número de puertas</label>
      <input type="text" class="form-control" id="numero_puertas" name="numero_puertas" placeholder="Número de puertas" value="{{isset($unidad->numero_puertas) ? $unidad->numero_puertas : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_inventario">Número de inventario</label>
      <input type="text" class="form-control" id="numero_inventario" name="numero_inventario" placeholder="Número de inventario" value="{{isset($unidad->numero_inventario) ? $unidad->numero_inventario : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="numero_pedido">Número de pedido</label>
      <input type="text" class="form-control" id="numero_pedido" name="numero_pedido" placeholder="Número de pedido" value="{{isset($unidad->numero_pedido) ? $unidad->numero_pedido : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="descripcion">Descripción</label>
      <textarea class="form-control" rows="3" id="descripcion" name="descripcion" {{isset($ver) ? 'disabled' : ''}}>{{isset($unidad->descripcion) ? $unidad->descripcion : ''}}</textarea>
    </div>
    </div>
  </div>

    @include('fotradis::unity.partials.formulario_tarjetacirculacion')
</form>