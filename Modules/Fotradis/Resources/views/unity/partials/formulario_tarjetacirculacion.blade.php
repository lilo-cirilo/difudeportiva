{{-- Formulario tarjeta circulación --}}
{{-- <form id="form_tarjeta_circulacion" action="{{ route('tarjetacirculacion.store') }}" method="POST"> --}}
  
  <hr>
    
  <h5>Datos tarjeta de circulación:</h5>

  <hr>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="oficina_expedidora">Oficina expedidora</label>
      <input type="text" class="form-control disabled" id="oficina_expedidora" name="oficina_expedidora" placeholder="Oficina expedidora" value="{{isset($tarjeta_circulacion->oficina_expedidora) ? $tarjeta_circulacion->oficina_expedidora : ''}}" {{isset($ver) ? 'disabled' : ''}}>
    </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="fecha_expedicion">Fecha de expedición*</label>
        <input type="text" class="form-control datepicker" id="fecha_expedicion" name="fecha_expedicion" placeholder="Fecha de expedición" value="{{isset($tarjeta_circulacion->fecha_expedicion) ? $tarjeta_circulacion->fecha_expedicion : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="vigencia">Vigencia*</label>
        <input type="text" class="form-control" id="vigencia" name="vigencia" placeholder="Vigencia" value="{{isset($tarjeta_circulacion->vigencia) ? $tarjeta_circulacion->vigencia : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="clave_repuve">Clave REPUVE</label>
        <input type="text" class="form-control" id="clave_repuve" name="clave_repuve" placeholder="REPUVE" value="{{isset($tarjeta_circulacion->clave_repuve) ? $tarjeta_circulacion->clave_repuve : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
    
  </div>

  <div  class="row" style="padding-left: 0; padding-right: 0;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="reg_ent">Reg Ent</label>
        <input type="text" class="form-control" id="reg_ent" name="reg_ent" placeholder="Reg Ent" value="{{isset($tarjeta_circulacion->reg_ent) ? $tarjeta_circulacion->reg_ent : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
      <div class="form-group">
        <label for="verificacion_vehicular">Verificación vehicular*</label>
        <input type="text" class="form-control" id="verificacion_vehicular" name="verificacion_vehicular" placeholder="Verificación vehicular" value="{{isset($tarjeta_circulacion->verificacion_vehicular) ? $tarjeta_circulacion->verificacion_vehicular : ''}}" {{isset($ver) ? 'disabled' : ''}}>
      </div>
    </div>
  </div>
{{-- </form> --}}