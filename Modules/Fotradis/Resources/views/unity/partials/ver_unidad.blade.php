<div class="modal fade" id="modal_ver_unidad" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="input_enable()">
        <span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Visualizando unidad</h4></center>
      </div>
      <div id="body_modal_ver_unidad" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" onclick="input_enable();" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal_ver_poliza" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <center><h4 class="modal-title">Visualizando poliza de garantia</h4></center>
      </div>
      <div class="modal-body">
        <div class="hovereffect"><img src="" class="img-thumbnail center-block" id="img_poliza_max" name="imagen"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Cerrar</button>
      </div>
    </div>
  </div>
</div>