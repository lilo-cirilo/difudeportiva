<div class="modal fade" id="modal_lista_mantenimiento" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn-info">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="input_enable()">
          <span aria-hidden="true">&times;</span></button>
          <center><h4 class="modal-title">Mantenimiento de la unidad {{isset($numero_unidad) ? $numero_unidad : 'Desconocida'}}</h4></center>
        </div>
        <div class="modal-body">
            {{-- <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                <input type="text" id="search" name="search" class="form-control">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                </span>
            </div> --}}
            {{-- {!! $mdt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'lista_mantenimiento', 'name' => 'lista_mantenimiento', 'style' => 'width: 100%']) !!} --}}
            <table id="lista_mantenimiento" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th class="text-center">FECHA</th>
                        <th class="text-center">COSTO</th>
                        <th class="text-center">TIPO</th>
                        <th class="text-center">DESCRIPCIÓN</th>
                    </tr>   
                </thead>
                <tbody>
                    @foreach($mantenimientos as $mantenimiento)
                        <tr>
                            <td class="text-center">{{ $mantenimiento->fecha }}</td>
                            <td class="text-center">{{ $mantenimiento->costo }}</td>
                            <td class="text-center">{{ $mantenimiento->tipo }}</td>
                            <td class="text-center">{{ $mantenimiento->descripcion }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" onclick="input_enable();" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
</div>