<script type="text/javascript">
	var form_unidad = $("#form_unidad")

	$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

	@if(isset($en_asignacion))
		swal({
			title: 'Esta unidad esta asignada a un conductor y una ruta',
			text: '¡No se pueden editar los campos número de unidad y tipo de servicio!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});
	@endif

	var ver = (function(){
		function unidad(matricula) {
			$.ajax({
				url:  "{{ URL::to('fotradis/unidad') }}" + "/" + matricula + "?accion=render",
				type: 'GET',
				success: function(response) {
					$("#body_modal_ver_unidad").html(response.html);
					$("#html_clave_vehicular").html(response.html_clave_vehicular);
					$("#modal_ver_unidad").modal("show");
				},
				error: function(response) {
					$("#body_modal_ver_conductor").html(response.html);	
					$("#modal_ver_conductor").modal("show");
				}
			});
		};

		return {
			unidad: unidad
		};
	})();

	var eliminar = (function(){
		function unidad(id) {
			$.ajax({
				url:  "{{ URL::to('fotradis/unidad') }}" + "/" + id,
				type: 'DELETE',
				success: function(response) {
          swal({
                  title: '¡Éxito!',
                  text: "La unidad se elimino correctamente.",
                  type: 'info',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'ACEPTAR'
                }).then((result) => {
                  if (result.value) {
								    //window.location.href = "{{ route('unidad.index') }}";
                    window.LaravelDataTables['unidades_actuales'].ajax.reload(null, false);
                  }
                })
        /*	$.ajax({
						url:  "URL::to('fotradis/tarjetacirculacion')" + "/" + id,
						type: 'DELETE',
						success: function(response) {
							swal({
								title: '¡Transacción exitosa!',
								text: 'Registro eliminado...',
								type: 'success'
							});
						},
						error: function(response) {
							swal({
								title: '¡Transacción incompleta!',
								text: 'No se puedo eliminar tarjeta de circulacion...',
								type: 'error'
							});
						}
					});*/
				},
				error: function(response) {
					swal({
						title: '¡Transacción rechazada!',
						text: 'Registro no eliminado...',
						type: 'error'
					});
				}
			});
		};

		return {
			unidad: unidad
		};
	})();

	var agregar_editar = (function(){
		function unidad() {
			validar_form_unidad()
			if(form_unidad.valid()) {
				swal({
					title: '¿Estas seguro?',
					text:  '¡Actualizando unidades!',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#28a745',
					cancelButtonColor: '#dc3545',
					confirmButtonText: 'SI',
					cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						var formDataUnidad = new FormData(form_unidad[0])
						@if(isset($unidad->poliza_garantia))
							formDataUnidad.append('poliza_garantia', '{{$unidad->poliza_garantia}}')
						@else
							formDataUnidad.append('poliza_garantia', 'no image')
						@endif

						$.ajax({
							url:  form_unidad.attr('action'),
							type: form_unidad.attr('method'),
							data: formDataUnidad,
							dataType: 'JSON',
							processData: false, 
							contentType: false,
							success: function(response) {
								swal({
									title: '¡Exito!',
									text: "La unidad se creo correctamente.",
									type: 'info',
									confirmButtonColor: '#3085d6',
									confirmButtonText: 'ACEPTAR'
								}).then((result) => {
									if (result.value) {
										window.location.href = "{{ route('unidad.index') }}";
									}
								})
							},
							error: function(response) {
								swal({
									title: '¡Transacción rechazada!',
									text: 'Registro no almacenado...',
									type: 'error'
								})
							}
						})
					}
				})
			}
		};
			
		function validar_form_unidad() {
			$.validator.addMethod('pattern_numeros', function (value, element) {
		        return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
			}, 'Solo números');
			return form_unidad.validate({
					rules: {
						numero_unidad: {
							required:true,
							maxlength:15
						},
						matricula: {
							required:true,
							maxlength:20
						},
						modelo: {
							required:true,
							maxlength:50
						},
						marca: {
							required:true,
							maxlength:20
						},
						anio: {
							required:true,
							pattern_numeros:'Sólo números AAAA',
							maxlength:4
						},
						tipo_servicio: {
							required:true,
							maxlength:15
						},
						numero_serie: {
							required:true,
							maxlength:20
						},
						numero_motor: {
							required:true,
							maxlength:15
						},
						numero_cilindros: {
							required:true,
							pattern_numeros:true,
							maxlength:2
						},
						clave_vehicular: {
							required:true,
							maxlength:15
						},
						tipo: {
							required:true,
							maxlength:255
						},
						combustible: {
							required:true,
							maxlength:10
						},
						fecha_expedicion: {
							required:true
						},
						vigencia: {
							required:true,
							pattern_numeros:'Sólo números',
							maxlength:2
						},
						verificacion_vehicular: {
							required:true,
							maxlength:17
						}
					}
				});
		};

		return {
			unidad: unidad
		};
	})();

	var poliza_fotografia = (function(){
		var poliza_garantia = $("#poliza_garantia");
		var imagen = $('#imagen');

		function init() {
			poliza_garantia = $("#poliza_garantia");
			imagen = $('#imagen');
		}

		function ver() {
			init();
			$("#img_poliza_max").attr('src', imagen.attr('src'));
			$("#modal_ver_poliza").modal("show");
		};

		function agregar() {
			init();
			poliza_garantia.click();
			editar();
		};

		function editar() {
			init();
			poliza_garantia.change(function() {
				var file = this.files[0];
				var reader = new FileReader();
				reader.onloadend = function() {
					imagen.attr('src', reader.result);
				};
				reader.onerror = function() {
				};
				if(file) {
					reader.readAsDataURL(file);
				}
				else {
				}
			});
		};

		function eliminar() {
			init();
			imagen.attr('src', "{{ asset('images/no-image.png') }}");
		};

		return {
			ver: ver,
			agregar: agregar,
			editar: editar,
			eliminar: eliminar
		};
	})();


	var generar = (function(){
		function bitacora_de_unidad(matricula) {

		};

		return {
			bitacora_de_unidad: bitacora_de_unidad
		};
	})();

	var lanzar = (function(){
		function mensaje_de_alerta(matricula, accion) {
			swal({
				title: '¿Estas seguro?',
				text: 'Se generarán cambios en la unidades',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("result.value true");
						switch(accion) {
						    case 'editar':
						        window.location.href = "{{ URL::to('fotradis/unidad') }}" + "/" + matricula + "?accion=null";
						        break;
						    case 'eliminar':
						    	eliminar.unidad(matricula);
						    	break;
						    case 'bitacora':
						    	generar.bitacora_de_unidad(matricula);
						    default:
						        console.log("No hay alguna acción que realizar");
						} 
					} else {
						console.log("result.value false");
					}
				});
		};

		return {
			mensaje_de_alerta: mensaje_de_alerta
		};
	})();

	function input_enable() {
		$("input").prop( "disabled", false );
		$("textarea").prop( "disabled", false );
	}
</script>