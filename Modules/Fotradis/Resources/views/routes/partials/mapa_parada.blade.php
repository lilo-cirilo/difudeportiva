<div class="modal fade" id="modal-parada">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-title-parada"></h4>
        </div>
        <div class="modal-body" id="modal-body-parada"></div>
        <div class="modal-footer" id="modal-footer-parada"></div>      
      </div>
  </div>
</div>