<?php $url = \Request::route()->getName(); ?>

<style type="text/css">
    .modal-body {
	    padding: 5px !important;
    }
    #map{
        background-color: lightblue;
        height: calc(100vh - 211px);
    }
</style>
@if($url != 'rutas.create')
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{isset($ruta->nombre) ? $ruta->nombre : ''}}</h4>
    </div>
@endif
    <div class="modal-body" >
        <div id="map"></div>
    </div>
@if($url != 'rutas.create')
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
@endif
<script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJY1fM_1PhRMuqn6eu9-2Cra1LRdnoqg&libraries=geometry&callback=initMap"></script>

<script>
var polyline;
var initMap = () => {
    var directionsService;
    var directionsDisplay;
    var inicio;
    var fin;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: 17.0669, lng: -96.7203},
        mapTypeControl: false,
          styles: [
        
            {
                featureType: 'poi.business',
                stylers: [{visibility: 'off'}]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{visibility: 'off'}]
            }
        ]
    });

    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: true,
        map: map
    });

    @if(isset($ruta->polyline))
        new google.maps.Polyline({
            path: google.maps.geometry.encoding.decodePath("{{$ruta->polyline}}"),
            geodesic: true,
            strokeColor: "{{$ruta->color}}",
            strokeOpacity: 0.8,
            strokeWeight: 5
        }).setMap(map);
    @endif
   
    @if(isset($paradas))
        @foreach($paradas as $parada)
            @if($parada->tipoparada === 'R')
                @if($parada->tipo == 1)
                    inicio = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };
                @elseif($parada->tipo == 2)
                    fin = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };
                @endif
                var m = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: {{$parada->tipo}} !=3 ? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
                    position: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },
                    title: "{{$parada->referencia}}",
                    icon: {{$parada->tipo}} !=3 ?
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 35),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 35)} :
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 22),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 11)}
                });
                m.setMap(map);
                //moveMarker(map, m);
            @endif
        @endforeach
    @endif

    @if($url === 'rutas.create')

        directionsService.route({
            origin: inicio,
            destination: fin,
            waypoints: [],
            travelMode: 'DRIVING',
            avoidTolls: true
        },(response, status) => {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                swal({
                    title: 'ERROR',
                    text: 'Error: ' + status,
                    type: 'error'
                });
            }
        });
        
        var controlUI = document.createElement('button');
        controlUI.textContent = "Guardar Ruta";
        controlUI.setAttribute('class', 'btn btn-block btn-info');
        controlUI.setAttribute('type', 'button');
        controlUI.addEventListener('click', function() {
            $.ajax({
                url:  "{{ URL::to('fotradis/save/polyline') }}",
                type: 'POST',
                data: {ruta_id:{{$ruta->id}},line:directionsDisplay.getDirections().routes[0].overview_polyline.replace(/\\/g,"\\\\")},
                dataType: 'JSON',
                success: function(response) {
                    swal({
                        title: 'Exito',
                        text: '¡La ruta se ha guardado!',
                        type: 'success'
                    });
                },
                error: function(response) {
                    swal({
                        title: 'ERROR',
                        text: '¡Datos de esta parada no encontrados!',
                        type: 'error'
                    });
                }
            });
        });
        var centerControlDiv = document.createElement('div');
        centerControlDiv.appendChild(controlUI);
        centerControlDiv.index = 1;
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(centerControlDiv);

        directionsDisplay.addListener('directions_changed', () => {
            console.log(directionsDisplay.getDirections());
        });
    @endif
}

function moveMarker( map, marker ) {
    
    //delayed so you can see it move
    setTimeout( function(){ 
    
        marker.setPosition( new google.maps.LatLng( 0, 0 ) );
        map.panTo( new google.maps.LatLng( 0, 0 ) );
        
    }, 1500 );

};
</script>