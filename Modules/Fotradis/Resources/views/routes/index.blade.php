@extends('fotradis::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('li-breadcrumbs')
    <li class="active">Rutas actuales</li>
@endsection

@section('content')
<section class="content">
    <div class="row">
    	<div class="col-xs-12">
    		<div class="box box-primary shadow">
          <div class="box-header with-border">
		        <h3 class="box-title">Rutas Actuales</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" onclick="nombre_ruta(0, 'add')" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar Ruta</button>
            </div>
          </div>
				  <div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
              <input type="text" id="search" name="search" class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
              </span>
            </div>
            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'rutas_del_dia', 'name' => 'rutas_del_dia', 'style' => 'width: 100%']) !!}
				  </div>
    		</div>
    	</div>
    </div>
  </section>


<!--div class="modal fade" id="modal_ver_ruta" data-keyboard="false" data-backdrop="static">
      <div id="body_modal_ver_ruta" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" onclick="input_enable();" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div-->

<div class="modal fade" id="modal_ver_ruta" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" id="body_modal_ver_ruta">
      
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@include('fotradis::routes.partials.modal_nombre_ruta')
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

{!! $dataTable->scripts() !!}

@include('fotradis::routes.js.rutas')
<script type="text/javascript">
	swal({
			title: 'Rutas asignadas a una unidad y un conductor',
			text: '¡No se pueden eliminar!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});

	$(document).ready(function() {
		block();
		var table = (function() {
			var tabla = undefined,
			btn_buscar = $('#btn_buscar'),
			search = $('#search');

			function init() {			
				search.keypress(function(e) {
					if(e.which === 13) {
						tabla.DataTable().search(search.val()).draw();
					}
				});

				btn_buscar.on('click', function() {
					tabla.DataTable().search(search.val()).draw();
				});
			};

			function set_table(valor) {
				tabla = $(valor);
			};

			function get_table() {
				return tabla;
			};

			return {
				init: init,
				set_table: set_table,
				get_table: get_table
			};
		})();

		table.set_table($('#rutas_del_dia').dataTable());
		table.init();
	    unblock();
	});
</script>
@endpush