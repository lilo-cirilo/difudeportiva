<script type="text/javascript">
	var resultados = "";
	var form_agregar_parada = $("#form_agregar_parada");
	var lat_parada = "";
	var lng_parada = "";
	var lng_parada_edit = "";
	var lat_parada_edit = "";

	$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

	@if(isset($en_asignacion))
		swal({
			title: 'Esta ruta esta asignada a un conductor y una unidad',
			text: '¡No se puede editar el nombre de ruta!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});
	@endif

	var ver = (function(){
		function ruta(ruta_id) {
			$.ajax({
				url:  "{{ URL::to('fotradis/rutas') }}" + "/" + ruta_id + "?accion=render",
				type: 'GET',
				success: function(response) {
					$("#body_modal_ver_ruta").html(response.html);	
					$("#modal_ver_ruta").modal("show");
				},
				error: function(response) {
					$("#body_modal_ver_ruta").html(response.html);	
					$("#modal_ver_ruta").modal("show");
				}
			});
		};

		function ubicacion_parada(lat, lng) {
			var map = new google.maps.Map(document.getElementById('parada_map'), {
			  center: {lat: lat, lng: lng},
			  zoom: 18
			});

			var marker = new google.maps.Marker({position: {lat: lat, lng: lng}, map: map, icon: "{{ asset('images/diftellevaicono.png') }}" });

			$("#modal_ver_parada").modal("show");
		};

		return {
			ruta: ruta,
			ubicacion_parada: ubicacion_parada
		};
	})();

	var eliminar = (function(){
		function parada_ruta(parada_id, ruta_id) {
			swal({
				title: '¿Estas seguro?',
				text: 'Se generarán cambios en los datos de Rutas',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						$.ajax({
							url:  "{{ URL::to('fotradis/ruta/parada') }}" + "/" + ruta_id + "/" + parada_id,
							type: 'DELETE',
							success: function(response) {
								swal({
										title: '¡Transacción exitosa!',
										text: 'Se ha almacenado...',
										type: 'success'
									});
							},
							error: function(response) {
								swal({
									title: '¡Registro rechazado!',
									text: 'No almacenado...',
									type: 'error'
								});
							}
						});
					} else {

					}
				});
		};

		function ruta(ruta_id) {
			$.ajax({
				url:  "{{ URL::to('fotradis/rutas') }}" + "/" + ruta_id,
				type: 'DELETE',
				success: function(response) {
					swal({
							title: '¡Transacción exitosa!',
							text: 'Registro eliminado...',
							type: 'success'
						});
					window.location.href = "{{ route('rutas.index') }}";
				},
				error: function(response) {
					swal({
						title: '¡Transacción rechazada!',
						text: 'Registro no eliminado...',
						type: 'error'
					});
				}
			});
		};

		return {
			parada_ruta: parada_ruta,
			ruta: ruta

		};
	})();

	var agregar_editar = (function(){
		var form_nombre_ruta = $("#form_nombre_ruta");
		function ruta(ruta_id, accion) {
			form_nombre_ruta.validate({
				rules : {
					nombre_ruta: {
						required: true
					}
				}
			});

			if(form_nombre_ruta.valid()) {
				swal({
				title: '¿Estas seguro?',
				text: 'Se generarán cambios en los datos de Rutas',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("form_nombre_ruta");
						var formData = new FormData(form_nombre_ruta[0]);
						formData.append('ruta_id', ruta_id);
						formData.append('accion', accion);
						formData.append('nombre', $("#nombre_ruta").val());

						$.ajax({
							url:  "{{ route('rutas.store') }}",
							type: 'POST',
							data: formData,
							dataType: 'JSON',
							processData: false, 
							contentType: false,
							success: function(response) {
								swal({
										title: '¡Transacción exitosa!',
										text: 'Se ha almacenado...',
										type: 'success'
									});
								window.location.href = "{{ URL::to('fotradis/rutas/create') }}" + "/?ruta=" + response.ruta;
							},
							error: function(response) {
								swal({
									title: '¡Registro rechazado!',
									text: 'No almacenado...',
									type: 'error'
								});
							}
						});
					} else {

					}
				});
			}
		};

		function parada(accion, parada_id) {
			form_agregar_parada.validate({
				rules : {
					ubicar_parada: {
						required: true
					},
					nombre_parada: {
						required: true
					},
					numeroestacion_parada: {
						required: true
					}
				}
			});

			if(form_agregar_parada.valid()) {
				var formData = new FormData(form_agregar_parada[0]);
				formData.append('nombre', $("#nombre_parada").val());
				formData.append('referencia', $("#ubicar_parada").val());
				formData.append('latitud', lat_parada);
				formData.append('longitud', lng_parada);
				formData.append('ruta_id', {{ isset($ruta->id) ? $ruta->id : '' }});
				formData.append('numero_estacion', $("#numeroestacion_parada").val());
				if(accion == 1) {
					formData.append('parada_id', parada_id);
				}
				else{
					formData.append('parada_id', '');
				}

				$.ajax({
					url:  "{{ URL::to('fotradis/parada/store') }}",
					type: 'POST',
					data: formData,
					dataType: 'JSON',
					processData: false, 
					contentType: false,
					success: function(response) {
						swal({
								title: '¡Transacción exitosa!',
								text: 'Se ha almacenado...',
								type: 'success'
							});
						$("#modal_ver_parada").modal("hide");
						// window.location.href = "{{ URL::to('fotradis/rutas') }}" + "/" + {{ isset($ruta->id) ? $ruta->id : '' }} + "?accion=null";
					},
					error: function(response) {
						swal({
							title: '¡Registro rechazado!',
							text: 'No almacenado...',
							type: 'error'
						});
					}
				});
			}
		};

		return {
			ruta: ruta,
			parada: parada
		};
	})();

	var lanzar = (function(){
		function mensaje_de_alerta(ruta, accion) {
			swal({
				title: '¿Estas seguro?',
				text: 'Se generarán cambios en los datos',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#28a745',
				cancelButtonColor: '#dc3545',
				confirmButtonText: 'SI',
				cancelButtonText: 'NO'
				}).then((result) => {
					if(result.value) {
						console.log("result.value true");
						switch(accion) {
						    case 'editar':
						        window.location.href = "{{ URL::to('fotradis/rutas/create') }}" + "/?ruta=" + ruta;
						        break;
						    case 'eliminar':
						    	eliminar.ruta(ruta);
						    	break;
					    	case 'eliminar_parada':
						    	eliminar.parada_ruta(ruta);
						    	break;
						    case 'agregar_parada':
						    	agregar_editar.parada(0, ruta);
						    	break;
					    	case 'editar_parada':
						    	agregar_editar.parada(1, ruta);
						    	break;
					    		
					    	break;
						    default:
						        console.log("No hay alguna acción que realizar");
						} 
					} else {
						console.log("result.value false");
					}
				});
		};

		return {
			mensaje_de_alerta: mensaje_de_alerta
		};
	})();

	function localizar_parada(tipo) {
		var map = new google.maps.Map(document.getElementById('parada_map'), {
			zoom: 16,
			scrollwheel: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});
		if(tipo = 'edit') {
			map.setCenter({lat:parseFloat(lat_parada_edit),lng:parseFloat(lng_parada_edit)});
			var marker = new google.maps.Marker({
				map: map,
				draggable: true,
				position: {lat:parseFloat(lat_parada_edit),lng:parseFloat(lng_parada_edit)}
			});

			google.maps.event.addListener(marker, 'dragend', function() 
			{
				lat_parada = marker.getPosition().lat()
				lng_parada = marker.getPosition().lng()
				console.log(marker.getPosition().lat())
				//geocodePosition(marker.getPosition());
			})
		} else {
			form_agregar_parada.validate({
				rules : {
					ubicar_parada: {
						required: true
					},
					nombre_parada: {
						required: true
					},
					numeroestacion_parada: {
						required: true
					}
				}
			});

			if(form_agregar_parada.valid()) {
				var geocoder = new google.maps.Geocoder();				
				geocoder.geocode({'address': $("#ubicar_parada").val()}, function(results, status) {
					if (status === 'OK') {
						resultados = results[0].geometry.location,
							lat_parada = resultados_lat = resultados.lat(),
							lng_parada = resultados_long = resultados.lng();
						
						map.setCenter(results[0].geometry.location);
						var marker = new google.maps.Marker({
							map: map,
							draggable: true,
							position: results[0].geometry.location
						});

						google.maps.event.addListener(marker, 'dragend', function() 
						{
							lat_parada = marker.getPosition().lat()
							lng_parada = marker.getPosition().lng()
							console.log(marker.getPosition().lat())
						})

						$("#nombre_parada").prop( "disabled", false );
						$("#numeroestacion_parada").prop( "disabled", false );
						$("#boton_enviar_parada").prop( "disabled", false );

					} else {
						var mensajeError = "";
						if (status === "ZERO_RESULTS") {
							mensajeError = "No hubo resultados para la dirección ingresada.";
						} else if (status === "OVER_QUERY_LIMIT" || status === "REQUEST_DENIED" || status === "UNKNOWN_ERROR") {
							mensajeError = "Error general del mapa.";
						} else if (status === "INVALID_REQUEST") {
							mensajeError = "Error de la web. Contacte con Name Agency.";
						}
						alert(mensajeError);
					}
				});	
			}
		}
		$("#modal_ver_parada").modal("show");	
	}
	
	function obtener_datos_parada(parada_id, ruta_id) {
		$.ajax({
			url:  "{{ URL::to('fotradis/show/parada') }}" + "/" + parada_id + "/" + ruta_id,
			type: 'GET',
			success: function(response) {
				$("#ubicar_parada").val(response.parada[0].referencia);
				$("#nombre_parada").val(response.parada[0].parada);
				$("#numeroestacion_parada").val(response.parada[0].numero_estacion);
				$("#nombre_parada").prop( "disabled", false );
				$("#numeroestacion_parada").prop( "disabled", false );
				$("#boton_enviar_parada").prop( "disabled", false );
				$("#ubicar_parada").prop( "disabled", false );
				$("#boton_enviar_parada").attr("onclick", "lanzar.mensaje_de_alerta(" + response.parada[0].parada_id + ",'editar_parada')");
				lat_parada_edit = lat_parada = response.parada[0].latitud;
				lng_parada_edit = lng_parada = response.parada[0].longitud;
				$("#modal_agregar_parada").modal("show");
			},
			error: function(response) {
				swal({
					title: 'ERROR',
					text: '¡Datos de esta parada no encontrados!',
					type: 'error'
				});
			}
		});
		$("#modal_agregar_parada").modal("show");
	}

	function nombre_ruta(ruta_id, accion) {
		$("#nombre_ruta").val("");
		$("#button_modal_nombre_ruta").attr("onclick", "agregar_editar.ruta("+ ruta_id + ",'" + accion + "')");
		$("#modal_nombre_ruta").modal("show");
	}

	/* function geocodePosition(pos) 
{
   geocoder = new google.maps.Geocoder();
   geocoder.geocode
    ({
        latLng: pos
    }, 
        function(results, status) 
        {
            if (status == google.maps.GeocoderStatus.OK) 
            {
                $("#mapSearchInput").val(results[0].formatted_address);
                $("#mapErrorMsg").hide(100);
            } 
            else 
            {
                $("#mapErrorMsg").html('Cannot determine address at this location.'+status).show(100);
            }
        }
    );
} */
</script>