<div class="modal fade" id="modal_control_rutas" data-backdrop="static" data-keyboard="false">
  <form id="form_control_rutas">
      <div class="modal-dialog">
        <div class="modal-content">
          <div id="header_modal_asignacion" class="modal-header btn-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="reiniciarTablaMonitoreo()">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 id="title_asignacion" class="modal-title">Asignación de rutas</h4></center>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="conductor_id">Conductor:</label>
              <select id="conductor_id" name="conductor_id" class="form-control select2" title="Elegir conductor" style="width: 100%;"></select>
            </div>

            <div class="form-group">
              <label for="unidad_id">Unidad:</label>
              <select id="unidad_id" name="unidad_id" class="form-control select2" title="Elegir unidad" style="width: 100%;"></select>
            </div>

            <div class="form-group">
              <label for="recorrido_id">Ruta:</label>
              <select id="recorrido_id" name="recorrido_id" class="form-control select2" title="Elegir ruta" style="width: 100%;"></select>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="reiniciarTablaMonitoreo()">Cerrar</button>
            <button type="button" class="btn btn-info pull-rigth" id="button_modal_control_rutas" >Listo</button>
          </div>
        </div>
      </div>
  </form>
</div>