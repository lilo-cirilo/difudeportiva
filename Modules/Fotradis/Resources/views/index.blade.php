@extends('fotradis::layouts.master')

@push('head')
  <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" rel="stylesheet">
  <style type="text/css">
  #monitoreo_map {
    background-color: lightblue;
    height: calc(100vh - 211px);
  }
  .products-list .product-img img {
    width: 17px;
    height: 30px;
  }
  .cards {
    margin-right: -15px !important;
    margin-left: -15px !important;
  }
  #audio {
    display: none
  }
  .nameMax {
    font-size: 22px !important;
    padding-bottom: 14px !important;
  }
  #ruta_mas_transitada{
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  #loaderTable{
    display: flex;
    justify-content: center;
  }
  </style>
@endpush

@section('content')

  <div class="row cards">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua shadow">
        <div class="inner">
          <h3 id="total_pasajeros"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>

          <p>Beneficiarios activos</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        {{-- <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a> --}}
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green shadow">
        <div class="inner">
          <h3 id="pasajeros_hoy"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i>{{-- <sup style="font-size: 20px">% --}}</sup></h3>
          <p>Registros del día</p>
        </div>
        <div class="icon">
          <i class="fa fa-user"></i>              
        </div>
        {{-- <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a> --}}
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow shadow">
        <div class="inner">
          <h3 id="pasajeros_mes"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>

          <p>Registros del mes</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        {{-- <a href="#" class="small-box-footer">Más información<i class="fa fa-arrow-circle-right"></i></a> --}}
      </div>
    </div>
    {{-- <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3 id="conductores_asignados"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>

          <p>Conductores asigandos a una unidad</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
      </div>
    </div> --}}
    <!-- ./col -->

    <div class="col-lg-3 col-xs-6"> {{--style="width:100%; word-wrap: break-word;">--}}
      <!-- small box -->
      <div class="small-box bg-red shadow">
        <div class="inner">
          <h3 id="ruta_mas_transitada"><i class="fa fa-spinner fa-pulse fa-fw"></i></h3>

          <p>Ruta más transitada este mes</p>
        </div>
        <div class="icon">
          <i class="fa fa-map-marker"></i>
        </div>
        {{-- <a href="#" class="small-box-footer">Más información <i class="fa fa-arrow-circle-right"></i></a> --}}
      </div>
    </div>
    <!--audio id="demo" src="/alerta.mp3"></audio-->

    <!-- ./col -->
  </div>
    <!-- TABLE: LATEST ORDERS -->
    <section class="content" style="padding-left: 0px;padding-right: 0px; min-height: 0px">
      <div class="row cards">
        <div class="col-xs-12">
          <div class="box box-primary shadow">
            <div class="box-header with-border">
              <h3 class="box-title">Rutas Asignadas</h3>
              <div class="box-tools pull-right">
              @if(auth()->user()->hasRolesModulo(['ADMINISTRADOR'],6))
                <button type="button" class="btn btn-box-tool" onclick="controlrutas.boton('0','add')" style="color: #d12654;"><i class="fa fa-fw fa-plus-square"></i> Agregar asignación</button>
              @endif  
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
            <div class="row">
              <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                <input type="text" id="search" name="search" class="form-control">
                <span class="input-group-btn">
                  <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                </span>
              </div>
            </div>
            <div id="loaderTable">
              <i  class="fa fa-refresh fa-spin fa-2x fa-fw" style="color: #c54e77;"></i>
            </div>
              <div id="content-table" class="hide">
                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'rutas_del_dia', 'name' => 'rutas_del_dia', 'style' => 'width: 100%']) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

  <div class="box box-primary shadow">
  <div class="box-header with-border">
    <h3 class="box-title">Monitoreo de unidades</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
      </button>
      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
  </div>

  <div class="box-body">
    <div id="monitoreo_map"></div>
  </div>
  </div>  
    @include('fotradis::partials.modal_control_rutas')
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGJY1fM_1PhRMuqn6eu9-2Cra1LRdnoqg&libraries=geometry"></script>
<script type="text/javascript" src="{{ asset('js/fotradis/poligono_puerto.js') }}"></script>
<script src="{{asset('js/firebase-app.js')}}"></script>
<script src="{{asset('js/firebase-auth.js')}}"></script>
<script src="{{asset('js/firebase-database.js')}}"></script>

<script type="text/javascript">
  var map;
  var waypts = [];
  var markers = [];
  var paradas = [];
  var mini =  true;
  var pasajeros;
  var servicioCambio = true;
  
  controlrutas.select2_rutas();
  controlrutas.select2_unidades();
  controlrutas.select2_conductores();
  var audio = document.getElementById("audio");
  
  swal({
			title: 'Asignaciones con sesión iniciada',
			text: '¡No se pueden eliminar ni cambiar de servicio!',
			type: 'warning',
			timer: 8000,
			toast : true,
			position : 'top-end',
			showConfirmButton: false
		});
    
  var table = (function() {
    var tabla = undefined,
    btn_buscar = $('#btn_buscar'),
    search = $('#search');

    function init() {     
      search.keypress(function(e) {
        if(e.which === 13) {
          tabla.DataTable().search(search.val()).draw();
        }
      });

      btn_buscar.on('click', function() {
        tabla.DataTable().search(search.val()).draw();
      });

      tabla.on('draw.dt', function () {
  			$(".myToggle").each(function(){
        		$(this).bootstrapToggle();
        		$(this).change(function() {
              cambiarServicio($(this).attr('id'),{estado : $(this).prop('checked') });
    			})
    		});
  		 });
    };

    function set_table(valor) {
      tabla = $(valor);
    };

    function get_table() {
      return tabla;
    };

    return {
      init: init,
      set_table: set_table,
      get_table: get_table
    };
  })();

  table.set_table($('#rutas_del_dia').dataTable());

  var config = {
    apiKey: "AIzaSyB7jM0WfuaZdwqfRlE5t-CJ3zfDz11xSUw",
    authDomain: "reginet-7c5b2.firebaseapp.com",
    databaseURL: "https://reginet-7c5b2.firebaseio.com",
    projectId: "reginet-7c5b2",
    storageBucket: "reginet-7c5b2.appspot.com",
    messagingSenderId: "770654201194"
  };

  firebase.initializeApp(config);

  var mAuth = firebase.auth().signInWithEmailAndPassword('diftelleva@gmail.com', 'sicarucheelu').catch(function(error) {
    // Encargarse de errores aquí
    var errorCode = error.code;
    var errorMessage = error.message;
  });

  function initMap() {    
    map = new google.maps.Map(document.getElementById('monitoreo_map'), {
      center: {lat: 17.027557, lng: -96.710725},
      zoom: 12,
      mapTypeControl: false,
      styles:  [
            {
                featureType: 'poi.business',
                stylers: [{visibility: 'off'}]
            },
            {
                featureType: 'transit',
                elementType: 'labels.icon',
                stylers: [{visibility: 'off'}]
            }
        ]
    });
    
    //-------------------------------------polígonos
    //centrando la atencion en oaxaca
    let coordmapini=[
      {lat:15.258438722900348,lng:-99.75428681640625},
      {lat:15.258438722900348,lng:-92.72303681640625},
      {lat:19.03689065576206,lng:-92.72303681640625},
      {lat:19.03689065576206,lng:-99.75428681640625}
    ];

    //generando un poligono cuandrado con el contorno de oaxaca en el centro
    oaxaca = new google.maps.Polygon({
      paths: [coordmapini,puertoCoord],
        strokeColor: '#FF0000',
        strokeOpacity: 0.35,
        strokeWeight: 3,
        fillColor: '#000000',
        fillOpacity: 0.0
    });

    //y asignado al mapa
    oaxaca.setMap(map);
    //-------------------------------------end polígonos

    var points = [];
    var marcadores = [];
    var obj = {};
    var audio = {};

    {{-- @if(isset($paradas)) --}}
      @foreach($paradas as $parada)
        @if($parada->tipoparada === 'R')
          marcadores.push({LatLng:{ lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },tipo:{{$parada->tipo}},ref:"{{$parada->referencia}}",icono:"{{$parada->icono}}"});                
        @endif
        @if($parada->tipo == 3)
            points.push({location: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} }, stopover: false});
        @endif
        @if($parada->tipo == 1)
            obj.inicio = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };
        @endif
        @if($parada->tipo == 2)
            obj.fin = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };                
        @endif
      @endforeach
    {{-- @endif --}}

    @if(isset(($rutas)))
      @foreach($rutas as $ruta)
        @if(isset($ruta->polyline))
            var lineaRuta = new google.maps.Polyline({
                path: google.maps.geometry.encoding.decodePath("{{$ruta->polyline}}"),
                geodesic: true,
                strokeColor: "{{$ruta->color}}",
                strokeOpacity: 0.8,
                strokeWeight: 5
            });
            lineaRuta.setMap(map);
        @endif
      @endforeach
    @endif

    /*for(var i = 0; i < marcadores.length ; i++){
        new google.maps.Marker({
          map: map,
          draggable: false,
          animation: marcadores[i].tipo !=3? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
          position: marcadores[i].LatLng,
          title: marcadores[i].ref,
          icon:{url:  marcadores[i].tipo !=3? "{{asset('images/ruta1.png')}}" : "{{asset('images/parada.png')}}"}
        });
    }*/

    @if(isset($paradas))
        @foreach($paradas as $parada)
            @if($parada->tipoparada === 'R' && $parada->tipo != 3)
                new google.maps.Marker({
                    map: map,
                    draggable: false,
                    animation: google.maps.Animation.DROP,
                    position: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },
                    title: "{{$parada->referencia}}",
                    icon: {{$parada->tipo}} !=3 ?
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 35),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 35)} :
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 22),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 11)}
                });
            @endif
            @if($parada->tipo == 3)
               var p = new google.maps.Marker({
                    draggable: false,
                    animation: {{$parada->tipo}} !=3 ? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
                    position: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },
                    title: "{{$parada->referencia}}",
                    icon: {{$parada->tipo}} !=3 ?
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 35),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 35)} :
                    {url:"{{asset($parada->icono)}}",size: new google.maps.Size(22, 22),origin: new google.maps.Point(0, 0),anchor: new google.maps.Point(11, 11)}
                });
                paradas.push({marcador: p, ruta:{{$parada->ruta}} });
            @endif
        @endforeach
    @endif

    map.addListener('zoom_changed', function() {
        if(map.getZoom() == 14){
            for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(map);
            }
        }else if (map.getZoom() < 14){
            for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(null);
            }
        }
    });

    var rutaspanel = '<div class="row" style="min-width: 130px;">' +
    '<div class="col-xs-12" style="padding-right: 0px;padding-left: 0px;">' +
    '<div class="box box-info shadow">' +
    '<div class="box-header with-border">' +
      '<h3 class="box-title" onclick="min()">RUTAS</h3>' +
      '<div class="box-tools pull-right">' +
                '<button type="button" class="btn btn-box-tool" onclick="min()" ><i id="min-plus" class="fa fa-plus"></i>' +
                '</button>' +
    '</div>' +
    '<div id="allRutas" class="box-body collapse" style="padding: 0px 10px;">' +
      '<ul class="products-list product-list-in-box">' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R001.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(1)" class="product-title">DIF - CREE</a>' +
               '<span class="product-description">' +
               '</span>' +
          '</div>' +
        '</li>' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R002.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(3)" class="product-title">VIGUERA - CD. ADMINISTRATIVA</a>' +
            '<span class="product-description">' +
                '</span>' +
          '</div>' +
        '</li>' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R003.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(6)" class="product-title">LLANO - MONTE ALBAN</a>' +
            '<span class="product-description">' +
                '</span>' +
          '</div>' +
        '</li>' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R004.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(7)" class="product-title">PARQUE EL LLANO</a>' +
            '<span class="product-description">' +
                '</span>' +
          '</div>' +
        '</li>' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R005.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(8)" class="product-title">SN. LUIS BELTRAN - CD. JUDICIAL</a>' +
            '<span class="product-description">' +
                '</span>' +
          '</div>' +
        '</li>' +
        '<li class="item">' +
          '<div class="product-img">' +
            '<img src="/images/R006.png" alt="Product Image">' +
          '</div>' +
          '<div class="product-info">' +
            '<a href="javascript:void(0)" onclick="selectRuta(10)" class="product-title">ATZOMPA - CONGRESO</a>' +
            '<span class="product-description">' +
            '</span>' +
          '</div>' +
        '</li>' +
      '</ul>' +
    '</div>' +
'  </div>' +
    '</div>' +
'  </div>' ;
/*
    var cree = document.createElement('button');
    cree.textContent = "CREE";
    cree.setAttribute('class', 'btn btn-block btn-info');
    cree.setAttribute('type', 'button');
    cree.addEventListener('click', function() {
        map.setCenter({lat:16.9473699,lng:-96.7157458});
        map.setZoom(17);
        for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(null);
            }
    });

    var canteras = document.createElement('button');
    canteras.textContent = "CD. CANTERAS";
    canteras.setAttribute('class', 'btn btn-block btn-info');
    canteras.setAttribute('type', 'button');
    canteras.addEventListener('click', function() {
        map.setCenter({lat:17.067440,lng:-96.704925});
        map.setZoom(17);
        for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(map);
            }
    });

    var creesb = document.createElement('button');
    creesb.textContent = "CREE - Sn.Bartolo";
    creesb.setAttribute('class', 'btn btn-block btn-info');
    creesb.setAttribute('type', 'button');
    creesb.addEventListener('click', function() {
        map.setCenter({lat:16.952405,lng:-96.709495});
        map.setZoom(16);
        for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(map);
            }
    });
*/
    var todo = document.createElement('button');
    todo.textContent = "Todo";
    todo.setAttribute('class', 'btn btn-block btn-info');
    todo.setAttribute('type', 'button');
    todo.addEventListener('click', function() {
        map.setCenter({lat: 17.027557, lng: -96.710725});
        map.setZoom(12);
        for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(null);
            }
    });

    

    var ControlDivUBI = document.createElement('div');
    ControlDivUBI.setAttribute('style','margin-left: 10px;');
    //ControlDivUBI.appendChild(canteras);
    //ControlDivUBI.appendChild(cree);
    //ControlDivUBI.appendChild(creesb);
    ControlDivUBI.appendChild(todo);
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(ControlDivUBI);

    var PanelRutas= document.createElement('div');
    PanelRutas.setAttribute('style','margin-top: 10px;margin-left: 10px;');
    PanelRutas.innerHTML=rutaspanel;
    PanelRutas.index = 1;
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(PanelRutas);
    
    var monitoreo_unidades = firebase.database().ref('ubicacion_unidades').on('value', function(snapshot) {
      deleteMarkers();
      if (snapshot != null) {
        snapshot.forEach(function(childSnapshot) {
            waypts.push({
                key: childSnapshot.key,
                position: new google.maps.LatLng(childSnapshot.val().latitud, childSnapshot.val().longitud),
                text: "Unidad: " + childSnapshot.key + "\nConductor: " + childSnapshot.val().conductor + "\nEsta en: " +childSnapshot.val().direccion
              });
          });

          waypts.forEach(function(feature) {
              var marker = new google.maps.Marker({
                id: feature.key,
                position: feature.position,
                title: feature.text,
                // label: "U" + feature.key,
                // icon: "{{ asset('images/diftelleva/rutaurvan-15.png') }}",
                // icon: {
                //   // path: google.maps.SymbolPath.CIRCLE,
                //   path: "{{ asset('images/diftelleva/rutaurvan-15.png') }}",
                //   scale: 7, //tamaño
                //   strokeColor: colorBorde, //color del borde
                //   strokeWeight: 2, //grosor del borde
                //   fillColor: '#fff', //color de relleno
                //   fillOpacity:1,// opacidad del relleno
                // },
                // animation: google.maps.Animation.BOUNCE,
                map: map
              });
              // marker.icon.strokeColor = "#f00";
              var alerta_unidad = firebase.database().ref('alerta_unidades/' + feature.key).on('value',function(snapshot){            
                if (snapshot != null) {
                  if(snapshot.val().alerta == "1") {
                    if (snapshot.val().servicio == "taxi") {
                      marker.setIcon("{{ asset('images/diftelleva/rutataxi-17.png') }}");
                    } else {
                      marker.setIcon("/images/diftelleva/rutaurvan-"+feature.key+"-alerta.png");
                    }
                    //var audio = document.getElementById('demo');
                    //audio.loop = true;
                    //audio.play();
                    if(audio[feature.key] === undefined){
                      audio[feature.key] = document.createElement('audio');
                      var tempSource = document.createElement('source');
                      tempSource.src = "/alerta.mp3";
                      tempSource.type = "audio/mp3";
                      audio[feature.key].appendChild(tempSource);
                      audio[feature.key].loop = true;
                    }
                    if(audio[feature.key] != undefined && audio[feature.key].paused ){
                      audio[feature.key].play();
                    }
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                    
                  } else if(snapshot.val().alerta == "0") {
                    if (snapshot.val().servicio == "taxi") {
                      marker.setIcon("{{ asset('images/diftelleva/rutataxi-22.png') }}");
                    } else {
                      marker.setIcon("/images/diftelleva/rutaurvan-"+feature.key+".png");
                    }
                    if(audio[feature.key] != undefined){
                      audio[feature.key].pause();
                      audio[feature.key].currentTime = 0;
                    }
                    //var audio = document.getElementById('demo');
                    //audio.pause();
                    //audio.currentTime = 0;
                    marker.setAnimation(null);
                    marker.setZIndex(1);
                   
                  }
                }
              });
              markers.push(marker);
            });
      }
    });

    var alerta_unidades = firebase.database().ref('alerta_unidades').on('value', function(snapshot) {
      snapshot.forEach(function(childSnapshot) {
        if (childSnapshot != null) {
          markers.forEach(function(feature) {
            if(feature.id == childSnapshot.key && childSnapshot.val().alerta == "1") {
              if(childSnapshot.val().servicio == "taxi") {
                feature.setIcon("{{ asset('images/diftelleva/rutataxi-17.png') }}");
              } else {
                feature.setIcon("/images/diftelleva/rutaurvan-"+childSnapshot.key+"-alerta.png");
              }
            
              feature.setAnimation(google.maps.Animation.BOUNCE);
            } else if(feature.id == childSnapshot.key && childSnapshot.val().alerta == "0") {
              if(childSnapshot.val().servicio == "taxi") {
                feature.setIcon("{{ asset('images/diftelleva/rutataxi-22.png') }}");
              } else {
                feature.setIcon("/images/diftelleva/rutaurvan-"+childSnapshot.key+".png");
              }
            
              feature.setAnimation(null);
            }
          });
        }
      });
    });
  }


function marcadores() {
  //get api uses
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  // waypoints to add
  // var points = [];
  // var marcadores = [];
  // var obj = {};
  @if(isset($paradas))
      @foreach($paradas as $parada)
          @if($parada->tipoparada === 'R')
                marcadores.push({LatLng:{ lat: {{$parada->latitud}}, lng: {{$parada->longitud}} },tipo:{{$parada->tipo}},ref:"{{$parada->referencia}}",icono:"{{$parada->icono}}"});                
            @endif
            @if($parada->tipo == 3)
                points.push({location: { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} }, stopover: false});
            @endif
            @if($parada->tipo == 1)
                obj.inicio = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };
            @endif
            @if($parada->tipo == 2)
                obj.fin = { lat: {{$parada->latitud}}, lng: {{$parada->longitud}} };                
            @endif
        @endforeach
    @endif

    //api map
    var map = new google.maps.Map(document.getElementById('monitoreo_map'), {
        zoom: 12,
        center: points[(points.length)/2].location
    });
    @foreach($rutas as $ruta)
      @if(isset($ruta->polyline))
          var lineaRuta = new google.maps.Polyline({
              path: google.maps.geometry.encoding.decodePath("{{$ruta->polyline}}"),
              geodesic: true,
              strokeColor: "{{$ruta->color}}",
              strokeOpacity: 1.0,
              strokeWeight: 3
          });
          lineaRuta.setMap(map);
      @endif
    @endforeach
    //add map
    //directionsDisplay.setMap(map);


    //console.log(marcadores);

    for(var i = 0; i < marcadores.length ; i++){
        new google.maps.Marker({
          map: map,
          draggable: false,
          animation: marcadores[i].tipo !=3? google.maps.Animation.BOUNCE : google.maps.Animation.DROP,
          position: marcadores[i].LatLng,
          title: marcadores[i].ref,
          icon:{url:  marcadores[i].tipo !=3? "{{asset('images/ruta1.png')}}" : "{{asset('images/parada.png')}}"}
        });
    }

    // FIN PARADAS RUTAS
  }

  function toggleBounce() {
    markers.forEach(function(feature) {
      feature.addListener('click', function(){
        if (feature.getAnimation() !== null) {
          feature.setAnimation(null);
        } else {
          feature.setAnimation(google.maps.Animation.BOUNCE);
        }
      });
    });
  }

  function actualizarConductoresF(unidad, ruta, conductor, nombreConductor, tipoServicio) {//uid, username, picture, title, body) {
    //Escribiendo simultaneamente en la lista de Conductores en firebase.
    var size_conductor = '' + conductor;
    var claveconductor = "C";
    
    var i = -1;
    while(size_conductor.length == i) {
      claveconductor = claveconductor + '0';
      i++;
    }
    claveconductor = claveconductor + conductor;

    var updates = {};
    updates['/Conductores/' + claveconductor] = {conductor: nombreConductor,clave: claveconductor, sesion: 0, unidad: unidad, tipo_servicio: tipoServicio, ruta: ruta, statusservicio: 0, index: conductor};

    return firebase.database().ref().update(updates);
  }

  function eliminarConductorF(conductor) {
    return firebase.database().ref('Conductores').child("C"+conductor).remove();
  }

Date.prototype.toString = function() { return this.getDate()+"/"+(this.getMonth()+1)+"/"+this.getFullYear(); } 

var conductoresSesionNoIniciada;
function fb() {
  var iniciar_conductores = firebase.database().ref('Conductores').on('value', function(snapshot) {
      conductoresSesionNoIniciada = Object.values(snapshot.val()).filter(s => s.sesion == 0)
      console.log(conductoresSesionNoIniciada)

      snapshot.forEach(function(childSnapshot) {
        var childKey = childSnapshot.key
        var childData = childSnapshot.val()
        $("#clave_conductor" + childData.index).text(childData.clave)//childKey).text(childData.clave);

        var sesionapp = $("#sesionapp" + childData.index)
        var estatusservicio = $("#estatusservicio" + childData.index)
        var eruta = $("#eruta" + childData.index)

        console.log(eruta.attr('id'))

        if(childData.sesion == 1) {
          sesionapp.text("INICIADA")
          sesionapp.attr("class", "label label-success")
          $("#"+childData.unidad).bootstrapToggle("disable")
          eruta.addClass('disabled')
        } else {
          sesionapp.text("NO INICIADA")
          sesionapp.attr("class", "label label-danger")
          $("#"+childData.unidad).bootstrapToggle("enable")
          eruta.removeClass('disabled')
        }

        if(childData.tipo_servicio == 'taxi') {
          switch(childData.statusservicio) {
            case 0:
              estatusservicio.text("EN ESPERA")
              estatusservicio.attr("class", "label label-warning")
            break;
            case 1:
              estatusservicio.text("EN SERVICIO")
              estatusservicio.attr("class", "label label-success")
            break;
            default:
              estatusservicio.text("")
              break;
          }
        } else {
          switch(childData.sesion) {
            case 0:
              estatusservicio.text("EN ESPERA")
              estatusservicio.attr("class", "label label-warning")
            break;
            case 1:
              estatusservicio.text("EN RUTA")
              estatusservicio.attr("class", "label label-success")
            break;
            default:
              estatusservicio.text("");
              break;
          }
        }
      });
      $('#loaderTable').addClass('hide');
      $('#content-table').removeClass('hide');
      unblock(); 
  });

  var total_pasajeros = firebase.database().ref('pasajeros').orderByChild('timestampFinServ').on('value', function(snapshot) {
      if (snapshot.val() != null) {
        $("#total_pasajeros").text(Object.keys(snapshot.val()).length);
      }
  });

  let _filterA = (obj_entrada) => {
    let obj_salida = {};
    for(var llave in obj_entrada) {
      if(llave.substring(0,1) === "A" ){
        obj_salida[llave] = obj_entrada[llave]
      }
    }
    return obj_salida;
  }

  var all_pasajeros = firebase.database().ref('pasajeros').on('value', (snapshot) => {
    if (snapshot.val() != null) {
      pasajeros = snapshot.val();
      pasajeros["A1000"] = {};
      pasajeros["B1000"] = {};
      pasajeros["A0018"] = {};
      pasajeros["A0043"] = {};
      pasajeros["A1000"]["tipoPasajero"] = "BENEFICIARIO";
      pasajeros["A0018"]["tipoPasajero"] = "BENEFICIARIO";
      pasajeros["A0043"]["tipoPasajero"] = "BENEFICIARIO";
      pasajeros["B1000"]["tipoPasajero"] = "BENEFICIARIO";
      console.log(pasajeros);
      //console.log(Object.values(snapshot.val()).filter(b => b.tipoServicio == "TAXI"));
      var total_taxi = Object.values(snapshot.val()).filter(b => b.tipoServicio == "TAXI").filter(b => new Date(Number(b.fechaFinServ.split("/")[2]),Number(b.fechaFinServ.split("/")[1]),Number(b.fechaFinServ.split("/")[0]),0,0,0) >= new Date()).length;
      var total_urban = Object.values(_filterA(pasajeros)).filter(b => b.tipoServicio == "URBAN").length;
      var total_ambos = Object.values(snapshot.val()).filter(b => b.tipoServicio == "AMBOS").filter(b => new Date(Number(b.fechaFinServ.split("/")[2]),Number(b.fechaFinServ.split("/")[1]),Number(b.fechaFinServ.split("/")[0]),0,0,0) >= new Date()).length/2;
      console.log("TAXI: " + total_taxi);
      console.log("URBAN: " + total_urban);
      console.log("AMBOS: " + total_ambos);
      console.log("TOTAL: " + (total_ambos + total_taxi + total_urban));
    }
  });

  var pasajeros_hoy = firebase.database().ref('registros_pasajeros').orderByChild('fecha').startAt(""+timestampMilliSeconds("")).endAt(""+timestampMilliSeconds("manana")).on('value', function(snapshot) {
      if (snapshot.val() != null){
        //console.log(snapshot.val()[Object.keys(snapshot.val())[Object.keys(snapshot.val()).length-1]]);
        //save_registro(snapshot.val()[Object.keys(snapshot.val())[Object.keys(snapshot.val()).length-1]]); 
        $("#pasajeros_hoy").text(Object.keys(snapshot.val()).length);
      }
  });

  var pasajeros_mes = firebase.database().ref('registros_pasajeros').orderByChild('fecha').startAt(""+timestampMilliSeconds("primero")).on('value', function(snapshot) {
      if (snapshot.val() != null) {
        console.log(snapshot.val());
        //console.log(Object.values(snapshot.val()).map(e=>{e.date = new Date(Number(e.fecha)).toString(); return e}));
        console.log(Object.values(snapshot.val()).map(e=>{e.date = new Date(Number(e.fecha)).toString(); return e}));
        Registro_Servicios(Object.values(snapshot.val()).map(e=>{e.date = new Date(Number(e.fecha)).toString(); return e}));
        $("#pasajeros_mes").text(Object.keys(snapshot.val()).length);
      }
        //console.log(Object.values(snapshot.val()).map(e=>{e.date = new Date(Number(e.fecha)).toString(); return e}));
        
  });  

  // var conductores_asignados = firebase.database().ref('Conductores')..on('value', function(snapshot) {
  //     if (snapshot.val() != null) 
  //       $("#conductores_asignados").text(Object.keys(snapshot.val()).length);
  // });

  var ruta_mas_transitada = firebase.database().ref('parada_urban_conductor').orderByKey().startAt(""+timestampMilliSeconds("primero")).on('value', function(snapshot) {
      if (snapshot.val() != null) {
        var r1 = Object.values(snapshot.val()).filter(b => b.ruta == "DIF - CREE").length
        var r2 = Object.values(snapshot.val()).filter(b => b.ruta == "VIGUERA - CD ADMINISTRATIVA").length
        var r3 = Object.values(snapshot.val()).filter(b => b.ruta == "SAN BELTRÁN-CD. JUDICIAL").length
        var r4 = Object.values(snapshot.val()).filter(b => b.ruta == "ATZOMPA - CONGRESO").length
        console.log("dif - cree " + r1 + " viguera " + r2 + " san luis beltrán " + r3 + " atzompa " + r4 + " total = " + Object.keys(snapshot.val()).length)
        var ruta
        var v = Math.max(r1,r2,r3,r4)
        if(r1==v)
          ruta = "DIF-CREE"
        else if(r2==v)
          ruta = "VIGUERA-CD ADM"
        else if(r3==v)
          ruta = "SN BELTRÁN-CD JUD"
        else if(r4==v)
          ruta = "ATZOMPA-CONGRESO"

        $("#ruta_mas_transitada").text(ruta)
      }
  });
  // var mostViewedPosts = firebase.database().ref('parada_urban_pasajero').orderByChild('ruta').on('value', function(snapshot) {
  //   snapshot.forEach(function(childSnapshot) {
  //     console.log(childSnapshot.val());
  //   });
  // });
}

  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
    setMapOnAll(null);
  }

  // Shows any markers currently in the array.
  function showMarkers() {
    setMapOnAll(map);
  }

  // Deletes all markers in the array by removing references to them.
  function deleteMarkers() {
    clearMarkers();
    markers = [];
    waypts = [];
  }

  function timestampSeconds(dia) {
    var hoy = new Date();
    hoy.setHours(0);
    hoy.setMinutes(0);
    hoy.setSeconds(0);
    hoy.setMilliseconds(0);
    if(dia == "manana")
      hoy.setDate(hoy.getDate() + 1);
    return Math.floor(hoy.getTime() / 1000);
  }

  function timestampMilliSeconds(dia) {
    var hoy = new Date();
    hoy.setHours(0);
    hoy.setMinutes(0);
    hoy.setSeconds(0);
    hoy.setMilliseconds(0);
    if(dia == "manana")
      hoy.setDate(hoy.getDate() + 1);
    else if(dia == "primero")
      hoy.setDate((hoy.getDate() + 1) - hoy.getDate());
    return hoy.getTime();
  }

  var min = () => {
    $("#allRutas").collapse('toggle');
    $("#min-plus").attr("class",mini ? "fa fa-minus" :"fa fa-plus" );
    mini = !mini;
  }

  var selectRuta = (id) => {
    for(var i=0;i<paradas.length;i++){
                paradas[i].marcador.setMap(null);
                paradas[i].marcador.setAnimation(null);
            }
    for(var i=0;i<paradas.length;i++){
      if(paradas[i].ruta == id){
        paradas[i].marcador.setMap(map);
        paradas[i].marcador.setAnimation(google.maps.Animation.BOUNCE);
      }
    }
  }

  var clearParadas = () => {
    for(var i = 0; i < paradas.length; i++){
      paradas[i].marcador.setMap(null);
      paradas[i].marcador.setAnimation(null);
    }
  }

  var save_registro = (pasajero) => {
    $.ajax({
            url: '{{ route('pasajero.save')}}',
            type: 'POST',
            data: pasajero,
            success: function(response) {
              console.log(response);
            },
            error: function(response) {
              console.log(response);
            }
        });
  }

  var get_json_conductor_ruta = () => {
    $.ajax({
      url:'{{route('conductor.ruta.asignacion')}}',
      type:'GET',
      success: function(response){
        console.log(response);
      },
      error: function(response){
        console.log(response);
      }
    });
  }

  var Registro_Servicios = (objReg) => {
    var arraySemanas = [];
    var fecha_inicio_mes = new Date(2018, 11, 17, 0, 0, 0);
    var fecha_fin_mes = new Date(2018, 11, 23, 0, 0, 0);
    var fecha_fin_semana = new Date();
    var filter_fecha;
    var filter_urban;
    var filter_taxi;
    var c_obj = 0;
    var c_semana = 1;
    while(c_obj < objReg.length && fecha_inicio_mes <= fecha_fin_mes){
      fecha_fin_semana.setDate(fecha_inicio_mes.getDate() + 6);
      var itemSemana = {};
      itemSemana["semana"] = "Semana " + c_semana;
      itemSemana["registros"] = [];
      itemSemana["total"] = {};
      itemSemana["total"]["semana"] = {};
      itemSemana["total"]["taxi"] = {};
      itemSemana["total"]["urban"] = {};
      itemSemana["total"]["semana"]["beneficiarios"] = 0;
      itemSemana["total"]["semana"]["acompanantes"] = 0;
      itemSemana["total"]["semana"]["suma"] = 0;
      itemSemana["total"]["taxi"]["beneficiarios"] = 0;
      itemSemana["total"]["taxi"]["acompanantes"] = 0;
      itemSemana["total"]["taxi"]["suma"] = 0;
      itemSemana["total"]["urban"]["beneficiarios"] = 0;
      itemSemana["total"]["urban"]["acompanantes"] = 0;
      itemSemana["total"]["urban"]["suma"] = 0;
      while(fecha_inicio_mes <= fecha_fin_semana && fecha_inicio_mes <= fecha_fin_mes){
        if(fecha_inicio_mes.getDay() != 0/* && fecha_inicio_mes.getDay() != 6*/){ // Omitimos los Domingos y Sabados
          var itemDia = {};
          itemDia["fecha"] = fecha_inicio_mes.toString();
          itemDia["urban"] = {};
          itemDia["taxi"] = {};
          filter_fecha = objReg.filter(r => r.date == fecha_inicio_mes.toString());
          filter_urban = filter_fecha.filter(r => r.servicio == "urban");
          filter_taxi = filter_fecha.filter(r => r.servicio == "taxi");
          itemDia["total_dia"] = filter_fecha.length;
          itemSemana["total"]["semana"]["suma"] += filter_fecha.length;
          itemDia["urban"]["beneficiarios"] = filter_urban.map(b => {return pasajeros[b.clave].tipoPasajero}).filter(r => r == "BENEFICIARIO").length;
          itemDia["urban"]["acompanantes"] = filter_urban.map(b => {return pasajeros[b.clave].tipoPasajero}).filter(r => r == "ACOMPAÑANTE").length;
          itemDia["urban"]["total"] = filter_urban.length;
          itemDia["taxi"]["beneficiarios"] = filter_taxi.map(b => {return pasajeros[b.clave].tipoPasajero}).filter(r => r == "BENEFICIARIO").length;
          itemDia["taxi"]["acompanantes"] = filter_taxi.map(b => {return pasajeros[b.clave].tipoPasajero}).filter(r => r == "ACOMPAÑANTE").length;
          itemDia["taxi"]["total"] = filter_taxi.length;
          itemSemana["total"]["taxi"]["beneficiarios"] += itemDia["taxi"]["beneficiarios"];
          itemSemana["total"]["taxi"]["acompanantes"] += itemDia["taxi"]["acompanantes"];
          itemSemana["total"]["taxi"]["suma"] += itemDia["taxi"]["total"];
          itemSemana["total"]["urban"]["beneficiarios"] += itemDia["urban"]["beneficiarios"];
          itemSemana["total"]["urban"]["acompanantes"] += itemDia["urban"]["acompanantes"];
          itemSemana["total"]["urban"]["suma"] += itemDia["urban"]["total"];
          itemSemana["total"]["semana"]["beneficiarios"] += (itemDia["taxi"]["beneficiarios"] + itemDia["urban"]["beneficiarios"]);
          itemSemana["total"]["semana"]["acompanantes"] += (itemDia["taxi"]["acompanantes"] + itemDia["urban"]["acompanantes"]);
          itemSemana["registros"].push(itemDia);
        }
        c_obj += objReg.filter(r => r.date == fecha_inicio_mes.toString()).length;
        fecha_inicio_mes.setDate(fecha_inicio_mes.getDate() + 1);
      }
      arraySemanas.push(itemSemana);
      c_semana++;
    }
    console.log(arraySemanas);
  }

  function cambiarServicio(unidad, estado){
    servicioCambio = estado['estado']
    mensaje = servicioCambio ? 'Se cambiará a servicio de TAXI la unidad '+unidad : 'Se cambiará a servicio de URBAN la unidad '+unidad    
    console.log(estado)
    swal({
      title: '',
      text: mensaje,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No, regresar',
      reverseButtons: true,
      allowEscapeKey: false,
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        if(conductoresSesionNoIniciada.filter(u => u.unidad == unidad).length == 0) {
          swal({
              title: 'Imposible reasignar',
              text: 'Por sesión iniciada',
              type: 'error',
              timer: 3000,
              allowEscapeKey: false,
				      allowOutsideClick: false
              }).then((result) => {
                  if (result.dismiss === swal.DismissReason.timer || result.value === true)
                    window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false)
          })
        } else {
          $.get('/fotradis/asignacion/'+unidad, function(data) {
          })
          .done(function(data) {
            console.log("SUCCESS "+data.val.conductor)
            controlrutas.boton('0','change', data)

            var optionConductor = new Option(data.val.conductor,data.val.conductor_id,true,true)
            var optionUnidad = new Option(data.val.unidad+" "+data.val.servicio,data.val.unidad_id,true,true)
            var optionRecorrido = new Option(data.val.recorrido,data.val.recorrido_id,true,true)

            $('#conductor_id').append(optionConductor).trigger('change')
            $('#unidad_id').append(optionUnidad).trigger('change')
            $('#recorrido_id').append(optionRecorrido).trigger('change')
          })
          .fail(function(data) {			
            console.log("FAIL "+data)
          });
        }
      } else {
        window.LaravelDataTables['rutas_del_dia'].ajax.reload(null, false);
      }
    })
  }
</script>
@endpush