<form id="form_modal_agregar_conductor" method="POST" action="#">
    <!-- Modal para entregar apoyo-->
    <div class="modal fade" id="modal_modo_agregar_conductor" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-success">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
            <center><h4 class="modal-title">Nuevo conductor</h4></center>
          </div>
          <div class="modal-body">
            <div class="">
              <div class="form-group">
                <label for="conductor">Empleados disponibles</label>
                  <select id="conductor" name="conductor" class="form-control select2" title="Elegir empleado" style="width: 100%;">
                  </select>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary pull-rigth" data-toggle="modal" title="Seleccionar empleado" onclick="empleados.seleccionar_empleado('')">Seleccionar</button>
          </div>
        </div>
      </div>
    </div>
</form>

<div class="modal fade" id="modal_ver_conductor">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Visualizando conductor</h4></center>
      </div>
      <div id="body_modal_ver_conductor" class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

