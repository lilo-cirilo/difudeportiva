<div id="ver_licencia">
  <div class="modal fade" id="modal_ver_licencia" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn-info">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <center><h4 class="modal-title">Visualizando licencia</h4></center>
        </div>
        <div id="body_modal_ver_licencia" class="modal-body">
            <div class="form-group">
              <label for="tipo">Tipo de licencia:</label>
              <input type="text" class="form-control input_licencia" id="tipo" name="tipo" value="{{ isset($licencia) ? $licencia->tipo: ''}}" />
              {{--<select id="tipo" name="tipo" class="form-control">
                  <option>A</option>
                  <option>B</option>
                  <option>C</option>
                  <option>D</option>
              </select>--}}
            </div>

            <div class="form-group">
              <label for="vigencia">Fecha de vigencia:</label>
              <input type="text" class="form-control datepicker input_licencia" id="vigencia" name="vigencia" value="{{ isset($licencia) ? $licencia->vigencia: ''}}" />
            </div>

            <div class="form-group">
              <label for="numero_licencia">Número de licencia:</label>
              <input type="text" class="form-control input_licencia" id="numero_licencia" name="numero_licencia" value="{{ isset($licencia) ? $licencia->numero_licencia: ''}}" />
            </div>

            <div class="form-group">
              <label for="fecha_expedicion">Fecha de expedición:</label>
              <input type="text" class="form-control datepicker input_licencia" id="fecha_expedicion" name="fecha_expedicion" value="{{ isset($licencia) ? $licencia->fecha_expedicion: ''}}" />
            </div>
          
            <div class="form-group">
              <label for="oficina_expedidora">Oficina de expedición:</label>
              <input type="text" class="form-control input_licencia" id="oficina_expedidora" name="oficina_expedidora" value="{{ isset($licencia) ? $licencia->oficina_expedidora: ''}}" />
            </div>
            
            <div class="form-group">
              <label for="entidad_id">Entidad federativa:</label>
              <input type="text" class="form-control input_licencia" id="entidad_id" name="entidad_id" value="{{ isset($licencia) ? $entidad: ''}}" />
              {{--<select id="entidad_id" name="entidad_id" class="form-control select2">--}}
              </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="modal_ver_conductor()">Cerrar</button>
          <!--<a class="btn btn-danger pull-right" onclick="licencias.eliminar_licencia({{ isset($licencia) ? $licencia->id : ''}})"><i class="fa fa-trash-o"></i> Eliminar</a>-->
        </div>
      </div>
    </div>
  </div>
</div>
