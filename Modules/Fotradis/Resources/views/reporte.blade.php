<div class="modal fade" id="modal-reporte" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <center><h4 class="modal-title">Generar reporte</h4></center>
      </div>
      <div class="modal-body">
        <form id="form-reporte">
          <div class="form-group">
            <label for="fechaIni">De:</label>
            <input type="text" class="form-control datepicker" id="fechaIni" name="fechaIni"/>	            
          </div>
      
          <div class="form-group">
            <label for="fechaFin">A:</label>
            <input type="text" class="form-control datepicker" id="fechaFin" name="fechaFin"/>
          </div>

          <div class="form-group">
            <label for="tipo-reporte">Seleccione el tipo de reporte que desea obtener:</label>
            <select class="form-control select2" id="tipo-reporte" class="tipo-reporte">
              <option value="heatmapdata">Visualizar mapa de calor</option>
              <option value="info">Reporte, registro de servicios</option>
              <option value="bitacora">Reporte, registro de kilometraje y gasolina</option>
              <option value="mantenimiento">Reporte, registro de mantenimientos a unidades</option>
            </select>
          </div>

          <div id="tipo-mapa-calor-div" class="form-group">
            <label for="tipo-mapa-calor">Seleccione el tipo de mapa de calor que desea obtener:</label>
            <select class="form-control select2 tipo" id="tipo-mapa-calor" name="tipo-mapa-calor">
              <option value="subidas">Subidas</option>
              <option value="bajadas">Bajadas</option>
            </select>
          </div>

          <div id="tipo-servicios-div" class="form-group" style="display:none;">
            <label for="tipo-servicio">Seleccione el tipo de reporte servicios:</label>
            <select class="form-control select2 tipo" id="tipo-servicio" name="tipo-servicio">
              <option value="general">General</option>
              <option value="especifico">Por conductor y por pasajero</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-danger pull-rigth" onclick="generarReporte();">Listo</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-mapacalor" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
        <center><h4 id="mapacalor-titulo" class="modal-title">Mapa de calor, zonas donde se suben pasajeros</h4></center>
      </div>
      <div class="modal-body">
        <div id="map" style="background-color: lightblue;height: calc(100vh - 211px);"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>