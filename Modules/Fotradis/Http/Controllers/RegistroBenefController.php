<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\View;

use App\DataTables\Fotradis\PasajerosDataTable;
use App\Models\DTLL\Pagos;
use App\Models\DTLL\TipoPasajero;
use App\Models\DTLL\TipoServicio;
use App\Models\DTLL\Pasajero;
use App\Models\DTLL\ServiciosAdquiridos;
use App\Models\DTLL\LugarPago;

use App\Models\Pais;
use App\Models\Entidad;
use App\Models\Municipio;
use App\Models\Localidad;
use App\Models\Persona;
use App\Models\Etnia;


class RegistroBenefController extends Controller
{
  public function __construct()
    {
      $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR',['except' => ['show','index']]);
      $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

  public function index(PasajerosDataTable $dataTable)
  {
    return $dataTable->render('fotradis::registrobenefs.idxListarPasajeros');
  }

  public function showFrm_AddPasajero()
  {
    return view('fotradis::registrobenefs.frmCrearEditarPasajero');

  }

	public function storePasajero(Request $request)
  {
    if($request->ajax()) {
    try{
      $datos = $request->all();
      /* *********************** HACEMOS ALGUNAS VERIFICACIONES *********************** */
      // Verificamos que la letra de la pulsera coincida con el tipo de servicio
      if ($datos['tipoServicio']=='1' && (strpos(strtoupper($datos['numPasajero']),'A') === false && strpos(strtoupper($datos['numPasajero']),'C') === false ) )  //Si es uno y no tiene 'A'
        throw new \Exception("LETRA_URBAN~".strtoupper($datos['numPasajero']));
      if ($datos['tipoServicio']=='2' && strpos(strtoupper($datos['numPasajero']),'B') === false) //Si es 2 y no tiene 'B'
        throw new \Exception("LETRA_TAXI~".strtoupper($datos['numPasajero']));
      if ($datos['tipoServicio']=='3'){ // Para el caso de ambos, por comodidad ponemos siempre en la variable numPasajero el de TAXI y en numPasajeroAmbos el de urban
        if (strpos(strtoupper($datos['numPasajero']),'B') === false) {
          $temp = strtoupper($datos['numPasajeroAmbos']);
          $datos['numPasajeroAmbos'] = strtoupper($datos['numPasajero']);
          $datos['numPasajero'] = $temp;
        }
      }

      /* *********************** OBTENEMOS ALGUNOS DATOS GENERALES *********************** */
      $localidad_id = Localidad::where('municipio_id', '=', $datos['municipio_id'])
                  ->where('nombre', '=', strtoupper($datos['localidad_id']))
                  ->get(['id']);
      $datos['usuario_id'] = Auth::user()->id; //$request->User()->id



      /* *********************** CREAMOS U OBTENEMOS LOS DATOS PERSONALES DEL BENEFICIARIO *********************** */
      $datosPersona['nombre'] = strtoupper($datos['nombre']);
      $datosPersona['primer_apellido'] = strtoupper($datos['primerApe']);
      $datosPersona['segundo_apellido'] = strtoupper($datos['segundoApe']);
      $datosPersona['curp'] = strtoupper($datos['curp']);
      if (strlen($datos['curp'])>=10 ) {
        if (strtoupper(substr($datos['curp'],10,1)) == 'M' || strtoupper(substr($datos['curp'],10,1)) == 'H')
          $datosPersona['genero'] = strtoupper(substr($datos['curp'],10,1)) === 'M'?'F':'M';
        if (ctype_digit(substr($datos['curp'],4,6))){     //Comprobamos si es numérico la posición donde va la fecha en la curp
          $year = (int)substr($datos['curp'],4,2);
          $anio = (($year)>=25 && ($year)<=99 ? '19':'20').$year;
          $datosPersona['fecha_nacimiento'] = $anio.'-'.substr($datos['curp'],6,2).'-'.substr($datos['curp'],8,2);
        }
      }
      $datosPersona['calle'] = strtoupper($datos['calle']);
      $datosPersona['numero_exterior'] = strtoupper($datos['numero']);
      $datosPersona['colonia'] = strtoupper($datos['colonia']);
      $datosPersona['localidad_id'] = ($localidad_id && count($localidad_id)==1 ) ? $localidad_id->pluck('id')[0] : null;
      $datosPersona['numero_celular'] = strlen($datos['telefono'])!==7 ? $datos['telefono'] : '';
      $datosPersona['numero_local'] = strlen($datos['telefono'])===7 ? $datos['telefono'] : '';
      $datosPersona['fotografia'] = $datos['foto'];
      $datosPersona['etnia_id'] = $datos['etnia_id'];
      $datosPersona['municipio_id'] = $datos['municipio_id'];
      $datosPersona['usuario_id'] = $datos['usuario_id'];

      DB::beginTransaction();
      //$persona = Persona::where('nombre',$datosPersona['nombre'])->where('primer_apellido',$datosPersona['primer_apellido'])->where('curp',$datosPersona['curp'])->first();
      $persona = Persona::firstOrCreate(
                              ['nombre'=>$datosPersona['nombre'], 'primer_apellido'=>$datosPersona['primer_apellido'], 'curp'=>$datosPersona['curp']],
                              $datosPersona
                            ); //Personas::create($datosPersona);  // $persona->update($datosPersona); // Programa::find($id)->update($request->all());
      /* *********************** INSERTAMOS EL REGISTRO DE PASAJERO *********************** */

      $pasajero_id = Pasajero::firstOrCreate(['persona_id'=>$persona->id],[
        'persona_id' => $persona->id,
        'pais_id' => $datos['pais'],
        'entidad_id' => $datos['entidad'],
        'localidad' => $datos['localidad_id'],
        'persona_emergencias' => $datos['persona_emergencias'],
        'telefono_emergencias' => $datos['telefono_emergencias'],
        'usuario_id' => $datos['usuario_id'],
        ])->id;

      // Checamos que el número de pasajero no esté en uso
      $servicio_adquirido = ServiciosAdquiridos::where('numPasajero',$datos['numPasajero'])->first();
      if ($servicio_adquirido)
        throw new \Exception("NUMPASAJERO_OCUPADO~".strtoupper($datos['numPasajero']));
      if ($datos['tipoServicio'] == '3') {
        $servicio_adquirido = ServiciosAdquiridos::where('numPasajero',$datos['numPasajeroAmbos'])->where('pasajero_id',$pasajero_id)->first();
        if ($servicio_adquirido)
        throw new \Exception("NUMPASAJERO_OCUPADO~".strtoupper($datos['numPasajeroAmbos']));
      }
      /* *********************** INSERTAMOS EL REGISTRO DE SERVICIO ADQUIRIDO *********************** */
      $servicio_adquirido = ServiciosAdquiridos::firstOrCreate(['numPasajero'=> $datos['numPasajero']],[
        'pasajero_id' => $pasajero_id,
        'tiposervicio_id' => $datos['tipoServicio']=="3"?"2":$datos['tipoServicio'],
        'tipopasajero_id' => $datos['tipoPasajero'],
        'numPasajero' => strtoupper($datos['numPasajero']),
        'usuario_id' => $datos['usuario_id'],
      ]);
      auth()->user()->bitacora(request(), [
        'tabla' => 'dtll_serviciosadquiridos',
        'registro' => $servicio_adquirido->id . '',
        'campos' => json_encode($servicio_adquirido) . '',
        'metodo' => request()->method()
      ]);
      if ($datos['tipoServicio']==="3")  //Si es taxi o ambos
      {
        $servicio_adq = ServiciosAdquiridos::firstOrCreate(['numPasajero'=> $datos['numPasajeroAmbos']],[
          'pasajero_id' => $pasajero_id,
          'tiposervicio_id' => "1",
          'tipopasajero_id' => $datos['tipoPasajero'],
          'numPasajero' => strtoupper($datos['numPasajeroAmbos']),
          'usuario_id' => $datos['usuario_id'],
        ]);
        auth()->user()->bitacora(request(), [
        'tabla' => 'dtll_serviciosadquiridos',
        'registro' => $servicio_adq->id . '',
        'campos' => json_encode($servicio_adq) . '',
        'metodo' => request()->method()
      ]);
      }

      /* *********************** INSERTAMOS EL REGISTRO DE PAGO *********************** */
      $fechaFinServ = $this->addDaysToDate($this->reverseDate ($datos['fechaIniServ'], '/', '-'),((int)$datos['numMesesServ'])*30);
      if ($datos['tipoServicio']!=="1"){  //Si es taxi o ambos
        Pagos::create([
                      'servicioadquirido_id'=>$servicio_adquirido->id,
                      'fechaIniServ' => (\DateTime::createFromFormat('d/m/Y', $datos['fechaIniServ']))->format('Y-m-d'),
                      'fechaFinServ' => $fechaFinServ,
                      'numMesesServ' => $datos['numMesesServ'],
                      'folioPago' => $datos['folioPago'],
                      'lugarPago_id' => $datos['lugarPago'],
                      'usuario_id' => $datos['usuario_id']
                    ]);
      }

      DB::commit();
      return response()->json(array(
                                      'success' => true,
                                      'estatus' => 'insertado',
                                      'rutaPeticion' => route('registrobenef.updateFoto',$persona->id),
                                      'yaTieneFoto'=>(($persona->fotografia===null || $persona->fotografia==="")?'no':'si' )
                                    )
                              );
    }catch(\Exception $e){
      DB::rollBack();

      if ($e !== null || $e->getMessage() !== null)
      {
        $mensajes = explode('~',$e->getMessage());
        if (strpos($e->getMessage(),"LETRA_URBAN") !== false)
          return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"La letra del número de pasajero ".$mensajes[1]." no corresponde al tipo URBAN especificado"));
        else if (strpos($e->getMessage(),"LETRA_TAXI") !== false)
          return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"La letra del número de pasajero ".$mensajes[1]." no corresponde al tipo TAXI especificado"));
        else if (strpos($e->getMessage(),"NUMPASAJERO_OCUPADO") !== false)
          return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"El número de pasajero ".$mensajes[1]." ya está asignado"));
        else
          abort(500,$e->getMessage());
      }
      // return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
    }
    }
  }

  public function showMdl_editPasajero(Request $request)
  {
    $pasajero = Pasajero::select(
                                's.pasajero_id','s.tiposervicio_id',
                                DB::raw('(CASE WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id  AND sa.deleted_at is null) = 2 THEN "AMBOS" ELSE ts.nombre END) as tipoServicio'),
                                's.tipopasajero_id','tp.nombre AS tipoPasajero',
                                DB::raw('(SELECT DATE_FORMAT(fechaIniServ,"%d/%m/%Y") FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE sa.pasajero_id=s.pasajero_id and pg.servicioadquirido_id=sa.id AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) AS fechaIniServ'),
                                DB::raw('(SELECT DATE_FORMAT(fechaFinServ,"%d/%m/%Y") FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE sa.pasajero_id=s.pasajero_id and pg.servicioadquirido_id=sa.id AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) AS fechaFinServ'),
                                DB::raw('(SELECT numMesesServ FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) AS numMesesServ'),
                                DB::raw('(SELECT folioPago FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) as folioPago'),
                                DB::raw('(SELECT pg.id FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) as pago_id'),
                                DB::raw('(CASE '.
                                  'WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND sa.deleted_at is null)=2 '.
                                  'THEN (SELECT numPasajero FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND numPasajero LIKE "%B%" AND sa.deleted_at is null) '.
                                  'ELSE s.numPasajero '.
                                'END) AS numPasajero'),
                                DB::raw('(CASE '.
                                  'WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND sa.deleted_at is null)=2 '.
                                  'THEN (SELECT numPasajero FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND numPasajero LIKE "%A%" AND sa.deleted_at is null) '.
                                  'ELSE "" '.
                                'END) AS numPasajeroAmbos'),
                                'pe.nombre','pe.primer_apellido AS primerApe','pe.segundo_apellido AS segundoApe','pe.curp','pe.etnia_id','et.nombre as etnia',
                                DB::raw('(CASE WHEN LENGTH(pe.numero_celular)=10 THEN pe.numero_celular ELSE pe.numero_local END) AS telefono'),
                                'pais_id','cp.nombre AS pais','entidad_id','ce.entidad','pe.calle','pe.numero_exterior AS numero','pe.colonia',
                                'pe.localidad_id', DB::raw('IFNULL ((SELECT nombre FROM cat_localidades WHERE id=pe.localidad_id AND deleted_at is null), dtll_pasajeros.localidad) AS localidad'),
                                'dtll_pasajeros.localidad as localidad2', 'cm.nombre AS municipio','cm.id AS municipio_id','dtll_pasajeros.persona_emergencias','dtll_pasajeros.telefono_emergencias','pe.fotografia AS foto')
                        ->join('personas AS pe','dtll_pasajeros.persona_id','pe.id')
                        ->join('dtll_serviciosadquiridos AS s','dtll_pasajeros.id','s.pasajero_id')
                        ->join('dtll_cat_tiposservicio AS ts','s.tiposervicio_id','ts.id')
                        ->join('dtll_cat_tipospasajero AS tp','s.tipopasajero_id','tp.id')
                        ->leftJoin('cat_paises AS cp','dtll_pasajeros.pais_id','cp.id')
                        ->leftJoin('cat_entidades AS ce','dtll_pasajeros.entidad_id','ce.id')
                        ->leftJoin('cat_municipios AS cm','pe.municipio_id','cm.id')
												->join('cat_etnias AS et','pe.etnia_id','et.id')
                        ->where('dtll_pasajeros.id',$request->id)
                        ->first();



    return view('fotradis::registrobenefs.frmCrearEditarPasajero')->with('pasajero', $pasajero);
  }

	public function updatePasajero(Request $request, $pasajero_id)
  {
    if($request->ajax())
    {
      try{
        $datos = $request->all();
				$pasajero = Pasajero::find($pasajero_id);


					/* *********************** OBTENEMOS ALGUNOS DATOS GENERALES *********************** */
					$datos['usuario_id'] = Auth::user()->id;
					$localidad_id = Localidad::where('municipio_id', '=', $datos['municipio_id'])
											->where('nombre', '=', strtoupper($datos['localidad_id']))
											->get(['id']);

					/************************* ESTABLECEMOS LOS DATOS DEL PASAJERO *************************/
					DB::beginTransaction();

					$pasajero->persona_emergencias=$datos['persona_emergencias'];
					$pasajero->telefono_emergencias=$datos['telefono_emergencias'];
					$pasajero->localidad=$datos['localidad_id'];
					$pasajero->usuario_id=$datos['usuario_id'];
					$pasajero->save();

					/************************* ESTABLECEMOS LOS DATOS DE LA PERSONA *************************/
					$persona = $pasajero->persona;

					$persona->nombre = strtoupper($datos['nombre']);
					$persona->primer_apellido = strtoupper($datos['primerApe']);
					$persona->segundo_apellido = strtoupper($datos['segundoApe']);
					$persona->curp = strtoupper($datos['curp']);
					if (strlen($datos['curp'])>=10 ) {
						if (strtoupper(substr($datos['curp'],10,1)) == 'M' || strtoupper(substr($datos['curp'],10,1)) == 'H')
							$persona->genero = strtoupper(substr($datos['curp'],10,1)) === 'M'?'F':'M';
						if (ctype_digit(substr($datos['curp'],4,6))){     //Comprobamos si es numérico la posición donde va la fecha en la curp
							$year = (int)substr($datos['curp'],4,2);
							$anio = (($year)>=25 && ($year)<=99 ? '19':'20').$year;
							$persona->fecha_nacimiento = $anio.'-'.substr($datos['curp'],6,2).'-'.substr($datos['curp'],8,2);
						}
					}

					$persona->calle = $datos['calle'];
					$persona->numero_exterior = $datos['numero'];
					$persona->colonia = $datos['colonia'];
					$persona->localidad_id = ($localidad_id && count($localidad_id)==1 ) ? $localidad_id->pluck('id')[0] : null;
					$persona->numero_celular = strlen($datos['telefono'])!==7 ? $datos['telefono'] : '';
					$persona->numero_local = strlen($datos['telefono'])===7 ? $datos['telefono'] : '';
					//$datosPersona['fotografia'] = $datos['foto'];
					$persona->etnia_id = $datos['etnia'];
					$persona->municipio_id = $datos['municipio_id'];
					$persona->usuario_id=$datos['usuario_id'];
					$persona->save();

					/* ACTUALIZAMOS EN BITÁCORA*/
					auth()->user()->bitacora(request(), [
						'tabla' => 'dtll_pasajeros',
						'registro' => $pasajero->id . '',
						'campos' => json_encode($pasajero) . '',
						'metodo' => request()->method()
					]);
					auth()->user()->bitacora(request(), [
						'tabla' => 'personas',
						'registro' => $persona->id . '',
						'campos' => json_encode($persona) . '',
						'metodo' => request()->method()
					]);

					DB::commit();
					return response()->json(array(
                                      'success' => true,
                                      'estatus' => 'insertado',
                                      'rutaPeticion' => route('registrobenef.updateFoto',$persona->id),
                                      'yaTieneFoto'=> (($persona->fotografia===null || $persona->fotografia==="")?'no':'si' ),
																			'fotoFirebase'=>(($persona->fotografia!=null && strpos($persona->fotografia, "firebasestorage")>=1)?'si':'no')
                                    )
                              );


      }catch(\Exception $e){
        DB::rollBack();

        if ($e !== null || $e->getMessage() !== null)
        {
          $mensajes = explode('~',$e->getMessage());
          if (strpos($e->getMessage(),"NUMPASAJERO_OCUPADO") !== false)
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"El número de pasajero ".$mensajes[1]." ya está asignado"));
          else
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>$e->getMessage()));
        }
      }
    }
  }

  public function updateFoto($persona_id, Request $request)
  {
    try {
      $persona = Persona::findOrFail($persona_id)->update(['fotografia'=>$request->input('foto','')]);
      return response()->json(array('success' => true, 'estatus' => 'actualizado'));
    }catch(\Exception $e){
      abort(500,$e->getMessage());
    }
  }

  public function showMdl_watchPasajero (Request $request)
  {
    $pasajero = Pasajero::select(
                                's.pasajero_id',
                                DB::raw('(CASE WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id  AND sa.deleted_at is null) = 2 THEN "AMBOS" ELSE ts.nombre END) as tipoServicio'),
                                'tp.nombre AS tipoPasajero',
                                DB::raw('(SELECT MIN(fechaIniServ) FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE sa.pasajero_id=s.pasajero_id and pg.servicioadquirido_id=sa.id AND fechaFinServ>NOW() AND sa.deleted_at is null AND pg.deleted_at is null) AS fechaIniServ'),
                                DB::raw('(SELECT MAX(fechaFinServ) FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE sa.pasajero_id=s.pasajero_id and pg.servicioadquirido_id=sa.id AND sa.deleted_at is null AND pg.deleted_at is null) AS fechaFinServ'),
                                DB::raw('(SELECT numMesesServ FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) AS numMesesServ'),
                                DB::raw('(SELECT folioPago FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) as folioPago'),
                                DB::raw('(SELECT pg.id FROM dtll_serviciosadquiridos sa, dtll_pagos pg WHERE pg.servicioadquirido_id=sa.id AND sa.pasajero_id=s.pasajero_id  AND sa.deleted_at is null AND pg.deleted_at is null ORDER BY fechaIniServ DESC LIMIT 1) as pago_id'),
                                DB::raw('(CASE '.
                                  'WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND sa.deleted_at is null)=2 '.
                                  'THEN (SELECT numPasajero FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND numPasajero LIKE "%B%" AND sa.deleted_at is null) '.
                                  'ELSE s.numPasajero '.
                                'END) AS numPasajero'),
                                DB::raw('(CASE '.
                                  'WHEN (SELECT count(sa.id) FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND sa.deleted_at is null)=2 '.
                                  'THEN (SELECT numPasajero FROM dtll_serviciosadquiridos sa WHERE pasajero_id=s.pasajero_id AND numPasajero LIKE "%A%" AND sa.deleted_at is null) '.
                                  'ELSE "" '.
                                'END) AS numPasajeroAmbos'),
                                'pe.nombre','pe.primer_apellido AS primerApe','pe.segundo_apellido AS segundoApe','pe.curp','pe.etnia_id','et.nombre as etnia',
                                DB::raw('(CASE WHEN LENGTH(pe.numero_celular)=10 THEN pe.numero_celular ELSE pe.numero_local END) AS telefono'),
                                'cp.nombre AS pais','ce.entidad AS estado','pe.calle','pe.numero_exterior AS numero','pe.colonia',
																DB::raw('IFNULL ((SELECT nombre FROM cat_localidades WHERE id=pe.localidad_id AND deleted_at is null), dtll_pasajeros.localidad) AS localidad'),
                                'cm.nombre AS municipio','dtll_pasajeros.persona_emergencias','dtll_pasajeros.telefono_emergencias','pe.fotografia AS foto')
                        ->join('personas AS pe','dtll_pasajeros.persona_id','pe.id')
                        ->join('dtll_serviciosadquiridos AS s','dtll_pasajeros.id','s.pasajero_id')
                        ->join('dtll_cat_tiposservicio AS ts','s.tiposervicio_id','ts.id')
                        ->join('dtll_cat_tipospasajero AS tp','s.tipopasajero_id','tp.id')
                        ->leftJoin('cat_paises AS cp','dtll_pasajeros.pais_id','cp.id')
                        ->leftJoin('cat_entidades AS ce','dtll_pasajeros.entidad_id','ce.id')
                        ->leftJoin('cat_municipios AS cm','pe.municipio_id','cm.id')
												->join('cat_etnias AS et','pe.etnia_id','et.id')
                        ->where('dtll_pasajeros.id',$request->id)
                        ->first();

    $view = View::make('fotradis::registrobenefs.partials.ptlVerPasajero')->with('pasajero', $pasajero);
    $html = $view->render();
    return response()->json(['status' => 'ok', 'html' => $html /*, 'html2' => $html2*/], 200);  // $html2 = '<button type="button" class="btn btn-primary pull-rigth" onclick="licencias.ver_licencia('. $licencia->id .')"><i class="fa fa-fw fa-eye"></i>Ver licencia</button>';
  }

  public function showMdl_deleteServicio(Request $request, $id)
  {
    if($request->ajax())
    {
      try{
        $datos = $request->all();
        $datos['usuario_id'] = Auth::user()->id;
        $datos['pasajero_id'] = $id;
        $servicios = ServiciosAdquiridos::select('dtll_serviciosadquiridos.id','dtll_serviciosadquiridos.pasajero_id','ts.nombre AS servicio','dtll_serviciosadquiridos.numPasajero')
                                        ->join('dtll_cat_tiposservicio AS ts','dtll_serviciosadquiridos.tiposervicio_id','ts.id')
                                        ->where('pasajero_id',$datos['pasajero_id'])
                                        ->get();
        $servicio = $servicios[0];
        $servicioAmbos = ($servicios->count('id') == 2) ? $servicios[1] : null;

        $view = View::make('fotradis::registrobenefs.partials.ptlEliminarServicio')->with('servicio', $servicio)->with('servicioAmbos', $servicioAmbos);
        $html = $view->render();
        return response()->json(['status' => 'ok', 'html' => $html, 'numServicios'=>$servicios->count('id')], 200);
      }catch(\Exception $e){
        if ($e !== null || $e->getMessage() !== null)
        {
          $mensajes = explode('~',$e->getMessage());
          if (strpos($e->getMessage(),"NUMPASAJERO_OCUPADO") !== false)
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"El número de pasajero ".$mensajes[1]." ya está asignado"));
          else
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>$e->getMessage()));
        }
      }
    }

  }

  public function deleteServicio(Request $request)
  {
    if($request->ajax())
    {
      try{
        $datos = $request->all();
        $datos['usuario_id'] = Auth::user()->id;
        DB::beginTransaction();
        if ($datos['numPasajero_isChecked'] === "true"){
          $servicio = ServiciosAdquiridos::find($datos['servicioadquirido_id']);
          $pago = Pagos::where('servicioadquirido_id',$datos['servicioadquirido_id']);
          $servicio->delete();
          $pago ->delete();
          auth()->user()->bitacora(request(), [
            'tabla' => 'dtll_serviciosadquiridos',
            'registro' => $servicio->id . '',
            'campos' => json_encode($servicio) . '',
            'metodo' => 'DELETE'
          ]);
        }if ($datos['numPasajeroAmbos_isChecked'] === "true") {
          $servicioAmbos = ServiciosAdquiridos::find($datos['servicioadquiridoAmbos_id']);
          $pago = Pagos::where('servicioadquirido_id',$datos['servicioadquiridoAmbos_id']);
          $servicioAmbos->delete();
          $pago->delete();
          auth()->user()->bitacora(request(), [
            'tabla' => 'dtll_serviciosadquiridos',
            'registro' => $servicioAmbos->id . '',
            'campos' => json_encode($servicioAmbos) . '',
            'metodo' => 'DELETE'
          ]);
        }

        if ( ($datos['numPasajero_isChecked'] === "true" && $datos['numPasajeroAmbos_isChecked'] === "true") || ($datos['numPasajero_isChecked'] === "true" && $datos['servicioadquiridoAmbos_id'] === "undefined") ) {
          $pasajero = ServiciosAdquiridos::withTrashed()->find($datos['servicioadquirido_id'])->pasajero;
          $pasajero->delete();
          auth()->user()->bitacora(request(), [
            'tabla' => 'dtll_pasajeros',
            'registro' => $pasajero->id . '',
            'campos' => json_encode($pasajero) . '',
            'metodo' => 'DELETE'
          ]);
        }

        DB::commit();
        return response()->json(array('success' => true, 'estatus' => 'eliminado'));
      }catch(\Exception $e){
        DB::rollBack();
        return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>$e->getMessage()));
      }
    }
  }

  public function showMdl_pagarServicio (Request $request)
  {
    $pago = Pagos::select('dtll_pagos.id', 'dtll_pagos.servicioadquirido_id', 'dtll_pagos.fechaIniServ', 'dtll_pagos.fechaFinServ')
                        ->join('dtll_serviciosadquiridos AS s','dtll_pagos.servicioadquirido_id','s.id')
                        ->where('s.pasajero_id',$request->id)
                        ->orderBy('fechaIniServ', 'desc')
                        ->first();

    $view = View::make('fotradis::registrobenefs.partials.ptlPagarServicio')->with('pago', $pago);
    $html = $view->render();
    return response()->json(['status' => 'ok', 'html' => $html], 200);
  }

  public function storePagoServicio(Request $request)
  {
    if($request->ajax())
    {
      try{
        $datos = $request->all();
        $datos['usuario_id'] = Auth::user()->id;
        $datos['lugarPago_id'] = 1;
        $datos['fechaIniServ'] = $this->reverseDate ($datos['fechaIniServ'], '/', '-');
        $datos['fechaFinServ'] = $this->reverseDate ($datos['fechaFinServ'], '/', '-');

        DB::beginTransaction();
        //servicioadquirido_id, fechaIniServ, fechaFinServ, numMesesServ, folioPago, lugarPago_id, usuario_id
        $pago = Pagos::create($datos);

        auth()->user()->bitacora(request(), [
          'tabla' => 'dtll_pagos',
          'registro' => $pago->id . '',
          'campos' => json_encode($pago) . '',
          'metodo' => request()->method()
        ]);

        DB::commit();
        return response()->json(array('success' => true, 'estatus' => 'insertado'));
      }catch(\Exception $e){
        DB::rollBack();

        if ($e !== null || $e->getMessage() !== null)
        {
          $mensajes = explode('~',$e->getMessage());
          if (strpos($e->getMessage(),"NUMPASAJERO_OCUPADO") !== false)
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>"El número de pasajero ".$mensajes[1]." ya está asignado"));
          else
            return response()->json(array('success' => false, 'estatus' => 'rechazado', 'mensaje'=>$e->getMessage()));
        }
      }
    }
  }

	public function showMdl_ReporteInscritos (Request $request)
	{
    $view = View::make('fotradis::registrobenefs.partials.ptlRepInscritos');
    $html = $view->render();
    return response()->json(['status' => 'ok', 'html' => $html], 200);
	}

	public function ReporteInscritos(Request $request)
	{
		$datos = $request->all();
		$reporte = Pasajero::select(
																DB::raw('CASE WHEN MONTH(p.fechaIniServ) IS NULL THEN MONTH(dtll_pasajeros.created_at) ELSE MONTH(p.fechaIniServ) END AS mes'),
                                'pe.nombre', 'pe.primer_apellido', 'pe.segundo_apellido',
                                DB::raw('lower(tp.nombre) as denominacion_social'),
                                DB::raw('concat("Servicio de transporte (", lower(ts.nombre), ")") as beneficio'),
                                DB::raw('"Oaxaca" as unidad_territorial'),
                                DB::raw('floor((DATE_FORMAT(NOW(), "%Y%m%d")-DATE_FORMAT(pe.fecha_nacimiento, "%Y%m%d")) / 10000) as edad'),
                                DB::raw('CASE genero WHEN "M" THEN "Masculino" WHEN "F" THEN "Femenino" END AS sexo')
															)
                        ->join('personas AS pe','dtll_pasajeros.persona_id','pe.id')
                        ->join('dtll_serviciosadquiridos AS s','dtll_pasajeros.id','s.pasajero_id')
                        ->leftJoin('dtll_pagos AS p','s.id','p.servicioadquirido_id')
                        ->join('dtll_cat_tipospasajero AS tp','s.tipopasajero_id','tp.id')
                        ->join('dtll_cat_tiposservicio AS ts','s.tiposervicio_id','ts.id')
												->whereNull('dtll_pasajeros.deleted_at')
												->whereNull('pe.deleted_at')
												->whereNull('s.deleted_at')
												->whereNull('p.deleted_at')
												->whereBetween('dtll_pasajeros.created_at', [$datos['fechaIniServ'], $datos['fechaFinServ']])
                        ->get();

			$nombreColumnas = ["mes","nombre","primer_apellido","segundo_apellido","denominacion_social","beneficio","unidad_territorial","edad","sexo" ];

			return response()->json(['status' => 'ok', 'data' => $reporte, 'cabeceras'=>$nombreColumnas]);
      // return response()->json($reporte);
	}


  public function getTiposServicio(Request $request)
  {
    if ($request->ajax()){
      $tiposservicio = TipoServicio::get()->toArray();
      return response()->json($tiposservicio);
    }
  }

  public function getTiposPasajero(Request $request)
  {
    if ($request->ajax()){
      $tipospasajero = TipoPasajero::get()->toArray();
      return response()->json($tipospasajero);
    }
  }

  public function getPaises(Request $request)
  {
    if ($request->ajax()){
      $paises = Pais::where('nombre', 'LIKE', '%' . $request->input('search') . '%')->get()->toArray();
      return response()->json($paises);
    }
  }

  public function getEntidades(Request $request)
  {
    if ($request->ajax()){
      $entidades = Entidad::where('entidad', 'LIKE', '%' . $request->input('search') . '%')->get()->toArray();
      return response()->json($entidades);
    }
  }

  public function getMunicipios(Request $request)
  {
    if ($request->ajax()){
      $municipios = Municipio::where('nombre', 'LIKE', '%' . $request->input('search') . '%')->get()->toArray();;
      return response()->json($municipios);
    }
  }

  public function getLugaresDePago(Request $request)
  {
    if ($request->ajax()){
      $lugarPago = LugarPago::where('nombre', 'LIKE', '%' . $request->input('search') . '%')->get()->toArray();
      return response()->json($lugarPago);
    }
  }

	public function getEtnias(Request $request)
  {
    if ($request->ajax()){
      $etnias = Etnia::where('nombre', 'LIKE', '%' . $request->input('search') . '%')->get()->toArray();;
      return response()->json($etnias);
    }
  }

  public function addDaysToDate($fecha,$dias)
  {
    $nuevafecha = strtotime ( $dias." day" , strtotime ( $fecha ) );
    $nuevafecha = date ( 'Y-m-d' , $nuevafecha ); //formatea nueva fecha
    return $nuevafecha; //retorna valor de la fecha
  }

  /*Invierte una fecha. Si está en formato dd/MM/yyyy la pone como yyyy/MM/dd*/
  public function reverseDate ($fecha, $separador, $nuevoSeparador)
  {
      $nuevaFecha="";
      if ($separador===null || $separador === "")   //Si no se especifica un separador el default será '/'
          $separador = "/";
      if ($fecha==="")
          return "";
      $values = explode($separador,$fecha);
      if ($nuevoSeparador!==null && $nuevoSeparador !== "")   //Si se desea cambiar el separador
          $separador = $nuevoSeparador;
      //Verificamos que haya un formato correcto
      if (strlen($fecha)===10 && count($values)===3)
          $nuevaFecha = $values[2].$separador.$values[1].$separador.$values[0];
      return $nuevaFecha;
  }
}
