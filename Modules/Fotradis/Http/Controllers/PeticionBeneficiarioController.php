<?php

namespace Modules\Fotradis\Http\Controllers;

use App\Http\Controllers\PeticionBeneficiarioBaseController; 

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController { 
    public function __construct() {
        parent::__construct('DIF TE LLEVA');
    }
}