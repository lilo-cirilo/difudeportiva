<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\View;

use App\Models\Empleado;
use App\Models\Persona;
use App\Models\Licencias;
use App\Models\Conductor;
use App\Models\Entidad;

use App\DataTables\Fotradis\ConductoresActuales;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index(ConductoresActuales $datatables)
    {
        return $datatables->render('fotradis::driver.index');
    }

    public function create(Request $request)
    {
        $empleado = Empleado::where('rfc', '=', $request->rfc)->get()->first();
        return response()->json(['id' => $empleado->id]);
    }

    public function show(Request $request)
    {
        $empleado = Empleado::findOrFail($request->empleado);
        $persona = Persona::findOrFail($empleado->persona_id);
        $conductor = Conductor::where('empleado_id', '=', $empleado->id)->get()->first();
        return view('fotradis::driver.create')->with('empleado', $empleado)->with('persona', $persona)->with('conductor', $conductor);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $datos = $request->except(['persona_emergencia', 'telefono_emergencia']);
            $datos['licencia_id'] = Licencias::where('numero_licencia', '=', $datos['licencia_id'])->first()->id;

            $conductor = Conductor::create($datos);

            $dt = $request->only(['clave_conductor']);
            $dt['clave_conductor'] = 'C' . $conductor->id;

            $conductor->update($dt);

            $empleado = Empleado::findOrFail($conductor->empleado_id);

            $empleado->persona_emergencia = $request->persona_emergencia;
            $empleado->telefono_emergencia = $request->telefono_emergencia;
            $empleado->save();

            DB::commit();

            return response()->json(array('success' => true, 'estatus' => 'nuevo', 'conductor' => $conductor->id, 'clave_conductor' => $datos['clave_conductor']));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function update($conductor, Request $request)
    {
        try {
            DB::beginTransaction();
            $datos = $request->except(['persona_emergencia', 'telefono_emergencia', 'clave_conductor']);
            $datos['licencia_id'] = Licencias::where('numero_licencia', '=', $datos['licencia_id'])->first()->id;

            $conductor_actualizar = Conductor::findOrFail($conductor);

            if ($conductor_actualizar) {

                $conductor_actualizar->update($datos);

                $empleado = Empleado::findOrFail($conductor_actualizar->empleado_id);

                $empleado->persona_emergencia = $request->persona_emergencia;
                $empleado->telefono_emergencia = $request->telefono_emergencia;
                $empleado->save();

                DB::commit();

                return response()->json(array('success' => true, 'estatus' => 'nuevo'));
            }
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            Conductor::destroy($request->conductor);

            DB::commit();

            return response()->json(array('success' => true, 'conductor' => $request->conductor));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function search(Request $request)
    {

        $conductor = Conductor::findOrFail($request->conductor_id);
        $persona = Persona::findOrFail($request->persona_id);
        $licencia = Licencias::findOrFail($conductor->licencia_id);

        $view = View::make('fotradis::driver.partials.create_edit_conductor')->with('persona', $persona)->with('conductor', $conductor);
        $html = $view->render();

        $html2 = '<button type="button" class="btn btn-primary pull-rigth" onclick="licencias.ver_licencia(' . $licencia->id . ')"><i class="fa fa-fw fa-eye"></i>Ver licencia</button>';

        return response()->json(['status' => 'ok', 'html' => $html, 'html2' => $html2, 'persona_id' => $persona->id, 'conductor_id' => $conductor->id], 200);
    }

    public function searchLicencia(Request $request)
    {
        $licencia = Licencias::findOrFail($request->licencia);
        $entidad = Entidad::findOrFail($licencia->entidad_id);

        $view = View::make('fotradis::driver.partials.modal_licencia')->with('licencia', $licencia)->with('entidad', $entidad->entidad);
        $html = $view->render();
        return response()->json(['status' => 'ok', 'html' => $html, 'licencia_id' => $licencia->id, 'entidad_id' => $entidad->id], 200);
    }

    public function empleadoSelect(Request $request)
    {
        $empleados = DB::table('empleados')
            ->select([
                'empleados.id as id',
                'empleados.rfc as rfc',
                DB::raw('UPPER(CONCAT(personas.nombre," ", personas.primer_apellido, " ", personas.segundo_apellido)) as nombre')
            ])
            ->join('personas', 'empleados.persona_id', '=', 'personas.id')
            ->leftJoin(
                DB::raw("
                (SELECT conductores.empleado_id as empleado_id FROM conductores INNER JOIN empleados ON conductores.empleado_id = empleados.id WHERE conductores.deleted_at IS NULL) AS nulos
            "),
                'empleados.id',
                '=',
                'nulos.empleado_id'
            )
            ->whereRaw('nulos.empleado_id is null')
            ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->get()
            ->toArray();
        return response()->json($empleados);
    }

    public function entidadSelect(Request $request)
    {
        $entidades = DB::table('cat_entidades')
            ->select([
                'cat_entidades.id',
                'cat_entidades.entidad'
            ])
            ->where('cat_entidades.entidad', 'LIKE', '%' . $request->input('search') . '%')
            ->take(10)
            ->get()
            ->toArray();

        return response()->json($entidades);
    }

    public function crearLicencia(Request $request)
    {
        try {
            DB::beginTransaction();

            $datos = $request->all();
            $licencia = Licencias::where(function ($query) use ($request) {
                return $query->where('numero_licencia', '=', $request['numero_licencia']);
            })
                ->first();

            if (!$licencia) {
                $licencia = Licencias::create($datos);

                DB::commit();

                return response()->json(array('success' => true, 'estatus' => 'nuevo', 'numero_licencia' => $licencia->numero_licencia));
            }

            return response()->json(array('success' => true, 'estatus' => 'existente', 'numero_licencia' => $licencia->numero_licencia));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function destroyLicencia(Request $request)
    {
        try {
            DB::beginTransaction();
            Licencias::destroy($request->licencia);

            DB::commit();

            return response()->json(array('success' => true, 'id' => $request->licencia));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function licenciaSelect(Request $request)
    {
        $licencias = DB::table('licencias')
            ->select([
                'licencias.id',
                'licencias.numero_licencia'
            ])
            ->leftJoin(
                DB::raw("
                (SELECT conductores.licencia_id as id FROM conductores INNER JOIN licencias ON conductores.licencia_id = licencias.id WHERE conductores.deleted_at IS NULL) AS nulos
            "),
                'licencias.id',
                '=',
                'nulos.empleado_id'
            )

            ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->whereRaw('conductores.deleted_at is null')
            ->whereRaw('(NOT EXISTS(SELECT empleados.id from empleados WHERE empleados.id = conductores.empleado_id))')
            ->where('licencias.numero_licencia', 'LIKE', '%' . $request->input('search') . '%')

            ->get()
            ->toArray();

        return response()->json($licencias);
    }
}