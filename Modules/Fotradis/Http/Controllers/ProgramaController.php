<?php

namespace Modules\Fotradis\Http\Controllers;

use App\Http\Controllers\ProgramaBaseController; 
use Illuminate\Routing\Controller; 

class ProgramaController extends ProgramaBaseController {
    function __construct() {
        parent::__construct('DIF TE LLEVA', 'fotradis');
    }
}