<?php

namespace Modules\Fotradis\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Conductor;

class DriverController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function list(Request $request){
      $conductores = Conductor::leftjoin('empleados','empleados.id','conductores.empleado_id')
      ->leftjoin('personas','personas.id','empleados.persona_id')
      ->leftjoin('controlrutas',function($query){
        $query->on('controlrutas.conductor_id','conductores.id')
        ->whereNull('controlrutas.deleted_at');
      })
      ->leftjoin('recorridos','recorridos.id','controlrutas.recorrido_id');
      if($request->filled('libre'))
        if($request->libre=='true')
          $conductores->whereNull('controlrutas.id');
        else if($request->libre=='false')
          $conductores->whereNotNull('controlrutas.id');
      return response()->json($conductores->get(['personas.nombre','primer_apellido','segundo_apellido','numero_celular','numero_local','persona_emergencia','telefono_emergencia','recorridos.nombre as ruta_actual','unidad_id as unidad_actual']));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id){
      $driver = Conductor::leftjoin('empleados','empleados.id','conductores.empleado_id')
      ->leftjoin('personas','personas.id','empleados.persona_id')
      ->where('clave_conductor',$id)->firstOrFail(['personas.nombre','personas.primer_apellido','personas.segundo_apellido']);
      return $driver? response()->json($driver) : null;
    }

}
