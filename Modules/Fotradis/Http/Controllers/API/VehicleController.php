<?php

namespace Modules\Fotradis\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;

use App\Models\Unidad;
use App\Models\ControlRutas;
use App\Models\DTLL\RegistroKm;
use App\Models\DTLL\RegistroGasolina;

class VehicleController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function list(Request $request){
      $unidades = Unidad::leftjoin('controlrutas',function($query){
        $query->on('controlrutas.unidad_id','unidades.id')
        ->whereNull('controlrutas.deleted_at');
      })
      ->leftjoin('recorridos','recorridos.id','controlrutas.recorrido_id')
      ->leftjoin('conductores','conductores.id','controlrutas.conductor_id')
      ->leftjoin('empleados','empleados.id','conductores.empleado_id')
      ->leftjoin('personas','personas.id','empleados.persona_id');
      if($request->filled('libre'))
        if($request->libre=='true')
          $unidades->whereNull('controlrutas.id');
        else if($request->libre=='false')
          $unidades->whereNotNull('controlrutas.id');
      if($request->filled('recorrido'))
        if($request->recorrido == 'null')
          $unidades->whereNull('recorridos.nombre');
        else
          $unidades->where('recorridos.nombre','like',"%$request->recorrido%");
      if($request->filled('conductor'))
        if($request->conductor=='null')
          $unidades->whereNull('personas.nombre');
        else
          $unidades->where(function($query) use ($request){
            $query->where('personas.nombre','like',"%$request->conductor%")
            ->orWhere('personas.primer_apellido','like',"%$request->conductor%")
            ->orWhere('personas.segundo_apellido','like',"%$request->conductor%");
          });
      return response()->json($unidades->get(['unidades.id','recorridos.nombre as recorrido','personas.nombre','personas.primer_apellido']));
    }

    public function status($unidad){
      $Unidad = Unidad::select(DB::raw(
      'numero_unidad,'.
      'modelo,'.
      'CONCAT(personas.nombre," ",primer_apellido," ",segundo_apellido) as conductor,'.
      'recorridos.nombre as recorrido,'.
      "(SELECT km from dtll_registrokm left join controlrutas on controlrutas.id = dtll_registrokm.controlruta_id where controlrutas.unidad_id = unidades.id and fecha >= date_format(now(),'%Y-%m-%d') order by fecha asc limit 1) as km_ini,".
      "(case when (SELECT count(km)  from dtll_registrokm left join controlrutas on controlrutas.id = dtll_registrokm.controlruta_id where controlrutas.unidad_id = unidades.id and fecha >= date_format(now(),'%Y-%m-%d') ) >= 2".
      "  then (SELECT km  from dtll_registrokm left join controlrutas on controlrutas.id = dtll_registrokm.controlruta_id where controlrutas.unidad_id = unidades.id and fecha >= date_format(now(),'%Y-%m-%d') order by fecha desc limit 1 ) ".
      '  else null'.
      '  end )as km_fin'
       ))->leftjoin('controlrutas',function($query){
        $query->on('controlrutas.unidad_id','unidades.id')
        ->whereNull('controlrutas.fechahora_sesion_fin');
      })
      ->leftjoin('recorridos','recorridos.id','controlrutas.recorrido_id')
      ->leftjoin('conductores','conductores.id','controlrutas.conductor_id')
      ->leftjoin('empleados','empleados.id','conductores.empleado_id')
      ->leftjoin('personas','personas.id','empleados.persona_id')
      ->where('numero_unidad',$unidad);
      return response()->json($Unidad->get());
    }

    public function addKm(Request $request){
      $unidad = ControlRutas::findorfail($request->input('session'));
      $ultimoKm = RegistroKm::leftjoin('controlrutas','controlrutas.id','dtll_registrokm.controlruta_id')
      ->where('unidad_id',$unidad->unidad_id)->orderBy('fecha','desc')->first(['km']);
      $ultimoKm = $ultimoKm? $ultimoKm->km : 0;
      try{
        DB::beginTransaction();
        if($request->input('km')>=$ultimoKm){
          RegistroKm::firstOrCreate(
            [
              'controlruta_id' => $request->input('session'),
              'km'             => $request->input('km'),
              'fecha'          => date("Y-m-d H:i:s"),
              'foto'           => $request->input('foto')
            ]
          );
          DB::commit();
          return response()->json(['result'=>true]);
        }else {
          abort(409,'km incorrecto');
        }
      }catch(Exception $e){
        abort(500, 'Error al registrar en la BD: ' . $e->getMessage());
      }
    }

    public function addGas(Request $request ){
      try{
        DB::beginTransaction();
          RegistroGasolina::firstOrCreate(
            [
              'controlruta_id' => $request->input('session'),
              'tipocarga_id'   => $request->input('tipocarga'),
              'litros'         => $request->input('litros'),
              'importe'        => $request->input('importe'),
              'folio'          => $request->input('folio'),
              'fecha'          => date("Y-m-d H:i:s"),
              'foto'           => $request->input('foto')
            ]
          );
        DB::commit();
        return response()->json(true);
      }catch(Exception $e){
        abort(500, 'Error al registrar en la BD: ' . $e->getMessage());
      }
    }

}
