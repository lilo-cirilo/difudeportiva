<?php

namespace Modules\Fotradis\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Recorridos;

class RouteController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request){
      return response()->json(Recorridos::all());
    }

    public function show($id){
      $route = Recorridos::findorfail($id);
      return $route? response()->json($route) : null;
    }

}
