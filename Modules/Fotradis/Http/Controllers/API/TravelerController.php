<?php

namespace Modules\Fotradis\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;

use App\Models\DTLL\BajaServicio;
use App\Models\DTLL\Pasajero;
use App\Models\DTLL\Servicio;
use App\Models\DTLL\Pagos;
use App\Models\ControlRutas;

class TravelerController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request){
      return response()->json(Pasajero::all());
    }

    public function show($id){
      $pasajero = Pasajero::leftjoin('personas','personas.id','dtll_pasajeros.persona_id')
      ->join('dtll_serviciosadquiridos','dtll_serviciosadquiridos.pasajero_id','dtll_pasajeros.id')
      ->join('dtll_cat_tipospasajero','dtll_cat_tipospasajero.id','dtll_serviciosadquiridos.tipopasajero_id')
      ->join('dtll_cat_tiposservicio','dtll_cat_tiposservicio.id','dtll_serviciosadquiridos.tiposervicio_id')
      ->leftjoin('dtll_pagos','dtll_pagos.servicioadquirido_id','dtll_serviciosadquiridos.id')
      ->where('dtll_serviciosadquiridos.numPasajero',$id)->latest('dtll_pagos.created_at')->first(['dtll_pasajeros.id','personas.nombre as nombre','primer_apellido','segundo_apellido','fotografia','dtll_serviciosadquiridos.numPasajero','dtll_cat_tipospasajero.nombre as tipo_pasajero','dtll_cat_tiposservicio.nombre as tipo_servicio','dtll_pagos.fechaFinServ']);
      return $pasajero? response()->json($pasajero) : null;
    }

    public function add(Request $request){
      $pasajero = Pasajero::findorfail($request->input('pasajero'));
      if(ControlRutas::findOrFail($request->input('session'))->recorrido->nombre == 'TAXI'){
        foreach ($pasajero->serviciosadquiridos as $servicio ) {
          if($servicio->tiposervicio->nombre == 'TAXI'){
            $pago=Pagos::where('servicioadquirido_id',$servicio->id)->orderBy('fechaFinServ','desc')->first();
            if(strtotime($pago->fechaFinServ) < strtotime(date("Y-m-d")))
              abort(403,'servicio caducado en '.$pago->fechaFinServ);
          }
        }
      }

      $ocupado = Pasajero::leftjoin('dtll_servicios','dtll_servicios.pasajero_id','dtll_pasajeros.id')
      ->leftjoin('dtll_serviciosbajadas','dtll_serviciosbajadas.servicio_id','dtll_servicios.id')
      ->where('dtll_pasajeros.id', $request->input('pasajero'))->whereNotNull('subida')->whereNull('bajada')
      ->first(['dtll_servicios.controlruta_id as controlruta_id','dtll_servicios.id as servicio_id']);

      if($ocupado) {
        if($request->input('session') == $ocupado->controlruta_id)
          return response()->json(['result'=>true,'ocupado' => $ocupado->id]);
        else {
          $no_cerro_sesion = ControlRutas::whereNull('fechahora_sesion_fin')
                                      ->where('id',$ocupado->controlruta_id)
                                      ->first();
          if($no_cerro_sesion)
            abort(409,'Beneficiario en otro viaje');
        }
      }
        
      try{
        DB::beginTransaction();
          if($ocupado) {
            BajaServicio::create([
              'servicio_id' => $ocupado->servicio_id,
              'bajada'      => date("Y-m-d H:i:s"),
              'latitud'     => null,
              'longitud'    => null
            ]);
          }

          $subida = Servicio::firstOrCreate(
            [
              'pasajero_id'    => $request->input('pasajero'),
              'controlruta_id' => $request->input('session'),
              'latitud'        => $request->input('latitud'),
              'longitud'       => $request->input('longitud'),
              'subida'         => date("Y-m-d H:i:s")
            ]
          );
        DB::commit();
        return response()->json(['result'=>true,'id'=>$subida->id]);
      }catch(Exception $e){
        return abort(500,'Error al registrar en la BD: ' . $e->getMessage());
      }
    }

    public function del(Request $request){
      $subida = Servicio::leftjoin('dtll_serviciosbajadas','dtll_serviciosbajadas.servicio_id','dtll_servicios.id')
      ->where('controlruta_id',$request->input('session'))
      ->where('pasajero_id',$request->input('pasajero'))
      ->firstOrFail(['dtll_servicios.id'])->id;
      
      try{
        DB::beginTransaction();
          $bajada = BajaServicio::where('servicio_id',$subida)->count();
          if($bajada >= 1)
            return response()->json(['result'=>'conflict','code'=>409,'message'=>'bajada ya registrada']);
          $se_bajo = BajaServicio::create([
            'servicio_id' => $subida,
            'bajada'      => date("Y-m-d H:i:s"),
            'latitud'     => $request->input('latitud'),
            'longitud'    => $request->input('longitud')
          ]);
        DB::commit();
        return response()->json(['result'=>true]);
      }catch(Exception $e){
        return abort(500,'Error al registrar en la BD: ' . $e->getMessage());
      }
    }
}
