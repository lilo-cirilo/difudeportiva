<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Support\Facades\View;

use App\Models\ParadasRutas;
use App\Models\Paradas;
use App\Models\Rutas;
use App\DataTables\Fotradis\RutasActuales;
use App\DataTables\Fotradis\ParadasActuales;
use App\Models\ControlRutas;

class RoutesController extends Controller
{

    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index(RutasActuales $datatables)
    {
        return $datatables->render('fotradis::routes.index');
    }

    public function create(ParadasActuales $dataTables, Request $request)
    {
        $ruta = Rutas::findOrFail($request->ruta);
        $controlrutas = Rutas::join("recorridos", "recorridos.id", "rutas.recorrido_id")
            ->join("controlrutas", "controlrutas.recorrido_id", "recorridos.id")
            ->where("rutas.id", "=", $request->ruta)
            ->whereNull("controlrutas.deleted_at")->count();

        $paradas = ParadasRutas::join("paradas", "paradas.id", "=", "paradas_rutas.parada_id")
            ->where("paradas_rutas.ruta_id", "=", $request->ruta)
            ->where("paradas_rutas.tipoparada", "!=", "F")
            ->orderBy('paradas_rutas.numero_estacion')
            ->get([
                'paradas.id as parada_id',
                'paradas.id as ruta_id',
                'paradas.latitud as latitud',
                'paradas.longitud as longitud',
                'paradas.nombre as parada',
                'paradas.referencia as referencia',
                'paradas_rutas.numero_estacion',
                'paradas_rutas.tipo',
                'paradas_rutas.icono',
                'paradas_rutas.tipoparada'
            ]);

        if ($controlrutas > 0) {
            return $dataTables
                ->with('paradas', $paradas)
                ->render('fotradis::routes.create', ['ruta' => $ruta, 'en_asignacion' => $controlrutas, 'paradas' => $paradas]);
        }
        return $dataTables
            ->with('paradas', $paradas)
            ->render('fotradis::routes.create', ['ruta' => $ruta, 'paradas' => $paradas]);
    }

    public function store(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $datos = $request->except(['ruta_id', 'accion']);

                if ($request->accion == 'add') {
                    $ruta = Rutas::create($datos);
                } else if ($request->accion == 'edit') {
                    $ruta = Rutas::findOrFail($request->ruta_id);
                    $ruta->update($datos);
                }

                DB::commit();

                return response()->json(array('success' => true, 'estatus' => true, 'ruta' => $ruta->id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function show(Request $request)
    {
        $ruta = Rutas::findOrFail($request->ruta);
        $paradas = ParadasRutas::join("paradas", "paradas.id", "=", "paradas_rutas.parada_id")
            ->where("paradas_rutas.ruta_id", "=", $request->ruta)->orderBy('paradas_rutas.numero_estacion')->get(['paradas.id as parada_id', 'paradas.id as ruta_id', 'paradas.latitud as latitud', 'paradas.longitud as longitud', 'paradas.nombre as parada', 'paradas.referencia as referencia', 'paradas_rutas.numero_estacion', 'paradas_rutas.tipo', 'paradas_rutas.icono', 'paradas_rutas.tipoparada']);

        if ($request->accion == "render") {
            $view = View::make('fotradis::routes.partials.visualizar_ruta')->with('paradas', $paradas)->with('ruta', $ruta)->with('accion', 'render');
            $html = $view->render();

            return response()->json(['status' => 'ok', 'html' => $html, 200]);
        }
    }

    public function edit(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $datos_paradas = $request->except(['ruta_id', 'parada_id']);

                $parada_create = '';
                $parada_update = '';
                $paradarutas_create = '';

                if ($request->parada_id == '')
                    $parada_create = Paradas::create($datos_paradas);
                else {
                    $parada_update = Paradas::findOrFail($request->parada_id);
                    $parada_update->update($datos_paradas);
                }

                if ($parada_create) {
                    $paradarutas_create = new ParadasRutas;
                    $paradarutas_create->ruta_id = $request->ruta_id;
                    $paradarutas_create->parada_id = $parada_create->id;
                    $paradarutas_create->tipo = $request->tipo;
                    $paradarutas_create->numero_estacion = $request->numero_estacion;
                    $paradarutas_create->save();
                } else if ($parada_update) {
                    $paradarutas_create = ParadasRutas::where("ruta_id", "=", $request->ruta_id)->where("parada_id", "=", $request->parada_id)->first();
                    $paradarutas_create->tipo = $request->tipo;
                    $paradarutas_create->numero_estacion = $request->numero_estacion;
                    $paradarutas_create->save();
                }

                DB::commit();
                return response()->json(array('success' => true, 'estatus' => 'insertado', 'parada_creada' => $parada_create, 'parada_actualizada' => $parada_update, 'paradas_rutas' => $paradarutas_create));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function show_parada(Request $request)
    {
        $parada = ParadasRutas::join("paradas", "paradas.id", "=", "paradas_rutas.parada_id")
            ->where("paradas_rutas.ruta_id", "=", $request->ruta_id)->where("paradas_rutas.parada_id", "=", $request->parada_id)->get(['paradas.id as parada_id', 'paradas.id as ruta_id', 'paradas.latitud as latitud', 'paradas.longitud as longitud', 'paradas.nombre as parada', 'paradas.referencia as referencia', 'paradas_rutas.numero_estacion as numero_estacion']);

        return response()->json(['status' => 'ok', 'parada' => $parada, 200]);
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();

                $paradasrutas_destroy = ParadasRutas::where('ruta_id', '=', $request->ruta_id)->where('parada_id', '=', $request->parada_id)->get();

                foreach ($paradasrutas_destroy as $registro) {
                    $registro->delete();
                }


                Paradas::destroy($request->parada_id);

                DB::commit();

                return response()->json(array('success' => true, 'ruta_id' => $request->ruta_id, 'parada_id' => $request->parada_id));
            } catch (Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function destroy(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();

                $paradasrutas_destroy = ParadasRutas::where('ruta_id', '=', $request->ruta)->get();

                foreach ($paradasrutas_destroy as $registro) {
                    $registro->delete();
                }


                Rutas::destroy($request->ruta);

                DB::commit();

                return response()->json(array('success' => true, 'ruta_id' => $request->ruta_id));
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function savePolyline(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();
                $ruta = Rutas::where('id', '=', $request->ruta_id);
                $ruta->update(['polyline' => $request->line]);
                DB::commit();
                return response()->json(['status' => 'ok', 200]);
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function IncompleteRoutes()
    {
        $controlrutas = ControlRutas::where('fechahora_sesion_fin', null)->get();

        return view('fotradis::IncompleteRoutes.index', compact('controlrutas'));
    }
}