<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\View;

//use App\DataTables\Fotradis\RutasDelDia;
use App\DataTables\Fotradis\Sesiones;
use App\DataTables\Fotradis\HistorialConductoresAsignados;
use App\Models\Rutas;
use App\Models\ParadasRutas;
use App\Models\Unidad;
use App\Models\Conductor;
use App\Models\Recorridos;
use App\Models\ControlRutas;

use App\Models\DTLL\Pasajero;
use App\Models\DTLL\Servicio;

class FotradisController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }


    public function index(Sesiones $datatable)
    {
        $paradas = ParadasRutas::join("paradas", "paradas.id", "=", "paradas_rutas.parada_id")
            ->get(['paradas_rutas.ruta_id as ruta', 'paradas.id as parada_id', 'paradas.id as ruta_id', 'paradas.latitud as latitud', 'paradas.longitud as longitud', 'paradas.nombre as parada', 'paradas.referencia as referencia', 'paradas_rutas.numero_estacion', 'paradas_rutas.tipo', 'paradas_rutas.icono', 'paradas_rutas.tipoparada']);
        $rutas = Rutas::orderBy('id', 'DESC')->get();
        return $datatable->render('fotradis::indexV2', ['paradas' => $paradas, 'rutas' => $rutas]);
    }

    public function show(Request $request)
    {
        if ($request->ajax()) {
            $rutas = Unidad::findOrFail($request->fotradi);
            return response()->json($rutas);
        }
    }

    public function stats()
    {
        $stats = [];
        $stats['benAct'] = count(Pasajero::join('dtll_servicios', 'dtll_servicios.pasajero_id', 'dtll_pasajeros.id')->where('subida', '>=', date('Y-m') . '-01')->groupBy('dtll_servicios.pasajero_id')->get(['dtll_servicios.pasajero_id']));
        $stats['hoy'] = Pasajero::leftjoin('dtll_servicios', 'dtll_servicios.pasajero_id', 'dtll_pasajeros.id')->where('subida', '>=', date('Y-m-d'))->count();
        $stats['mes'] = Pasajero::leftjoin('dtll_servicios', 'dtll_servicios.pasajero_id', 'dtll_pasajeros.id')->where('subida', '>=', date('Y-m') . '-01')->count();
        $stats['ruta'] =  Recorridos::select('recorridos.nombre', DB::raw('(SELECT count( subida ) FROM dtll_servicios left JOIN controlrutas ON controlrutas.id = dtll_servicios.controlruta_id left JOIN recorridos r ON r.id = controlrutas.recorrido_id WHERE r.id = recorridos.id ) as conta'))
            ->where('recorridos.id', '!=', 100)->orderby('conta', 'desc')->first()->nombre;
        return response()->json($stats);
    }

    public function historial(HistorialConductoresAsignados $datatables)
    {
        return $datatables->render('fotradis::edit');
    }

    public function select_rutas(Request $request)
    {
        if ($request->ajax()) {
            $rutas = DB::table('recorridos')
                ->select([
                    'recorridos.id as id',
                    'recorridos.nombre as nombre'
                ])

                ->where('recorridos.nombre', 'LIKE', '%' . $request->input('search') . '%')
                ->whereRaw('recorridos.deleted_at is null')
                ->take(10)
                ->get()
                ->toArray();
            return response()->json($rutas);
        }
    }
    public function select_unidades(Request $request)
    {
        if ($request->ajax()) {
            if ($request->todas) {
                $unidades = DB::table('unidades')
                    ->select([
                        'unidades.id as id',
                        'unidades.numero_unidad as numero_unidad',
                        'unidades.tipo_servicio as tipo_servicio'
                    ])
                    ->where('unidades.numero_unidad', 'LIKE', '%' . $request->input('search') . '%')
                    ->whereRaw('unidades.deleted_at is null')
                    ->take(10)
                    ->get()
                    ->toArray();
                return response()->json($unidades);
            }
            $unidades = DB::table('unidades')
                ->select([
                    'unidades.id as id',
                    'unidades.numero_unidad as numero_unidad',
                    'unidades.tipo_servicio as tipo_servicio'
                ])

                ->leftJoin(
                    DB::raw("
                    (SELECT unidades.id as id FROM unidades INNER JOIN controlrutas ON unidades.id = controlrutas.unidad_id WHERE controlrutas.deleted_at IS NULL) AS nulos
                "),
                    'unidades.id',
                    '=',
                    'nulos.id'
                )
                ->whereRaw('nulos.id is null')
                ->whereRaw('unidades.deleted_at is null')
                ->where('unidades.numero_unidad', 'LIKE', '%' . $request->input('search') . '%')
                ->take(10)
                ->get()
                ->toArray();
            return response()->json($unidades);
        }
    }
    public function select_conductores(Request $request)
    {
        if ($request->ajax()) {
            $conductores = DB::table('personas')
                ->select([
                    'conductores.id as id',
                    DB::raw('UPPER(CONCAT(personas.nombre," ", personas.primer_apellido, " ", personas.segundo_apellido)) as nombre')
                ])
                ->join('empleados', 'empleados.persona_id', '=', 'personas.id')
                ->join('conductores', 'conductores.empleado_id', '=', 'empleados.id')
                ->leftJoin(
                    DB::raw("
                    (SELECT conductores.id as id FROM conductores INNER JOIN controlrutas ON conductores.id = controlrutas.conductor_id WHERE controlrutas.deleted_at IS NULL) AS nulos
                "),
                    'conductores.id',
                    '=',
                    'nulos.id'
                )
                ->whereRaw('nulos.id is null')
                ->whereRaw('conductores.deleted_at is null')
                ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
                ->take(10)
                ->get()
                ->toArray();
            return response()->json($conductores);
        }
    }

    public function saveRegistroPasajero(Request $request)
    {
        if ($request->ajax()) {
            $pasajero = DB::table('dtll_regservspasajeros')
                ->select('*')
                ->where('clave', $request->input('clave'))
                ->where('timestamp', $request->input('timestamp'))
                ->where('conductor', $request->input('conductor'))
                ->get();
            if ($pasajero != null) {
                try {
                    DB::beginTransaction();
                    $registro_pasajero = RegistroPasajero::create($request);
                    DB::commit();
                    return response()->json(array('success' => true, 'message' => 'Registro con exito', 'existe' => false));
                } catch (Exeption $e) {
                    DB::rollBack();
                    return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
                }
            } else {
                return response()->json(array('success' => true, 'message' => 'El registro ya existe.', 'existe' => true));
            }
        }
    }

    public function getAsignacion(Request $request)
    {
        if ($request->ajax()) {
            try {
                DB::beginTransaction();

                $asignacion = DB::table('personas')
                    ->select([
                        DB::raw('UPPER(CONCAT(personas.nombre," ", personas.primer_apellido, " ", personas.segundo_apellido)) as conductor'),
                        'controlrutas.id as asignacionKey',
                        'controlrutas.unidad_id as unidad_id',
                        'controlrutas.conductor_id as conductor_id',
                        'controlrutas.recorrido_id as recorrido_id',
                        'unidades.numero_unidad as unidad',
                        'unidades.tipo_servicio as servicio',
                        'recorridos.nombre as recorrido'
                    ])
                    ->join('empleados', 'empleados.persona_id', '=', 'personas.id')
                    ->join('conductores', 'conductores.empleado_id', '=', 'empleados.id')
                    ->join('controlrutas', 'controlrutas.conductor_id', 'conductores.id')
                    ->join('unidades', 'unidades.id', 'controlrutas.unidad_id')
                    ->join('recorridos', 'controlrutas.recorrido_id', 'recorridos.id')
                    ->where('unidades.numero_unidad', '=', $request->numero_unidad)
                    ->whereRaw('controlrutas.deleted_at is null')
                    ->first();
                DB::commit();

                return response()->json(array('success' => true, 'val' => $asignacion));
            } catch (Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function getConductorRuta(Request $request)
    {
        if ($request->ajax()) {
            $conductor_ruta = DB::table('controlrutas')
                ->select([
                    'recorridos.nombre as ruta',
                    DB::raw('CONCAT( "C", conductores.id ) AS conductor')
                ])
                ->join('recorridos', 'recorridos.id', '=', 'controlrutas.recorrido_id')
                ->join('conductores', 'conductores.id', '=', 'controlrutas.conductor_id')
                ->join('unidades', 'unidades.id', '=', 'controlrutas.unidad_id')
                ->whereRaw('controlrutas.deleted_at IS NULL')
                ->where('unidades.tipo_servicio', 'urban')
                ->get()->toArray();

            return response()->json($conductor_ruta);
        }
    }

    public function pasajerosABordo(Request $request)
    {
        $a_bordo = Unidad::join('controlrutas', 'controlrutas.unidad_id', 'unidades.id')
            ->join('dtll_servicios', 'dtll_servicios.controlruta_id', 'controlrutas.id')
            ->join('dtll_pasajeros', 'dtll_pasajeros.id', 'dtll_servicios.pasajero_id')
            ->join('personas', 'personas.id', 'dtll_pasajeros.persona_id')
            ->whereNull('fechahora_sesion_fin')
            ->where('unidades.numero_unidad', $request->unidad)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('dtll_serviciosbajadas')
                    ->whereRaw('dtll_servicios.id = dtll_serviciosbajadas.servicio_id');
            });

        if ($a_bordo->count() < 1)
            return response()->json(array('success' => false, 'unidad' => $request->unidad));
        return response()->json(array(
            'success' => true, 'unidad' => $request->unidad,
            'abordo' => $a_bordo->get([
                'personas.nombre as nombre',
                'personas.primer_apellido as pa', 'personas.segundo_apellido as sa',
                'dtll_pasajeros.telefono_emergencias as nump'
            ])
        ));
    }
}