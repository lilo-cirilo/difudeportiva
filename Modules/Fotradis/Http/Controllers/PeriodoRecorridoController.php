<?php

namespace Modules\Fotradis\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query;
use Illuminate\Database\QueryException;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\View;

use App\DataTables\Fotradis\PeriodoServicios;
use App\Models\Recorridos;

class PeriodoRecorridoController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModuleV2:fotradis,ADMINISTRADOR', ['except' => ['show', 'index']]);
        $this->middleware('rolModuleV2:fotradis,MONITOR', ['only' => ['show', 'index']]);
    }

    public function index(PeriodoServicios $datatable)
    {
        return $datatable->render('fotradis::travels.index');
    }

    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->in == "true") {
                Recorridos::destroy($request->periodorecorrido);
            } else {
                $recorrido = Recorridos::where('id', $request->periodorecorrido)->withTrashed()->first();
                $recorrido->deleted_at = null;
                $recorrido->save();
            }

            DB::commit();

            return response()->json(array('success' => true, 200));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $request['nombre'] = strtoupper($request['nombre']);
            Recorridos::firstOrCreate($request->all());
            DB::commit();
            return response()->json(array('success' => true, 200));
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }
}