<?php

Route::group(['middleware' => ['web'], 'prefix' => 'fotradis', 'namespace' => 'Modules\Fotradis\Http\Controllers'], function () {
  // *************************************************************************************************************************
  // ------------------------------------------- PARA EL REGISTRO DE BENEFICIARIOS -------------------------------------------
  // *************************************************************************************************************************
  //Route::resource('registrobenef', 'RegistroBenefController', ['except' => ['update','delete','edit','show','store','create']]);
  Route::get('/registrobenef', 'RegistroBenefController@index')->name('registrobenef.index');                                                    //Muestra el listado de pasajeros.
  Route::get('/registrobenef/getaddpasajero', 'RegistroBenefController@showFrm_AddPasajero')->name('registrobenef.getaddpasajero');              //Obtiene los datos para el formulario Agregar pasajero.
  Route::get('/registrobenef/geteditpasajero/{id}', 'RegistroBenefController@showMdl_editPasajero')->name('registrobenef.geteditpasajero');      //Obtiene los datos para el formulario Editar pasajero.    Para mandar a llamar por nombre se hace: 'registrobenef.geteditpasajero[id]'
  Route::get('/registrobenef/getwatchpasajero/{id}', 'RegistroBenefController@showMdl_watchPasajero')->name('registrobenef.getwatchpasajero');   //Obtiene los datos del pasajero para el formulario en modo lectura.
  Route::post('/registrobenef/setstorepasajero', 'RegistroBenefController@storePasajero')->name('registrobenef.setstorepasajero');               //Guarda los datos del formulario Agregar pasajero.
  Route::post('/registrobenef/setupdatepasajero/{id}', 'RegistroBenefController@updatePasajero')->name('registrobenef.setupdatepasajero');        //Actualiza los datos del formulario Editar pasajero.
  Route::get('/registrobenef/getdeleteservicio/{id}', 'RegistroBenefController@showMdl_deleteServicio')->name('registrobenef.getdeleteservicio'); //Obtiene los datos para borrar un servicio de pasajero.
  Route::post('/registrobenef/setdeleteservicio', 'RegistroBenefController@deleteServicio')->name('registrobenef.setdeleteservicio');            //Obtiene los datos para borrar un servicio de pasajero.

  Route::get('/registrobenef/getpagarservicio/{id}', 'RegistroBenefController@showMdl_pagarServicio')->name('registrobenef.getpagarservicio');   //Obtiene los datos para el formulario Pagar servicio.
  Route::post('/registrobenef/setstorepagoserv', 'RegistroBenefController@storePagoServicio')->name('registrobenef.setstorepagoserv');           //Guarda los datos del formulario Pagar servicio.

  Route::get('/registrobenef/tiposservicio', 'RegistroBenefController@getTiposServicio')->name('registrobenef.getTiposServicio');                //Obtiene datos para el select Tipos de servicio
  Route::get('/registrobenef/tipospasajero', 'RegistroBenefController@getTiposPasajero')->name('registrobenef.getTiposPasajero');                //Obtiene datos para el select Tipos de pasajero
  Route::get('/registrobenef/paises', 'RegistroBenefController@getPaises')->name('registrobenef.getPaises');                                     //Obtiene datos para el select Países
  Route::get('/registrobenef/entidades', 'RegistroBenefController@getEntidades')->name('registrobenef.getEntidades');                            //Obtiene datos para el select entidades
  Route::get('/registrobenef/municipios', 'RegistroBenefController@getMunicipios')->name('registrobenef.getMunicipios');                         //Obtiene datos para el select Municipios
  Route::get('/registrobenef/lugarpago', 'RegistroBenefController@getLugaresDePago')->name('registrobenef.getLugaresPago');                      //Obtiene datos para el select Lugares de pago
  Route::get('/registrobenef/etnias', 'RegistroBenefController@getEtnias')->name('registrobenef.getEtnias');                      //Obtiene datos para el select Lugares de pago

  Route::put('/registrobenef/updatefoto/{persona_id}', 'RegistroBenefController@updateFoto')->name('registrobenef.updateFoto');

  Route::get('/registrobenef/draftrepinscritos', 'RegistroBenefController@showMdl_ReporteInscritos')->name('registrobenef.draftrepinscritos');
  Route::get('/registrobenef/repinscritos', 'RegistroBenefController@ReporteInscritos')->name('registrobenef.repinscritos');
  // -------------------------------------------------------------------------------------------------------------------------
  Route::resource('peticiones', 'PeticionController', ['as' => 'fotradis', 'only' => ['index', 'show', 'update', 'destroy']]);                            //Peticiones
  Route::resource('programas', 'ProgramaController', ['as' => 'fotradis']);                                                                         //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'fotradis']);                   //Beneficiarios
  // *************************************************************************************************************************
  // -------------------------------------------------------------------------------------------------------------------------
  // *************************************************************************************************************************

  Route::get('/', 'FotradisController@index')->name('fotradis.index');
  Route::get('select/controlrutas', 'FotradisController@select_rutas')->name('controlrutas.select');
  Route::get('select/controlunidades', 'FotradisController@select_unidades')->name('controlunidades.select');
  Route::get('select/controlconductores', 'FotradisController@select_conductores')->name('controlconductores.select');
  Route::get('/historial', 'FotradisController@historial')->name('fotradis.historial');
  Route::get('/pasajeros/{unidad}', 'FotradisController@pasajerosABordo')->name('fotradis.abordo');
  Route::resource('controlrutas', 'FotradisController', ['except' => ['index', 'historial']]);
  Route::resource('fotradis', 'FotradisController', ['except' => ['index', 'historial']]);
  //Conductor
  Route::resource('conductor', 'DriverController', ['except' => ['update']]);
  Route::post('conductor/{conductor}', 'DriverController@update')->name('conductor.update');
  Route::delete('licencia/{licencia}', 'DriverController@destroyLicencia');
  Route::get('licencia/search/{licencia}', 'DriverController@searchLicencia');
  Route::get('conductor/search/{persona_id}/{conductor_id}', 'DriverController@search');
  Route::get('/licencias', 'DriverController@licenciaSelect')->name('licencias.fotradis.select');
  Route::get('/empleados/conductores', 'DriverController@empleadoSelect')->name('empleados.fotradis.select');
  Route::get('entidades', 'DriverController@entidadSelect')->name('entidades.select');
  Route::post('/crear/licencia', 'DriverController@crearLicencia')->name('fotradis.crear.licencia');
  //Unidad
  Route::resource('unidad', 'UnityController', ['except' => ['update']]);
  Route::post('unidad/{unidad}', 'UnityController@update')->name('unidad.update');
  //Tarjeta circulación
  Route::resource('tarjetacirculacion', 'TarjetaCirculacionController');
  //Rutas
  Route::resource('rutas', 'RoutesController', ['except' => ['edit', 'update']]);
  Route::post('parada/store', 'RoutesController@edit')->name('parada.store');
  Route::post('save/polyline', 'RoutesController@savePolyline')->name('parada.updatePolyline');
  Route::get('show/parada/{parada_id}/{ruta_id}', 'RoutesController@show_parada');
  Route::delete('ruta/parada/{ruta_id}/{parada_id}', 'RoutesController@update');
  Route::resource('bitacora', 'BitacoraController');
  Route::get('/conductor/ruta/asignacion', 'FotradisController@getConductorRuta')->name('conductor.ruta.asignacion');
  //Registro Pasajeros
  Route::post('pasajeros', 'FotradisController@saveRegistroPasajero')->name('pasajero.save');
  //Obtener asignación con número de unidad
  Route::get('asignacion/{numero_unidad}', 'FotradisController@getAsignacion')->name('asignacion.get');
  Route::get('mantenimiento/{unidad_id}', 'UnityController@listaMantenimiento');
  Route::post('mantenimiento', 'UnityController@agregarMantenimiento');
  //Estadisticas del home
  Route::get('/estadisticas', 'FotradisController@stats')->name('fotradis.stats');
  //Detalles de sesion
  Route::get('/sesion/{id}', 'FotradisController@Details');

  Route::resource('periodorecorridos', 'PeriodoRecorridoController');

  Route::post('/info', 'InfoAppController@servicios');
  Route::post('/bitacora', 'InfoAppController@bitacora');
  Route::post('/mantenimiento', 'InfoAppController@mantenimiento');
  Route::post('/heatmapdata', 'InfoAppController@coordenadas');



  //DTLL index de peticiones incompletas
  Route::get('/routes/incompletes', 'RoutesController@IncompleteRoutes')->name('rutas.incompletas');
});

Route::post('/dtll/api/takeAccess', 'Modules\Fotradis\Http\Controllers\API\ApiController@login'); //valida clave de conductor, da acceso al api | test = ok

Route::group(['middleware' => 'api', 'prefix' => 'dtll/api', 'namespace' => 'Modules\Fotradis\Http\Controllers\API'], function () {
  Route::get('/routes', 'RouteController@index');            //Regresa los recorridos disponibles                                      | test = ok
  Route::get('/route/{id}', 'RouteController@show');         //Regresa los datalles de un unico recorrido                              | test = ok
  Route::get('/traveler/{id}', 'TravelerController@show');   //valida clave de pasajero, regresa los datos o null                      | test = ok
  Route::get('/vans', 'VehicleController@list');             //obtener unidades que no se encuentren asignadas en el dia(filtrando)    | test = ok
  Route::get('/van/{id}', 'VehicleController@status');       //saber si se ha registrado km inicial, final o carga de gasolina         | test = ok
  Route::get('/drivers', 'DriverController@list');           //obtener conductores que no se encuentren asignados en el dia(filtrando) | test = ok
  Route::get('/driver/{id}', 'DriverController@show');
  Route::post('/recorder/km', 'VehicleController@addKm');    //registrar km inicial o final                                            | test = ok
  Route::post('/recorder/gas', 'VehicleController@addGas');  //registrar cargas de gasolina                                            | test = ok
  Route::post('/session', 'ApiController@appLogIn');         //regitstrar asignaciones                                                 | test = ok
  Route::delete('/session/{id}', 'ApiController@appLogOut'); //removerasignaciones                                                     | test = ok
  Route::post('/traveler/add', 'TravelerController@add');    //registrar el abordaje de un beneficiario                                | test = ok
  Route::post('/traveler/remove', 'TravelerController@del'); //reistrar que bajo un pasajero                                           | test = ok
});