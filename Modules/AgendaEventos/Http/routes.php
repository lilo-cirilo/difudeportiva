<?php

Route::group(['middleware' => 'web', 'prefix' => 'agenda', 'namespace' => 'Modules\AgendaEventos\Http\Controllers'], function() {
		Route::get('/', 'AgendaEventosController@index')->name('agenda.index');

		Route::get('excelpeticion', 'AgendaEventosController@excelPeticion')->name('agenda.excelPeticion');
    Route::get('excelevento', 'AgendaEventosController@excelEvento')->name('agenda.excelEvento');
    Route::get('search', 'AgendaEventosController@search')->name('agenda.search');
    Route::get('/municipios/{id}/inversiones', 'AgendaEventosController@inversiones')->name('agenda.inversiones');
    Route::resource('calendarios', 'TipoEventoController', ['except' => ['show'], 'as' => 'agenda' ]);
    Route::get('calendarios/search', 'TipoEventoController@search')->name('agenda.calendarios.search');
    Route::resource('productos', 'EventoController', ['as' => 'agenda' ]);
    Route::resource('vestimenta', 'VestimentaController', ['as' => 'agenda' ]);
    Route::resource('eventos', 'EventoController', ['as' => 'agenda' ]);
    Route::resource('eventos.areas', 'EventoAreaController', ['as' => 'agenda' ]);
    Route::resource('fichas', 'FichaController', ['as' => 'agenda' ]);
    Route::resource('asistentes', 'AsistenteController', ['as' => 'agenda' ]);
    Route::resource('aportaciones', 'AportacionController', ['as' => 'agenda' ]);
    Route::resource('actividades', 'ActividadController', ['as' => 'agenda' ]);
    Route::resource('responsables', 'ResponsableController', ['as' => 'agenda' ]);
		Route::resource('autoridades', 'AutoridadController', ['as' => 'agenda' ]);

		Route::get('peticiones/localidades', 'SqlPeticionController@getLocalidades')->name('agenda.peticiones.localidades');

    Route::get('peticiones', 'SqlPeticionController@index')->name('agenda.peticiones');

    //Route::resource('peticiones', 'SqlPeticionController', ['as' => 'agenda.peticiones']);

    Route::resource('personas', 'PersonaController', ['as' => 'agenda']);

    Route::post('cargarpresidentes', 'AutoridadMunicipioController@cargar')->name('agenda.cargarpresidentes');
    Route::post('autoridadesmunicipios/create_edit', 'AutoridadMunicipioController@create_edit')->name('agenda.create_edit');
    Route::resource('autoridadesmunicipios', 'AutoridadMunicipioController', ['as' => 'agenda']);

    Route::post('cargardiputados', 'AutoridadDiputadoController@cargar')->name('agenda.cargardiputados');
    Route::resource('diputados', 'AutoridadDiputadoController', ['as' => 'agenda']);

    Route::get('participaciones/select', 'ParticipacionController@select')->name('agenda.participaciones.select');

    Route::resource('api/v1/personas', 'Api\v1\PersonaController', ['as' => 'api.agenda']);
    Route::resource('api/v1/calendariospersonas', 'Api\v1\CalendarioPersonaController', ['as' => 'api.calendarios.personas']);
    Route::resource('api/v1/eventospersonas', 'Api\v1\EventoPersonaController', ['as' => 'api.eventos.personas']);
});
