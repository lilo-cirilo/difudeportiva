<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Modules\AgendaEventos\Http\Controllers\BaseController;

use Modules\AgendaEventos\DataTables\PersonasDataTable;

class PersonaController extends BaseController {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        $dataTable = new PersonasDataTable;
        return $dataTable
        ->render('agendaeventos::directorio.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}