<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Actividad;

class ActividadController extends Controller {

    public function index(Request $request) {
		if($request->has('fichainfo_id')){
			return Actividad::actividades()->where('even_actividades.fichainfo_id', $request['fichainfo_id'])->get();
		}elseif($request->has('actividad_id')){
			return Actividad::responsables()->where('even_actividades.id', $request['actividad_id'])->get();
		}
    }

    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
            if(Actividad::where('fichainfo_id', $request['fichainfo_id'])->where('hora', $request['hora'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra registrada una actividad en la misma hora.'));
			}

            $actividad = Actividad::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'actividad' => $actividad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function show() {
        return view('agendaeventos::show');
	}
	
    public function update($actividad_id, Request $request) {
		$actividad = Actividad::findOrFail($actividad_id);

		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
            $actividad->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'actividad' => $actividad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function destroy($id) {
		$actividad = Actividad::find($id);
        if(!$actividad) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $actividad->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'actividad eliminada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
