<?php

namespace Modules\AgendaEventos\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use Modules\AgendaEventos\Entities\EventoPersona;

use Yajra\Datatables\Datatables;

class EventoPersonaController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $evento_id = $request->evento_id;

        if($evento_id === 'undefined') {
            $evento_id = 0;
        }
        
        $eventoPersona = DB::select(DB::raw("SELECT
            eep.id id,
        
        IF (
            eep.persona_id IS NULL,
            eep.nombre,
            p.nombre
        ) nombre,
        
        IF (
            eep.persona_id IS NULL,
            eep.primer_apellido,
            p.primer_apellido
        ) primer_apellido,
        
        IF (
            eep.persona_id IS NULL,
            eep.segundo_apellido,
            p.segundo_apellido
        ) segundo_apellido,
        eep.correo correo
        FROM
            even_eventos_personas eep
        LEFT JOIN personas p ON eep.persona_id = p.id WHERE eep.evento_id = $evento_id"));

        return Datatables::of($eventoPersona)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            $array = $request->all();

            for($i = 0; $i < count($array); $i++) {
                $data = $array[$i];

                unset($data['evento_id']);

                unset($data['correo']);

                $data['usuario_id'] = auth()->user()->id;

                $eventoPersona = EventoPersona::firstOrCreate(
                    [
                        'evento_id' => $array[$i]['evento_id'],
                        'correo' => $array[$i]['correo']
                    ],
                    $data
                );

                if($eventoPersona->wasRecentlyCreated) {
                }
                else {
                    $eventoPersona->update($data);
                }
            }
            
            DB::commit();
            
            return response()->json(['success' => ['code' => 200, 'message' => 'OK', 'id' => '', 'eventoPersona' => '']], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();

            return $e->getMessage();
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}