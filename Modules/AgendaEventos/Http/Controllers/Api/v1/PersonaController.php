<?php

namespace Modules\AgendaEventos\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use Modules\AgendaEventos\Entities\Persona;

//use Modules\AgendaEventos\Http\Controllers\BaseController;

class PersonaController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        $personas = Persona::select('*');
        
        if(!empty($request->input('fields'))) {
            $fields = explode(',', $request->get('fields'));
            $personas->select($fields);
        }
        
        if(!empty($request->input('page'))) {
            $page = $request->get('page');
        }
        else {
            $page = '1';
        }

        if(!empty($request->input('perpage'))) {
            $perpage = $request->get('perpage');
        }
        else {
            $perpage = '10';
        }
        
        $count = $personas->count();
        $offset = ($page - 1) * $perpage;
        
        $personas = $personas->skip($offset)->take($perpage)->get();

        return $personas;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request) {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        try {
            DB::beginTransaction();

            $persona = Persona::find($id);

            if(!$persona) {
                return response()->json(['error' => ['code' => 404, 'message' => 'Not Found', 'id' => $id]], 404);
            }
            
            $persona->update($request->all());

            DB::commit();
            
            return response()->json(['success' => ['code' => 200, 'message' => 'OK', 'id' => $id, 'persona' => $persona]], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}