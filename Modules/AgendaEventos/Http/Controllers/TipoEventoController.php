<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\DataTables\AgendaEventos\TiposDataTable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\TipoEvento;
use View;

use Modules\AgendaEventos\DataTables\PersonasDataTable;

class TipoEventoController extends Controller {
    //Test: OK 
    //Fecha: 10/01/2019
    public function index(PersonasDataTable $personas, TiposDataTable $dataTable) {
        $personas->with('tipo', 'modal_personas');
        if(request()->get('table') === 'personas') {
            return $personas->render('agendaeventos::tipos.index', compact('personas', 'dataTable'));
        }
        else {
            return $dataTable->render('agendaeventos::tipos.index', compact('personas', 'dataTable'));
        }
    }

    //Test: OK 
    //Fecha: 10/01/2019
    public function search(Request $request) {
        return TipoEvento::where('nombre','like','%'.$request['search'].'%')->whereNotNull('id_calendario_google')->take(10)->get();
    }

    //Test: OK 
    //Fecha: 10/01/2019
    public function create() {
        return response()->json([
            'html' =>  view::make('agendaeventos::tipos.create_edit')->render()
        ], 200);
    }

    //Test: OK 
    //Fecha: 10/01/2019
    public function store(Request $request) {
        try {
            DB::beginTransaction();

            if(TipoEvento::where('nombre', $request['nombre'])->whereNotNull('id_calendario_google')->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un calendario registrado con el mismo nombre.'));
            }

            $tipoEvento = TipoEvento::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'tipoevento' => $tipoEvento));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    //Test: OK 
    //Fecha: 10/01/2019
    public function edit($id) {
        return response()->json([
            'html' =>  view::make('agendaeventos::tipos.create_edit')
            ->with('tipo', TipoEvento::findOrFail($id))
            ->render()
        ], 200);
    }

    //Test: OK 
    //Fecha: 10/01/2019
    public function update($id, Request $request) {
        $tipoEvento = TipoEvento::findOrFail($id);
        if(!$tipoEvento){
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }

        try {
            DB::beginTransaction();
            if(TipoEvento::where('nombre', $request['nombre'])->whereNotNull('id_calendario_google')->where('id','!=',$id)->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra un calendario registrado con el mismo nombre.'));
            }

            $tipoEvento = $tipoEvento->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'tipoevento' => $tipoEvento));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    //Test: OK
    //Fecha: 10/01/2019
    public function destroy($id) {
        $tipoEvento = TipoEvento::find($id);
        if(!$tipoEvento) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $tipoEvento->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Calendario eliminado exitosamente.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
