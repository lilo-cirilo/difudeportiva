<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\DataTables\PersonasDataTable;
use Modules\AgendaEventos\DataTables\PresidenteMunicipalDataTable;
use Modules\AgendaEventos\DataTables\DiputadoDataTable;
use App\Models\Cargo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Evento;
use Modules\AgendaEventos\Entities\Ficha;
use Modules\AgendaEventos\Entities\Participacion;
use Modules\AgendaEventos\Entities\TipoEvento;
use Modules\AgendaEventos\Entities\Vestimenta;

class FichaController extends Controller {

    public function index() {
        return view('agendaeventos::index');
    }

    public function create() {
        return view('agendaeventos::ficha.create_edit');
    }

    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
      		$request['cargo_persona_convoca_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_persona_convoca'))])->id;
      		$request['cargo_persona_preside_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_persona_preside'))])->id;
      		$request['presidenta_participacion_id'] = Participacion::firstOrCreate(['nombre' => strtoupper($request->input('participacion_presidenta'))])->id;
      		$request['director_participacion_id'] = Participacion::firstOrCreate(['nombre' => strtoupper($request->input('participacion_director'))])->id;
            
            if($request->input('tipoevento') !== null) {
                $request['tipoevento_id'] = TipoEvento::firstOrCreate(['nombre' => $request->input('tipoevento'), 'id_calendario_google' => null])->id;
            }
            /*else {
                $request['tipoevento_id'] = null;
            }*/

            if(Ficha::where('evento_id', $request['evento_id'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra una ficha registrada para ese evento.'));
			}
			
			if(Ficha::where('numero_ficha', $request['numero_ficha'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra una ficha registrada con ese número de ficha.'));
            }

            $ficha = Ficha::create($request->all());

            if($ficha->evento->estado_modulo_id === 1) {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }

            DB::commit();
            return response()->json(array('success' => true, 'ficha' => $ficha));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    public function show($id) {
        return Ficha::findOrFail($id);
        /*return Ficha::with(array('exam' => function($query) {
            $query->orderBy('result', 'DESC');
        }))
        ->get();*/
    }

    public function edit(Request $request, $evento_id, PersonasDataTable $personas, PresidenteMunicipalDataTable $municipales, DiputadoDataTable $autoridaddiputados) {
        $evento = Evento::findOrFail($evento_id);

        $bitacora = DB::select("SELECT * FROM bitacora WHERE tabla = 'even_fichasinfo' AND usuario_id != 13 AND registro = $evento->id ORDER BY created_at DESC");

        /*if($evento->estado_modulo_id === 1) {
            $evento->update(['estado_modulo_id' => 2]);
        }*/
        
        $vestimenta = Vestimenta::get();
        $participaciones = Participacion::get();
        
        $personas->with('tipo', 'afuncionalessolicitantes');
        $municipales->with('tipo', 'autoridadmunicipio');
        $autoridaddiputados->with('tipo', 'autoridaddiputado');
        
        if(request()->get('table') === 'personas') {
            return $personas->render('agendaeventos::ficha.create_edit', compact('personas','municipales','autoridaddiputados','evento','vestimenta','participaciones','bitacora'));
        } elseif (request()->get('table') === 'autoridadmunicipio') {
            return $municipales->render('agendaeventos::ficha.create_edit', compact('personas','municipales','autoridaddiputados','evento','vestimenta','participaciones','bitacora'));
        } else {
            return $autoridaddiputados->render('agendaeventos::ficha.create_edit', compact('personas','municipales','autoridaddiputados','evento','vestimenta','participaciones','bitacora'));
        }
    }

    public function update($id, Request $request) {
		$ficha = Ficha::findOrFail($id);
		try {
			if(Ficha::where('numero_ficha', $request['numero_ficha'])->where('id', '!=', $id)->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra una ficha registrada con ese número de ficha.'));
            }
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
      		$request['cargo_persona_convoca_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_persona_convoca'))])->id;
            $request['cargo_persona_preside_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_persona_preside'))])->id;
            $request['presidenta_participacion_id'] = Participacion::firstOrCreate(['nombre' => strtoupper($request->input('participacion_presidenta'))])->id;

            if($request->input('participacion_director') === null) {
                $request['director_participacion_id'] = null;
            }
            else {
                $request['director_participacion_id'] = Participacion::firstOrCreate(['nombre' => strtoupper($request->input('participacion_director'))])->id;
            }
            
            if($request->input('tipoevento') !== null) {
                $request['tipoevento_id'] = TipoEvento::firstOrCreate(['nombre' => $request->input('tipoevento'), 'id_calendario_google' => null])->id;
            }
            
            if($ficha->asistentes->count() > 0 && $ficha->aportaciones->count() > 0) {
                $ficha->evento->update(['estado_modulo_id' => 3]);
            }
            else {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }
            
            $ficha->update($request->all());

            auth()->user()->bitacora(request(), [
                'tabla' => 'even_fichasinfo',
                'registro' =>  $ficha->id . '',
                'campos' => '',
                //'campos' => json_encode($ficha) . '',
                'metodo' => request()->method()
            ]);

            DB::commit();
            return response()->json(array('success' => true, 'ficha' => $ficha));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    /*public function getBitacora() {
        return DB::select("SELECT * FROM bitacora WHERE tabla = 'even_fichasinfo' AND usuario_id != 13 ORDER BY created_at DESC");
    }*/

    public function destroy() {
    }
}
