<?php

namespace Modules\AgendaEventos\Http\Controllers;

use App\Models\Cargo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Autoridad;

class AutoridadController extends Controller {
    
    public function index(Request $request) {
        return Autoridad::search()->where('even_autoridadesmuni.fichainfo_id', $request['fichainfo_id'])->get();
	}
	
    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['nombre'] = $request['nombre_autoridad'];
      		$request['cargo_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_autoridad'))])->id;

            if(Autoridad::where('fichainfo_id', $request['fichainfo_id'])->where('nombre', $request['nombre'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra registrada una autoridad con el mismo nombre.'));
			}

            $autoridad = Autoridad::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'autoridad' => $autoridad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function update($autoridad_id, Request $request) {
		$autoridad = Autoridad::findOrFail($autoridad_id);

		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['nombre'] = $request['nombre_autoridad'];
      		$request['cargo_id'] = Cargo::firstOrCreate(['nombre' => strtoupper($request->input('cargo_autoridad'))])->id;
            $autoridad->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'autoridad' => $autoridad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}

    public function destroy($id) {
		$autoridad = Autoridad::find($id);
        if(!$autoridad) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $autoridad->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Autoridad eliminada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
