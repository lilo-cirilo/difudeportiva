<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AgendaEventos\Entities\AutoridadDiputado;
use Modules\AgendaEventos\Entities\Ficha;
use Modules\AgendaEventos\Entities\Diputado;

class AutoridadDiputadoController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request) {
        return AutoridadDiputado::
        select([
            'even_autoridadesdipu.id as id',
            'even_autoridadesdipu.fichainfo_id as fichainfo_id',
            'diputadoslocales.nombre as nombre',
            'diputadoslocales.primer_apellido as primer_apellido',
            'diputadoslocales.segundo_apellido as segundo_apellido',
            'cat_cargos.nombre as cargo',
            'cat_partidospoliticos.siglas as siglas'
        ])

        ->join('diputadoslocales', 'diputadoslocales.id', 'even_autoridadesdipu.diputadolocal_id')
        ->join('cat_cargos', 'cat_cargos.id', 'diputadoslocales.cargo_id')
        ->join('cat_partidospoliticos', 'cat_partidospoliticos.id', 'diputadoslocales.partidopolitico_id')

        ->where('even_autoridadesdipu.fichainfo_id', $request['fichainfo_id'])
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $request['usuario_id'] = auth()->user()->id;

            if(AutoridadDiputado::where('fichainfo_id', $request['fichainfo_id'])->where('diputadolocal_id', $request['diputadolocal_id'])->count() > 0) {
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra registrad(o)a este diputado'));
            }

            $autoridad = AutoridadDiputado::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'autoridad' => $autoridad));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id) {
		$autoridad = AutoridadDiputado::find($id);
        if(!$autoridad) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $autoridad->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Autoridad eliminada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    public function cargar(Request $request) {
        $ficha = Ficha::where('id', $request['fichainfo_id'])->get();
        $diputados = Diputado::where('distrito', $ficha[0]->evento->municipio->distrito->id)->get();
        if(!$diputados) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            for($i = 0; $i < count($diputados); $i++) {
                if(AutoridadDiputado::where('fichainfo_id', $request['fichainfo_id'])->where('diputadolocal_id', $diputados[$i]->id)->count() === 0) {
                    $autoridad = AutoridadDiputado::create([
                        'fichainfo_id' => $request['fichainfo_id'],
                        'diputadolocal_id' => $diputados[$i]->id,
                        'usuario_id' => auth()->user()->id
                    ]);
                }
            }
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Autoridad cargada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
