<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\ActividadResponsables;
use Modules\AgendaEventos\Entities\Responsable;

class ResponsableController extends Controller {
    
    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['responsable_id'] = Responsable::firstOrCreate(
				[
					'nombre' => $request['nombre_responsable'], 
					'cargo' => $request['cargo_responsable']], 
				[
					'usuario_id' => $request['usuario_id']
				]
			)->id;

            if(ActividadResponsables::where('actividad_id', $request['actividad_id'])->where('responsable_id', $request['responsable_id'])->count() > 0) {
                return response()->json(array('success' => false, 'message' => 'Ese responsable ya se encuentra asociado a la actividad.'));
			}

            $actividad_responsable = ActividadResponsables::create($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'actividad_responsable' => $actividad_responsable));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function update($actividad_responsable_id, Request $request) {
		$actividad_responsable = ActividadResponsables::findOrFail($actividad_responsable_id);
		$actividad_responsable->responsable->nombre =  $request['nombre_responsable'];
		$actividad_responsable->responsable->cargo =  $request['cargo_responsable'];
		$actividad_responsable->responsable->save();

		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['responsable_id'] = Responsable::firstOrCreate(
				[
					'nombre' => $request['nombre_responsable'], 
					'cargo' => $request['cargo_responsable']], 
				[
					'usuario_id' => $request['usuario_id']
				]
			)->id;

            $actividad_responsable->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'actividad_responsable' => $actividad_responsable));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function destroy($id) {
		$actividad_responsable = ActividadResponsables::find($id);
        if(!$actividad_responsable) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();
            $actividad_responsable->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Responsable eliminado.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
