<?php

namespace Modules\AgendaEventos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\Entities\Ficha;
use Modules\AgendaEventos\Entities\Aportacion;
use Modules\AgendaEventos\Entities\Producto;

class AportacionController extends Controller {
    
    public function index(Request $request) {
        return Aportacion::search()->where('even_aportaciones.fichainfo_id', $request['fichainfo_id'])->get();
	}
	
    public function store(Request $request) {
		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
      		$request['producto_id'] = Producto::firstOrCreate(['nombre' => strtoupper($request->input('producto'))])->id;      		

            if(Aportacion::where('fichainfo_id', $request['fichainfo_id'])->where('dependencia_id', $request['dependencia_id'])->where('producto_id', $request['producto_id'])->count() > 0){
                return response()->json(array('success' => false, 'message' => 'Ya se encuentra registrada esa aportación de la misma dependencia.'));
			}

            $aportacion = Aportacion::create($request->all());

            $ficha = Ficha::findOrFail($request['fichainfo_id']);

            if($ficha->asistentes->count() > 0 && $ficha->aportaciones->count() > 0) {
                $ficha->evento->update(['estado_modulo_id' => 3]);
            }
            else {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }

            DB::commit();
            return response()->json(array('success' => true, 'aportacion' => $aportacion));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}

    public function update($aportacion_id, Request $request) {
		$aportacion = Aportacion::findOrFail($aportacion_id);

		try {
            DB::beginTransaction();
			$request['usuario_id'] = auth()->user()->id;
			$request['producto_id'] = Producto::firstOrCreate(['nombre' => strtoupper($request->input('producto'))])->id;
            $aportacion->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'aportacion' => $aportacion));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
	}
	
    public function destroy($id) {
		$aportacion = Aportacion::find($id);
        if(!$aportacion) {
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try {
            DB::beginTransaction();

            $request['fichainfo_id'] = $aportacion->fichainfo_id;

            $aportacion->delete();

            $ficha = Ficha::findOrFail($request['fichainfo_id']);

            if($ficha->asistentes->count() > 0 && $ficha->aportaciones->count() > 0) {
                $ficha->evento->update(['estado_modulo_id' => 3]);
            }
            else {
                $ficha->evento->update(['estado_modulo_id' => 2]);
            }

            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Aportación eliminada.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
