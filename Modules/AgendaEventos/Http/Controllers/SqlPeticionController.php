<?php

namespace Modules\AgendaEventos\Http\Controllers;
use Modules\AgendaEventos\Entities\SqlPeticion;
use Illuminate\Support\Facades\DB;
use Modules\AgendaEventos\DataTables\SqlPeticionDataTable;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class SqlPeticionController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SqlPeticionDataTable $dataTable) {
			return $dataTable->render('agendaeventos::sqlpeticion.index', compact('dataTable'));
    }

    public function getLocalidades(Request $request) {
        if($request->ajax()) {
            $localidades = DB::connection('sqlsrv')
            ->table('BdLocalidades.dbo.vLocal2000')
            ->select([
                DB::raw('[Clave_Municipio] AS id'),
                DB::raw("CONCAT('(', Region, ') ', '(', Distrito, ') ', Municipio) as nombre")
            ])
            ->where('Clave_Municipio', '!=', '000')
            ->where('Municipio', 'LIKE', '%' . $request->input('search') . '%')
            ;

            $localidades = $localidades
            ->distinct('Clave_Municipio')
            ->take(10)
            ->get()
            ->toArray();

            return response()->json($localidades);
        }
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agendaeventos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agendaeventos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agendaeventos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}