@extends('agendaeventos::layouts.app')

@push('head')
@endpush

@section('content-title', 'Agenda de giras y eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
<li class="active">Calendarios</li>
@endsection

@section('content')
    <section class="content">
        <div class="box box-primary shadow">
            <div class="box-header with-border">
                <h3 class="box-title">Catálogo de calendarios</h3>
            </div>
            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="buscar" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                    </span>
                </div>
                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tipos', 'name' => 'tipos', 'style' => 'width: 100%']) !!}
            </div>
        </div>        
    </section>    

    <div class="modal fade" id="modal-tipo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-tipo-title"></h4>
                </div>
                <div class="modal-body" id="modal-body-tipo"></div>
                <div class="modal-footer" id="modal-footer-tipo">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                    <button type="button" class="btn btn-success" onclick="TipoEvento.guardar()"><i class="fa fa-database"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_compartir">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal_compartir_title"></h4>
                </div>

                <div class="modal-body" id="modal_compartir_body">
                    <form id="form_compartir" name="form_compartir">
                        <div class="form-group">
                            <label for="email">Correo Electrónico:</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                    </form>
                </div>

                <div class="modal-footer" id="modal_compartir_footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                    <button type="button" class="btn btn-success" onclick="TipoEvento.compartir()"><i class="fa fa-database"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
{!! $dataTable->scripts() !!}

<script>
function block() {
    $('#app').block({ message: null });
}
function unblock() {
    $('#app').unblock();
}
function handleClientLoad() {
    //alert('handleClientLoad');
}
</script>

<script>
/*const app = new Vue({
    el: '#demo'
});*/
/*const app = new Vue({
    el: '#demo',
    data: () => ({
        message: 'Hola Mundo.'
    })
});*/
</script>
@endpush