<form data-toggle="validator" role="form" id="form_tipo">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="nombre">Nombre* :</label>
                <input type="text" id="nombre" name="nombre" placeholder="NOMBRE" class="form-control" value="{{ isset($tipo) ? $tipo->nombre : '' }}">
            </div>
        </div>                    
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="descripcion">Descripción:</label>
                <textarea id="descripcion" name="descripcion" class="form-control" placeholder="DESCRIPCION" rows="3" value="{{ $tipo->descripcion or '' }}">{{ $tipo->descripcion or '' }}</textarea>
            </div>
        </div>
    </div>
</form>