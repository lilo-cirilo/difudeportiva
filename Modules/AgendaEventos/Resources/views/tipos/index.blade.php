@extends('agendaeventos::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    
    <style>
        span.select2-selection__rendered, .select2-results__option {
            text-transform: uppercase;
        }

        [class^="icheckbox_square-"]:not(.checked) {
            background-position: -24px 0 !important
        }
    </style>
@endpush

@section('content-title', 'Agenda de giras y eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Calendarios</li>
@endsection

@section('content')
    <section class="content">
        <div class="box box-primary shadow">
            <div class="box-header with-border">
                <h3 class="box-title">Catálogo de calendarios</h3>
            </div>
            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="buscar" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                    </span>
                </div>
                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tipos', 'name' => 'tipos', 'style' => 'width: 100%']) !!}
            </div>
        </div>        
    </section>    

    <div class="modal fade" id="modal-tipo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-tipo-title"></h4>
                </div>
                <div class="modal-body" id="modal-body-tipo"></div>
                <div class="modal-footer" id="modal-footer-tipo">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                    <button type="button" class="btn btn-success" onclick="TipoEvento.guardar()"><i class="fa fa-database"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal_compartir">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal_compartir_title">Compartir</h4>
                </div>

                <div class="modal-body" id="modal_compartir_body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#compartir" data-toggle="tab" aria-expanded="true">Compartir Calendario</a></li>
                            <li class=""><a href="#tablaCompartir" data-toggle="tab" aria-expanded="false">Invitados Calendario</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="compartir">
                                <form id="form_compartir" name="form_compartir">

                                    <div class="form-group">
                                        <label for="persona_id" id="label_persona_id">PERSONA*:</label>
                                        <label for="nombre_persona" id="label_nombre" style="display: none;">NOMBRE*:</label>
                                        <div class="input-group">
                                            <select id="persona_id" name="persona_id" class="form-control select2 input-persona" style="width: 100%;" readonly="readonly">
                                            </select>
                                            <input type="text" class="form-control" id="nombre_persona" name="nombre_persona" style="display: none;">
                                            <div class="input-group-btn" id="input_group_btn_search">
                                                <button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal_personas();">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-primary" style="padding-left: 25px; padding-right: 25px;" onclick="toggle_form();">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" id="form_group_primer_apellido" style="display: none;">
                                        <label for="primer_apellido">APELLIDO PATERNO*:</label>
                                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido">
                                    </div>

                                    <div class="form-group" id="form_group_segundo_apellido" style="display: none;">
                                        <label for="segundo_apellido">APELLIDO MATERNO*:</label>
                                        <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="email">CORREO*:</label>
                                        <input type="text" class="form-control" id="email" name="email">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="rol">ROL:</label>
                                        <select id="rol" name="rol" class="form-control select2" style="width: 100%;">
                                            <!--<option value="1">NONE</option>-->
                                            <option value="3">LECTOR</option>
                                            <option value="4">ESCRITOR</option>
                                            <option value="5">PROPIETARIO</option>
                                        </select>
                                    </div>

                                </form>

                                <div class="row">
                                    <button type="button" class="btn btn-success" onclick="TipoEvento.compartir()"><i class="fa fa-database"></i> Compartir</button>
                                </div>
                            </div>
                            <div class="tab-pane" id="tablaCompartir">
                                <table class="table table-bordered" id="calendario_personas" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Primer Apellido</th>
                                            <th>Segundo Apellido</th>
                                            <th>Correo</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Primer Apellido</th>
                                            <th>Segundo Apellido</th>
                                            <th>Correo</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer" id="modal_compartir_footer">
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="modal_personas">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal_personas_title">DIRECTORIO:</h4>
                </div>

                <div class="modal-body" id="modal_personas_body">

                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_input" name="search_input" class="form-control">

                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default button-dt tool" id="search_button" name="search_button">BUSCAR</button>
                    </span>
                </div>

                    {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}

                </div>

                <div class="modal-footer" id="modal_personas_footer">
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
{!! $dataTable->scripts() !!}
{!! $personas->html()->scripts() !!}
@include('agendaeventos::eventos.js.app')
@include('agendaeventos::eventos.js.tipoevento')
<script type="text/javascript">
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    var is_toggled = 0;
    var oTable;
    
    $(".modal").modal({
        keyboard: false,
        backdrop: "static"
    }).modal('hide');

    $('#search_button').on('click', function() {
        window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
    });

    $('#search_input').keypress(function(e) {
        if(e.which === 13) {
            window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
        }
    });
    
    function abrir_modal_personas() {
        window.LaravelDataTables['personas'].ajax.reload(null, false);
        $('#modal_personas').modal('show');
    }

    function toggle_form() {
        $('#form_compartir').validate().resetForm();
        $('#form_compartir').removeData('validator');
        $('#persona_id').empty().trigger('change');
        $("#email").val('');
        $("#nombre_persona").val('');
        $("#primer_apellido").val('');
        $("#segundo_apellido").val('');

        if(is_toggled === 0) {
            is_toggled = 1;

            $('#label_persona_id').hide();
            $('#persona_id').hide();
            $('#input_group_btn_search').hide();

            $('#label_nombre').show();
            $('#nombre_persona').show();
            $('#form_group_primer_apellido').show();
            $('#form_group_segundo_apellido').show();
            
            $('#form_compartir').validate({
                rules: {
					nombre_persona: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
					primer_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					segundo_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
                    email: {
                        required: true,
                        email: true
                    }
                }
            }); 
        }
        else {
            is_toggled = 0;

            $('#label_persona_id').show();
            $('#persona_id').show();
            $('#input_group_btn_search').show();

            $('#label_nombre').hide();
            $('#nombre_persona').hide();
            $('#form_group_primer_apellido').hide();
            $('#form_group_segundo_apellido').hide();
            
            $('#form_compartir').validate({
                rules: {
                    persona_id: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });
        }
    }

    function seleccionar_persona(id, email, nombre) {
        window.LaravelDataTables['personas'].ajax.reload(null, false);
        $('#persona_id').html("<option value='" + id + "'selected>" + nombre + "</option>");
        $('#email').val(email);
        $('#modal_personas').modal('hide');
    }
    
    $(document).ready(() => {
        TipoEvento.get_tipos();

        oTable = $('#calendario_personas').DataTable({
            language: {
                'url': '{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}'
            },
            processing: true,
            serverSide: true,
            ajax: '/agenda/api/v1/calendariospersonas' + '?calendario_id=' + TipoEvento.get_calendario_id(),
            columns: [
                { data: 'nombre', name: 'nombre' },
                { data: 'primer_apellido', name: 'primer_apellido' },
                { data: 'segundo_apellido', name: 'segundo_apellido' },
                { data: 'correo', name: 'correo' }
            ]
        });
    });
</script>
<script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};TipoEvento.handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
@endpush