@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@if(auth()->check())
  @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('message-url-all', '#')
@section('message-all', '')
@section('message-number')
@section('message-text', 'No tienes mensajes')

@section('task-number')
@section('task-text', 'No tienes tareas')
{{-- @section('logo', asset('images/AF.png'))
@section('mini-logo', asset('images/funcionalesIcono.png')) --}}

@section('content-title', 'Agenda de eventos y giras')

@push('head')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style>
    .modal-dialog:not([role="document"]) > .modal-content > .modal-body {
        overflow-y: auto;
        max-height: calc(100vh - 210px);
    }

    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    /*.nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
        background: #d46985;
    }*/

</style>
@endpush

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Agenda de eventos y giras</a></li>
        @yield('li-breadcrumbs')
    </ol>
@endsection

@section('sidebar-menu')
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Agenda de eventos y giras</li>
        <li>
            <a href="{{ route('home') }}">
                <i class="fa fa-home"></i><span>IntraDIF</span>
            </a>
        </li>

        <li {{ ($ruta === 'agenda.index' || $ruta == '') ? 'class=active' : '' }}>
            <a href="{{ route('agenda.index') }}">
                <i class="fa fa-list-alt"></i> <span>Agenda de eventos</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        <li {{ ($ruta === 'agenda.peticiones' || $ruta == '') ? 'class=active' : '' }}>
            <a href="{{ route('agenda.peticiones') }}">
                <i class="fa fa-list-alt"></i> <span>Gestion de solicitudes</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        {{-- <li {{ ($ruta === 'agenda.inversiones' || $ruta == '') ? 'class=active' : '' }}>
            <a href="{{ route('agenda.inversiones') }}">
                <i class="fa fa-list-alt"></i> <span>Inversiones en programas</span>
                <span class="pull-right-container"></span>
            </a>
        </li> --}}

        @if(auth()->user()->hasRoles(['ADMINISTRADOR DE EVENTOS']))

        <li class="header">Catálogos</li>

        <li {{ $ruta === 'agenda.calendarios.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.calendarios.index') }}">
                <i class="fa fa-list-alt"></i> <span>Calendarios</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        {{-- <li {{ $ruta === 'agenda.productos.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.productos.index') }}">
                <i class="fa fa-list-alt"></i> <span>Productos</span>
                <span class="pull-right-container"></span>
            </a>
        </li> --}}

        <li {{ $ruta === 'agenda.vestimenta.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.vestimenta.index') }}">
                <i class="fa fa-list-alt"></i> <span>Vestimenta</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        <li {{ $ruta === 'agenda.personas.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.personas.index') }}">
                <i class="fa fa-list-alt"></i> <span>Directorio</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        <li {{ $ruta === 'agenda.autoridadesmunicipios.create' ? 'class=active' : '' }}>
          <a href="{{ route('agenda.autoridadesmunicipios.create') }}">
              <i class="fa fa-list-alt"></i> <span>Autoridades Municipales</span>
              <span class="pull-right-container"></span>
          </a>
        </li>

        @endif
    </ul>
@endsection

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	function block() {
        baseZ = 1030;
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

        function unblock_error() {
            if($.unblockUI()) {
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
            }
        }

	    setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
@endpush
