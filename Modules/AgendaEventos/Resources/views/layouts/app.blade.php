@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@auth
  @section('user-name', auth()->user()->persona->nombre)
@endauth

@section('breadcrumbs')
@endsection

@auth
  @section('sidebar-menu')
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">AGENDA DE EVENTOS Y GIRAS</li>
        <li>
            <a href="{{ route('home') }}">
                <i class="fa fa-home"></i><span>IntraDIF</span>
            </a>
        </li>

        <li {{ ($ruta === 'agenda.index' || $ruta == '') ? 'class=active' : '' }}>
            <a href="{{ route('agenda.index') }}">
                <i class="fa fa-list-alt"></i> <span>AGENDA DE EVENTOS</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        {{-- <li {{ ($ruta === 'agenda.inversiones' || $ruta == '') ? 'class=active' : '' }}>
            <a href="{{ route('agenda.inversiones') }}">
                <i class="fa fa-list-alt"></i> <span>INVERSIONES EN PROGRAMAS</span>
                <span class="pull-right-container"></span>
            </a>
        </li> --}}

        @if(auth()->user()->hasRoles(['ADMINISTRADOR DE EVENTOS']))

        <li class="header">CATALOGOS</li>

        <li {{ $ruta === 'agenda.calendarios.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.calendarios.index') }}">
                <i class="fa fa-list-alt"></i> <span>CALENDARIOS</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        {{-- <li {{ $ruta === 'agenda.productos.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.productos.index') }}">
                <i class="fa fa-list-alt"></i> <span>PRODUCTOS</span>
                <span class="pull-right-container"></span>
            </a>
        </li> --}}

        <li {{ $ruta === 'agenda.vestimenta.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.vestimenta.index') }}">
                <i class="fa fa-list-alt"></i> <span>VESTIMENTA</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        <li {{ $ruta === 'agenda.personas.index' ? 'class=active' : '' }}>
            <a href="{{ route('agenda.personas.index') }}">
                <i class="fa fa-list-alt"></i> <span>DIRECTORIO</span>
                <span class="pull-right-container"></span>
            </a>
        </li>

        @endif
    </ul>
  @endsection
@endauth

@push('head')
<link href="{{ asset('css/base.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-title')
AGENDA DE EVENTOS Y GIRAS
@endsection

@push('body')
<script type="text/javascript" src="{{ asset('js/base.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@endpush
