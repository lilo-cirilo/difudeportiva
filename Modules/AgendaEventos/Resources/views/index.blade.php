@extends('agendaeventos::layouts.master')

@push('head')
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">

    <link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
    <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables/rowGroup.bootstrap.min.css') }}">
    <style>
        span.select2-selection__rendered, .select2-results__option {
            text-transform: uppercase;
        }

        [class^="icheckbox_square-"]:not(.checked) {
            background-position: -24px 0 !important
        }
    </style>
@endpush

@section('content-title', 'Agenda de Giras y Eventos')
@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Agenda de Giras y Eventos</li>
@endsection

@section('content')
    <section class="content">
        <div class="nav-tabs-custom shadow">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_lista" data-toggle="tab" aria-expanded="true">Lista de Giras y Eventos</a></li>
                <li class=""><a href="#tab_calendario" data-toggle="tab" aria-expanded="false">Calendario</a></li>              
            </ul>
            <div class="tab-content">
                @if(auth()->user()->hasRoles(['SUPERADMIN', 'ADMINISTRADOR', 'ADMINISTRADOR DE EVENTOS']))
                <div class="row" style="margin-left: -15px;margin-right: -15px;">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                        <div class="form-group">
                            <label for="tiposeventos_id">Descargar Excel por Calendario* :</label>
                            <select id="tiposeventos_id" name="tipoevento_id" class="form-control select2" style="width: 100%;">
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <div class="form-group">
                            <label for="">*</label>
                            <button type="button" class="btn btn-block btn-info" onclick="tipoEventoExcel();">Descargar</button>
                        </div>
                    </div>
                </div>
                @endif
                <div class="tab-pane active" id="tab_lista">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                            <input type="text" id="buscar" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                            </span>
                        </div>
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'giras', 'name' => 'giras', 'style' => 'width: 100%']) !!}
                </div>
                <div class="tab-pane" id="tab_calendario">
                    <div class="row">
                        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                            <div class="box box-solid box-primary">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Calendarios</h4>
                                </div>
                                <div class="box-body">
                                    @foreach ($tipos as $tipo)
                                        <div class="form-group">
                                            <label>
                                            <input type="checkbox" value="{{ $tipo->id }}" name="tipos_eventos[]" class="square-{{ $tipo->color }}">
                                                {{ $tipo->nombre }}
                                            </label>
                                        </div>    
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div id="calendario"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>    

    <div class="modal fade" id="modal-evento">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal-evento-title"></h4>
                </div>
                <div class="modal-body" id="modal-body-evento"></div>
                <div class="modal-footer" id="modal-footer-evento">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban"></i> Cancelar</button>
                    <button type="button" class="btn btn-success" onclick="Evento.guardar()"><i class="fa fa-database"></i> Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_personas">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="modal_personas_title">DIRECTORIO:</h4>
                </div>

                <div class="modal-body" id="modal_personas_body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search_input" name="search_input" class="form-control">

                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default button-dt tool" id="search_button" name="search_button">BUSCAR</button>
                        </span>
                    </div>

                    {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}

                </div>

                <div class="modal-footer" id="modal_personas_footer">
                    <div class="pull-right">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>

<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="../bower_components/moment/moment.js"></script>
<script src="../bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src='../bower_components/fullcalendar/dist/locale-all.js'></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
@include('agendaeventos::eventos.js.app')
@include('agendaeventos::eventos.js.evento')
<script type="text/javascript">
            $('#tiposeventos_id').select2({
                language: 'es',
                allowClear : true,
                placeholder : 'Seleccione el calendario',
				ajax: {
					url: "{{ route('agenda.calendarios.search') }}",
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
            });
            
    var imprimirFichaI = (id) => {
        block();
                $.ajax({
					url: "/agenda/fichas/" + id,
					type: 'GET',
					success: (response) => {
						console.log(response);
						jsreport.serverUrl = 'http://187.157.97.110:3001';
						{{-- jsreport.serverUrl = '{{ env("REPORTER_URL", "http://187.157.97.110:3001") }}'; --}}
						var request = {
							template:{
								shortid: "BytOF_sq4"
							},
							data: response
						};
						jsreport.renderAsync(request).then(function(res) {
                            //window.open(res.toDataURI());
                            res.download('FichaInformativa.pdf');
                            unblock();
						});
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
    }

    var tipoEventoExcel = () => {
        block();
        var url;
        if($("#tiposeventos_id").select2('val') === null) {
            url = '/agenda/excelevento';
        }
        else {
            url = '/agenda/excelevento?even_cat_tiposevento_id=' + $("#tiposeventos_id").select2('val');
        }
				$.ajax({
					url: url,
					type: 'GET',
					success: (response) => {
						console.log(response);
                        var reformattedArray = response.map(function(obj) {
                            return Object.values(obj);
                        });

						jsreport.serverUrl = 'http://187.157.97.110:3001';
						var request = {
							template:{
								shortid: 'HJ5VttAyS'
							},
							data: {
                                titulo: 'Agenda Eventos',
                                subtitulo: 'Lista de datos',
                                cabeceras: [
                                    'CALENDARIO',
                                    'EVENTO',
                                    'REGION',
                                    'DISTRITO',
                                    'MUNICIPIO',
                                    'TIPO INSTITUCION',
                                    'INSTITUCION',
                                    'NUMERO FICHA',
                                    'PRODUCTO',
                                    'FECHA',
                                    'CANTIDAD',
                                    'MONTO'
                                ],
                                datos: reformattedArray
                            }
						};

						jsreport.renderAsync(request).then(function(res) {
                            res.download('datos.xlsx');
                            unblock();
						});
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
    }

    var eventoMunicipioExcel = (evento_id, gira_nombre, id_calendario_google, id_evento_google) => {
        block();
				$.ajax({
					url: '/agenda/excelevento?evento_id=' + evento_id,
					type: 'GET',
					success: (response) => {
						console.log(response);
                        var reformattedArray = response.map(function(obj) {
                            return Object.values(obj);
                        });

						jsreport.serverUrl = 'http://187.157.97.110:3001';
						var request = {
							template:{
								shortid: 'HJ5VttAyS'
							},
							data: {
                                titulo: 'Agenda Eventos',
                                subtitulo: 'Lista de datos',
                                cabeceras: [
                                    'CALENDARIO',
                                    'EVENTO',
                                    'REGION',
                                    'DISTRITO',
                                    'MUNICIPIO',
                                    'TIPO INSTITUCION',
                                    'INSTITUCION',
                                    'NUMERO FICHA',
                                    'PRODUCTO',
                                    'FECHA',
                                    'CANTIDAD',
                                    'MONTO'
                                ],
                                datos: reformattedArray
                            }
						};

						jsreport.renderAsync(request).then(function(res) {
                            res.download('datos.xlsx');
                            unblock();
						});
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
    }

    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    var is_toggled = 0;
    
    $(".modal").modal({
        keyboard: false,
        backdrop: "static"
    }).modal('hide');

    $('#search_button').on('click', function() {
        window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
    });

    $('#search_input').keypress(function(e) {
        if(e.which === 13) {
            window.LaravelDataTables['personas'].search($('#search_input').val()).draw();
        }
    });

    function abrir_modal_personas() {
        window.LaravelDataTables['personas'].ajax.reload(null, false);
        $('#modal_personas').modal('show');
    }
    
    $(document).ready(() => {        
        Evento.get_giras();
    });

    function toggle_form() {
        $('#form_compartir').validate().resetForm();
        $('#form_compartir').removeData('validator');
        $('#persona_id').empty().trigger('change');
        $("#email").val('');
        $("#nombre_persona").val('');
        $("#primer_apellido").val('');
        $("#segundo_apellido").val('');

        if(is_toggled === 0) {
            is_toggled = 1;

            $('#label_persona_id').hide();
            $('#persona_id').hide();
            $('#input_group_btn_search').hide();

            $('#label_nombre').show();
            $('#nombre_persona').show();
            $('#form_group_primer_apellido').show();
            $('#form_group_segundo_apellido').show();
            
            $('#form_compartir').validate({
                rules: {
					nombre_persona: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
					primer_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					segundo_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
                    email: {
                        required: true,
                        email: true
                    }
                }
            }); 
        }
        else {
            is_toggled = 0;

            $('#label_persona_id').show();
            $('#persona_id').show();
            $('#input_group_btn_search').show();

            $('#label_nombre').hide();
            $('#nombre_persona').hide();
            $('#form_group_primer_apellido').hide();
            $('#form_group_segundo_apellido').hide();
            
            $('#form_compartir').validate({
                rules: {
                    persona_id: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                }
            });
        }
    }

    function seleccionar_persona(id, email, nombre) {
        window.LaravelDataTables['personas'].ajax.reload(null, false);
        $('#persona_id').html("<option value='" + id + "'selected>" + nombre + "</option>");
        $('#email').val(email);
        $('#modal_personas').modal('hide');
    }

    function patch_event() {
        return gapi.client.calendar.events.get({
            "calendarId": 'op9ikse90201gdleagadd9cu5o@group.calendar.google.com',
            "eventId": 'nqkml7b26577pr0bvsms26r5us'
        })
        .then(function(response) {
            var attendees;

            if(typeof response.result.attendees === 'undefined') {
                attendees = [];
            }
            else {
                attendees = response.result.attendees;
            }

            console.log(attendees);

            /*var obj = {
                'email': 'alexander.ruiz@itoaxaca.edu.mx'
            };

            attendees.push(obj);*/

            var patch_event = {
                'attendees': attendees
            };

            return gapi.client.calendar.events.patch({
                'calendarId': 'op9ikse90201gdleagadd9cu5o@group.calendar.google.com',
                'eventId': 'nqkml7b26577pr0bvsms26r5us',
                'resource': patch_event
            })
            .then(function(response) {
                console.log(response);
            },
            function(error) {
                console.log(error);
            });

        },
        function(error) {
            console.log(error);
        });
    }
</script>
<script async defer src="https://apis.google.com/js/api.js" onload="this.onload=function(){};Evento.handleClientLoad()" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
{!! $dataTable->scripts() !!}
{!! $personas->html()->scripts() !!}
@endpush