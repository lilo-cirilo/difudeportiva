<div class="nav-tabs-custom" id="tabs">
    <ul class="nav nav-tabs">
    <li class="active"><a href="#evento">Evento</a></li>
    
    <li><a href="#areas">Áreas Participantes</a></li>
    <li><a href="#compartir">Compartir Evento</a></li>
    <li><a href="#tablaCompartir">Invitados Evento</a></li>
    
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="evento">
            <form data-toggle="validator" role="form" id="form_evento">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="nombre">Nombre de la gira o del evento* :</label>
                            <select id="nombre" name="nombre" class="form-control select2" style="width: 100%;">
                                @if(isset($evento))
                                    <option value="{{ $evento->gira->nombre }}" selected>{{ $evento->gira->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="tipoevento_id">Calendario* :</label>
                            <select id="tipoevento_id" name="tipoevento_id" class="form-control select2" style="width: 100%;">
                                @if(isset($evento))
                                    <option value="{{ $evento->calendario->id }}" selected>{{ $evento->calendario->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="form-group">
                            <label for="fecha">Fecha* :</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="fecha" name="fecha" autocomplete="off" value="{{ isset($evento) ? $evento->get_formato_fecha() : '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Hora* :</label>
                                <div class="input-group">
                                    <input type="text" class="form-control timepicker" id="horax" name="horax" value="{{ isset($evento) ? $evento->get_formato_hora() : '' }}">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="calle">Calle* :</label>
                            <input type="text" class="form-control" id="calle" name="calle" placeholder="NOMBRE DE LA CALLE" autocomplete="off" value="{{ isset($evento) ? $evento->calle : '' }}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="municipio_id">Municipio* :</label>
                            <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
                                @if(isset($evento) && isset($evento->municipio))
                                <option value="{{ $evento->municipio->id }}" selected>({{ $evento->municipio->distrito->region->nombre }}) ({{ $evento->municipio->distrito->nombre }}) {{ $evento->municipio->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="localidad_id">Localidad:</label>
                            <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
                                @if(isset($evento) && isset($evento->cat_localidad))
                                    <option value="{{ $evento->cat_localidad->id }}" selected>{{ $evento->cat_localidad->nombre }}</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="localidad">Otra localidad:</label>
                            <input type="text" id="localidad" name="localidad" placeholder="OTRA LOCALIDAD" class="form-control" value="{{ isset($evento) ? $evento->localidad : '' }}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="observacion">Observaciones:</label>
                            <textarea id="observacion" name="observacion" class="form-control" rows="3" value="{{ $evneto->observacion or '' }}">{{ $evneto->observacion or '' }}</textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="tab-pane" id="areas">
            <form data-toggle="validator" role="form" id="form_area">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="area_id">Área* :</label>
                            <select id="area_id" name="area_id" class="form-control select2" style="width: 100%;"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="genera_ficha">Genera Ficha* :</label>
                            <select id="genera_ficha" name="genera_ficha" class="form-control select2" style="width: 100%;">
                                <option value="0">NO</option>
                                <option value="1">SI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="observaciones">Observación:</label>
                            <textarea id="observaciones" name="observaciones" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                        <div class="form-group" style="margin-top: 20px;">
                            <label for="" style="color:white">Agregar:</label>
                            <button id="btnAgregar" type="button" class="btn btn-block btn-info" onclick="Evento.agregar_area()">
                                Agregar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <table id="tblAreas" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center">Área</th>
                                <th class="text-center">Genera Ficha</th>
                                <th class="text-center">Observación</th>
                                <th class="text-center">Editar</th>
                                <th class="text-center">Eliminar</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="tab-pane" id="compartir">
            <form data-toggle="validator" role="form" id="form_compartir">
                <div class="form-group">
                    <label for="persona_id" id="label_persona_id">PERSONA*:</label>
                    <label for="nombre_persona" id="label_nombre" style="display: none;">NOMBRE*:</label>
                    <div class="input-group">
                        <select id="persona_id" name="persona_id" class="form-control select2 input-persona" style="width: 100%;" readonly="readonly">
                        </select>
                        <input type="text" class="form-control" id="nombre_persona" name="nombre_persona" style="display: none;">
                        <div class="input-group-btn" id="input_group_btn_search">
                            <button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal_personas();">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-primary" style="padding-left: 25px; padding-right: 25px;" onclick="toggle_form();">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="form-group" id="form_group_primer_apellido" style="display: none;">
                    <label for="primer_apellido">APELLIDO PATERNO*:</label>
                    <input type="text" class="form-control" id="primer_apellido" name="primer_apellido">
                </div>

                <div class="form-group" id="form_group_segundo_apellido" style="display: none;">
                    <label for="segundo_apellido">APELLIDO MATERNO*:</label>
                    <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido">
                </div>
                
                <div class="form-group">
                    <label for="email">CORREO*:</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
            </form>
            
            <div class="row">
                <button type="button" class="btn btn-success" onclick="Evento.compartir()"><i class="fa fa-database"></i> Compartir</button>
            </div>
        </div>

        <div class="tab-pane" id="tablaCompartir">
            <table class="table table-bordered" id="evento_personas" style="width: 100%;">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Primer Apellido</th>
                        <th>Segundo Apellido</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Primer Apellido</th>
                        <th>Segundo Apellido</th>
                        <th>Correo</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        
    </div>
</div>