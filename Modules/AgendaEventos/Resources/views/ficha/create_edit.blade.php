@extends('agendaeventos::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css')}}">
<style>
    textarea {
        resize: none;
    }
	span.select2-selection__rendered, .select2-results__option, .select2-search__field {
		text-transform: uppercase;
	}
</style>
@endpush

@section('content-title', 'Ficha informativa')
@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li> <a href="{{ route('agenda.index') }}">Agenda</a></li>
    @if (isset($evento->ficha))
        <li class="active">Editar ficha</li>
    @else
        <li class="active">Nueva ficha</li>
    @endif
@endsection

@section('content')
    <div class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $evento->gira->nombre }}</h3>

            <div class="pull-right">
              <button type="button" class="btn btn-primary" onclick="Ficha.guardar()">
                <i class="fa fa-database"></i> Guardar
              </button>

              <button type="button" class="btn btn-info" onclick="Ficha.imprimir()">
                <i class="fa fa-file-pdf-o"></i> Imprimir
              </button>
            </div>

            
        </div>
        <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <strong><i class="fa fa-calendar margin-r-5"></i> Fecha y hora</strong>
                <p class="text-muted">
                    {{ $evento->get_fecha_letra() }}
                    <br>
                    {{ $evento->get_formato_hora_minutos() . ' HRS.' }}
                </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <strong><i class="fa fa-map-marker margin-r-5"></i> Ubicación</strong>
                <p class="text-muted">
                    {{ $evento->calle }}
                    <br>
                    {{ $evento->localidad  ? $evento->localidad : $evento->cat_localidad->nombre }}
                    <br>
                    {{ $evento->municipio->nombre }}
                </p>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="nav-tabs-custom no-padding-b" id="tabFicha">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_datos">Datos generales</a></li>
                        <li><a href="#tab_asistentes">Asistentes</a></li>
                        <li><a href="#tab_actividades">Actividades</a></li>
												<li><a href="#tab_aportaciones">Aportaciones</a></li>
												<li><a href="#tab_autoridades">Autoridades municipales</a></li>
												<li><a href="#tab_diputados">Diputados</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab_datos">
							<form role="form" id="form_datos" onsubmit="return false;">                            
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">                                                                                    
										<div class="box-body" style="padding-left: 0px; padding-right: 0px;">
                                            {{-- <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
                                                    <div class="form-group">
                                                        <button type="button" class="btn bg-purple" onclick="getDarkSky();"><i class="fa fa-cloud"></i> Cargar Datos de Pronostico</button>
                                                    </div>
												</div>
                                            </div> --}}
											<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
													<div class="form-group">
														<label>No. Ficha* :</label>
														<input type="text" id="numero_ficha" name="numero_ficha" class="form-control" value="{{ isset($evento->ficha) ? $evento->ficha->numero_ficha : '' }}">
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
													<div class="form-group">
														<label for="tipoevento">Tipo de evento* :</label>               
                                                        <select id="tipoevento" class="form-control select2"  name="tipoevento" style="width: 100%;">
                                                            
																{{-- <option value=""></option>
																<option value="FEDERAL" {{ isset($evento->ficha) && $evento->ficha->tipoevento->nombre == 'FEDERAL' ? 'selected' : '' }}>FEDERAL</option>
																<option value="GOBIERNO DEL ESTADO" {{ isset($evento->ficha) && $evento->ficha->tipoevento->nombre == 'GOBIERNO DEL ESTADO' ? 'selected' : '' }}>GOBIERNO DEL ESTADO</option>
																<option value="SEÑORA IVETTE" {{ isset($evento->ficha) && $evento->ficha->tipoevento->nombre == 'SEÑORA IVETTE' ? 'selected' : '' }}>SEÑORA IVETTE</option>
                                                                <option value="DIRECCIÓN GENERAL" {{ isset($evento->ficha) && $evento->ficha->tipoevento->nombre == 'DIRECCION GENERAL' ? 'selected' : '' }}>DIRECCIÓN GENERAL</option> --}}
                                                            {{-- @if(isset($evento->ficha)) --}}
                                                            <option value=""></option>
                                                            <option value="FEDERAL" {{ isset($evento->ficha->tipoevento) && $evento->ficha->tipoevento->nombre == 'FEDERAL' ? 'selected' : '' }}>FEDERAL</option>
                                                            <option value="GOBIERNO DEL ESTADO" {{ isset($evento->ficha->tipoevento) && $evento->ficha->tipoevento->nombre == 'GOBIERNO DEL ESTADO' ? 'selected' : '' }}>GOBIERNO DEL ESTADO</option>
                                                            <option value="SEÑORA IVETTE" {{ isset($evento->ficha->tipoevento) && $evento->ficha->tipoevento->nombre == 'SEÑORA IVETTE' ? 'selected' : '' }}>SEÑORA IVETTE</option>
                                                            <option value="DIRECCIÓN GENERAL" {{ isset($evento->ficha->tipoevento) && $evento->ficha->tipoevento->nombre == 'DIRECCION GENERAL' ? 'selected' : '' }}>DIRECCIÓN GENERAL</option>
                                                            {{-- @endif --}}
														</select>                                                        
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
													<div class="form-group">
														<label for="vestimenta_id">Código de vestimenta* :</label>
                                                        <select id="vestimenta_id" class="form-control select2"  name="vestimenta_id" style="width: 100%;">
                                                            <option value=""></option>
                                							@foreach($vestimenta as $ropa)
																  <option value="{{ $ropa->id }}" {{ isset($evento->ficha) && $evento->ficha->vestimenta_id == $ropa->id ? 'selected' : '' }}>{{ $ropa->nombre . " (". $ropa->codigo . ")" }}</option>
															@endforeach
														</select>                                                        
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
													<div class="form-group">
														<label>Aforo* :</label>
														<div class="input-group">
															<input type="text" id="aforo" name="aforo" class="form-control" value="{{ isset($evento->ficha) ? $evento->ficha->aforo : '' }}">
															<span class="input-group-addon">personas</span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">
										<div class="box-body" style="padding-left: 0px; padding-right: 0px;">
											<div class="row">
												<div class="col-sm-6 col-md-3">
													<div class="form-group">
														<label for="temperatura">Temperatura* :</label>
														<div class="input-group">
															<input type="text" class="form-control" id="temperatura" name="temperatura" value="{{ isset($evento->ficha) ? $evento->ficha->temperatura : '' }}">
															<span class="input-group-addon">°C</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6 col-md-3">
													<div class="form-group">
														<label for="precipitacion">Precipitación* :</label>
														<div class="input-group">
															<input type="text" class="form-control" id="precipitacion" name="precipitacion" value="{{ isset($evento->ficha) ? $evento->ficha->precipitacion : '' }}">
															<span class="input-group-addon">%</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6 col-md-3">
													<div class="form-group">
														<label for="humedad">Humedad* :</label>
														<div class="input-group">
															<input type="text" class="form-control" id="humedad" name="humedad" value="{{ isset($evento->ficha) ? $evento->ficha->humedad : '' }}">
															<span class="input-group-addon">%</span>
														</div>
													</div>
												</div>
												<div class="col-sm-6 col-md-3">
													<div class="form-group">
														<label for="viento">Viento* :</label>
														<div class="input-group">
															<input type="text" class="form-control" id="viento" name="viento" value="{{ isset($evento->ficha) ? $evento->ficha->viento : '' }}">
															<span class="input-group-addon">Km/h</span>
														</div>
													</div>
												</div>

												<div class="col-sm-6 col-md-3">
													<div class="form-group">
														<label for="fila_honor">Fila de Honor* :</label>
														<select id="fila_honor" class="form-control select2"  name="fila_honor" style="width: 100%;">
															<option value="0" {{ isset($evento->ficha) && $evento->ficha->fila_honor == '0' ? 'selected' : '' }}>NO</option>
															<option value="1" {{ isset($evento->ficha) && $evento->ficha->fila_honor == '1' ? 'selected' : '' }}>SI</option>
														</select>
													</div>
												</div>

											</div>
										</div>
									</div>

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="form-group">
											<label for="antecedentes">Antecedentes del evento* :</label> 
											<textarea id="antecedentes" name="antecedentes" class="form-control" rows="5" aria-invalid="false">{{ isset($evento->ficha) ? $evento->ficha->antecedentes : ''  }}</textarea>
										</div>
                                    </div>

                                    {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left: 0px; padding-right: 0px;">
                                        <div class="form-group">
											<label for="anexos">Anexos del evento* :</label>
											<textarea id="anexos" name="anexos" class="form-control" rows="10" aria-invalid="false">{{ isset($evento->ficha) ? $evento->ficha->anexos : '' }}</textarea>
										</div>
                                    </div> --}}
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="box">
											<div class="box-header with-border">
												<h3 class="box-title">Objetivo y participaciones</h3>
											</div>
											<div class="box-body">
												<div class="row">
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
														<div class="form-group">
															<label for="objetivo">Objetivo del evento* :</label> 
															<textarea id="objetivo" name="objetivo" class="form-control" rows="5">{{ isset($evento->ficha) ? $evento->ficha->objetivo : ''  }}</textarea>
														</div>
													</div>
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
														<div class="form-group">
															<label for="participacion_presidenta">Participación de la Presidenta* :</label>
															<select id="participacion_presidenta" class="form-control select2" name="participacion_presidenta" style="width: 100%;">
                                                            @if(isset($evento->ficha->participacionpresidenta))
                                                                <option selected value="{{ $evento->ficha->participacionpresidenta->nombre }}">{{ $evento->ficha->participacionpresidenta->nombre }}</option>
                                                            @endif
															</select>
														</div>
													</div>
                                                    @if(Auth::user()->persona->empleado->area->id !== 119)
													<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
														<div class="form-group">
															<label for="participacion_director">Participación del Director General* :</label>
															<select id="participacion_director" class="form-control select2" name="participacion_director" style="width: 100%;">
                                                            @if(isset($evento->ficha->participaciondirector))
                                                                <option selected value="{{ $evento->ficha->participaciondirector->nombre }}">{{ $evento->ficha->participaciondirector->nombre }}</option>
                                                            @endif
															</select>
														</div>
													</div>
                                                    @endif
												</div>
												<hr>
												
												<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
													<div class="form-group">
														<label for="persona_convoca_id">Autoridad que convoca* :</label> 
														<div class="input-group">
															<select id="persona_convoca_id" name="persona_convoca_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
																@if( isset($evento->ficha) ) 
																	<option selected value="{{ $evento->ficha->personaconvoca->id }}">{{ $evento->ficha->personaconvoca->get_nombre_completo() }}</option>
																@endif
															</select>
															<div class="input-group-btn">
																<button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="Personas.abrir_modal('persona_convoca_id')">
																	<i class="fa fa-search"></i>
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
													<div class="form-group">
														<label for="cargo_persona_convoca">Cargo* :</label>
														<select id="cargo_persona_convoca" class="form-control select2" name="cargo_persona_convoca" style="width: 100%;">
															@if(isset($evento->ficha))                                            
																<option value="{{ $evento->ficha->cargopersonaconvoca->nombre }}">{{ $evento->ficha->cargopersonaconvoca->nombre }}</option>
															@endif                       
														</select>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
													<div class="form-group">
														<label for="persona_preside_id">Autoridad que preside* :</label> 
														<div class="input-group">
															<select id="persona_preside_id" name="persona_preside_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
																@if( isset($evento->ficha) ) 
																	<option selected value="{{ $evento->ficha->personapreside->id }}">{{ $evento->ficha->personapreside->get_nombre_completo() }}</option>
																@endif
															</select>
															<div class="input-group-btn">
																<button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="Personas.abrir_modal('persona_preside_id')">
																	<i class="fa fa-search"></i>
																</button>
															</div>
														</div>
													</div>
												</div>
												<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
													<div class="form-group">
														<label for="cargo_persona_preside">Cargo* :</label>
														<select id="cargo_persona_preside" class="form-control select2" name="cargo_persona_preside" style="width: 100%;">
															@if(isset($evento->ficha))                                            
																<option value="{{ $evento->ficha->cargopersonapreside->nombre }}">{{ $evento->ficha->cargopersonapreside->nombre }}</option>
															@endif                       
														</select>
													</div>
												</div>                                
											</div>
										</div>
									</div>
								</div>
							</form>                            
                        </div>
                        <div class="tab-pane" id="tab_asistentes">
                            <form role="form" id="form_asistente" onsubmit="return false;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="asistente_id">Asistente* :</label> 
                                            <div class="input-group">
                                                <select id="asistente_id" name="asistente_id" class="form-control input-persona" style="width: 100%;" readonly="readonly"></select>
                                                <div class="input-group-btn">
                                                    <button type="button" id="btnBuscarAsistente" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="Personas.abrir_modal('asistente_id');">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label for="cargo_asistente">Cargo* :</label>
                                            <select id="cargo_asistente" class="form-control select2" style="width: 100%;" name="cargo_asistente"></select>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                                        <div class="form-group">
                                            <label for="posicion">Posición* :</label>
                                            <input id="posicion" class="form-control" name="posicion">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-1 col-lg-1">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" id="btnAgregarAsistente" onclick="Asistente.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Lista de asistentes</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="asistentes" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nombre</th>
                                                        <th class="text-center">Cargo</th>
                                                        <th class="text-center">Posición en la fila de honor</th>
                                                        <th class="text-center">Editar</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_actividades">
                            <form role="form" id="form_actividad" onsubmit="return false;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="actividad">Actividad* :</label> 
                                            <input id="actividad" name="actividad" class="form-control select2"/>
                                        </div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                        <div class="bootstrap-timepicker"><div class="bootstrap-timepicker-widget dropdown-menu"><table><tbody><tr><td><a href="#" data-action="incrementHour"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="incrementMinute"><i class="glyphicon glyphicon-chevron-up"></i></a></td><td class="separator">&nbsp;</td><td class="meridian-column"><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-up"></i></a></td></tr><tr><td><span class="bootstrap-timepicker-hour"></span></td> <td class="separator">:</td><td><span class="bootstrap-timepicker-minute"></span></td> <td class="separator">&nbsp;</td><td><span class="bootstrap-timepicker-meridian"></span></td></tr><tr><td><a href="#" data-action="decrementHour"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator"></td><td><a href="#" data-action="decrementMinute"><i class="glyphicon glyphicon-chevron-down"></i></a></td><td class="separator">&nbsp;</td><td><a href="#" data-action="toggleMeridian"><i class="glyphicon glyphicon-chevron-down"></i></a></td></tr></tbody></table></div>
                            				<div class="form-group">
                                				<label>Hora* :</label>
                                				<div class="input-group">
                                    				<input type="text" class="form-control timepicker" id="horax" name="horax" value="">
                                    				<div class="input-group-addon">
                                        				<i class="fa fa-clock-o"></i>
                                    				</div>
                                				</div>
                            				</div>
                        				</div>
									</div>
									<div class="col-xs-4 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
											<label for="tiempo">Duración* :</label>
											<div class="input-group">
												<input id="tiempo" class="form-control" name="tiempo">
												<span class="input-group-addon">minutos</span>
											</div>
                                        </div>
									</div>
                                    <div class="col-xs-4 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" id="btnAgregarActividad" onclick="Actividad.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Programa de actividades</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="actividades" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Hora</th>
                                                        <th class="text-center">Actividad</th>
                                                        <th class="text-center">Duración (minutos)</th>
                                                        <th class="text-center">Responsables</th>
                                                        <th class="text-center">Editar</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_aportaciones">
                            <form role="form" id="form_aportacion" onsubmit="return false;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
										<div class="form-group">
											<label for="dependencia_id">Dependencia* :</label> 
											<div class="input-group">
												<select id="dependencia_id" name="dependencia_id" class="form-control select2" style="width: 100%;"></select></select>
												<div class="input-group-btn">
													<button type="button" class="btn btn-info" data-toggle="tooltip" title="Nueva dependencia" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-dependencia')">
														<i class="fa fa-plus"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label for="producto">Descripción del aporte* :</label> 
                                            <input type="text" id="producto" name="producto" class="form-control">
                                        </div>
									</div>									
									<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
											<label for="cantidad">Cantidad* :</label>
											<input type="text" id="cantidad" class="form-control" name="cantidad">
                                        </div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
											<label for="monto">Monto* :</label>
											<input type="text" id="monto" class="form-control" name="monto">
                                        </div>
									</div>									                               
                                    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" id="btnAgregarAportacion" onclick="Aportacion.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Lista de aportaciones</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="aportaciones" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Dependencia</th>
                                                        <th class="text-center">Aportación</th>
														<th class="text-center">Cantidad</th>
														<th class="text-center">Monto</th>
                                                        <th class="text-center">Editar</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="tab-pane" id="tab_autoridades">
							{{-- <form role="form" id="form_autoridad" onsubmit="return false;">
                                <div class="row">
									<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                                        <div class="form-group">
                                            <label for="nombre_autoridad">Nombre* :</label>
                                            <input id="nombre_autoridad" class="form-control" name="nombre_autoridad">
                                        </div>
									</div>
                                    <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                                        <div class="form-group">
											<label for="cargo_autoridad">Cargo* :</label>
											<select id="cargo_autoridad" name="cargo_autoridad" class="form-control select2" style="width: 100%;"></select>
                                        </div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                                        <div class="form-group">
											<label for="partidopolitico_id">Partido político* :</label>
											<select id="partidopolitico_id" name="partidopolitico_id" class="form-control select2" style="width: 100%;"></select>
                                        </div>
                                    </div>       
                                    <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" id="btnAgregarAutoridad" onclick="Autoridad.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
                            </form> --}}
                            {{-- <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Lista de autoridades municipales</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="autoridades" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nombre</th>
                                                        <th class="text-center">Cargo</th>
                                                        <th class="text-center">Partido político</th>
                                                        <th class="text-center">Editar</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}
														
							<form role="form" id="form_autoridad" onsubmit="return false;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                        <div class="form-group">
                                            <label for="presidentemunicipal_id">Autoridades municipales* :</label>
                                            <div class="input-group">
                                                <select id="presidentemunicipal_id" name="presidentemunicipal_id" class="form-control input-persona" style="width: 100%;" readonly="readonly"></select>
                                                <div class="input-group-btn">
                                                    <button type="button" id="municipalesSearchButton" class="btn btn-info" data-toggle="tooltip" title="" style="padding-left: 25px; padding-right: 25px;" onclick="$('#' + 'municipales_modal').modal('show');">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" onclick="Autoridad.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
							</form>
                            <div class="row">
                                <div class="col-xs-4 col-sm-6 col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <label for="" style="color:white">Cargar Presidentes Municipales:</label>
                                        <button type="button" class="btn btn-block btn-info" onclick="Autoridad.cargarListado();">Precargar</button>
                                    </div>
                                </div>
                            </div>

							<div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Lista de autoridades municipales</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="autoridades" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nombre</th>
                                                        <th class="text-center">Primer Apellido</th>
                                                        <th class="text-center">Segundo Apellido</th>
                                                        <th class="text-center">Cargo</th>
                                                        <th class="text-center">Partido Político</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>

                        <div class="tab-pane" id="tab_diputados">
                            <form role="form" id="form_diputado" onsubmit="return false;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                        <div class="form-group">
                                            <label for="diputadolocal_id">Diputados* :</label>
                                            <div class="input-group">
                                                <select id="diputadolocal_id" name="diputadolocal_id" class="form-control input-persona" style="width: 100%;" readonly="readonly"></select>
                                                <div class="input-group-btn">
                                                    <button type="button" id="municipalesSearchButton" class="btn btn-info" data-toggle="tooltip" title="" style="padding-left: 25px; padding-right: 25px;" onclick="$('#' + 'diputados_modal').modal('show');">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                        <div class="form-group">
                                            <label for="" style="color:white">Agregar:</label>
                                            <button type="button" class="btn btn-block btn-info" onclick="Diputado.guardar()">Agregar</button>
                                        </div>
                                    </div>
                                </div>
							</form>
                            <div class="row">
                                <div class="col-xs-4 col-sm-6 col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <label for="" style="color:white">Cargar Diputados:</label>
                                        <button type="button" class="btn btn-block btn-info" onclick="Diputado.cargarListado();">Precargar</button>
                                    </div>
                                </div>
                            </div>

							<div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <div class="box box-primary">
                                        <div class="box-header with-border">
                                            <h4 class="box-title">Lista de diputados</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="diputados" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nombre</th>
                                                        <th class="text-center">Primer Apellido</th>
                                                        <th class="text-center">Segundo Apellido</th>
                                                        <th class="text-center">Cargo</th>
                                                        <th class="text-center">Partido Político</th>
                                                        <th class="text-center">Eliminar</th>
                                                    </tr>   
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                Log:
                <br>
                @foreach($bitacora as $r)
                    <strong>Usuario:</strong> {{ $r->usuario_red }}
                    <strong>Tabla:</strong> {{ $r->tabla }}
                    <strong>Accion:</strong> {{ $r->metodo }}
                    <strong>Fecha:</strong> {{ $r->created_at }}
                    <br>
                @endforeach
            </div>
        </div>
	</div>
	
<div class="modal fade" id="modal-personas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-personas">Tabla personas:</h4>
            </div>
            <div class="modal-body" id="modal-body-personas">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_personas" name="search_personas" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="btn_buscar_personas" name="btn_buscar_personas">Buscar</button>
                    </span>
                </div>
                {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer" id="modal-footer-personas">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="municipales_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="municipales_modal_title">Tabla Autoridades Municipales:</h4>
            </div>
            <div class="modal-body" id="municipales_modal_body">
                {{-- <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="municipales_search_input" name="municipales_search_input" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="municipales_search_button" name="municipales_search_button">Buscar</button>
                    </span>
                </div> --}}
                {!! $municipales->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'municipales', 'name' => 'municipales', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer" id="municipales_modal_footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="diputados_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="diputados_modal_title">Tabla Diputados:</h4>
            </div>
            <div class="modal-body" id="diputados_modal_body">
                {{-- <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="diputados_search_input" name="diputados_search_input" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="diputados_search_button" name="diputados_search_button">Buscar</button>
                    </span>
                </div> --}}
                {!! $autoridaddiputados->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'autoridaddiputados', 'name' => 'autoridaddiputados', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer" id="diputados_modal_footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-persona">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-persona')">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-persona"></h4>
            </div>
            <div class="modal-body" id="modal-body-persona"></div>
            <div class="modal-footer" id="modal-footer-persona">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-responsables">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-responsables')">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-responsable"></h4>
            </div>
            <div class="modal-body">
				<div class="row">
					<form role="form" id="form_responsable" onsubmit="return false;">
						<div class="row">                                    
							<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
								<div class="form-group">
									<label for="nombre_responsable">Responsable* :</label>
									<input id="nombre_responsable" class="form-control" name="nombre_responsable">
								</div>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
								<div class="form-group">
									<label for="cargo_responsable">Cargo* :</label>
									<input id="cargo_responsable" class="form-control" name="cargo_responsable">
								</div>
							</div>
							<div class="col-xs-4 col-sm-6 col-md-2 col-lg-2">
								<div class="form-group">
									<label for="" style="color:white">Agregar:</label>
									<button type="button" class="btn btn-block btn-info" id="btnAgregarResponsable" onclick="Actividad.guardarResponsable()">Agregar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h4 class="box-title">Responsables</h4>
							</div>
							<div class="box-body">
								<table id="responsables" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
									<thead>
										<tr>
											<th class="text-center">Nombre</th>
											<th class="text-center">Cargo</th>
											<th class="text-center">Editar</th>
											<th class="text-center">Eliminar</th>
										</tr>   
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="modal-footer">
                <div class="pull-right">                    
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dependencia">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-dependencia')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-dependencia">Registrar dependencia o institución</h4>
            </div>
            <div class="modal-body" id="modal-body-dependencia"></div>
            <div class="modal-footer" id="modal-footer-dependencia">
                <button type="button" class="btn btn-success" onclick="dependencia.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
            </div>      
        </div>
    </div>
</div>

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
{!! $personas->html()->scripts() !!}
{!! $municipales->html()->scripts() !!}
{!! $autoridaddiputados->html()->scripts() !!}
@include('agendaeventos::ficha.js.personas')
@include('dependencias.js.dependencia')
@include('agendaeventos::ficha.js.dependencias')
@include('personas.js.persona')
@include('agendaeventos::eventos.js.app')
@include('agendaeventos::ficha.js.ficha')
@include('agendaeventos::ficha.js.asistente')
@include('agendaeventos::ficha.js.aportacion')
@include('agendaeventos::ficha.js.actividad')
@include('agendaeventos::ficha.js.autoridad')
@include('agendaeventos::ficha.js.diputados')
<script type="text/javascript">
    var fecha = '{{ $evento->fecha }}';//console.log(fecha);
    var getDarkSky = () => {
        //var dateString = moment.unix(1562130000).format('YYYY/MM/DD');
        block();

        $.ajax({
            url: 'https://cors-anywhere.herokuapp.com/' + 'https://api.darksky.net/forecast/9dace7724a7e2fcb80736915fa7da30b/' + '{{ $evento->municipio->latitud }},{{ $evento->municipio->longitud }}' + '?lang=es&units=si',
            type: 'GET',
            success: (response) => {
                var daily = response.daily;
                for(var i = 0; i < daily.data.length; i++) {
                    //var hoy = moment().format('YYYY-MM-DD');
                    //var fecha_ficha = moment('2019/12/08').format('YYYY-MM-DD');
                    fecha_ficha = moment('2019/07/12', 'YYYY-MM-DD');
                    var fecha = moment.unix(daily.data[i].time, 'YYYY-MM-DD');

                    if(fecha_ficha.isBefore(fecha)) {
                        $('#temperatura').val(daily.data[0].temperatureLow);
                        $('#precipitacion').val(daily.data[0].precipIntensity);
                        $('#humedad').val(daily.data[0].humidity);
                        $('#viento').val(daily.data[0].windSpeed);
                        unblock();
                        //console.log('Antes.');
                        return;
                    }

                    if(fecha_ficha.isSame(fecha)) {
                        $('#temperatura').val(daily.data[i].temperatureLow);
                        $('#precipitacion').val(daily.data[i].precipIntensity);
                        $('#humedad').val(daily.data[i].humidity);
                        $('#viento').val(daily.data[i].windSpeed);
                        unblock();
                        //console.log('Igual.');
                        return;
                    }

                    if(fecha_ficha.isAfter(fecha) && i === (daily.data.length - 1)) {
                        $('#temperatura').val(daily.data[i].temperatureLow);
                        $('#precipitacion').val(daily.data[i].precipIntensity);
                        $('#humedad').val(daily.data[i].humidity);
                        $('#viento').val(daily.data[i].windSpeed);
                        unblock();
                        //console.log('Despues.');
                        return;
                    }
                }
                
                unblock();
            },
            error: (response) => {
                unblock();
            }
        });
    }

    var seleccionar_autoridad_municipal = (id, nombreAutoridadMunicipal) => {
        $('#presidentemunicipal_id').html("<option value='" + id + "'selected>" + nombreAutoridadMunicipal + "</option>");
        $('#municipales_modal').modal('hide');
    }

    var seleccionar_autoridad_diputado = (id, nombreAutoridadDiputado) => {
        $('#diputadolocal_id').html("<option value='" + id + "'selected>" + nombreAutoridadDiputado + "</option>");
        $('#diputados_modal').modal('hide');
    }

	var cerrar_modal = (modal) => {
        $('#' + modal).modal('hide');
    }

	var seleccionar_persona = (id, nombre) => {
		Personas.seleccionar(id, nombre);
	}

	var abrir_modal = () => {
		Dependencias.agregar();
	}

	var agregar_persona = () => {
		Personas.agregar();
	}

	var persona_create_edit_error = (response) => {
		Personas.create_edit_error(response);
	}

	var persona_create_edit_success = (response) => {
		Personas.seleccionar(response.id, response.nombre);
	}

	var dependencia_create_edit_success = (response) => {
		Dependencias.seleccionar(response.id, response.nombre);
	}

	var dependencia_create_edit_error = (response) => {
		Dependencias.create_edit_error(response);
	}

	$(document).ready(() => {
        //CKEDITOR.replace('anexos');
		app.to_upper_case();
		Personas.inicializar();
		Ficha.inicializar();
    	Asistente.inicializar();
		Aportacion.inicializar();
        Actividad.inicializar();
		Autoridad.inicializar();
        Diputado.inicializar();
    	@if(isset($evento->ficha))
      		Asistente.getAsistentes({{ $evento->ficha->id }});
			Aportacion.getAportaciones({{ $evento->ficha->id }});
			Actividad.getActividades({{ $evento->ficha->id }});
            Autoridad.getAutoridades({{ $evento->ficha->id }});
            Diputado.getAutoridades({{ $evento->ficha->id }});
    	@endif		
    });
</script>
@endpush