<script>
  	var Asistente = (() => {

		var id = undefined;
		var asistentes = undefined;

		var inicializar = () => {

			$('.timepicker').timepicker({
                showInputs: false,
                defaultTime: false
            });		

			$('#cargo_asistente').select2({
                language: 'es',
                tags: true,
                placeholder : 'SELECCIONE O INGRESE EL CARGO',
                ajax: {
                    url: "{{ route('cargos.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.nombre,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });		

			$('#form_asistente').validate({
                rules: {
                    asistente_id: {
                        required: true
                    },
                    cargo_asistente: {
                        required: true
                    },
                    posicion: {
                        required: true,
						pattern_integer: ''
                    }
				}
			});
		}

		var guardar = () => {
			if($('#form_asistente').valid()) {
				block();
				var url = id ? '{{ route("agenda.asistentes.update", ":asistente_id") }}' : '{{ route("agenda.asistentes.store")}}'; 
				url = id ? url.replace(':asistente_id', id) : url;
				var type = id ? 'PUT' : 'POST';
				var data = $('#form_asistente').serialize() + '&fichainfo_id=' + Ficha.getId();
				var title = id ? 'Asistente actualizado' : 'Asistente registrado';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getAsistentes(Ficha.getId());
							$('#btnBuscarAsistente').removeAttr('disabled');
							$('#btnAgregarAsistente').text("Agregar");
                        	$('#btnAgregarAsistente').css('background-color','##00c0ef');
                        	$('#btnAgregarAsistente').css('border-color', '##00c0ef');
              				$('#asistente_id, #cargo_asistente').val(null).trigger('change');
              				$('#posicion').val('');
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else{
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var getAsistentes = (fichainfo_id) => {
            if ($.fn.DataTable.isDataTable( '#asistentes' ) ) {
                asistentes.ajax.url( '/agenda/asistentes?fichainfo_id=' + fichainfo_id ).load();
		    }else{
                asistentes = $("#asistentes").DataTable({
                    ajax: {
                        url: '/agenda/asistentes?fichainfo_id=' + fichainfo_id,
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "nombre" },
                        { "data": "cargo" },
						{ "data": "posicion" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Asistente.editar(" + data.id + ', ' + data.persona_id + ", \"" + data.nombre + "\", \"" + data.cargo + "\", " + data.posicion + ")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Asistente.eliminar(' + data.id + ', \'' + data.nombre + '\'' + ')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3,4] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
        }

		var editar = (asistente_id, persona_id, nombre, cargo, posicion) => {
            id = asistente_id;
            var personaOpt = new Option(nombre, persona_id, true, true);
            $('#asistente_id').append(personaOpt).trigger('change');
            $('#btnBuscarAsistente').attr('disabled', 'disabled');
			var cargoOpt = new Option(cargo, cargo, true, true);
            $('#cargo_asistente').append(cargoOpt).trigger('change');
			$('#posicion').val(posicion);
            $('#btnAgregarAsistente').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
            $('#asistente_id, #cargo_asistente').valid();
        }

		var eliminar = (asistente_id, nombre) => {
			swal({
				title: '¿Desea eliminar al asistente?',
				html: nombre,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/asistentes/' + asistente_id;
					app.ajaxHelper( url, 'DELETE').done( (response) => {
						unblock();
						if(!response.success){
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}else{							
							asistentes.ajax.reload(null, false);
							id = undefined;
							$('#btnBuscarAsistente').removeAttr('disabled');
							$('#btnAgregarAsistente').text("Agregar");
                        	$('#btnAgregarAsistente').css('background-color','##00c0ef');
                        	$('#btnAgregarAsistente').css('border-color', '##00c0ef');
              				$('#asistente_id, #cargo_asistente').val(null).trigger('change');
              				$('#posicion').val('');
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
        }

		return {
			inicializar,
			guardar,
			editar,
			eliminar,
			getAsistentes
		}
	})();
</script>