<script>
  	var Diputado = (() => {

		var id = undefined;
		var autoridades = undefined;

		var inicializar = () => {
			$('#form_diputado').validate({
				rules: {
					diputadolocal_id: {
						required: true
					}
				}
			});
		}

		var guardar = () => {
			if($('#form_diputado').valid()) {
				block();
				var url = '{{ route("agenda.diputados.store")}}';
				var type = 'POST';
				var data = $('#form_diputado').serialize() + '&fichainfo_id=' + Ficha.getId();
				var title = 'Diputad(o)a registrada.';

				$.ajax({
					url: url,
					type: type,
					data: data,
					success: (response) => {
						unblock();		
						if(response.success) {
							id = undefined;
							getAutoridades(Ficha.getId());
							$('#diputadolocal_id').empty();
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}else{
							swal(
								'Error',
								response.message,
								'error'
							)
						}                    
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'Revise los datos',
							'error'
						)
					}
				});
			}
		}

		var getAutoridades = (fichainfo_id) => {
				if($.fn.DataTable.isDataTable('#diputados')) {
					autoridades.ajax.url('/agenda/diputados?fichainfo_id=' + fichainfo_id).load();
		    }
				else {
                autoridades = $("#diputados").DataTable({
                    ajax: {
                        url: '/agenda/diputados?fichainfo_id=' + fichainfo_id,
                        dataSrc: ''
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { 'data': 'nombre' },
												{ 'data': 'primer_apellido' },
												{ 'data': 'segundo_apellido' },
                        { 'data': 'cargo' },
						            { 'data': 'siglas' },
                        /*{
                            data: null,
                            render: ( data, type, row ) => {
															return "<button type='button' onclick='Autoridad.editar(" + data.id + ', ' + data.partido_id + ", \"" + data.partido + "\", \"" + data.cargo + "\", \"" + data.nombre + "\")' class='btn btn-xs btn-warning'>Editar</button>";
														}
												},*/
                        {
                            data: null,
                            render: (data, type, row) => {
															return '<button type="button" onclick="Diputado.eliminar(' + data.id + ', \'' + data.nombre + '\', \'' + data.primer_apellido + '\', \'' + data.segundo_apellido + '\')" class="btn btn-xs btn-danger">Eliminar</button>';
														}
                        }
                    ],
                    columnDefs: [
                        { className: 'text-center', targets: [2,3,4] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
        }
    }

		var editar = (autoridad_id, partido_id, partido, cargo, nombre) => {
    }

		var eliminar = (autoridad_id, nombre, primer_apellido, segundo_apellido) => {
			swal({
				title: '¿Desea eliminar a la autoridad?',
				html: nombre + ' ' + primer_apellido + ' ' + segundo_apellido,
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, eliminar',
				cancelButtonText: 'No, regresar',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					var url = '/agenda/diputados/' + autoridad_id;
					app.ajaxHelper(url, 'DELETE').done((response) => {
						unblock();
						if(!response.success) {
							swal(
								'¡Error!',
								response.message,
								'error'
							);
						}
						else {
							autoridades.ajax.reload(null, false);
							id = undefined;
							swal({
								title: response.message,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
					});
				}
			});
    }

		var cargarListado = () => {
			block();
				var url = '{{ route("agenda.cargardiputados")}}' + '?fichainfo_id=' + Ficha.getId();
				var type = 'POST';
				var title = 'Diputados precargados.';

				$.ajax({
					url: url,
					type: type,
					success: (response) => {
						unblock();		
						if(response.success) {
							autoridades.ajax.reload(null, false);
							swal({
								title: title,
								type: 'success',
								timer: 3000,
								toast : true,
								position : 'top-end',
								showConfirmButton: false
							});
						}
						else {
							swal(
								'Error',
								response.message,
								'error'
							)
						}
					},
					error: (response) => {
						unblock();
						swal(
							'Error',
							'',
							'error'
						)
					}
				});
		}

		return {
			inicializar,
			guardar,
			editar,
			eliminar,
			getAutoridades,
			cargarListado
		}
	})();
</script>