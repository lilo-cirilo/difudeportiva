<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\AgendaEventos\Entities\Base;

class CalendarioPersona extends Base {
    protected $table = 'even_calendarios_personas';

    protected $fillable = ['calendario_id', 'persona_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'kind', 'etag', 'ruleid', 'scope_type', 'scope_value', 'role', 'correo', 'usuario_id'];
}
