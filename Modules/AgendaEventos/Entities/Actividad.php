<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Actividad extends Model {
    protected $table = 'even_actividades';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fichainfo_id","hora","actividad","tiempo","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeActividades($query) {
        $query->select([
					'even_actividades.id as id',
					'even_actividades.fichainfo_id as fichainfo_id',
					DB::raw('TIME_FORMAT(even_actividades.hora, "%H:%i") as hora'),
					DB::raw('TIME_FORMAT(even_actividades.hora, "%h:%i %p") as horax'),
					'even_actividades.tiempo as duracion',
					'even_actividades.actividad as actividad'					
            	]);
	}
	
	public function scopeResponsables($query) {
        $query->join('even_actividades_responsables','even_actividades_responsables.actividad_id','even_actividades.id')
        ->join('even_responsables','even_responsables.id','even_actividades_responsables.responsable_id')
        ->whereNull('even_actividades_responsables.deleted_at')
				->select([
					'even_actividades.id as id',
					'even_actividades_responsables.id as actividad_responsable_id',
					'even_responsables.nombre as responsable',
					'even_responsables.cargo as cargo'
            	]);
    }

    public function ficha() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Ficha', 'fichainfo_id');        
    }

    public function actividadresponsables() {
        return $this->hasMany('Modules\AgendaEventos\Entities\ActividadResponsables','actividad_id');
	}
}