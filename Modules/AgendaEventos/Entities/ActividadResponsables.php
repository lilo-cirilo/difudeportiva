<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActividadResponsables extends Model {
    protected $table = 'even_actividades_responsables';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["actividad_id","responsable_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function actividad() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Actividad');        
    }

    public function responsable() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Responsable');        
    }

}