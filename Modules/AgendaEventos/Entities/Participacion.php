<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Participacion extends Model {
    protected $table = 'even_cat_participaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}