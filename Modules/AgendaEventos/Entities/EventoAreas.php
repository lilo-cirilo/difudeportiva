<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventoAreas extends Model {
    protected $table = "even_areas_eventos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["area_id","evento_id","genera_ficha","observaciones","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query){
        $query->join('cat_areas','cat_areas.id','even_areas_eventos.area_id')          
                ->select([
                    'cat_areas.id as area_id',
                    'cat_areas.nombre as area',
                    'even_areas_eventos.genera_ficha as genera_ficha',
                    'even_areas_eventos.observaciones as observaciones'
                ]);
    }

    public function area(){
    	return $this->belongsTo('App\Models\Area','area_id','id');
    }

    public function evento(){
    	return $this->belongsTo('Modules\AgendaEventos\Evento','evento_id','id');
    }
}

