<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Estado extends Model {
    use SoftDeletes;

    protected $table = 'cat_estados';

    protected $fillable = [];
}