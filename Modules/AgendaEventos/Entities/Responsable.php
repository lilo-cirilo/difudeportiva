<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Responsable extends Model {
    protected $table = 'even_responsables';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","cargo","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}