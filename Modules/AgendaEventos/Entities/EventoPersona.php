<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\AgendaEventos\Entities\Base;

class EventoPersona extends Base {
    protected $table = 'even_eventos_personas';

    protected $fillable = ['evento_id', 'persona_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'correo', 'usuario_id'];
}