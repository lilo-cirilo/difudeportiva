<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

class SqlPeticion extends Model {
    protected $connection = 'sqlsrv';

    protected $table = 'BdSICOPE.dbo.v_Peticiones as p';

    protected $fillable = [];
}