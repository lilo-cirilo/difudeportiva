<?php

namespace Modules\AgendaEventos\Entities;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class AutoridadDiputado extends Model {
    use SoftDeletes;

    protected $table = 'even_autoridadesdipu';

    protected $fillable = ['fichainfo_id', 'diputadolocal_id', 'usuario_id'];

    public function diputado() {
        return $this->belongsTo('Modules\AgendaEventos\Entities\Diputado','diputadolocal_id');
    }
}