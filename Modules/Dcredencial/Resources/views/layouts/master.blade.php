@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@section('breadcrumbs')
@endsection

@section('logo', asset('images/Bienestar.png'))
@section('mini-logo', asset('images/bienestarIcono.png'))

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
	<li class="header active">Programa Credencializacion</li>
	<li {{ ($ruta === 'home') ? 'class=active' : '' }}>
		<a href="{{ route('home') }}">
			<i class="fa fa-home"></i><span>IntraDIF</span>
		</a>
  </li>    
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
		<ul class="treeview-menu">
			<li><a href="{{ route('dcredencial.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
			<li><a href="{{ route('dcredencial.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
		</ul>
	</li>
	<li {{ ($ruta === 'dcredencial.inicio') ? 'class=active' : '' }}>
		<a href="{{ route('dcredencial.inicio') }}">
			<i class="fa fa-dashboard"></i><span>Dashboard</span>
		</a>
	</li>
	<li {{ ($ruta === 'dcredencial.home') ? 'class=active' : '' }}>
		<a href="{{ route('dcredencial.home') }}">
			<i class="fa fa-list-alt"></i><span>Lista</span>
		</a>
	</li>
	<li {{ ($ruta === 'dsolicitud') ? 'class=active' : '' }}>
		<a href="{{ route('dsolicitud') }}">
			<i class="fa fa-list"></i><span>Solicitudes</span>
		</a>
	</li>
</ul>
@endsection
@section('styles')
<!-- Bootstrap 3.3.7 -->
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<!-- Ionicons -->
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<!-- daterange picker -->
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<!-- bootstrap datepicker -->
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- iCheck for checkboxes and radio inputs -->
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap Color Picker -->
<link href="{{ asset('bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Bootstrap time Picker -->
<link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" type="text/css" rel="stylesheet">
<!-- Select2 -->
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<!-- Theme style -->
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">


@stop

@if(auth()->check())
@section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
@section('user-name', auth()->user()->persona->nombre)
@section('user-job')
@section('user-log', auth()->user()->created_at)
@endif

@section('content-title')
Credencializacion
@endsection

@section('scripts')
<!-- jQuery 3 -->
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Select2 -->
<script type="text/javascript" src="{{ asset('bower_components/select2/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<!-- InputMask -->
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- date-range-picker -->
<script type="text/javascript" src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- bootstrap datepicker locales -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<!-- bootstrap color picker -->
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
<!-- bootstrap time picker -->
<script type="text/javascript" src="{{ asset('plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('dist/js/adminlte.min.js') }}"></script>
<!-- blockui -->
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<!-- axios -->
<script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>
<!-- vue -->
<script type="text/javascript" src="{{ asset('bower_components/vue/vue.js') }}"></script>

<script type="text/javascript">
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		//setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}

	$('.modal').modal({
		keyboard: false,
		backdrop: 'static'
	});

	$('.modal').modal('hide');
	
</script>
@stop