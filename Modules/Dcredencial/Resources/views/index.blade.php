  @extends('dcredencial::layouts.master')

@section('content-subtitle')
Lista de credenciales
@endsection

@push('head')

<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<style>
	.modal-XL{
			width: 75%;
			margin: 15px auto;
	}
	[v-cloak] {display: none;}
</style>

<div id="dCredencial">
	<div v-cloak>
		<div class="box box-primary shadow">
			<div class="box-header with-border">
					<h3 class="box-title">Lista de beneficiarios</h3>
					<a href="{{ route('nuevaCred') }}" class="btn btn-success pull-right" @click="modal(), editMode=true, solicitudMode=false"><i class="fa  fa-user-plus"></i> Nuevo</a>
			</div>
			<div class="box-body">		
				<div class="box-body no-padding">
					{{-- NUEVA TABLA  --}}
				<v-client-table ref="listaPersonas" :data="listaPersonas" :columns="columns" :options="options">
						<div slot="beforeFilter" >
							{{-- <button class="dt-button buttons-reset button-dt tool" data-toggle="modal" data-target="#modal" @click.prevent="participante = false, evento= false, resetNewEvento(), editMode=false" >
								<span> <i class="fa fa-plus"></i> </span>
							</button> --}}
							<button class="btn btn-success" type="button"  @click="GenerarExcel()">
									<i class="fa fa-file-excel-o"></i>
									Exportar a Excel
							</button>
						</div>
						<template slot="opciones" slot-scope="props">
							<button title="Detalle" class="btn btn-info btn-sm" @click.prevent="showPerson(props.row)"> <i class="fa fa-eye"></i></button>
							<button title="Eliminar" class="btn btn-danger btn-sm" @click.prevent="eliminar(props.row)"> <i class="fa fa-trash"></i></button>
						</template>
					</v-client-table>
						{{-- FIN NUEVA TABLA --}}
				</div>
			</div>
		</div>   
		<div class="box box-danger" v-if="personaDatos">
			<div class="box-header with-border">
				<h3 class="box-title"><i class="fa fa-user"></i> Nombre : @{{` ${persona.nombre} ${persona.primer_apellido} ${persona.segundo_apellido}`}}</h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" @click.prevent="personaDatos = false"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-2">
						<p><b>CURP : </b>  @{{persona.curp}}</p>
					</div>
					<div class="col-xs-2">
						<p><i class="fa fa-calendar-check-o"></i> <b>Fecha Nacimiento : </b>  @{{persona.fecha_nacimiento}}</p>
					</div>
					<div class="col-xs-1">
						<p><i class="fa fa-venus-double"></i> <b>Genero : </b>  @{{persona.genero}}</p>
					</div>
					<div class="col-xs-2">
						<p><i class="fa fa-envelope-o"></i> <b>Correo : </b>  @{{persona.email}}</p>
					</div>
					<div class="col-xs-2">
						<p><i class="fa fa-mobile-phone"></i> <b>Celular : </b>  @{{persona.numero_celular}}</p>
					</div>
					<div class="col-xs-2">
						<p><i class="fa fa-phone"></i> <b>Telefono : </b>  @{{persona.numero_local}}</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3><i class="fa fa-street-view"></i> Direccion</h3>
					</div>
					<div class="col-xs-3">
						<p><i class="fa fa-map-pin"></i> <b>Municipio : </b>  @{{persona.municipio.nombre}}</p>
					</div>
					<div class="col-xs-3">
						<p><i class="fa fa-map-pin"></i> <b>Localidad : </b>  @{{persona.localidad.nombre}}</p>
					</div>
					<div class="col-xs-3">
						<p><i class="fa fa-map-signs"></i> <b>Calle : </b>  @{{persona.calle}}</p>
					</div>
					<div class="col-xs-3">
						<p><i class="fa fa-houzz"></i> <b>Colonia : </b>  @{{persona.colonia}}</p>
					</div>
					<div class="col-md-12"><br></div>
					<div class="col-xs-2">
						<p><i class="fa fa-arrow-circle-left"></i> <b>No. Interior : </b>  @{{persona.numero_interior}}</p>
					</div>
					<div class="col-xs-2">
						<p><i class="fa fa-arrow-circle-right"></i> <b>No. Exterior : </b>  @{{persona.numero_exterior}}</p>
					</div>
					<div class="col-xs-3">
						<p><i class="fa fa-toggle-up"></i> <b>Codigo Postal : </b>  @{{persona.codigopostal}}</p>
					</div>
					<div class="col-xs-5">
						<p><i class="fa fa-toggle-up"></i> <b>Referencia : </b>  @{{persona.referencia_domicilio}}</p>
					</div>
					<div class="col-md-12">
						<h3><i class="fa fa-user-secret"></i> Datos de contacto en caso de emergencia</h3>
					</div>
					<div class="col-xs-4">
						<p><i class="fa fa-user"></i> <b>Avisar : </b>  @{{`${persona.dtutor.tutorado.nombre} ${persona.dtutor.tutorado.primer_apellido} ${persona.dtutor.tutorado.segundo_apellido}`}}</p>
					</div>
					<div class="col-xs-8">
						<p><i class="fa fa-user"></i> <b>Direccion : </b>  @{{`${persona.dtutor.tutorado.municipio.nombre}, ${persona.dtutor.tutorado.localidad.nombre}, ${persona.dtutor.tutorado.calle}, ${persona.dtutor.tutorado.colonia}, ${persona.dtutor.tutorado.codigopostal}`}}</p>
					</div>
					<div class="col-md-6" v-if="persona.fotografia != null">
						<p><b>Fotografia</b></p>
						<img :src="formatRuta(persona.fotografia)" alt="Fotografia" width="30%">
					</div>
					<div class="col-md-6" v-if="persona.certificado != null">
						<p><b>Certificado</b></p>
						<img :src="formatRuta(persona.certificado.ruta_imagen)" alt="Certificado" width="50%">
					</div>
				
				</div>
			</div>
		</div>
	</div>       
</div>

@stop

@push('body')

<script type="text/javascript" src="{{ asset('plugins/vue/vue-tables-2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	Vue.use(VueTables.ClientTable);
	// Vue.use(VeeValidate);
	var credencial = new Vue ({
		el:'#dCredencial',
			data:{
				listaPersonas : [],
				columns: ['nombre','primerApellido','segundoApellido','fechaNacimiento','curp','opciones'],
				options: {
					filterable: ["folio", "nombre", "fecha","estatus","primerApellido","segundoApellido","fechaNacimiento","curp"],
					texts: {
						count:
							" {from} a {to} de {count} registros|{count} Registros|1 Registro",
						first: "First",
						last: "Last",
						filter: "Filtrar:",
						filterPlaceholder: "Buscar",
						limit: "Registros:",
						page: "Pagina:",
						noResults: "No se encontraron coincidencias",
						filterBy: "Filtrar",
						loading: "Cargando...",
						defaultOption: "Seleccionar {column}",
						columns: "Columnas"
					},
					highlightMatches: true,
					filterByColumn: true
				},
				persona:{municipio:{},localidad:{},dtutor:{tuturado:{localidad:{},municipio:{}}}},
				personaDatos:false
			},
			methods:{
				listaPCredenciales:function(){
					block();
					let me = this
					axios.get("dcredencial/list").then(r => {
						unblock();
						me.listaPersonas    = r.data.lista
					}).catch( error =>{
						unblock();
						swal({position: 'top-end',type: 'error', title: 'Error al consultar los datos',	showConfirmButton: false,	timer: 1700})			
					})
				},
				GenerarExcel() {
					block();
					var data = {};
					data.titulo ="Reporte de Beneficiarios",
    			data.subtitulo= "Lista de Personas Registradas",
					data.cabeceras=Object.keys(this.$refs.listaPersonas.allFilteredData[0]).map(e=>e.replace("_"," ")).map(e=>e.toUpperCase());
					data.datos=this.$refs.listaPersonas.allFilteredData.map(e=>Object.values(e));
					jsreport.serverUrl = 'http://187.157.97.110:3001';
					var request = {
						template:{
								shortid: "HJ5VttAyS"
						},
						data: data
					};
					jsreport.renderAsync(request).then(function(res) {
						res.download(`REPORTE.xlsx`)
						unblock();
					});
				},
				eliminar(row){
					let me = this
					var url = "dcredencial/destroy/"+row.id
					swal({
						title: 'Estas seguro de eliminar',
						text: `${row.nombre} ${row.primerApellido} ${row.segundoApellido} del Programa Credencializacion`,
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Si'
					}).then((result) => {
						if (result.value) {
							axios.delete(url).then(response =>{
								swal('Eliminado!','Se elimino correctamente.','success')
								me.listaPCredenciales();
							}).catch(fail =>{
								swal('Error!','Ocurrio un error al eliminar.','warning')
							})
						}
					})
				},
				showPerson(row){
					block();
					let me = this;
					me.persona.fotografia = null;
					me.persona.certificado = null;
					axios.get(`dcredencial/getPerson/${row.id}`).then(response => {
						unblock();
						if(response.data.success){
							me.persona = response.data.persona
							me.personaDatos = true;
						}
						else
							swal({type: 'error',title: 'Oops...',	text: 'Ocurrio un error!',footer: 'Contacte al Administrador',showConfirmButton: false,timer: 1700})
						
					}).catch(error => {
						unblock();
						swal({
							type: 'error',
							title: 'Oops...',
							text: 'Ocurrio un error!',
							footer: 'Contacte al Administrador',
							showConfirmButton: false,
							timer: 1700
						})
					})          
				},
				formatRuta(ruta){
					return ruta.replace("public", "storage");
				},
			},
			mounted() {
				this.listaPCredenciales();
			},
	})
</script>

@endpush

