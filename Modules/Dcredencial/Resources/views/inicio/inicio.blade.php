@extends('dcredencial::layouts.master')

@section('content-subtitle')
{{-- Lista de Solicitudes --}}
@endsection

@push('head')

<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="dInicioMenu">
  <div v-cloak>
    <section class="content">
      <div class="box box-primary shadow">
        <div class="box-header with-border">
          <div class="col-md-12">
            <div class="row">
              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3>@{{hombres+mujeres+menorH+menorM}}</h3>      
                    <p>Total credenciales</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-address-card"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>@{{hombres}}</h3>
                    <p>Hombres</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-male"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                <div class="small-box" style="background: #ff88a4">
                  <div class="inner">
                    <h3 style="color: white">@{{mujeres}}</h3>
                    <p style="color: white">Mujeres</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-female"></i>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-red">
                  <div class="inner">
                    <h3>@{{menorH+menorM}}</h3>          
                    <p>Menores de edad</p>
                  </div>
                  <div class="icon">
                    <i class="fa fa-child"></i>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-center">
          <h2 class="page-header">Numero de credenciales por región</h2>        
        </div>
        <div class="box-body">
          <canvas id="myChart" width="1200" height="400"></canvas>
        </div>              
        <div class="box-footer">
        </div>
      </div>      
    </section>
  </div>    
</div>
@stop
@push('body')
<script type="text/javascript" src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>
{{-- {!! $dataTable->scripts() !!} --}}
<script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('inicio/inicio.js') }}"></script>

<script type="text/javascript">

</script>


@endpush
