<?php

namespace Modules\Dcredencial\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Persona;
use App\Models\Municipio;
use App\Models\Localidad;
use App\Models\PersonasPrograma;
use App\Models\Tutor;
use App\Models\CertificadoMedico;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\JsonResponse;
use App\Models\Instituciones;
use App\Models\CredImpresion;
use App\Models\Programa;

class DcredencialController extends Controller
{
    public function __construct() {
       
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('dcredencial::index');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
			// return $request->all();
			try {
				$programa = Programa::select('id','nombre')->where('nombre','CREDENCIALIZACION')->whereNull('padre_id')->with('aniosprogramas:id,programa_id')->first();
				DB::beginTransaction();
				$directorio                     = 'public/fotografia';
				if (Persona::where('curp', $request->curp)->count() > 0) {
					$persona = Persona::where('curp', $request->curp)->first();
					$tabla = "PersonasTipo";
				}else{
					$persona = new Persona;

					$persona->nombre                = $request->nombre;
					$persona->primer_apellido       = $request->primerApellido;
					$persona->segundo_apellido      = $request->segundoApellido;
					$persona->calle                 = $request->calle;
					$persona->numero_exterior       = $request->numExt;
					$persona->numero_interior       = $request->numInt;
					$persona->colonia               = $request->colonia;
					$persona->codigopostal          = $request->cp;
					$persona->localidad_id          = $request->localidad;
					$persona->municipio_id          = $request->municipio;
					$persona->curp                  = $request->curp;
					$persona->genero                = $request->genero;
					$persona->clave_electoral       = $request->claveElectoral;
					$persona->numero_celular        = $request->celular;
					$persona->numero_local          = $request->telefono;
					$persona->email                 = $request->email;
					$persona->referencia_domicilio  = $request->referencia;
					$persona->created_at            = date("Y-m-d H:i:s");
					$persona->usuario_id            = auth()->user()->id;
					$persona->fecha_nacimiento      = $request->fechaNacimiento;
						// $persona->firma                 = $request->firma;
					if($request->hasFile('fotografia')) {
						$url    = Storage::putFile($directorio, $request->fotografia);
						$partes = explode('public', $url);
						$persona->fotografia = 'storage' . $partes[1];
					}

					$persona->save();
					$tabla = "Persona&PersonasTipo";

				}

				$personaPrograma = PersonasPrograma::where('persona_id',$persona->id)->where('anios_programa_id',$programa->aniosprogramas[0]['id'])->count();

				if ($personaPrograma == 0)
				{
					$programaPersona = PersonasPrograma::create([
						'persona_id' => $persona->id,
						'anios_programa_id' => $programa->aniosprogramas[0]['id'],
						'usuario_id' => auth()->user()->id,
					]);
				}

				$tutor = Persona::create([
					'nombre' => $request->nombreC,
					'primer_apellido' => $request->apellidoC,
					'segundo_apellido' => $request->apellidoSC,
					'calle' => $request->calleC,
					'colonia' => $request->coloniaC,
					'numero_exterior' => $request->numeroC,
					'codigopostal' => $request->cpC,
					'municipio_id' => $request->municipioC,
					'localidad_id' => $request->localidadC,
					'numero_celular' => $request->telefonoC,
					'usuario_id' => auth()->user()->id,
				]);

				$personaTutor = Tutor::create([
					'persona_id' => $persona->id,
					'tutorado_id' => $tutor->id,
					'programa_id' => $programa->aniosprogramas[0]['id'], // id del programa
					'activo' => 1,
					'usuario_id' => auth()->user()->id,
				]);

				$dtAdicionales = new CertificadoMedico;

				if($request->hasFile('certificado')) {
					$url    = Storage::putFile($directorio, $request->certificado);
					$partes = explode('public', $url);
					$dtAdicionales->ruta_imagen = 'storage' . $partes[1];
				}

				$dtAdicionales->persona_id = $persona->id;
				$dtAdicionales->usuario_id = auth()->user()->id;
				$dtAdicionales->institucion_rubro_id = $request->institucion;

				$dtAdicionales->save();

				$credencial = CredImpresion::create([
					'tipo_impresion_id' => 1,
					'fecha_impresion' => date("Y-m-d H:i:s"),
					'usuario_id' => auth()->user()->id,
				]);

				auth()->user()->bitacora(request(), [
					'tabla' => $tabla,
					'registro' => $persona->id . '',
					'campos' => json_encode($request->all()) . '',
					'metodo' => request()->method(),
					'usuario_id' => auth()->user()->id
				]);

				DB::commit();
				return response()->json(['success' => true, 'id' => $persona->id ], 200);

			} catch (Exception $e) {
				DB::rollback();
				return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
			}
    }

    
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
			try {
				DB::beginTransaction();
					$user = Persona::find($request->bitacora);

					$user->calle                 = $request->calle;
					$user->numero_exterior       = $request->numero_exterior;
					$user->numero_interior       = $request->numero_interior;
					$user->colonia               = $request->colonia;
					$user->numero_celular        = $request->numero_celular;
					$user->numero_local          = $request->numero_local;
					$user->email                 = $request->email;
					$user->referencia_domicilio  = $request->referencia_domicilio;
					$user->firma                 = $request->firma;
					$directorio                  = 'public/fotografia';

					if($request->hasFile('fotografia')) {
						$url    = Storage::putFile($directorio, $request->fotografia);
						$partes = explode('public', $url);
						$user->fotografia = 'storage' . $partes[1];
					}

					$user->firma = $request->firma;
					
					$status = $user->save();

					$tutor = Persona::find($request->tutor_id);

					$tutor->nombre  = $request->nombreC;
					$tutor->primer_apellido = $request->apellidoC;
					$tutor->segundo_apellido = $request->apellidoSC;
					$tutor->calle = $request->calleC;
					$tutor->colonia = $request->coloniaC;
					$tutor->numero_exterior = $request->numeroC;
					$tutor->numero_celular = $request->telefonoC;

					$tutor->save();

					auth()->user()->bitacora(request(), [
						'tabla' => 'Persona',
						'registro' => $user->id . '',
						'campos' => json_encode($request->all()) . '',
						'metodo' => request()->method(),
						'usuario_id' => auth()->user()->id
					]);
				DB::commit();
				return response()->json(['success' => $status, 'id' => $user->id ], 200);
					
			} catch (Exception $e) {
				DB::rollback();
				return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
			}
       
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
			try {
				DB::beginTransaction();
				$programa = Programa::select('id','nombre')->where('nombre','CREDENCIALIZACION')->whereNull('padre_id')->with('aniosprogramas:id,programa_id')->first();
				$persona = PersonasPrograma::where('persona_id',$request->id)->where('anios_programa_id',$programa->aniosprogramas[0]['id'])->delete();
				auth()->user()->bitacora(request(), [
					'tabla' => 'PersonasTipo',
					'registro' => $request->id . '',
					'campos' => json_encode($request->all()) . '',
					'metodo' => request()->method(),
					'usuario_id' => auth()->user()->id
				]);

				DB::commit();
				return response()->json(['success' => true, 'code' => 200, 'estatus' => 'destroy'], 200);	
			}
			catch(\Exception $e) {
				DB::rollBack();
				return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
			}
    }
    /**
    * Funcion para obtener los datos del beneficiario a editar y validar que la curp no se encuntre registrada
    */

    public function consulta (Request $request){

        $persona = Persona::where('curp',$request->curp)->first();

        if (empty($persona))
            return response()->json(['status'=>'vacio']);

        $personaPrograma =  DB::table('personas')->select('personas.id')
            ->join('personas_programas','personas_programas.persona_id','=','personas.id')
            ->where('personas.deleted_at',null)
            ->where('personas.id', $persona->id)
            ->where('personas_programas.anios_programa_id',59) // id del programa creado en la tabla
            ->get()
            ->toArray();

        $personaPrograma = empty($personaExiste) ? false :true;

        if ($personaPrograma)
            return response()->json(['status'=>'Repetido']);

        $personaDatos = Persona::where('curp',$request->curp)->with('localidad','municipio','tutor')->first();
        $certMedico = (CertificadoMedico::where('persona_id',$personaDatos->id)->get()->toArray() != [])?CertificadoMedico::where('persona_id',$persona->id)->first():false;

         return response()->json(['persona' => $personaDatos,'success' => true],200 );
        
       
        return response()->json(['status'=>'no debe llegar aqui']);

    }
    /**
    * Funcion para listar las personas del programa
    */
    public function listaPersonas (){

			$programa = Programa::select('id','nombre')->where('nombre','CREDENCIALIZACION')->whereNull('padre_id')->with('aniosprogramas:id,programa_id')->first();
			$personasProg = PersonasPrograma::where('anios_programa_id', $programa->aniosprogramas[0]['id'])->with('persona')->get();
			$list = collect([]);
			foreach ($personasProg as $p) {
				$list->prepend(['id' => $p->persona->id, 'nombre'=>$p->persona->nombre, 'primerApellido' => $p->persona->primer_apellido, 'segundoApellido' => $p->persona->segundo_apellido, 'fechaNacimiento' => $p->persona->fecha_nacimiento, 'curp'=>  $p->persona->curp, ]);
			}
			return response()->json(['lista' => $list],200);      
    }

    /**
    * Funcion para listar los municipios
    */

    
    /**
    * Funcion para validar que la clave del lector no se encuntre registrada
    */

    public function consultaElector(Request $request)
    {
        $validacion = Persona::where('clave_electoral',$request['cve'])->get()->toArray();

        if (empty($validacion)) 
            return response()->json(['success' => false], 200);
        else
            return response()->json(['success' => true], 200);        
    }

    public function storeSolicitud (Request $request)
    {
        // return $request->all();
        try {
             DB::beginTransaction();

            $directorio = 'public/fotografia';
            $url        = Storage::putFile($directorio, $request->comprobante);
            $partes     = explode('public', $url);             

             $folio = DB::table('cred_solicitudesmodifdatos')->insert(
                array( 
                    'persona_id'            => $request->id,
                    'motivo'                => $request->dato,
                    'ruta'                  => 'storage' . $partes[1],
                    'status_solicitud_id'   => 2,
                    'usuario_id'            => auth()->user()->id,
                    'fecha_solicitud'       => date("Y-m-d H:i:s")
                    
                )
                
            );
             auth()->user()->bitacora(request(), [
                    'tabla' => 'solicitudesmodifdatos',
                    'registro' => $request->id . '',
                    'campos' => json_encode($request->all()) . '',
                    'metodo' => request()->method(),
                    'usuario_id' => auth()->user()->id
                ]);
             DB::commit();
             return response()->json(['success' => "true", 'id' => $folio ], 200);
            
        } catch (Exception $e) {
             // DB::rollback();

            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    public function reimpresionCred (Request $request){

        try {
             DB::beginTransaction();

             DB::table('cred_impresion')->insert(
                array(  
                    'usuario_id'            => auth()->user()->id,
                    'tipo_impresion_id'     => 2, // impresion
                    'fecha_impresion'       => date("Y-m-d H:i:s"),
                    'updated_at'            => date("Y-m-d H:i:s")
                    )
                );

             auth()->user()->bitacora(request(), [
                    'tabla' => 'cred_impresion',
                    'registro' => $request->id ,
                    'campos' => json_encode($request->all()) . '',
                    'metodo' => request()->method(),
                    'usuario_id' => auth()->user()->id
                ]);
             DB::commit();
             return response()->json(['success' => "true" ], 200);
            
        } catch (Exception $e) {
             DB::rollback();

            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    public function busquedaPersona(Request $request)
    {

        $columns = ['nombre','primer_apellido','segundo_apellido','curp'];
        $term = $request->curpPersona;
        $words_search = explode(" ",$term);

       $personaList = DB::table('personas')
            ->join('personas_programas','personas_programas.persona_id','=','personas.id')
            // ->join('personas_tipos', 'personas.id', '=', 'personas_tipos.persona_id')
            // ->where('personas_tipos.modulo_id',9)
            ->where('personas_programas.anios_programa_id',59)
            ->where('personas_programas.deleted_at',null)
            ->where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
            $personaList = $personaList
            ->select('personas.*')
            // ->select('personas.*','personas_tipos.id as idTipo')
            ->paginate(20);

             return [
                'pagination'=>[
                    'total'         => $personaList->total(),
                    'current_page'  => $personaList->currentPage(),
                    'per_page'      => $personaList->perPage(),
                    'last_page'     => $personaList->lastPage(),
                    'from'          => $personaList->firstItem(),
                    'to'            => $personaList->lastPage(),

                ],
                'lista' => $personaList
            ]; 
    }

    public function institucionesList(){
        $list = Instituciones::where('tipoinstitucion_id',1)->orderBy('id')->get();

        return response()->json($list);
    }

    public function showPersona($id){
			$persona = Persona::with('municipio','localidad','dtutor.tutorado.municipio','dtutor.tutorado.localidad','certificado')->find($id);
      return response()->json(['persona'=>$persona,'success' => true],200 );
    }

}



