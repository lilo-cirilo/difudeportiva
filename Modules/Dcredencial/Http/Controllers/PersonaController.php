<?php

namespace Modules\Dcredencial\Http\Controllers;

use App\Models\Localidad;
use App\Models\Municipio;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('dcredencial::solicitud.persona');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dcredencial::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('dcredencial::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('dcredencial::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
		}
		
		public function municipio($municipio)
		{
			$municipios = Municipio::select('nombre','id')->where('nombre', 'like', "%$municipio%")->take(10)->get();
			return new JsonResponse(['data' => $municipios, 'message' => 'ok']);
		}

		public function localidades($municipioId, $localidad)
    {
        $localidades = Localidad::where('nombre', 'like', "%$localidad%")->where('municipio_id', $municipioId)->get();
        return new JsonResponse(['data' => $localidades, 'message' => 'ok']);
    }
}
