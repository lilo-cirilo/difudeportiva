<?php

namespace Modules\Dcredencial\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Persona;
use Maatwebsite\Excel\Facades\Excel;

class DsolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

         return view('dcredencial::solicitud.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('dcredencial::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function listSolicitudes()
    {
        $listaPendientes=DB::table('cred_solicitudesmodifdatos')
        ->join('personas','personas.id','=','cred_solicitudesmodifdatos.persona_id')
        // ->join('personas_tipos', 'personas.id', '=', 'personas_tipos.persona_id')
        // ->where('personas_tipos.modulo_id','=',9)
        // ->where('personas_tipos.deleted_at','=',null)
        ->select('cred_solicitudesmodifdatos.*','personas.nombre','personas.id as idPersona','personas.curp','personas.segundo_apellido')
        ->paginate(10);
        // ->get();
        // return response()->json(['success' => "true", 'lista' => $listaPendientes ], 200);
        return [
            'pagination'=>[
                'total'         => $listaPendientes->total(),
                'current_page'  => $listaPendientes->currentPage(),
                'per_page'      => $listaPendientes->perPage(),
                'last_page'     => $listaPendientes->lastPage(),
                'from'          => $listaPendientes->firstItem(),
                'to'            => $listaPendientes->lastPage(),

            ],
            'lista' => $listaPendientes
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('dcredencial::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
             DB::beginTransaction();

             DB::table('cred_solicitudesmodifdatos')
                ->where('id', $request->idPendiente)
                ->update(['status_solicitud_id' => $request->status, 'created_at' => date("Y-m-d H:i:s")]);

             auth()->user()->bitacora(request(), [
                    'tabla' => 'cred_solicitudesmodifdatos',
                    'registro' => $request->idPendiente . '',
                    'campos' => json_encode($request->all()) . '',
                    'metodo' => request()->method(),
                    'usuario_id' => auth()->user()->id
                ]);
             DB::commit();

             return response()->json(['success' => "true" ], 200);
            
        } catch (Exception $e) {
             // DB::rollback();

            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function busqueda(Request $request)
    {
        $columns = ['nombre','curp','motivo'];
        $term = $request->curp;
        $words_search = explode(" ",$term);

        $listaPendientes = DB::table('personas')

            // ->join('personas_tipos', 'personas.id', '=', 'personas_tipos.persona_id')
            ->join('cred_solicitudesmodifdatos','personas.id','=','cred_solicitudesmodifdatos.persona_id')
            // ->where('personas_tipos.modulo_id','=',9)
            // ->where('personas_tipos.deleted_at','=',null)
            ->where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
          $listaPendientes = $listaPendientes
             ->select('cred_solicitudesmodifdatos.*','personas.nombre','personas.id as idPersona','personas.curp','personas.segundo_apellido')
            ->paginate(20);

            

             return [
                'pagination'=>[
                    'total'         => $listaPendientes->total(),
                    'current_page'  => $listaPendientes->currentPage(),
                    'per_page'      => $listaPendientes->perPage(),
                    'last_page'     => $listaPendientes->lastPage(),
                    'from'          => $listaPendientes->firstItem(),
                    'to'            => $listaPendientes->lastPage(),

                ],
                'lista' => $listaPendientes
            ];
    }

    public function fileXLS (){
       Excel::create('Reporte_Solicitudes', function($excel) {
 
            $excel->sheet('Lista', function($sheet) {
 
                $solicitudes =DB::table('cred_solicitudesmodifdatos')
                ->join('personas','personas.id','=','cred_solicitudesmodifdatos.persona_id')
                // ->join('personas_tipos', 'personas.id', '=', 'personas_tipos.persona_id')
                // ->where('personas_tipos.modulo_id','=',9)
                ->where('personas.deleted_at','=',null)
                ->select('personas.curp','personas.nombre','personas.primer_apellido as primerApellido','personas.segundo_apellido as segundoApellido','cred_solicitudesmodifdatos.motivo','cred_solicitudesmodifdatos.fecha_solicitud as FechaSolicitud','cred_solicitudesmodifdatos.created_at as FechaCambio')
                ->get();

                $solicitudes = json_decode(json_encode($solicitudes), true);
                $sheet->fromArray($solicitudes);
 
            });
        })->export('xls');
    }
}
