<?php

Route::group(['middleware' => 'web', 'prefix' => 'dcredencial', 'namespace' => 'Modules\Dcredencial\Http\Controllers'], function()
{
    Route::get('/', 'DcredencialController@index')->name('dcredencial.home');
    Route::post('/consulta', 'DcredencialController@consulta');
    Route::post('/consultacve', 'DcredencialController@consultaElector');
    Route::get('/list', 'DcredencialController@listaPersonas');
    Route::get('/datos', 'DcredencialController@paginador');
    
    Route::get('/listMunicipio', 'DcredencialController@municipioList');
    Route::get('/listLocalidad/{id}', 'DcredencialController@localidadesList')->where('id','[0-9]+');
    Route::post('/nuevo','DcredencialController@store');
    Route::post('/edit','DcredencialController@update');
    Route::post('/solicitud','DcredencialController@storeSolicitud');
    Route::delete('/destroy/{id}', 'DcredencialController@destroy')->where('id','[0-9]+');
    Route::post('/busquedaPersona','DcredencialController@busquedaPersona');
    Route::get('credencial/filexls','DcredencialController@fileXLS')->name('credfilexls');
    Route::get('/prueba','DcredencialController@prueba');
    Route::get('/getInstituciones','DcredencialController@institucionesList');
    Route::get('/getPerson/{id}','DcredencialController@showPersona');

    /*Solicitudes*/

    Route::get('/solicitud','DsolicitudController@index')->name('dsolicitud');
    Route::get('solicitud/listaPendientes', 'DsolicitudController@listSolicitudes');
    Route::post('solicitud/saveStatus', 'DsolicitudController@update');
    Route::post('solicitud/busqueda','DsolicitudController@busqueda');
    Route::get('solicitud/filexls','DsolicitudController@fileXLS')->name('solicitudfilexls');

    /*Credencial*/
    Route::post('/reimpresion', 'DcredencialController@reimpresionCred');

    /*inicio*/
    Route::get('/inicio','InicioController@index')->name('dcredencial.inicio');
    Route::get('/inicio/consulta','InicioController@consultaDatos');
		
		//Personas
		Route::get('/persona/nuevo','PersonaController@index')->name('nuevaCred');
		Route::get('/municipios/{municipio}','PersonaController@municipio');
		Route::get('/municipios/{municipios_ids}/localidades/{localidad}', 'PersonaController@localidades');

    /* peticiones para el administrador*/

    Route::resource('peticiones', 'PeticionController', ['only' => ['index', 'show', 'update'], 'as' => 'dcredencial' ]);
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'dcredencial' ]);
    Route::resource('programas','ProgramaController',['as'=>'dcredencial']);


});
