<?php

namespace Modules\SillaDeRuedas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\PeticionBeneficiarioBaseController;

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController {
    function __construct() {
      parent::__construct('SILLA DE RUEDAS');
    }
}