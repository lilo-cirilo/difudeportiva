<?php

namespace Modules\SillaDeRuedas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use View;

use App\DataTables\SillaDeRuedas\ProductoDataTable;
use App\Models\Producto;
use App\Models\AreasProducto;


class InventarioController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductoDataTable $dataTable)
    {
        //return view('silladeruedas::inventario.listarMatInv');
        return $dataTable->render('silladeruedas::inventario.listarMatInv');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('silladeruedas::inventario.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('silladeruedas::inventario.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('silladeruedas::inventario.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function listaMateriales()
    {
      /*$productos = Producto::search(Auth::user()->persona->empleado->area_id)      
                  ->get(["cat_productos.id as id", "cat_productos.producto as producto", 
                  "cat_productos.vigencia as vigencia", "cat_unidadmedidas.unidadmedida as unidad"]);    
  
      return Datatables::of($productos)
        ->addColumn('editarStock', function($productos) {
            return '<a onclick="abrir_modal(\'modal-regismaterial\', ' . $productos->id . ');" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('editarNecesidad', function($productos) {
            return '<a onclick="abrir_modal(\'modal-regismaterial\', ' . $productos->id . ');" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('eliminar', function($productos) {
            return '<a class="btn btn-danger btn-xs" onclick="material_delete(' . $productos->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
        })
        ->rawColumns(['editarStock', 'editarNecesidad','eliminar'])
        ->make();*/
    }

    public function ShowFrm_AddMaterial()
    {
      /*$u_medida = Unidadmedida::get();      
      return response()->json([
          'html' =>  view::make('afuncionales::productos.iframe.create_edit')->with('unidades', $u_medida)->render() 
        ], 200);*/
      return response()->json([
          'html' =>  view::make('silladeruedas::inventario.frmNewEditMaterial')->render()
      ], 200);
    }

    public function ShowFrm_EditMaterial($id)
    {
      //$u_medida = Unidadmedida::get();
      $variable = Producto::findOrFail($id);
      return response()->json([
          'html' =>  view::make('silladeruedas::inventario.frmNewEditMaterial')
            ->with('producto',$variable)
          ->render() 
      ], 200);
    }

    public function storeMaterial(Request $request) 
    {
      if($request->ajax()) {
        try{
            DB::beginTransaction();
            $p = Producto::firstOrCreate([
                'producto' => $request->input('material'),
                'vigencia' => 'N/A',
                'unidadmedida_id' => 1,
                'usuario_id' => $request->User()->id
            ]);
            $request['producto_id'] = $p->id;
            //$d = DetalleProducto::create($request->all());
            $ap = AreasProducto::create([
                'producto_id' => $p->id,
                'area_id' => Auth::user()->persona->empleado->area_id,
                'usuario_id' => Auth::user()->id
            ]);
            DB::commit();
            return response()->json(array('success' => true, 'producto' => $p, 'areaproducto_id' => $ap->id));
        }catch(Exception $e){
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    public function updateMaterial(Request $request, $id)
    {
        if($request->ajax()) {
            //dd($request->input('material'));
            try {
                DB::beginTransaction();
                
                $request['usuario_id'] = $request->User()->id;
                $request['producto'] = $request->input('material');
                
                $p = Producto::find($id)->update($request->all());
                DB::commit();
                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function deleteMaterial(Request $request, $id)
    {
      if($request->ajax()) {
        try {
          DB::beginTransaction();
          $producto = Producto::findOrFail($id);
          $producto->delete();              
          auth()->user()->bitacora(request(), [
              'tabla' => 'cat_productos',
              'registro' => $producto->id . '',
              'campos' => json_encode($producto) . '',
              'metodo' => request()->method()
            ]);
            DB::commit();
            return response()->json(array('success' => true, 'id' => $id));
          }catch(\Exception $e) {
              DB::rollBack();
              return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
          }
        }
    }
}
