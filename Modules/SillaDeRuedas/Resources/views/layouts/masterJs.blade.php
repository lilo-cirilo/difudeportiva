<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>

<script type="text/javascript">
  function block() 
  {
    $.blockUI({
      css: {
      border: 'none',
      padding: '5px',
      backgroundColor: 'none',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: .8,
      color: '#fff',
      },
      baseZ: 10000,
      message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
    });

    function unblock_error() {
        if($.unblockUI())
        alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
    }

    setTimeout(unblock_error, 120000);
  }

  function unblock() 
  {
    $.unblockUI();
  }
</script>