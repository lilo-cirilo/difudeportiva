@extends('silladeruedas::layouts.master')

@section('localCSS')
	@include('silladeruedas::inventario.css.cssListarMatInv')
@endsection

@section('content-title','Inventario de material')
@section('content-subtitle', 'Listar material')

@section('li-breadcrumbs')
    <li class="active">Listar material</li>
@endsection

@section('content')	
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">

          <div class="box-header with-border">
            <h3 class="box-title">Listado de materiales</h3>
            <a class="btn btn-success pull-right" onclick="abrir_modal('modal-regismaterial')"><i class="fa fa-plus"></i> Agregar</a>
          </div>

          <div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
              <input type="text" id="search" name="search" class="form-control">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
              </span>
            </div>

            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tblMateriales', 'name' => 'tblMateriales', 'style' => 'width: 100%']) !!}
            
          </div>
          
        </div>
      </div>
    </div>
  </section>

<div class="modal fade" id="modal-regismaterial">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="cerrar_modal('modal-regismaterial')" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-regismaterial-title"></h4>
      </div>
      <div class="modal-body" id="modal-body-regismaterial"></div>
      <div class="modal-footer" id="modal-footer-regismaterial">
        <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-regismaterial')">Cancelar</button>
        <button type="button" class="btn btn-success" onclick="material.create_edit()">Guardar</button>    
      </div>
    </div>
  </div>
</div>
@stop

<!--@section('localScripts')
@endsection-->

@push('body')
  @include('silladeruedas::inventario.js.jsListarMatInv')
@endpush