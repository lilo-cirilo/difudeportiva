<form data-toggle="validator" role="form" id="form_material" onsubmit="return false;" action="{{ isset($producto) ? URL::to('silladeruedas/inventario/updatematerial').'/'.$producto->id : URL::to('silladeruedas/inventario/storematerial') }}" method="{{ isset($producto) ? 'PUT' : 'POST' }}">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="form-group">
        <label for="material">Nombre:</label>
        <input type="text" class="form-control" id="material" name="material" placeholder="material" value="{{isset($producto) ? $producto->producto : ''}}">
      </div>
    </div>
  </div>
</form>