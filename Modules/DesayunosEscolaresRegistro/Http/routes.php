<?php

Route::group(['middleware' => 'web', 'prefix' => 'pesotalla', 'namespace' => 'Modules\DesayunosEscolaresRegistro\Http\Controllers'], function()
{
    Route::resource('/', 'DesayunosEscolaresRegistroController',['as'=>'pesotalla']);
    Route::get('/count', 'DesayunosEscolaresRegistroController@getRegistro');
    Route::resource('/subject', 'BeneficiarioController',['as'=>'pesotalla']);
    Route::get('persona/{persona}','BeneficiarioController@find')->name('pesotalla.persona.find');
});
