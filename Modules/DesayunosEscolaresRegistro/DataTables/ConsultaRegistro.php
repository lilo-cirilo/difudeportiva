<?php

namespace Modules\DesayunosEscolaresRegistro\DataTables;

use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

use Modules\DesayunosEscolaresRegistro\Entities\Beneficiario;
use Modules\DesayunosEscolaresRegistro\Entities\Usuario;
use Modules\DesayunosEscolaresRegistro\Entities\EscuelaUsuario;

class ConsultaRegistro extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        /* ->addColumn('nombre',function($instituto) {
            $escuela = EscuelaUsuario::where('institucion_usuario_id',$instituto->id)->first()->escuela;
            return '('.$escuela->clave .') '.$escuela->nombre;
        }) */
        ->addColumn('progreso', function($instituto) {
            $total = Beneficiario::where('institucion_usuario_id',$instituto->id)->count();
            $registrados = Beneficiario::
                                        join('personas_programas','personas_programas.id','peta_beneficiarios.persona_programa_id')
                                        ->join('personas','personas.id','personas_programas.persona_id')
                                        ->join('peta_instituciones_usuarios','peta_instituciones_usuarios.id','peta_beneficiarios.institucion_usuario_id')
                                        ->join('extradatospersonas','extradatospersonas.persona_id','personas.id')
                                        ->where('peta_beneficiarios.institucion_usuario_id',$instituto->id)
                                        ->count();
            if($registrados == $total)
                return '<span class="badge badge-success">'. $registrados.'/'.$total .'</span>';
            if($registrados != 0)
                return '<span class="badge badge-info">'. $registrados.'/'.$total .'</span>';
            
            return '<span class="badge badge-danger">'. $registrados.'/'.$total .'</span>';
        })
        ->rawColumns(['progreso']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $model = Usuario::join('cat_instituciones','cat_instituciones.id','peta_instituciones_usuarios.institucion_id')
                        ->join('peta_escuelas_usuarios','peta_escuelas_usuarios.institucion_usuario_id','peta_instituciones_usuarios.id')
                        ->join('cat_escuelas','cat_escuelas.id','peta_escuelas_usuarios.escuela_id')
                        ->join('cat_municipios','cat_municipios.id','cat_escuelas.municipio_id')
                        ->join('cat_localidades','cat_localidades.id','cat_escuelas.localidad_id');
                        /* if(($this->request->input('showRecord')=='true')) {
                        $model->join(DB::raw(
                            '(select peta_instituciones_usuarios.id as iu from peta_beneficiarios '.
                            'inner join personas_programas on personas_programas.id = peta_beneficiarios.persona_programa_id '.
                            'inner join personas on personas.id = personas_programas.persona_id '.
                            'inner join peta_instituciones_usuarios on peta_instituciones_usuarios.id = peta_beneficiarios.institucion_usuario_id '.
                            //'inner join extradatospersonas on extradatospersonas.persona_id = personas.id '.
                            'WHERE NOT EXISTS ( '.
                                'SELECT * FROM extradatospersonas '.
                                'WHERE extradatospersonas.persona_id = personas.id)'.
                            ' group by 1) as filtro '
                        ),'peta_instituciones_usuarios.id','filtro.iu');
                        } */
                        $model
                            ->select([
                                'peta_instituciones_usuarios.id as id',
                                'cat_escuelas.clave as clave',
                                'cat_escuelas.nombre as nombre',
                                'cat_municipios.nombre as municipio',
                                'cat_localidades.nombre as localidad'
                            ]);
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.showRecord = $("#showRecord").prop("checked"); }'])
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'clave' => ['data' => 'clave', 'name' => 'cat_escuelas.clave'],
            'instituto' => ['data' => 'nombre', 'name' => 'cat_escuelas.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'progreso' => ['data' => 'progreso', 'name' => 'progreso', 'exportable' => true, 'searchable' => false, 'orderable' => false, 'printable' => true]
            /* 'Clave' => ['data' => 'clave', 'name' => 'cat_institutos.clave'],
            'Municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'Localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'Progreso' => ['data' => 'progreso', 'name' => 'progreso'] */
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() {
                recargarGral();
             }',
            // 'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'processing' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return "progreso al ".date('YmdHis');
    }
}