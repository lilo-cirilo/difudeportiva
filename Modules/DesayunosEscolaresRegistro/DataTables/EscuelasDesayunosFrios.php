<?php

namespace Modules\DesayunosEscolaresRegistro\DataTables;

use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

use App\Models\Persona;
use Modules\DesayunosEscolaresRegistro\Entities\Beneficiario;
use Modules\DesayunosEscolaresRegistro\Entities\ExtraDatosPersona;

class EscuelasDesayunosFrios extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->editColumn('nombre', function($persona) {
            return strtoupper($persona->nombre);
        })
        ->editColumn('primer_apellido', function($persona) {
            return strtoupper($persona->primer_apellido);
        })
        ->editColumn('segundo_apellido', function($persona) {
            return strtoupper($persona->segundo_apellido);
        })
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d/m/Y') : '';
        })
        ->addColumn('edad', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->addColumn('estatus', function($persona) {
            $c = ExtraDatosPersona::where('persona_id',$persona->id)->count();
            if($c > 0)
                return '<span class="badge badge-dark">Ya registrado</span>';//badge badge
            return '<span class="badge badge-warning">Sin registro</span>';
        })
        ->addColumn('peso', function($persona) {
            $e = ExtraDatosPersona::where('persona_id',$persona->id);
            if($e->first())
                return $e->get()->last()->peso;
            return '';
        })
        ->addColumn('talla', function($persona) {
            $e = ExtraDatosPersona::where('persona_id',$persona->id);
            if($e->first())
                return $e->get()->last()->talla;
            return '';
        })
        ->addColumn('fecha_peso', function($persona) {
            $e = ExtraDatosPersona::where('persona_id',$persona->id);
            if($e->first())
                return $e->get()->last()->get_formato_fecha_peso();
            return '';
        })
        ->addColumn('fecha_talla', function($persona) {
            $e = ExtraDatosPersona::where('persona_id',$persona->id);
            if($e->first())
                return $e->get()->last()->get_formato_fecha_talla();
            return '';
        })
        ->addColumn('actualizar', function($persona) {
            //return '<a class="btn btn-warning btn-sm" onclick="beneficiario.agregar('.$persona->id.','.(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age.');"><i class="fa fa-fw fa-edit"></i></a>';
            return "<button class='btn btn-info btn-sm' onClick='modal.loadModal(\"/pesotalla/subject/$persona->id\",\"edit\",{inst_user:$persona->institucion_usuario})'><i class='fa fa-eye'></i></button>";
        })
        ->addColumn('baja',function($persona){
            return "<button class='btn btn-danger btn-sm' onClick='transaccion.formAjax(\"/pesotalla/subject/$persona->beneficiario\",\"DELETE\",\"-beneficiarios\")'><i class='fa  fa-thumbs-down'></i></button>";
        })
        ->rawColumns(['actualizar','estatus','baja']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $model = Beneficiario::join('personas_programas','personas_programas.id','peta_beneficiarios.persona_programa_id')
                             ->join('personas','personas.id','personas_programas.persona_id')
                             /* ->join('anios_programas','anios_programas.id','personas_programas.anios_programa_id')
                             ->join('programas','programas.id','anios_programas.programa_id') */
                             ->join('peta_instituciones_usuarios','peta_beneficiarios.institucion_usuario_id','peta_instituciones_usuarios.id');
                             //->where('programas.id',config('desayunosescolaresregistro.programaId'))
                             if(auth()->user()->usuario != 'conreg')
                                $model->where('peta_instituciones_usuarios.access_usuario_id',auth()->user()->id);
                             $model->select([
                                'personas.id as id',
                                'personas.curp',
                                'personas.nombre as nombre',
                                'personas.primer_apellido',
                                'personas.segundo_apellido',
                                'personas.fecha_nacimiento',
                                'peta_instituciones_usuarios.id as institucion_usuario',
                                'peta_beneficiarios.id as beneficiario'
                             ])
                             ->groupBy(DB::raw('1,2,3,4,5,6,7,8'));
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'nombre'           => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido'  => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            //'fecha_nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'edad'             => ['data' => 'edad', 'name' => 'edad'],
            'curp'             => ['data' => 'curp', 'name' => 'personas.curp'],
            'peso'             => ['data' => 'peso','name'=>'peso','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => true],
            'fecha_peso'       => ['data' => 'fecha_peso','name'=>'fecha_peso','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => true],
            'talla'            => ['data' => 'talla','name'=>'talla','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => true],
            'fecha_talla'      => ['data' => 'fecha_talla','name'=>'fecha_talla','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => true],
            'estatus'          => ['data' => 'estatus', 'name' => 'estatus'],
            //'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            //'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            //'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            //'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'actualizar'       => ['data' => 'actualizar', 'name' => 'actualizar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'baja'             => ['data' => 'baja', 'name' => 'baja', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
        'language' => [
            'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
        ],
        'dom' => 'Btip',
        'buttons' => [
            'pdf' , 'excel', 'reset', 'reload'  
        ],
        'lengthMenu' => [ 10 ],
        'responsive' => true,
        'columnDefs' => [
            [ 'className' => 'text-center', 'targets' => '_all' ]
        ]
        ];

        return $builderParameters;

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'Peso y talla de '.auth()->user()->usuario.' al ' . date('d-m-Y');
    }
}