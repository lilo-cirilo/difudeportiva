<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    {{-- <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template"> --}}
    {{-- <meta name="author" content="Łukasz Holeczek"> --}}
    {{-- <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard"> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Main styles for this application-->
    <link href="{{ Module::asset('asistenciaalimentaria:css/app.css') }}" rel="stylesheet">
    @stack('head')
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    
    @include('asistenciaalimentaria::layouts.includes.navbar')

    <div class="app-body">
      
        <div class="sidebar">
            <nav class="sidebar-nav">
            </nav>
        </div>

      <main class="main">
        
        {{-- @include('asistenciaalimentaria::layouts.includes.breadcrumb') --}}

        <div class="container-fluid">
          
          @yield('content')
          
        </div>

      </main>

      {{-- @include('asistenciaalimentaria::layouts.includes.aside')   --}}

    </div>
    <footer class="app-footer">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span>Unidad de Informática</span>
      </div>
    </footer>
    <script src="{{ Module::asset('asistenciaalimentaria:js/app.js') }}"></script>
    <script src="{{ Module::asset('asistenciaalimentaria:js/all.js') }}"></script>
    <script type="text/javascript">
      $.extend($.validator.messages, {
        required: 'Este campo es obligatorio.',
        remote: 'Por favor, rellena este campo.',
        email: 'Por favor, escribe una dirección de correo válida.',
        url: 'Por favor, escribe una URL válida.',
        date: 'Por favor, escribe una fecha válida.',
        dateISO: 'Por favor, escribe una fecha (ISO) válida.',
        number: 'Por favor, escribe un número válido.',
        digits: 'Por favor, escribe sólo dígitos.',
        creditcard: 'Por favor, escribe un número de tarjeta válido.',
        equalTo: 'Por favor, escribe el mismo valor de nuevo.',
        extension: 'Por favor, escribe un valor con una extensión aceptada.',
        maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
        minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
        rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
        range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
        max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
        min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
        nifES: 'Por favor, escribe un NIF válido.',
        nieES: 'Por favor, escribe un NIE válido.',
        cifES: 'Por favor, escribe un CIF válido.'
      });
    </script>
    @stack('body')
  </body>

</html>
