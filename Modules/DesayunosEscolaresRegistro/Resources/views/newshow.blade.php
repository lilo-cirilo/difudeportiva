@extends('desayunosescolaresregistro::layouts.newmaster')

@section('title','PESO Y TALLA NACIONAL')

@section('content-subtitle', 'Registros de peso y talla')

@push('head')
	
  {{-- <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet"> --}}
  <style>
    .error{
      width: 100%;
    }
    .img-conv {
      max-width: 50%;
      margin-inline: 25%;
    }
  </style>
@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
				<h4 id="progresion" class="box-title">Listado de Escuelas {{$registrados.' de '.$total}}</h4>
		</div>
		<hr>
		<div class="box-body">
				{{-- <input type="checkbox" name="showRecord" id="showRecord"><span> Mostrar sólo quienes faltan por registrar</span>
				<hr> --}}
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
					<input type="text" id="search" name="search" class="form-control">
					<div class="input-group-append">
						<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
					</div>
				</div>
				{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'peso-talla', 'name' => 'peso-talla', 'style' => 'width: 100%']) !!}
		</div>
	</div>
@stop

@push('body')
	<script type="text/javascript" src="{{ Module::asset('asistenciaalimentaria:js/all.js') }}"></script>
  {!! $dataTable->scripts() !!}
  <script type="text/javascript">
		var tabla = ((tablaId) => {
			$('#search').keypress(function(e) {
				if(e.which === 13) {
					$(tablaId).DataTable().search($('#search').val()).draw()
				}
			})
			$('#btn_buscar').on('click', function() {
				$(tablaId).DataTable().search($('#search').val()).draw()
			})
			function recargar(params) {
				$(tablaId).DataTable().ajax.reload(null,false);
			}
			return {
				recargar : recargar
		}})('#peso-talla');
		$('#showRecord').iCheck({
			checkboxClass: "icheckbox_square-green",
			increaseArea: "10%"
		}).on('ifChanged', function (e) {
			tabla.recargar();
		})
		function recargarGral() {
				$.get('/pesotalla/count', function(data) {
				})
				.done(function(data) {
						console.log(data)
						$("#progresion").text("Listado de Escuelas, "+data.registrados+" de "+data.total+" niños(as) registrados")
				})
		}

		function ajaxSuccess(response,tabla) {
			window.LaravelDataTables[tabla].ajax.reload();
			Swal.fire('¡Guardado!','Operación realizada con éxito','success')
		}
		function ajaxError(response) {
			tabla.recargar();
      //console.log(response.responseJSON.message);
      Swal.fire('¡Error!',response.responseJSON.message,'error')
		}
	</script>
@endpush