<script>
    // Asistencia Alimentaria Patrón auxiliar para controlar el modal y selección de personas
    var Personas = (() => {

    var elemento = undefined
    var datatable_personas = undefined
    var nuevo_beneficiario = false

    var inicializar = () => {                
        datatable_personas = $('#personas').DataTable()
        $('#search_personas').keypress(e => {
            if(e.which === 13) {
                datatable_personas.search($('#search_personas').val()).draw()
            }
        })
        $('#btn_buscar_personas').on('click', () => {
            datatable_personas.search($('#search_personas').val()).draw()
        })
        $('#modal-personas').on('shown.bs.modal', evt => {
            if(datatable_personas !== undefined) {
                datatable_personas.ajax.reload()
            }
        })
    }

    //El parámetro "e" hace referencia al elemento que lo llamó
    var abrir_modal = (e) => {
        elemento = e
        nuevo_beneficiario = false
        if(elemento === 'beneficiarios'){
            nuevo_beneficiario = true
        }
        $('#modal-personas').modal('show')
    }

    var seleccionar = (id, nombre) => {                
        var newOption = new Option(nombre, id, true, true)
        //Si la persona a la que va dirigida la solicitud es OTRA a las opciones mostradas
        if(elemento === 'persona_id'){                    
            $('#persona_id').append(newOption).trigger('change')
            $('#persona_id').valid()
        }
        //Si se trata del remitente
        else if(elemento === 'solicitante_id'){
            $('#solicitante_id').append(newOption).trigger('change')
            $('#solicitante_id').valid()
        }

        //Si se estan agregando beneficiarios
        else if(elemento === 'beneficiarios'){
            mostrar(id, true)
        }

        $('#modal-personas, #modal-persona').modal('hide')
        unblock()
    }

    var mostrar = (id, nuevo) => {
        nuevo_beneficiario = nuevo
        block()
        var url = '{{ route("personas.mostrar", [":id"] ) }}'

        //Muestra informacion del beneficiario
        if(id){
            url = url.replace(':id', id)
            $.get(url, function(data) {
                $('#modal-beneficiario').html(data.persona)
                $('#modal-beneficiario').modal('show')
                if(!nuevo_beneficiario){
                    $("#modal-footer-beneficiario button:not(:eq(0))").hide()
                }
                unblock()
            })
        }
        //Muestra información del remitente
        else if($('#solicitante_id').val() !== null){                                
            url = url.replace(':id', $('#solicitante_id').val())
            $.get(url, function(data) {
                $('#modal-beneficiario').html(data.persona)
                $('#modal-beneficiario').modal('show')
                $("#modal-footer-beneficiario button:not(:eq(0))").hide()
                unblock()
            })
        }
        //No se ha seleccionado a ningun remitente o beneficiario
        else{
            unblock()
            swal(
                '',
                'Debe seleccionar a una persona',
                'info'
            )
        }
    }

    var create_edit_error = (response) => {
        unblock()
        if(response.status === 422) {
            swal({
                title: 'Error al registrar',				
                text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                type: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Regresar',
                allowEscapeKey: false,
                allowOutsideClick: false
            })
        }
    }

    var agregar = (id) => {
        block()
        $.get('/personas/search?tipo=create_edit&id=' + id)
        .done(data => {
            
        })
        .fail(data => {
            $('#modal-title-persona').text('Agregar Persona:')
            $('#modal-body-persona').html(data.responseJSON.html)
            $('#modal-footer-persona').html(
                '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>'+
                '<button type="submit" class="btn btn-success" onclick="persona.create_edit()"><i class="fa fa-database"></i> Guardar</button>'                
            )
            app.to_upper_case()
            persona.init()
            persona.editar_fotografia()
            persona.agregar_fecha_nacimiento()
            persona.agregar_inputmask()
            persona.agregar_select_create()
            persona.agregar_select_edit()
            persona.agregar_validacion_atnciudadana()      
            $('#modal-persona').modal('show')
            unblock()
        })
    }

    return {
        inicializar,
        abrir_modal,
        seleccionar,
        mostrar,
        agregar,
        create_edit_error
    }
    })()
</script>