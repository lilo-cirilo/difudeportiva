<h5>Personal</h5>

<hr>

<div class="row">  
  <div class="col-xs-12 col-md-6 col-lg-4">
    <div class="form-group">
      <label for="nombre">*Nombre(s):</label>
      <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $persona->nombre or '' }}" placeholder="INGRESE NOMBRE(S)">
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-4">
    <div class="form-group">
      <label for="primer_apellido">*Primer Apellido:</label>
      <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $persona->primer_apellido or '' }}" placeholder="INGRESE EL PRIMER APELLIDO">
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-4">
    <div class="form-group">
      <label for="segundo_apellido">Segundo Apellido:</label>
      <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $persona->segundo_apellido or '' }}" placeholder="INGRESE EL SEGUNDO APELLIDO">
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="form-group">
      <label for="genero">*Género:</label>
      <select id="genero" name="genero" class="form-control select2" style="width: 100%;">
        <option value=""></option>
        <option value="M" {{ (isset($persona) && $persona->genero == 'M') ? 'selected' : '' }}>MASCULINO</option>
        <option value="F" {{ (isset($persona) && $persona->genero == 'F') ? 'selected' : '' }}>FEMENINO</option>
      </select>
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="form-group">
      <label for="fecha_nacimiento">*Fecha de Nacimiento:</label>
      <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ isset($persona) ? ($persona->fecha_nacimiento ? $persona->get_formato_fecha_nacimiento() : '') : '' }}" placeholder="DIA/MES/AÑO">
    </div>
  </div>
  <div class="col-xs-12  col-md-6 col-lg-3">
    <div class="form-group">
      <label for="curp">*CURP:</label>
      <input data-inputmask='"mask": "aaaa999999aaaaaa*9"' data-mask type="text" class="form-control mask" id="curp" name="curp" value="{{ $persona->curp or '' }}" placeholder="CLAVE UNICA DE REGISTRO DE POBLACION">
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="form-group">
      <label for="etnia_id" class="center-block">Grupo étnico:</label>
      <select id="etnia_id" name="etnia_id" class="form-control select2" style="width: 100%;">
          <option value=""></option>
          @if(isset($persona) && isset($persona->etnia))
            <option value="{{ $persona->etnia->id }}" selected>{{ $persona->etnia->nombre }}</option>
          @endif
        </select>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-md-6 col-lg-12">
    <div class="form-group">
      <label for="num_acta_nacimiento">Número de acta de nacimiento:</label>
      <input type="text" class="form-control" id="num_acta_nacimiento" name="num_acta_nacimiento" value="{{ $persona->num_acta_nacimiento or '' }}" placeholder="INGRESE NÚMERO DE ACTA DE NACIMIENTO">
    </div>
  </div>
</div>

<h5>Domicilio</h5>

<hr>

<div class="row">                  
  <div class="col-xs-12 col-md-6 col-lg-8">
    <div class="form-group">
      <label for="calle">Calle:</label>
      <input type="text" class="form-control" id="calle" name="calle" value="{{ $persona->calle or '' }}" placeholder="NOMBRE DE LA CALLE">
    </div>
  </div>              
  <div class="col-xs-12 col-md-3 col-lg-2">
    <div class="form-group">
      <label for="numero_exterior">Número Ext:</label>
      <input type="text" class="form-control" id="numero_exterior" name="numero_exterior" value="{{ $persona->numero_exterior or '' }}" placeholder="NUMERO EXTERIOR">
    </div>
  </div>
  <div class="col-xs-12 col-md-3 col-lg-2">
    <div class="form-group">
      <label for="numero_interior">Número Interior:</label>
      <input type="text" class="form-control" id="numero_interior" name="numero_interior" value="{{ $persona->numero_interior or '' }}" placeholder="NUMERO INTERIOR">
    </div>
  </div>
  <div class="col-xs-12 col-md-9 col-lg-8">
    <div class="form-group">
      <label for="colonia">Colonia:</label>
      <input type="text" class="form-control" id="colonia" name="colonia" value="{{ $persona->colonia or '' }}" placeholder="NOMBRE DE LA COLONIA">
    </div>
  </div>   
  <div class="col-xs-12 col-md-3 col-lg-2">
    <div class="form-group">
      <label for="codigopostal">Código Postal:</label>
      <input type="text" class="form-control" id="codigopostal" name="codigopostal" value="{{ $persona->codigopostal or '' }}" placeholder="CODIGO POSTAL">
    </div>
  </div>
</div>
  
<div class="row">
  <div class="col-xs-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label for="municipio_id">*Municipio:</label>
      <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
        @if(isset($persona) && isset($persona->municipio))
          <option value="{{ $persona->municipio->id }}" selected>({{ $persona->municipio->distrito->region->nombre }}) ({{ $persona->municipio->distrito->nombre }}) {{ $persona->municipio->nombre }}</option>
        @endif
      </select>
    </div>
  </div>    
  <div class="col-xs-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label for="localidad_id">Localidad:</label>
      <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
        @if(isset($persona) && isset($persona->localidad))
          <option value="{{ $persona->localidad->id }}" selected>({{ $persona->localidad->cve_localidad }}) {{ $persona->localidad->nombre }}</option>
        @endif
      </select>
    </div>
  </div>
</div>
  
<div class="row">
  <div class="col-xs-12 col-md-12 col-lg-12">
    <div class="form-group">
      <label for="referencia_domicilio">Referencias del Domicilio:</label>
      <textarea class="form-control" rows="3" id="referencia_domicilio" name="referencia_domicilio" placeholder="ESPECIFIQUE REFERENCIAS">{{ $persona->referencia_domicilio or '' }}</textarea>
    </div>
  </div>
</div>  

<h5>Contacto</h5>

<hr>

<div class="row">
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="form-group">
      <label for="numero_celular">Celular:</label>
      <input type="text" class="form-control mask" id="numero_celular" name="numero_celular" data-inputmask='"mask": "(999) 999-999-9999"' data-mask value="{{ $persona->numero_celular or '' }}" placeholder="CELULAR PERSONAL">
    </div>
  </div>
  <div class="col-xs-12 col-md-6 col-lg-3">
    <div class="form-group">
      <label for="numero_local">Teléfono:</label>
      <input type="text" class="form-control mask" id="numero_local" name="numero_local" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{ $persona->numero_local or '' }}" placeholder="NUMERO DE CASA">
    </div>
  </div>
  <div class="col-xs-12 col-md-12 col-lg-6">
    <div class="form-group">
      <label for="email">Correo Electrónico:</label>
      <input type="text" class="form-control" id="email" name="email" value="{{ $persona->email or '' }}" placeholder="CORREO@EJEMPLO.COM">
    </div>
  </div>
</div>