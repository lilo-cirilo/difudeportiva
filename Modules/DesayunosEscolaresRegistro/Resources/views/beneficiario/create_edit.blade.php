@php
    $form_name = 'form-beneficiarios'
@endphp
<div class="modal-header">
  <h4 class="modal-title">
    {{ isset($persona) ? 'Editar' : 'Agregar' }} niño(a)
    @if (isset($persona))
      <i class="fas fa-user-edit" id="edit-icon"></i>
    @else
      <i class="fas fa-user-plus"></i>
    @endif
  </h4>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body with-border">
  <form data-toggle="validator" role="form" id="{{ $form_name }}" class="needs-validation">
    <ul class="nav nav-tabs" id="tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#tab-persona" role="tab" aria-controls="home" aria-selected="true">Datos generales</a>
      </li>
      @isset($vista_beneficiario)
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tab-beneficiario" role="tab" aria-controls="profile" aria-selected="false">Datos peso y talla</a>
        </li>
      @endisset
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade show active" id="tab-persona" role="tabpanel" aria-labelledby="home-tab">
        @include('desayunosescolaresregistro::beneficiario.datos_persona')
      </div>
      @isset($vista_beneficiario)
        <div class="tab-pane fade" id="tab-beneficiario" role="tabpanel" aria-labelledby="profile-tab">
          {{--Aquí va la vista correspondiente al tipo de beneficiario es decir si es de desayunos escolares peso y talla--}}
          @includeIf($vista_beneficiario)
        </div>
      @endisset
    </div>
  </form>
</div>
<div class="modal-footer">
<span class="mr-auto">Los campos con * son obligatorios</span>
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  @isset($vista_beneficiario)
    <div id="next"><button class="btn float-right btn-secondary" id="btn-next"><span>Siguiente <i class="fa fa-arrow-circle-right"></i></span></button></div>
    <div id="back"><button class="btn float-right btn-secondary" id="btn-back"><span><i class="fa fa-arrow-circle-left"></i> Atrás</span></button></div>
    <div id="btn-save-bf"><button class="btn float-right btn-primary" onclick="verificarPesoTalla('{{ isset($persona) ? route('pesotalla.subject.update',[$persona->id]) : route('pesotalla.subject.store') }}','{{ isset($persona) ? 'PUT' : 'POST'}}','{{ $form_name }}',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button></div>
  @endisset
</div>
{{-- </div> --}}
<script>
  (function() {
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    function buscarPersona() {
      //En caso de corresponder a un usuario que no pueda editar información
      /* @if(auth()->user()->hasRoles(['SUPERADMIN']))
        $('form').find('input, textarea, button, select').prop('readonly', true)
        //$('.box-footer').html('')
      @endif */
      @if(isset($persona->bienestarpersona))
        @if($persona->bienestarpersona->deleted_at == null)
          $('#tab-persona').find('input, textarea, button, select').prop('readonly', true);
          $('#tab-persona').find('select').prop('disabled', true);
          Swal.fire(
            '¡Esta persona pertenece al padrón de bienestar!',
            'Por lo tanto los datos generales no pueden ser modificados',
            'warning'
          )
        @endif
      @endif
      /*
        Este proceso se realizó por que el formulario se enviaba a ún cuando faltaban
        campos por rellenar en el tab con id "tab-beneficiario"
      */
      $('#back').hide()
      $('#btn-save-bf').hide()
      let tabBeneficiario = $('#tabs a[href="#tab-beneficiario"]')
      let tabPersona = $('#tabs a[href="#tab-persona"]')
      tabBeneficiario.addClass('disabled')
      $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        let back = $('#back')
        let next = $('#next')
        let save = $('#btn-save-bf')
        if(String(e.target).split('#')[1] === 'tab-persona') {
          back.hide()
          next.show()
          save.hide()
        } else {
          next.hide()
          save.show()
          back.show()
        }
      })
      $('#btn-next').on('click',function(){
        if(validacion.revisar()) {
          tabBeneficiario.removeClass('disabled').tab('show')
          tabPersona.addClass('disabled')
        }
        var genero = ($('#curp').val()).substring(10,11)
        if(genero.toUpperCase() == 'H')
          $('#genero-alim').hide()
        else
          $('#genero-alim').show()
      })
      $('#btn-back').on('click',function(){
        tabBeneficiario.addClass('disabled')
        tabPersona.removeClass('disabled').tab('show')
      })
      /*
        autocompletado para agilizar proceso de llenado
      */
      var cache=[];
      $('#curp').autocomplete({
        minLength : 5,
        source: function(request, response) {
          var term = request.term;
          if (term in cache) {
            response(cache[ term ]);
            return;
          }
          $.ajax({
            url: "{{ route('personas.index') }}",
            dataType: "json",
            global: false,
            data: {
              columns : {
                0 : {
                  name : 'personas.curp',
                  search : {
                    value : term.replace('_','')
                  }
                }
              },
              tipo : 'select'
            },
            success: function (data) {
              cache[ term ] = data.data;
              response(data.data);
            }
          });
        },
        select: function(event, ui) {
          //encontrarPersona(ui.item.edit);
          var persona = (ui.item.edit).split("/");
          encontrarPersona(persona[persona.length-1]);
        },
        appendTo: "#{{ $tipo }}"
      });
    }
    function encontrarPersona(persona) {
      $.get('/pesotalla/persona/'+persona,function (data) {
        // if(data.bienestar === 1) {
        //   Swal.fire({
        //     title: '¡Esta persona pertenece al padrón de bienestar!',
        //     type: 'warning',
        //     timer: 2500,
        //     toast : true,
        //     position : 'center',
        //     showConfirmButton: false
        //   });
        // }
        Object.keys(data.persona).forEach(element => {
          if(data.bienestar === 1){
            $('#tab-persona').find('input, textarea, button, select').prop('readonly', true);
            $('#tab-persona').find('select').prop('disabled', true);
          }
          else{
            $('#'+element).removeAttr('readonly');
            $('#tab-persona select').prop('disabled',false)
          }
          var aux = data.persona[element]
          if (aux != null) {
            if(element.indexOf('fecha')>=0)
              aux = aux.split('-').reverse().join('/')
            if(element == 'tiporopa_id')
              $('#tiporopa_id option[value='+aux+']').attr('selected', 'selected').trigger('change');
            else
              $('#'+element).val(aux);
          }
        });
        $('#municipio_id').html('<option value="'+data.persona.municipio_id+'" selected>'+data.mun+'</option>').trigger('change');
        $('#localidad_id').html('<option value="'+data.persona.localidad_id+'" selected>'+data.loc+'</option>').trigger('change');
        $('#etnia_id').html('<option value="'+data.persona.etnia_id+'" selected>'+data.etn+'</option>').trigger('change');
        $('#genero').trigger('change');
        add = $('#edit-icon');
        if (Object.keys(add).length === 0) {
          $('#curp').prop('readonly',true)
          Swal.fire(
            '¡Importante!',
            'Tal vez para este proceso no puedas modificar algunos campos',
            'warning'
          )
        }
      })
    }
    
    function initComponets() {
      $('.select2').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumResultsForSearch: Infinity
      });
      $('#fecha_nacimiento').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd/mm/yyyy',
        startDate: '01-01-1900',
        endDate: '0d',
        orientation: 'bottom',
        enableOnReadonly : false
      });
      $('#fecha_talla').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment('2000-01-01').format('DD/MM/YYYY'),
          endDate: moment().format('DD/MM/YYYY')
      });
      $('#fecha_peso').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment('2000-01-01').format('DD/MM/YYYY'),
          endDate: moment().format('DD/MM/YYYY')
      });
      $('#municipio_id').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('municipios.select') }}',
          delay: 500,
          dataType: 'JSON',
          global: false,
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#municipio_id').valid();
        $('#localidad_id').empty();
        $('#localidad_id').select2({
          language: 'es',
          placeholder : 'SELECCIONE ...',
          ajax: {
            url: '{{ route('localidades.select') }}',
            delay: 500,
            dataType: 'JSON',
            global: false,
            type: 'GET',
            data: function(params) {
              return {
                search: params.term,
                municipio_id: $('#municipio_id').val()
              };
            },
            processResults: function(data, params) {
              params.page = params.page || 1;
              return {
                results: $.map(data, function(item) {
                  return {
                    id: item.id,
                    text: '('+item.cve_localidad+') '+item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                })
              };
            },
            cache: true
          }
        }).change(function(event) {
          $('#localidad_id').valid();
        });
      });
      $('.mask').inputmask();
      $('a[data-toggle="tab"]').on('click', function(){
        if ($(this).parent('li').hasClass('disabled')) {
          return false;
        }
      });
      $('#etnia_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        ajax: {
          url: '{{ route('vale.etnias.select') }}',
          delay: 500,
          dataType: 'JSON',
          global: false,
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#etnia_id').valid();
      });
    }
    initComponets()
    buscarPersona();
    $('#{{ $form_name }}').validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          pattern_nombre: ''
        },
        primer_apellido: {
          required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        segundo_apellido: {
          //required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        fecha_nacimiento: {
          required: true,
          pattern_fecha: ''
        },
        curp: {
          required: true,
          minlength: 18,
          maxlength: 19,
          pattern_curp: ''
        },
        genero: {
          required: true
        },
        calle: {
          //required: true,
          minlength: 3
        },
        numero_exterior: {
          //required: true,
          minlength: 1,
          pattern_numero: ''
        },
        numero_interior: {
          //required: false,
          minlength: 1,
          pattern_numero: ''
        },
        colonia: {
          //required: true,
          minlength: 3
        },
        codigopostal: {
          //required: false,
          minlength: 5,
          maxlength: 5,
          pattern_integer: ''
        },
        municipio_id: {
          required: true
        },
        localidad_id: {
          required: false
        },
        referencia_domicilio: {
          //required: true,
          minlength: 3
        },
        numero_celular: {
          required: false,
          pattern_telefono: ''
        },
        numero_local: {
          required: false,
          pattern_telefono: ''
        },
        email: {
          required: false,
          email: true
        },
        etnia_id: {
          required: false,
        },
        talla: {
            required: true,
            number: true,
            minlength: 3,
            maxlength: 5
        },
        peso: {
            required: true,
            number: true,
            minlength: 2,
            maxlength: 6
        },
        fecha_talla: {
            required: true,
            minlength: 10,
            maxlength: 10,
            pattern_fecha: ''
        },
        fecha_peso: {
            required: true,
            minlength: 10,
            maxlength: 10,
            pattern_fecha: ''
        },
        tiporopa_id: {
            required: true
        },
        grado: {
            required: true
        }
      },
      messages: {
      }
    });
    $.validator.addMethod('pattern_fecha', function (value, element) {
        return this.optional(element) || /^[0-9]{2}[/][0-9]{2}[/][0-9]{4}$/.test(value);
    }, 'Revise los caracteres y vuelve a intentarlo')
    validacion.iniciar('{{ $form_name }}')    
    $('#curp').focus(function(){
      if($('#curp').val().length==18) return false
      var fecha = $('#fecha_nacimiento').val().split('/')
      var vcurp = new curp($('#nombre').val(),$('#primer_apellido').val(),$('#segundo_apellido').val(),fecha[2],fecha[1],fecha[0],$('#genero').val(),'')
      $('#curp').val(vcurp.validar())
      var event = new InputEvent('input', {
        view: window
      });
      var cb = document.getElementById('curp'); 
      cb.dispatchEvent(event);
      $('#curp').setSelectionRange(16,16)
    })
  })()
  
  function verificarPesoTalla(url, type, formNameId, extraData = false,contype=undefined) {
      var talla = $('#talla')
      var peso = $('#peso')
      fecha_nacimiento = ($('#fecha_nacimiento').val()).split('/')
      nacimiento = moment(fecha_nacimiento[2]+'-'+fecha_nacimiento[1]+'-'+fecha_nacimiento[0])
      hoy = moment()
      edad = hoy.diff(nacimiento,'years')

      //if($('#'+formNameId).valid()) {
        t = (edad <= 3 && (parseFloat(talla.val()) <= 160.0 && parseFloat(talla.val()) >= 50.0)) ||
                (edad > 3 && (parseFloat(talla.val()) <= 190.0 && parseFloat(talla.val()) >= 80.0))
                ? true : false

        p = (edad <= 3 && (parseFloat(peso.val()) <= 30.0 && parseFloat(peso.val()) >= 8.0)) ||
                (edad > 3 && (parseFloat(peso.val()) <= 100.0 && parseFloat(peso.val()) >= 14.0))
                ? true : false
        mensaje = true
        if(!t)
          mensaje = 'Talla'
        else if(!p)
          mensaje = 'Peso'
        
        console.log(edad,t,p,mensaje);
        if(mensaje != true) {
          Swal.fire(
            '',
            mensaje+' no creíble',
            'error'
          )
          return
        }
        transaccion.formAjax(url,type,formNameId,extraData,contype)
      //}
    }
</script>