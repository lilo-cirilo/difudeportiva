@extends('desayunosescolaresregistro::layouts.newmaster')

@section('title','PESO Y TALLA NACIONAL')

@section('content-subtitle', 'Registro de peso y talla')

@push('head')
	
  {{-- <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet"> --}}
	{{-- <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet"> --}}
  <style>
    .error{
      width: 100%;
    }
    .img-conv {
      max-width: 50%;
      margin-inline: 25%;
    }
  </style>
@endpush

@section('content')
	<hr>
	<h5 class="box-title">LISTADO DE  NIÑOS(AS)</h5>
	<div class="row">
		@if (auth()->user()->usuario != 'conreg')
			<hr>
			@php
					$desayuno = auth()->user()->desayunousuario->desayuno;
			@endphp
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Región: </strong> {{$desayuno->escuela->municipio->distrito->region->nombre or '-----'}} </p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Distrito: </strong> {{$desayuno->escuela->municipio->distrito->nombre or '-----'}} </p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Localidad: </strong> {{$desayuno->escuela->localidad->nombre or '-----'}} </p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Municipio: </strong> {{$desayuno->escuela->municipio->nombre or '-----'}} </p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Nombre Escuela: </strong> {{$desayuno->escuela->nombre or '-----'}} </p>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Clave Escuela: </strong> {{$desayuno->escuela->clave or '-----'}} </p>
			</div>
			{{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Nivel: </strong> {{auth()->user()->desayunousuario->desayuno->escuela->nivel->nivel or '-----'}} </p>
			</div>--}}
			{{--<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
					<p><strong>Tipo Escuela: </strong> ----- </p>
			</div>--}}
		@else
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<a href="{{route('pesotalla..create')}}">Consultar progreso</a>
			</div>
		@endif
	</div>
	<hr>
	<div class="box box-primary">
		<div class="box-header with-border">
			<button class="btn float-right btn-secondary" tabindex="0" onclick="modal.loadModal('/pesotalla/subject/'+undefined,'create');"><span><i class="fa fa-plus"></i> Agregar persona</span></button>
		</div>
		<div class="box-body">
			<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
				<input type="text" id="search" name="search" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
        </div>
			</div>
			{!! $dataTable->table(['class' => 'table table-bordered table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'beneficiarios']) !!}
		</div>
	</div>
@stop

@push('body')
	<script type="text/javascript" src="{{ Module::asset('asistenciaalimentaria:js/all.js') }}"></script>
  {!! $dataTable->scripts() !!}
  <script type="text/javascript">
		var tabla = ((tablaId) => {
			$('#search').keypress(function(e) {
				if(e.which === 13) {
					$(tablaId).DataTable().search($('#search').val()).draw()
				}
			})
			$('#btn_buscar').on('click', function() {
				$(tablaId).DataTable().search($('#search').val()).draw()
			})
			function recargar(params) {
				$(tablaId).DataTable().ajax.reload(null,false);
			}
			return {
				recargar : recargar
		}})('#beneficiarios');



		function ajaxSuccess(response,tabla) {
			window.LaravelDataTables[tabla].ajax.reload();
			Swal.fire('¡Guardado!','Operación realizada con éxito','success')
		}
		function ajaxError(response) {
			tabla.recargar();
      //console.log(response.responseJSON.message);
      Swal.fire('¡Error!',response.responseJSON.message,'error')
		}
	</script>
@endpush