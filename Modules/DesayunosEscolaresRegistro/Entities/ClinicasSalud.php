<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicasSalud extends Model{
  use SoftDeletes;
  
  protected $table = 'cat_clinicassalud';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
