<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiario extends Model{
  use SoftDeletes;
  
  protected $table = 'peta_beneficiarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['persona_programa_id','institucion_usuario_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function personaprograma()
  {
    return $this->belongsTo('App\Models\PersonaPrograma');
  }

  public function petausuario()
  {
    return $this->belongsTo('Modules\DesayunosEscolaresRegistro\Entities\Usuario');
  }
}
