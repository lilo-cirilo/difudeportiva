<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClinicaUsuario extends Model{
  use SoftDeletes;
  
  protected $table = 'peta_clinicas_usuarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['clinica_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function usuario()
  {
    return $this->belongsTo('App\Models\Usuario');
  }

  public function institucion()
  {
    return $this->belongsTo('App\Models\Instituciones');
  }
}
