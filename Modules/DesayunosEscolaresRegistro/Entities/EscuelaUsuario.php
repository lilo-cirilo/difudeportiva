<?php

namespace Modules\DesayunosEscolaresRegistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EscuelaUsuario extends Model{
  use SoftDeletes;
  
  protected $table = 'peta_escuelas_usuarios';
  protected $dates = ['deleted_at'];
  protected $fillable=['escuela_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function escuela()
  {
    return $this->belongsTo('App\Models\Escuela');
  }
}
