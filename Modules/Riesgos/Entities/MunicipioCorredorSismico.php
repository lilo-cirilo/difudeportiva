<?php

namespace Modules\Riesgos\Entities;

use Modules\Riesgos\Entities\Base;

class MunicipioCorredorSismico extends Base{

  protected $table = 'ries_corredoressis_municipiosries';

  protected $fillable = ['corredor_id','municipio_riesgo_id'];

  protected $with = ['municipioriesgo'];

  public function corredorsismico()
  {
    return $this->belongsTo('Modules\Riesgos\Entities\CorredorSismico');
  }

  public function municipioriesgo(){
    return $this->belongsTo('Modules\Riesgos\Entities\MunicipioRiesgo','municipio_riesgo_id','id');
  }
}
