<?php

namespace Modules\Riesgos\Entities;

use Modules\Riesgos\Entities\Base;

class MunicipioRiesgo extends Base{

  protected $table = 'ries_municipios_riesgos';

  protected $fillable = ['minicipio_id','riesgo_id','usuarios_id'];

  public function municipio()
  {
    return $this->belongsTo('App\Models\Municipio');
  }

  public function riesgo()
  {
    return $this->belongsTo('Modules\Riesgos\Entities\Riesgo');
  }

  public function corredormunicipio()
  {
      return $this->hasMany('Modules\Riesgos\Entities\MunicipioCorredorSismico','corredor_id','id');
  }
}
