<?php

namespace Modules\Riesgos\Entities;

use Modules\Riesgos\Entities\Base;

class Riesgo extends Base{

  protected $table = 'ries_cat_riesgos';

  protected $fillable = ['nombre'];

  public function municipios(){
    return $this->hasMany('Modules\Riesgos\Entities\MunicipioRiesgo');
  }

}
