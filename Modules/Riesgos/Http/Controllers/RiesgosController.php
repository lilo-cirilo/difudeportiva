<?php

namespace Modules\Riesgos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Region;
use Modules\Riesgos\Entities\Riesgo;
use Modules\Riesgos\Entities\CorredorSismico;

use  Modules\Riesgos\DataTables\riesgosxmunicipio;

class RiesgosController extends Controller{

  public function index(riesgosxmunicipio $tabla){
    $munXregion = [];
    foreach (Region::All() as $region) {
      $aux = [];
      foreach ($region->distritos as $distrito) {
        foreach ($distrito->municipios as $mun) {
          ///array_push($aux,);
          $aux[$mun->id] = ['nombre'=>$mun->nombre,'lat'=>$mun->latitud,'lng'=>$mun->longitud];
        }
      }
      $munXregion[$region->nombre]=["municipios"=>$aux,"estado"=>true];
    }
    $municipiosXriesgo = [];
    foreach (Riesgo::orderby('created_at')->get() as $riesgo) {
      $munRies = [];
      if($riesgo->nombre == 'SISMOS'){
        $aux=[];
        foreach (CorredorSismico::all() as $corredor ) {
          $munRies[$corredor->nombre] = $corredor->municipiosriesgocorredor->pluck('municipioriesgo.municipio_id');
          $aux[$corredor->nombre] = false;
        }
        $municipiosXriesgo[$riesgo->nombre] = ['municipios'=>$munRies, "estado"=>$aux];
      }else{
        $munRies = $riesgo->municipios->pluck('municipio_id');
        $municipiosXriesgo[$riesgo->nombre] = ['municipios'=>$munRies, "estado"=>false];
      }
    }
    //return view('riesgos::index',['regiones'=>json_encode($munXregion),'riesgos'=>json_encode($municipiosXriesgo)]);
    return $tabla->render('riesgos::index',['regiones'=>json_encode($munXregion),'riesgos'=>json_encode($municipiosXriesgo)]);
  }
}
