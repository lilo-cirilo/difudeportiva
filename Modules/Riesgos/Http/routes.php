<?php

Route::group(['middleware' => 'web', 'prefix' => 'riesgos', 'namespace' => 'Modules\Riesgos\Http\Controllers'], function()
{
    Route::get('/', 'RiesgosController@index')->name('riesgos.home');
});
