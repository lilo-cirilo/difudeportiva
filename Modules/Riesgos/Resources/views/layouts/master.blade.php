@extends('admin-lte::layouts.main')

@if(auth()->check())
@section('user-avatar')
  @php
    $userimg = auth()->user()->persona->get_url_fotografia();
  @endphp
  {{ asset($userimg) }}
@endsection
@section('user-name', auth()->user()->persona->nombre)
@section('user-job')
@section('user-log', auth()->user()->created_at)
@endif

@section('content-title', 'Evaluacion De Riesgos')

@section('sidebar-menu')
  <ul class="sidebar-menu" data-widget="tree" id="sidebar">
    <li class="treeview">
      <a href="#">
        <i class="fa fa-exclamation-triangle"></i> <span>Evaluacion de riesgos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('riesgos.home') }}"><i class="fa fa-exclamation"></i>Por municipio</a></li>
        {{-- <li><a href="{{ route('cecyd.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li> --}}
      </ul>
    </li>
  </ul>
@endsection


@push('body')
  <script>
    $('input[type="text"]').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    $('#sidebar').find('a').each(function (){
      //auto seleccionar elemento activos en la barra lateral
      if($(this).attr('href')==window.location){
        $(this).parent().addClass('active');//primer nivel
        $(this).parent().parent().parent('li').addClass('active');//segundo nivel
        $(this).parent().parent().parent().parent().parent('li').addClass('active');//tercer nivel
      }
    })
    function block() {
      $.blockUI({
        css: {
          border: 'none',
          padding: '0px',
          backgroundColor: 'none',
          '-webkit-border-radius': '10px',
          '-moz-border-radius': '10px',
          opacity: .8,
          color: '#fff'
        },
        baseZ: 10000,
        message: '<div align="center"><img style="width:168px; height:168px;" src="{{ asset('images/AsisAlimentaria/loader3.gif') }}"><br /><p style=" color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">Procesando...</p></div>',
      });
      
      function unblock_error() {
        if($.unblockUI())
        alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
      }
      
      setTimeout(unblock_error, 120000);
    }

    function unblock() {
      $.unblockUI();
    }
  </script>
@endpush