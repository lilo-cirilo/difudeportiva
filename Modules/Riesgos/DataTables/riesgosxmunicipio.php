<?php

namespace Modules\Riesgos\DataTables;

use Yajra\DataTables\Services\DataTable;

use Modules\Riesgos\Entities\MunicipioRiesgo;
use Modules\Riesgos\Entities\Riesgo;

class riesgosxmunicipio extends DataTable
{
  public function dataTable($query){
    return datatables($query)
    ->addColumn('convenio',function ($municipio){
      $siTiene = Riesgo::where('nombre','like','%APCE%')->first()->municipios;//->pluck('municipio_id');
      return $siTiene->contains('municipio_id',$municipio->clave_mun) ? 'EXISTENTE' : 'AUSENTE';
    })
    ->filterColumn('convenio',function ($query,$keyword){
      $siTiene = Riesgo::where('nombre','like','%APCE%')->first()->municipios->pluck('municipio_id');
      if(strtoupper($keyword)=='EXISTENTE')
        $query->whereIn('cat_municipios.id',$siTiene);
      else if(strtoupper($keyword)=='AUSENTE')
        $query->whereNotIn('cat_municipios.id',$siTiene);
    });
  }
  
  public function query(MunicipioRiesgo $model){
    return $model->newQuery()->join('ries_cat_riesgos','ries_cat_riesgos.id','ries_municipios_riesgos.riesgo_id')
    ->join('cat_municipios','cat_municipios.id','ries_municipios_riesgos.municipio_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
    ->where('ries_cat_riesgos.nombre','not like','%APCE%')
    ->select(
      'ries_cat_riesgos.id as id',
      'cat_municipios.id as clave_mun',
      'cat_municipios.nombre as municipio',
      'ries_cat_riesgos.nombre as riesgo',
      'cat_regiones.nombre as region',
      'cat_distritos.nombre as distrito'
    );
  }
  
  public function html(){
    return $this->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());  
  }
  
  protected function getColumns()
  {
    return [
      'region'          => ['data'      => 'region'    , 'name' => 'cat_regiones.nombre'],
      'clave_municipio' => ['data'      => 'clave_mun' , 'name' => 'cat_municipios.id'],
      'municipio'       => ['data'      => 'municipio' , 'name' => 'cat_municipios.nombre'],
      'riesgo'          => ['data'      => 'riesgo'    , 'name' => 'ries_cat_riesgos.nombre'],
      'distrito'        => ['data'      => 'distrito'  , 'name' => 'cat_distritos.nombre'],
      'convenio'        => ['orderable' => false]
    ];
  }
  protected function getBuilderParameters() {
    $builderParameters = [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      //'preDrawCallback' => 'function() { block(); }',
      //'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
      'buttons' => [
        [
          'extend' => 'export',
          //'text' => '<i class="fa fa-plus"></i> Nuevo',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => 'button-dt tool'
        ]
      ],
      'lengthMenu' => [ [10], [10] ],
      'responsive' => true,
      'columnDefs' => [
          //[ 'className' => 'text-center', 'targets' => [] ],
          [ 'visible' => false, 'targets' => 0 ]
      ],
      'rowGroup' => [
        'dataSrc' => 'region'
      ]
    ];

    return $builderParameters;
  }
  protected function filename()
  {
    return 'Riesgos por municipio ' . date('YmdHis');
  }
}
