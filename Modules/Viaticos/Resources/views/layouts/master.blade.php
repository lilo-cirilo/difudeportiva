<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Viaticos</title>
    <!-- Main styles for this application-->
    <link href="{{ asset('viaticosm/app.css') }}" rel="stylesheet">
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    
    @include('viaticos::layouts.includes.navbar')

    <div class="app-body">
      
      @include('viaticos::layouts.includes.sidebar')

      <main class="main">
        
        @include('viaticos::layouts.includes.breadcrumb')

        <div class="container-fluid">
          <div class="animated fadeIn">

            @yield('content')
          
          </div>
        </div>

      </main>

      @include('viaticos::layouts.includes.aside')  

    </div>
    <footer class="app-footer">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span>Unidad de Informática</span>
      </div>
    </footer>
    <script src="{{ asset('viaticosm/app.js') }}"></script>
  </body>
</html>
