// window.Vue = require('vue');
import Vue from 'vue'
import VueRouter from 'vue-router'
import BlockUI from 'vue-blockui'

try {
  window.$ = window.jQuery = require('jquery');
} catch (e) {}
require('popper.js');
require('bootstrap');
require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
require('@coreui/coreui/dist/js/coreui.min.js');

Vue.use(VueRouter)
Vue.use(BlockUI)

import Home from './components/Home.vue';
// Vue.component('example-component', require('./components/Home.vue').default);

const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    }
  ],
});

const app = new Vue({
  el: '#app',
  // components: {  },
  router,
});