<?php

namespace Modules\Viaticos\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerificarToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$usuario = auth()->user()){
            return redirect('login');
        }
        $jwt = $request->header('mitoken');
        if(!$jwt && !$request->token){
            return new JsonResponse(['message' => 'Hace falta el token'], 401);
        }
        try{
            if($jwt){
                $inf = JWT::decode($jwt, 'mitoken', ['HS256']);
            }else{
                $inf = JWT::decode($request->token, 'mitoken', ['HS256']);
            }
            if(!$usuario = Usuario::find($inf->usuario_id) && $usuario->vida_token){

            }
            $request['usuario_id'] = $inf->usuario_id;
            $request['modulo_id'] = $inf->modulo_id;
            $request['rol_id'] = $inf->rol_id;
            return $next($request);
        } catch(\UnexpectedValueException $e){
            return new JsonResponse(['message' => 'Token incorrecto'], 403);
        }
    }
}
