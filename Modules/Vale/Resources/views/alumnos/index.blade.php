@extends('vale::layouts.master')

@section('content-title', '')

@section('content-subtitle', '')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<!-- <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"> -->
<!-- sweetalert2 -->
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
select[readonly] {
	background: #eee;
	cursor: no-drop;
}

select[readonly] option {
	display: none;
}

.modal-body {
    overflow-y: auto;
	max-height: calc(100vh - 210px);
}

.disabled-select {
    background-color: #d5d5d5;
    opacity: 0.5;
    border-radius: 3px;
    cursor: not-allowed;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
}

select[readonly].select2-hidden-accessible + .select2-container {
    pointer-events: none;
    touch-action: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection {
    background-color: white;
    box-shadow: none;
}

select[readonly].select2-hidden-accessible + .select2-container .select2-selection__arrow,
select[readonly].select2-hidden-accessible + .select2-container .select2-selection__clear {
    display: none;
}
</style>
@endpush

@section('content')
<div id="app">
    <h1></h1>
    <p>
        <router-link to="/index">Go to Index</router-link>
        <router-link to="/create">Go to Create</router-link>
    </p>
    
    <div v-show="['index'].indexOf($route.name) > -1" class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">Tabla de Alumnos:</h3>
        </div>
        
        <div class="box-body">
        {!! $alumnos->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'alumnos', 'name' => 'alumnos', 'style' => 'width: 100%']) !!}
        </div>
    </div>
    <!--
    <keep-alive include="index">
    <router-view></router-view>
    </keep-alive>
    -->
    <router-view></router-view>

    <div class="modal fade" id="modal-personas">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
    				<h4 class="modal-title" id="modal-title-personas"></h4>
    			</div>
    			<div class="modal-body" id="modal-body-personas">
                {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
                </div>
    			<div class="modal-footer" id="modal-footer-personas">
    				<div class="pull-right">
                        <button type="button" class="btn btn-success" onclick="bus.$emit('agregarPersona');"><i class="fa fa-plus"></i> Agregar</button>
    					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="modal fade" id="modal-persona">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
    				<h4 class="modal-title" id="modal-title-persona"></h4>
    			</div>
    			<div class="modal-body" id="modal-body-persona">
                <create-edit-persona template="Modal"></create-edit-persona>
                </div>
    			<div class="modal-footer" id="modal-footer-persona">
    				<div class="pull-right">
                        <!--<button type="submit" class="btn btn-primary" @click.prevent="formValidar"><i class="fa fa-database"></i> Guardar</button>-->
    					<!--<button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick=""><i class="fa fa-plus"></i> Agregar</button>-->
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>

<!-- sweetalert2 -->
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/axios.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/bluebird/latest/bluebird.min.js"></script>

<script>
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

$.fn.modal.prototype.constructor.Constructor.DEFAULTS.backdrop = 'static';
$.fn.modal.prototype.constructor.Constructor.DEFAULTS.keyboard =  false;

const bus = new Vue();

const CreateEdit = Vue.component('create', {
    name: 'create',
    template: `
    <div class="box box-primary shadow">
        <form id="formGrupo" name="formGrupo">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ title }}</h3>
            </div>
            
            <div class="box-body">
            
                <div v-show="['edit'].indexOf($route.name) > -1" class="form-group">
                    <label for="id">Id:</label>
                    @{{ alumno.id }}
                </div>
                
                <div class="form-group">
                    <label for="matricula">Matricula:</label>
                    <input type="text" class="form-control" id="matricula" name="matricula" v-model="alumno.matricula" @input="setUpperCase($event, alumno, 'matricula')">
				</div>
                
                <div class="form-group">
                    <label for="persona_id">Persona:</label>
                    <div class="input-group">
                        <select id="persona_id" name="persona_id" class="form-control select2 input-persona" style="width: 100%;" readonly="readonly">
                        </select>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" @click.prevent="buscarAlumno()">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
				</div>
                
                <div class="pull-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" @click.prevent="validarForm()"><i class="fa fa-database"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
    `,
    data() {
        return {
            title: 'Crear Alumno:',
            form: null,
            formValidate: null,
            persona: null,
            alumno: {
                id: null,
                persona_id: null,
                matricula: null
            }
        }
    },
    watch: {
        '$route' (to, from) {
            if(typeof this.$route.params.editId !== 'undefined') {
                this.title = 'Editar Alumno:';
            }
            else {
                this.title = 'Crear Alumno:';
            }
            this.formValidate.resetForm();
            this.alumno.id = null;
            this.alumno.persona_id = null;
            this.alumno.matricula = null;
        }
    },
    created() {
        bus.$on('seleccionarPersona', (id, nombre) => {
            $('#persona_id').html('').select2();
            var option = new Option(nombre, id, true, true);
            this.persona.append(option).trigger('change');
            $('#modal-personas').modal('hide');
        });
    },
    beforeDestroy() {
        bus.$off('seleccionarPersona');
    },
    mounted() {
        let data = this;
        
        data.persona = $('#persona_id');
        
        data.persona.select2({
            language: 'es'
        }).change(function(event) {
            data.alumno.persona_id = event.currentTarget.value;
            data.persona.valid();
        });
        
        if(typeof this.$route.params.editId !== 'undefined') {
            this.title = 'Editar Alumno:';
            this.show(this.$route.params.editId);
        }
        
        this.form = $('#formGrupo');

        this.formValidate = this.form.validate({
            rules: {
                matricula: {
                    required: true
                },
                persona_id: {
                    required: true
                }
            },
            messages: {
            }
        });
    },
    methods: {
        setUpperCase(e, o, prop) {
            const start = e.target.selectionStart;
            e.target.value = e.target.value.toUpperCase();
            this.$set(o, prop, e.target.value);
            e.target.setSelectionRange(start, start);
        },
        validarForm(e) {
            if(this.form.valid()) {
                block();
                if(typeof this.$route.params.editId !== 'undefined') {
                    this.update();
                }
                else {
                    this.create();
                }
            }
        },
        buscarAlumno() {
            $('#modal-personas').modal('show');
            
            $('#modal-personas').on('shown.bs.modal', function(event) {
                window.LaravelDataTables['personas'].ajax.reload();
			});
        },
        create() {
            axios.post('/vale/alumnos', {
                alumno: this.alumno
            })
            .then(function(response) {
                unblock();
                console.log(response);
                window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                router.push({ name: 'index' });
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.alumno) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            })
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        },
        show(id) {
            axios.get('/vale/alumnos/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.alumno.id = response.data.alumno.id;
                this.alumno.persona_id = response.data.alumno.persona_id;
                this.alumno.matricula = response.data.alumno.matricula;
                if(this.alumno.persona !== null) {
                    var nombre = response.data.alumno.persona.nombre + ' ' + response.data.alumno.persona.primer_apellido + ' ' + response.data.alumno.persona.segundo_apellido;
                    var option = new Option(nombre, response.data.alumno.persona.id, true, true);
                    
                    this.persona.append(option).trigger('change');
                }
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.alumno) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    router.push({ name: 'index' });
                }
            });
        },
        update() {
            axios.put('/vale/alumnos/' + this.alumno.id, {
                alumno: this.alumno
            })
            .then(function(response) {
                unblock();
                console.log(response);
                window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                router.push({ name: 'index' });
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.alumno) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            })
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        }
    }
});

const Destroy = Vue.component('destroy', {
    name: 'destroy',
    template: `
    <div class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">@{{ title }}</h3>
        </div>
            
        <div class="box-body">
            
            <div class="form-group">
                <label>Id:</label>
                @{{ alumno.id }}
            </div>
                
            <div class="form-group">
                <label>Persona:</label>
                @{{ alumno.persona_id }}
            </div>

            <div class="form-group">
                <label>Matricula:</label>
                @{{ alumno.matricula }}
            </div>
                
            <div class="pull-right">
                <div class="box-footer">
                    <button type="submit" class="btn btn-danger" @click.prevent="destroy()"><i class="fa fa-trash"></i> Eliminar</button>
                </div>
            </div>

        </div>
        
    </div>
    `,
    data() {
        return {
            title: 'Eliminar Alumno:',
            alumno: {
                id: null,
                persona_id: null,
                matricula: null
            }
        }
    },
    mounted() {
        this.show(this.$route.params.destroyId);
    },
    methods: {
        show(id) {
            axios.get('/vale/alumnos/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.alumno.id = response.data.alumno.id;
                this.alumno.persona_id = response.data.alumno.persona_id;
                this.alumno.matricula = response.data.alumno.matricula;
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.alumno) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    router.push({ name: 'index' });
                }
            });
        },
        destroy() {
            swal({
				title: '',
				text: '',
				type: 'info',
                showConfirmButton: true,
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Eliminar',
				cancelButtonText: 'Cancelar',
				allowEscapeKey: false,
				allowOutsideClick: false
			}).then((result) => {
                if(result.value) {
                    axios.delete('/vale/alumnos/' + this.alumno.id, {})
                    .then(function(response) {
                        unblock();
                        console.log(response);
                        window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                        router.push({ name: 'index' });
                        Swal({
                            type: 'success',
                            title: response.status + ' ' + response.statusText,
                            html: '<code>' + JSON.stringify(response.data.alumno) + '</code>',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }.bind(this))
                    .catch(function(error) {
                        if(error.response) {
                            unblock();
                            console.log(error.response);
                            window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                            Swal({
                                type: 'error',
                                title: error.response.status + ' ' + error.response.statusText,
                                text: error.response.data.message,
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
                        else if(error.request) {
                            unblock();
                            console.log(error.request);
                            window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                        else {
                            unblock();
                            console.log('Error', error.message);
                            window.LaravelDataTables['alumnos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                    });
                }

				if(result.dismiss) {
                }
                
                if(result.dismiss === swal.DismissReason.timer) {
                }
			});
        }
    }
});

const CreateEditPersona = Vue.component('create-edit-persona', {
    name: 'CreateEditPersona',
    template: `
    <!--<div class="box box-primary shadow">-->
        <form id="form_persona" name="form_persona">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ title }}</h3>
            </div>
            
            <div class="box-body">

                <div class="row">
                
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                        <div class="hovereffect">
                            <img v-bind:src="persona.fotografia" class="img-thumbnail center-block" id="imagen" name="imagen">
                            <div class="overlay">
                                <h2>Acciones</h2>
                                <button type="button" class="btn btn-success" @click.prevent="agregarFotografia"><i class="fa fa-database"></i> Agregar</button>
                                <button type="button" class="btn btn-danger" @click.prevent="eliminarFotografia"><i class="fa fa-trash"></i> Eliminar</button>
                                <input ref="file" type="file" id="fotografia" name="fotografia" @change="onFileChange" style="display: none;">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

                        <div class="row" style="padding-left: 0; padding-right: 0;">
                        
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="nombre">Nombre:</label>
                                    <input type="text" class="form-control" id="nombre" name="nombre" v-model="persona.nombre" @input="setUpperCase($event, persona, 'nombre')">
                                </div>
                            </div>

                        </div>

                        <div class="row" style="padding-left: 0; padding-right: 0;">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="primer_apellido">Apellido Paterno:</label>
                                    <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" v-model="persona.primer_apellido" @input="setUpperCase($event, persona, 'primer_apellido')">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="segundo_apellido">Apellido Materno:</label>
                                    <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" v-model="persona.segundo_apellido" @input="setUpperCase($event, persona, 'segundo_apellido')">
                                </div>
                            </div>

                        </div>

                        <div class="row" style="padding-left: 0; padding-right: 0;">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="genero">Género:</label>
                                    <select id="genero" name="genero" class="form-control select2" style="width: 100%;" v-model="persona.genero">
                                        <option value="M">MASCULINO</option>
                                        <option value="F">FEMENINO</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                                    <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" v-model="persona.fecha_nacimiento">
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-left: 0; padding-right: 0;">

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="curp">Curp:</label>
                                    <input type="text" class="form-control" id="curp" name="curp" v-model="persona.curp" @input="setUpperCase($event, persona, 'curp')">
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="clave_electoral">Clave Electoral:</label>
                                    <input type="text" class="form-control" id="clave_electoral" name="clave_electoral" v-model="persona.clave_electoral" @input="setUpperCase($event, persona, 'clave_electoral')">
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <h4>Dirección:</h4>
                <hr>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                        <div class="form-group">
                            <label for="calle">Calle:</label>
                            <input type="text" class="form-control" id="calle" name="calle" v-model="persona.calle" @input="setUpperCase($event, persona, 'calle')">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                        <div class="form-group">
                            <label for="numero_exterior">No. Exterior:</label>
                            <input type="text" class="form-control" id="numero_exterior" name="numero_exterior" v-model="persona.numero_exterior" @input="setUpperCase($event, persona, 'numero_exterior')">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                        <div class="form-group">
                            <label for="numero_interior">No. Interior:</label>
                            <input type="text" class="form-control" id="numero_interior" name="numero_interior" v-model="persona.numero_interior">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                        <div class="form-group">
                            <label for="colonia">Colonia:</label>
                            <input type="text" class="form-control" id="colonia" name="colonia" v-model="persona.colonia" @input="setUpperCase($event, persona, 'colonia')">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                        <div class="form-group">
                            <label for="codigopostal">Código Postal:</label>
                            <input type="text" class="form-control" id="codigopostal" name="codigopostal" v-model="persona.codigopostal">
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                        <div class="form-group">
                            <label for="etnia_id">Etnia:</label>
                            <select id="etnia_id" name="etnia_id" class="form-control select2" style="width: 100%;" v-model="persona.etnia_id">
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="municipio_id">Municipio:</label>
                            <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;" v-model="persona.municipio_id">
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="localidad_id">Localidad:</label>
                            <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;" v-model="persona.localidad_id">
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="referencia_domicilio">Referencias del Domicilio:</label>
                            <textarea class="form-control" rows="3" id="referencia_domicilio" name="referencia_domicilio" v-model="persona.referencia_domicilio" @input="setUpperCase($event, persona, 'referencia_domicilio')"></textarea>
                        </div>
                    </div>

                </div>

                <h4>Contacto:</h4>
                <hr>

                <div class="row">

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                        <div class="form-group">
                            <label for="numero_celular">Celular:</label>
                            <input type="text" class="form-control" id="numero_celular" name="numero_celular" v-model="persona.numero_celular" data-inputmask='"mask": "(999) 999-999-9999"' data-mask>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                        <div class="form-group">
                            <label for="numero_local">Télefono:</label>
                            <input type="text" class="form-control" id="numero_local" name="numero_local" v-model="persona.numero_local" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="form-group">
                            <label for="email">Correo Electrónico:</label>
                            <input type="text" class="form-control" id="email" name="email" v-model="persona.email" @input="setUpperCase($event, persona, 'email')">
                        </div>
                    </div>

                </div>

                <div class="pull-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" @click.prevent="formValidar"><i class="fa fa-database"></i> Guardar</button>
                    </div>
                </div>

            </div>
        </form>
        <!--</div>-->
    `,
    props: ['template'],
    data() {
        return {
            title: 'Agregar Persona:',
            form: null,
            form_data: new FormData(),
            form_validate: null,
            genero: null,
            fecha_nacimiento: null,
            etnia: null,
            municipio: null,
            localidad: null,
            tipo_template: this.template,
            persona: {
                id: null,
                nombre: null,
                primer_apellido: null,
                segundo_apellido: null,
                calle: null,
                numero_exterior: null,
                numero_interior: null,
                colonia: null,
                codigopostal: null,
                etnia_id: null,
                localidad_id: null,
                curp: null,
                genero: 'M',
                fecha_nacimiento: null,
                clave_electoral: null,
                numero_celular: null,
                numero_local: null,
                email: null,
                referencia_domicilio: null,
                fotografia: null,
                imagen: false,
                municipio_id: null
            }
        }
    },
    /*watch: {
        '$route' (to, from) {
            if(typeof this.$route.params.editId !== 'undefined') {
                this.title = 'Editar Grupo:';
            }
            else {
                this.title = 'Crear Grupo:';
            }
            this.form_validate.resetForm();
            this.grupo.id = 0;
            this.grupo.nombre = '';
            this.grupo.descripcion = '';
            this.grupo.capacidad = '';
        }
    },*/
    created() {
        bus.$on('agregarPersona', (id, nombre) => {
            Object.assign(this.$data, this.$options.data());
            
            this.init();

            this.tipo_template = 'Modal';

            this.title = 'Agregar Persona:';
            
            $('#modal-persona').modal('show');
        });
        
        bus.$on('editarPersona', (id, nombre) => {
            Object.assign(this.$data, this.$options.data());
            
            this.init();

            this.tipo_template = 'Modal';

            this.title = 'Editar Persona:';
            
            $('#modal-persona').modal('show');

            this.show(id);
        });
    },
    beforeDestroy() {
        bus.$off('agregarPersona');

        bus.$off('editarPersona');
    },
    mounted() {
        /*$('#modal-persona').on('shown.bs.modal', function(e) {
            $('#form_persona').validate().resetForm();
        });*/

        $('#modal-persona').on('hide.bs.modal', function() {
            $('#form_persona').validate().resetForm();
        });
        
        this.init();

        if(this.tipo_template === 'Normal') {
            if(typeof this.$route.params.editId !== 'undefined') {
                this.title = 'Editar Persona:';
                this.show(this.$route.params.editId);
            }
        }
    },
    methods: {
        init() {
            let data = this;
            
            data.form = $('#form_persona');
            
            data.form_validate = data.form.validate({
                rules: {
                    nombre: {
                        required: true,
                        minlength: 3,
                        pattern_nombre: ''
                    },
                    primer_apellido: {
                        required: true,
                        minlength: 1,
                        pattern_apellido: ''
                    },
                    segundo_apellido: {
                        required: true,
                        minlength: 1,
                        pattern_apellido: ''
                    },
                    fecha_nacimiento: {
                        required: true
                    },
                    curp: {
                        required: false,
                        minlength: 18,
                        maxlength: 19,
                        pattern_curp: ''
                    },
                    clave_electoral: {
                        required: false,
                        minlength: 13,
                        pattern_numero: ''
                    },
                    calle: {
                        required: true,
                        minlength: 3
                    },
                    numero_exterior: {
                        required: true,
                        minlength: 1,
                        pattern_numero: ''
                    },
                    numero_interior: {
                        required: false,
                        minlength: 1,
                        pattern_numero: ''
                    },
                    colonia: {
                        required: true,
                        minlength: 3
                    },
                    codigopostal: {
                        required: false,
                        minlength: 5,
                        maxlength: 5,
                        pattern_integer: ''
                    },
                    municipio_id: {
                        required: true
                    },
                    localidad_id: {
                        required: false
                    },
                    referencia_domicilio: {
                        required: true,
                        minlength: 3
                    },
                    numero_celular: {
                        required: false,
                        pattern_telefono: ''
                    },
                    numero_local: {
                        required: false,
                        pattern_telefono: ''
                    },
                    email: {
                        required: false,
                        email: true
                    },
                    etnia_id: {
                        required: true
                    }
                },
                messages: {
                }
            });
            
            data.genero = $('#genero');

            data.fecha_nacimiento = $('#fecha_nacimiento');

            data.etnia = $('#etnia_id');
            
            data.municipio = $('#municipio_id');
            
            data.localidad = $('#localidad_id');
            
            $('[data-mask]').inputmask();

            data.genero.select2({
                language: 'es'
            })
            .change(function(event) {
                data.persona.genero = event.currentTarget.value;
            });

            data.fecha_nacimiento.datepicker({
                autoclose: true,
                language: 'es',
                startDate: '01-01-1900',
                endDate: '0d',
                orientation: 'bottom'
            })
            .change(function(event) {
                data.persona.fecha_nacimiento = event.currentTarget.value;
                data.fecha_nacimiento.valid();
            });

            data.etnia.select2({
                language: 'es',
                //minimumInputLength: 2,
                ajax: {
                    url: '{{ route('vale.etnias.select') }}',
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(function(event) {
                data.persona.etnia_id = event.currentTarget.value;
                data.etnia.valid();
            });
            
            data.municipio.select2({
                language: 'es',
                minimumInputLength: 2,
                ajax: {
                    url: '{{ route('municipios.select') }}',
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(function(event) {
                data.persona.municipio_id = event.currentTarget.value;
                data.municipio.valid();
                data.localidad.empty();
                data.localidad.select2({
                    language: 'es',
                    ajax: {
                        url: '{{ route('localidades.select') }}',
                        delay: 500,
                        dataType: 'JSON',
                        type: 'GET',
                        data: function(params) {
                            return {
                                search: params.term,
                                municipio_id: data.municipio.val()
                            };
                        },
                        processResults: function(data, params) {
                            params.page = params.page || 1;
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        id: item.id,
                                        text: item.nombre,
                                        slug: item.nombre,
                                        results: item
                                    }
                                })
                            };
                        },
                        cache: true
                    }
                }).change(function(event) {
                    data.persona.localidad_id = event.currentTarget.value;
                    data.localidad.valid();
                });
            });

            data.localidad.select2({
                language: 'es'
            });
            
            data.persona.fotografia = '/' + 'images/no-image.png';
        },
        setUpperCase(e, o, prop) {
            const start = e.target.selectionStart;
            e.target.value = e.target.value.toUpperCase();
            this.$set(o, prop, e.target.value);
            e.target.setSelectionRange(start, start);
        },
        formValidar(e) {
            let data = this;
            if(data.form.valid()) {
                block();
                
                if(data.persona.id !== null) {
                    data.update();
                }
                else {
                    data.create();
                }
            }
        },
        agregarFotografia() {
            $('#fotografia').click();
        },
        onFileChange(event) {
            let data = this;

            this.form_data.append('fotografia', event.target.files[0]);
            
            var file = event.target.files[0];

            var reader = new FileReader();

			reader.onloadend = function() {
                data.persona.fotografia = reader.result;

                data.persona.imagen = true;
			};

			reader.onerror = function() {
			};

			if(file) {
				reader.readAsDataURL(file);
			}
			else {
			}
        },
        eliminarFotografia() {
            this.form_data = new FormData();

            this.persona.fotografia = '/' + 'images/no-image.png';
            
            this.persona.imagen = false;
        },
        create() {
            this.form_data.append('persona', JSON.stringify(this.persona));

            axios.post('/vale/personas', this.form_data)
            .then(function(response) {
                unblock();let data = this;
                console.log(response);
                this.persona.id = response.data.persona.id;
                //window.LaravelDataTables['personas'].ajax.reload(null, false);
                //router.push({ name: 'index' });
                if(data.tipo_template === 'Modal') {
                    let nombre = data.persona.nombre + ' ' + data.persona.primer_apellido + ' ' + data.persona.segundo_apellido;console.log(nombre);
                    let id = data.persona.id;console.log(id);console.log(data.persona.id);
                    $('#persona_id').html('').select2();

                    //var option = new Option(data.persona.nombre + ' ' + data.persona.primer_apellido + ' ' + data.persona.segundo_apellido, data.persona.id, true, true);
                    //$('#persona_id').append(option).trigger('change');

                    var option = new Option(nombre, id, true, true);
                    $('#persona_id').append(option).trigger('change');

                    $('#modal-personas').modal('hide');
                    $('#modal-persona').modal('hide');
                }
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.persona) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    //window.LaravelDataTables['personas'].ajax.reload(null, false);
                    //router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        },
        show(id) {
            block();
            axios.get('/vale/personas/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.persona.id = response.data.persona.id;
                this.persona.nombre = response.data.persona.nombre;
                this.persona.primer_apellido = response.data.persona.primer_apellido;
                this.persona.segundo_apellido = response.data.persona.segundo_apellido;
                this.persona.calle = response.data.persona.calle;
                this.persona.numero_exterior = response.data.persona.numero_exterior;
                this.persona.numero_interior = response.data.persona.numero_interior;
                this.persona.colonia = response.data.persona.colonia;
                this.persona.codigopostal = response.data.persona.codigopostal;
                var fecha = response.data.persona.fecha_nacimiento.split('-');
                this.fecha_nacimiento.datepicker('setDate', fecha[2] + '/' + fecha[1] + '/' + fecha[0]);
                this.fecha_nacimiento.change();
                if(response.data.persona.etnia !== null) {
                    var option = new Option(response.data.persona.etnia.nombre, response.data.persona.etnia.id, true, true);
                    this.etnia.append(option).trigger('change');
                    this.persona.etnia_id = response.data.persona.etnia.id;
                }
                if(response.data.persona.municipio !== null) {
                    if(response.data.persona.municipio.distrito !== null) {
                        if(response.data.persona.municipio.distrito.region !== null) {
                            var option = new Option('(' + response.data.persona.municipio.distrito.region.nombre + ') (' + response.data.persona.municipio.distrito.nombre + ') ' + response.data.persona.municipio.nombre, response.data.persona.municipio.id, true, true);
                            this.municipio.append(option).trigger('change');
                            this.persona.municipio_id = response.data.persona.municipio.id;
                        }
                    }
                }
                if(response.data.persona.localidad !== null) {
                    var option = new Option(response.data.persona.localidad.nombre, response.data.persona.localidad.id, true, true);
                    this.localidad.append(option).trigger('change');
                    this.persona.localidad_id = response.data.persona.localidad.id;
                }
                this.persona.curp = response.data.persona.curp;
                this.persona.genero = response.data.persona.genero;
                this.genero.val(response.data.persona.genero).trigger('change');
                this.persona.clave_electoral = response.data.persona.clave_electoral;
                this.persona.numero_celular = response.data.persona.numero_celular;
                this.persona.numero_local = response.data.persona.numero_local;
                this.persona.email = response.data.persona.email;
                this.persona.referencia_domicilio = response.data.persona.referencia_domicilio;
                if(response.data.persona.fotografia !== null) {
                    this.persona.fotografia = response.data.persona.fotografia;
                    
                    this.persona.imagen = true;
                }
                else {
                    this.persona.fotografia = '/' + 'images/no-image.png';
                    
                    this.persona.imagen = false;
                }
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.persona) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    //router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    //router.push({ name: 'index' });
                }
            });
        },
        update() {
            this.form_data.append('persona', JSON.stringify(this.persona));
            
            axios.post('/vale/personas/' + this.persona.id + '?_method=PUT', this.form_data)
            .then(function(response) {
                unblock();
                console.log(response);
                window.LaravelDataTables['personas'].ajax.reload(null, false);
                //router.push({ name: 'index' });
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.persona) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            })
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    //window.LaravelDataTables['grupos'].ajax.reload(null, false);
                    //router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                }
            });
        }
    }
});

const DestroyPersona = Vue.component('destroy', {
    name: 'destroy',
    template: `
    <div class="box box-primary shadow">
        <form id="formGrupo" name="formGrupo">
            <div class="box-header with-border">
                <h3 class="box-title">@{{ title }}</h3>
            </div>
            
            <div class="box-body">
            
                <div class="form-group">
                    <label>Id:</label>
                    @{{ grupo.id }}
                </div>
                
                <div class="form-group">
                    <label>Nombre:</label>
                    @{{ grupo.nombre }}
                </div>

                <div class="form-group">
                    <label>Descripción:</label>
                    @{{ grupo.descripcion }}
                </div>

                <div class="form-group">
                    <label>Capacidad:</label>
                    @{{ grupo.capacidad }}
                </div>
                
                <div class="pull-right">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-danger" @click.prevent="destroy()"><i class="fa fa-trash"></i> Eliminar</button>
                    </div>
                </div>

            </div>
        </form>
    </div>
    `,
    data() {
        return {
            title: 'Eliminar Grupo:',
            grupo: {
                id: 0,
                nombre: '',
                descripcion: '',
                capacidad: ''
            }
        }
    },
    mounted() {
        this.show(this.$route.params.destroyId);
    },
    methods: {
        show(id) {
            axios.get('/vale/grupos/' + id, {})
            .then(function(response) {
                unblock();
                console.log(response);
                this.grupo.id = response.data.grupo.id;
                this.grupo.nombre = response.data.grupo.nombre;
                this.grupo.descripcion = response.data.grupo.descripcion;
                this.grupo.capacidad = response.data.grupo.capacidad;
                Swal({
                    type: 'success',
                    title: response.status + ' ' + response.statusText,
                    html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                    confirmButtonText: 'Cerrar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }.bind(this))
            .catch(function(error) {
                if(error.response) {
                    unblock();
                    console.log(error.response);
                    router.push({ name: 'index' });
                    Swal({
                        type: 'error',
                        title: error.response.status + ' ' + error.response.statusText,
                        text: error.response.data.message,
                        confirmButtonText: 'Cerrar',
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    });
                }
                else if(error.request) {
                    unblock();
                    console.log(error.request);
                    router.push({ name: 'index' });
                }
                else {
                    unblock();
                    console.log('Error', error.message);
                    router.push({ name: 'index' });
                }
            });
        },
        destroy() {
            swal({
				title: '',
				text: '',
				type: 'info',
                showConfirmButton: true,
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonClass: 'btn btn-success',
				cancelButtonClass: 'btn btn-danger',
				confirmButtonText: 'Eliminar',
				cancelButtonText: 'Cancelar',
				allowEscapeKey: false,
				allowOutsideClick: false
			}).then((result) => {
                if(result.value) {
                    axios.delete('/vale/grupos/' + this.grupo.id, {})
                    .then(function(response) {
                        unblock();
                        console.log(response);
                        window.LaravelDataTables['grupos'].ajax.reload(null, false);
                        router.push({ name: 'index' });
                        Swal({
                            type: 'success',
                            title: response.status + ' ' + response.statusText,
                            html: '<code>' + JSON.stringify(response.data.grupo) + '</code>',
                            confirmButtonText: 'Cerrar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    }.bind(this))
                    .catch(function(error) {
                        if(error.response) {
                            unblock();
                            console.log(error.response);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                            Swal({
                                type: 'error',
                                title: error.response.status + ' ' + error.response.statusText,
                                text: error.response.data.message,
                                confirmButtonText: 'Cerrar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                        }
                        else if(error.request) {
                            unblock();
                            console.log(error.request);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                        else {
                            unblock();
                            console.log('Error', error.message);
                            window.LaravelDataTables['grupos'].ajax.reload(null, false);
                            router.push({ name: 'index' });
                        }
                    });
                }

				if(result.dismiss) {
                }
                
                if(result.dismiss === swal.DismissReason.timer) {
                }
			});
        }
    }
});

const routes = [
    {
        path: '/',
        redirect: '/index'
    },
    {
        path: '/index',
        name: 'index'
    },
    {
        path: '/create',
        name: 'create',
        component: CreateEdit
    },
    {
        path: '/show/:showId',
        name: 'show',
        component: CreateEdit
    },
    {
        path: '/edit/:editId',
        name: 'edit',
        component: CreateEdit
    },
    {
        path: '/destroy/:destroyId',
        name: 'destroy',
        component: Destroy
    }
]

const router = new VueRouter({
    routes
});

const app = new Vue({
    router,
    data() {
        return {}
    },
    methods: {},
    components: { CreateEditPersona }
}).$mount('#app');
</script>
{!! $alumnos->html()->scripts() !!}
{!! $personas->html()->scripts() !!}
@endpush