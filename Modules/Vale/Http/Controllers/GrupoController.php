<?php

namespace Modules\Vale\Http\Controllers;

use App\DataTables\Vale\GruposDataTable;
use Modules\Vale\Entities\Grupo;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class GrupoController extends Controller {
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(GruposDataTable $dataTable) {
        return $dataTable
        ->render('vale::grupos.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('vale::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $grupo = Grupo::create($request->grupo);
                
                DB::commit();
                
                return response()->json(['grupo' => $grupo], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request, $id) {
        if($request->ajax()) {
            try {
                $grupo = Grupo::find($id);
                
                if(!$grupo) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                return response()->json(['grupo' => $grupo], 200);
            }
            catch(\Exception $e) {
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('vale::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $grupo = Grupo::find($id);

                if(!$grupo) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }
                
                $grupo->update($request->grupo);
                
                DB::commit();
                
                return response()->json(['grupo' => $grupo], 201);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                
                $grupo = Grupo::find($id);

                if(!$grupo) {
                    return response()->json(['message' => 'No encontrado.'], 404);
                }

                $tmp = $grupo;
                
                $grupo->delete();
                
                DB::commit();
                
                return response()->json(['grupo' => $tmp], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                
                return $e->getMessage();
            }
        }
        else {
            abort(403, 'Acción no autorizada.');
        }
    }
}