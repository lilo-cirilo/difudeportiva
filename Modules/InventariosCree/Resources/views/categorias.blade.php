{{-- Extender el layout del módulo --}}
@extends('inventarioscree::layouts.master')

{{-- Nombre de la página actual --}}
@section('title')
	@parent - Categorías
@endsection

{{-- Título y subtítulo --}}
@section('content-title', 'Categorías')
@section('content-subtitle', 'Módulo de administración')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
	<ol class="breadcrumb">
		<li>
			<a href="{{ route('inventarios-cree.index') }}">
				<i class="fa fa-home"></i>
				Inicio
			</a>
		</li>
		<li class="active">
			<i class="fa fa-tags"></i>
      		Categorías
      	</li>
	</ol>
@endsection

{{-- Contenido principal --}}
@section('content')
    <div id="categorias">
    	<div class="row">
			<div class="col-md-12">
				<!-- Box con tabla de documentos y sus opciones -->
				<div class="box box-primary">
					<!-- Header del box -->
					<div class="box-header with-border">
						<div class="row">
							<div class="col-md-6">
                                <div class="form-group">
    								<button type="button" class="btn btn-primary" @click="abrirModal('registrar')">
    									<i class="fa fa-plus"></i>
    									Nuevo
    								</button>
                                </div>
							</div>
							<div class="col-md-4 pull-right">
								<div class="input-group">
	                  				<input type="text" class="form-control" placeholder="Buscar..." v-model="search">

	              					<span class="input-group-addon">
	              						<i class="fa fa-search"></i>
	              					</span>
	                			</div>
							</div>
						</div>
					</div>

					<!-- Body del box -->
					<div class="box-body table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Descripción</th>
									<th>Activar / Inactivar</th>
									<th>Editar</th>
								</tr>
							</thead>
							<tbody>
                                <tr v-if="!categorias_filtered.length">
                                    <td colspan="4" class="text-center">
                                        <strong>No hay resultados</strong>
                                    </td>
                                </tr>
								<tr v-for="categoria in categorias_filtered">
									<td v-text="categoria.nombre"></td>
									<td v-text="categoria.descripcion"></td>
									<td>
                                        <button type="button" :class="{ 'btn': true, 'btn-danger': categoria.active, 'btn-info': !categoria.active }" @click="setStatus(categoria.id, !categoria.active)">
                                            <i :class="{ 'fa': true, 'fa-remove': categoria.active, 'fa-undo': !categoria.active }"></i>
                                        </button>
									</td>
									<td>
										<button type="button" class="btn btn-success" @click="abrirModal('editar', categoria)">
											<i class="fa fa-edit"></i>
										</button>
									</td>
								</tr>
							</tbody>
						</table>
					</div>

					<!-- Footer del box -->
					<div class="box-footer clearfix">
              			<ul class="pagination pagination-sm no-margin pull-right">
                			<li :class="previousClasses">
                				<a href="#" @click="setPaginaActual(paginas.actual - 1)">
                					<i class="fa fa-angle-left"></i>
                				</a>
                			</li>
                            <li :class="{ 'active': pagina == paginas.actual }" v-for="pagina in paginas.total">
                                <a href="#" @click="setPaginaActual(pagina)" v-text="pagina"></a>
            				</li>
            				<li :class="nextClasses">
            					<a href="#" @click="setPaginaActual(paginas.actual + 1)">
            						<i class="fa fa-angle-right"></i>
            					</a>
            				</li>
              			</ul>
            		</div>
				</div>

				<!-- Modal -->
				<div class="modal fade" id="modal">
					<div class="modal-dialog modal-lg">
            			<div class="modal-content">
              				<!-- Header del modal -->
              				<div class="modal-header">
                				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  					<span aria-hidden="true">
                  						<i class="fa fa-remove"></i>
                  					</span>
                  				</button>

            					<h4 class="modal-title" v-text="modal.titulo"></h4>
              				</div>

              				<!-- Formulario del modal -->
              				<form @submit.prevent="validateBeforeSubmit" role="form">
              					<!-- Body del modal -->
              					<div class="modal-body">
                					<div class="row">
                						<div class="col-md-6">
                							<div :class="{ 'form-group': true, 'has-error': errors.has('nombre') }">
                								<label class="control-label" for="nombre">Nombre</label>

                								<div class="input-group">
                									<div class="input-group-addon">
                										<i class="fa fa-pencil"></i>
                									</div>

                									<input type="text" class="form-control" id="nombre" name="nombre" v-model="formulario.nombre" v-validate="'required'">
                								</div>

                                                <span class="help-block" v-text="errors.first('nombre')"></span>
                							</div>
                						</div>

                						<div class="col-md-6">
                							<div class="form-group">
                  								<label class="control-label" for="descripcion">Descripción</label>
                  								<textarea class="form-control" rows="3" id="descripcion" name="descripcion" v-model="formulario.descripcion"></textarea>
                							</div>
                						</div>
                					</div>
              					</div>

	              				<!-- Footer del modal -->
	              				<div class="modal-footer">
	                				<button type="button" class="btn btn-default" data-dismiss="modal" v-text="'Cancelar'"></button>
	                				<button type="submit" class="btn btn-primary" v-text="modal.boton"></button>
	              				</div>
	              			</form>
            			</div>
          			</div>
        		</div>

				<!-- Loader -->
                <transition name="fade">
                    <div id="loader-container" v-if="loader">
                        <div id="loader-image" class="la-timer la-3x">
                            <div></div>
                        </div>

                        <h1 id="loader-text">Cargando...</h1>
                    </div>
                </transition>
			</div>
		</div>
    </div>
@stop

{{-- Script de vue --}}
@section('scripts')
	@parent

	<script type="text/javascript" charset="utf-8">
		new Vue({
			el: '#categorias',
			data: {
				formulario: {
					id: 0,
                    nombre: '',
					descripcion: ''
				},
                categorias: [],
                paginas: {
                    total: 0,
                    actual: 1
                },
                modal: {
					titulo: '',
					boton: '',
					accion: ''
				},
                loader: false,
                search: ''
			},
			computed: {
				previousClasses: function() {
                    return {
                        'disabled': this.paginas.actual == 1
                    }
                },
                nextClasses: function() {
                    return {
                        'disabled': this.paginas.actual == this.paginas.total
                    }
                },
                categorias_filtered: function() {
                    let categorias = this.categorias;

                    if(this.search)
                    {
                        categorias = categorias.filter(categoria => {
                            let search = this.search.toLowerCase();

                            return categoria.nombre.toLowerCase().match(search) || (categoria.descripcion ? categoria.descripcion.toLowerCase() : '').match(search);
                        });
                    }

                    this.paginas.total = Math.ceil(categorias.length / 10);

                    if(categorias.length < 10) this.paginas.actual = 1;
                    
                    if(this.paginas.total < this.paginas.actual) this.paginas.actual = 1;

                    if(categorias.length >= 10)
                    {
                        let n = this.paginas.actual * 10;

                        return categorias.slice(n - 10, n);
                    }

                    return categorias;
                }
			},
			methods: {
				getCategorias: function() {
					let me = this;
                    this.loader = true;

                    axios.get('/inventarios-cree/categorias/getCategorias').then(function(response) {
                        me.categorias = response.data;
                        me.loader = false;
                    })
                    .catch(function (error) {
                        me.mostrarAlerta(true, error.message);
                    });
				},
				setPaginaActual(pagina) {
                    if(pagina > 0 && pagina <= this.paginas.total && this.paginas.actual != pagina) this.paginas.actual = pagina;
                },
            	abrirModal(accion, data = {}) {
                    this.$validator.reset();

            		this.modal.accion = accion;
            		this.modal.titulo = 'Editar categoría';
					this.modal.boton = 'Actualizar';

            		if(accion == 'registrar')
					{
						this.modal.titulo = 'Registrar categoría';
						this.modal.boton = 'Registrar';
					}

            		this.formulario.id = data.id ? data.id : 0;
                    this.formulario.nombre = data.nombre ? data.nombre : '';
					this.formulario.descripcion = data.descripcion ? data.descripcion : '';

					$('#modal').modal();
				},
                validateBeforeSubmit() {
                    let me = this;

                    this.$validator.validateAll().then((result) => {
                        if(result) me.accionForm();
                    });
                },
				accionForm() {
					let me = this;
		            let metodo = 'put';
		            let uri = 'actualizar';
		            let msg = 'actualizada';

		            this.loader = true;

		            if(this.modal.accion == 'registrar')
		            {
		                metodo = 'post';
		                uri = 'registrar';
		                msg = 'registrada';
		            }

		            axios({
		                method: metodo,
		                url: 'categorias/' + uri,
		                data: me.formulario
		            })
		            .then(function() {
		                me.getCategorias();
		                me.mostrarAlerta(false, 'Categoría ' + msg);

		                $('#modal').modal('hide');
		            })
		            .catch(function(err) {
		                me.mostrarAlerta(true, err);
		            });
				},
                setStatus(id, status) {
                    let me = this;

                    swal({
                        title: '¿Está seguro de ' + (status ? 'activar' : 'inactivar') + ' esta categoría?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Aceptar',
                        cancelButtonText: 'Cancelar'
                    }).then((result) => {
                        if (result.value)
                        {
                            this.loader = true;

                            axios.put('categorias/setStatus', {
                                id: id,
                                status: status
                            })
                            .then(function() {
                                me.getCategorias();
                                me.mostrarAlerta(false, 'Categoría actualizada');
                            })
                            .catch(function(error) {
                                me.mostrarAlerta(true, error);
                            });
                        }
                    });
                },
				mostrarAlerta(error, mensaje) {
					this.loader = false;

                    if(error) 
                    {
                        swal({
                            type: 'error',
                            title: mensaje
                        });
                    }
                    else
                    {
                        swal({
                            type: 'success',
                            title: mensaje,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
			},
			mounted: function() {
            	let me = this;

            	this.getCategorias();
            }
		});
	</script>
@stop