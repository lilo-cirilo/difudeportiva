<?php

namespace Modules\InventariosCree\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\InventariosCree\Articulo;
use Modules\InventariosCree\Http\Requests\ArticuloRequest;

class ArticuloController extends Controller
{
    public function index(Request $request)
    {
        return view('inventarioscree::articulos');
    }

    public function getArticulos(Request $request)
    {
        if(!$request->ajax()) return redirect('/');

        return Articulo::with('categoria')->where('area_id', 1)->get();
    }

    public function store(ArticuloRequest $request)
    {
        if(!$request->ajax()) return redirect('/');

        DB::transaction(function() use($request) {
            $articulo = new Articulo();
            $articulo->nombre = $request->nombre;
            $articulo->descripcion = $request->descripcion;
            $articulo->categoria()->associate($request->categoria_id);
            $articulo->area()->associate(1); // Pendiente de saber que área debe ir aquí
            $articulo->save();
        });
    }

    public function update(ArticuloRequest $request)
    {
        if(!$request->ajax()) return redirect('/');

        DB::transaction(function() use($request) {
            $articulo = Articulo::findOrFail($request->id);
            $articulo->nombre = $request->nombre;
            $articulo->descripcion = $request->descripcion;
            $articulo->active = 1;
            $articulo->categoria()->associate($request->categoria_id);
            $articulo->save();
        });
    }

    public function setStatus(Request $request)
    {
        if(!$request->ajax()) return redirect('/');
        
        $articulo = Articulo::findOrFail($request->id);
        $articulo->active = $request->status;
        $articulo->save();
    }
}