<?php

namespace Modules\InventariosCree\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('inventarioscree::index');
    }
}
