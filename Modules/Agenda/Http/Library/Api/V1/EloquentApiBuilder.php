<?php

namespace Modules\Agenda\Http\Library\Api\V1;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

class EloquentApiBuilder {
    protected $model;
    protected $request;
    protected $data;
    protected $offset = 0;
    protected $per_page = 10;
    protected $per_page_start = 1;
    protected $per_page_finish = 100;
    
    function __construct(Model $model, Request $request) {
        $this->model = $model->select('*');
        $this->request = $request;

        $this->buildQuery($this->model, $this->request);//throw new \Exception('SOME EXCEPTION');
		}
		
		function buildQuery($model, $request) {
        if(isset($request->columns)) {
            $model->select(explode(',', $request->columns));
        }

        if(isset($request->filter)) {
            if(is_array($request->filter)) {
                $requestItems = $request->filter;
            }
            else {
                $requestItems = json_decode($request->filter);
            }
            
            foreach($requestItems as $requestItem) {
                $this->createFilter($model, $requestItem);
            }
        }
        
        if(isset($request->order_by)) {
            $requestItems = json_decode($request->order_by);
            
            foreach($requestItems as $requestItem) {
                $model->orderBy($requestItem->column, $requestItem->direction);
            }
        }
        
        if(isset($request->per_page)) {
            if(!($request->per_page < $this->per_page_start) && !($request->per_page > $this->per_page_finish)) {
                $this->per_page = $request->per_page;
            }
        }
        
        if(isset($request->with)) {
            if(is_array($request->with)) {
                $requestItems = $request->with;
            }
            else {
                $requestItems = json_decode($request->with);
            }
              
            foreach($requestItems as $requestItem) {
                $this->buildSubQuery($model, $requestItem);
            }
        }
    }
		
		function buildSubQuery($model, $request) {
				$model->with([$request->table => function($query) use ($request) {
            $this->buildQuery($query, $request);
				}]);
		}
    
    function createFilter($model, $request) {
        //$method = strtolower($array->method);
        
        $model->{$request->method}($request->column, $request->operator, $request->value);

        /*if($array->method === 'where') {
            $this->model->where($array->column, $array->operator, $array->value);
        }
        
        if($array->method === 'orWhere') {
            $this->model->orWhere($array->column, $array->operator, $array->value);
        }*/
    }
    
    function setResponse() {
        $this->data = $this->model->paginate($this->per_page);
    }
    
    function getResponse() {
        return $this->data;
    }
}