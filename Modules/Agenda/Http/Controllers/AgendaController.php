<?php

namespace Modules\Agenda\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;

use \Firebase\JWT\JWT;

use Validator;

class AgendaController extends Controller {
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'password' => 'required',
        ]);

        if($validator->fails()) {
            return response()->json(['status_code' => 480, 'status_message' => '', 'data' => $validator->errors()], 480);
        }

        $usuario = [
            'usuario' => $request->user,
            'password' => $request->password
        ];

        if(Auth::attempt($usuario)) {
            $datos_token = [
                'id' => Auth::user()->id,
                'nombre' => Auth::user()->persona->nombre,
                'primer_apellido' => Auth::user()->persona->primer_apellido,
                'segundo_apellido' => Auth::user()->persona->segundo_apellido,
                'usuario' => Auth::user()->usuario,
                'datetime' => date('Y-m-d h:i:s')
            ];

            $jwt = JWT::encode($datos_token, config('app.jwt_token'), 'HS256');

            $datos_usuario = [
                'id' => Auth::user()->id,
                'nombre' => Auth::user()->persona->nombre,
                'primer_apellido' => Auth::user()->persona->primer_apellido,
                'segundo_apellido' => Auth::user()->persona->segundo_apellido,
                'token' => $jwt
            ];

            return response()->json(['status_code' => 200, 'status_message' => 'OK.', 'data' => $datos_usuario], 200);
        }
        else {
            return response()->json(['status_code' => 480, 'status_message' => '', 'data' => null], 480);
        }
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('agenda::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('agenda::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('agenda::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('agenda::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}