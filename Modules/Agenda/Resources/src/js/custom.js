/* ------------------------------------------------------------------------------
 *
 *  # Custom JS code
 *
 *  Place here all your custom js. Make sure it's loaded after app.js
 *
 * ---------------------------------------------------------------------------- */
import Vue from 'vue'
import VueRouter from 'vue-router'

//import axios from 'axios'

Vue.use(VueRouter)

import Home from './components/Index.vue';
import Table from './components/Table.vue';

const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Table
    }
  ],
});

const app = new Vue({
  el: '#app',
  // components: {  },
  router,
});