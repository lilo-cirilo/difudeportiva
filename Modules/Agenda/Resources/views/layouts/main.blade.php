<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title', config('app.name', 'DIF'))</title>

  @section('styles')
  <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="{{ Module::asset('agenda:css/global.css') }}" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->
  <style type="text/css">
    #navbar-mobile {
      background: #263238;
    }
  </style>
  @show

  @stack('head')

  @section('scripts')
  <!-- Core JS files -->
  <script src="{{ Module::asset('agenda:js/core.js') }}"></script>
  <!-- /core JS files -->
  @show
</head>

<body>

  <!-- Main navbar -->
  <div class="navbar navbar-expand-md navbar-dark">
    <div class="navbar-brand" style="padding-top: 0; padding-bottom: 0;">
      <a href="../full/index.html" class="d-inline-block">
        <img src="{{ Module::asset('agenda:images/logo_dif.png') }}" alt="" style="height: 47px;">
      </a>
    </div>

    <div class="d-md-none">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
        <i class="icon-tree5"></i>
      </button>
      <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
        <i class="icon-paragraph-justify3"></i>
      </button>
    </div>

    <div class="collapse navbar-collapse" id="navbar-mobile">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
            <i class="icon-paragraph-justify3"></i>
          </a>
        </li>
      </ul>

      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a href="#" class="navbar-nav-link">
            Text link
          </a>
        </li>

        <li class="nav-item dropdown">
          <a href="#" class="navbar-nav-link">
            <i class="icon-bell2"></i>
            <span class="d-md-none ml-2">Notifications</span>
            <span class="badge badge-mark border-white ml-auto ml-md-0"></span>
          </a>          
        </li>

        <li class="nav-item dropdown dropdown-user">
          <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
            <img src="{{ Module::asset('agenda:images/image.png') }}" class="rounded-circle" alt="">
            <span>Victoria</span>
          </a>

          <div class="dropdown-menu dropdown-menu-right">
            <a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
            <a href="#" class="dropdown-item"><i class="icon-coins"></i> My balance</a>
            <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
            <a href="#" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->


  <!-- Page content -->
  <div class="page-content">

    <!-- Main sidebar -->
    @include('agenda::layouts.main-sidebar.main')
    <!-- /main sidebar -->


    <!-- Main content -->
    <div class="content-wrapper">

      <!-- Page header -->
      @include('agenda::layouts.main-header.main')
      <!-- /page header -->


      <!-- Content area -->
      <div class="content">
        @yield('content')
      </div>
      <!-- /content area -->


      <!-- Footer -->
      @include('agenda::layouts.main-footer.main')
      <!-- /footer -->

    </div>
    <!-- /main content -->

  </div>
  <!-- /page content -->

  <!-- Theme JS files -->
  <script src="{{ Module::asset('agenda:js/vendor.js') }}"></script>
  <!-- /theme JS files -->

  @stack('body')
</body>
</html>