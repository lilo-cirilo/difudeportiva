let mix = require('laravel-mix');

mix.sass('Resources/src/sass/app.scss', 'Resources/assets/css/global.css')
.options({
	processCssUrls: false
})
.setPublicPath('Resources/assets/');

mix.copyDirectory('Resources/src/images/', 'Resources/assets/images/');

mix.scripts([
	//Core
	'Resources/src/js/main/jquery.min.js',
	'Resources/src/js/main/bootstrap.bundle.min.js',
	'Resources/src/js/plugins/loaders/blockui.min.js',
	//core
	'Resources/src/js/app.js',

	/*'Resources/src/js/plugins/extensions/jquery_ui/interactions.min.js',
	'Resources/src/js/plugins/extensions/jquery_ui/widgets.min.js',
	'Resources/src/js/plugins/extensions/jquery_ui/effects.min.js',
	'Resources/src/js/plugins/extensions/mousewheel.min.js',
	'Resources/src/js/plugins/extensions/jquery_ui/globalize/globalize.js',
	'Resources/src/js/plugins/extensions/jquery_ui/globalize/cultures/globalize.culture.es-MX.js',*/
	'Resources/src/js/plugins/ui/moment/moment.min.js',
	'Resources/src/js/plugins/pickers/daterangepicker.js',
	'Resources/src/js/plugins/pickers/anytime.min.js',

	'Resources/src/js/plugins/forms/wizards/steps.min.js',
	'Resources/src/js/plugins/forms/selects/select2.min.js',
	'Resources/src/js/plugins/forms/validation/validate.min.js',
	'node_modules/axios/dist/axios.min.js'
], 'Resources/assets/js/core.js');

mix.copy('Resources/src/js/main/bootstrap.bundle.min.js.map', 'Resources/assets/js/bootstrap.bundle.min.js.map');

mix.js('Resources/src/js/custom.js', 'Resources/assets/js/vendor.js');