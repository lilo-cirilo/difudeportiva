<?php

namespace Modules\Sicodoc\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
use Illuminate\Http\JsonResponse;

class VerifySicodocToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $jwt = $request->header('sicodoc-token');
        if(!$jwt && !$request->token){
            return new JsonResponse(['data' => null, 'message' => 'Hace falta el token sicodoc'], 401);
        }
        try{
            if($jwt){
                $inf = JWT::decode($jwt, config('app.jwt_sicodoc_token'), ['HS256']);
            }else{
                $inf = JWT::decode($request->token, config('app.jwt_sicodoc_token'), ['HS256']);
            }
            $request['usuario_id'] = $inf->usuario_id;
            $request['modulo_id'] = $inf->modulo_id;
            $request['rol_id'] = $inf->rol_id;
            $request['area_id'] = $inf->area_id;
            return $next($request);
        } catch(\UnexpectedValueException $e){
            return new JsonResponse(['data' => null, 'message' => 'Token incorrecto'], 403);
        }
    }
}
