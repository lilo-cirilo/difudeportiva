<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Modules\Sicodoc\Entities\Documentos;
use Modules\Sicodoc\Entities\SeguimientoDocumentos;
use Modules\Sicodoc\Entities\ResponsablesAreas;
use Modules\Sicodoc\Entities\Responsables;
use Modules\Sicodoc\Entities\HistorialContenido;
use Modules\Sicodoc\Entities\InstitucionResponsable;
use App\Models\Instituciones;
use App\Models\Empleado;
use App\Models\Area;
use Modules\Sicodoc\Entities\Estatus;
use Modules\Sicodoc\Entities\ArchivosDocumento;
use Modules\Sicodoc\Entities\RespuestasDocumentos;
use Modules\Sicodoc\Entities\DocumentoDetalles;
use Modules\Sicodoc\Entities\ObservacionesSeguimiento;
use Modules\Sicodoc\Entities\Folio;
use Modules\Sicodoc\Entities\Chat;

class DocumentosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
        // $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR', ['only' => 'index']);
    }

    public function index()
    {
        return "xD";
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fechaentrada' => 'required|date',
            'remitente_id' => 'required|int',
            'destinatario_id' => 'required|int',
            'tipodocumento_id' => 'required|int',
            'es_copia_conocimiento' => 'required',
            'numerodocumento' => 'max:255',
            'asunto' => 'string|max:255',
            'contenido' => 'string',
            'observaciones' => 'string',
            'folio' => 'max:255',
		]);

        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail();
            
            if($request->fechavencimiento == null){
                $newfechavencimiento = Carbon::parse($request->fechavencimiento)->addDays(10);
            }

            $documento = Documentos::create([
                'progresivo' => 1,
                'fechaentrada' => $request->fechaentrada,
                'usuario_id' => $request->usuario_id,
                'remitente_id' => $request->remitente_id,
                'destinatario_id' => $request->destinatario_id,
                'tipodocumento_id' => $request->tipodocumento_id,
                'numerodocumento' => $request->numerodocumento ? strtoupper($request->numerodocumento) : null,
                'asunto' => strtoupper($request->asunto),
                'observaciones' => $request->observaciones,
                'fechavencimiento' => $request->fechavencimiento ? $request->fechavencimiento : $newfechavencimiento ,
                'estatus_id' => 1,
                'folio' => $request->folio ? $request->folio : null,
                'es_copia_conocimiento' => $request->es_copia_conocimiento['id']
			]);
            
            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => 1,
                'areaturnado_id' => $area->area->id,
                'observaciones' => "Nuevo Documento",
                'usuario_id' => $request->usuario_id,
			]);
						
            /*if (!is_null($request->folio)) {
                $folio = Folio::findOrfail(1);
                $folio->folio_correspondencia = $folio->folio_correspondencia + 1;
                $folio->numero_oficio = $folio->numero_oficio + 1;
                $folio->usuario_id = $request->usuario_id;
                $folio->save();
            }*/

            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);

            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'sdoc_documentos',
            //     'registro' => $documento->id . '',
            //     'campos' => json_encode($request->all()) . '',
            //     'metodo' => request()->method(),
            //     'usuario_id' => auth()->user()->id
            // ]);
            return new JsonResponse(['data' => $documento, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
	}
		
	public function storeNuevoDocumentoSalida(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fechaentrada' => 'required|date',
            'remitente_id' => 'required|int',
            'destinatario_id' => 'required|int',
            'tipodocumento_id' => 'required|int',
            //'numerodocumento' => 'max:255',
            'asunto' => 'string|max:255',
            'contenido' => 'string',
            'observaciones' => 'string',
            //'fechavencimiento'=>'required|date',
            //'folio' => 'max:255',
            /*'atencion_id' => 'int',
            'atencion_separador' => 'string',
            'copiaPara' => 'array'*/
		]);

        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }

        try{
            DB::beginTransaction();

            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail();
            
            if($request->fechavencimiento == null){
                $newfechavencimiento = Carbon::parse($request->fechavencimiento)->addDays(10);
            }

            $documento = Documentos::create([
                'progresivo' => 1,
                'fechaentrada' => $request->fechaentrada,
                'usuario_id' => $request->usuario_id,
                'remitente_id' => $request->remitente_id,
                'destinatario_id' => $request->destinatario_id,
                'tipodocumento_id' => $request->tipodocumento_id,
                'numerodocumento' => $request->numerodocumento ? strtoupper($request->numerodocumento) : null,
                'asunto' => strtoupper($request->asunto),
                'observaciones' => $request->observaciones,
                'fechavencimiento' => $request->fechavencimiento ? $request->fechavencimiento : $newfechavencimiento ,
                'estatus_id' => 1,
                'folio' => $request->folio ? $request->folio : null,
            ]);
            
            if ($request->atencion_id || $request->copiaPara) {
                DocumentoDetalles::create([
                    'documento_id' => $documento->id,
                    'institucionresponsable_id' => $request->atencion_id ? $request->atencion_id : null,
                    'separador_institucion_cargo' => $request->atencion_separador ? mb_strtoupper($request->atencion_separador) : null,
                    'copia_conocimiento' => $request->copiaPara ? json_encode($request->copiaPara, JSON_UNESCAPED_UNICODE) : null,
                    'usuario_id' => $request->usuario_id,
                ]);
            };
						
            $contenido = HistorialContenido::create([
                'documento_id' => $documento->id,
                'contenido' => $request->contenido ? $request->contenido : null,
                'usuario_id' => $request->usuario_id
            ]);
            
            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => 1,
                'areaturnado_id' => $area->area->id,
                'observaciones' => "Nuevo Documento",
                'usuario_id' => $request->usuario_id,
			]);
						
            /*if (!is_null($request->numerodocumento)) {
                $folio = Folio::findOrfail(1);
                $folio->folio_correspondencia = $folio->folio_correspondencia + 1;
                $folio->numero_oficio = $folio->numero_oficio + 1;
                $folio->usuario_id = $request->usuario_id;
                $folio->save();
            }*/

            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
			]);

			DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'sdoc_documentos',
            //     'registro' => $documento->id . '',
            //     'campos' => json_encode($request->all()) . '',
            //     'metodo' => request()->method(),
            //     'usuario_id' => auth()->user()->id
            // ]);
            return new JsonResponse(['data' => $documento, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($tipo)
    {
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $empleado = Empleado::where('persona_id', '=', $usuario->persona_id)->firstOrfail();

        $arrayDocsRespuestas = RespuestasDocumentos::select('documentorespuesta_id')->get();

        if ($tipo < 1) { //Documentos de respuesta
            if($usuario->hasRolesStrModulo(['ADMINISTRADOR'], 'sicodoc')){
                $documentosRespuesta = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta')
                    ->whereIn('id', $arrayDocsRespuestas)->orWhereNull('folio')->orderby('fechaentrada','desc')->get();
                return new JsonResponse( ['data' => $documentosRespuesta, 'message' => "ok"]);
            }else{
                $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail()->area_id;
                $documentosRespuesta = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area)->whereIn('documento_id', $arrayDocsRespuestas);
                })->orderby('fechaentrada','desc')->get();
                $arrayDoctosRespuestaIds = [];
                foreach ($documentosRespuesta as $item) {
                    $arrayDoctosRespuestaIds[] = $item->id;
                }
                $documentosSalida = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area);
                })->whereNull('folio')->whereNotIn('id', $arrayDoctosRespuestaIds)->orderby('fechaentrada','desc')->get();
                $joinQuerys = json_decode($documentosRespuesta);
                foreach ($documentosSalida as $item) {
                    $joinQuerys[] = $item;
                }
                return new JsonResponse( ['data' => $joinQuerys, 'message' => "ok"]);
            }
        }else { // Documentos de entrada
            if($usuario->hasRolesStrModulo(['ADMINISTRADOR'], 'sicodoc')){
                $documentosEntrada = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta')
                ->where('estatus_id', '<>', 9)->whereNotIn('id', $arrayDocsRespuestas)->whereNotNull('folio')->orderby('fechaentrada','desc')->get();
                return new JsonResponse( ['data' => $documentosEntrada, 'message' => "ok"]);
            }else{
                $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail()->area_id;
                $documentosEntrada = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta','lasthistorial')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area)->where('estatus_id', '<>', 9)->whereNotIn('documento_id', $arrayDocsRespuestas);
                })->whereNotNull('folio')->orderby('fechaentrada','desc')->get();
                return new JsonResponse( ['data' => $documentosEntrada, 'empleado' => $empleado, 'message' => "ok"]);
            }
        }
    }

    public function showChat($docId)
    {
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        
        return new JsonResponse( ['data' =>Documentos::with('historial.chat.usuario.persona','estatus','tipodocumento','remitente','destinatario','esrespuesta')->findOrfail($docId), 'message' => "ok"]);
    }

    public function getTextoVinculacionResponsableDirGeneral($docId){
        $seguimiento = SeguimientoDocumentos::where('documento_id', '=', $docId)->where('estatus_id', '=', 5)->first();
        
        return new JsonResponse( ['data' => $seguimiento->observaciones, 'message' => "ok"]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $data = Documentos::with('destinatario','remitente','tipoDocumento','estatus','lasthistorial','copiasconocimiento.area','esrespuesta.documentoentrada','contenido','detalle.atencion')->find($id);
        if($data->detalle) $data->detalle->copia_conocimiento = json_decode($data->detalle->copia_conocimiento);
        return new JsonResponse(['data' =>  $data, 'message' => "ok"]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fechaentrada' => 'required|date',
            //'remitente_id' => 'required|int',
            //'destinatario_id' => 'required|int',
            'tipodocumento_id' => 'required|int',
            'numerodocumento' => 'string|max:255',
            'asunto' => 'required|string|max:255',
            'copiaconocimiento' => 'required|int',
            'observaciones' => 'required|string',
            'fechavencimiento'=>'date',
            'folio' => 'required|string|max:255',
        ]);
        if ($validator->fails()) return new JsonResponse(['data' => null , 'message' => $validator->errors()],480);
        
        $documento = Documentos::find($request->id);
        
        if($documento == null) return new JsonResponse(['data' => null, 'message' => 'No se encontró el documento en el sistema'], 404);

        try{
			$usuario = \App\Models\Usuario::find(request()->usuario_id);
						
            DB::beginTransaction();
						
            $documento->numerodocumento = $request->numerodocumento;
            $documento->folio = $request->folio;
            $documento->fechaentrada = $request->fechaentrada;
            $documento->fechavencimiento = $request->fechavencimiento;
            $documento->asunto = mb_strtoupper($request->asunto);
            $documento->observaciones = $request->observaciones;
            $documento->usuario_id = $usuario->id;
            $documento->tipodocumento_id = $request->tipodocumento_id;
            $documento->es_copia_conocimiento = $request->copiaconocimiento;
            $documento->usuario_id = $request->usuario_id;

            if ($documento->contenido) {
                $contenido = HistorialContenido::create([
                    'documento_id' => $documento->id,
                    'contenido' => $request->contenido,
                    'usuario_id' => $request->usuario_id
                ]);
            }
            $documento->save();
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $documento, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    public function updateDoctoSalida(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fechaentrada' => 'required|date',
            //'remitente_id' => 'required|int',
            //'destinatario_id' => 'required|int',
            'tipodocumento_id' => 'required|int',
            'asunto' => 'required|string|max:255',
            'copiaconocimiento' => 'required|int',
            'observaciones' => 'required|string',
            'fechavencimiento'=>'date'
        ]);
        if ($validator->fails()) return new JsonResponse(['data' => null , 'message' => $validator->errors()],480);
        
        $documento = Documentos::find($request->id);
        
        if($documento == null) return new JsonResponse(['data' => null, 'message' => 'No se encontró el documento en el sistema'], 404);

        try{
			$usuario = \App\Models\Usuario::find(request()->usuario_id);
						
            DB::beginTransaction();
						
            $documento->numerodocumento = $request->numerodocumento;
            $documento->folio = $request->folio;
            $documento->fechaentrada = $request->fechaentrada;
            $documento->fechavencimiento = $request->fechavencimiento;
            $documento->asunto = mb_strtoupper($request->asunto);
            $documento->observaciones = $request->observaciones;
            $documento->usuario_id = $usuario->id;
            $documento->tipodocumento_id = $request->tipodocumento_id;
            $documento->es_copia_conocimiento = $request->copiaconocimiento;
            $documento->usuario_id = $request->usuario_id;

            if ($documento->contenido) {
                $contenido = HistorialContenido::create([
                    'documento_id' => $documento->id,
                    'contenido' => $request->contenido,
                    'usuario_id' => $request->usuario_id
                ]);
            }
            
            if($request->atencion_id) {
                $detalle = DocumentoDetalles::where('documento_id', '=', $documento->id)->first();
                $detalle->institucionresponsable_id = $request->atencion_id;
                $detalle->separador_institucion_cargo = $request->atencionSeparador;
                $detalle->save();
            }

            $documento->save();
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $documento, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $documento = Documentos::with('historial')->find($id);
        if($documento == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        try{
            DB::beginTransaction();
            
            $historial = SeguimientoDocumentos::where('id',$documento->historial->id)->delete();
            $documento->delete();
            DB::commit();

            return new JsonResponse(['data' => $documento, 'message'=>'Se elimino correctamente']);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    public function getCount(){
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $estatus = Estatus::all();
        $nuevo = collect([]);
        $arrayDocsRespuestas = RespuestasDocumentos::select('documentorespuesta_id')->get();
        if($usuario->hasRolesStrModulo(['ADMINISTRADOR'], 'sicodoc')){
            foreach ($estatus as $status) {
                $documento = Documentos::where('estatus_id',$status->id)->with('historial')->where('estatus_id', '<>', 9)->whereNotIn('id',$arrayDocsRespuestas)->whereNotNull('folio')->count();
                $nuevo->prepend(['total' => $documento, 'estatus'=> $status->nombre, 'color'=> $status->color, 'descripcion'=> $status->descripcion, 'icono'=> $status->icono]);
            }
            return new JsonResponse(['data' =>$nuevo, 'message' => "ok"]);
        }            
        else{
            $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail()->area_id;
            foreach ($estatus as $status) {
                $documento = Documentos::where('estatus_id',$status->id)->with('historial')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area)->where('estatus_id', '<>', 9)->whereNotIn('documento_id',$arrayDocsRespuestas);
                })->whereNotNull('folio')->count();
                $nuevo->prepend(['total' => $documento, 'estatus'=> $status->nombre, 'color'=> $status->color, 'descripcion'=> $status->descripcion, 'icono'=> $status->icono,'id'=> $status->id,]);
            }
            return new JsonResponse(['data' =>$nuevo, 'message' => "ok"]);
        }
    }

    public function getCountRespuesta(){
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $estatus = Estatus::all();
        $nuevo = collect([]);
        $arrayDocsRespuestas = RespuestasDocumentos::select('documentorespuesta_id')->get();
        if($usuario->hasRolesStrModulo(['ADMINISTRADOR'], 'sicodoc')){
            foreach ($estatus as $status) {
                $documento = Documentos::where('estatus_id',$status->id)->whereIn('id',$arrayDocsRespuestas)->with('historial')->count();
                $nuevo->prepend(['total' => $documento, 'estatus'=> $status->nombre, 'color'=> $status->color, 'descripcion'=> $status->descripcion, 'icono'=> $status->icono]);
            }
            return new JsonResponse(['data' =>$nuevo, 'message' => "ok"]);
        }            
        else{
            $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail()->area_id;
            foreach ($estatus as $status) {
                $documento = Documentos::where('estatus_id',$status->id)->with('historial')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area)->whereIn('documento_id',$arrayDocsRespuestas);
                })->get();
                $arrayDoctosIds = [];
                foreach ($documento as $item) {
                    $arrayDoctosIds[] = $item->id;
                }
                $documentosSalida = Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario','esrespuesta')->whereHas('historial', function ($query) use ($area, $arrayDocsRespuestas) {
                    $query->where('areaturnado_id', $area);
                })->where('estatus_id',$status->id)->whereNull('folio')->whereNotIn('id', $arrayDoctosIds)->orderby('fechaentrada','desc')->get();
                $joinQuerys = json_decode($documento);
                foreach ($documentosSalida as $item) {
                    $joinQuerys[] = $item;
                }
                $nuevo->prepend(['total' => count($joinQuerys), 'estatus'=> $status->nombre, 'color'=> $status->color, 'descripcion'=> $status->descripcion, 'icono'=> $status->icono,'id'=> $status->id,]);
            }
            return new JsonResponse(['data' =>$nuevo, 'message' => "ok"]);
        }
    }

    public function change(Request $request)
    {
        $documento = Documentos::find($request->id);
        
        if($documento == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el documento'], 404);
        
        try{
            DB::beginTransaction();
            $documento->estatus_id = $request->estatus_id;
            $documento->save();

            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => $request->estatus_id,
                'areaturnado_id' => $request->areaturnado_id,
                'observaciones' => $request->observaciones ? $request->observaciones : null,
                'usuario_id' => $request->usuario_id
            ]);         

            if($request->observaciones_adm != ''){
                $observacion = ObservacionesSeguimiento::create([
                    'seguimiento_id' => $seguimiento->id,
                    'observaciones' => $request->observaciones_adm,
                    'usuario_id' => $request->usuario_id
                ]);
			}
						
            if($request->copiaPara != NULL){

                if(sizeOf($request->copiaPara)>0){

                    $max = sizeOf($request->copiaPara);
                    //\Log::debug($request->copiaPara);
                    foreach ($request->copiaPara as $copia) {
                        //\Log::debug($copia);
                        //MANDOS MEDIOS Y SUPERIORES
                        if($copia['id']==1001){
                            $responsables = ResponsablesAreas::where('tipoarea_id','<','5')->get();
                            foreach($responsables as $responsable){
                                SeguimientoDocumentos::create([
                                    'documento_id' => $documento->id,
                                    'estatus_id' => 9,
                                    'areaturnado_id' => $responsable['area_id'],
                                    'observaciones' => 'Copia de Conocimiento',
                                    'usuario_id' => $request->usuario_id
                                ]);
                            }
                        }
                        //DIRECTORES
                        if($copia['id']==1002){
                            $responsables = ResponsablesAreas::where('tipoarea_id','=','1')->get();
                            foreach($responsables as $responsable){
                                SeguimientoDocumentos::create([
                                    'documento_id' => $documento->id,
                                    'estatus_id' => 9,
                                    'areaturnado_id' => $responsable['area_id'],
                                    'observaciones' => 'Copia de Conocimiento',
                                    'usuario_id' => $request->usuario_id
                                ]);
                            }
                        }
                        //JEFES DE UNIDAD
                        if($copia['id']==1003){
                            $responsables = ResponsablesAreas::where('tipoarea_id','=','3')->get();
                            foreach($responsables as $responsable){
                                SeguimientoDocumentos::create([
                                    'documento_id' => $documento->id,
                                    'estatus_id' => 9,
                                    'areaturnado_id' => $responsable['area_id'],
                                    'observaciones' => 'Copia de Conocimiento',
                                    'usuario_id' => $request->usuario_id
                                ]);
                            }
                        }
                        //JEFES DE DEPARTAMENTO
                        if($copia['id']==1004){
                            $responsables = ResponsablesAreas::where('tipoarea_id','=','4')->get();
                            foreach($responsables as $responsable){
                                SeguimientoDocumentos::create([
                                    'documento_id' => $documento->id,
                                    'estatus_id' => 9,
                                    'areaturnado_id' => $responsable['area_id'],
                                    'observaciones' => 'Copia de Conocimiento',
                                    'usuario_id' => $request->usuario_id
                                ]);
                            }
                        }
                        if($copia['id']<1000){
                            SeguimientoDocumentos::create([
                                'documento_id' => $documento->id,
                                'estatus_id' => 9,
                                'areaturnado_id' => $copia['area_id'],
                                'observaciones' => 'Copia de Conocimiento',
                                'usuario_id' => $request->usuario_id
                            ]);
                        }
                    }
                }
            }
						
            
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);
            /*auth()->user()->bitacora(request(), [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => $request->usuario_id
            ]);*/
            DB::commit();
            return new JsonResponse(['data' => $documento, 'message'=>"Se actualizo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: No se pudo actualizar' . $e->getMessage()],409);
        }

    }

    public function finalizacionDirecta(Request $request)
    {
        $documento = Documentos::find($request->id);
        
        if($documento == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el documento'], 404);
        
        try{
            DB::beginTransaction();
            $documento->estatus_id = 7;
            $documento->save();
            
            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => 11,
                'areaturnado_id' => $request->areaturnado_id,
                'observaciones' => $request->observaciones ? $request->observaciones : null,
                'usuario_id' => $request->usuario_id
            ]);
            
            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => 7,
                'areaturnado_id' => $request->areaturnado_id,
                'observaciones' => $request->observaciones ? $request->observaciones : null,
                'usuario_id' => $request->usuario_id
            ]);   

            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);
            DB::commit();
            return new JsonResponse(['data' => $documento, 'message'=>"Se actualizo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: No se pudo actualizar' . $e->getMessage()],409);
        }

    }

    private function getDireccion($id) {
        $area = Area::find($id);
        $nombre = $area->nombre;

        if (in_array($nombre, array('DIRECCIÓN GENERAL', 'DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS', 'DIRECCIÓN JURÍDICA')))
            return $area;
        else
            return $this->getDireccion($area->padre_id);
    }

    public function toRevision(Request $request)
    {
        $documento = Documentos::find($request->id);

        if($documento == null) return new JsonResponse(['data' => null, 'message' => 'No existe el documento'], 404);
        
        $empleado = Responsables::with([
            'responsableArea' => function ($query) {
                $query->select('id', 'area_id', 'empleado_id');
            }
        ])->find($request->destinatario_id ? $request->destinatario_id : $request->remitente_id);

        if($empleado == null) return new JsonResponse(['data' => null, 'message' => 'No existe el empleado destinatario'], 404);

        $direccion = $this->getDireccion($empleado->responsableArea->area_id);
        
        try{
            DB::beginTransaction();
            
            $documento->estatus_id = $request->estatus_id;
            $documento->save();

            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => $request->estatus_id,
                'areaturnado_id' => $direccion->id,
                'observaciones' => $request->observaciones ? $request->observaciones : null,
                'usuario_id' => $request->usuario_id
            ]);
            
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);

            DB::commit();
            return new JsonResponse(['data' => $documento, 'message'=>"Se mandó a revisión correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: No se pudo mandar a revisión' . $e->getMessage()],409);
        }

    }

    public function toRevisionSalida(Request $request)
    {
        $direccion = null;

        $documento = Documentos::with('esrespuesta')->find($request->id);

        if($documento == null) return new JsonResponse(['data' => null, 'message' => 'No existe el documento'], 404);
        
        if (!$documento->esrespuesta) {
            $empleado = Responsables::with([
                'responsableArea' => function ($query) {
                    $query->select('id', 'area_id', 'empleado_id');
                }
            ])->find($request->destinatario_id ? $request->destinatario_id : $request->remitente_id);
    
            if($empleado == null) return new JsonResponse(['data' => null, 'message' => 'No existe el remitente'], 404);
    
            $direccion = $this->getDireccion($empleado->responsableArea->area_id);
            
        }else{ //Documentos que son respuesta

            $Respuesta = RespuestasDocumentos::where('documentorespuesta_id', '=', $documento->id)->first();
            $documentoEntrada = Documentos::find($Respuesta->documento_id);

            if($documentoEntrada == null) return new JsonResponse(['data' => null, 'message' => 'No existe el documento original'], 404);

            $seguimientoDocumentoEntrada = SeguimientoDocumentos::where('documento_id', '=', $documentoEntrada->id)->where('estatus_id', '=', 5)->orderby('created_at', 'asc')->first();

            $direccion = $this->getDireccion($seguimientoDocumentoEntrada->areaturnado_id);
        }

        if($direccion === null) return new JsonResponse(['data' => null, 'message' => 'No fue posible vincular a la dirección correspondiente'], 404);
        
        try{
            DB::beginTransaction();
            
            $documento->estatus_id = $request->estatus_id;
            $documento->save();

            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => $request->estatus_id,
                'areaturnado_id' => $direccion->id,
                'observaciones' => $request->observaciones ? $request->observaciones : null,
                'usuario_id' => $request->usuario_id
            ]);
            
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);

            DB::commit();
            return new JsonResponse(['data' => $documento, 'message'=>"Se mandó a validación correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: No se pudo mandar a validación' . $e->getMessage()],409);
        }

    }

    public function vistoBueno(Request $request)
    {
        $documento = Documentos::find($request->id);
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
        
        if($documento == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el documento'], 404);

        if($usuario == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el usuario'], 404);
        
        $empleado = Empleado::where('persona_id', '=', $usuario->persona_id)->first();

        if($empleado == null) return new JsonResponse(['data' => null, 'message' => 'No existe el empleado destinatario'], 404);

        $direccion = $this->getDireccion($empleado->area_id);
        
        try{
            DB::beginTransaction();
            $documento->estatus_id = $request->estatus_id;
            
            $seguimiento = SeguimientoDocumentos::create([
                'documento_id' => $documento->id,
                'estatus_id' => $request->estatus_id,
                'areaturnado_id' => $direccion->id,
                'observaciones' => $request->observaciones,
                'usuario_id' => $request->usuario_id
            ]);
            
            if ($request->numero_oficio) 
                $documento->numerodocumento = $request->numero_oficio;
            
            $documento->save();
            
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);
            /*auth()->user()->bitacora(request(), [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => $request->usuario_id
            ]);*/
            DB::commit();
            return new JsonResponse(['data' => $documento, 'message'=>"Se actualizo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: No se pudo actualizar' . $e->getMessage()],409);
        }

    }

    public function detallesSolicitudFinalizacion($did)
    {
        return new JsonResponse([
            'data' => SeguimientoDocumentos::where('documento_id', '=', $did)->where('estatus_id', '=', 11)->first(),
            'message' => "ok"
        ]);
    }

    public function archivos($did,$tid)
    {
        return new JsonResponse( ['data' =>ArchivosDocumento::with('archivo')->whereHas('archivo', function ($query) use ($tid) {
            $query->where('tipoarchivo_id',$tid);
        })->where('documento_id',$did)->orderby('created_at','desc')->get(), 'message' => "ok"]);
    }

    public function respuesta(Request $request)
    {
        //return $request -> all();
        $validator = Validator::make($request->all(), [
            'fechaentrada' => 'required|date',
            'remitente_id' => 'required|int',
            //'respuesta_id' => 'required|int',
            'destinatario_id' => 'required|int',
            'tipodocumento_id' => 'required|int',
            //'numerodocumento' => 'required|string|max:255',
            'asunto' => 'string|max:255',
            'contenido' => 'required|string',
            'observaciones' => 'string',
            //'fechavencimiento'=>'required|date',
            //'fechaentrada'=>'required|date',
            //'folio' => 'required|string|max:255',
            /*'atencion_id' => 'int',
            'atencion_separador' => 'string',
            'copiaPara' => 'array'*/
        ]);

        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }

        $documentoR = Documentos::find($request->respuesta_id);
        
        if($documentoR == null){
            //Nuevo documento de salida (No es respuesta de otro)
            $this->storeNuevoDocumentoSalida($request);
        }

        if($documentoR != null){
			try{
                DB::beginTransaction();

                $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
                $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail();
                //$area = Empleado::with('area')->where('persona_id',auth()->user()->persona_id)->firstOrFail();
                $documento = Documentos::create([
                    'progresivo' => 1,
                    'fechaentrada' => $request->fechaentrada,
                    'usuario_id' => $request->usuario_id,
                    'remitente_id' => $request->remitente_id,
                    'destinatario_id' => $request->destinatario_id,
                    'tipodocumento_id' => $request->tipodocumento_id,
                    'numerodocumento' => null,
                    'asunto' => mb_strtoupper($request->asunto),
                    'observaciones' => $request->observaciones,
                    'fechavencimiento' => $request->fechavencimiento,
                    'estatus_id' => 1,
                    'folio' => null,
                ]); 

                if ($request->atencion_id || $request->copiaPara) {
                    DocumentoDetalles::create([
                        'documento_id' => $documento->id,
                        'institucionresponsable_id' => $request->atencion_id ? $request->atencion_id : null,
                        'separador_institucion_cargo' => $request->atencion_separador ? mb_strtoupper($request->atencion_separador) : null,
                        'copia_conocimiento' => $request->copiaPara ? json_encode($request->copiaPara, JSON_UNESCAPED_UNICODE) : null,
                        'usuario_id' => $request->usuario_id,
                    ]);
                }
                            
                $contenido = HistorialContenido::create([
                    'documento_id' => $documento->id,
                    'contenido' => $request->contenido ? $request->contenido : null,
                    'usuario_id' => $request->usuario_id
                ]);
                
                $seguimiento = SeguimientoDocumentos::create([
                    'documento_id' => $documento->id,
                    'estatus_id' => 1,
                    'areaturnado_id' => $area->area->id,
                    'observaciones' => "Nuevo Documento",
                    'usuario_id' => $request->usuario_id,
                ]);

                $seguimiento2 = SeguimientoDocumentos::create([
                    'documento_id' => $request->respuesta_id,
                    'estatus_id' => 6,
                    'areaturnado_id' => $area->area->id,
                    'observaciones' => "Documento Respondido",
                    'usuario_id' => $request->usuario_id,
                ]);

                $respuesta = RespuestasDocumentos::create([
                    'documento_id' => $request->respuesta_id,
                    'documentorespuesta_id' => $documento->id,
                    'usuario_id' => $request->usuario_id,
                ]);

                $documentoR->estatus_id = 6;
                $documentoR->save();
                
                DB::commit();
                //$usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
                $usuario->bitacora($request, [
                    'tabla' => 'sdoc_documentos',
                    'registro' => $documento->id . '',
                    'campos' => json_encode($request->all()) . '',
                    'metodo' => $request->method(),
                    'usuario_id' => $request->usuario_id    
                ]);
                // auth()->user()->bitacora(request(), [
                //     'tabla' => 'sdoc_documentos',
                //     'registro' => $documento->id . '',
                //     'campos' => json_encode($request->all()) . '',
                //     'metodo' => request()->method(),
                //     'usuario_id' => auth()->user()->id
                // ]);

                return new JsonResponse(['data' => $documento, 'message'=>"Se guardo correctamente"]);
            }catch(Exception $e){
                DB::rollBack();
                return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
            }
		}
    }

    public function chat(Request $request){

        $validator = Validator::make($request->all(), [
            'documento_id' => 'required|int',
            'area_id' => 'required|int',
            'texto' => 'required|string'
        ]);

        if ($validator->fails()) {return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);}

        $documento = Documentos::find($request->documento_id);
        
        if($documento == null){return new JsonResponse(['data'=> null, 'message'=>'El documento no existe.'], 404);}

        try{
            DB::beginTransaction();

            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail();

            $seguimiento = SeguimientoDocumentos::firstOrCreate(
                ['documento_id' => $request->documento_id,
                'estatus_id' => 11],
                ['documento_id' => $request->documento_id,
                'estatus_id' => 11,
                'areaturnado_id' => $area->area->id,
                'observaciones' => $request->texto,
                'usuario_id' => $usuario->id]
            );

            $chat = Chat::create([
                'seguimiento_documento_id' => $seguimiento->id,
                'area_id' => $area->area->id,
                'texto' => $request->texto,
                'usuario_id' =>  $usuario->id
            ]);

            if ($documento->estatus_id !== 11) {
                $documento->estatus_id = 11;
                $documento->save();
            }
            
            DB::commit();

            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $documento->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);

            return new JsonResponse(['data' => $documento, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }
		
    public function copias()
    {
            $usuario = \App\Models\Usuario::find(request()->usuario_id);

            if($usuario->hasRolesStrModulo(['ADMINISTRADOR'], 'sicodoc'))
                    return new JsonResponse( ['data' =>Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario')->whereHas('historial', function ($query) {
                        $query->where('estatus_id', 9);
                })->get(), 'message' => "ok"]);
            else{
                    $area = Empleado::with('area')->where('persona_id',$usuario->persona_id)->firstOrFail()->area_id;
                    return new JsonResponse( ['data' =>Documentos::with('historial.area','historial.usuario.persona','historial.estatus','historial.area','estatus','tipodocumento','remitente','destinatario')->whereHas('historial', function ($query) use ($area) {
                        $query->where('estatus_id', 9);
                })->whereHas('historial', function ($query) use ($area) {
                            $query->where('areaturnado_id', $area);
                    })->orderby('fechaentrada','desc')->get(), 'message' => "ok"]);
            }
            
    }

	public function historial($documento_id)
    {
		$info = Documentos::with('historial.estatus')->find($documento_id);
        return new JsonResponse(['data' => $info->historial  , 'message' => "ok"]);
	}
		
	public function print($id)
    {
        //return new JsonResponse(['data' => Documentos::with('destinatario','remitente','tipoDocumento','esrespuesta','contenido','copiasconocimiento.area.areasresponsables.empleado.persona')->find($id), 'message' => "ok"]);$data = Documentos::with('destinatario','remitente','tipoDocumento','esrespuesta','contenido','copiasconocimiento.area.areasresponsables.empleado.persona')->find($id);
        $data = Documentos::with('destinatario','remitente','tipoDocumento','esrespuesta','contenido','copiasconocimiento.area.areasresponsables.empleado.persona')->find($id);
        $copias = $data->copiasconocimiento;
        $ccp =[];
        foreach($copias as $copia){
            $newData =[
                'area' => $copia->area->nombre,
                'encargado' => $copia->area->areasresponsables->empleado->persona->nombre.' '.$copia->area->areasresponsables->empleado->persona->primer_apellido.' '.$copia->area->areasresponsables->empleado->persona->segundo_apellido 
            ];
            array_push($ccp,$newData);
        } 

        return $documento = [
            'tipo_correspondencia' => $data->destinatario->tipo,
            'area' => $data->remitente->area,
            'asunto' => $data->asunto,
            'tipo_documento' => $data->tipoDocumento->nombre,
            'numero_documento' => $data->numerodocumento,
            'fecha_documento' => $data->fechaentrada,
            'destinatario' => [
                'nombre' => $data->destinatario->responsable,
                'cargo' => $data->destinatario->cargo,
                'area' => $data->destinatario->area
            ],
            'contenido' => $data->contenido->contenido,
            'remitente' => [
                'nombre' => $data->remitente->responsable,
                'cargo' => $data->remitente->cargo,
                'area' => $data->remitente->area
            ],
            'ccp' => $ccp

        ];

    }
    
    public function newPrint($id)
    {
        $data = Documentos::with('destinatario','remitente','tipoDocumento','esrespuesta','contenido','detalle.atencion')->find($id);
        $copias = $data->detalle && $data->detalle->copia_conocimiento ? json_decode($data->detalle->copia_conocimiento) : []; 
        $ccp =[];
        foreach($copias as $copia){
            $newData =[
                'area' => $copia,
                'encargado' => ''
            ];
            array_push($ccp,$newData);
        } 

        return $documento = [
            'tipo_correspondencia' => $data->destinatario->tipo,
            'area' => $data->remitente->area,
            'asunto' => $data->asunto,
            'tipo_documento' => $data->tipoDocumento->nombre,
            'numero_documento' => $data->numerodocumento,
            'fecha_documento' => $data->fechaentrada,
            'destinatario' => [
                'nombre' => $data->destinatario->responsable,
                'cargo' => $data->destinatario->cargo,
                'area' => $data->destinatario->area
            ],
            'contenido' => $data->contenido->contenido,
            'remitente' => [
                'nombre' => $data->remitente->responsable,
                'cargo' => $data->remitente->cargo,
                'area' => $data->remitente->area
            ],
            'ccp' => count($ccp) > 0 ? $ccp : null,
            'atencion' => $data->detalle && $data->detalle->atencion ? [
                'responsable' => $data->detalle && $data->detalle->atencion ? $data->detalle->atencion->responsable : null,
                'area' => $data->detalle && $data->detalle->atencion ? $data->detalle->atencion->area : null,
                'separador' => $data->detalle ? $data->detalle->separador_institucion_cargo : null,
                'cargo' => $data->detalle && $data->detalle->atencion ? $data->detalle->atencion->cargo : null
            ] : null,
            'sdoc' => true
        ];

	}
		
    public function obtenerFolio(){
        $folio = Folio::first();

        return new JsonResponse(['data' => $folio, 'message' => 'Ok']);
    }

}
