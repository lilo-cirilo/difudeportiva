<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\Dependencia;
use Modules\Sicodoc\Entities\Responsables;
use Modules\Sicodoc\Entities\ResponsablesInstituciones;
use Modules\Sicodoc\Entities\ResponsablesAreas;

class ResponsablesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sicodoc::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sicodoc::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|string|max:255',
            'encargado' => 'required|string|max:255',
            'cargoencargado' => 'required|string|max:255'
            //'calle' => 'required|string|max:255',
            //'colonia' => 'required|string|max:255',
            //'numero' => 'required|string|max:7' 
		]);
				
		\Log::debug($request);

        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();
            
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            \Log::debug($usuario);
            $dependencia = Dependencia::create([
                'nombre'=> $request->nombre,
                'calle'=> $request->calle ? $request->calle : null,
                'numero'=> $request->numero ? $request->numero : null,
                'colonia'=> $request->colonia ? $request->colonia : null,
                'telefono'=> $request->telefono ? $request->telefono : null,
                'email'=> $request->email ? $request->email : null,
                'encargado'=> $request->encargado,
                'cargoencargado'=> $request->cargoencargado,
                'tipoinstitucion_id'=>$request->tipoinstitucion_id,
                'entidad_id'=> $request->entidad_id,
                'usuario_id' => $request->usuario_id,
			]);
						
			\Log::debug($dependencia);
            
            $responsable = Responsables::create([
                'areas_responsables_id' => null,
                'instituciones_responsables_id' => $dependencia->id,
                'tipo' => "EXTERNO",
                'usuario_id' => $request->usuario_id,
            ]);
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'sdoc_documentos',
            //     'registro' => $dependencia->id . '',
            //     'campos' => json_encode($request->all()) . '',
            //     'metodo' => request()->method(),
            //     'usuario_id' => auth()->user()->id,
            // ]);
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'sdoc_documentos',
                'registro' => $dependencia->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method(),
                'usuario_id' => $request->usuario_id    
            ]);
            return new JsonResponse(['data' => $responsable, 'message'=>"Se guardó correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sicodoc::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sicodoc::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function responsablearea($responsable)
    {
        $responsables = ResponsablesAreas::select('id','area_id','area','responsable', 'cargo')
        ->where('responsable', 'like', "%$responsable%")
        //->whereIn('area', ['PRESIDENCIA', 'DIRECCIÓN GENERAL', 'DIRECCIÓN JURIDICA', 'DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS'])
        //->take(10)
        ->get();
        return new JsonResponse(['data' => $responsables, 'message' => 'Ok']);
    }

    public function responsableinstitucion($responsable)
    {
        $responsables = ResponsablesInstituciones::select('id','area', 'responsable', 'cargo')->where('responsable', 'like', "%$responsable%")->take(10)
        ->get();
        return new JsonResponse(['data' => $responsables, 'message' => 'Ok']);
    }

    public function catalogoInstituciones($search)
    {
        $instituciones = ResponsablesInstituciones::select('area')->where('area', 'like', "%$search%")->distinct('area')->take(10)
        ->get();
        return new JsonResponse(['data' => $instituciones, 'message' => 'Ok']);
    }
}
