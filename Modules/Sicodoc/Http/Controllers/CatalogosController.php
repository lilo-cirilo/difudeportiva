<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Sicodoc\TipoOficio;
use Illuminate\Support\Facades\DB;

class CatalogosController extends Controller
{
    public function tipos_documentos(Request $request)
    {
        if($request->ajax()) return TipoOficio::orderBy('id', 'asc')->get();

        return redirect('/sicodoc');
    }

    public function areas(Request $request)
    {
        if($request->ajax()) return DB::table('cat_areas')->orderBy('id', 'asc')->get();

        return redirect('/sicodoc');
    }
}