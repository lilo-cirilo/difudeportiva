<?php

namespace Modules\Sicodoc\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use Modules\Sicodoc\Entities\DocumentosHistorial;

class DocumentosHistorialController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sicodoc::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sicodoc::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'documento_id' => 'required|max:11|integer',
            'usuario_entrada_id' => 'required|max:11|integer',
            'usuario_salida_id' => 'required|max:11|integer',
            
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data'=> null, 'message'=>$validator->errors()], 480);
        }
        try{
            DB::beginTransaction();

            $historial = DocumentosHistorial::create([
                'documento_id' => $request->documento_id,
                'estatus_id' => 1,
                'usuario_entrada_id' => 2,
                'usuario_salida_id' => 3,
                'observaciones' => "nuevo registro",
                
            ]);
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $historial, 'message'=>"Se guardo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sicodoc::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sicodoc::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
