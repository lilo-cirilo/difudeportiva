<?php

Route::group(['prefix' => 'sicodoc', 'namespace' => 'Modules\Sicodoc\Http\Controllers'], function()
{
	Route::post('/nuevo', 'ResponsablesController@store');
	Route::group(['middleware' => 'web'], function(){
			Route::get('/', 'SicodocController@index');
	});

	Route::group(['prefix' => 'archivos', 'middleware' => ['web', 'auth']], function(){
		Route::post('/subir/{tid}/{did}', 'ArchivosController@store')->name('subir');
		Route::post('/actualizar/{tid}/{did}', 'ArchivosController@update')->name('actualizar');
		Route::post('/limpiar/{tid}/{did}', 'ArchivosController@deleteFiles')->name('limpiar');
		Route::get('/imprimir/{id}', 'DocumentosController@print')->name('imprimir');
	});

	Route::group(['middleware' => 'apiSICODOC'], function(){

		Route::get('/permissions', 'SicodocController@permissions');

		Route::prefix('documentos')->name('documentos.')->group(function () {
			//Route::get('/permissions', 'SicodocController@permissions');
			Route::get('/', 'DocumentosController@index')->name('index');
			Route::post('/nuevo', 'DocumentosController@store')->name('registrar');
			Route::get('/editar/{id}', 'DocumentosController@edit')->where('id', '[0-9]+');
			Route::put('/actualizar', 'DocumentosController@update')->name('actualizar');
			Route::put('/actualizarsalida', 'DocumentosController@updateDoctoSalida')->name('actualizar');
			Route::get('/lista/{tipo}', 'DocumentosController@show');
			Route::get('/listaConocimiento', 'DocumentosController@copias');
			Route::delete('/eliminar/{id}', 'DocumentosController@destroy')->where('id', '[0-9]+');
			Route::get('/conteo', 'DocumentosController@getCount');
			Route::get('/conteoRespuestas', 'DocumentosController@getCountRespuesta');
			Route::get('/archivos/{did}/{tid}', 'DocumentosController@archivos');
			Route::post('/respuesta', 'DocumentosController@respuesta')->name('respuesta');
			Route::post('/chat', 'DocumentosController@chat')->name('chat');
			Route::get('/showChat/{did}', 'DocumentosController@showChat')->name('showChat');
			Route::get('/textovincular/{did}', 'DocumentosController@getTextoVinculacionResponsableDirGeneral')->name('textovincular');
			Route::post('/vincular','DocumentosController@change')->name('vincular');
			Route::post('/finalizar','DocumentosController@finalizacionDirecta')->name('finalizar');
			Route::post('/revision','DocumentosController@toRevision')->name('revision');
			Route::post('/revisionsalida','DocumentosController@toRevisionSalida')->name('revisionsalida');
			Route::post('/vistobueno','DocumentosController@vistoBueno')->name('vistobueno');
			Route::get('/detsolfin/{id}', 'DocumentosController@detallesSolicitudFinalizacion')->where('id', '[0-9]+');
			Route::get('/historial/{documento_id}','DocumentosController@historial')->where('id', '[0-9]+')->name('historial');
			Route::get('/imprimir/{id}', 'DocumentosController@print')->name('imprimir');
			Route::get('/newimprimir/{id}', 'DocumentosController@newPrint')->name('newimprimir');
		});

		Route::prefix('responsables')->name('responsables.')->group(function () {
			Route::post('/nuevo', 'ResponsablesController@store');
			Route::get('/instituciones/{responsable}', 'ResponsablesController@responsableinstitucion');
			Route::get('/areas/{responsable}', 'ResponsablesController@responsablearea');
			Route::get('/catalogoinstituciones/{search}', 'ResponsablesController@catalogoInstituciones');
		});

	});

	Route::prefix('tipoDocumento')->name('tipoDocumento.')->group(function () {
		Route::get('/lista', 'TipoDocumentoController@list');
	});

	Route::prefix('tipoInstitucion')->name('tipoInstitucion.')->group(function () {
		Route::get('/lista', 'TipoInstitucionController@list');
	});

	Route::prefix('entidades')->name('entidades.')->group(function () {
		Route::get('/entidad/{nombre}', 'EntidadesController@get');
		Route::get('/lista', 'EntidadesController@list');
		//Route::get('/lista', 'EntidadesController@list');
	});

	Route::prefix('municipios')->name('municipios.')->group(function () {
		Route::get('/{nombre}', 'MunicipiosController@get');
	});

	Route::prefix('municipios')->name('municipios.')->group(function () {
		Route::get('/{nombre}', 'MunicipiosController@get');
	});

	Route::prefix('folio')->name('folio.')->group(function () {
		Route::get('/ultimofolio', 'DocumentosController@obtenerFolio')->name('ultimofolio');
	});

});