<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
    PETICIÓN: Moisés.
    MOTIVO: Para guardar folio y número de oficio de dirección general
    ALCANCE: SICODOC
*/

class CreateSdocFoliodisponibleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdoc_foliodisponible', function (Blueprint $table) {
						$table->increments('id');
						$table->integer('folio_correspondencia');
						$table->integer('numero_oficio');
						$table->unsignedInteger('usuario_id');
            $table->timestamps();
						$table->softDeletes();
				});
				
				DB::table('sdoc_foliodisponible')->insert([
					['folio_correspondencia' => 4149,
					 'numero_oficio' => 1273,
					 'usuario_id' => 1,
					 'created_at' => date("Y-m-d H:i:s"),
					 'updated_at' => date("Y-m-d H:i:s")]
			]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdoc_foliodisponible');
    }
}
