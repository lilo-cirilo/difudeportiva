<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      	<li class="nav-item">
        	<a class="nav-link" href="{{ route('home') }}">
          		<i class="nav-icon fa fa-home"></i> Home
        	</a>
		</li>
		
		<li class="nav-title">Correspondencia de Entrada</li>
      	<li class="nav-item">
			<a class="nav-link" href="/sicodoc#/home">
        		<i class="nav-icon fa fa-list"></i> Listado
        	</a>
      	</li>
		
		@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR', 'CAPTURISTA'], 'sicodoc'))
			<li class="nav-item">
				<a class="nav-link" href="/sicodoc#/documento/nuevo">
					<i class="nav-icon fa fa-arrow-circle-right"></i> Registrar Documento
				</a>
			</li>
		@endif
		
		{{-- <li class="nav-title">Listado de copias de conocimiento</li> --}}
		<li class="nav-item">
			<a class="nav-link" href="/sicodoc#/copiasConocimiento">
				<i class="nav-icon fa fa-cc"></i> Copias de Conocimiento
			</a>
		</li> 

		
		<li class="nav-title">Correspondencia de Salida</li>
			
		@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR', 'ADMINISTRATIVO', 'RESPONSABLE', 'REVISOR ENTRADA', 'REVISOR SALIDA', 'DIRECTOR'], 'sicodoc'))	
			<li class="nav-item">
				<a class="nav-link" href="/sicodoc#/listadosalida">
					<i class="nav-icon fa fa-th-large"></i> Listado
				</a>
			</li> 
		@endif
	
		@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ADMINISTRATIVO','RESPONSABLE'], 'sicodoc'))	
			<li class="nav-item">
				<a class="nav-link" href="/sicodoc#/documento/respuesta">
				<i class="nav-icon fa fa-plus"></i> Nuevo documento</a>
			</li>
		@endif
      
      <li class="divider"></li>

    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>