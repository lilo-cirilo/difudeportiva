<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>SICOSED</title>
    <!-- Main styles for this application-->
    {{--  <link href="{{ Module::asset('bancadif:css/app.css') }}" rel="stylesheet">  --}}
    <meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
    <link href="{{ asset('doctos/css/app.css') }}" rel="stylesheet">
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    
    @include('sicodoc::layouts.includes.navbar')

    <div class="app-body">
      
      @include('sicodoc::layouts.includes.sidebar')
      
      <main class="main">
          @include('sicodoc::layouts.includes.breadcrumb')

        <div class="container-fluid">
          <div class="animated fadeIn">

            @yield('content')
          
          </div>
        </div>

      </main>
    </div>
    <footer class="app-footer d-print-none">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span>Unidad de Informática</span>
      </div>
    </footer>
    <script>
      sessionStorage.setItem('sicodoc_token', '{{$sicodoc_token}}');
    </script>
    {{-- <script src="{{ Module::asset('sicodoc:js/app.js') }}"></script> --}}
     <script src="{{ asset('doctos/js/app.js') }}"></script>
  </body>
</html>
