import Vuex from 'vuex';
import Vue from 'vue';

import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    // empleado: null
    roles: [],
    //area_id: null
  },
  mutations: {
    // setEmpleado: function (state, empleado) {
    //   state.empleado = empleado;
    // }
    setRoles(state, roles){
      state.roles = roles;
    }
  },
  getters: {
    isRoleInRoles: (state) => (acceptRoles) => {
      // return state.roles.filter(role => role.name === acceptRoles);
      return state.roles.filter(role => acceptRoles.includes(role.name));
    },
    has: (state, getters) => (acceptRoles) => {
      return getters.isRoleInRoles(acceptRoles).length > 0;
    },
    loadedRoles: (state) => {
      return state.roles.length > 0;
    },
    area: (state) => {
      return state.roles[0].area_id || null;
    }
  },
  actions: {
    fetchRoles(context) {
      axios.defaults.headers.common["sicodoc-token"] = sessionStorage.getItem("sicodoc_token");
      return axios.get('sicodoc/permissions').then(response => response.data).then(roles => context.commit('setRoles', roles));
    }
  }
});