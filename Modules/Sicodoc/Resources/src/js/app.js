
import 'vue-multiselect/dist/vue-multiselect.min.css';
import 'vue2-dropzone/dist/vue2Dropzone.min.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import {ServerTable, ClientTable, Event} from 'vue-tables-2';
import Multiselect from 'vue-multiselect';
import datetime from 'vuejs-datetimepicker';
import vue2Dropzone from 'vue2-dropzone';
import VeeValidate, { Validator } from 'vee-validate';
import attributesEs from 'vee-validate/dist/locale/es';
import attributesEn from 'vee-validate/dist/locale/en';

import router from './routes'
import store from './store'

import Datepicker from 'vuejs-datepicker';
import CKEditor from '@ckeditor/ckeditor5-vue';
import VueResource from 'vue-resource';

require('bootstrap');
require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
require('@coreui/coreui/dist/js/coreui.min.js');
window.moment=require('moment');
window.Swal=require('sweetalert2');
//window.convertir=require('numero-a-letras');


try {
  window.$ = window.jQuery = require('jquery');
} catch (e) {console.log(e);}


Vue.component('multiselect', Multiselect)
Vue.component('datetime', datetime)
Vue.component('dropzone',vue2Dropzone)
Vue.component('datepicker',Datepicker)


Vue.use(VueRouter);
Vue.use(CKEditor);
Vue.use(VueResource);
Vue.use (ClientTable , { theme: 'bootstrap4',template: 'default' })
Vue.use (Event)
Vue.use(VeeValidate,{
  locale: 'es',
  classes: true,
  classNames: {
    valid: "is-valid",
    invalid: "is-invalid"
  },
    dictionary: {
      translationsEn: { attributes: attributesEn },
      translationsEs: { attributes: attributesEs }
    }
});
Validator.localize('es',attributesEs);

/*

import Home from './components/Home.vue';
import NuevoDocumento from './components/NuevoDocumento.vue';
import RespuestaDocumento from './components/RespuestaDocumento.vue';



const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Home
    },
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/documento/nuevo',
      name: 'nuevodocumento',
      component: NuevoDocumento
    },
    {
      path: '/documento/respuesta/:doctoId',
      name: 'respuestadocumentop',
      component: RespuestaDocumento,
      props: true
    },
    {
      path: '/documento/respuesta/',
      name: 'respuestadocumento',
      component: RespuestaDocumento
    },
    
  ],
});

const app = new Vue({
  el: '#app',
  router,
});*/

const app = new Vue({
  el: '#app',
  store,
  router,
  mounted(){
    store.dispatch('fetchRoles');
  }
});
