import VueRouter from 'vue-router'
import Vue from 'vue'
import store from './../store'

import Index from './../components/Index.vue';
import Home from './../components/Home.vue';
import NuevoDocumento from './../components/NuevoDocumento.vue';
import RespuestaDocumento from './../components/RespuestaDocumento.vue';
import NoAutorizado from './../components/403.vue';
import CopiasConocimiento from './../components/CopiasConocimiento.vue';
import ListadoSalida from './../components/ListadoSalida.vue';

const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: Home,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
           return next('/')
          })
        }
        else if(! store.getters.has(['CAPTURISTA', 'RESPONSABLE','ADMINISTRATIVO','ADMINISTRADOR','REVISOR ENTRADA','REVISOR SALIDA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
           return next('/home')
          })
        }
        else if(! store.getters.has(['CAPTURISTA','RESPONSABLE','ADMINISTRATIVO','ADMINISTRADOR','REVISOR ENTRADA','REVISOR SALIDA','DIRECTOR'])){
          return next('/403');
        }
        next();
      }
      // meta: {requiresCap: true, requiresAdmin: true}
    },
    {
      path: '/listadosalida',
      name: 'listadosalida',
      component: ListadoSalida,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
           return next('/listadosalida')
          })
        }
        else if(! store.getters.has(['RESPONSABLE','ADMINISTRATIVO','ADMINISTRADOR','REVISOR ENTRADA','REVISOR SALIDA','DIRECTOR'])){
          return next('/403');
        }
        next();
      }
      // meta: {requiresCap: true, requiresAdmin: true}
    },
    {
      path: '/documento/nuevo',
      name: 'nuevodocumento',
      component: NuevoDocumento,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/documento/nuevo')
          })
        }
        else if(! store.getters.has(['CAPTURISTA', 'RESPONSABLE', 'ADMINISTRADOR', 'ADMINISTRATIVO'])){
          return next('/403');
        }
        next();
      }
      // meta: {requiresCap: true, requiresAdmin: true}
    },
    {
      path: '/documento/respuesta/:doctoId',
      name: 'respuestadocumentop',
      component: RespuestaDocumento,
      props: true
    },
    {
      path: '/documento/respuesta/',
      name: 'respuestadocumento',
      component: RespuestaDocumento
    },
    {
      path: '/403',
      name: 'NoAutorizado',
      component: NoAutorizado
		},
		{
      path: '/copiasConocimiento',
      name: 'copiasconocimiento',
      component: CopiasConocimiento,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/copiasConocimiento')
          })
        }
        else if(! store.getters.has(['RESPONSABLE', 'ADMINISTRATIVO', 'ADMINISTRADOR', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   if (to.meta.requiresCap){
//       if(store.getters.has(['CAPTURISTA'])){
//         next();
//       }
    
//       else if (to.meta.requiresAdmin){  
//           if(store.getters.has(['ADMINISTRADOR'])){
//             next();
//           }
//           else {
//             next('/403');
//           }
//         }
      
//       else {
//         next('/403');
//       }
//     }

//    else{
//      next();
//     }

// });

export default router;