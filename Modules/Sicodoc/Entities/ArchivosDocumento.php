<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArchivosDocumento extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_documentos_archivos';
    protected $fillable = ['documento_id','archivo_id','usuario_id','created_at'];
    protected $hidden = [ 'updated_at', 'deleted_at'];

    public function documento()
    {
        return $this->belongsTo('Modules\Sicodoc\Entities\Documentos','documento_id','id');
    }

    public function archivo() {
        return $this->belongsTo('Modules\Sicodoc\Entities\Archivo', 'archivo_id', 'id');
    }

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }


}
