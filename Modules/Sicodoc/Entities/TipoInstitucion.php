<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoInstitucion extends Model
{
    use SoftDeletes;
    protected $table = 'cat_tiposinstitucion';
    protected $fillable = ['nombre'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
