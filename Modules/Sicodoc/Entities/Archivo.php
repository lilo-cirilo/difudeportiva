<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Archivo extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_archivos';
    protected $fillable = ['tipoarchivo_id','nombre','url','tamanio','mime','usuario_id'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
