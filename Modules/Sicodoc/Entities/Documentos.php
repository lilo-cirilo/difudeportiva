<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documentos extends Model
{
    use SoftDeletes;
    protected $table = 'sdoc_documentos';
    protected $fillable = ['progresivo','fechaentrada','usuario_id','remitente_id','destinatario_id','tipodocumento_id','numerodocumento','asunto','observaciones','fechavencimiento','estatus_id','folio', 'es_copia_conocimiento'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

     public function historial()
     {
      return $this->hasMany('Modules\Sicodoc\Entities\SeguimientoDocumentos','documento_id','id');
		 }
		 
		 public function lasthistorial()
		 {
			return $this->hasOne('Modules\Sicodoc\Entities\SeguimientoDocumentos','documento_id','id')->latest();
		 }

		 public function copiasconocimiento()
		 {
			return $this->hasMany('Modules\Sicodoc\Entities\SeguimientoDocumentos','documento_id','id')->where('estatus_id',9);
		 }

    public function destinatario()
    {
      return $this->belongsTo('Modules\Sicodoc\Entities\AllResponsables','destinatario_id','id');
    }
    public function remitente()
    {
      return $this->belongsTo('Modules\Sicodoc\Entities\AllResponsables','remitente_id','id');
    }
    public function tipoDocumento()
    {
      return $this->belongsTo('Modules\Sicodoc\Entities\TipoDocumento','tipodocumento_id','id');
    }
    public function estatus()
    {
       return $this->hasOne('Modules\Sicodoc\Entities\Estatus','id','estatus_id');
    }
    public function archivos()
		{
			return $this->hasMany('Modules\Sicodoc\Entities\ArchivosDocumento','documento_id','id');
		}
		public function esrespuesta()
		{
			return $this->belongsTo('Modules\Sicodoc\Entities\RespuestasDocumentos','id','documentorespuesta_id');
		}
		public function contenido()
		{
			//return $this->belongsTo('Modules\Sicodoc\Entities\HistorialContenido','id','documento_id');
			return $this->hasOne('Modules\Sicodoc\Entities\HistorialContenido','documento_id','id')->latest();
    }
    
    public function detalle()
		{
			return $this->hasOne('Modules\Sicodoc\Entities\DocumentoDetalles','documento_id','id');
		}
}
