<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;

class RespuestasDocumentos extends Model
{
    protected $table = 'sdoc_respuestas_documentos';
	protected $fillable = ['id','documento_id','documentorespuesta_id','usuario_id'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

	public function documentoentrada()
	{
		return $this->belongsTo('Modules\Sicodoc\Entities\Documentos','documento_id','id');
	}
}
