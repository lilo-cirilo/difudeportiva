<?php

namespace Modules\Sicodoc\Entities;

use Illuminate\Database\Eloquent\Model;

class ResponsablesInstituciones extends Model
{
    protected $table = 'responsables_instituciones';
    protected $fillable = ['id','area','responsable','cargo'];
}
