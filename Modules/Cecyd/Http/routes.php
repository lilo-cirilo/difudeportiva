<?php

Route::group(['middleware' => 'web', 'prefix' => 'cecyd', 'namespace' => 'Modules\Cecyd\Http\Controllers'], function(){
  Route::get('/', 'PeticionController@index');
  Route::resource('peticiones', 'PeticionController',['as'=>'cecyd','only'=>['index','show','update','destroy']]);   //Peticiones
  Route::resource('programas', 'ProgramaController',['as'=>'cecyd']);                                                //Programas
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'cecyd' ]);                                //Beneficiarios
});
