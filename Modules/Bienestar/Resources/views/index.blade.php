@extends('bienestar::layouts.master')

@section('content-subtitle')
Dashboard
@endsection

@push('head')
@endpush

@section('content')
<div class="row cards">

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="small-box bg-aqua shadow">
			<div class="inner">
				<p>BENEFICIARIOS: </p>
				<h4 id="padron"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h4>
				<a class="btn btn-primary btn-sm btn-flat" href="/bienestar/bienestarpersonas/export"><i class="fa fa-fw fa-file-excel-o"></i> Descargar</a>
			</div>
			<!--<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>-->
		</div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="small-box bg-green shadow">
			<div class="inner">
				<p>FEMENINO: </p>
				<h4 id="f"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h4>
			</div>
			<!--<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>-->
		</div>
	</div>

	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
		<div class="small-box bg-yellow shadow">
			<div class="inner">
				<p>MASCULINO: </p>
				<h4 id="m"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h4>
			</div>
			<!--<div class="icon">
				<i class="ion ion-person-add"></i>
			</div>-->
		</div>
	</div>

</div>

<div class="nav-tabs-custom">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_1" data-toggle="tab">REGIONES:</a></li>
		<li><a href="#tab_2" data-toggle="tab">DISPERSIONES:</a></li>
		<li><a href="#tab_3" data-toggle="tab">DISCAPACIDADES:</a></li>
	</ul>

	<div class="tab-content">

		<div class="tab-pane active" id="tab_1">
			<div class="box-header with-border">
			</div>

			<div class="box-body" id="container" style="width: 100%; height: 650px;"></div>

			<div class="box-footer">
				<div id="mensaje"></div>
			</div>
		</div>

		<div class="tab-pane" id="tab_2">
			<div class="form-group">
				<label for="pagado">Seleccionar:</label>
				<div class="input-group">
					<select id="pagado" name="pagado" class="form-control select2" style="width: 100%;">
						<option value="0">DISPERSIONES EN PROCESO DE PAGO</option>
						<option value="1">PAGOS CONCLUIDOS</option>
						<option value="2">RECHAZADOS POR BANCO</option>
					</select>
					<div class="input-group-btn">
						<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="show();">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<canvas id="dispersiones" style="width: 100%; height: 400px;"></canvas>
		</div>

		<div class="tab-pane" id="tab_3">

			<table class="table">
				<tr>
					<th>General:</th>
					<th>Especifica:</th>
					<th>Total:</th>
				</tr>

				@foreach($discapacidades as $discapacidad)
				<tr>
					<td>{!! $discapacidad->general !!}</td>
					<td>{!! $discapacidad->especifica !!}</td>
					<td>{!! $discapacidad->total !!}</td>
				<tr>
				@endforeach

			</table>

		</div>

	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components\e\echarts.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\echarts-gl.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\ecStat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\dataTool.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\china.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\world.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\bmap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components\e\simplex.js') }}"></script>

<script type="text/javascript">
	(function() {
		var attachEvent = document.attachEvent;
		var isIE = navigator.userAgent.match(/Trident/);
		//console.log(isIE);
		var requestFrame = (function() {
			var raf = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame ||
			function(fn) { return window.setTimeout(fn, 20); };
			return function(fn) { return raf(fn); };
		})();

		var cancelFrame = (function() {
			var cancel = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame ||
			window.clearTimeout;
			return function(id) { return cancel(id); };
		})();

		function resizeListener(e) {
			var win = e.target || e.srcElement;
			if(win.__resizeRAF__) cancelFrame(win.__resizeRAF__);
			win.__resizeRAF__ = requestFrame(function() {
				var trigger = win.__resizeTrigger__;
				trigger.__resizeListeners__.forEach(function(fn) {
					fn.call(trigger, e);
				});
			});
		}

		function objectLoad(e) {
			this.contentDocument.defaultView.__resizeTrigger__ = this.__resizeElement__;
			this.contentDocument.defaultView.addEventListener('resize', resizeListener);
		}

		window.addResizeListener = function(element, fn) {
			if(!element.__resizeListeners__) {
				element.__resizeListeners__ = [];
				if(attachEvent) {
					element.__resizeTrigger__ = element;
					element.attachEvent('onresize', resizeListener);
				}
				else {
					if(getComputedStyle(element).position == 'static') element.style.position = 'relative';
					var obj = element.__resizeTrigger__ = document.createElement('object'); 
					obj.setAttribute('style', 'display: block; position: absolute; top: 0; left: 0; height: 100%; width: 100%; overflow: hidden; pointer-events: none; z-index: -1;');
					obj.__resizeElement__ = element;
					obj.onload = objectLoad;
					obj.type = 'text/html';
					if(isIE) element.appendChild(obj);
					obj.data = 'about:blank';
					if(!isIE) element.appendChild(obj);
				}
			}
			element.__resizeListeners__.push(fn);
		};

		window.removeResizeListener = function(element, fn) {
			element.__resizeListeners__.splice(element.__resizeListeners__.indexOf(fn), 1);
			if(!element.__resizeListeners__.length) {
				if(attachEvent) element.detachEvent('onresize', resizeListener);
				else {
					element.__resizeTrigger__.contentDocument.defaultView.removeEventListener('resize', resizeListener);
					element.__resizeTrigger__ = !element.removeChild(element.__resizeTrigger__);
				}
			}
		}
	})();
</script>

<script type="text/javascript">
	var padron = JSON.parse('{!! $padron !!}');

	var regiones = JSON.parse('{!! $regiones !!}');

	var generos = JSON.parse('{!! $totalGenero !!}');

	var arr_regiones = [];

	for(var i = 0; i < regiones.length; i++) {
		arr_regiones.push(regiones[i].name);
	}

	$('#padron').text(padron);

	$('#m').text(generos[0].value);

	$('#f').text(generos[generos.length - 1].value);

	var padronRegion = JSON.parse('{!! $padronRegion !!}');

	for(var i = 0; i < padronRegion.length; i++) {
		if(padronRegion[i].name === 'REGIÓN: CAÑADA') {
			padronRegion[i].itemStyle = { color: '#FFC300' };
		}
		if(padronRegion[i].name === 'REGIÓN: COSTA') {
			padronRegion[i].itemStyle = { color: '#FF33C1' };
		}
		if(padronRegion[i].name === 'REGIÓN: ISTMO') {
			padronRegion[i].itemStyle = { color: '#F50D25' };
		}
		if(padronRegion[i].name === 'REGIÓN: MIXTECA') {
			padronRegion[i].itemStyle = { color: '#9033FF' };
		}
		if(padronRegion[i].name === 'REGIÓN: PAPALOAPAM') {
			padronRegion[i].itemStyle = { color: '#3390FF' };
		}
		if(padronRegion[i].name === 'REGIÓN: SIERRA NORTE') {
			padronRegion[i].itemStyle = { color: '#117810' };
		}
		if(padronRegion[i].name === 'REGIÓN: SIERRA SUR') {
			padronRegion[i].itemStyle = { color: '#33FF39' };
		}
		if(padronRegion[i].name === 'REGIÓN: VALLES CENTRALES') {
			padronRegion[i].itemStyle = { color: '#FF5733' };
		}
	}
	
	var generoRegion = JSON.parse('{!! $generoRegion !!}');

	for(var i = 0; i < generoRegion.length; i++) {
		if(generoRegion[i].genero === 'M') {
			generoRegion[i].itemStyle = { color: '#E27D60' };
		}
		if(generoRegion[i].genero === 'F') {
			generoRegion[i].itemStyle = { color: '#A9F5E1' };
		}
	}

	//console.log(generoRegion);
	var dom = document.getElementById('container');
	var eChart = echarts.init(dom);
	var componentIndex;
	var dataIndex;
	//var app = {};
	option = null;
	option = {
		/*title : {
			text: '',
			subtext: '',
			x: 'center'
		},*/
		tooltip : {
			trigger: 'item',
			formatter: function(data) {
				if(componentIndex !== data.componentIndex || dataIndex !== data.dataIndex) {
					$('#mensaje').html('<div class="alert alert-info alert-dismissible" style="background-color: ' + data.color + ' !important; border-color: white; text-align: center;">' +
					//'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
					//'<h4>' +
					//'<i class="icon fa fa-info"></i> ' +
					//data.seriesName + ' ' +
					//'</h4>' +
					'<h4>' +
					data.name +
					'</h4>' +
					'<h4>' +
					'TOTAL: ' + data.value + ' PERSONA(S) ' + ' (' + data.percent + '%)' +
					'</h4>' +
					'</div>');
					//console.log(data);
				}

				componentIndex = data.componentIndex;
				dataIndex = data.dataIndex;
			}
			//formatter: "{a} <br/>{b} : {c} ({d}%)"
		},
		legend: {
			orient: 'horizontal',
			top: 'top',
			data: arr_regiones
		},
		series : [
		{
			name: 'TOTAL REGIÓN',
			type: 'pie',
			selectedMode: 'single',
			radius: '65%',
			label: {
				normal: {
					show: false,
					position: 'inner'
				}
			},
			center: ['50%', '55%'],
			data: padronRegion,
			itemStyle: {
				emphasis: {
					shadowBlur: 10,
					shadowOffsetX: 0,
					shadowColor: 'rgba(0, 0, 0, 0.5)'
				}
			}
		},
		{
			name: 'TOTAL GENERO',
			type:'pie',
			radius: ['75%', '85%'],
			center: ['50%', '55%'],
			avoidLabelOverlap: true,
			label: {
				normal: {
					show: false,
					formatter:'{d}%'
				}
			},
			data: generoRegion
		}
		]
	};
	;
	if(option && typeof option === 'object') {
		eChart.setOption(option, true);

		eChart.on('dblclick', function(params) {
			if('id' in params.data) {
				window.location.href = 'bienestar/distritos/' + params.data.id;
				//window.location.href = 'bienestar/distritos/' + params.data.id + '?color=' + params.color;
			}
		});
	}
	/*$('#container').onresize = function() {
		eChart.resize();
	};*/
	eChartResize = function() {
		eChart.resize();
	};
	addResizeListener(dom, eChartResize);
	//removeResizeListener(dom, eChartResize);
</script>

<script type="text/javascript" src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>

<script type="text/javascript">
var ctx = document.getElementById('dispersiones').getContext('2d');

var dispersiones = new Chart(ctx, {
	type: 'bar',
	data: {
		labels: '',
		datasets: ''
	},
	options: { 'responsive': true }
});
show();

/*function addData(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function removeData(chart) {
	chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    chart.update();
}*/

function show() {
	block();

	var labels = ["Enero-Febrero", "Marzo-Abril", "Mayo-Junio", "Julio-Agosto", "Septiembre-Octubre", "Noviembre-Diciembre"];

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$.ajax({
		url: "{{ route('bienestar.barras') }}",
		type: 'POST',
		data: {
			pagado: $('#pagado').val()
		},
		success: function(response) {
			unblock();

			dispersiones.data.labels  = response[0];

			dispersiones.data.datasets = response[1];

			var data = response[1];

			var suma = [];

			for(var i = 0; i < data[data.length - 1].data.length; i++) {
				suma[i] = 0;
				for(var j = 0; j < data.length; j++) {
					suma[i] = suma[i] + data[j].data[i];
				}
			}

			var new_labels = [];

			for(var i = 0; i < labels.length; i++) {
					new_labels[i] = labels[i] + ': ' + suma[i];
			}

			dispersiones.data.labels = new_labels;

			dispersiones.update();
		},
		error: function(response) {
			unblock();
		}
	});
}
</script>
@endpush