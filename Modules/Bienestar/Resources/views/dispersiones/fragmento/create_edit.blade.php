<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="nombre_listado">Oficio:</label>
            <input type="text" class="form-control" id="nombre_listado" name="nombre_listado" value="{{ $dispersion->nombre_listado or '' }}">
        </div>

        <div class="form-group">
            <label for="importe">Importe:</label>
            <input type="text" class="form-control" id="importe" name="importe" value="1000" readonly="readonly">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label for="bimestre">Bimestre:</label>
            <select id="bimestre" name="bimestre" class="form-control select2" style="width: 100%;">
                @for($i = 1; $i <= 6; $i++)
                <option @if(isset($dispersion) && $i === $dispersion->bimestre) selected @endif>{{ $i }}</option>
                @endfor
            </select>
        </div>

        <div class="form-group">
            <label for="ejercicio_id">Ejercicio:</label>
            <select id="ejercicio_id" name="ejercicio_id" class="form-control select2" style="width: 100%;">
                {{--@foreach($ejercicios as $ejercicio)
                    <option value="{{ $ejercicio->id }}"
                        @if(isset($dispersion) && $ejercicio->id === $dispersion->ejercicio->id)
                        selected
                        @endif>{{ $ejercicio->anio }}
                    </option>
                @endforeach--}}
                <option value="8" selected>2018</option>
            </select>
        </div>
    </div>
</div>