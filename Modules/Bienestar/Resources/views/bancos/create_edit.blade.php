@extends('bienestar::layouts.master')

@section('content-subtitle')
{{ isset($candidato) ? 'Editar' : 'Agregar' }} Solicitante
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<!-- sweetalert2 -->
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
select[readonly] {
	background: #eee;
	cursor: no-drop;
}

select[readonly] option {
	display: none;
}

.modal-body {
	overflow-y: auto;
	max-height: calc(100vh - 210px);
}
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($candidato) ? 'Editar' : 'Agregar' }} Solicitante</h3>
	</div>
	<!-- /.box-header -->
	<!-- form start -->
	<form data-toggle="validator" role="form" id="form_candidato" action="{{ isset($candidato) ? route('bienestar.solicitantes.update', $candidato->id) : route('bienestar.solicitantes.store') }}" method="{{ isset($candidato) ? 'PUT' : 'POST' }}">
		<div class="box-body">

			<div class="row">

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

					<h4>Datos del Solicitante:</h4>

					<hr>

					<div class="form-group">
						<label for="foliounico">Folio Único:</label>
						<input type="text" class="form-control" id="foliounico" name="foliounico" value="{{ $candidato->foliounico or '' }}">
					</div>

					<div class="form-group">
						<label for="tutorado_id">Solicitante:</label>
						<div class="input-group">
							<select id="tutorado_id" name="tutorado_id" class="form-control select2" style="width: 100%;" readonly="readonly">
								@if(isset($candidato))
								<option value="{{ $candidato->persona->id }}" selected>{{ $candidato->persona->get_nombre_completo() }}</option>
								@endif
							</select>
							<div class="input-group-btn">
								<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas('tutorado_id');" {{ isset($candidato) ? 'disabled' : '' }}>
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="discapacidad_id">Discapacidad:</label>
						<select id="discapacidad_id" name="discapacidad_id" class="form-control select2" style="width: 100%;">
							@if(isset($candidato))
							<option value="{{ $candidato->discapacidad->id }}" selected>{{ $candidato->discapacidad->nombre }}</option>
							@endif
						</select>
					</div>

					<div class="form-group">
						<label for="estado_civil">Estado Civil:</label>
						<select id="estadocivil_id" name="estadocivil_id" class="form-control select2" style="width: 100%;">
							@if(isset($candidato))
							<option value="{{ $candidato->estadocivil->id }}" selected>{{ $candidato->estadocivil->nombre }}</option>
							@endif
						</select>
					</div>

					<div class="form-group">
						<label for="grado_estudios">Escolaridad:</label>
						<select id="escolaridad_id" name="escolaridad_id" class="form-control select2" style="width: 100%;">
							@if(isset($candidato))
							<option value="{{ $candidato->escolaridad->id }}" selected>{{ $candidato->escolaridad->nombre }}</option>
							@endif
						</select>
					</div>

					<div class="form-group">
						<label for="ocupacion">Ocupación:</label>
						<select id="ocupacion_id" name="ocupacion_id" class="form-control select2" style="width: 100%;">
							@if(isset($candidato))
							<option value="{{ $candidato->miocupacion->id }}" selected>{{ $candidato->miocupacion->nombre }}</option>
							@endif
						</select>
					</div>

				</div>

				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">

					<h4>Datos del Tutor:</h4>

					<hr>

					<div class="form-group">
						<label for="tutor">Seleccionar Tutor:</label>
						<select id="tutor" name="tutor" class="form-control select2" style="width: 100%;">
							<option value="NO">MISMA PERSONA</option>
							<option value="SI">OTRA PERSONA</option>
						</select>
					</div>

					<div class="form-group" id="fila_tutor" name="fila_tutor" style="display: none;">
						<label for="persona_id">Tutor:</label>
						<div class="input-group">
							<select id="persona_id" name="persona_id" class="form-control select2" style="width: 100%;" readonly="readonly">
								@if(isset($tutor))
								<option value="{{ $tutor->persona->id }}" selected>{{ $tutor->persona->get_nombre_completo() }}</option>
								@endif
							</select>
							<div class="input-group-btn">
								<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas('persona_id');">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div>
		<!-- /.box-body -->

		<div class="box-footer">
			<div class="pull-right">
				<button type="button" class="btn btn-success" onclick="candidato.create_edit();"><i class="fa fa-database"></i> Guardar</button>
				<a href="{{ URL::previous() }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
			</div>
		</div>
	</form>
</div>
<!-- /.box -->

<div class="modal fade" id="modal-personas">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-title-personas">Tabla Personas:</h4>
			</div>
			<div class="modal-body" id="modal-body-personas">

				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
					<input type="text" id="search" name="search" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
					</span>
				</div>

				{!! $solicitantes->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}

			</div>
			<div class="modal-footer" id="modal-footer-personas">
				<div class="pull-right">
					<button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-tutores">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-title-tutores">Tabla Tutores:</h4>
			</div>
			<div class="modal-body" id="modal-body-tutores">

				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
					<input type="text" id="search" name="search" class="form-control">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
					</span>
				</div>

				{!! $tutores->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'tutores', 'name' => 'tutores', 'style' => 'width: 100%']) !!}

			</div>
			<div class="modal-footer" id="modal-footer-tutores">
				<div class="pull-right">
					<button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-persona">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-title-persona"></h4>
			</div>
			<div class="modal-body" id="modal-body-persona">

			</div>
			<div class="modal-footer" id="modal-footer-persona">
				<div class="pull-right">
					<button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@push('body')
<!-- sweetalert2 -->
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $solicitantes->html()->scripts() !!}
{!! $tutores->html()->scripts() !!}

@include('personas.js.persona')

<script type="text/javascript">
	function quitar_candado() {
		swal({
			title: 'Ingrese sus credenciales para desbloquear:',
			html:
			'<div class="form-group has-feedback"><input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario"><span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>' +
			'<div class="form-group has-feedback"><input type="password" class="form-control" id="contrasenia" name="contrasenia" placeholder="Contraseña"><span class="glyphicon glyphicon-lock form-control-feedback"></span></div>',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			confirmButtonText: 'Aceptar',
			cancelButtonText: 'Cancelar',
			allowEscapeKey: false,
			allowOutsideClick: false,
			focusConfirm: true,
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return new Promise(function(resolve, reject) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});

					$.ajax({
						url: "{{ route('bienestar.solicitantes.acceso') }}",
						type: 'POST',
						data: {
							usuario: $('#usuario').val(),
							contrasenia: $('#contrasenia').val()
						},
						success: function(response) {
							resolve();
						},
						error: function(response) {
							resolve();
							swal.showValidationError(JSON.parse(response.responseText).errors[0].message);
						}
					});
				});
			}
		}).then((result) => {
			if(result.value) {
				$('#num_cuenta').attr('readonly', false);
				$('#agregarCandado').hide();
				$('#quitarCandado').show();
				setTimeout(function() {
					agregar_candado();
				}, 60000);
			}
		});
	}

	function agregar_candado() {
		$('#num_cuenta').attr('readonly', true);
		$('#quitarCandado').hide();
		$('#agregarCandado').show();
	}

	@if(isset($tutor))
		@if($tutor->persona->id === $candidato->persona->id)
		@else
			$('#fila_tutor').show();
			$('#tutor option[value=SI]').attr('selected', 'selected');
		@endif
	@else

	@endif

	var candidato = (function() {
		var modal_personas = $('#modal-personas'),
		modal_tutores = $('#modal-tutores'),
		modal_persona = $('#modal-persona'),
		personas = $('#personas'),
		datatable_personas = undefined,
		datatable_tutores = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search'),
		elemento = undefined,
		ele = undefined,
		tutor = $('#tutor'),
		fila_tutor = $('#fila_tutor'),
		discapacidad = $('#discapacidad_id'),
		estado_civil = $('#estado_civil'),
		grado_estudios = $('#grado_estudios'),
		banco = $('#banco_id'),
		form = $('#form_candidato'),
		action = form.attr('action'),
		method = form.attr('method');

		function agregar_modal_static() {
			$('.modal').modal({
				backdrop: 'static'
			});

			$('.modal').modal('hide');
		};

		function abrir_modal_personas(el) {
			elemento = $('#' + el);
			ele = el;
			if(ele === 'tutorado_id') {
				modal_personas.modal('show');
			}
			if(ele === 'persona_id') {
				modal_tutores.modal('show');
			}
		};

		function persona_create_edit_success(response) {
			if(elemento !== undefined && ele !== undefined) {
				elemento.html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
				modal_persona.modal('hide');
				if(ele === 'tutorado_id') {
					modal_personas.modal('show');
				}
				if(ele === 'persona_id') {
					modal_tutores.modal('show');
				}
			}
			unblock();
		};

		function persona_create_edit_error(response) {
			unblock();
		};

		function init_modal_persona() {
			$.fn.modal.Constructor.prototype.enforceFocus = function() {};

			var table = $('#personas').dataTable();

			datatable_personas = $(table).DataTable();

			search.keypress(function(e) {
				if(e.which === 13) {
					datatable_personas.search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				datatable_personas.search(search.val()).draw();
			});

			modal_personas.on('shown.bs.modal', function(event) {
				recargar_tabla_personas();
			});

			var tabla_tutores = $('#tutores').dataTable();

			datatable_tutores = $(tabla_tutores).DataTable();

			modal_tutores.on('shown.bs.modal', function(event) {
				recargar_tabla_tutores();
			});
		}

		function recargar_tabla_personas() {
			if(datatable_personas !== undefined) {
				datatable_personas.ajax.reload();
			}
		};

		function recargar_tabla_tutores() {
			if(datatable_tutores !== undefined) {
				datatable_tutores.ajax.reload();
			}
		};

		function seleccionar_persona(id, nombre) {
			if(elemento !== undefined) {
				elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
				if(ele === 'tutorado_id') {
					modal_personas.modal('hide');
				}
				if(ele === 'persona_id') {
					modal_tutores.modal('hide');
				}
			}
		};

		function agregar_tutor() {
			tutor.change(function() {
				if($(this).val() === 'NO') {
					$('#persona_id').rules('remove', 'required');
					fila_tutor.removeAttr('style').hide();
				}
				if($(this).val() === 'SI') {
					$('#persona_id').rules('add', { required: true });
					fila_tutor.show();
				}
			});
		};

		function agregar_discapacidad_select_create() {
			discapacidad.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('discapacidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					//delay: 500,
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						var datos = [];

						for(var j = 0; j < data.length; j++){
							var found = false;

							for(var i = 0; i < datos.length; i++) {
								if(datos[i].text === data[j].padre) {
									datos[i].children.push({
										'id': data[j].id,
										'text': data[j].nombre
									});

									found = true;

									break;
								}
							}

							if(!found) {
								datos.push({
									text: data[j].padre,

									children: [{
										'id': data[j].id,
										'text': data[j].nombre
									}]
								});
							}
						}

						params.page = params.page || 1;

						return {
							results: datos
						};
					},
					cache: true
				}
			}).change(function(event) {
				discapacidad.valid();
			});

			estado_civil.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});

			grado_estudios.select2({
				language: 'es'
			});

			tutor.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});
		};

		function agregar_banco_select_create() {
			banco.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('bancos.select') }}',
					//delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				banco.valid();
			});
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					foliounico: {
						required: true,
						minlength: 5,
						maxlength: 5,
						pattern_integer: ''
					},
					tutorado_id: {
						required: true
					},
					persona_id: {
						required: true
					},
					discapacidad_id: {
						required: true
					},
					estadocivil_id: {
						required: true
					},
					escolaridad_id: {
						required: true
					},
					ocupacion_id: {
						required: true
					}/*,
					banco_id: {
						required: true
					},
					num_cuenta: {
						required: true,
						minlength: 11,
						maxlength: 11,
						pattern_integer: ''
					},
					num_tarjeta: {
						required: false,
						minlength: 16,
						maxlength: 16,
						pattern_integer: ''
					}*/
				},
				messages: {
				}
			});
		};

		function create_edit() {
			if(form.valid()) {

				block();

				var formData = new FormData(form[0]);

				@if(isset($candidato))
				var old_candidato_id = {{ $candidato->id }};
				formData.append('old_candidato_id', old_candidato_id);
				@endif

				@if(isset($tutor))
				var old_tutor_id = {{ $tutor->id }};
				formData.append('old_tutor_id', old_tutor_id);
				@endif

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(response) {
						app.set_bloqueo(false);
						unblock();
						swal({
							title: 'Solicitante registrado.',
							text: '¿Agregar otro solicitante?',
							type: 'success',
							timer: 10000,
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonClass: 'btn btn-success',
							cancelButtonClass: 'btn btn-danger',
							confirmButtonText: 'Si',
							cancelButtonText: 'No',
							allowEscapeKey: false,
							allowOutsideClick: false
						}).then((result) => {
							if(result.value) {
								location.reload();
							}

							if(result.dismiss) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}

							if(result.dismiss === swal.DismissReason.timer) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}
						});
					},
					error: function(response) {
						unblock();

						if(response.status === 422) {
							swal({
								title: 'Error al tratar de guardar al solicitante.',
								text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Cerrar',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}

						if(response.status === 409) {
							var e = JSON.parse(response.responseText).errors[0];
							var l = e.data;
							var m = e.message;
							swal({
								title: 'Error al tratar de guardar al solicitante.',
								html: m,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Si',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
					}
				});

			}
		};

		return {
			agregar_modal_static: agregar_modal_static,
			persona_create_edit_success: persona_create_edit_success,
			persona_create_edit_error: persona_create_edit_error,
			abrir_modal_personas: abrir_modal_personas,
			init_modal_persona: init_modal_persona,
			recargar_tabla_personas: recargar_tabla_personas,
			seleccionar_persona: seleccionar_persona,
			agregar_tutor: agregar_tutor,
			agregar_discapacidad_select_create: agregar_discapacidad_select_create,
			agregar_banco_select_create: agregar_banco_select_create,
			agregar_validacion: agregar_validacion,
			create_edit: create_edit
		};
	})();

	function seleccionar_persona(id, nombre) {
		candidato.seleccionar_persona(id, nombre);
	}

	function agregar_persona() {
		var modal_persona = $('#modal-persona');

		var modal_title_persona = $('#modal-title-persona');

		var modal_body_persona = $('#modal-body-persona');

		var modal_footer_persona = $('#modal-footer-persona');

		$.get('/personas/search?tipo=create_edit', function(data) {
		})
		.done(function(data) {
		})
		.fail(function(data) {

			modal_title_persona.text('Agregar Persona:');
			modal_body_persona.html($.parseJSON(data.responseText).html);

			app.to_upper_case();

			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();

			modal_persona.modal('show');

			var btn_create_edit = $('#btn_create_edit');
			btn_create_edit.attr('class', 'btn btn-success');
			btn_create_edit.html('<i class="fa fa-plus"></i> Agregar');

		});
	}

	function editar_persona(id) {
		var modal_persona = $('#modal-persona');

		var modal_title_persona = $('#modal-title-persona');

		var modal_body_persona = $('#modal-body-persona');

		var modal_footer_persona = $('#modal-footer-persona');

		var tipo_persona = "'" + tipo_persona + "'";

		$.get('/personas/search?tipo=create_edit&id=' + id, function(data) {
		})
		.done(function(data) {

			modal_title_persona.text('Editar Persona:');
			modal_body_persona.html(data.html);

			app.to_upper_case();

			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_select_edit();
			persona.agregar_validacion();

			modal_persona.modal('show');

			var btn_create_edit = $('#btn_create_edit');
			btn_create_edit.attr('class', 'btn btn-warning');
			btn_create_edit.html('<i class="fa fa-pencil"></i> Editar');

		})
		.fail(function(data) {
		});
	}

	app.to_upper_case();
	app.agregar_bloqueo_pagina();

	candidato.agregar_modal_static();
	candidato.init_modal_persona();
	candidato.agregar_tutor();
	candidato.agregar_discapacidad_select_create();
	candidato.agregar_banco_select_create();
	candidato.agregar_validacion();

	function persona_create_edit_success(response) {
		candidato.persona_create_edit_success(response);
	}

	function persona_create_edit_error(response) {
		candidato.persona_create_edit_error(response);
		if(response.status === 422) {
			swal({
				title: 'Error al tratar de guardar a la persona.',
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Cerrar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}

		if(response.status === 409) {
			var e = JSON.parse(response.responseText).errors[0];
			var l = e.data;
			var m = e.message;
			swal({
				title: 'Error al tratar de guardar a la persona.',
				html: m,
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Si',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	}

    function agregar_estadosciviles_select_create(estadocivil) {
        estadocivil.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('estadosciviles.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            estadocivil.valid();
        });
    };

    function agregar_escolaridades_select_create(escolaridad) {
        escolaridad.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('escolaridades.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            escolaridad.valid();
        });
    };

    function agregar_ocupaciones_select_create(ocupacion) {
        ocupacion.select2({
            language: 'es',
            //minimumInputLength: 2,
            ajax: {
                url: '{{ route('ocupaciones.select') }}',
                //delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            ocupacion.valid();
        });
    };

    agregar_estadosciviles_select_create($('#estadocivil_id'));

    agregar_escolaridades_select_create($('#escolaridad_id'));

    agregar_ocupaciones_select_create($('#ocupacion_id'));
</script>
@endpush