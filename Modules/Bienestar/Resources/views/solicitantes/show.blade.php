@extends('bienestar::layouts.master')

@section('content-subtitle')
	Consultar Solicitante
@endsection

@push('head')
<style type="text/css">
	.collapsed .fa {
		transform: rotate(45deg);
	}
	button .fa {
  		transition: .3s transform ease-in-out;
	}
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Consultar Solicitante:</h3>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
	@include('bienestar::solicitantes.plantillas.datos_Candidatos')		
	</div>
	<!-- /.box-body -->

	<div class="box-footer">
		<a href="{{ route('bienestar.solicitantes.index') }}" class="btn btn-danger btn-md"><i class="fa fa-reply"></i> Regresar</a>
		<!--<a href="{{ route('bienestar.solicitantes.edit', $solicitante->id) }}" class="btn btn-warning btn-md pull-right"><i class="fa fa-pencil"></i> Editar</a>-->
	</div>
</div>
@stop

@push('body')
<script type="text/javascript">
</script>
@endpush
