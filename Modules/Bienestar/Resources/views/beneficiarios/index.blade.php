@extends('bienestar::layouts.master')

@section('content-subtitle')
Tabla de Beneficiarios
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary shadow">
	<div class="box-header with-border">
		<h3 class="box-title">Tabla de Beneficiarios:</h3>

		{{--@if(auth()->user()->hasRoles(['SUPERADMIN', 'COORDINADOR', 'OPERATIVO', 'APOYO OPERATIVO', 'ADMINISTRATIVO', 'LEGAL', 'AUXILIAR', 'FINANCIERO', 'MEDICO', 'CAPTURISTA']))
		<a href="{{ route('bienestar.solicitantes.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar</a>
		@endif--}}

		<div class="box box-solid collapsed-box" style="margin-bottom: 0px; box-shadow: 0 0px 0px rgba(0, 0, 0, 0);">
			<div class="box-header with-border">
				<h3 class="box-title"></h3>

				<div class="box-tools">
					<button type="button" class="btn btn-box-tool" data-widget="collapse">FILTROS <i class="fa fa-filter"></i></button>
				</div>
			</div>
			<div class="box-body no-padding" id="filtro" style="display: none; color: #97a0b3;">
				<div class="form-group">
					<!--<label for="foliounico" class="col-sm-2 control-label">FOLIO:</label>-->
					<label>FOLIO:</label>
					<input type="text" class="form-control" name="foliounico" id="foliounico">
				</div>
			</div>
		</div>
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>

		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiarios', 'name' => 'beneficiarios', 'style' => 'width: 100%']) !!}

	</div>
</div>

<div class="modal fade" id="modal-inactivar">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="cerrar_modal('modal-inactivar')" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="modal-inactivar-title">Inactivar beneficiario</h4>
			</div>
			<div class="modal-body" id="modal-body-inactivar">
				<form data-toggle="validator" role="form" id="form_inactivar">              
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Folio Unico:</label>
								<span id="folio"></span>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label>Beneficiario:</label>
								<span id="beneficiario"></span>
							</div>
						</div>
				 
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="motivo_id">Motivo:</label>
								<select class="form-control select2" id="motivo_id" name="motivo_id" style="width: 100%;">                    
									<option selected value="">SELECCIONE UNA OPCION</option>
									@foreach($motivos as $motivo)
										<option value="{{ $motivo->id }}">{{ $motivo->motivo }}</option>
									@endforeach
								</select>
							</div>
						</div>
						
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="form-group">
								<label for="observacion">Observacion:</label>
								<textarea id="observacion" name="observacion" class="form-control" rows="3"></textarea>
							</div>
						</div>
					</div>
				 </form>
			</div>
			<div class="modal-footer" id="modal-footer-inactivar">
				<button type="button" class="btn btn-success" onclick="inactivar_beneficiario()">
					<i class="fa fa-database"></i>
					Inactivar</button>
				<button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-inactivar')">
						<i class="fa fa-remove"></i>
					Cerrar</button>    
			</div>      
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	$(':input').on('propertychange input', function(e) {
		var ss = e.target.selectionStart;
		var se = e.target.selectionEnd;
		e.target.value = e.target.value.toUpperCase();
		e.target.selectionStart = ss;
		e.target.selectionEnd = se;
	});

	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#beneficiarios').dataTable());
	table.init();

	$('#form_inactivar').validate({
		rules: {
			motivo_id: {
				required: true
			}				
		},
		messages: {
		}
	});

	$("#motivo_id").select2({
		language: 'es',
		placeholder: 'SELECCIONE UN MOTIVO',
        minimumResultsForSearch: Infinity			
	}).change(function(event) {
		$("#motivo_id").valid();				
	});			

	function abrir_modal(modal, persona_programa_id, bienestar_persona_id, folio, beneficiario) {
		//console.log(persona_programa_id);
		//console.log(bienestar_persona_id);
		$('#motivo_id').val(null).trigger('change');
		$('#observacion').val('');
		$("#form_inactivar").attr('action','/bienestar/beneficiarios/inactivarbeneficiario/' + persona_programa_id + '?persona_programa_id=' + persona_programa_id + '&bienestar_persona_id=' + bienestar_persona_id);
		$("#folio").text(folio);
		$("#beneficiario").text(beneficiario);
		$("#"+modal).modal("show");
	}

	function cerrar_modal(modal){
		$("#"+modal).modal('hide');
	}

	function inactivar_beneficiario() {
		if(!$('#form_inactivar').valid()){
			return;
		}
		swal({
			title: '¿Está seguro?',
			text: '¡Usted va a inactivar a un beneficiario!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'No',
			allowEscapeKey: false,
			allowOutsideClick: false
		}).then((result) => {
			if(result.value) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: $('#form_inactivar').attr("action"),
					type: 'POST',
					data: $('#form_inactivar').serialize(),
					success: function(response) {
						cerrar_modal("modal-inactivar");
						table.get_table().DataTable().ajax.reload(null, false);
						unblock();
						swal({
							title: '¡Correcto!',
							text: 'El beneficiario ha sido inactivado.',
							type: 'success',
							timer: 3000,
							confirmButtonText: 'Si',
							allowEscapeKey: false,
							allowOutsideClick: false
						});
					},
					error: function(response) {
						table.get_table().DataTable().ajax.reload(null, false);
						unblock();
						if(response.status === 409) {
							var e = JSON.parse(response.responseText).errors[0];
							var l = e.data;
							var m = e.message;
							swal({
								title: 'Error al tratar de inactivar al beneficiario.',
								html: m,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Si',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
					}
				});
			}
		});
	}

	function reactivar_beneficiario(candidato_id) {
		swal({
			title: '¿Estas seguro?',
			text: '¡Usted va a reactivar a un beneficiario!',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si',
			cancelButtonText: 'No',
			allowEscapeKey: false,
			allowOutsideClick: false
		}).then((result) => {
			if(result.value) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: "{{ URL::to('bienestar/beneficiarios/reactivarbeneficiario') }}" + '/' + candidato_id,
					type: 'POST',
					success: function(response) {
						table.get_table().DataTable().ajax.reload(null, false);
						unblock();
						swal({
							title: '¡Correcto!',
							text: 'El beneficiario ha sido reactivado.',
							type: 'success',
							timer: 3000,
							confirmButtonText: 'Si',
							allowEscapeKey: false,
							allowOutsideClick: false
						});
					},
					error: function(response) {
						table.get_table().DataTable().ajax.reload(null, false);
						unblock();
						if(response.status === 409) {
							var e = JSON.parse(response.responseText).errors[0];
							var l = e.data;
							var m = e.message;
							swal({
								title: 'Error al tratar de reactivar al beneficiario.',
								html: m,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Si',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
					}
				});
			}
		});
	}
</script>

<script type="text/javascript">
	//console.log(window.LaravelDataTables['beneficiarios']);
$(document).ready(function () {
    /*$('.js-datatable-filter-form :input').on('change', function (e) {
        window.LaravelDataTables["dataTableBuilder"].draw();
    });*/

    //$('#beneficiarios').on('preXhr.dt', function ( e, settings, data ) {
    	//data.ejemplo = 'Hola Mundo';
        /*$('.js-datatable-filter-form :input').each(function () {
            data[$(this).prop('name')] = $(this).val();
        });*/
    //});

    /*$('#filtro :input').on('change', function (e) {
        window.LaravelDataTables["beneficiarios"].draw();
    });*/

    $('#filtro :input').on('keydown', function(e) {
    	if(e.which == 13) {
    		window.LaravelDataTables["beneficiarios"].draw();
    		e.preventDefault();
    	}
    });

    $('#beneficiarios').on('preXhr.dt', function ( e, settings, data ) {
        $('#filtro :input').each(function () {
            data[$(this).prop('name')] = $(this).val();
        });
    });
});
</script>
@endpush