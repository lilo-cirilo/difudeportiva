<?php

namespace Modules\Bienestar\Entities;

use Illuminate\Database\Eloquent\Model;

use Modules\Bienestar\Entities\Base;

class RefrendoPersona extends Base {
    protected $table = 'bien_refrendos_personas';

    protected $fillable = ['refrendo_id', 'bien_persona_id', 'estado_programa_id', 'usuario_id'];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}