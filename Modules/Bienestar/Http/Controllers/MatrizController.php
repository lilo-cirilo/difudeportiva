<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Persona;
use App\Models\BienestarDispersion;
use App\Models\PersonasPrograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use App\Models\Programa;

use App\DataTables\Bienestar\MatrizDataTable;

class MatrizController extends Controller {
    use Bienestar;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(MatrizDataTable $dataTable) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'ASESOR FINANCIERO']);
        
        return $dataTable
        ->with('tipo', 'normal')
        ->render('bienestar::matriz.index');
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('bienestar::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return view('bienestar::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('bienestar::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}