<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Ejercicio;
use App\Models\Programa;
use App\Models\AniosPrograma;
use App\Models\CuentasBancos;
use App\Models\BienestarPersona;
use App\Models\Tutor;

use Modules\Bienestar\Http\Requests\CuentaRequest;
use App\DataTables\Bienestar\SolicitantesDataTable;
use View;

class BancoController extends Controller {
    use Bienestar;

    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SolicitantesDataTable $dataTable) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);
        
        $ejercicio_id = Ejercicio::where('anio', $this->anio)->first()->id;
        
        return $dataTable
        ->with(['tipo' => 'bancos', 'ejercicio_id' => $ejercicio_id])
        ->render('bienestar::bancos.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);

        return view('bienestar::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);

        return view('bienestar::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);
        
        $solicitante = BienestarPersona::findOrFail($id);
        
        $tutor = Tutor::where('tutorado_id', $solicitante->persona->id)->where('programa_id', '=', 1)->firstOrFail();
        
        return response()->json([
            'html' => view::make('bienestar::bancos.plantillas.create_edit')
            ->with(['solicitante' => $solicitante, 'tutor' => $tutor
            ])
            ->render()], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(CuentaRequest $request, $id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);

        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $cuenta_banco = CuentasBancos::where('persona_id', $id)->first();
        
                //Si el tutor tiene cuenta se inactiva y se elimina
                if($cuenta_banco) {
                    $cuenta_banco->update(['activo' => 0]);

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'cuentasbancos',
                        'registro' => $cuenta_banco->id . '',
                        'campos' => json_encode($cuenta_banco) . '',
                        'metodo' => request()->method()
                    ]);

                    $cuenta_banco->delete();

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'cuentasbancos',
                        'registro' => $cuenta_banco->id . '',
                        'campos' => json_encode($cuenta_banco) . '',
                        'metodo' => request()->method()
                    ]);
                }
                
                //Crea un nuevo registro en la BD con los datos de la cuenta bancaria
                $cuenta_banco = CuentasBancos::create([
                    'persona_id' => $id,
                    'banco_id' => $request->banco_id,
                    'num_cuenta' => $request->num_cuenta,
                    'activo' => 1,
                    'usuario_id' => $request->User()->id
                ]);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'cuentasbancos',
                    'registro' => $cuenta_banco->id . '',
                    'campos' => json_encode($cuenta_banco) . '',
                    'metodo' => request()->method()
                ]);

                DB::commit();
                return response()->json(['success' => true, 'cuenta' => $cuenta_banco->num_cuenta, 'code' => 200 ], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error : ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'FINANCIERO']);
    }
}