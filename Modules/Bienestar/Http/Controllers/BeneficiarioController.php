<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\File;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Persona;
use App\Models\BienestarPersona;
use App\Models\BienestarDispersion;
use App\Models\DocumentosPersona;
use App\Models\PersonasPrograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use App\Models\Programa;
use App\Models\Motivo;
use App\Models\StatusBienestarPersona;

use App\DataTables\Bienestar\BeneficiariosDataTable;

//use DataTables;

class BeneficiarioController extends Controller {
    use Bienestar;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(BeneficiariosDataTable $dataTable) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'FINANCIERO', 'APOYO OPERATIVO', 'CAPTURISTA', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES', 'ASESOR FINANCIERO']);

        /*$posiciones_disponibles = PersonasPrograma::withTrashed()
        ->posicionesdisponibles($this->programa, $this->anio)
        ->get(['bienestarpersonas.posicion as posicion']);return $posiciones_disponibles;*/

        $motivos = DB::table('motivos_programas')
        ->select(['motivos_programas.id', 'cat_motivos.motivo'])
        ->where('programas.nombre', '=', 'BIENESTAR')
        ->join('cat_motivos', 'cat_motivos.id', '=', 'motivos_programas.motivo_id')
        ->join('programas', 'programas.id', '=', 'motivos_programas.programa_id')
        ->get();

        return $dataTable
        ->with('tipo', 'normal')
        ->render('bienestar::beneficiarios.index', ['motivos' => $motivos]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('bienestar::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($persona_id, $usuario_id) {
        $ejercicio      = Ejercicio::where('anio',date('Y'))->first();
        $programa_id    = Programa::where('nombre','BIENESTAR')->first();
        $ejercicioActual= AniosPrograma::where('ejercicio_id',$ejercicio->id)
        ->where('programa_id',$programa_id->id)->first();

        $beneficiario['persona_id'] = $persona_id;
        $beneficiario['anios_programa_id']= $ejercicioActual->id;
        $beneficiario['baja']=0;
        $beneficiario['fechabaja']=null;
        $beneficiario['motivo_id']=null;
        $beneficiario['usuario_id']=$usuario_id;

        try {
            DB::beginTransaction();
            $new = PersonasPrograma::firstOrCreate($beneficiario);
            DB::commit();
            return array('success' => true, 'id' => $new->id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES']);

        $beneficiario = PersonasPrograma::find($id);
        $solicitante = $beneficiario;
        return view('bienestar::beneficiarios.show', ['solicitante' => $solicitante]);
    }

    public function update($id, Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO', 'ENLACE CAÑADA', 'ENLACE COSTA', 'ENLACE ISTMO', 'ENLACE MIXTECA', 'ENLACE PAPALOAPAM', 'ENLACE SIERRA NORTE', 'ENLACE SIERRA SUR', 'ENLACE VALLES CENTRALES']);

        if($request->ajax()) {
            try {
                $bienestar_persona = BienestarPersona::find($id);
                
                $bienestar_persona->update($request->all());
                
                return $bienestar_persona;
            }
            catch(\Exception $e) {
                DB::rollBack();
                return array('success' => false);
            }
        }
    }
    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('bienestar::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function refrendar(Request $request) {
        if($request->ajax()){
            $refrendado=PersonasPrograma::find($request->input('persona_id'));
            if($request->input('operacion')=='refrendo'){
                $estado=(object)$this->store($refrendado->persona->id,$request->User()->id);
                if ($estado->success) {
                    $motivo_id=Motivo::where([['tipo','like','BIENESTAR'],['motivo','like','REFRENDO']])->first()->id;
                    $estado=(object)$this->destroy($refrendado->id,$motivo_id);
                    if($estado->success){
                        try{
                            $docPerson['persona_id']=$refrendado->persona->id;
                            $docPerson['usuario_id']=$request->user()->id;
                            $documentos=(object)json_decode($request->input('documentos'));

                            DB::beginTransaction();
                            foreach ($documentos as $documento) {
                                $docPerson['documentos_programa_id']=$documento->id;
                                $url = Storage::putFile('public/documentos', $request->file($documento->nombre));
                                $partes = explode('public', $url);
                                $docPerson['presento'] = 'storage'.$partes[1];
                                $new=DocumentosPersona::create($docPerson);
                            }
                            DB::commit();
                            return response()->json(array('success' => true, 'message' => 'Refrendo correcto'));
                        }catch(Exeption $e){
                            DB::rollBack();
                            return response()->json(array('success' => false, 'message' => 'Error guardando las imagenes'));
                        }
                    }
                    return response()->json(array('success' => false, 'message' => 'Error removiendo al beneficiario'));
                }
                return response()->json(array('success' => false, 'message' => 'Error agregando beneficiario al nuevo ejercicio'));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id,$motivo_id){
        try {
            $refrendado=PersonasPrograma::find($id);
            if(isset($refrendado)){
                DB::beginTransaction();
                $refrendado->update(['motivo_id'=>$motivo_id]);
                $refrendado->delete();
                DB::commit();
                return array('success' => true);
            }
            return array('success' => false);
        }catch(\Exception $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }

    public function inactivar_beneficiario($id, Request $request) {
        $this->acceso(['SUPERADMIN', 'ADMINISTRADOR', 'OPERATIVO']);
        
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $status_bienestar_persona = StatusBienestarPersona::create([
                    'bienestarpersona_id' => $request->bienestar_persona_id,
                    'statusproceso_id' => 7,
                    'motivoprograma_id' => $request->motivo_id,
                    'observacion' => $request->observacion,
                    'usuario_id' => $request->user()->id
                ]);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'status_bienestarpersonas',
                    'registro' => $status_bienestar_persona->id . '',
                    'campos' => json_encode($status_bienestar_persona) . '',
                    'metodo' => request()->method()
                ]);

                $bienestar_persona = BienestarPersona::find($request->bienestar_persona_id);

                if($bienestar_persona) {
                    $bienestar_persona->delete();

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'bienestarpersonas',
                        'registro' => $bienestar_persona->id . '',
                        'campos' => json_encode($bienestar_persona) . '',
                        'metodo' => request()->method()
                    ]);
                }

                $persona_programa = PersonasPrograma::find($request->persona_programa_id);

                if($persona_programa) {
                    $persona_programa->delete();

                    auth()->user()->bitacora(request(), [
                        'tabla' => 'personas_programas',
                        'registro' => $persona_programa->id . '',
                        'campos' => json_encode($persona_programa) . '',
                        'metodo' => request()->method()
                    ]);
                }

                /*$beneficiario = PersonasPrograma::withTrashed()
                ->select('personas_programas.*')
                ->with('anios_programa.programa')
                ->with('anios_programa.ejercicio')
                ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
                ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
                ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
                ->where('programas.nombre', 'LIKE', $this->programa)
                ->where('cat_ejercicios.anio', '=', $this->anio)
                ->with(['bienestarpersonas' => function($q) {
                    $q->withTrashed()
                    ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'bienestarpersonas.ejercicio_id')
                    ->where('cat_ejercicios.anio', '=', $this->anio);
                }])
                ->with('bienestardispersiones.listasdispersiones')
                ->join('bienestardispersiones', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')
                ->join('listas_dispersiones', 'listas_dispersiones.id', '=', 'bienestardispersiones.lista_id')
                ->where('listas_dispersiones.status', '!=', 'Finalizada')
                ->find($id);

                if(isset($beneficiario)) {
                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario no se puede inactivar por que se encuentra en listado(s) de dispersión que aun no finaliza(n).'])], 409);
                }
                else {
                    $beneficiario = PersonasPrograma::withTrashed()
                    ->select('personas_programas.*')
                    ->with('anios_programa.programa')
                    ->with('anios_programa.ejercicio')
                    ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
                    ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
                    ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
                    ->where('programas.nombre', 'LIKE', $this->programa)
                    ->where('cat_ejercicios.anio', '=', $this->anio)
                    ->with(['bienestarpersonas' => function($q) {
                        $q->withTrashed()
                        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'bienestarpersonas.ejercicio_id')
                        ->where('cat_ejercicios.anio', '=', $this->anio);
                    }])
                    ->find($id);

                    if(isset($beneficiario)) {
                        if(isset($beneficiario->bienestarpersonas)) {
                            if(isset($beneficiario->anios_programa)) {
                                if(isset($beneficiario->anios_programa->programa)) {
                                    if(isset($beneficiario->anios_programa->ejercicio)) {
                                        $inactivo = false;

                                        if(!$beneficiario->bienestarpersonas->first()->trashed()) {
                                            $beneficiario->bienestarpersonas->first()->delete();
                                        }
                                        else {
                                            $inactivo = true;
                                        }

                                        if(!$beneficiario->trashed()) {
                                            $beneficiario->delete();
                                        }
                                        else {
                                            $inactivo = true;
                                        }

                                        if($inactivo) {
                                            return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario ya esta inactivo.'])], 409);
                                        }
                                    }
                                    else {
                                        return response()->json(['errors' => array(['code' => 409, 'message' => 'El año no existe.'])], 409);
                                    }
                                }
                                else {
                                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa no existe.'])], 409);
                                }
                            }
                            else {
                                return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa no existe.'])], 409);
                            }
                        }
                        else {
                            return response()->json(['errors' => array(['code' => 409, 'message' => 'La bienestar persona no existe.'])], 409);
                        }
                    }
                    else {
                        return response()->json(['errors' => array(['code' => 409, 'message' => 'La persona programa no existe.'])], 409);
                    }
                }*/

                DB::commit();

                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }

    public function reactivar_beneficiario(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $beneficiario = PersonasPrograma::withTrashed()
                ->select('personas_programas.*')
                ->with('anios_programa')
                ->with('anios_programa.programa')
                ->with('anios_programa.ejercicio')
                ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
                ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
                ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
                ->where('programas.nombre', 'LIKE', $this->programa)
                ->where('cat_ejercicios.anio', '=', $this->anio)
                ->with(['bienestarpersonas' => function($q) {
                    $q
                    ->withTrashed()
                    ->select('bienestarpersonas.*')
                    ->join('personas_programas', 'personas_programas.persona_id', '=', 'bienestarpersonas.persona_id')
                    ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'bienestarpersonas.ejercicio_id')
                    ->where('cat_ejercicios.anio', '=', $this->anio);
                }])
                ->with(['bienestardispersiones' => function($q) {
                    $q
                    ->select('bienestardispersiones.*');
                }])
                ->find($request->id);

                if(isset($beneficiario)) {
                    if(isset($beneficiario->bienestarpersonas)) {
                        if(isset($beneficiario->anios_programa)) {
                            if(isset($beneficiario->anios_programa->programa)) {
                                if(isset($beneficiario->anios_programa->ejercicio)) {
                                    if($this->vigencia <= $beneficiario->anios_programa->ejercicio->anio) {
                                        $activo = false;

                                        if($beneficiario->bienestardispersiones->where('pagado', '=', 1)->first() !== null) {
                                            $tmp_beneficiario = BienestarPersona::where('posicion', '=', $beneficiario->bienestarpersonas->posicion)
                                            ->first();return $tmp_beneficiario;

                                            if(isset($tmp_beneficiario)) {
                                                return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario no se puede reactivar porque esa posición de la matriz está ocupada.'])], 409);
                                            }
                                            else {
                                                if($beneficiario->bienestarpersonas->first()->trashed()) {
                                                    $beneficiario->bienestarpersonas->first()->restore();
                                                }
                                                else {
                                                    $activo = true;
                                                }

                                                if($beneficiario->trashed()) {
                                                    $beneficiario->restore();
                                                }
                                                else {
                                                    $activo = true;
                                                }

                                                if($activo) {
                                                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario ya esta activo.'])], 409);
                                                }
                                            }
                                        }
                                        else {
                                            $posicion = PersonasPrograma::withTrashed()
                                            ->posicion($this->programa, $this->anio)
                                            ->get(['bienestarpersonas.posicion as posicion'])->count() + 1;//return $beneficiario->bienestarpersonas;

                                            if($posicion <= $this->cupo) {
                                                $beneficiario->bienestarpersonas->update(['posicion' => $posicion]);

                                                if($beneficiario->bienestarpersonas->trashed()) {
                                                    $beneficiario->bienestarpersonas->restore();
                                                }
                                                else {
                                                    $activo = true;
                                                }

                                                if($beneficiario->trashed()) {
                                                    $beneficiario->restore();
                                                }
                                                else {
                                                    $activo = true;
                                                }

                                                if($activo) {
                                                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario ya esta activo.'])], 409);
                                                }
                                            }
                                            else {
                                                $posiciones_disponibles = PersonasPrograma::withTrashed()
                                                ->posicionesdisponibles($this->programa, $this->anio)
                                                ->get(['bienestarpersonas.posicion as posicion']);

                                                if(count($posiciones_disponibles) > 0) {
                                                    $beneficiario->bienestarpersonas->update(['posicion' => $posiciones_disponibles[0]['posicion']]);

                                                    if($beneficiario->bienestarpersonas->trashed()) {
                                                        $beneficiario->bienestarpersonas->restore();
                                                    }
                                                    else {
                                                        $activo = true;
                                                    }

                                                    if($beneficiario->trashed()) {
                                                        $beneficiario->restore();
                                                    }
                                                    else {
                                                        $activo = true;
                                                    }

                                                    if($activo) {
                                                        return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario ya esta activo.'])], 409);
                                                    }
                                                }
                                                else {
                                                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El beneficiario no se puede convertir en beneficiario porque la matriz está llena.'])], 409);
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        return 'No Vigente.';
                                    }
                                }
                                else {
                                    return response()->json(['errors' => array(['code' => 409, 'message' => 'El año no existe.'])], 409);
                                }
                            }
                            else {
                                return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa no existe.'])], 409);
                            }
                        }
                        else {
                            return response()->json(['errors' => array(['code' => 409, 'message' => 'El programa no existe.'])], 409);
                        }
                    }
                    else {
                        return response()->json(['errors' => array(['code' => 409, 'message' => 'La bienestar persona no existe.'])], 409);
                    }
                }
                else {
                    return response()->json(['errors' => array(['code' => 409, 'message' => 'La persona programa no existe.'])], 409);
                }

                DB::commit();

                return response()->json(['success' => array(['code' => 200])], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
            }
        }
    }
}