<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Traits\Bienestar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\BienestarDispersion;
use App\Models\PersonasPrograma;
use App\Models\AniosPrograma;
use App\Models\ListaDispersion;
use App\Models\Persona;
use App\Models\CuentasBancos;

use App\Models\FinancierosDispersiones;

use Illuminate\Support\Facades\DB;

class PersonaDispersionController extends Controller
{
    use Bienestar;
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update_bienestar_dispersiones(Request $request) {
        return 'Inicio.';

        //$oficio_i = '944A';
        $oficio_i = '0051';

        $oficio = '0051';

        $financieros_dispersiones = FinancierosDispersiones::where('nombre_oficio', '=', 'OF' . $oficio_i)->get();//return count($financieros_dispersiones);

        $lista_dispersion = ListaDispersion::where('nombre_listado', 'like', '%' . $oficio . '%')

        ->join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->whereNull('bienestardispersiones.deleted_at')

        ->join('personas_programas', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')
        
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')

        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->get([
            'bienestardispersiones.id as id',
            'bienestarpersonas.foliounico as folio'
        ])

        ;//return count($lista_dispersion);
        
        foreach($financieros_dispersiones as $result_f) {

            foreach($lista_dispersion as $result_l) {

                if($result_f->folio === $result_l->folio) {
                    $pagado = 1;
                    $observacion = 'NINGUNA';

                    if($result_f->rechazado === '1') {
                        $pagado = 2;
                        $observacion = $result_f->observacion;
                    }

                    $persona = BienestarDispersion::where('id', '=', $result_l->id);

                    $persona->update(['pagado' => $pagado, 'observacion' => $observacion]);

                    break;
                }

            }

        }

        return 'Final.';
    }
    
    public function store(Request $request) {
        if($request->ajax()){
            $integrantes = (object)($request->input('detalles'));
            //dd($integrantes);
            try {
                DB::beginTransaction();
                $lista                  =ListaDispersion::findOrFail($integrantes->lista_id);
                //$detalles['importe']    =$lista->importe;
                $detalles['usuario_id'] =$request->User()->id;
                $detalles['lista_id']   =$lista->id;
                $detalles['pagado']     =0;
                $detalles['observacion']='NINGUNA';

                if($integrantes->miembro_id=='all'){
                    $aniosPrograma = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
                    ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
                    ->where('programas.nombre','like','BIENESTAR')
                    ->where('cat_ejercicios.anio',$request->input('anio',$this->anio))
                    ->first(['anios_programas.id'])->id;

                    $bimestre   = $lista->bimestre;
                    $anio       = $lista->ejercicio->anio;

                    //QUERY QUE SELECCIONA
                    $results =
                    PersonasPrograma::Search($this->programa, $this->anio) 
                    ->wherenotnull('cuentasbancos.num_cuenta')
                    ->where('cuentasbancos.num_cuenta','!=','')
                    ->generarDispersion($anio,$bimestre,$aniosPrograma,$lista->id)
                    ->get(['personas_programas.id']);//dd(count($results));

                    //DB::beginTransaction();
                    foreach($results as $result){
                        $tutor=PersonasPrograma::find($result->id)->persona->tutor_bienestar(1)->first()->persona_id;
                        $cuenta=Cuentasbancos::where([['persona_id',$tutor],['activo',1]])->first();
                        if($cuenta)
                            $cuenta=$cuenta->id;
                        else
                            //dd('Error.');
                            dd('Tutorado: ' . PersonasPrograma::find($result->id)->persona->id . ' Tutor: ' . $tutor);
                        $detalles['cuenta_id']=$cuenta;
                        $detalles['personas_programa_id']=$result->id;
                        BienestarDispersion::firstOrCreate($detalles);
                    }
                }else{
                    //DB::beginTransaction();
                    $tutor=PersonasPrograma::find($integrantes->miembro_id)->persona->tutor_bienestar(1)->first()->persona_id;
                    $cuenta=Cuentasbancos::where([['persona_id',$tutor],['activo',1]])->first()->id;
                    $detalles['cuenta_id']=$cuenta;
                    $detalles['personas_programa_id']=$integrantes->miembro_id;
                    $detalles['lista_id']=$integrantes->lista_id;
                    BienestarDispersion::firstOrCreate($detalles);
                }
                DB::commit();
                return response()->json(array('success' => true));
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
        if($request->ajax()) {
            try {
                DB::beginTransaction();//dd($request->all());

                $dispersion = BienestarDispersion::find($request->id)->update($request->datos);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'listas_dispersiones',
                    'registro' => $dispersion->id . '',
                    'campos' => json_encode($dispersion) . '',
                    'metodo' => request()->method()
                ]);

                DB::commit();
                return response()->json(['success' => true, 'code' => 200, 'id' => $dispersion->id, 'estatus' => 'nuevo'], 200);
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request){
        $detalles=(object)$request->input('detalles');
        try {
            DB::beginTransaction();
            if($detalles->miembro_id!='all'){
                $miembro=BienestarDispersion::where('lista_id',$detalles->lista_id)
                ->where('personas_programa_id',$detalles->miembro_id)
                ->firstOrFail();
                $miembro->forceDelete();
            }else{
                $miembros=BienestarDispersion::where('lista_id',$detalles->lista_id);
                if($miembros->count()>0)
                    $miembros->forceDelete();
            }
            DB::commit();
            return response()->json(array('success' => true));
        }
        catch(Exeption $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
    }
}
