<?php

namespace Modules\Bienestar\Http\Controllers;

use App\Models\Persona;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use View;

class PersonaController extends Controller {
    
    public function __construct() {
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id) {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }

    public function search(Request $request) {
        if($request->has('id')) {
            $persona = Persona::find($request->id);
        }
        
        if($request->has('curp')) {
            $persona = Persona::where('curp', $request->curp)->first();
        }      
        
        if(isset($persona)) {
            if($request->has('tipo')) {
                $view = View::make('bienestar::personas.iframe.' . $request->tipo)->with('persona', $persona);
                $html = $view->render();
                return response()->json(['success' => true, 'code' => 200, 'id' => $persona->id, 'html' => $html], 200);
            }           
        }

        $view = View::make('personas.iframe.create_edit');
        $html = $view->render();
        return response()->json(['success' => false, 'code' => 404, 'message' => 'Error: No se encuentra una persona con esos criterios de búsqueda.', 'html' => $html], 404);
    }
}