<?php

namespace Modules\Bienestar\Http\DataTables;

use Modules\Bienestar\Entities\Refrendo;

use Yajra\DataTables\Services\DataTable;

class RefrendoDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        $dataTable = datatables($query)
        ->addColumn('refrendar', function($model) {
            return '<a href="' . route('bienestar.refrendos.edit', $model->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
            //return '';
        })
        ->rawColumns(['refrendar']);
        
        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = Refrendo::select(
            [
                'bien_refrendos.id as id',
                'cat_ejercicios.anio as anio',
                'cat_estados.nombre as estado'
            ]
        )
        ->join('anios_programas', 'bien_refrendos.anio_programa_id', '=', 'anios_programas.id')
        ->whereNull('anios_programas.deleted_at')
        ->join('cat_ejercicios', 'anios_programas.ejercicio_id', '=', 'cat_ejercicios.id')
        ->whereNull('cat_ejercicios.deleted_at')
        ->join('estados_programas', 'bien_refrendos.estado_programa_id', '=', 'estados_programas.id')
        ->whereNull('estados_programas.deleted_at')
        ->join('cat_estados', 'estados_programas.estado_id', '=', 'cat_estados.id')
        ->whereNull('cat_estados.deleted_at');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns =
        [
            ['title' => 'ID', 'data' => 'id', 'name' => 'bien_refrendos.id'],
            ['title' => 'AÑO', 'data' => 'anio', 'name' => 'cat_ejercicios.anio'],
            ['title' => 'ESTADO', 'data' => 'estado', 'name' => 'cat_estados.nombre']
        ];

        array_push($columns, ['title' => 'REFRENDAR', 'data' => 'refrendar', 'name' => 'refrendar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]);

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> EXCEL',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> REINICIAR',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> RECARGAR',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            //'scrollX' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'refrendo_' . date('YmdHis');
    }
}