<?php

namespace Modules\Bienestar\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class DispersionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'nombre_listado' => [
                'required',                
                Rule::unique('listas_dispersiones')->where(function($query) {
                    return $query
                    ->where('nombre_listado', $this->nombre_listado)
                    ->where('id', '!=', $this->id)
                    ->whereNull('deleted_at');
                })
            ]
        ];
    }

    public function messages() {
        return [
            'nombre_listado.unique' => 'El nombre del oficio ya está en uso.'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
