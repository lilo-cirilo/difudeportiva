@extends('consultasmedicas::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
{{-- <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"> --}}

<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/animate.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ $title }}</h3>
	</div>
	<div class="box-body">
    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
      <input type="text" id="search" name="search" class="form-control">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
      </span>
    </div>
    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'registros']) !!}

	</div>
</div>

<div class="modal fade" id="create_edit">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

{!! $dataTable->scripts() !!}
<script type="text/javascript">
  var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#registros').dataTable());
	table.init();

	function cargarModal(url) {
		$.get(url, function(data) {
			$('.modal-content').html(data.body);
			$('#create_edit').modal("show");
		});
	}

	function procesarRegistro() {
		if($("#form_registro").valid()){
			block();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: $('#form_registro').attr('action'),
				type: $('#form_registro').attr('method'),
				data   : $('#form_registro').serialize(),
				dataType: 'JSON',
				success: function(response) {
					unblock();
					swal({
						title: '¡Guardado!',
						text: 'El registro se ha agregado correctamente',
						type: 'success',
						timer: 2500
					})
					$('#create_edit').modal("hide");
					$('#registros').DataTable().ajax.reload();
				},
				error : function(response){
					unblock();
					swal({
						title: '¡Error!',
						text: 'El registro no se ha guarado',
						type: 'error',
						timer: 2500
					})
				}
			});
		}
	}

	function eliminarRegistro(url) {
		swal({
			title              : '¿Está seguro?',
			text               : '¡Usted va a eliminar este registro!',
			type               : 'warning',
			focusCancel		   : true,
			showCancelButton   : true,
			confirmButtonColor : '#C51414',
			cancelButtonColor  : '#554747',
			confirmButtonText  : 'Si',
			cancelButtonText   : 'No',
			allowEscapeKey     : true,
			allowOutsideClick  : true,
			animation          : false,
			customClass        : 'animated pulse',
		}).then((result)=>{
			if(result.value){
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: url,
					type: 'DELETE',
					success: function(response) {
						unblock();
						swal({
							title             : 'Eliminado!',
							text              : 'El registro se ha eliminado correctamente',
							type              : 'info',
							timer             : 3000,
							animation         : false,
							customClass       : 'animated jackInTheBox',
							showConfirmButton : false,
						})
						$('#registros').DataTable().ajax.reload();
					},
					error : function(response){
						unblock();
						swal({
							title: '¡Error!',
							text: 'El registro no se ha eliminado',
							type: 'error',
							timer: 2500
						})
					}
				});
			}
		});
	}
	</script>
</script>
@endpush
