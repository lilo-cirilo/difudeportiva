<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h4 class="modal-title">{{isset($registro)? 'Modificar' : 'Crear'}} registro</h4>
</div>
<form data-toggle="validator" role="form" id="form_registro" action="{{ $url }}" method="{{isset($registro) ? 'PUT' : 'POST' }}">
  <div class="modal-body">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="nombre">Nombre:</label>
          <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="{{ $registro->nombre or '' }}">
        </div>
      </div>
    </div>
    @isset($campoExtra)
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="{{ $campoExtra }}">{{ $campoExtra }}:</label>
          <input class="form-control" id="{{ $campoExtra }}" name="{{ $campoExtra }}" placeholder="{{ $campoExtra }}" value="{{ $registro->$campoExtra or ''  }}">
        </div>
      </div>
    </div>
    @endisset
  </div>
</form>
<div class="modal-footer">
  <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary" onclick="procesarRegistro()">Guardar</button>
</div>