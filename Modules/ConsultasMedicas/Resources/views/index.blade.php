@extends('consultasmedicas::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="../../plugins/timepicker/bootstrap-timepicker.min.css">
<link href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" type="text/css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
</style>
@endpush

@section('content-subtitle', 'Mi espacio')

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Mis citas de hoy</h3>
	</div>
	<div class="box-body">
    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
      <input type="text" id="search" name="search" class="form-control">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
      </span>
    </div>
    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'citas','style'=>'width:100%;']) !!}
    
	</div>
</div>

<div class="box box-info">
	<div class="box-header with-border">
		<h3 class="box-title">Mi productividad</h3>
	</div>
	<div class="box-body">
    
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-primary"><i class="ion ion-ios-medical-outline"></i></span>
        
        <div class="info-box-content">
          <span class="info-box-text">Pacientes del dia</span>
          <span class="info-box-number">{{ $desempenio['dia'] }}</span>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-medical-outline"></i></span>
        
        <div class="info-box-content">
          <span class="info-box-text">Pacientes de la semana</span>
          <span class="info-box-number">{{ $desempenio['semana'] }}</span>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-blue"><i class="ion ion-ios-medical-outline"></i></span>
        
        <div class="info-box-content">
          <span class="info-box-text">Pacientes del mes</span>
          <span class="info-box-number">{{ $desempenio['mes'] }}</span>
        </div>
      </div>
    </div>
    
    
	</div>
</div>

<div class="modal fade" id="modal-cita" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
    </div>
  </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script src="../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
{!! $dataTable->scripts() !!}
<script>
  var dia='';
  $.fn.modal.Constructor.prototype.enforceFocus = function() {};

  function cargarModal(url) {
    $.get(url,function (data){
      $('.modal-content').html(data.body);
      $("#modal-cita").modal("show");
      $('.timepicker').timepicker({
        showInputs: false
      });
      $('#fecha').datepicker({
        beforeShowDay: filterNonWorkingDays,
        autoclose: true,
        language: 'es',
        startDate: '0d'
      });
      
      // $("#fecha").change(function(event) {
      //   var actual = new Date();
      //   // actual = moment(actual).format('YYYY/MM/DD');
      //   actual = moment(actual,'YYYY MM DD');
      //   fecha = $("#fecha").val();
      //   array_fecha = fecha.split('/');
      //   fecha = array_fecha[2] + '/' + array_fecha[1] +'/' + array_fecha[0];
      //   if($("#fecha").val() ==='' || array_fecha[2].length == 4 && fecha <actual){
      //     $("#fecha").val('');
      //     swal({
      //       title : 'No se puede asignar una fecha pasada',
      //       type : 'error',
      //       toast : true,
      //       timer : 1500,
      //       showConfirmButton : false
      //     })
      //   }
      // });
      
      $('#hora_fin').change(function(event) {
        hora_start = convertTime12to24($("#hora_inicio").val());
        hora_end = convertTime12to24($("#hora_fin").val());
        console.log($("#hora_fin").val() === '' ||hora_start >= hora_end);
        if ($("#hora_fin").val() === '' ||hora_start >= hora_end) {
          $("#hora_fin").val('');
          swal({
            title: '¡No se puede asignar una hora fin anterior a la inicial',
            type: 'error',
            timer: 3000,
            toast : true,
            position : 'top-end',
            showConfirmButton: false
          });
        }
      });
      $("#fecha").inputmask();
      $('#beneficiario').select2({
        language: 'es',
        ajax: {
          url: '{{ route('consultasmedicas.pacientes.index') }}',
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              columns : {
                1 : { name : 'personas.nombre' },
                2 : { name : 'personas.primer_apellido' },
                3 : { name : 'personas.segundo_apellido' }
              },
              search : {
                vealue : params.term
              },
              tipo   : 'select',
              filtrado: 'xDoc'
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data.data, function(item) {
                return {
                  id: item.id,
                  text: item.completo,
                  slug: item.completo,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      });
      $("#form_cita").validate({
        rules : {
          hora_inicio : {
            required : true
          },
          hora_fin : {
            required : true
          },
          beneficiario : {
            required : true
          },
          observaciones : {
            required : false,
            minlength : 3
          }
        }
      });
      $(".myToggle").each(function(){
        $(this).bootstrapToggle();
      })
    })
  }
  
  function guardaCita() {
    if ($("#form_cita").valid()) {
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: $('#form_cita').attr('method'),
        url: $('#form_cita').attr('action'),
        data: obtenDatos(),
        dataType: 'json',
        success: function(data) {
          console.log(data);
          if(data[0].code === 409){
            swal({
              title: '¡No se registro la cita!',
              text: 'Se genero un problema',
              type: 'error'
            });
          }
          else if(data[0].code === 410){
            swal({
              type: 'error',
              title: 'Existe una cita en este rango de horas',
              text: 'seleccione otras',
              timer: 1500
            });
          }
          else if(data[0].code === 411){
            swal({
              type: 'error',
              title: 'La persona ya tiene una cita este dia en el programa ' +data[0].programa ,
              text: 'seleccione otro horario u otra fecha',
            });
          }
          else {
            swal({
              type: 'success',
              title: 'Cita registrada',
              text: '',
              showConfirmButton: false,
              timer: 2000
            });
            $("#beneficiario").select2("val", " ");
            $("#observaciones").val('');
            $("#modal-cita").modal('hide');
            dia='';
            $('#citas').DataTable().ajax.reload();
          }
        },
        error : function(data){
          swal({
            title: '¡No se registro la cita!',
            text: 'Se genero un problema',
            type: 'error'
          });
          console.log(data);
        }
      });
    }
  }

  function eliminaCita(id) {
    swal({
      title: '¿Está seguro de eliminar la cita?',
      text: '¡La cita se eliminará de forma permanente!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, eliminar',
      cancelButtonText: 'No, regresar',
      reverseButtons: true,
      allowEscapeKey: false,
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: "DELETE",
          url: "{{ URL::to('canna/citas') }}" + '/' + id,
          success: function(data) {
            console.log(data);
            $("#beneficiario").select2("val", " ");
            $("#observaciones").val('');
            $("#modal-cita").modal('hide');
            dia='';
            swal({
              title: '¡Eliminada!',
              text: 'La cita ha sido eliminada.',
              type: 'success',
              showConfirmButton: false,
              timer: 3000
            });
            $('#calendar').fullCalendar('refetchEvents');

          },
          error : function(data){
            console.log(data);
          }
        });
      }
    });

  }

  function getInicioFinMes() {
    if($("#calendar").fullCalendar('getCalendar') === undefined){
      return {};
    }
    var calendar = $('#calendar').fullCalendar('getCalendar');
    var view = calendar.view;
    Date.prototype.toString = function() { return this.getFullYear() + "-" + (this.getMonth()+1) + "-" + this.getDate(); }
    dias = {
      inicio : view.start._d,
      fin : view.end._d
    };
    return dias;
  }

  function obtenDatos() {
    cita = {
      fecha : ($('#form_cita').attr('method') == 'PUT') ? $("#fecha").val() : $("#fecha").val().split('/').reverse().join('/'),
      hora_inicio : convertTime12to24($("#hora_inicio").val()),
      hora_fin : convertTime12to24($("#hora_fin").val()),
      persona_id : $("#beneficiario").val(),
      color : $("#color").val(),
      observaciones : $("#observaciones").val().toUpperCase()
    };
    if($('#form_cita').attr('method') == 'PUT'){
      cita["asistencia"] = document.getElementById('asistencia').checked
      //cita["fecha"] = $("#fecha").val();
    }
    return cita;
  }

  function filterNonWorkingDays(date) { // Is it a weekend?
    if ([ 0, 6 ].indexOf(date.getDay()) >= 0)
      return { enabled: false, classes: "weekend"};
  }

  function convertTime12to24(time12h) {
    const [time, modifier] = time12h.split(' ');
    let [hours, minutes] = time.split(':');
    if (hours === '12') {
      hours = '00';
    }
    if (modifier === 'PM') {
      hours = parseInt(hours, 10) + 12;
    }
    return hours + ':' + minutes;
  }
  
  function atenderPaciente (go, send) {
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    $.ajax({
      type: 'put',
      url: send,
      data: {tipo : 'asistencia'},
      dataType: 'json',
      success: function(data) {
        window.location.href = go;
      }
    })
  }

</script>
@endpush
