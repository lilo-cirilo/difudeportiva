<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">{{isset($cita) ? 'Cita' : 'Nueva cita'}}</h4>
  </div>
  <form id="form_cita" data-toggle="validator" role="form"
  action="{{isset($cita) ? route('consultasmedicas.citas.update',$cita->id) : route('consultasmedicas.citas.store')}}"
  method="{{isset($cita) ? 'PUT' : 'POST'}}">
  <div class="modal-body">
    <div class="box-body">
      
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
          <div class="form-group">
            <label>FECHA :</label>
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" @if (isset($mayor) && $mayor == 0)
              disabled
              @endif class="form-control pull-right" name="fecha" id="fecha" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask value="{{ $cita->date or '' }}" readonly>
            </div>
          </div>
        </div>
        
        @isset($cita)
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6 pull-right">
          <div class="form-group pull-right">
            <label for="color">ASISTENCIA</label>
            <input id="asistencia" @if ( isset($cita) && $cita->asistencia == 1)
            checked
            @endif @if (isset($mayor) && $mayor == 0)
            disabled
            @endif name="asistencia" data-toggle="toggle" data-on="Si" data-off="No" data-onstyle="success" data-offstyle="danger" data-size="small" data-width="84" type="checkbox" class="myToggle">
          </div>
        </div>
        @endisset
      </div>
      
      
      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6">
          <div class="bootstrap-timepicker">
            <div class="form-group">
              <label>INICIO</label>
              <div class="input-group">
                <input type="text" @if (isset($mayor) && $mayor == 0)
                disabled
                @endif class="form-control timepicker" id="hora_inicio" name="hora_inicio" value="{{$cita->start or ''}}">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-6">
          <div class="bootstrap-timepicker">
            <div class="form-group">
              <label>FIN</label>
              <div class="input-group">
                <input type="text" @if (isset($mayor) && $mayor == 0)
                disabled
                @endif class="form-control timepicker" id="hora_fin" name="hora_fin" value="{{$cita->end or ''}}">
                <div class="input-group-addon">
                  <i class="fa fa-clock-o"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-xs-12 col-sm-9 col-md-12 col-lg-12">
          <div class="form-group">
            <label for="beneficiario">BENEFICIARIO</label>
            <select id="beneficiario" @if (isset($mayor) && $mayor == 0)
            disabled
            @endif name="beneficiario" class="form-control select2" style="width: 100%;">
            @isset($cita)
            <option value="{{ $cita->persona_id }}" selected>{{$cita->title}}</option>
            @endisset
          </select>
        </div>
      </div>
    </div>
    
    <div class="row">
      <div class="col-xs-12 col-sm-9 col-md-12 col-lg-12">
        <div class="form-group">
          <label>OBSERVACIONES</label>
          <textarea class="form-control" @if (isset($mayor) && $mayor == 0)
          disabled
          @endif id="observaciones" name="observaciones" rows="3" placeholder="OBSERVACIONES" >{{$cita->observaciones or ''}}</textarea>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
  @isset($cita)
  <button type="button" @if (isset($mayor) && $mayor == 0)
  style="display: none;"
  @endif class="btn btn-danger" onclick="eliminaCita('{{$cita->id}}')">Eliminar</button>
  @endisset
  <button type="button" @if (isset($mayor) && $mayor == 0)
  style="display: none;"
  @endif class="btn btn-primary" onclick="guardaCita()">Guardar</button>
</div>
</form>
