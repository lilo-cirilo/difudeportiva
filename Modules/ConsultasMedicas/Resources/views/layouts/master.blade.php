@extends('vendor.admin-lte.layouts.main')

@section('logo-mini')
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini">
    <b>
      <img id="img-logo" src="@yield('mini-logo', asset('images/serv med/icono.png'))" style="padding-left: 10px;" alt="DIF">
    </b>
  </span>
@endsection
@section('logo-lg')
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg">
    <b>
      <img id="img-logo" src="@yield('logo', asset('images/serv med/intranet.png'))" alt="DIF">
    </b>
  </span>
@endsection

@if(auth()->check())
  @section('user-avatar')
    @php
        $userimg = auth()->user()->persona->get_url_fotografia();
        $userimg = $userimg != 'images/no-image.png'? $userimg : 'images/serv med/usuario.png';
    @endphp
    {{ asset($userimg) }}
  @endsection
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@include('consultasmedicas::layouts.fragmentos.navbar')

@section('content-title', 'Area blanca sistemas DIF Oaxaca')

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href=" "><i class="fa fa-dashboard"></i>Area Blanca</a></li>
        @yield('li-breadcrumbs')
    </ol>
@endsection

@push('head')
  <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('dist/css/amarillo.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
@endpush

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
  <script type="text/javascript">
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
    $('#sidebar').find('a').each(function (){
      //auto seleccionar elemento activos en la barra lateral
      if($(this).attr('href')==window.location){
        $(this).parent().addClass('active');//primer nivel
        $(this).parent().parent().parent('li').addClass('active');//segundo nivel
        $(this).parent().parent().parent().parent().parent('li').addClass('active');//tercer nivel
      }
    })
    function block() {
        $.blockUI({
            css: {
                border: 'none',
                padding: '0px',
                backgroundColor: 'none',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#fff'
            },
            baseZ: 10000,
            message: '<div align="center"><img style="width:128px; height:164px;" src="{{ asset('images/consultasmedicas/loading.gif') }}"><br /><p style=" color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">Procesando...</p></div>',
        });

        function unblock_error() {
            if($.unblockUI())
                alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
        }

        setTimeout(unblock_error, 120000);
    }

    function unblock() {
        $.unblockUI();
    }
    
    $(".modal").modal({
    keyboard: false,
    backdrop: "static"
    });
    $(".modal").modal('hide');
  </script>
@endpush
