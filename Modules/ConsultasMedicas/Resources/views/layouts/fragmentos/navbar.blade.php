@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree" id="sidebar">
  <li>
    <a href="{{ route('consultasmedicas.dash') }}">
      <i class="fa fa-stethoscope"></i> <span>Dashboard</span>
    </a>
  </li>
  <li>
    <a href="{{ route('consultasmedicas.home') }}">
      <i class="fa fa-stethoscope"></i> <span>Inicio</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-group"></i> <span>Pacientes</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('consultasmedicas.pacientes.index') }}"><i class="fa fa-hospital-o"></i>Lista de pacientes</a></li>
      <li><a href="{{ route('consultasmedicas.pacientes.create') }}"><i class="fa fa-plus-square"></i>Nuevo paciente</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o "></i> <span>Peticiones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="{{ route('consultasmedicas.peticiones.index') }}"><i class="fa fa-list-alt"></i>Lista de peticiones</a></li>
      <li><a href="{{ route('consultasmedicas.programas.index') }}"><i class="fa fa-cubes"></i>Programas</a></li>
    </ul>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa  fa-list"></i> <span>Catálogos</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li class="treeview">
        <a href="#">
          <i class="fa fa-list-ul"></i> <span>Generales</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('consultasmedicas.diagnosticos.index') }}"><i></i> Diagnósticos</a></li>
          <li><a href="{{ route('consultasmedicas.clasificacion.index') }}"><i></i> Programas de clasificación</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-list-ul"></i> <span>Odontologicos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ route('consultasmedicas.odontologia.servicios.index') }}"><i></i> Servicios</a></li>
          <li><a href="{{ route('consultasmedicas.odontologia.piezasdentales.index') }}"><i></i>Piezas dentales</a></li>
        </ul>
      </li>
    </ul>
  </li>
  <li>
    <a href="{{ route('consultasmedicas.citas.index') }}">
      <i class="fa fa-calendar"></i><span>Agenda</span>
    </a>
  </li>
</ul>
@endsection