@extends('consultasmedicas::layouts.master')

@section('content-subtitle', 'Paciente')

@push('head')
  {{-- <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"> --}}
  <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

  <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Paciente</h3>
	</div>
	<div class="box-body">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>
                <i class="fa fa-user margin-r-5"></i>PACIENTE
              </th>
              <td>{{$paciente->persona->obtenerNombreCompleto()}}</td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-barcode margin-r-5"></i>CURP
              </th>
              <td>{{$paciente->persona->curp}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="table-responsive">
        <table class="table">
          <tbody>
            <tr>
              <th>
                <i class="fa fa-hospital-o margin-r-5"></i>DERECHOHABIENCIA
              </th>
              <td>{{$paciente->seguro->nombre or '-'}}</td>
            </tr>
            <tr>
              <th>
                <i class="fa fa-barcode margin-r-5"></i>FOLIO
              </th>
              <td>{{$paciente->folioseguro or '-'}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="box box-info">
  <div class="box-header with-border">
    <h3 class="box-title">Evolucion</h3>
  </div>
  <div class="box-body">

    {!! $graf->render() !!}

  </div>
</div>

<div class="box box-success">
  <div class="box-header with-border">
    <h3 class="box-title">Expediente</h3>
  </div>
  <div class="box-body">
    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
      <input type="text" id="search" name="search" class="form-control">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
      </span>
    </div>

    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'expediente', 'name' => 'expediente', 'style' => 'width: 100%']) !!}

    <div>
      <a href="{{ route('consultasmedicas.pacientes.consultas.create',$paciente->id) }}" class="pull-right btn btn-success">Dar consulta</a>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-consulta" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

    </div>
  </div>
</div>

@stop

@push('body')
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
  <script src="/vendor/datatables/buttons.server-side.js"></script>

  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>

  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  {!! $dataTable->scripts() !!}

  <script type="text/javascript">
    var table = (function() {
      var tabla = undefined,
      btn_buscar = $('#btn_buscar'),
      search = $('#search');

      function init() {
        search.keypress(function(e) {
          if(e.which === 13) {
            tabla.DataTable().search(search.val()).draw();
          }
        });

        btn_buscar.on('click', function() {
          tabla.DataTable().search(search.val()).draw();
        });
      };

      function set_table(valor) {
        tabla = $(valor);
      };

      function get_table() {
        return tabla;
      };

      return {
        init: init,
        set_table: set_table,
        get_table: get_table
      };
    })();

    table.set_table($('#expediente').dataTable());
    table.init();
    
    function muestraCita(id) {
      $.get(window.location.href+'/consultas/'+id,function (data){
        $('.modal-content').html(data.body);
        $("#modal-consulta").modal("show");

        $('#discapacidad').select2({
          placeholder : "NO REGISTRADAS",
          //enable : false,
          language : 'es'
        });
        $('input[type="checkbox"]').iCheck({
          checkboxClass: 'icheckbox_square',
          increaseArea: '20%'
        });
        getImc();
      })
    }
    function getImc() {
      var peso  = $('#peso').val();
      var talla = $('#talla').val();
      var elemento = $('#imc');
      var imc = (parseFloat(peso) / Math.pow(parseFloat(talla),2)).toFixed(2);
      elemento.val(imc);
      if(imc<16){
        elemento.css('color','red');
        elemento.attr('title','Delgadez severa').tooltip('fixTitle').tooltip('show');
      } else if (imc < 17 && imc >= 16) {
        elemento.css('color','orange');
        elemento.attr('title','Delgadez moderada').tooltip('fixTitle').tooltip('show');
      } else if (imc < 18.5 && imc >= 17) {
        elemento.css('color','yellow');
        elemento.attr('title','Delgadez aceptable').tooltip('fixTitle').tooltip('show');
      } else if (imc < 25 && imc >= 18.5) {
        elemento.css('color','green');
        elemento.attr('title','Normal').tooltip('fixTitle').tooltip('show');
      } else if (imc < 30 && imc >= 25) {
        elemento.css('color','yellow');
        elemento.attr('title','sobrepeso').tooltip('fixTitle').tooltip('show');
      } else if (imc < 35 && imc >= 30) {
        elemento.css('color','orange');
        elemento.attr('title','Obeso tipo I').tooltip('fixTitle').tooltip('show');
      } else if (imc < 40 && imc >= 35 ) {
        elemento.css('color','red');
        elemento.attr('title','Obeso tipo II').tooltip('fixTitle').tooltip('show');
      } else if (imc >= 40) {
        elemento.css('color','red');
        elemento.attr('title','Obeso tipo III').tooltip('fixTitle').tooltip('show');
      }
    }
  </script>
@endpush
{{--  --}}
