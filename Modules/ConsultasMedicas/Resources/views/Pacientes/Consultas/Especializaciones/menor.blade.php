<hr>
<div class="row">
  
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="form-group">
      <label for="pesoParaTalla" class="center-block">Peso para la talla</label>
      <input class="texto-plano" type="text" id="pesoParaTalla" name="pesoParaTalla">
    </div>
  </div>
  
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="form-group" style="text-align: center;">
      <label for="ninio_sano" class="center-block">Niño sano</label>
      <input type="checkbox" id="ninio_sano" name="ninio_sano">
    </div>
  </div>
  
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="form-group" style="text-align: center;">
      <label for="cancer" class="center-block">Expedición cedula cáncer</label>
      <input type="checkbox" id="cancer" name="cancer">
    </div>
  </div>
</div>
<h4>Evaluación de desarrollo infantil</h4>
<div class="row">
  <div class="col-xs-4">
    <div class="form-group">
      <label for="tipo_edi">Tipo:</label>
      <input id="tipo_edi" name="tipo_edi" type="text" class="form-control">
    </div>
  </div>
  <div class="col-xs-4">
    <div class="form-group">
      <label for="res_edi">Resultado:</label>
      <input id="res_edi" name="res_edi" type="text" class="form-control">
    </div>
  </div>
  <div class="col-xs-4">
    <div class="form-group">
      <label for="res_battelle">Resultado de Battelle:</label>
      <input id="res_battelle" name="res_battelle" type="text" class="form-control">
    </div>
  </div>

</div>