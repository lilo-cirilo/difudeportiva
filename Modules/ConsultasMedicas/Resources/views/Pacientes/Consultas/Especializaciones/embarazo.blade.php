<div class="collapse" id="extraembarazo">
  <div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group">
        <label for="trimestre_gestacional" class="center-block">Trimestre gestacional:</label>
        <input type="text" class="form-control" id="trimestre_gestacional" name="trimestre_gestacional">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group" style="text-align: center;">
        <label for="primer_embarazo" class="center-block">Primer embarazo:</label>
        <input type="checkbox" id="primer_embarazo" name="primer_embarazo">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group" style="text-align: center;">
        <label for="acido_folico" class="center-block">Prescripción de ácido fólico</label>
        <input type="checkbox" id="acido_folico" name="acido_folico">
      </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group" style="text-align: center;">
        <label for="riesgo" class="center-block">Embarazo de alto riesgo</label>
        <input type="checkbox" id="riesgo" name="riesgo">
      </div>
    </div>
  </div>
  <div class="collapse" id="riesgoembarazo">
    <div id="alto_riesgo" class="row">

      <div class="col-xs-12 col-md-6 col-lg-3">
        <div class="form-group">
          <label for="riesgo_de">Riesgo de:</label>
          <select id="riesgo_de" name="riesgo_de" class="form-control select2" style="width: 100%;">
            <option value=""></option>
            <option value="Preeclampsia">Preeclampsia</option>
            <option value="Eclampsia">Eclampsia</option>
          </select>
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
        <div class="form-group" style="text-align: center;">
          <label for="infeccion" class="center-block">Infección urinaria:</label>
          <input type="checkbox" id="infeccion" name="infeccion">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
        <div class="form-group" style="text-align: center;">
          <label for="hemorragia" class="center-block">Riesgo de hemorragia:</label>
          <input type="checkbox" id="hemorragia" name="hemorragia">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
        <div class="form-group" style="text-align: center;">
          <label for="apoyo" class="center-block">Apoyo al traslado:</label>
          <input type="checkbox" id="apoyo" name="apoyo">
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="analisis" class="center-block">Resultado de análisis clínicos:</label>
          <input type="text" class="form-control" id="analisis" name="analisis">
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group">
        <label for="terapia" class="center-block">Terapia hormonal:</label>
        <input type="text" class="form-control" id="terapia" name="terapia">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group">
        <label for="descanso" class="center-block">Descanso por puerperio:</label>
        <input type="text" class="form-control" id="descanso" name="descanso">
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
      <div class="form-group" style="text-align: center;">
        <label for="anticonceptivo" class="center-block">Anticonceptivo postparto</label>
        <input type="checkbox" id="anticonceptivo" name="anticonceptivo">
      </div>
    </div>
  </div>

</div>