@extends('consultasmedicas::layouts.master')

@push('head')
  <style type="text/css">
    select[readonly] {
      background: #eee;
      cursor: no-drop;
    }
    select[readonly] option {
      display: none;
    }
    textarea{
      resize: none;
    }
    .modal-body{
      overflow-y: auto;
      max-height: 400px;
    }
    .border-0{
      padding-bottom: 0px !important;
      box-shadow   : 0px 0px !important;
    }
  </style>

  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/iCheck/all.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
  {{-- poner esto antes de ui xD --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css') }}">
  {{-- poner ui despues de bootstrap-datepicker --}}
  <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/jquery-ui/themes/cupertino/jquery-ui.css') }}">
@endpush

@section('content-subtitle', 'Nuevo paciente')

@section('li-breadcrumbs')
  <li><a href="{{route('consultasmedicas.pacientes.index')}}">pacientes</a></li>
  <li class="active">Nuevo paciente</li>
@endsection

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">{{ isset($persona) ? 'EDITAR' : 'AGREGAR' }} PACIENTE</h3>
    </div>
    
    <form data-toggle="validator" role="form" id="form_paciente" action="{{ isset($persona) ? route('consultasmedicas.pacientes.update',$persona->id) : route('consultasmedicas.pacientes.store') }}" method="{{ isset($persona) ? 'PUT' : 'POST' }}">
      <div class="box-body">
        
        <div class="row">
          
          <div class="col-xs-12  col-md-6 col-lg-4">
            <div class="form-group">
              <label for="curp">*CURP:</label>
              <input data-inputmask='"mask": "aaaa999999aaaaaa*9"' data-mask type="text" class="form-control mask" id="curp" name="curp" value="{{ $persona->curp or '' }}" placeholder="CLAVE UNICA DE REGISTRO DE POBLACION">
            </div>
          </div>
          
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="form-group">
              <label for="nombre">*Nombre:</label>
              <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $persona->nombre or '' }}" placeholder="INGRESE NOMBRE(S)">
            </div>
          </div>
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="form-group">
              <label for="primer_apellido">*Apellido Paterno:</label>
              <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $persona->primer_apellido or '' }}" placeholder="INGRECE EL PRIMER APELLIDO">
            </div>
          </div>
          
        </div>
        <div class="row">
          <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="form-group">
              <label for="segundo_apellido">*Apellido Materno:</label>
              <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $persona->segundo_apellido or '' }}" placeholder="INGRECE EL SEGUNDO APELLIDO">
            </div>
          </div>
          
          <div class="col-xs-6 col-md-4 col-lg-2">
            <div class="form-group">
              <label for="genero">*Género:</label>
              <select id="genero" name="genero" class="form-control select2">
                <option value=""></option>
                <option value="M" {{ (isset($persona) && $persona->genero == 'M') ? 'selected' : '' }}>MASCULINO</option>
                <option value="F" {{ (isset($persona) && $persona->genero == 'F') ? 'selected' : '' }}>FEMENINO</option>
              </select>
            </div>
          </div>
          
          <div class="col-xs-6 col-md-4 col-lg-3">
            <div class="form-group">
              <label for="fecha_nacimiento">*Fecha de Nacimiento:</label>
              <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ isset($persona) ? $persona->get_formato_fecha_nacimiento() : '' }}" placeholder="DIA/MES/AÑO">
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="clave_electoral">Clave Electoral:</label>
              <input type="text" class="form-control" id="clave_electoral" name="clave_electoral" value="{{ $persona->clave_electoral or '' }}" placeholder="13 CARACTERES EN EL INE/IFE">
            </div>
          </div>
        </div>
        <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="rfc">RFC:</label>
              <input data-inputmask='"mask": "aaaa999999***", "clearIncomplete" :  true ' data-mask type="text" class="form-control mask" id="rfc" name="rfc" value="{{ $persona->clave_electoral or '' }}" placeholder="REGISTRO FEDERAL DE CONTRIBUYENTE">
            </div>
          </div>
          
          <div class="col-xs-6 col-md-4 col-lg-3">
            <div class="form-group">
              <label for="derechohabiencia">Derechohabiencia:</label>
              <select id="derechohabiencia" name="derechohabiencia" class="form-control select2">
                <option value=""></option>
                @foreach ($derechohabiecias as $derechohabiecia)
                <option value="{{ $derechohabiecia->id }}" {{ (isset($paciente) && $paciente->derechohabiencia == $derechohabiecia->id) ? 'selected' : '' }}>{{ $derechohabiecia->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="folio_derechohabiencia">Folio:</label>
              <input type="text" class="form-control" id="folio_derechohabiencia" name="folio_derechohabiencia" value="{{ $persona->clave_electoral or '' }}" placeholder="FOLIO DE DERECHOHABIENCIA">
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="etnia" class="center-block">¿Pertenece a una etnia?</label>
              <select id="etnia" name="etnia" class="form-control select2">
                <option value=""></option>
                @foreach ($etnias as $etnia)
                <option value="{{ $etnia->id }}" {{ (isset($paciente) && $paciente->etnia == $etnia->id) ? 'selected' : '' }}>{{ $etnia->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        
        <h4>Dirección:</h4>
        <hr>
        <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
              <label for="pais">*País:</label>
              <select id="pais" name="pais" class="form-control select2">
                @foreach ($paises as $pais)
                <option value="{{ $pais->id }}" {{ ($pais->nombre == 'MÉXICO'||isset($paciente) && $paciente->pais_id == $pais->id) ? 'selected' : '' }}>{{ $pais->nombre }}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
              <label for="estado">*Estado:</label>
              <select id="estado" name="estado" class="form-control select2" style="width: 100%;">
                <option value=""></option>
                @if(isset($paciente) && isset($paciente->estado))
                <option value="{{ $paciente->estado }}" selected>{{ $paciente->estado }}</option>
                @endif
                @foreach ($entidades as $entidad)
                <option value="{{ $entidad->id }}" {{ ($entidad->entidad == 'OAXACA'||isset($paciente) && $paciente->entidad_id == $entidad->id) ? 'selected' : '' }}>{{ $entidad->entidad }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div id="direccion" class="collapse">
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
              <div class="form-group">
                <label for="calle">*Calle:</label>
                <input type="text" class="form-control" id="calle" name="calle" value="{{ $persona->calle or '' }}" placeholder="NOMBRE DE LA CALLE">
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
              <div class="form-group">
                <label for="numero_exterior">*Número Exterior:</label>
                <input type="text" class="form-control" id="numero_exterior" name="numero_exterior" value="{{ $persona->numero_exterior or '' }}" placeholder="NUMERO EXTERIOR">
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
              <div class="form-group">
                <label for="numero_interior">Número Interior:</label>
                <input type="text" class="form-control" id="numero_interior" name="numero_interior" value="{{ $persona->numero_interior or '' }}" placeholder="NUMERO INTERIOR">
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
              <div class="form-group">
                <label for="colonia">*Colonia:</label>
                <input type="text" class="form-control" id="colonia" name="colonia" value="{{ $persona->colonia or '' }}" placeholder="NOMBRE DE LA COLONIA">
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
              <div class="form-group">
                <label for="codigopostal">Código Postal:</label>
                <input type="text" class="form-control" id="codigopostal" name="codigopostal" value="{{ $persona->codigopostal or '' }}" placeholder="CODIGO POSTAL">
              </div>
            </div>
          </div>
          
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
              <div class="form-group">
                <label for="municipio_id">*Municipio:</label>
                <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
                  @if(isset($persona) && isset($persona->municipio))
                  <option value="{{ $persona->municipio->id }}" selected>({{ $persona->municipio->distrito->region->nombre }}) ({{ $persona->municipio->distrito->nombre }}) {{ $persona->municipio->nombre }}</option>
                  @endif
                </select>
              </div>
            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
              <div class="form-group">
                <label for="localidad_id">Localidad:</label>
                <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
                  @if(isset($persona) && isset($persona->localidad))
                  <option value="{{ $persona->localidad->id }}" selected>{{ $persona->localidad->nombre }}</option>
                  @endif
                </select>
              </div>
            </div>
          </div>
          
          <div class="row">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="referencia_domicilio">*Referencias del Domicilio:</label>
                <textarea class="form-control" rows="3" id="referencia_domicilio" name="referencia_domicilio" placeholder="ESPECIFIQUE REFERENCIAS">{{ $persona->referencia_domicilio or '' }}</textarea>
              </div>
            </div>
          </div>
        </div>
        
        <h4>Contacto:</h4>
        <hr>
        <div class="row">
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="numero_celular">Celular:</label>
              <input type="text" class="form-control mask" id="numero_celular" name="numero_celular" data-inputmask='"mask": "(999) 999-999-9999"' data-mask value="{{ $persona->numero_celular or '' }}" placeholder="CELULAR PERSONAL">
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
              <label for="numero_local">Teléfono:</label>
              <input type="text" class="form-control mask" id="numero_local" name="numero_local" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{ $persona->numero_local or '' }}" placeholder="NUMERO DE CASA">
            </div>
          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
              <label for="email">Correo Electrónico:</label>
              <input type="text" class="form-control" id="email" name="email" value="{{ $persona->email or '' }}" placeholder="CORREO@EJEMPLO.COM">
            </div>
          </div>
        </div>
      </div>
      
  </div>
  <div class="box-footer">
    <div class="pull-right">
      <button type="button" class="btn btn-success" onclick="procesarpaciente()"><i class="fa fa-database"></i> Guardar</button>
      <a href="{{ route('consultasmedicas.pacientes.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
    </div>
  </div>
    </form>
@stop

@push('body')
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/jquery-ui/jquery-ui.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
  
  
  @include('personas.js.persona');
  
  <script type="text/javascript">
    app.agregar_bloqueo_pagina();
    
    $(document).ready(function() {
      $('.select2').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumResultsForSearch: Infinity
      });
      $('#derechohabiencia').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
      });
      $('#etnia').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'NO PERTENECE',
      });
      $('#m_derechohabiencia').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
      });
      $('#m_etnia').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'NO PERTENECE',
      });
      $('#direccion').collapse();
      $('#fecha_nacimiento').datepicker({
        autoclose: true,
        language: 'es',
        startDate: '01-01-1900',
        endDate: '0d',
        orientation: 'bottom'
      });
      $('#municipio_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('municipios.select') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#municipio_id').valid();
        $('#localidad_id').empty();
        $('#localidad_id').select2({
          language: 'es',
          placeholder : 'SELECCIONE ...',
          ajax: {
            url: '{{ route('localidades.select') }}',
            delay: 500,
            dataType: 'JSON',
            type: 'GET',
            data: function(params) {
              return {
                search: params.term,
                municipio_id: $('#municipio_id').val()
              };
            },
            processResults: function(data, params) {
              params.page = params.page || 1;
              return {
                results: $.map(data, function(item) {
                  return {
                    id: item.id,
                    text: item.nombre,
                    slug: item.nombre,
                    results: item
                  }
                })
              };
            },
            cache: true
          }
        }).change(function(event) {
          $('#localidad_id').valid();
        });
      });
      $('.mask').inputmask();
      $('#rfc').on('click',function () {
        if ($('#curp').val().length >= 10 && $('#rfc').val().length != 13) {
          $('#rfc').val($('#curp').val().substring(0,10));
        }
      });
      $('#pais').change(function(){
        if($('#pais').val()!=165){
          $('#estado option:selected').removeAttr("selected");
          $('#estado').select2({
            placeholder : 'No disponible',
            disabled : true,
            en : 'es'
          });
          ocultarDireccion(true);
          $('#estado').val('99');
        }else {
          //$('#estado').empty();
          $('#estado').select2({
            disabled : false,
            placeholder : 'seleccione ...'
          });
          ocultarDireccion(false);
          $('#estado').val('');
        }
      });
      $('#estado').change(function(){
        if($('#estado').val()!=20){
          ocultarDireccion(true);
        }else{
          ocultarDireccion(false);
        }
      });
      $('a[data-toggle="tab"]').on('click', function(){
        if ($(this).parent('li').hasClass('disabled')) {
          return false;
        }
      });
      $(':input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_square',
        increaseArea: '20%'
      });
      
      buscarPersona();
      //$('#modal-aux').modal('show');
    });
    
    function ocultarDireccion(bandera) {
      if(bandera){
        $('#calle').val('CALLE FUERA DEL ESTADO');
        $('#numero_exterior').val('S/N');
        $('#numero_interior').val('S/N');
        $('#colonia').val('COLONIA FUERA DEL ESTADO');
        $('#codigopostal').val('00000');
        $('#municipio_id').val('999');
        $('#localidad_id').val('');
        $('#referencia_domicilio').val('FUERA DEL ESTADO');
        $('#direccion').collapse('hide');
      }else{
        $('#calle').val('');
        $('#numero_exterior').val('');
        $('#numero_interior').val('');
        $('#colonia').val('');
        $('#codigopostal').val('');
        $('#municipio_id').val('');
        $('#localidad_id').val('');
        $('#referencia_domicilio').val('');
        $('#direccion').collapse('show');
      }
    }
    
    function procesarpaciente(paciente) {
      var form = paciente ? $('#form_consolidacion') : $('#form_paciente');
      var data = paciente ? {
        persona_id  : paciente,
        seguro_id   : $('#m_derechohabiencia').val(),
        folioseguro : $('#m_folio_derechohabiencia').val(),
        indigena    : $('#m_etnia').val(),
        estado_id   : 20,
        pais_id     : 165,
        rfc         : $('#m_rfc').val()
      } : {
        persona : {
          nombre               : $('#nombre').val(),
          primer_apellido      : $('#primer_apellido').val(),
          segundo_apellido     : $('#segundo_apellido').val(),
          curp                 : $('#curp').val(),
          genero               : $('#genero').val(),
          fecha_nacimiento     : $('#fecha_nacimiento').val().split('/').reverse().join('-'),
          clave_electoral      : $('#clave_electoral').val(),
          calle                : $('#calle').val(),
          numero_exterior      : $('#numero_exterior').val(),
          numero_interior      : $('#numero_interior').val(),
          colonia              : $('#colonia').val(),
          codigopostal         : $('#codigopostal').val(),
          municipio_id         : $('#municipio_id').val() ? $('#municipio_id').val() : 999,
          localidad_id         : $('#localidad_id').val(),
          referencia_domicilio : $('#referencia_domicilio').val(),
          numero_celular       : $('#numero_celular').val(),
          numero_local         : $('#numero_local').val(),
          email                : $('#email').val()
        },
        seguro_id   : $('#derechohabiencia').val(),
        folioseguro : $('#folio_derechohabiencia').val(),
        indigena    : $('#etnia').val(),
        estado_id   : $('#estado').val() ? $('#estado').val() : 99,
        pais_id     : $('#pais').val(),
        rfc         : $('#rfc').val()
      };
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      
      if(form.valid()){
        $.ajax({
          url          : form.attr('action'),
          type         : form.attr('method'),
          data         : data,
          dataType     : 'JSON',
          success      : function(response) {
            app.set_bloqueo(false);
            unblock();
            swal({
              title: '¡Guardado!',
              text : 'El paciente se ha agregado correctamente',
              type : 'success',
              confirmButtonText : 'CONTINUAR'
            }).then((x)=>{
              window.location.href = "{{ route('consultasmedicas.pacientes.index') }}/"+response.id+"/consultas/create";
            });
          },
          error        : function(response){
            unblock();
            swal({
              title             : '¡Error!',
              text              : 'El paciente no se ha guardado',
              type              : 'error',
              timer             : 2500,
              showConfirmButton : false
            })
          }
        });
      }
    }
    function buscarPersona() {
      var cache=[];
      $('#curp').autocomplete({
        source: function(request, response) {
          var term = request.term;
          if (term in cache) {
            response(cache[ term ]);
            return;
          }
          $.ajax({
            url: "{{ route('personas.index') }}",
            dataType: "json",
            data: {
              columns : {
                0 : {
                  name : 'personas.curp',
                  search : {
                    value : term.replace('_','')
                  }
                }
              },
              tipo : 'select'
            },
            success: function (data) {
              cache[ term ] = data.data;
              response(data.data);
            }
          });
        },
        minLength: 3,
        select: function(event, ui) {
          encontrarPersona(ui.item.edit);
        }
      });
    }
    function encontrarPersona(url) {
      $.get(url+'?tipo=onlydata',function (data) {
        Object.keys(data.persona).forEach(element => {
          $('#'+element).val(data.persona[element]);
        });
        $('#municipio_id').html('<option value="'+data.persona.municipio_id+'" selected>'+data.mun+'</option>');
        $('#municipio_id').trigger('change');
        $('#genero').trigger('change');
      })
    }
    $('#form_paciente').validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          pattern_nombre: ''
        },
        primer_apellido: {
          required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        segundo_apellido: {
          required: true,
          minlength: 1,
          pattern_apellido: ''
        },
        fecha_nacimiento: {
          required: true
        },
        curp: {
          required: true,
          minlength: 18,
          maxlength: 19,
          pattern_curp: ''
        },
        genero: {
          required: true
        },
        clave_electoral: {
          required: false,
          minlength: 13,
          pattern_numero: ''
        },
        calle: {
          required: true,
          minlength: 3
        },
        numero_exterior: {
          required: true,
          minlength: 1,
          pattern_numero: ''
        },
        numero_interior: {
          required: false,
          minlength: 1,
          pattern_numero: ''
        },
        colonia: {
          required: true,
          minlength: 3
        },
        codigopostal: {
          required: false,
          minlength: 5,
          maxlength: 5,
          pattern_integer: ''
        },
        municipio_id: {
          required: true
        },
        localidad_id: {
          required: false
        },
        referencia_domicilio: {
          required: true,
          minlength: 3
        },
        numero_celular: {
          required: false,
          pattern_telefono: ''
        },
        numero_local: {
          required: false,
          pattern_telefono: ''
        },
        email: {
          required: false,
          email: true
        }
      },
      messages: {
      }
    });
    
    $('#form_curp').validate({
      rules: {
        search_curp: {
          required: true,
          minlength: 18,
          maxlength: 19,
          pattern_curp: ''
        }
      },
      messages: {
      }
    });
    
    $.extend($.validator.messages, {
      required: 'Este campo es obligatorio.',
      remote: 'Por favor, rellena este campo.',
      email: 'Por favor, escribe una dirección de correo válida.',
      url: 'Por favor, escribe una URL válida.',
      date: 'Por favor, escribe una fecha válida.',
      dateISO: 'Por favor, escribe una fecha (ISO) válida.',
      number: 'Por favor, escribe un número válido.',
      digits: 'Por favor, escribe sólo dígitos.',
      creditcard: 'Por favor, escribe un número de tarjeta válido.',
      equalTo: 'Por favor, escribe el mismo valor de nuevo.',
      extension: 'Por favor, escribe un valor con una extensión aceptada.',
      maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
      minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
      rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
      range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
      max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
      min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
      nifES: 'Por favor, escribe un NIF válido.',
      nieES: 'Por favor, escribe un NIE válido.',
      cifES: 'Por favor, escribe un CIF válido.'
    });
    
    $.validator.setDefaults({
      highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
      },
      errorPlacement: function(error, element) {
        $(element).parents('.form-group').append(error);
      }
    });
    
  </script>
@endpush
  