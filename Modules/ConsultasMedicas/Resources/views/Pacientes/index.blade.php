@extends('consultasmedicas::layouts.master')

@section('content-subtitle', 'Tabla de Pacientes')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
{{-- <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet"> --}}
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">Tabla de Pacientes:</h3>
		{{-- <a href="{{ route('braille.Pacientes.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Agregar</a> --}}
	</div>
	<div class="box-body">
		<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
			<input type="text" id="search" name="search" class="form-control">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
			</span>
		</div>

		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'Pacientes']) !!}

	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#Pacientes').dataTable());
	table.init();

	function eliminarRegistro(url,tipoRegistro) {
		swal({
			title              : '¿Está seguro?',
			text               : '¡Usted va a eliminar un '+tipoRegistro+'!',
			type               : 'warning',
			focusCancel		   : true,
			showCancelButton   : true,
			confirmButtonColor : '#C51414',
			cancelButtonColor  : '#554747',
			confirmButtonText  : 'Si',
			cancelButtonText   : 'No',
			allowEscapeKey     : true,
			allowOutsideClick  : true,
			animation          : false,
			customClass        : 'animated pulse',
		}).then((result)=>{
			if(result.value){
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: url,
					type: 'DELETE',
					success: function(response) {
						unblock();
						swal({
							title             : 'Eliminado!',
							text              : 'El '+tipoRegistro+' se ha eliminado correctamente',
							type              : 'info',
							timer             : 3000,
							animation         : false,
							customClass       : 'animated jackInTheBox',
							showConfirmButton : false,
						})
						$('#'+tipoRegistro+'s').DataTable().ajax.reload();
					},
					error : function(response){
						unblock();
						swal({
							title             : '¡Error!',
							text              : 'El '+tipoRegistro+' no se ha eliminado',
							type              : 'error',
							timer             : 3000,
							animation         : false,
							customClass       : 'animated jackInTheBox',
							showConfirmButton : false,
						})
					}
				});
			}
		});
	}

</script>
@endpush
