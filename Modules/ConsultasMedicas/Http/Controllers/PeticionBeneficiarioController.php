<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\PeticionBeneficiarioBaseController;

class PeticionBeneficiarioController extends PeticionBeneficiarioBaseController{
  public function __construct(){
    parent::__construct('SERVICIOS MEDICOS');
  }
}