<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use App\Http\Controllers\ProgramaBaseController;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ProgramaController extends ProgramaBaseController{
    
  function __construct(){
    parent::__construct('SERVICIOS MEDICOS','consultasmedicas');
  }

}
