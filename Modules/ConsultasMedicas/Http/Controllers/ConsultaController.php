<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\Models\Discapacidad;

use App\Models\ConsultasMedicas\Pacientes;
use App\Models\ConsultasMedicas\ClasificacionEnfermedades;
use App\Models\ConsultasMedicas\ServiciosOdontologicos;
use App\Models\ConsultasMedicas\ConsultaOdontologica;
use App\Models\ConsultasMedicas\DiscapacidadesConsulta;
use App\Models\ConsultasMedicas\ConsultaPsicologica;
use App\Models\ConsultasMedicas\DetalleEmbarazo;
use App\Models\ConsultasMedicas\PiezasDentales;
use App\Models\ConsultasMedicas\Medicamentos;
use App\Models\ConsultasMedicas\DetalleMenor;
use App\Models\ConsultasMedicas\Diagnostico;
use App\Models\ConsultasMedicas\Consulta;
use App\Models\ConsultasMedicas\Receta;
use App\Models\ConsultasMedicas\Cie10;

use View;

class ConsultaController extends Controller{

  /**
   * Crea una nueva instancia del controlador
   * Definiendo que el usuario debe estar logueado y autorizado
   * Ademas de los roles que debe tener
   */
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,MEDICO,ODONTOLOGO,PSICOLOGO');

  }
  /**
   * Busca en la base de datos registros CIE10
   * @param  Request  $request contiene los elementos de la solicitud
   * @return Response json con los registros coincidentes
   */
  public function cie10List(Request $request){
    $diag = Cie10::where('nombre','like','%'.$request->search.'%')->take(10)->get()->toArray();
    Return response()->json($diag);
  }

  /**
   * Busca en la base de datos registros cat_medicamentos
   * @param  Request  $request contiene los elementos de la solicitud
   * @return Response json con los registros coincidentes
   */
  public function medicamentosList(Request $request){
    $medi = Medicamentos::where('nombre_generico','like','%'.$request->search.'%')->take(10)->get()->toArray();
    Return response()->json($medi);
  }

  /**
   * Vista para crear un nuevo registro, muestra información de un paciente
   * @param  integer $paciente identificador del paciente, enviado en la ruta
   * @param  Request $request el objeto con los datos de la solicitud
   * @return Response contiene el html del formulario necesario.
   */
  public function create($paciente, Request $request){
    $area='';
    $servicios=null;
    $piezas=null;
    if($request->user()->hasRoles(['MEDICO'])){
      $area = 'general';
    } else if($request->user()->hasRoles(['PSICOLOGO'])){
      $area = 'psicologia';
    } else if($request->user()->hasRoles(['ODONTOLOGO'])){
      $area = 'odontologia';
      $servicios['preventivos'] = ServiciosOdontologicos::where('tipo','PREVENTIVO')->get();
      $servicios['curativos'] = ServiciosOdontologicos::where('tipo','CURATIVO')->get();
      $piezas = PiezasDentales::all();
    }
    return view('consultasmedicas::Pacientes.Consultas.create',[
      'paciente'=>Pacientes::find($paciente),
      'discapacidades' => Discapacidad::all(),
      'clasificaciones'=> ClasificacionEnfermedades::all(),
      'area'=>$area,
      'servicios'=>$servicios,
      'piezas'=>$piezas
    ]);
  }

  /**
   * Almacena un nuevo registro de la vista en base a un paciente
   * @param  integer $paciente identificador del paciente, enviado en la ruta
   * @param  Request $request el objeto con los datos de la solicitud
   * @return Response arreglo con el estado resultante de la solicitud
   */
  public function store($paciente, Request $request){
    try{
      DB::beginTransaction();

      //Se definen los campos necesarios para crear la consulta
      $aux['peso'] = $request->input('peso',0);
      $aux['talla'] = $request->input('talla',0);
      $aux['paciente_id'] = $paciente;
      $aux['empleado_id'] = $request->User()->persona->empleado->id;
      $aux['referido'] = ($request->referido) ? 1 : 0;
      $aux['contrareferido'] = ($request->contrareferido) ? 1 : 0;
      //y se almacena en la base de datos
      $consulta = Consulta::create($aux);
      //se comprueba se si mandaron discapacidades desde la vista
      if($request->has('discapacidad_id')){
        foreach ($request->discapacidad_id as $discapacidad) {
          $aux = array();
          $aux['consulta_id'] = $consulta->id;
          $aux['discapacidad_id'] = $discapacidad;
          DiscapacidadesConsulta::create($aux);
        }
      }
      //Se comprueba la existencia de datos sobre embarazo
      if ($request->has('embarazo')) {
        $aux = array();
        $aux['consulta_id'] = $consulta->id;
        $aux['trimestre_gestacional'] = $request->embarazo['trimestre_gestacional'];
        $aux['primer_embarazo'] = ($request->embarazo['primer_embarazo']) ? 1 : 0;
        $aux['acido_folico'] = ($request->embarazo['acido_folico']) ? 1 : 0;
        $aux['anticonceptivo'] = ($request->embarazo['anticonceptivo']) ? 1 : 0;
        $aux['terapia'] = $request->embarazo['terapia'];
        $aux['descanso'] = $request->embarazo['descanso'];
        if(isset($request->embarazo['riesgo'])){
          $aux['riesgo_de'] = $request->embarazo['riesgo']['riesgo_de'];
          $aux['infeccion_urinaria'] = ($request->embarazo['riesgo']['infeccion']) ? 1 : 0;
          $aux['hemorragia'] = ($request->embarazo['riesgo']['hemorragia']) ? 1 : 0;
          $aux['apoyo_traslado'] = ($request->embarazo['riesgo']['apoyo']) ? 1 : 0;
          $aux['resultado_analisis_clinicos'] = $request->embarazo['riesgo']['analisis'];
        }
        DetalleEmbarazo::create($aux);
      }
      //Se comprueba los diagnosticos agregados y se agregan uno a uno
      if ($request->has('diagnosticos')) {
        foreach ($request->diagnosticos as $diagnostico) {
          $aux = array();
          $aux['diagnostico'] = $diagnostico[0];
          $aux['cie10_id'] = explode("-", $diagnostico[1])[0];
          $aux['primeravezpadecimiento'] = ($diagnostico[2] == 'SI') ? 1 : 0;
          $aux['primeravezanual'] = ($diagnostico[3] == 'SI') ? 1 : 0;
          $aux['clasificacion_id'] = explode('-', $diagnostico[4])[0];
          $aux['consulta_id'] = $consulta->id;
          Diagnostico::create($aux);
        }
      }
      //Se almacena la información psicológica en caso de existir
      if($request->has('psicologia')){
        $aux = array();
        $aux = $request->psicologia;
        $aux['consulta_id'] = $consulta->id;
        ConsultaPsicologica::create($aux);
      }
      //se almacena la información odontológica en caso de existir
      if($request->has('actividades_odontologicas')) {
        foreach ($request->actividades_odontologicas as $act) {
          $aux = array();
          $aux['consulta_id'] = $consulta->id;
          $aux['servicio_id'] = explode('-', $act[1])[0];
          if($act[2] !== '-------'){
            $aux['pieza_dental_id'] = explode('-', $act[2])[0];
          }
          if($act[3] !== '-------'){
            $aux['cantidad'] = $act[3];
          }
          ConsultaOdontologica::create($aux);
        }
      }
      //se comprueban los tratamientos y se agregan uno por uno
      if($request->has('tratamientos')) {
        foreach ($request->tratamientos as $tratamiento) {
          $aux = array();
          $aux['consulta_id'] = $consulta->id;
          $aux['medicamento_id'] = explode('-', $tratamiento[0])[0];
          $aux['dosis'] = explode('-', $tratamiento[1])[0];//$tratamiento[1];
          $aux['intervalo'] = explode('-', $tratamiento[2])[0];//$tratamiento[2];
          $aux['duracion'] = explode('-', $tratamiento[3])[0];//$tratamiento[3];
          Receta::create($aux);
        }
      }
      DB::commit();
      return response()->json(array('success' => true));
    } catch (Exception $e) {
      DB::rollBack();
      return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
    }
  }

  /**
   * Muestra la información de un registro especifico
   * @param  integer     $id     Identificador del registro, enviado en la ruta
   * @return Response            Retorna la vista con los datos requeridos
   */
  public function show($consulta){
    $consulta = Consulta::findorfail($consulta);
    return response()->json([
      'body' => view::make('consultasmedicas::Pacientes.Consultas.show',[
        'consulta'=> $consulta,
        'discapacidades' => Discapacidad::all()
      ])->render()
    ], 200);
  }

}
