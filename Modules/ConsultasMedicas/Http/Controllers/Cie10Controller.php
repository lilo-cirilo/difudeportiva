<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use App\DataTables\ConsultasMedicas\Cie10Tabla;
use App\Models\ConsultasMedicas\Cie10;

use View;

class Cie10Controller extends Controller{

  /**
  * Crea una nueva instancia del controlador Cie10Controller
  * Define que el usuario debe estar logeado y autorizado asi como los roles
  */
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,MEDICO,ODONTOLOGO,PSICOLOGO');

  }
  /**
   * Vista que muestra los registros existentes
   * @param  Cie10Tabla $tabla Servicio de DataTable que contiene los datos a mostrar
   * @return View              Contiene el html y la tabla con los datos
   */
  public function index(Cie10Tabla $tabla){
    return $tabla->render('consultasmedicas::Catalogos.index',[
      'title'=>'Lista de diagnosticos medicos'
    ]);
  }

  /**
   * Vista para crear un nuevo registro
   * @return Response contiene el html del formulario necesario.
   */
  public function create(){
    return response()->json([
      'body' => view::make('consultasmedicas::Catalogos.create_edit',[
        'url'=> route('consultasmedicas.diagnosticos.store')
      ])
      ->render()
    ], 200);
  }

  /**
   * Almacena un nuevo registro de la vista
   * @param  Request $request el objeto con los datos de la solicitud
   * @return Response arreglo con el estado resultante de la solicitud
   */
  public function store(Request $request){
    try {
      DB::beginTransaction();
      $new = Cie10::firstOrCreate(
          $request->all()
      );
      DB::commit();
      return array('success' => true, 'id' => $new->id);
    }catch(Exeption $e) {
      DB::rollBack();
      return array('success' => false);
    }
  }

  /**
   * Muestra la vista para editar un registro especifico
   * @param  integer     $id     identificador del registro a modificar, enviado en la ruta
   * @return Response            contiene el html con el formulario necesario
   */
  public function edit($id){
    return response()->json([
      'body' => view::make('consultasmedicas::Catalogos.create_edit',[
        'registro' => Cie10::find($id),
        'url'=> route('consultasmedicas.diagnosticos.update',$id)
      ])
      ->render()
    ], 200);
  }

  /**
   * Actualiza el registro especificado
   * @param  integer $id identificador del registro, enviando en la ruta
   * @param  Request $request contiene los datos para actualizar el registro
   * @return Response contiene un arreglo con el estado de la solicitud
   */
  public function update($id, Request $request){
    try {
      DB::beginTransaction();
      $new = Cie10::find($id);
      $new->update(
          $request->all()
      );
      DB::commit();
      return array('success' => true, 'id' => $new->id);
    }catch(Exeption $e) {
      DB::rollBack();
      return array('success' => false);
    }
  }

  /**
   * Eliminar un registro en especifico
   * @param  integer $id identificador del registro, enviado en la ruta
   * @return Response contiene un arreglo con el estado de la solicitud
   */
  public function destroy($id){
    try {
      DB::beginTransaction();
      Cie10::find($id)->delete();
      DB::commit();
      return array('success' => true, 'id' => $id);
    }catch(Exeption $e) {
      DB::rollBack();
      return array('success' => false);
    }
  }
}
