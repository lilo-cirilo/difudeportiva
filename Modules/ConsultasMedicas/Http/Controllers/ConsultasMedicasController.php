<?php

namespace Modules\ConsultasMedicas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\DataTables\ConsultasMedicas\CitasTabla;
use App\DataTables\ConsultasMedicas\MedicosTabla;

use App\Models\ConsultasMedicas\ConsultaOdontologica;
use App\Models\ConsultasMedicas\Diagnostico;
use App\Models\ConsultasMedicas\Consulta;
use App\Models\ConsultasMedicas\Receta;
use App\Models\Usuario;

class ConsultasMedicasController extends Controller{
  
  /**
  * Crea una nueva instancia del controlador
  * Definiendo que el usuario debe estar logueado y autorizado
  * Ademas de los roles que debe tener
  */
  public function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,MEDICO,PSICOLOGO,ODONTOLOGO');
  }
  
  /**
  * Vista que muestra los registros existentes
  * @param  CitasTabla $tabla Servicio de DataTable que contiene los datos a mostrar
  * @return View              Contiene el html y la tabla con los datos
  */
  public function index(CitasTabla $tabla,Request $request){
    $fecha = strtotime(date("Y-m-d"));
    $iniSemana = strtotime('last Sunday',$fecha);
    $finSemana = strtotime('next Saturday',$fecha);
    $iniMes = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
    $finMes = date('Y-m-d', mktime(0,0,0, date('m'), date("d", mktime(0,0,0, date('m')+1, 0, date('Y'))), date('Y')));
    // $consultas = Consulta::where('empleado_id',$request->User()->persona->empleado->id);
    //  dd($request->User()->persona->empleado->id  );
    $NumConsultas = Array();
    $NumConsultas['dia']    = Consulta::where('empleado_id',$request->User()->persona->empleado->id)
    ->where('created_at','like','%'.date("Y-m-d").'%')->count();
    $NumConsultas['semana'] = Consulta::where('empleado_id',$request->User()->persona->empleado->id)
    ->whereBetween('created_at', [date('Y-m-d', $iniSemana), date('Y-m-d', $finSemana)])->count();
    $NumConsultas['mes']    = Consulta::where('empleado_id',$request->User()->persona->empleado->id)
    ->whereBetween('created_at', [$iniMes,$finMes.' 23:59:59'])->count();
    return $tabla->with('tipo','xDoc')
    ->render('consultasmedicas::index',['desempenio'=>$NumConsultas]);
  }
  /**
   * Regresa un conjunto de gráficas predefinidas
   *
   * @return vista con las gráficas
   */
  public function dashboard(){//MedicosTabla $tabla){
    $numMedi = Usuario::join('usuarios_roles','usuarios_roles.usuario_id','usuarios.id')->join('cat_roles','cat_roles.id','usuarios_roles.rol_id')->where('cat_roles.nombre','MEDICO')->count();
    $numPsic = Usuario::join('usuarios_roles','usuarios_roles.usuario_id','usuarios.id')->join('cat_roles','cat_roles.id','usuarios_roles.rol_id')->where('cat_roles.nombre','ODONTOLOGO')->count();
    $numOdon = Usuario::join('usuarios_roles','usuarios_roles.usuario_id','usuarios.id')->join('cat_roles','cat_roles.id','usuarios_roles.rol_id')->where('cat_roles.nombre','PSICOLOGO')->count();

    $pastel = app()->chartjs
    ->name('Distribucion')
    ->type('pie')
    ->size(['width' => 400, 'height' => 200])
    ->labels(['Medicos generales', 'Odontología', 'Psicólogos'])
    ->datasets([
      [
        'backgroundColor' => ['#88FF54', '#1EB9EB', '#EBE23D'],
        'hoverBackgroundColor' => ['#39E735', '#36A2EB', '#E7CC2D'],
        'data' => [$numMedi,$numPsic,$numOdon]
      ]
    ])
    ->options([
      'title' => [
        'text' => "Total de medicos: ".($numMedi+$numPsic+$numOdon),
        'display' => true,
        'position' => 'bottom'
      ]
      ]);
    
    $numMedi = Consulta::leftjoin('consulta_odontologica','consulta_odontologica.consulta_id','consultas.id')->leftjoin('consulta_psicologica','consulta_psicologica.consulta_id','consultas.id')->wherenull('consulta_odontologica.consulta_id')->wherenull('consulta_psicologica.consulta_id')->distinct('consultas.id')->count();
    $numPsic = Consulta::leftjoin('consulta_psicologica','consulta_psicologica.consulta_id','consultas.id')->wherenotnull('consulta_psicologica.consulta_id')->distinct('consultas.id')->count();
    $numOdon = Consulta::select(DB::raw('distinct(consultas.id)'))->leftjoin('consulta_odontologica','consulta_odontologica.consulta_id','consultas.id')->wherenotnull('consulta_odontologica.consulta_id')->get()->count();

    $barras = app()->chartjs
    ->name('Consultas')
    ->type('bar')
    ->size(['width' => 400, 'height' => 200])
    ->labels(['Total de citas: '.($numMedi+$numPsic+$numOdon)])
    ->datasets([
        [
            "label" => "Consultas generales",
            'backgroundColor' => ['#88FF54'],
            'data' => [$numMedi]
        ],
        [
            "label" => "Consultas psicologicas",
            'backgroundColor' => ['#1EB9EB'],
            'data' => [$numPsic]
        ],
        [
            "label" => "Consultas odontologicas",
            'backgroundColor' => ['#EBE23D'],
            'data' => [$numOdon]
        ]
    ])
    ->options( [ 'scales'=>[ 'yAxes'=>[[ 'ticks'=>[ 'beginAtZero' => true ], ]], ], ] );

    $diagnostico = Diagnostico::select(DB::raw('nombre, count(cie10_id) top'))->join('cat_cie10','diagnosticos.cie10_id','cat_cie10.id')->groupby('cie10_id','cat_cie10.nombre')->orderby('top','desc')->first();
    $tratamiento = Receta::select(DB::raw('nombre_generico,forma_farmaceutica,COUNT(medicamento_id) top'))->join('cat_medicamentos','cat_medicamentos.id','recetas.medicamento_id')->groupby('medicamento_id','nombre_generico','forma_farmaceutica')->orderby('top','desc')->first();
    $servOdon = ConsultaOdontologica::select(DB::raw('nombre,tipo, COUNT(servicio_id) top'))->join('cat_servicios_odontologicos','cat_servicios_odontologicos.id','consulta_odontologica.servicio_id')->groupby('servicio_id','cat_servicios_odontologicos.nombre','cat_servicios_odontologicos.tipo')->orderby('top','desc')->first();

    return view('consultasmedicas::dashboard',['pastel'=>$pastel,'barras'=>$barras,'diag'=>$diagnostico,'trat'=>$tratamiento,'serv'=>$servOdon]);
  }
}
    