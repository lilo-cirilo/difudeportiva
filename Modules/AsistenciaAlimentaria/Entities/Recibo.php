<?php
namespace Modules\AsistenciaAlimentaria\Entities;

class Recibo extends Base{

  protected $table = 'alim_recibos';
  protected $dates = ['deleted_at'];
  protected $fillable=['programacion_requisicion_id',
                       'folio_caja',
                       'cantidad',
                       'fecha_pago',
                       'referenciabancaria_id',
                       'usuario_id'

    ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    public function requisicionprogramacion()
    {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion', 'programacion_requisicion_id', 'id');
    }
    public function referenciasbancarias()
    {
        return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\Referenciasbancarias');
    }
}