<?php
namespace Modules\AsistenciaAlimentaria\Entities;

class Requisicion extends Base{

  protected $table = 'alim_requisiciones';
  protected $dates = ['deleted_at'];
  protected $fillable=[
      'licitacion_id',
      'num_bimestre',
      'num_entrega',
      'num_oficio',
      'fecha_oficio',
      'especial',
      'tipo_entrega_id',
      'estado_programa_id',
      'programa_id',
      'periodo',
      'observaciones',
      'usuario_id'];
  protected $hidden = array('updated_at', 'deleted_at');
  
  public function licitacion(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Licitacion','licitacion_id','id');
  }

  public function tipoentrega()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\TipoEntrega','tipo_entrega_id','id');
  }

  public function estadoprograma()
  {
    return $this->belongsTo('App\Models\EstadoPrograma','estado_programa_id','id');
  }

  public function programa()
  {
    return $this->belongsTo('App\Models\Programa','programa_id','id');
  }

  public function usuario()
  {
    return $this->belongsTo('App\Models\Usuario');
  }

  public function get_formato_fecha_oficio() {
    return \Carbon\Carbon::parse($this->fecha_oficio)->format('d/m/Y');
  }

  public function programacionrequisicion(){
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion','requisicion_id','id');
  }
}