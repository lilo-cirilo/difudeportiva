<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivosCancelacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_motivoscanceprog';
  protected $dates = ['deleted_at'];
  protected $fillable=['motivo','programacion_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }

  public function programacion()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion');
  }

}
