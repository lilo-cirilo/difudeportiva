<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehiculoProveedor extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_vehiculosproveedor';
  protected $dates = ['deleted_at'];
  protected $fillable=[
      'vehiculo_id',
      'placas',
      'usuario_id',
      'usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function vehiculo()
  {
    return $this->belongsTo('App\Models\Vehiculo');
  }
}
