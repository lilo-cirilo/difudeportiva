<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OficioValidacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_oficiosvalidacion';
  protected $dates = ['deleted_at'];
  protected $fillable=['num_oficio','ejercicio_id','fecha_oficio','programa_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function ejercicio()
  {
    return $this->belongsTo('App\Models\Ejercicio');
  }

  public function programa()
  {
    return $this->belongsTo('App\Models\Programa');
  }
}
