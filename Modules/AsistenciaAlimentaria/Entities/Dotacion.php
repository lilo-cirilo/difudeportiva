<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dotacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_dotaciones';
  protected $dates = ['deleted_at'];
  protected $fillable=['tipoaccion_id','cuota_recuperacion','ejercicio_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
  public function tipoaccion(){
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\TipoAccion');
  }

  public function detallesDotacion(){
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\DetalleDotacion');
  }
 
  public function detallesLicitacion()
  {
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\DetallesLicitacion');
  }

  public function requisicioncantidaddotacion()
  {
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion');
  }

  public function costo(){
      $costo = 0;
      foreach ($this->detallesDotacion as $detalle) {
          $costo += $detalle->cantidad * $detalle->costoproducto->costo;
      }
      return $costo;
  }

  public function ejercicio(){
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Ejercicio');
  }
}
