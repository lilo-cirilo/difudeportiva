<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comite extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_comites';
  protected $dates = ['deleted_at'];
  protected $fillable=['persona_id','cargo_programa_id','programacion_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }
 
  public function programacion()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion');
  }

  public function persona()
  {
      return $this->belongsTo('App\Models\Persona');
  }

  public function cargoprograma()
  {
      return $this->belongsTo('App\Models\CargoPrograma','cargo_programa_id','id');
  }
}
