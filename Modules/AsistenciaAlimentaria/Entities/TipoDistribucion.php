<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoDistribucion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_cat_tiposdistribucion';
  protected $dates = ['deleted_at'];
  protected $fillable=['nombre'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
}
