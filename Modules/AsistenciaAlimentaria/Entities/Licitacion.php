<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licitacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_licitaciones';
  protected $dates = ['deleted_at'];
  protected $fillable=['modulo_proveedor_id','num_licitacion','ejercicio_id','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function detallelicitacion(){
    return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\DetalleLicitacion','id','licitacion_id');
  }
 
  public function ejercicio(){
    return $this->belongsTo('App\Models\Ejercicio');
  }

  public function moduloproveedor() {
    return $this->belongsTo('App\Models\ModuloProveedor','modulo_proveedor_id','id');
  }
  
}
