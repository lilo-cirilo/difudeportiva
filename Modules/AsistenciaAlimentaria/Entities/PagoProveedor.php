<?php
namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PagoProveedor extends Model{
  use SoftDeletes;

  protected $table = 'alim_pagoproveedores';
  protected $dates = ['deleted_at'];
  protected $fillable=['alimentarios_requisicion_id','programa_id','numero_factura','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }

  public function programa()
  {
      return $this->belongsTo('App\Models\Programa');
  }

  public function alimemtariosRequisicion()
  {
      return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\AlimentarioRequisicion');
  }
}