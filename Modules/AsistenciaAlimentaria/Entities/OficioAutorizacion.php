<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OficioAutorizacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_oficio_autorizacion';
  protected $dates = ['deleted_at'];
  protected $fillable=['num_oficio','num_referencia','tipo_oficio_id','fecha_emision','region_id','dotacion_id','costo_autorizado','num_dotaciones'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function tipooficio()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\TipoOficio');
  }

  public function dotacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Dotacion');
  }
}
