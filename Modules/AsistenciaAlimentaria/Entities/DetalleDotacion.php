<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetalleDotacion extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_detallesdotacion';
  protected $dates = ['deleted_at'];
  protected $fillable=['costo_producto_id','dotacion_id','cantidad'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
 
  public function costoproducto()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\CostoPresentacionProducto','costo_producto_id','id');
  }

  public function dotacion()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Dotacion');
  }
}
