<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Programa;

class Programacion extends Model
{
    use SoftDeletes;

    protected $table = 'alim_programacion';
    protected $dates = ['deleted_at'];
    protected $fillable = ['anios_programa_id', 'estado_programa_id', 'financiamiento_id', 'solicitud_id', 'alimentario_id', 'usuario_id', 'esampliacion'];
    protected $hidden = array('deleted_at');
    //protected $with = ['comite'];

    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario');
    }

    public function alimentario()
    {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Alimentario');
    }

    public function financiamiento()
    {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Financiamiento');
    }

    public function estadoprograma()
    {
        return $this->belongsTo('App\Models\EstadoPrograma', 'estado_programa_id', 'id');
    }

    public function anioprograma()
    {
        return $this->belongsTo('App\Models\AniosPrograma', 'anios_programa_id', 'id');
    }

    public function beneficiarios()
    {
        return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\Beneficiario', 'programacion_id', 'id');
    }

    public function comite()
    {
        return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\Comite', 'programacion_id', 'id');
    }

    public function convenio()
    {
        return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\Convenio', 'programacion_id', 'id');
    }

    public function referenciasbancaria()
    {
        return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\ReferenciaBancaria', 'programacion_id', 'id');
    }

    public function programacionrequisiciones()
    {
        return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion', 'programacion_id', 'id');
    }

    public function scopeGetValidForRequisicion( $query, $requisicion, $request){
        $mismoBimestre = RequisicionProgramacion::whereHas('estadoPrograma',function ($qery){
            $qery->whereHas('estado',function($qry){
                $qry->where('nombre','!=','CANCELADA');
            });
        })
        ->whereHas('requisicion',function($q) use($requisicion){
            $q->where('num_bimestre',$requisicion->num_bimestre)
            ->Where('id','!=',$requisicion->id);
        })
        ->get()->pluck('programacion_id');
        // dd($requisicion);
        $numBeneficiarios;
        $numComites;
        $comites = [];
        switch ($requisicion->programa_id) {
            case config('asistenciaalimentaria.ccncId'):
                $query->whereNull('alim_sujetos.id');
                $numBeneficiarios = 30;
                $numComites = 3;
                $comites =["PRESIDENTE(A)","VOCAL DE ABASTO Y ALMACÉN","VOCAL DE ORIENTACIÓN ALIMENTARIA Y SALUD"];
                break;
            case config('asistenciaalimentaria.sujetosId'):
                if( $request->has('reqFor') && $request->get('reqFor')=='desarrollo'){
                    $query->whereNotNull('alim_sujetos.id')->whereNotNull('alim_sujetos.dedesarrollo_programa_id');
                    $numBeneficiarios = 1;
                    $numComites = 1;
                    $comites =["PRESIDENTE(A)"];
                }else{
                    $query->whereNotNull('alim_sujetos.id')->whereNull('alim_sujetos.dedesarrollo_programa_id');
                    $numBeneficiarios = 30;
                    $numComites = 3;
                    $comites =["PRESIDENTE(A)","SECRETARIO(A)","TESORERO(A)"];
    
                }
                break;
            case config('asistenciaalimentaria.lecheId'):
                $numBeneficiarios = 1;
                $numComites = 1;
                $comites =["PRESIDENTE(A)"];
                break;
            case config('asistenciaalimentaria.desayunosId'):
                $numBeneficiarios = 1;
                $numComites = 1;
                $comites =["PRESIDENTE(A)"];
                break;
            default:
                $numBeneficiarios = 30;
                $numComites = 3;
                break;
        }
        $subWhere = "nombre = '$comites[0]'";
        for ($i=1; $i < count($comites); $i++) { 
            $aux = $comites[$i];
            $subWhere = $subWhere." or nombre = '$aux'";
        }
        $query->join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
        ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
        ->join('alim_cat_financiamientos','alim_cat_financiamientos.id','alim_programacion.financiamiento_id')
        ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
        ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
        ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
        ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
        ->join('estados_programas','estados_programas.id','alim_programacion.estado_programa_id')
        ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
        ->leftJoin('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id')
        ->where('alim_alimentarios.programa_id',$requisicion->programa_id)
        ->where('cat_ejercicios.anio',config('asistenciaalimentaria.anioActual'))
        ->where('cat_estados.nombre','ACTIVA - BLOQUEADA')
        ->whereRaw("(select count(*) from alim_beneficiarios where programacion_id=alim_programacion.id and alim_beneficiarios.deleted_at is null) >= $numBeneficiarios")
        ->whereRaw('(select count(*) from alim_convenios where programacion_id=alim_programacion.id and alim_convenios.deleted_at is null) = 1')
        ->whereRaw("(select count(*) from alim_comites
            inner join cargos_programas on cargos_programas.id = alim_comites.cargo_programa_id
            inner join cat_cargos on cat_cargos.id = cargos_programas.cargo_id
            where programacion_id=alim_programacion.id
            and cargos_programas.programa_id = $requisicion->programa_id
            and alim_comites.deleted_at is null
            and ($subWhere)) >= $numComites")
        ->whereNotIn('alim_programacion.id',$mismoBimestre);
        // dd($mismoBimestre);
        // dd($query->toSql());
    }
}
