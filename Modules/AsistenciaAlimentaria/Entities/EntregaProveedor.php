<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EntregaProveedor extends Model{
  use SoftDeletes;
  
  protected $table = 'alim_entregasproveedor';
  protected $dates = ['deleted_at'];
  protected $fillable=[
    'requisicion_datocarga_id',
    'fecha_entrega',
    'recibe_comite_id',
    'observaciones',
    'usuario_id'
  ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function getFormatFecha()
  {
    return (\DateTime::createFromFormat('Y-m-d', $this->fecha_entrega))->format('d/m/Y');
  }

  public function comite()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Comite','recibe_comite_id','id')->withTrashed();
  }

  public function validacionentrega()
  {
    return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\ValidacionEntrega','id','entregaproveedor_id');
  }
  public function validacion()
  {
      return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\ValidacionEntrega', 'entregaproveedor_id', 'id');
  }
}
