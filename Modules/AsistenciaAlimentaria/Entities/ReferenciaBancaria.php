<?php
namespace Modules\AsistenciaAlimentaria\Entities;

class ReferenciaBancaria extends Base{

  protected $table = 'alim_referenciasbancarias';
  protected $dates = ['deleted_at'];
  protected $fillable=[
                    'programacion_id',
                    'banco_id',
                    'referencia',
                    'recibo_id'


    ];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    public function recibo()
    {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Recibo', 'recibo_id', 'id');
    }
}