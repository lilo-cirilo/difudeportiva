<?php

namespace Modules\AsistenciaAlimentaria\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RequisicionProgramacion extends Model {
	use SoftDeletes;
	protected $table = 'alim_programacion_requisiciones';
	protected $dates = ['deleted_at'];
	protected $fillable = [
		'requisicion_id',
		'programacion_id',
		'estado_programa_id',
		'motivo_cancelacion',
		'usuario_id'
	];
	protected $hidden = array('deleted_at');
	
	public function usuario()
	{
		return $this->belongsTo('App\Models\Usuario');
	}
	
	public function requisicion()
	{
		return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Requisicion');
	}
	
	public function programacion()
	{
		return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Programacion');
	}

	public function estadoPrograma()
	{
		return $this->belongsTo('App\Models\EstadoPrograma');
	}

	public function requisicioncantidaddotacion()
	{
		return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion','programacion_requisicion_id','id');
	}
	
	public function requisicionDatoCarga()
	{
		return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\RequisicionDatoCarga','programacion_requisicion_id','id');
	}

	public function recibo()
	{
		return $this->hasOne('Modules\AsistenciaAlimentaria\Entities\Recibo','programacion_requisicion_id','id');
	}

}