<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reporte</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-8">
                <div class="row">
                    <div class="col">
                        <div class="row"><h4>DIRECCIÓN DE OPERACIÓN DE ASISTENCIA ALIMENTARIA</h4></div>
                        <div class="row"><h5>VALIDACIÓN DE CENSO DE BENEFICIARIOS</h5></div>
                        <div class="row"><h6>{{ $programacion->anioprograma->programa->nombre }}</h6></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="row">REGION: <strong>{{ $programacion->alimentario->municipio->distrito->region->nombre }}</strong></div>
                        <div class="row">MUNICIPIO: <strong>{{ $programacion->alimentario->municipio->nombre }}</strong></div>
                        <div class="row">EJERCICIO: <strong>{{ $programacion->anioprograma->ejercicio->anio }}</strong></div>
                        @if ($programacion->alimentario->programa_id == config('asistenciaalimentaria.desayunosId'))
                            <div class="row">ESCUELA: <strong>{{ $programacion->alimentario->desayuno->escuela->nombre }}</strong></div>
                        @endif
                    </div>
                    <div class="col">
                        <div class="row">DISTRITO: <strong>{{ $programacion->alimentario->municipio->distrito->nombre }}</strong></div>
                        <div class="row">LOCALIDAD: <strong>{{ $programacion->alimentario->localidad->nombre }}
                            @isset($programacion->alimentario->sublocalidad)
                                - {{ $programacion->alimentario->sublocalidad }}
                            @endisset</strong></div>
                        <div class="row">COCINA: <strong>{{ $programacion->alimentario->foliodif }}</strong></COCINA:></div>
                        @if ($programacion->alimentario->programa_id == config('asistenciaalimentaria.desayunosId'))
                            <div class="row">CLAVE: <strong>{{ $programacion->alimentario->desayuno->escuela->clave }}</strong></div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-4">
                <img src="{{ asset('images/reportes/Oaxaca_DIF.png') }}" alt="Oaxaca | DIF" class="img-fluid">
                <p class="mt-4 text-right">FECHA: {{ Carbon\Carbon::now()->format('d-m-Y') }}</p>
                <p class="text-left"><strong>TOTAL DE BENEFICIARIOS: {{ $programacion->beneficiarios->count() }}</strong></p>
            </div>
        </div>
    </div>
</body>

</html>