<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Print Table</title>
        <meta charset="UTF-8">
        <meta name=description content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            body{
                display: flex;
                margin-top: 0;
                flex-wrap: wrap;
            }
            .flex-container {
                width: 100%;
                display: flex;
                justify-content: flex-end;
            }
            .flex-container > div {
                text-align: center;
                font-size: 8px;
                align-self: center;
            }
            div.footer{
                width: 100%;
            }
            div.footer img {
                width: 100%;
                height: auto;
            }
            body {
                display: flex;
                min-height: 95vh;
                flex-direction: column;
            }
            main {
                flex: 1 0 auto;
                border:solid 2px #e4e4e4;
            }
            header{
                display: flex;
                justify-content: flex-end;
            }
            #logo{
                width: auto;
                max-height: 70px;
                margin-bottom: 5px;
                float: right;;
            }
            .page-break{ display: block; page-break-before: always; }
            table{
                margin-bottom: 50px;
                margin-top: 50px;
            }
        </style>
    </head>
    <body style="font-size:12px">
        <table class="table table-bordered table-sm table-striped">
            @php
                $aux = 'nuevo';
                $conta = 1;
                $benefs = 0;
            @endphp
            @foreach($data as $row)
                @php
                    $conta++;
                @endphp
                @if ($conta % 37 == 0)
                    </table>
                    <table class="table table-bordered table-sm table-striped">
                @endif
                @if ($row == reset($data) || ($conta % 37 == 0 && $conta!=1))
                    {{-- @php --}}
                        {{-- if($conta != 2) --}}
                            $conta++;   
                    {{-- @endphp --}}
                    <tr>
                        <th style="white-space: nowrap;">#</th>
                        @foreach($row as $key => $value)
                            @if(!$loop->last)
                                <th class="text-center" style="white-space: nowrap;">{!! strtoupper($key) !!}</th>
                            @endif
                        @endforeach
                    </tr>
                @endif
                @if ($aux!==$row["Pertenece a"])
                    @if ($benefs>0)
                        @php
                            $conta++;
                        @endphp
                        <tr>
                            <td class="text-center" colspan="{{ count($row) }}"> TOTAL DE BENEFICIARIOS POR PROGRAMA: {{ $benefs }} </td>
                        </tr>
                    @endif
                    @php
                        $aux=$row["Pertenece a"];
                        $conta++;
                        $benefs=0;
                    @endphp
                    <tr style="background:lightgrey;">
                        <td colspan="{{ count($row) }}">{{ $aux }}</td>
                    </tr>
                @endif
                <tr>
                    @php
                        $benefs++;
                    @endphp
                    <td class="text-center">{{ $benefs }}</td>
                    @foreach($row as $key => $value)    
                        @if(/* (is_string($value) || is_numeric($value)) && */ !$loop->last)
                            <td class="text-center" style="white-space: nowrap;">{!! $value !!}</td>
                        @endif
                    @endforeach
                    {{-- <td>{{ $loop->index }} | {{ $conta }}</td>   --}}
                </tr>
            @endforeach
            <tr>
                <td class="text-center" colspan="{{ count($data[0]) }}"> TOTAL DE BENEFICIARIOS POR PROGRAMA: {{ $benefs }} </td>
            </tr>
        </table>
    </body>
</html>
