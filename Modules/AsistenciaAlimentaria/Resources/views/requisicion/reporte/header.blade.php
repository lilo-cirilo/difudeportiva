<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Reporte</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <style>
            
        </style>
    </head>
    
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-10">
                    
                            <div class="row"><h5><strong>SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA DEL ESTADO DE OAXACA</strong></h5></div>
                            <div class="row"><h5><strong>DIRECCIÓN DE ASISTENCIA ALIMENTARIA</strong></h5></div>
                            <div class="row"><h6>{{$requisicion->programa_id}}</h6></div>
                            <div class="row"><h6>REQUISICION DE DISTRIBUCION DE DOTACIONES ALIMENTICIAS SEGUN NUMERO DE OFICIO {{$requisicion->num_oficio or ''}} DE ALIMENTARIOS,CORRESPONDIENTE AL EJERCICIO </h6></div>
                            <hr style= "border:3px solid #979A9A">
                            <div class="row"><h6>BENEFICIARIOS Y DOTACIONES</h6></div>
                        </div>
                    </div>
                    
                </div>
                <div class="col-2">
                    <img src="" alt="Oaxaca | DIF" class="img-fluid">
                    
                </div>
            </div>

            <div class="row"  style="text-align:center;">
                <div class="col-4">
                    <div class="row "> </div>
                </div>
                <div class="col-1">
                    <font size=1 >ATENCION A MENORES DE 5 AÑOS EN RIESGO,NO ESCOLARIZADOS</font>
                     <hr style="background:#000">
                </div>
                <div class="col-1">0
                    <font size=1 >DESAYUNOS ESCOLARES</font>
                    <hr style="background:#000">
                </div>
                <div class="col-2">
                    <font size=1 >ASISTENCIA ALIMENTARIA A SUJETOS VULNERABLES</font>
                    <hr style="background:#000">
                </div>
                <div class="col-1">
                    <font size=1 >TOTAL BENEF.</font>
                </div>
                <div class="col-1">
                    <font size=2 >TOTAL DOT.</font>
                </div>
                <div class="col-1">
                    <font size=2 >PRESIDENTA</font>
                </div>
                <div class="col-1">
                    <font size=2 >VOCAL DE ABASTO Y ALMACEN</font>
                </div>
                <div class="col-1">
                    <font size=2 >VOCAL DE ORIENTACION ALIMENTARIA/font>
                </div>
            </div>
    
            <div class="row"  style="text-align:center;">
                <div class="col-3">
                    <div class="row"> </div>
                </div>
                <div class="col-1">
                    <font size="2">TOTAL COCINAS</font>
                </div>
                <div class="col-1">
                    <font size=2 >6-11 MESES</font>
                </div>
                <div class="col-1">
                    <font size=2 >1-5 AÑOS 11 MESES</font>
                </div>
                <div class="col-2">
                   <font size=2 >6-A 12 AÑOS</font>
                </div>
                <div class="col-1">
                   <font size=2 >15 -35 AÑOS</font>
                </div>
                <div class="col-1">
                   <font size=2 >EMB. Y/O LACT.</font>
                </div>
                <div class="col-1">
                   <font size=2 >SUJETOS VULNERABLES</font>
                </div>
            </div>
            
             <div class="row"  style="text-align:center;">
                <div class="col-0">
                    <div class="row"> FOLIO FICHA PAGO</div>
                </div>
                <div class="col-2">
                    <div class="row"> LOCALIDAD (COL.BARRIO O RANCHERIA)</div>
                </div>
                <div class="col-1">
                    <div class="row"> FOLIO DIF</div>
                </div>
                <div class="col-1">
                    <div class="row"> </div>
                </div>
                <div class="col-1">
                    <font size=2 >BENEF.</font>
                </div>
                <div class="col-1">
                    <font size=2 >DOT.</font>
                </div>
                <div class="col-1">
                    <font size=2 >BENEF.</font>
                </div>
                <div class="col-2">
                   <font size=2 >DOT.</font>
                </div>
                <div class="col-1">
                   <font size=2 >BENEF.</font>
                </div>
                <div class="col-1">
                   <font size=2 >DOT.</font>
                </div>
                <div class="col-1">
                   <font size=2 >BENEF.</font>
                </div>
            </div>
            <hr style= "border:3px solid #979A9A">
            
          
           
        </div>
    </body>
    
    </html>