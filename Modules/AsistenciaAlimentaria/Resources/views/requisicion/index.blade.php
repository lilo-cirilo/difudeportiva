@extends('asistenciaalimentaria::layouts.master')

@section('content-title', 'Tabla de Requisiciones')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
    <div class="box-header with-border text-center">
        <h4>{{$programa->nombre}}</h4>
    </div>
    <div class="box-body">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Requisiciones
                {{-- <div class="card-header-actions">
                    <a class="card-header-action" href="https://datatables.net" target="_blank">
                    <small class="text-muted">docs</small>
                    </a>
                </div> --}}
            </div>
            <div class="card-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                    <input type="text" id="search" name="search" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </div>
                </div>
                {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'requisiciones']) !!}
            </div>
        </div>
    </div>
</div>
@stop

@push('body')
	{!! $dataTable->scripts() !!}
	<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
            $('#search').keypress(function(e) {
                if(e.which === 13) {
                    $(tablaId).DataTable().search($('#search').val()).draw()
                }
            })
            $('#btn_buscar').on('click', function() {
                $(tablaId).DataTable().search($('#search').val()).draw()
            })
            function recargar(params) {
                $(tablaId).DataTable().ajax.reload(null,false);
            }
            return {
                recargar : recargar
            }
        })('#requisiciones');
    function enviarProveedor(url,reload) {
        Swal.fire({
            title: "¿Estás seguro?",
            text: "Se enviará al proveedor esta requisición!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, hazlo!",
            cancelButtonText: "No."
        }).then((result) => {
            if (result.value) {
                transaccion.miniAjax(url,"PUT",{estado:"ENVIADA"})
            }
        })
    }
    /* function generarRecibos(url) {
        $.blockUI();
        $.ajax({
            url : url,
            global : false
        }).done(function (data, textStatus, jqXHR) {
            jsreport.serverUrl = "http://187.157.97.110:3002";
            // jsreport.serverUrl = "http://localhost:5488";

            var request = {
                template: {
                    shortid: "B1lTKVbhlH"
                    // shortid : "S1eeAHi5eH"
                },
                data: data
            }
            jsreport.renderAsync(request).then(function (res2) {
                res2.download('recibo(s).pdf');
            }).catch(()=>{
                Swal.fire('Error','No se genero el pdf','error');
            }).finally(()=>{
                $.unblockUI();
            })
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire('Error','Algo salio mal','error');
        })
    } */
    function generarRecibos(url) {
        $.blockUI({
            message:`<i class="fas fa-spinner fa-pulse fa-3x"></i></br><h4 id="block_message">Cargando datos</h4>`})
        $.ajax({
            url : url,
            global : false
        }).done(function (data, textStatus, jqXHR) {
            jsreport.serverUrl = "http://187.157.97.110:3002";
            // jsreport.serverUrl = "http://localhost:5488";
            let arraySalida = [];
            let temp_array = data.datos;
            //arraySalida = data.datos;
            var c = 0;
            if(data.datos.length > 500){
                while (temp_array.length > 500) {
                    arraySalida[c] = temp_array.slice(0,500);
                    temp_array = temp_array.slice(500);
                    c++;
                }
            }
            if (temp_array.length > 0)
                arraySalida[c] = temp_array;
            $('#block_message').html(`<h4>Generando archivo <label id="generacionActual">0</label> de <label id="generacionTotal">${c+1}</label></h4>`)
            //data.data.datos = arraySalida[0];
            reporteRecursivo(0,c,arraySalida)
            
        }).fail(function (jqXHR, textStatus, errorThrown) {
            Swal.fire('Error','No se obtuvieron los datos','error');
        })
    }

    function reporteRecursivo(index,limite,datas) {
        $('#generacionActual').text(parseInt($('#generacionActual').text())+1)
        var request = {
            template: {
                shortid: "B1lTKVbhlH"
            },
            data: {datos:datas[index]}
        }
        jsreport.renderAsync(request).then(function (res2) {
            res2.download('recibo(s).pdf');
            if(index<limite){
                reporteRecursivo(index+1,limite,datas)
            }else{
                $.unblockUI()
            }
        }).catch(function (error) {
            Swal.fire('Error','No se genero el dpf, '+error.message,'error');
            $.unblockUI()
        })
    }
  </script>
@endpush