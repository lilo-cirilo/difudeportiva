@php
    $prefix = (strpos($_SERVER["REQUEST_URI"],'ccnc') !== false ? 'ccnc' : (strpos($_SERVER["REQUEST_URI"],'desayunos') !== false ? 'desayunos' : (strpos($_SERVER["REQUEST_URI"],'caics') !== false ? 'caics' : 'sujetos')));
@endphp
<div class="modal-header">
<h5 class="modal-title">{{isset($requisicion) ? 'Actualizar' : 'Agregar'}} requisición <i class="fa fa-book" aria-hidden="true"></i></h5>
<button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
    <form role="form" id="form-requisicion">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="num_oficio">*Número de oficio:</label>
                <input type="text" id="num_oficio" name="num_oficio" class="form-control" placeholder="Ingrese número de oficio" value="{{$requisicion->num_oficio or ''}}" required>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="fecha_oficio">*Fecha oficio:</label>
                    <input type="text" id="fecha_oficio" name="fecha_oficio" class="form-control mask" value="{{isset($requisicion) ? $requisicion->get_formato_fecha_oficio()  : ''}}" data-inputmask='"mask": "99/99/9999"' placeholder="Ingrese fecha de oficio" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="num_bimestre">*Bimestre:</label>
                    <select id="num_bimestre" name="num_bimestre" class="form-control" style="width: 100%" required>
                        @for ($i = 1; $i <= 6; $i++)
                            <option value="{{ $i }}" {{isset($requisicion) ? ($requisicion->num_bimestre == $i ? 'selected' : '') : ''}}>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="especial">*Especial:*</label>
                    <select id="especial" name="especial" class="form-control" style="width: 100%" {{isset($requisicion) ? '' : 'selected'}} required>
                        <option value="0" {{isset($requisicion) ? ($requisicion->especial == 0 ? 'selected' : '') : ''}}>NO</option>
                        <option value="1" {{isset($requisicion) ? ($requisicion->especial == 1 ? 'selected' : '') : ''}}>SI</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="num_entrega">*Número de entrega:</label>
                    <select id="num_entrega" name="num_entrega" class="form-control" style="width: 100%" required>
                        @for ($i = 1; $i <= 9; $i++)
                            <option value="{{ $i }}" {{isset($requisicion) ? ($requisicion->num_entrega == $i ? 'selected' : '') : ''}}>{{ $i }}</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="tipo_entrega_id">*Tipo entrega:</label>
                    <select id="tipo_entrega_id" name="tipo_entrega_id" class="form-control select2" style="width: 100%" required>
                        @if (isset($requisicion))
                            <option value="{{ $requisicion->tipoentrega->id }}" selected>{{ $requisicion->tipoentrega->nombre }}</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="licitacion_id">*Licitación:</label>
                    <select id="licitacion_id" name="licitacion_id" class="form-control select2" style="width: 100%" required>
                        @if (isset($requisicion))
                            <option value="{{ $requisicion->licitacion_id }}" selected>{{ "#licitación: {$requisicion->licitacion->num_licitacion} - proveedor: {$requisicion->licitacion->moduloproveedor->recmproveedor->nombre} - {$requisicion->licitacion->ejercicio->anio}" }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div id="periodo" class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="{{ isset($requisicion->periodo) ? '' : 'display:none' }}">
                <div class="form-group">
                    <label for="periodo">Periodo*</label>
                    <select type="number" id="periodo" name="periodo" class="form-control" value="{{ $requisicion->periodo or '' }}" required>
                        <option value="1" {{ isset($requisicion) ? ($requisicion->periodo == 1 ? 'selected' : '') : ''}}>1 Mes</option>
                        <option value="2" {{ isset($requisicion) ? ($requisicion->periodo == 2 ? 'selected' : '') : 'selected'}}>2 Meses</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label for="observaciones">Observación*</label>
                    <textarea id="observaciones" name="observaciones" class="form-control" style="width: 100%;" required>{{ $requisicion->observaciones or '' }}</textarea>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    @php
        $r = isset($requisicion) ? route("alimentarios.$prefix.requisiciones.update",$requisicion->id) : route("alimentarios.$prefix.requisiciones.index");
        $m = isset($requisicion) ? 'PUT' : 'POST';
    @endphp
    <button class="btn pull-right btn-primary" onclick="transaccion.formAjax('{{ URL::to($r) }}','{{$m}}','form-requisicion',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
</div>
<script type="text/javascript">
    (function(){
        validacion.iniciar('form-requisicion',{ num_oficio: {
            pattern_num_ofi : '' 
        }})
        $('#fecha_oficio').datepicker({
            autoclose: true,
            language: 'es',
            format: 'dd/mm/yyyy',
            startDate: '01-01-1900',
            endDate: '0d',
            orientation: 'bottom',
            enableOnReadonly : false
        })
        $('.mask').inputmask()
        $('.select2').select2({
            allowClear : true,
            language: 'es',
            placeholder : 'SELECCIONE ...',
            minimumResultsForSearch: Infinity
        })
        $('#programa').select2({
            language: 'es',
            placeholder : 'SELECCIONE ...',
            ajax: {
                url: '{{ route('alimentarios.estados') }}',
                delay: 500,
                dataType: 'JSON',
                global: false,
                type: 'GET',
                data: function(params) {
                return {
                    search: params.term,
                    programa_id: "{{config('asistenciaalimentaria.fatherId')}}"
                };
                },
                processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function(item) {
                    return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                    }
                    })
                };
                },
                cache: true
            }
        }).change(function(event) {
            $('#programa').valid()
        })
        $('#tipo_entrega_id').select2({
            language: 'es',
            placeholder : 'SELECCIONE ...',
            ajax: {
                url: '{{ route('alimentarios.tiposentrega') }}',
                delay: 500,
                dataType: 'JSON',
                global: false,
                type: 'GET',
                data: function(params) {
                return {
                    search: params.term
                };
                },
                processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function(item) {
                    return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                    }
                    })
                };
                },
                cache: true
            }
        }).change(function(event) {
            $('#tipo_entrega_id').valid()
            if($('#tipo_entrega_id').val()==3)
                $('#periodo').show();
            else
                $('#periodo').hide();
        })
        $('#licitacion_id').select2({
            language: 'es',
            placeholder : 'SELECCIONE ...',
            ajax: {
                url: '{{ route('alimentarios.catlicitaciones') }}',
                delay: 500,
                dataType: 'JSON',
                global: false,
                type: 'GET',
                data: function(params) {
                return {
                    search: params.term
                };
                },
                processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data, function(item) {
                    return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                    }
                    })
                };
                },
                cache: true
            }
        }).change(function(event) {
            $('#licitacion_id').valid()
        })
        $.validator.addMethod('pattern_num_ofi', function (value, element) {
            return this.optional(element) || /^(\w+\/\w+\/\w+\/\w)|\d+$/i.test(value);
        }, "Introduce un numero de oficio valido");
    })()
</script>