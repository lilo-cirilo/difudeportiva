@extends('asistenciaalimentaria::layouts.master')

@section('title', 'Catalogo de oficios')

@push('head')
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <i class="fa fa-align-justify"></i> Catalogo de oficios
        </div>
        <div class="card-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                <input type="text" id="search" name="search" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                </div>
            </div>
            {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'oficios']) !!}
        </div>
    </div>
@stop

@push('body')
	{!! $dataTable->scripts() !!}
	<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
            $('#search').keypress(function(e) {
                if(e.which === 13) {
                    $(tablaId).DataTable().search($('#search').val()).draw()
                }
            })
            $('#btn_buscar').on('click', function() {
                $(tablaId).DataTable().search($('#search').val()).draw()
            })
            function recargar(params) {
                $(tablaId).DataTable().ajax.reload(null,false);
            }
            return {
                recargar : recargar
            }
        })('#oficios');
  </script>
@endpush