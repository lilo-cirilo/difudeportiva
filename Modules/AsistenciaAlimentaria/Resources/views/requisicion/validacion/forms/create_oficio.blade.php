<div class="modal-header">
    <div class="modal-title">
    <h5><strong>Nuevo oficio para <br> </strong>{{ $programa->nombre }}</h5>
    </div>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
  <form id="form-oficios">
    <div class="row" style="display:none;">
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" name='programa_id' value="{{ $programa->id }}" class="form-control">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="num_oficio">N° de Oficio:</label>
                <input type="text" name="num_oficio" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="fecha">Fecha Oficio:</label>
                <input type="text" name="fecha" id="fecha" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="ejericio_id">Ejercicio</label>
                <select name="anio" id="ejercicio_id" style='width:100%;' required>
                    <option selected disabled>seleccione ...</option>
                    <option value="{{ date('Y') }}">{{ date('Y') }}</option>
                    <option value="{{ date('Y')-1 }}">{{ date('Y')-1 }}</option>
                </select>
            </div>
        </div>
    </div>
  </form>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    @php
        $aux=$programa->id==config('asistenciaalimentaria.ccncId') ? 'ccnc' : ($programa->id == config('asistenciaalimentaria.sujetosId') ? 'sujetos' : 'desayunos')
    @endphp
    <button class="btn btn-primary pull-right" onclick='transaccion.formAjax("{{ route("alimentarios.$aux.validacion.oficios.store") }}","POST","form-oficios")'><span><i class="fa fa-save"></i> Guardar</span></button>
</div>
<script type="text/javascript">
    (function(){
        validacion.iniciar('form-oficios')
        $('#fecha').datepicker({
            autoclose: true,
            language: 'es',
            format: 'dd/mm/yyyy',
            startDate: moment().day(-14).format('DD/MM/YYYY'),//moment().format('DD/MM/YYYY')
            endDate: moment().format('DD/MM/YYYY')
        });
        $
    })()
</script>
