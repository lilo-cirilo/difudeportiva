@extends('asistenciaalimentaria::layouts.master')

@section('title', 'Tabla de distribuciones')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
    <div class="box-header with-border text-center">
        <h4>{{-- {{$programa->nombre}} --}}</h4>
    </div>
    <div class="box-body">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> distribuciones
            </div>
            <div class="card-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                    <input type="text" id="search" name="search" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </div>
                </div>
                {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped table-sm', 'style'=>'width:100%', 'id' => 'requisiciones'],true) !!}
            </div>
        </div>
    </div>
</div>
@stop

@push('body')
{!! $dataTable->scripts() !!}
<script>
    ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
        }
    })('#requisiciones');
</script>
@endpush