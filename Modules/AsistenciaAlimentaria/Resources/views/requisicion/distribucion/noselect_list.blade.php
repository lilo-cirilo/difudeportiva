{{-- Modal usado para mortrar las cocinas que no son aptas para la distribucion --}}
<div class="modal-header">
  <h5 class="modal-title">Programaciones incompletas (no disponibles para selección)</h5>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  <div class="card">
      <div class="card-header bg-danger font-weight-bold">
          <i class="fa fa-align-justify"></i> Lista de programaciones activas-bloqueadas no disponibles para requisición
      </div>
      <div class="card-body">
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
            <input type="text" id="search" name="search" class="form-control">
            <div class="input-group-append">
                <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
            </div>
        </div>
        {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'noseleccion']) !!}
      </div>
    </div>
</div>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
  (function() {
    var tablaB = ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
    }})('#noseleccion')
  })()
</script>