
<div class="modal-header bg-gray-dark text-white">
<div class="modal-title">
  <h5><strong>FOLIO: </strong>{{$requisicionProgramacion->programacion->alimentario->foliodif}}</h5>
  <h5><strong>MUNICIPIO: </strong>{{$requisicionProgramacion->programacion->alimentario->municipio->nombre}}</h5>
  <h5><strong>LOCALIDAD: </strong>{{$requisicionProgramacion->programacion->alimentario->localidad->nombre}}</h5>
</div>
<button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
  <form id="form-dotacion">
    <table class="table table-sm">
      <thead>
        <tr>
          <th scope="col">Nombre sub-programa</th>
          <th scope="col"># Beneficiarios</th>
          <th scope="col"># Dotaciones</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($requisicionProgramacion->requisicioncantidaddotacion as $item)
          @php
            $numBenef = $item->cantidad_beneficiarios;
            $dotacion = $item->cantidad_dotaciones;
            $accion   = $item->dotacion->tipoaccion;
          @endphp
          <tr>
            <th id="{{$accion->id}}">{{ explode('(',explode(')',$accion->nombre)[0])[1] }}</th>
            <td>{{ $numBenef }}</td>
            <td> <div class="form-group"><input type="number" min="0" value="{{ $dotacion }}" required /></div></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </form>
</div>
@if (!isset($footernull))
  <div class="modal-footer">
      <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
      <button class="btn pull-right btn-success" onclick="filter()"><span><i class="fa fa-save"></i> Guardar</span></button>
  </div>
  <script type="text/javascript">
    (function(){
      validacion.iniciar('form-dotacion')
    })()
    function filter() {
      array = []
      object = $('#form-dotacion tbody tr')
      $.each( object, function( key, value ) {
        array.push({
          tipoaccion_id:          value.children[0].id,
          cantidad_beneficiarios: value.children[1].textContent,
          cantidad_dotaciones:    value.children[2].children[0].children[0].value
        })
      }).promise().done( function(){
        console.log(array)
        transaccion.formAjax('{{route("alimentarios.dotaciones.uall",[$requisicionProgramacion->requisicion->id,$requisicionProgramacion->programacion->id])}}','POST','form-dotacion',{dotaciones:JSON.stringify(array)},false)
      })
    }
  </script>
@else
  <script type="text/javascript">
    (function(){
      $('#form-dotacion input').prop('readonly',true)
    })()
  </script>
@endif