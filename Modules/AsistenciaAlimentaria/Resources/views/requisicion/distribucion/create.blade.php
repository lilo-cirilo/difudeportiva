@extends('asistenciaalimentaria::layouts.master')

@section('content-subtitle', 'Tabla de selección de programaciones')

@push('head')
@endpush

@section('content')
<div class="box box-primary">
    <div class="box-header with-border text-center">
        <h4>SELECCIÓN DE PROGRAMACIONES PARA <strong class="font-italic">{{$requisicion->programa->nombre}}</strong>  DE LA SIG. REQUISICIÓN:</h4>
        <div class="container">
            <div class="row">
                <div class="col-sm"><h5><strong class="font-italic">NÚMERO DE LICITACIÓN: </strong>  {{$requisicion->licitacion->num_licitacion or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">BIMESTRE: </strong>  {{$requisicion->num_bimestre or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">TIPO DE ENTREGA: </strong>  {{$requisicion->num_entrega or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">NÚMERO DE OFICIO: </strong>  {{$requisicion->num_oficio or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">PERIODO: </strong>  {{$requisicion->periodo or ''}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">¿ESPECIAL? </strong>  {{$requisicion->especial == 1 ? 'SI' : 'NO'}}</h5></div>
                <div class="col-sm"><h5><strong class="font-italic">ESTADO </strong>  {{$requisicion->estadoprograma->estado->nombre or ''}}</h5></div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header font-weight-bold" title="disponibles para selección por que cumplen con beneficiarios, miembros del comité que reciben dotaciones, convenio y son activas-bloqueadas">
            <i class="fa fa-align-justify"></i> Programaciones completas

            <div class="card-header-actions" style="padding-right: 20px;">
                @if ($requisicion->programa->id == config('asistenciaalimentaria.sujetosId'))
                <div class="row">
                    <label style="padding-right: 5px;" for="desarrollo">Solo desarrollo</label> 
                    <label class="switch switch-label switch-outline-success-alt switch-sm">
                        <input id="desarrollo" class="switch-input" type="checkbox" >
                        <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                    </label>
                </div>
                {{-- Solo desarrollo <input type="checkbox" name="desarrollo" id="desarrollo"> --}}
                @endif
                {{-- <a class="card-header-action" href="https://datatables.net" target="_blank">
                <small class="text-muted">docs</small>
                </a> --}}
            </div>
        </div>
        <div class="card-body">
            {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'seleccion'], true) !!}
        </div>
    </div>
</div>
@stop
{{-- @include('asistenciaalimentaria::requisicion.distribucion.info') --}}
@push('body')
	{!! $dataTable->scripts() !!}
	<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        /* var table = $('#seleccion').DataTable();
 
        table.on( 'select', function ( e, dt, type, indexes ) {
            console.log('select');
            data = table[ type ]( indexes ).nodes().to$();
            console.log(data);
        } );

        table.on( 'deselect', function ( e, dt, type, indexes ) {
            console.log('deselect');
        } ); */
        return {
            recargar : recargar
		}})('#seleccion');
    //exito en las operaciones anteriores
    function ajaxSuccess(params) {
        tabla.recargar();
        const Toast = Swal.mixin({
            toast: true,
            position: 'center',
            showConfirmButton: false,
            timer: 3000
        })
        Toast.fire({
            type: 'success',
            title: 'Proceso completado exitosamente'
        })
    }
    //fallo en las operaciones anteriores
    function ajaxError(params) {
        tabla.recargar();
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
        })
        Toast.fire({
            type: 'error',
            title: 'Algo salió mal'
        })
    }
  </script>
@endpush