<div class="modal animated bounce hide" id="{{$tipo}}">
  <div class="modal-dialog modal-dialog-centered {{ isset($lg) ? ($lg ? 'modal-lg' : 'modal-sm') : 'modal-full'}}">
    <div class="modal-content">
      @include($vista)
    </div>
  </div>
</div>
<script>
  (function() {
    var modalAnimated = $('#{{$tipo}}')
    var animatedIn = 'bounce'
    var animatedOut = 'zoomOut'
    modalAnimated.on('show.bs.modal', function () {
      var closeModalBtns = modalAnimated.find('button[data-custom-dismiss="modal"]');
      closeModalBtns.one('click', function() {
        modalAnimated.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function( evt ) {
          modalAnimated.modal('hide')
        });
        modalAnimated.removeClass(animatedIn).addClass(animatedOut);
      })
    })

    modalAnimated.on('hidden.bs.modal', function ( evt ) {
      var closeModalBtns = modalAnimated.find('button[data-custom-dismiss="modal"]');
      modalAnimated.removeClass(animatedOut).addClass(animatedIn)
      modalAnimated.off('webkitAnimationEnd oanimationend msAnimationEnd animationend')
      closeModalBtns.off('click')
    })

    $(':text').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  })()
</script>