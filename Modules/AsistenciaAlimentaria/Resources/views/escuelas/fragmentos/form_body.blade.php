<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="escuela_id">Escuela*</label>
            <select id="escuela_id" name="escuela_id" class="form-control select2 req" style="width: 100%" {{ isset($alimentario) ? ($alimentario->desayuno ? '' : 'disabled') : '' }} required>
							@if (isset($alimentario->desayuno))
									<option value="{{ $alimentario->desayuno->escuela->id }}">{{ $alimentario->desayuno->escuela->nombre }}</option>
							@endif
						</select>
        </div>
		</div>
		<div id="escuelaPadre" class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
			<div class="form-group">
					<input id="checkEscuelaPadre" type="checkbox" {{ isset($alimentario) ? (isset($alimentario->desayuno) ? ($alimentario->desayuno->escuela->extension == 1 ? 'checked' : '') : '') : '' }}>
					<label for>
							Extensión de
					</label>
					<select id="padre_id" name="padre_id" class="form-control select2 req" style="width: 100%" {{ isset($alimentario) ? (isset($alimentario->desayuno) ? ($alimentario->desayuno->escuela->extension == 1 ? '' : 'disabled') : 'disabled') : 'disabled' }} required>
							@if (isset($alimentario->desayuno))
								@if (isset($alimentario->desayuno->escuela->padre))
									<option value="{{ $alimentario->desayuno->escuela->padre->id }}">{{ $alimentario->desayuno->escuela->padre->nombre }}</option>
								@endif
							@endif
					</select>
			</div>
		</div>
    {{-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="tipodistribucion_id">Tipo de distribución*</label>
            <select id="tipodistribucion_id" name="tipodistribucion_id" class="form-control select2 req" style="width: 100%">
                @foreach ($distribuciones as $distribucion)
                    <option value="{{ $distribucion->id }}">{{ $distribucion->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div> --}}
</div>