<section class="content">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">{{ isset($data) ? 'Actualizar escuela' : 'Registrar escuela'}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form" id="form-escuela">
                @include('asistenciaalimentaria::alimentarios.fragmentos.form_body')
                @include('asistenciaalimentaria::escuelas.fragmentos.form_body')
            </form>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <label>Los campos con * son obligatorios</label>
            <button class="btn pull-right button-dt tool" tabindex="0" onclick="Verify.transaction('{{ isset($data) ? route('alimentarios.desayunos.update') : route('alimentarios.desayunos.store')}}', '{{ isset($data) ? 'PUT' : 'POST' }}',null,'form-escuela')"><span><i class="fa fa-save"></i> Guardar</span></button>
        </div>
    </div>
</section>