
<div class="modal-header mh-bordered">
  <h3 class="modal-title">Registrar convenio {{ isset($convenio->programacion->alimentario->folio) ? 'para alimentario '.$convenio->programacion->alimentario->folio : ''  }}</h3>
</div>
{{-- modal-header --}}
<div class="modal-body">
  <form role="form" id="form-convenios">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
                <label for="tipoconvenio_id">Tipo de convenio*</label>
                <select id="tipoconvenio_id" name="tipoconvenio_id" class="form-control select2" style="width: 100%" required>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">
                <label for="fecha_convenio">Fecha de convenio*</label>
                <input type="text" id="fecha_convenio" name="fecha_convenio" class="form-control date-picker" placeholder="Seleccione fecha de convenio" readonly required>
            </div>
        </div>
    </div>{{-- datos convenio --}}
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <label for="ubicacion_digital_archivo">Adjuntar archivo digital*</label>
          <input id="ubicacion_digital_archivo" name="ubicacion_digital_archivo" type="file" class="file" data-show-preview="false">
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="form-group">
              <label for="ubicacion_fisica_archivo">Ubicación física del archivo</label>
              <input type="text" id="ubicacion_fisica_archivo" name="ubicacion_fisica_archivo" class="form-control" placeholder="Ingrese fila, columna y pasillo" value="{{ isset($convenio->ubicacion_fisica_archivo) ? 'para alimentario '.$convenio->ubicacion_fisica_archivo : ''  }}">
          </div>
      </div>
    </div>{{-- datos de archivo --}}
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <label><input name="tiene_acta_aceptacion" id="tiene_acta_aceptacion" type="checkbox" class="checkModal" required></label>
          <label>¿Tiene acta de aceptación?</label>
        </div>
      </div>
    </div>{{-- info de actas 1 --}}
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
          <label><input name="tiene_acta_comite" id="tiene_acta_comite" type="checkbox" class="checkModal" required></label>
          <label>¿Tiene acta de comité?</label>
        </div>
      </div>
    </div>{{-- info de actas 2 --}}
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">  
        <div class="form-group">
          <label><input name="tiene_acta_contraloria" id="tiene_acta_contraloria" type="checkbox" class="checkModal"></label>
          <label>¿Tiene acta de contraloria?</label>
        </div>  
      </div>
    </div>{{-- info de actas 3 --}}
  </form>
</div>
{{-- modal-body --}}
<div class="modal-footer">
  <label class="mr-auto">Los campos con * son obligatorios</label>
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal"><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  <button class="btn float-right btn-primary" tabindex="0" onclick="transaccion.formAjax( '{{ route('alimentarios.programacion.convenios.store',$programacion_id) }} ','POST','form-convenios',{},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
</div>
<script>
  (function () {
    $('.checkModal').iCheck({
      checkboxClass: "icheckbox_square-green",
      increaseArea: "10%"
    })
    $('#tipoconvenio_id').select2({
      language: 'es',
      placeholder : 'Seleccione',
      ajax: {
          url: "{{ route('alimentarios.tiposconvenio') }}",
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: (params) => {
              return {
                  search: params.term
              }
          },
          processResults: (data, params) => {
              params.page = params.page || 1
              return {
                  results: $.map(data, (item) => {
                      return {
                          id: item.id,
                          text: item.nombre,
                          slug: item.nombre,
                          results: item
                      }
                  })
              }
          },
          cache: true
      }
    })
    $('#ubicacion_digital_archivo').fileinput({
      dropZoneEnabled: true,
      showPreview: false,
      showRemove: false,
      showUpload: false,
      language: 'es',
      allowedFileExtensions: ["jpg", "png", "jpeg"]
    })
    $('#fecha_convenio').datepicker({
      autoclose: true,
      language: 'es',
      format: 'yyyy/mm/dd',
      startDate: moment('{{date('Y')}}/01/01').format('YYYY/MM/DD'),
      endDate: moment().format('YYYY/MM/DD')
    })
    validacion.iniciar('form-convenios')
  })()
  
</script>