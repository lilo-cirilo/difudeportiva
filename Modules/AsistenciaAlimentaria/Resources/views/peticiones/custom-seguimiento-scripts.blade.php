<script type="text/javascript" src="{{ asset('js/asistenciaalimentaria/validaciones.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/asistenciaalimentaria/transacciones.js') }}"></script>
<script>
  $('#checkSubLocalidad').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  }).on('ifChanged', function (e) {
    if(e.target.checked) {
      $('#sublocalidad').prop('disabled',false)
      $('#sublocalidad').attr('required',true)
    } else {
      $('#sublocalidad').prop('disabled',true)
      $('#sublocalidad').attr('required',false)
      $('#sublocalidad').val('')
    }
  })
  $("#localidad_id").select2({disabled:true})
  $('#municipio_id').select2({
    language: 'es',
    minimumInputLength: 2,
    allowClear : true,
    placeholder : 'Seleccione un municipio',
    ajax: {
      url: '{{ route('municipios.select') }}',
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        }
      },
      cache: true
    }
  }).change((event) => {
    $('#municipio_id').valid()
    $('#localidad_id').empty()
    $('#localidad_id').prop('disabled',false)
    $('#localidad_id').select2({
      language: 'es',
      allowClear : true,
      placeholder : 'Seleccione una localidad',
      ajax: {
        url: '{{ route('localidades.select') }}',
        delay: 500,
        dataType: 'JSON',
        type: 'GET',
        data: (params) => {
          return {
            search: params.term,
            municipio_id: $('#municipio_id').val()
          }
        },
        processResults: (data, params) => {
          params.page = params.page || 1
          return {
            results: $.map(data, (item) => {
              return {
                id: item.id,
                text: item.nombre,
                slug: item.nombre,
                results: item
              }
            })
          }
        },
        cache: true
      }
    }).change((event) => {
      $('#localidad_id').valid()
    })
  })
  $('#financiamiento_id').select2({
    language: 'es',
    allowClear : true,
    placeholder : 'Seleccione una opción',
    ajax: {
      url: "{{ route('alimentarios.tiposfinanciamiento') }}",
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        }
      },
      cache: true
    }
  })

  validacion.iniciar('form-cocinas')

  function ajaxSuccess(response) {
    console.log(response)
    $.ajax({
      url: window.location.href,
      type: 'PUT',
      data : {
        'estatus' : 8,
        'observacion' : `Se atendio la peticion generando la cocina: ${response.folio}`
      },
      success: function(response2) {
        unblock();
        Swal.fire({
          title: 'Peticion Atendida',				
          text: `Se atendio la peticion generando la cocina: ${response.folio}`,
          type: 'success',
          allowEscapeKey: false,
          allowOutsideClick: false,
          showConfirmButton : true
        });
      },
      error: function(response){
        unblock()
        if(response.status === 422) {                        
          Swal.fire({                                                                                                                                                                                                                                                                                                                               
            title: 'Error al cambiar el estatus',				
            text: response.responseJSON.errors[0].message,
            type: 'error',
            allowEscapeKey: false,
            allowOutsideClick: false
          });
        }else{
          Swal.fire(
          'Error',
          '',
          'error'
          )
        }                                
      }
    });
  }
  function ajaxError(response) {
    unblock()
    if(response.status == 409) {
      Swal.fire({
        title: '¡Transacción rechazada!',
        text: 'Valor duplicado',
        type: 'error'
      })
    } else {
      Swal.fire({
        title: '¡Transacción rechazada!',
        text: 'No se pudo finalizar la petición',
        type: 'error'
      })
    }
  }
  function procesarAlimentario(url,metodo) {
    if(validacion.revisar()){
      transaccion.formAjax(url,metodo,'form-cocinas')
    }
  }
</script>