<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
          <label for="nombre_asociacion">Nombre de asociación*</label>
          <input type="text" id="nombre_asociacion" name="nombre_asociacion" class="form-control req" maxlength="100" placeholder="Ingrese nombre de la asociación" value="{{ $alimentario->sujeto->nombre_asociacion or '' }}">
      </div>
  </div>
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <div class="form-group">
          <label for="oficio">Oficio</label>
          <input type="text" id="oficio" name="oficio" class="form-control req" maxlength="30" placeholder="Ingrese oficio" value="{{ $alimentario->sujeto->oficio or '' }}">
      </div>
  </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <input id="checkDesarrollo" name="esdesarrollo" type="checkbox" {{ isset($alimentario->sujeto) ? ($alimentario->sujeto->esdesarrollo == 1 ? 'checked' : '') : '' }}>
            <label for>
                ¿Pertenece al departamento de desarrollo?
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
        <input id="checkAmpliacion" name="esampliacion" type="checkbox" {{ isset($alimentario) ? ($alimentario->esampliacion == 1 ? 'checked' : '') : 'checked' }}>
            <label for>
                ¿Ampliacion?
            </label>
        </div>
    </div>
</div>