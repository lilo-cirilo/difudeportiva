<div class="modal-header">
  <h4 class="modal-title">Editar Usuario <i class="fas fa-user-edit"></i></h4>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body with-border">
  <form data-toggle="validator" role="form" id="edit-user" class="needs-validation">
    <div class="row">
      <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="rol_id" class="center-block">*Rol:</label>
          <select id="rol_id" name="rol_id" class="form-control select2" style="width: 100%;" required>
          </select>
        </div>
      </div>
    </div>
    <div class="row" id="region">
      <div class="col-xs-12 col-md-12 col-lg-12">
        <div class="form-group">
          <label for="region_id" class="center-block">*Región:</label>
          <select id="region_id" name="region_id" class="form-control select2" style="width: 100%;">
          </select>
        </div>
      </div>
    </div>
  </form>
</div>
<div class="modal-footer">
  <span class="mr-auto">Los campos con * son obligatorios</span>
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  <div id="btn-save-bf"><button class="btn float-right btn-primary" onclick="transaccion.formAjax('{{ route('alimentarios.usuarios.update',$usuario) }}','PUT','edit-user')"><span><i class="fa fa-save"></i> Guardar</span></button></div>
</div>
<script>
  (function() {
    function initComponets() {
      $('.select2').select2({
        allowClear : true,
        language: 'es',
        placeholder : 'SELECCIONE ...',
        minimumResultsForSearch: Infinity
      });
      $('#region_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        ajax: {
          url: '{{ route('alimentarios.usuarios.create') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#region_id').valid();
      });
      $('#rol_id').select2({
        language: 'es',
        placeholder : 'SELECCIONE ...',
        ajax: {
          url: '{{ route('alimentarios.usuarios.index') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#rol_id').valid();
        let rol = $('#rol_id').select2('data')[0].results.nombre
        if(rol != 'SUPERADMIN') {
          $('#region').show()
        } else {
          $('#region').hide()
        }
      })
    }
    initComponets()
    validacion.iniciar('edit-user')
  })()
</script>