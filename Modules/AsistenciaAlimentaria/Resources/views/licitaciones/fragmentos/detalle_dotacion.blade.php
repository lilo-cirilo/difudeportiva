
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="producto_id">*Producto:</label>
            <select id="producto_id" name="producto_id" class="form-control select2" style="width: 100%">
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="precio_producto">*Precio producto:</label>
            <input type="text" name="precio_producto" id="precio_producto" class="form-control" placeholder="Ingresar precio del producto">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="unidad_medida">*Unidad de medida:</label>
            <input type="text" name="unidad_medida" id="unidad_medida" class="form-control" placeholder="Ingresar unidad de medida">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="cantidad">*Cantidad:</label>
            <input type="text" name="cantidad" id="cantidad" class="form-control" placeholder="Ingresar cantidad de productos">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="presentacion">*Presentación:</label>
            <input type="text" name="presentacion" id="presentacion" class="form-control" placeholder="Ingresar una descripción">
        </div>
    </div>
</div>