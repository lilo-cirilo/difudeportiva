
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="programa_id">*Programa(s):</label>
            <select id="programa_id" name="programa_id" class="form-control select2" style="width: 100%">
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="precio_dotacion">*Precio dotación:</label>
            <input type="text" name="precio_dotacion" id="precio_dotacion" class="form-control" placeholder="Ingresar precio de dotación">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="precio_adquirido">*Precio adquirido:</label>
            <input type="text" name="precio_adquirido" id="precio_adquirido" class="form-control" placeholder="Ingresar precio adquirido">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="precio_cuota">*Precio cuota:</label>
            <input type="text" name="precio_cuota" id="precio_cuota" class="form-control" placeholder="Ingresar precio por cuota">
        </div>
    </div>
</div>