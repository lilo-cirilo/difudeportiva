
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="region_id">*Región:</label>
            <select id="region_id" name="region_id" class="form-control select2" style="width: 100%">
            </select>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="fecha_oficio">*Fecha oficio:</label>
            <input type="text" name="fecha_oficio" id="fecha_oficio" class="form-control" placeholder="Ingresar fecha de oficio">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="no_oficio">*Número de oficio:</label>
            <input type="text" name="no_oficio" id="no_oficio" class="form-control" placeholder="Ingresar número de oficio">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="tipo_oficio">*Tipo de oficio:</label>
            <select name="tipo_oficio" id="tipo_oficio" class="form-control select2" style="width:100%;"></select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="no_referencia">*Número de referencia:</label>
            <input type="text" name="no_referencia" id="no_referencia" class="form-control" placeholder="Ingresar número de referencia">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="inversion_oficio">*Inversión de oficio:</label>
            <input type="text" name="inversion_oficio" id="inversion_oficio" class="form-control" placeholder="Ingresar inversión de oficio">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="precio_dotacion_oficio">*Precio de dotación del oficio:</label>
            <input type="text" name="precio_dotacion_oficio" id="precio_dotacion_oficio" class="form-control" placeholder="Ingresar precio de dotación del oficio">
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group">
            <label for="no_dot_oficio">*Número de dotaciones del oficio:</label>
            <input type="text" name="no_dot_oficio" id="no_dot_oficio" class="form-control" placeholder="Ingresar númerode dotaciones del oficio">
        </div>
    </div>
</div>