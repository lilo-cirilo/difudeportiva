<div class="modal-header">
    <h4 class="modal-title">Registrar dotación</h4> <i class="fa fa-shopping-basket"></i>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form data-toggle="validator" role="form" id="form-dotaciones" class="needs-validation">       
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="programa">*Programa(s):</label>
                    <select id="programa" class="form-control select2" style="width: 100%">
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="tipoaccion_id">*Sub-programa(s):</label>
                    <select id="tipoaccion_id" name="tipoaccion_id" class="form-control select2" style="width: 100%">
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="form-group">
                    <label for="cuota_recuperacion">*Cuota de recuperación:</label>
                    <input type="number" name="cuota_recuperacion" id="cuota_recuperacion" class="form-control" placeholder="Ingresar cuota de recuperación" required>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-dismiss="modal" aria-hidden="true" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    <button class="btn btn-primary" onclick="transaccion.formAjax('{{ route('alimentarios.dotacion.store') }}','POST','form-dotaciones')">Guardar</button>
</div>
<script type="text/javascript">
    (function(){
        validacion.iniciar('form-dotaciones')
        $('.select2').select2({
            allowClear : true,
            language: 'es',
            placeholder : 'SELECCIONE ...',
            minimumResultsForSearch: Infinity
        });
        $('#programa').select2({
            language: 'es',
            placeholder : 'SELECCIONE ...',
            ajax: {
                url: '{{ route('programa.subprogramas',config("asistenciaalimentaria.fatherId")) }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.nombre,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            $('#programa').valid();
            $('#tipoaccion_id').empty()
            $('#tipoaccion_id').select2({
                language: 'es',
                placeholder : 'SELECCIONE ...',
                ajax: {
                    url: '{{ URL::to("alimentarios/subprogramas") }}'+'/'+$('#programa').val()+'/tiposaccion',
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(function(event) {
                $('#tipoaccion_id').valid();
            });
        });
    })()
</script>