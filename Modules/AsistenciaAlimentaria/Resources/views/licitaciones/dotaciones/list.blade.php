<div class="modal-header">
    <h4 class="modal-title">Elegir dotaciones {{ $licitacion->num_licitacion }}</h4> <i class="fa fa-shopping-basket"></i>
    <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
        <input type="text" id="search_dot" name="search_dot" class="form-control">
        <div class="input-group-append">
            <button class="btn btn-secondary" id="btn_buscar_dot" name="btn_buscar_dot">Buscar</button>
        </div>
    </div>
    {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'dotaciones']) !!}
</div>
<div class="modal-footer">
    <button class="btn btn-secondary mr-auto" tabindex="0" data-dismiss="modal" aria-hidden="true" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
</div>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    var tabla = ((tablaId) => {
        $('#search_dot').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search_dot').val()).draw()
            }
        })
        $('#btn_buscar_dot').on('click', function() {
            $(tablaId).DataTable().search($('#search_dot').val()).draw()
        })
        $(tablaId).DataTable().on( "user-select", function ( e, dt, type, cell, originalEvent ) {
            if ( $(originalEvent.target).index() === 0 ) {
                e.preventDefault();
            } else {
                if (type === 'row' && !$(originalEvent.target).closest('tr').hasClass('selected')) {
                    var data = $(tablaId).DataTable().rows( [cell[0][0].row] ).data();
                    Swal.fire({
                        title: 'Ingresa cantidad de '+data.pluck('programa')[0]+', '+data.pluck('subprograma')[0].split('(')[1].split(')')[0],
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        cancelButtonText: 'Cancelar',
                        confirmButtonText: 'Guardar',
                        showLoaderOnConfirm: true,
                        preConfirm: (cantidad) => {
                            if(isNaN(parseInt(cantidad))) {
                                alert('Sólo un número entero')
                                $('tr').removeClass("selected");
                            }
                            transaccion.miniAjax('{{route("alimentarios.licitacion.dotaciones.store",$licitacion->id)}}' ,'POST',{cantidad:cantidad,dotacion_id:data.pluck('dotacion_id')[0]},'json',true,'application/x-www-form-urlencoded; charset=UTF-8','dotaciones')
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    })
                }
            }
        });
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false)
        }
        return {
            recargar : recargar
        }
    })('#dotaciones')
</script>