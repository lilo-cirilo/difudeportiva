<div class="modal-header">
  <h5 class="modal-title">
    Agregar proveedor
    <i class="fas fa-suitcase"></i>
  </h5>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body">
  @include('proveedores.form')
</div>
<div class="modal-footer">
  <button class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  <button type="button" class="btn btn-primary" onclick="transaccion.formAjax('{{ route('proveedor.store') }}','POST','form-proveedor')">Guardar</button>
</div>
<script type="text/javascript">
  (function(){
    validacion.iniciar('form-proveedor',{rfc:{pattern_rfc:''}})
    $('#banco_id').select2({
      language: 'es',
      placeholder : 'SELECCIONE ...',
      ajax: {
      url: '{{ route('bancos.select') }}',
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: function(params) {
          return {
          search: params.term
          };
      },
      processResults: function(data, params) {
          params.page = params.page || 1;
          return {
          results: $.map(data, function(item) {
              return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
              }
          })
          };
      },
      cache: true
      }
  }).change(function(event) {
      $('#banco_id').valid();
  });
  })()
</script>