<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      @if (auth()->user()->hasRoles(['SUPERADMIN']))
        <li class="nav-item">
          <a class="nav-link" href="{{ route('alimentarios.home') }}">
            <i class="nav-icon fas fa-home"></i> Home
          </a>
        </li>
      @endif
      @if (auth()->user()->hasRoles(['ADMINISTRADOR','CAPTURISTA','REGIONAL']))
        @php
            $programas = [
                (object)['nombre'=>'Cocinas','prefijo'=>'ccnc','id'=>config('asistenciaalimentaria.ccncId'), 'icon' => 'fas fa-utensils'],
                (object)['nombre'=>'Desayunos','prefijo'=>'desayunos','id'=>config('asistenciaalimentaria.desayunosId'), 'icon'=>'fas fa-school'],
                (object)['nombre'=>'Sujetos','prefijo'=>'sujetos','id'=>config('asistenciaalimentaria.sujetosId'), 'icon'=>'fas fa-female']
            ]
        @endphp
        @foreach ($programas as $programa)
            @if ( (auth()->user()->persona->empleado->area->id == config('asistenciaalimentaria.areaDesarrolloId') && $programa->prefijo=='sujetos')
                || auth()->user()->persona->empleado->area->id != config('asistenciaalimentaria.areaDesarrolloId') )
                <li class="nav-divider"></li>
                <li class="nav-title">{{ $programa->nombre }}</li>
                <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle {{ $programa->nombre }}">
                    <i class="nav-icon {{ $programa->icon }}"></i>{{ $programa->nombre }}</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle {{ $programa->nombre }}">
                        <i class="nav-icon fas fa-star"></i>Programacion</a>
                    <ul class="nav-dropdown-items">
                        {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route("alimentarios.$programa->prefijo.programacion.index") }}">
                            <i class="nav-icon fas fa-list-alt"></i>Lista de ejercicios</a>
                        </li> --}}
                        <li class="nav-item">
                        <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.$programa->prefijo.programacion.show",config('asistenciaalimentaria.anioActual')) }}">
                            <i class="nav-icon fas fa-star"></i>Programación actual</a>
                        </li>
                    </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle {{ $programa->nombre }}" href="#">
                            <i class="nav-icon fas fa-archive"></i>Consultar</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.$programa->prefijo.index") }}">
                                <i class="nav-icon fas fa-utensils"></i>Alimentarios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.programa.beneficiarios",config("asistenciaalimentaria.{$programa->prefijo}Id")) }}">
                                <i class="nav-icon fas fa-laugh-beam "></i>Beneficiarios</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.programa.comites",config("asistenciaalimentaria.{$programa->prefijo}Id")) }}">
                                <i class="nav-icon fas fa-laugh-beam "></i>Comites</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.cuotasrecuperacion",$programa->id) }}">
                                <i class="nav-icon fas fa-cash-register"></i> Cuotas recuperación</a>
                            </li>
                        {{-- <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="nav-icon fas fa-file"></i>Convenios</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="nav-icon fas fa-users"></i>Comités</a>
                        </li> --}}
                        
                        </ul>
                    </li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link nav-dropdown-toggle {{ $programa->nombre }}" href="#">
                            <i class="nav-icon fas fa-box"></i>Requisiciones</a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a href="{{ route("alimentarios.$programa->prefijo.requisiciones.index") }}" class="nav-link {{ $programa->nombre }}">
                                    <i class="nav-icon fas fa-dolly"></i>Lista de requisiciones</a></li>
                            <li class="nav-item">
                                <a href="{{ route("alimentarios.programa.distribuciones",$programa->id) }}" class="nav-link {{ $programa->nombre }}">
                                    <i class="nav-icon fas fa-truck"></i>Lista de distribuciones</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link {{ $programa->nombre }}" href="{{ route("alimentarios.$programa->prefijo.validacion.oficios.index") }}">
                        <i class="nav-icon fas fa-tasks"></i>Validación</a>
                    </li>
                </ul>
                </li>
            @endif
        @endforeach
      @endif
      @if (auth()->user()->hasRoles(['SUPERADMIN']))
        <li class="nav-divider"></li>
        <li class="nav-title">General</li>
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle">
            <i class="nav-icon fas fa-dollar-sign "></i>Licitaciones</a>
            <ul class="nav-dropdown-items">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('alimentarios.licitaciones.index') }}">
                    <i class="nav-icon fas fa-list-alt"></i>Lista de licitaciones</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('alimentarios.dotacion.index') }}">
                    <i class="nav-icon fas fa-boxes"></i>Lista dotaciones</a>
                </li>
            </ul>
        </li>
        <li class="nav-item mt-auto">
            <a class="nav-link nav-link-info" href="{{ route('alimentarios.configuracion.index') }}" target="_top">
            <i class="nav-icon fas fa-cogs"></i> Configuración</a>
        </li>
      @endif
      @if (auth()->user()->hasRoles(['PROVEEDOR']))
        <li class="nav-title">PROVEEDOR</li>
        <li class="nav-item">
            <a class="nav-link"
                href="{{ route('alimentarios.proveedor.requisiciones.index',config('asistenciaalimentaria.anioActual')) }}">
                <i class="nav-icon fas fa-clipboard-list"></i> Lista de requisiciones</a>
        </li>
        <li class="nav-item">
            <a class="nav-link"
                href="{{ route('alimentarios.proveedor.ordencarga.index',1) }}">
                <i class="nav-icon fas fa-shipping-fast"></i> Lista de ordenes de carga</a>
        </li>
      @endif
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>