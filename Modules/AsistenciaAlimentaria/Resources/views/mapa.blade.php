@extends('asistenciaalimentaria::layouts.master')
@push('head')
<style>
  .container-fluid {
    height: calc(100% - 75px) !important;
  }

  .mapa {
    margin-top: 30px !important;
  }

  .card {
    height: 100% !important;
  }

  .card-body {
    padding: 0;
  }

  label {
    margin-bottom: 0 !important;
  }

  .dropdown-item {
    padding: 10px 10px !important;
    justify-content: space-between !important;
  }
</style>
@endpush
@section('content')
<div class="row">
  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-primary">
      <div class="card-body pb-0">
        <div class="text-value text-center">
          <h1>1473</h1>
        </div>
        <div>
          <h2 class="text-center">COCINAS COMEDOR NUTRICIONAL COMUNITARIA</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-info">
      <div class="card-body pb-0">
        <div class="text-value text-center">
          <h1>1740</h1>
        </div>
        <div>
          <h2 class="text-center">DESAYUNOS ESCOLARES FRIOS</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-warning">
      <div class="card-body pb-0">
        <div class="text-value text-center">
          <h1>1109</h1>
        </div>
        <div>
          <h2 class="text-center">ASISTENCIA ALIMENTARIA A SUJETOS VULNERABLES CON CARENCIA ALIMENTARIA</h2>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-success">
      <div class="card-body pb-0">
        <div class="text-value text-center">
          <h1>314185</h1>
        </div>
        <div>
          <h2 class="text-center">Personas beneficiadas</h2>
        </div>
      </div>
    </div>
  </div>

</div>
<div class="card mapa">
  <div class="card-header with-border">
    <h3 class="box-title">Direccion de Asistencia Alimentaria</h3>
  </div>
  <div class="card-body shadow">
    <div id="map" style="height: 100%;"></div>
  </div>
</div>
<div style="display:none">
  <div id="controles" class="d-flex justify-content-around " style="padding-top: 10px;">
    <div class="btn-group" role="group" style="height: 40px; width: 35%; min-width:160px">
      <div class="btn-group" role="group">
        <button id="busqueda" class="btn bg-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false" title="Busqueda">
          <i class="fas fa-search"></i><span class="caret"></span>
        </button>
        <ul class="dropdown-menu map-control">
          <li>
            <input type="text" id="municipios" class="form-control" placeholder="Buscar ...">
          </li>
        </ul>
      </div>
      <div class="btn-group" role="group">
        <button class="btn bg-white dropdown-toggle" id="dropdownMenuButton" type="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false"><i class="fas fa-filter"></i></button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" x-placement="bottom-start"
          style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 35px, 0px);">
          @foreach (array_keys((array)json_decode($regiones)) as $region)
          <label class="dropdown-item d-flex align-items-center"
            for="{{ mb_strtolower(str_replace(' ','',$region)) }}">{{ ucfirst(mb_strtolower($region)) }}
            <label class="switch switch-label switch-danger switch-sm">
              <input class="switch-input sw-region" type="checkbox" checked
                id="{{ mb_strtolower(str_replace(' ','',$region)) }}">
              <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
            </label>
          </label>
          @endforeach
        </div>
      </div>
      <div class="btn-group" role="group">
        <button class="btn bg-white dropdown-toggle" id="showOptions" type="button" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false"><i class="fas fa-eye"></i></button>
        <div class="dropdown-menu" aria-labelledby="showOptions" x-placement="bottom-start">
          @foreach (array_keys((array)json_decode($puntos)) as $programa)
          <label class="dropdown-item d-flex align-items-center"
            for="{{ strtolower($programa) }}">{{ ucfirst($programa) }}
            <label class="switch switch-label switch-sm switch-danger">
              <input class="switch-input sw-programa" type="checkbox" id="{{ strtolower($programa) }}">
              <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
            </label>
          </label>
          @endforeach
          <label class="dropdown-item d-flex align-items-center" for="marcador-mapa_calor">Marcador/Mapa de calor
            <label class="switch switch-label switch-sm switch-danger">
              <input class="switch-input" type="checkbox" id="marcador-mapa_calor">
              <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
            </label>
          </label>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@push('body')
<script type="text/javascript"
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDpYJe13cGrxH5Hk-9u5fu4PFQI-YMD5M&libraries=visualization">
</script>
<script type="text/javascript" src="{{ asset('js/oax_map.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/coordMunicipios.js') }}"></script>
<script type="text/javascript">
  var map,oaxaca,infoWindow           //mapa, forma de oaxaca, ventana de informacion
	var munXregion = {!! $regiones !!}  //separa los municipios de cada region
	var Regiones = {}                   //almacena el dibujo de los municipios, por region.
	var puntos = @json(json_decode($puntos)); //las hubiacaciones de cada cocina
	var cocinas,casas, escuelas         //los objetos mapas de caor
	var cmarks = [],emarks = [],smarks = [] //arreglo de cada marcador por programa
  var mmomc = false                   //abreviacion de 'mostrar mascadores ocultar mapa calor' true muestra marcadores, false muestra el mapa de calor
	function initMap() {
			map = new google.maps.Map(document.getElementById('map'), {
					zoom   : 6,//7.75,
					center : {lat : 17.15727971021128, lng: -96.23866181640627 },
					minZoom: 6,//7.75,
					styles : [
							{
									"featureType": "administrative.country",
									"elementType": "geometry.fill",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.country",
									"elementType": "geometry.stroke",
									"stylers": [
									{
											"visibility": "on"
									}
									]
							},
							{
									"featureType": "administrative.country",
									"elementType": "labels.text",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.province",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.province",
									"elementType": "geometry.fill",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.locality",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.locality",
									"elementType": "labels",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "administrative.neighborhood",
									"elementType": "labels.text",
									"stylers": [
									{
											"visibility": "on"
									}
									]
							},
							{
									"featureType": "landscape",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "landscape",
									"elementType": "geometry.stroke",
									"stylers": [
									{
											"visibility": "off"
									},
									{
											"saturation": "1"
									}
									]
							},
							{
									"featureType": "landscape.man_made",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "landscape.man_made",
									"elementType": "labels",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "landscape.natural",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "landscape.natural",
									"elementType": "geometry.fill",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "landscape.natural",
									"elementType": "labels",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "poi",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "road",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "transit",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "off"
									}
									]
							},
							{
									"featureType": "water",
									"elementType": "all",
									"stylers": [
									{
											"visibility": "on"
									}
									]
							}
					],
					disableDefaultUI: true,
					mapTypeControl: false,
					zoomControlOptions: {
							position: google.maps.ControlPosition.RIGHT_CENTER
					},
					fullscreenControlOptions:{
							position: google.maps.ControlPosition.TOP_RIGHT
					},
					scaleControl: false,
					streetViewControl: false,
					rotateControl: false,
					fullscreenControl: true,
					zoomControl: true,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			})
			google.maps.event.addListenerOnce(map,'idle',limitarMapa)
	}
	function limitarMapa() {
			var zoom = 0;
			if (oaxaca != null)
					oaxaca.setMap(null)
			if (document.fullscreenElement || document.webkitFullscreenElement || document.mozFullscreenElement || document.MSFullscreenElement){
					zoom = ajustar($(document).width())
			}else {
					zoom = ajustar($('#map').width())
			}
			//obteniendo las coordenadas del mapa alejando(con zoom de 6)
			// map.setOptions({zoom:6, minZoom: 6, center: {lat : 17.15727971021128, lng: -96.23866181640627 }})
			// map.setZoom(6)
			let ne = map.getBounds().getNorthEast()
			let sw = map.getBounds().getSouthWest()
			let limites = {
					maxLat : ne.lat(),
					maxLng : ne.lng(),
					minLat : sw.lat(),
					minLng : sw.lng(),
			}
			let coordmapini=[
					{lat: limites.minLat,lng: limites.minLng},//{lat:15.258438722900348, lng:-99.75428681640625},
					{lat: limites.minLat,lng: limites.maxLng},//{lat:15.258438722900348, lng:-92.72303681640625},
					{lat: limites.maxLat,lng: limites.maxLng},//{lat:19.03689065576206,  lng:-92.72303681640625},
					{lat: limites.maxLat,lng: limites.minLng}//{lat:19.03689065576206,  lng:-99.75428681640625}
			];
			//generando un poligono cuandrado con el contorno de oaxaca en el centro
			oaxaca = new google.maps.Polygon({
					paths: [coordmapini,oaxCoord],
					strokeColor: '#FFF',
					strokeOpacity: .5,
					strokeWeight: 3,
					fillColor: '#000000BF',
					fillOpacity: 1
			})
			//y asignado al mapa
			oaxaca.setMap(map)

			var activeCenterLat;
			var activeCenterLng;
			google.maps.event.addListener(map,'zoom_changed',function () {
					// toggleMarkersHeatMap();
					console.log(map.getZoom(),zoom)
					if(map.getZoom() == zoom){
							console.log("centrando ...")
							map.setCenter({lat : 17.15727971021128, lng: -96.22866181640627 })
							console.log("centrado")
					}
			});
			google.maps.event.addListener(map,'drag',function() {
					if(map.getZoom() <= zoom)
							map.setCenter({lat : 17.15727971021128, lng: -96.23866181640627 });
					else{
							let ne = map.getBounds().getNorthEast()
							let sw = map.getBounds().getSouthWest()
							let current = {
									maxLat : ne.lat(),
									maxLng : ne.lng(),
									minLat : sw.lat(),
									minLng : sw.lng(),
									center : map.getCenter()
							}
							// current = borders();
							if( current.maxLng <= limites.maxLng && current.minLng >= limites.minLng )
									activeCenterLng = current.center.lng();
							else
									map.setCenter(
											new google.maps.LatLng(
											activeCenterLat,
											activeCenterLng
											)
									);
							if( current.maxLat <= limites.maxLat && current.minLat >= limites.minLat )
									activeCenterLat = current.center.lat();
							else
									map.setCenter(
											new google.maps.LatLng(
											activeCenterLat,
											activeCenterLng
											)
									);
					}
			})
	}
	function ajustar(ancho){
		var zoom;
			if(ancho <= 575.98) {
					console.log('Extra small devices (portrait phones)');
					map.setOptions({minZoom: 6 });
					zoom = 6
			} else if (ancho >= 576 && ancho <= 767.98 ) {
					console.log('Small devices (landscape phones)');
					map.setOptions({minZoom: 6.5 });
					zoom = 6
			} else if (ancho >= 768 && ancho <= 991.98 ) {
					console.log('Medium devices (tablets)');
					map.setOptions({minZoom: 7 });
					zoom = 7
			} else if (ancho >= 992 && ancho <= 1199.98 ) {
					console.log('Large devices (desktops)');
					map.setOptions({minZoom: 7 })
					zoom = 7
			} else if (ancho >= 1200) {
					map.setOptions({minZoom: 8 })
					zoom = 8
					console.log('Extra large devices');
			}
			map.setZoom(zoom);
			map.setCenter({lat : 17.15727971021128, lng: -96.23866181640627 });
			return zoom;
	}
	function pintarMunicipios() {
			let nombreRegion = Object.keys(munXregion);
			nombreRegion.forEach(region => {
				let muns = [];
				for (let i = 0; i < munXregion[region].length; i++) {
						//generando un poligono con el contorno del municipio
						muns[i] = new google.maps.Polygon({
							paths: coordMunicipios[munXregion[region][i][0]],
							strokeColor: '#999',//colores[region],
							strokeOpacity: .5,
							strokeWeight: .5,
							fillColor: '#ffffffbf',//colores[region],
							fillOpacity: 1,
							map : map
						});
				}
				//relacionando poligonos a regiones
				Regiones[region.replace(' ','')] = muns;
			});
	}
	function initControls() {
			$('#municipios').autocomplete({
					source: function( request, response ) {
							$.ajax({
							url: '{{ route('municipios.select') }}',
							dataType: "json",
							method : 'get',
							data: {
									search: request.term
							},
							success: function (data) {
									$.map(data,function(n,i){
									/*   var pa = n.primer_apellido == null ? '' : n.primer_apellido
									var sa = n.segundo_apellido == null ? '' : n.segundo_apellido */
									n.label = n.nombre
									})
									response(data);
							}});
					},
					minLength: 4,
					select: function( event, ui ) {
							let data = ui.item;
							map.setZoom(15);
							map.setCenter({lat: parseFloat(data.lat), lng: parseFloat(data.lng)});
							$('#municipios').val('')
					},
					appendTo : "#controles"
			});
			$('#busqueda').click(function () {
					$('#municipios').val('');
			});
			$('input:checkbox.sw-region').change(function (event) {
				pintarRegiones($(event.target).attr('id').toUpperCase(),$(event.target).prop('checked'))
			})
			$('input:checkbox.sw-programa').change(function (event) {
				toggleDatos($(event.target).attr('id').toLowerCase(),$(event.target).prop('checked'))
			})
      $('#marcador-mapa_calor').on('click',function (event) {
        mmomc = $(event.target).prop('checked')
      })
			jQuery('.dropdown-menu').on('click', function (e) {
				e.stopPropagation();
			});
			map.controls[google.maps.ControlPosition.TOP_RIGHT].push(document.querySelector('#controles'))
	}
	function pintarRegiones(region,estado){
		console.log(region,estado);
		if(Regiones.hasOwnProperty(region)){
			municipios = Regiones[region]
			municipios.forEach(municipio => {
				municipio.setOptions({
					// strokeColor   : estado ? '#999999FF' : '#999999FF',
					// strokeOpacity : estado ? .5 : .5,
					// strokeWeight  : estado ? .5 : 0,
					// fillColor     : estado ? '#ffffffBF' : '#000000BF',
					map : estado ? map : null
				});
			});
		}
		else
			alert('Region invalida, llame a informática.')
	}
  function mapaDeCalor() {
    var tamañoBolita = 30;
    cocinas = new google.maps.visualization.HeatmapLayer({
      data: puntos.cocinas.map(e=>e.ubicacion),
      // map : map ,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      'rgba(211, 255, 0, 1)',
      /* 'rgba(211, 196, 0, 0.2)',
      'rgba(211, 127, 0, 0.3)',
      'rgba(211, 96, 0, 0.4)', */
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      'rgba(168, 96, 0, 1)',
      /* 'rgba(150, 68, 0, 0.6)',
      'rgba(140, 50, 0, 0.7)',
      'rgba(130, 50, 0, 0.8)', */
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)',
      'rgba(170, 20, 0, 1)'
      ],
      radius : tamañoBolita
    });
    escuelas = new google.maps.visualization.HeatmapLayer({
      data: puntos.escuelas.map(e=>e.ubicacion),
      //map : map,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 80, 1)',
      'rgba(0, 191, 80, 1)',
      'rgba(0, 127, 80, 1)',
      'rgba(0, 96, 70, 1)',
      'rgba(0, 63, 60, 1)',
      'rgba(0, 63, 55, 1)',
      'rgba(0, 63, 40, 1)',
      'rgba(0, 63, 30, 1)',
      'rgba(0, 63, 20, 1)'
      ],
      radius : tamañoBolita
    });
    casas = new google.maps.visualization.HeatmapLayer({
      data: puntos.sujetos.map(e=>e.ubicacion),
      //map : map,
      gradient : [
      'rgba(0, 255, 255, 0)',
      'rgba(0, 255, 255, 1)',
      'rgba(0, 191, 255, 1)',
      'rgba(0, 127, 255, 1)',
      'rgba(0, 63, 255, 1)',
      'rgba(0, 0, 255, 1)',
      'rgba(0, 0, 223, 1)',
      'rgba(0, 0, 191, 1)',
      'rgba(0, 0, 159, 1)',
      'rgba(0, 0, 127, 1)'
      ],
      radius : tamañoBolita
    });
  }
  function marcadores() {
    var tamañoMarkers = 32;
    var ie = new google.maps.MarkerImage(
      "{{ asset('images/AsisAlimentaria/escuelas-icon.png') }}",
      new google.maps.Size(tamañoMarkers,tamañoMarkers),
      new google.maps.Point(0,0),
      new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      new google.maps.Size(tamañoMarkers,tamañoMarkers)
    );
    var is = {
      url : "{{ asset('images/AsisAlimentaria/apoyo-icon.png') }}",
      size: new google.maps.Size(tamañoMarkers,tamañoMarkers),
      origin : new google.maps.Point(0,0),
      anchor : new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      scaledSize: new google.maps.Size(tamañoMarkers, tamañoMarkers)
    };
    var ic = {
      url : "{{ asset('images/AsisAlimentaria/cocinas-icon.png') }}",
      size: new google.maps.Size(tamañoMarkers,tamañoMarkers),
      origin : new google.maps.Point(0,0),
      anchor : new google.maps.Point((tamañoMarkers/2),(tamañoMarkers/2)),
      scaledSize: new google.maps.Size(tamañoMarkers, tamañoMarkers)
    };
    puntos.cocinas.forEach(function (item) {
      let temp = new google.maps.Marker({
        position: item.ubicacion,
        //map: map,
        icon: ic,
        title: "cocina "+item.folio,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(item.folio));
        infoWindow.open(map, temp)
      });
      cmarks.push(
        temp
      );
    })
    puntos.escuelas.forEach(function (item) {
      let temp = new google.maps.Marker({
        position: item.ubicacion,
        //map: map,
        icon: ie,
        title: "escuela"+item.folio,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(item.folio));
        infoWindow.open(map, temp)
      });
      emarks.push(
        temp
      );
    })
    puntos.sujetos.forEach(function (item) {
      let temp = new google.maps.Marker({
        position: item.ubicacion,
        //map: map,
        icon: is,
        title: "casa de dia "+item.folio,
        zindex: 11101
      })
      temp.addListener('click', function (event) {
        infoWindow.setContent(detalles(item.folio));
        infoWindow.open(map, temp)
      });
      smarks.push(
        temp
      );
    })
  }
  function toggleDatos(data = 'toggle',state) {
    let mapaaux
    switch (data) {
      case 'cocinas':
        mapaaux = state ? map : null
        cmarks.forEach(function(punto){
          punto.setMap(mmomc ? mapaaux : null);
        });
        cocinas.setMap(mmomc ? null : mapaaux);
        break;
      case 'escuelas':
        mapaaux = state ? map : null
        emarks.forEach(function(punto){
          punto.setMap(mmomc ? mapaaux : null);
        });
        escuelas.setMap(mmomc ? null : mapaaux);
        break;
      case 'sujetos':
        mapaaux = state ? map : null
        smarks.forEach(function(punto){
          punto.setMap(mmomc ? mapaaux : null);
        });
        casas.setMap(mmomc ? null : mapaaux);
        break;
      default:

        break;
    }
  }
  function toggleMarkersHeatMap(params) {
    if(map.getZoom() > 10 ){
      if(cocinas.getMap()!=null){
        cmarks.forEach(function(punto){
          punto.setMap(map);
        });
        cocinas.setMap(null);
      }
      if(escuelas.getMap()!=null){
        emarks.forEach(function(punto){
          punto.setMap(map);
        });
        escuelas.setMap(null);
      }
      if(casas.getMap()!=null){
        smarks.forEach(function(punto){
          punto.setMap(map);
        });
        casas.setMap(null);
      }
    } else {
      if(cmarks[0].getMap()!=null){
        cocinas.setMap(map);
        cmarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
      if(emarks[0].getMap()!=null){
        escuelas.setMap(map);
        emarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
      if(smarks[0].getMap()!=null){
        casas.setMap(map);
        smarks.forEach(function(punto){
          punto.setMap(null);
        });
      }
    }
  }
	$(document).ready(function () {
			initMap()
			pintarMunicipios()
			initControls()
      for (const programa in puntos) {
        if (puntos.hasOwnProperty(programa)) {
            element = puntos[programa];
            puntos[programa] = element.map(item => ({ubicacion : new google.maps.LatLng(item.latitud,item.longitud), folio: item.foliodif }))
        }
      }
      mapaDeCalor()
      marcadores()
			//configurar el zoom minimo cada vez que se entra o sale de pantalla comlpeta
			document.addEventListener("fullscreenchange", limitarMapa);       //Estandar (jajajajajajaja)
			document.addEventListener("webkitfullscreenchange", limitarMapa); //Webkit (Safari, Chrome y Opera 15+)
			document.addEventListener("mozfullscreenchange", limitarMapa);    //Firefox
			document.addEventListener("MSFullscreenChange", limitarMapa);     //Internet Explorer 11+
			$(window).on('resize',() => {
					limitarMapa()
			});
	})
</script>
@endpush
