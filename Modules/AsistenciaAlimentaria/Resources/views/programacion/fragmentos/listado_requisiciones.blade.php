
<div class="modal-header badge badge-light">
  <div class="modal-title">
    <h5><strong>FOLIO: </strong>{{$programacion->alimentario->foliodif}}</h5>
    <h5><strong>MUNICIPIO: </strong>{{$programacion->alimentario->municipio->nombre}}</h5>
    <h5><strong>LOCALIDAD: </strong>{{$programacion->alimentario->localidad->nombre}}</h5>
  </div>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
  </div>
  <div class="modal-body">
    <form id="form-dotacion">
      <table class="table table-sm">
        <thead>
          <tr>
            <th scope="col">Folio recibo</th>
            <th scope="col"># Oficio</th>
            <th scope="col"># Bimestre</th>
            <th scope="col"># Entrega</th>
            <th scope="col">Tipo entrega</th>
            <th scope="col">Observaciones</th>
            <th scope="col">Estatus</th>
            <th scope="col">Importe</th>
            <th scope="col">Generar</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($recibos as $item)
            <tr>
              <th>{{ $item->id or '' }}</th>
              <td>{{ $item->num_oficio or '' }}</td>
              <td>{{ $item->num_bimestre or '' }}</td>
              <td>{{ $item->num_entrega or '' }}</td>
              <td>{{ $item->tipoentrega or '' }}</td>
              <td>{{ $item->observaciones or '' }}</td>
              <th><span class="badge badge-{{ isset($item->fecha_pago) ? 'success' : 'danger' }}">{{ isset($item->fecha_pago) ? 'PAGADO' : 'NO PAGADO' }}</span></th>
              <th>{{ $item->importe_total or '' }}</th>
              <td>
                @if(!isset($item->fecha_pago))
                  <button type="button" class="btn btn-danger" onclick="getRecibo({{$item->programacion_id}},{{$item->requisicion_id}});"><i class="fas fa-file-pdf"></i> PDF</button>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </form>
  </div>
  <div class="modal-footer">
      <button type="button" class="btn btn-secondary mr-auto" tabindex="0" data-custom-dismiss="modal" ><span><i class="far fa-times-circle"></i> Cerrar</span></button>
  </div>
  <script>
    function getRecibo(prog,req) {
      $.blockUI();
      $.ajax({
        url : "{{ route('alimentarios.programacion.requisicion.recibo',['p_id','r_id']) }}".replace('p_id',prog).replace('r_id',req),
        global : false
      })
      .done(function(data) {
        jsreport.serverUrl = "http://187.157.97.110:3002";
        // jsreport.serverUrl = "http://localhost:5488";

        var request = {
            template: {
                shortid: "B1lTKVbhlH"
            },
            data: data
        }
        jsreport.renderAsync(request).then(function (res2) {
            res2.download('recibo(s).pdf');
        }).catch(()=>{
            Swal.fire('Error','No se genero el pdf','error');
        }).finally(()=>{
            $.unblockUI();
        })
      })
      .fail(function(data) {	
      });
    }
  </script>