@extends('asistenciaalimentaria::layouts.master')

@section('title','CCNC-Programación')

@push('head')
{{--   <style>
    .error{
      width: 100%;
    }
    .img-conv {
      max-width: 50%;
      margin-inline: 25%;
    }
  </style> --}}
@endpush

@section('content')
<div class="card">
    <div class="card-header">
        <i class="fas fa-align-justify"></i> Planeación {{$programa}}
        {{-- <div class="card-header-actions">
						<a class="card-header-action" href="https://datatables.net" target="_blank">
						<small class="text-muted">docs</small>
						</a>
					</div> --}}
    </div>
    <div class="card-body">
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
            <input type="text" id="search" name="search" class="form-control">
            <div class="input-group-append">
                <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
            </div>
        </div>
        {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap
        dataTables_wrapper dt-bootstrap4', 'style'=>'width:100%', 'id' => 'alimentarios'],true) !!}
    </div>
</div>
@stop
@section('aside')
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">
        <i class="fas fa-users"></i>
      </a>
    </li>
  </ul>
  <!-- Tab panes-->
  <div class="tab-content">
    <div class="tab-pane active" id="timeline" role="tabpanel">
        <div class="list-group list-group-accent">
          <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Total de beneficiarios</div>
          <div class="list-group-item list-group-item-accent-warning list-group-item-divider">
            <div class="text-center">
              <strong id="total-benef"></strong>
            </div>
          </div>
        </div>
        <div class="list-group list-group-accent">
          <div class="list-group-item list-group-item-accent-secondary bg-light text-center font-weight-bold text-muted text-uppercase small">Beneficiarios por subprograma</div>
          <div id="text-benef" class="text-right">
          </div>
        </div>
    </div>
  </div>
@endsection
@push('body')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    var tabla = ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
    }})('#alimentarios');

    function ajaxSuccess(response,tabla2) {
        if($('#create').length > 0) $('#create').modal('hide')
        if($('#edit').length > 0) $('#edit').modal('hide')
        Swal.fire('¡Guardado!','Operación realizada con éxito','success')
    }
    function ajaxError(response,tabla2) {
        Swal.fire('¡Error!',response.responseJSON.message,'error')
    }
    function modalHidden(identificador) {
        if($('#'+identificador+' .modal-title:contains("beneficiario")').length == 0)
            $('#progShow').modal('show')
        else
            $('#list').modal('show')
    }
    (()=>{
        if(!localStorage.getItem('tutorial')){
            //initialize instance
            var enjoyhint_instance = new EnjoyHint({});
            
            //simple config.
            //Only one step - highlighting(with description) "New" button
            //hide EnjoyHint after a click on the button.
            var enjoyhint_script_steps = [
                {
                    'click .navbar-toggler.aside-menu-toggler' : 'Para ver las consultas de beneficiarios por programa, despliega el panel lateral dando clic aquí'
                },
                {
                    'click .aside-menu' : 'Una vez desplegada, mostrara información de los beneficiarios'
                },
                {
                    'click .column-filter' : 'Los resultados se actualizaran en automático de acuerdo a los filtros que se usen'
                }
            ];
            
            //set script config
            enjoyhint_instance.set(enjoyhint_script_steps);
            
            
            //run Enjoyhint script
            enjoyhint_instance.run();
            localStorage.setItem('tutorial',true)
        }
    })()
    function eliminarBenefRec(urlBase,index,limite,datos) {
        $('#eliminacionActual').text(parseInt($('#eliminacionActual').text())+1)
        $.ajax({
            url : urlBase.replace('benef_id',datos[index]),
            type: 'DELETE',
            global: false
        }).fail(function (jqXHR, textStatus, errorThrown){
            eliminacionesFallidas++;
        }).always(function (data, textStatus, jqXHR){
            if(index+1<limite)
                eliminarBenefRec(urlBase,index+1,limite,datos)
            else{
                $.unblockUI();
                Swal.fire({
                    title: eliminacionesFallidas==0? 'Exito' : 'Error',
                    text: eliminacionesFallidas==0? 'Beneficiarios eliminados' : 'Ocurrio un error al eliminar '+eliminacionesFallidas+' beneficiarios',
                    type: eliminacionesFallidas==0? 'success' : 'warning',
                    showConfirmButton : true
                }).then( (result) => {
                    LaravelDataTables.beneficiarios.ajax.reload();
                })
            }
        })
    }
</script>
@endpush