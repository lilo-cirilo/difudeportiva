@php
    $prefix = (strpos($_SERVER["REQUEST_URI"],'ccnc') !== false ? 'ccnc' : (strpos($_SERVER["REQUEST_URI"],'desayunos') !== false ? 'desayunos' : 'sujetos'));
@endphp
<div class="box box-primary">            
    <div class="box-header with-border">
        <h3 class="box-title">{{ isset($alimentario) ? 'Actualizar '.$prefix.' '.$alimentario->foliodif : 'Registrar '.$prefix }}</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <form role="form" id="form-cocinas">
            @include('asistenciaalimentaria::alimentarios.fragmentos.form_body')
            @if (isset($programa))
                @if ($programa === config('asistenciaalimentaria.sujetosId'))
                    @include('asistenciaalimentaria::sujetos.fragmentos.form_body')
                @endif
                @if ($programa === config('asistenciaalimentaria.desayunosId'))
                    @include('asistenciaalimentaria::escuelas.fragmentos.form_body')
                @endif
            @endif
        </form>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
        <label>Los campos con * son obligatorios</label>
        <button class="btn float-right btn-primary" tabindex="0" onclick="transaccion.formAjax('{{ isset($alimentario) ? route('alimentarios.'.$prefix.'.update',$alimentario->id) : route('alimentarios.'.$prefix.'.store')}}', '{{ isset($alimentario) ? 'PUT' : 'POST' }}','form-cocinas',{ programa_id: {{config('asistenciaalimentaria.'.$prefix.'Id')}} })"><span><i class="fa fa-save"></i> Guardar</span></button>
    </div>
</div>