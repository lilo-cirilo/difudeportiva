@extends('asistenciaalimentaria::layouts.master')

@push('head')
@endpush

@section('content-title', 'Cocinas Comedor Nutricionales Comunitarias')
@section('content-subtitle', 'Asistencia Alimentaria')

@section('content')
<section class="content">
  @include('asistenciaalimentaria::alimentarios.fragmentos.crear_editar')
</section>
@stop

@push('body')
<script>
  $('#checkSedesol').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  })
  $('#checkAmpliacion').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  })
  $('#checkDesarrollo').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  })
  $('#checkSubLocalidad').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  }).on('ifChanged', function (e) {
    if(e.target.checked) {
      $('#sublocalidad').prop('disabled',false)
      $('#sublocalidad').attr('required',true)
    } else {
      $('#sublocalidad').prop('disabled',true)
      $('#sublocalidad').attr('required',false)
      $('#sublocalidad').val('')
    }
  })
	$('#checkEscuelaPadre').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  }).on('ifChanged', function (e) {
    if(e.target.checked) {
      $('#padre_id').prop('disabled',false)
      $('#padre_id').attr('required',true)
    } else {
      $('#padre_id').prop('disabled',true)
      $('#padre_id').attr('required',false)
      $('#padre_id').empty()
    }
  })
  $('#checkEscuela').iCheck({
    checkboxClass: "icheckbox_square-green",
    increaseArea: "10%"
  }).on('ifChanged', function (e) {
    if(e.target.checked) {
      $('#escuela_id').prop('disabled',false)
      $('#escuela_id').attr('required',true)
			$('#escuelaPadre').show()
    } else {
      $('#escuela_id').prop('disabled',true)
      $('#escuela_id').attr('required',false)
      $('#escuela_id').empty()
			//disabled posible padre (escuela)
			$('#padre_id').prop('disabled',true)
      $('#padre_id').attr('required',false)
			$('#checkEscuelaPadre').closest('.icheckbox_square-green').removeClass('checked')
			$('#escuelaPadre').hide()
    }
  })
  //$("#localidad_id").select2({disabled:true})
  $('#municipio_id').select2({
    language: 'es',
    minimumInputLength: 2,
    allowClear : true,
    placeholder : 'Seleccione un municipio',
    ajax: {
      url: '{{ route('municipios.select') }}',
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        }
      },
      cache: true
    }
  }).change((event) => {
    $('#municipio_id').valid()
    $('#localidad_id').empty()
    $('#localidad_id').prop('disabled',false)
  })
  $('#localidad_id').select2({
    language: 'es',
    allowClear : true,
    placeholder : 'Seleccione una localidad',
    ajax: {
      url: '{{ route('localidades.select') }}',
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term,
          municipio_id: $('#municipio_id').val()
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data, (item) => {
            return {
              id: item.id,
              text: '('+item.cve_localidad+') '+item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        }
      },
      cache: true
    }
  }).change((event) => {
    $('#localidad_id').valid()
    //$('#escuela_id').empty()
  })
  $('#escuela_id').select2({
    language: 'es',
    allowClear : true,
    placeholder : 'Seleccione una opción',
    ajax: {
      url: "{{ route('alimentarios.escuelas') }}",
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term,
          municipio_id: $('#municipio_id').val(),
          localidad_id: $('#localidad_id').val(),
          type : 'query_append',
          page : params.page || 1
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data.data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          }),
          pagination: {
            more : data.current_page < data.last_page
          }
        }
      },
      cache: true
    }
  })
  $('#financiamiento_id').select2({
    language: 'es',
    allowClear : true,
    placeholder : 'Seleccione una opción',
    ajax: {
      url: "{{ route('alimentarios.tiposfinanciamiento') }}",
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          })
        }
      },
      cache: true
    }
  })
	$('#padre_id').select2({
    language: 'es',
    allowClear : true,
    placeholder : 'Seleccione una opción',
    ajax: {
      url: "{{ route('alimentarios.escuelas') }}",
      delay: 500,
      dataType: 'JSON',
      type: 'GET',
      data: (params) => {
        return {
          search: params.term,
          municipio_id: $('#municipio_id').val(),
          localidad_id: $('#localidad_id').val(),
          type : 'query_append',
          page : params.page || 1
        }
      },
      processResults: (data, params) => {
        params.page = params.page || 1
        return {
          results: $.map(data.data, (item) => {
            return {
              id: item.id,
              text: item.nombre,
              slug: item.nombre,
              results: item
            }
          }),
          pagination: {
            more : data.current_page < data.last_page
          }
        }
      },
      cache: true
    }
  })
  
  validacion.iniciar('form-cocinas')
  
  //exito en las operaciones anteriores
  function ajaxSuccess(params) {
    Swal.fire('¡Guardado!','Operación realizada con éxito','success')
    @if (!isset($alimentario))
      $('#municipio_id').empty()
      $('#localidad_id').empty()
      $('#financiamiento_id').empty()
      $('#escuela_id').empty()
      $('#form-cocinas')[0].reset()
    @endif
  }
  //fallo en las operaciones anteriores
  function ajaxError(params) {
    Swal.fire('¡Error!',params.responseJSON.message,'error')
  }
</script>
@endpush