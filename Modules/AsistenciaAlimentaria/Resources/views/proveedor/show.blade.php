@extends('asistenciaalimentaria::layouts.master')

@section('content-subtitle', 'Estado de requisiciones')

@push('head')
    <style>
        .table-danger, .table-danger > th, .table-danger > td{
            background-color: #cc6e6e !important;
        }
        .table-warning, .table-warning > th, .table-warning > td{
            background-color: #aea71a !important;
        }
        .table-success, .table-success > th, .table-success > td{
            background-color: #2a9750 !important;
        }
    </style>
@endpush

@section('content')
<div class="card">
    <div class="card-header with-border text-center">
        <h4>Reporte de reqisicion</h4>
    </div>
    <div class="card-body">
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
            <input type="text" id="search" name="search" class="form-control">
            <div class="input-group-append">
                <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
            </div>
        </div>
        {!! $dataTable->table(['class' => 'table table-dark table-bordered dt-responsive nowrap
        dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'requisiciones']) !!}
    </div>
</div>
@stop

@push('body')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
    }})('#requisiciones');
</script>
@endpush