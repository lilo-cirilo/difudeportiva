<style>
  #tbl tbody { display:block; max-height:250px; overflow-y:scroll; }
  #tbl thead, #tbl tbody tr { display:table; width:100%; table-layout:fixed; }
</style>
<div class="modal-header with-border">
  <h5 class="modal-title">{{ isset($ordencarga) ? 'Actualizar' : 'Registrar' }} orden de carga</h5>
</div>
  {{-- modal-header --}}
<div class="modal-body">
  <form id="form-orden">
    <h6>Datos del chofer</h6>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="nombre">*Nombre:</label>
          <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $ordencarga->chofer->nombre or '' }}" placeholder="INGRESE NOMBRE" required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="primer_apellido">*Primer Apellido:</label>
          <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $ordencarga->chofer->primer_apellido or '' }}" placeholder="INGRESE EL PRIMER APELLIDO" required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="segundo_apellido">Segundo Apellido:</label>
          <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $ordencarga->chofer->segundo_apellido or '' }}" placeholder="INGRESE EL SEGUNDO APELLIDO"/>
        </div>
      </div>
    </div>
    <h6>Datos de ruta</h6>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="placas">*Placas del vehículo:</label>
          <input type="text" class="form-control" id="placas" name="placas" value="{{ $ordencarga->placas or '' }}" placeholder="INGRESE PLACAS" required/>
        </div>
      </div>
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="ruta">*Ruta:</label>
          <input type="text" class="form-control" id="ruta" name="ruta" value="{{ $ordencarga->ruta or '' }}" placeholder="INGRESE RUTA" required/>
        </div>
      </div>
    </div>
    <h6>Datos de carga</h6>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="form-group">
          <label for="fecha">*Fecha de salida:</label>
          <input type="text" class="form-control mask" id="fecha" name="fecha" value="{{ isset($ordencarga) ? $ordencarga->getFormatFecha() : '' }}" data-inputmask='"mask": "99/99/9999"' placeholder="INGRESE FECHA DE CARGA" required/>
        </div>
      </div>
    </div>
  </form>
  <div class="card">
      <div class="card-header">
      </div>
      <div class="card-body">
        <div id="tb" class="table table-bordered">
        </div>
      </div>
  </div>
</div>
{{-- modal-body --}}
<div class="modal-footer">
    <button class="btn btn-secondary pull-left mr-auto" tabindex="0" data-custom-dismiss="modal"><span><i class="far fa-times-circle"></i> Cerrar</span></button>
    <label>Los campos con * son obligatorios</label>
    @php
        $r = route('alimentarios.proveedor.requisicion.ordenCarga.store',[$proveedor_id,$requisicion_id]);/* isset($ordencarga) ?  : route('alimentarios.proveedor.ordenCarga.update',[$requisicion_id,$ordencarga->id]); */
    @endphp
    <button class="btn pull-right button-dt tool" onclick="transaccion.formAjax('{{ $r }}','{{ isset($ordencarga) ? 'PUT' : 'POST' }}','form-orden',{datos:localStorage.getItem('datos{{ $requisicion_id }}')},false)"><span><i class="fa fa-save"></i> Guardar</span></button>
</div>

<script>
    (function() {
      validacion.iniciar('form-orden');
      lsj = JSON.parse(localStorage.getItem('datos{{ $requisicion_id }}'));
      $('.card-header').html('<i class="fa fa-align-justify"></i>     <strong>'+(lsj == null ? 0 : lsj.length)+'</strong> programacion(es) seleccionada(s) para esta orden de carga');
      var array = JSON.parse(localStorage.getItem("datos{{$requisicion_id}}")) || [];
      trs = '<table id="tbl" class="table table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4"><thead><tr><th>Municipio</th><th>Localidad</th></tr></thead><tbody>';
      array.forEach(element => {
        e = JSON.parse(element);
        trs += '<tr><td>'+e.municipio+'</td><td>'+e.localidad+'</td></tr>';
      });
      trs+='</tbody></table>';
      $('#tb').append(trs);
      var cache=[];
      $('#nombre').autocomplete({
        //minLength : 5,
        source: function(request, response) {
          var term = request.term;
          if (term in cache) {
            response(cache[ term ]);
            return;
          }
          $.ajax({
            url: "{{ route('alimentarios.proveedor.chofer.search',$proveedor_id) }}",
            dataType: "json",
            global: false,
            data: {
              search : term.replace('_','')     
            },
            success: function (data) {
              cache[ term ] = data;
              response(data);
            }
          });
        },
        select: function(event, ui) {
          //encontrarPersona(ui.item.edit);
          event.preventDefault()
          $('#nombre').val(ui.item.nombre)
          $('#primer_apellido').val(ui.item.primer_apellido)
          $('#segundo_apellido').val(ui.item.segundo_apellido)
        },
        appendTo: "#create"
      })
      $('#fecha').datepicker({
          autoclose: true,
          language: 'es',
          format: 'dd/mm/yyyy',
          startDate: moment().day(-45).format('DD/MM/YYYY'),//moment().format('DD/MM/YYYY')
          endDate: moment().day(8).format('DD/MM/YYYY')
      });
      $('.mask').inputmask();
    })()
</script>