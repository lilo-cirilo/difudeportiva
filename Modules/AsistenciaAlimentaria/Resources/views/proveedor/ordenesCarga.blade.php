@extends('asistenciaalimentaria::layouts.master')

@section('content-subtitle', 'Ordenes de carga')

@push('head')
@endpush

@section('content')
    <div class="card">
        <div class="card-header with-border text-center">
            <h4>Ordenes de carga</h4>
        </div>
        <div class="card-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
                <input type="text" id="search" name="search" class="form-control">
                <div class="input-group-append">
                    <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
                </div>
            </div>
            {!! $dataTable->table(['class' => 'table table-dark table-bordered table-striped dt-responsive nowrap dataTables_wrapper dt-bootstrap4 no-footer', 'style'=>'width:100%', 'id' => 'ordenes'],true) !!}
        </div>
    </div>
@stop

@push('body')
{!! $dataTable->scripts() !!}
<script type="text/javascript">
    var tabla = 
        ((tablaId) => {
        $('#search').keypress(function(e) {
            if(e.which === 13) {
                $(tablaId).DataTable().search($('#search').val()).draw()
            }
        })
        $('#btn_buscar').on('click', function() {
            $(tablaId).DataTable().search($('#search').val()).draw()
        })
        function recargar(params) {
            $(tablaId).DataTable().ajax.reload(null,false);
        }
        return {
            recargar : recargar
    }})('#ordenes');
    function cancelarOrdenCarga(id){
        var ruta = "{{ route('alimentarios.proveedor.ordencarga.destroy',[1,'p_id']) }}".replace('p_id',id)
        Swal.fire({
            title: '¿Estas seguro?',
            text: '¡Se cancelara la orden de carga!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            confirmButtonText: 'SI',
            cancelButtonText: 'NO'
        }).then( (result) => {
            if (result.value) {
                transaccion.miniAjax(ruta,'DELETE',{},'json',true,'application/x-www-form-urlencoded; charset=UTF-8','ordenes')
            }
        })
    }
</script>
@endpush