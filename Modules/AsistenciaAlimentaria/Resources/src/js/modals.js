function initModalComponets() {}

function modalHidden(identificador) {}

var modal = (function () {
  function primerModal(url,identificador,extra=null) {
    if($('#forModals').length == 0){
      $('body').append('<div id="forModals"></div>');
    }
    url+='?id='+identificador
    for (const llave in extra) {
      if (extra.hasOwnProperty(llave)) {
        url+='&'+llave+'='+extra[llave]
      }
    }
    $.get(url, function(data) {
      $('#forModals').html(data)
    }).done(function() {
      $('.modal').modal('hide')
      $('body').removeClass('modal-open')
      $('.modal-backdrop').remove()
      $('#'+identificador).modal('show')
    });
    mayusculas()
  }  
  function segundoModal(url,identificador,extra=null) {
    url+='?id='+identificador
    for (const llave in extra) {
      if (extra.hasOwnProperty(llave)) {
        url+='&'+llave+'='+extra[llave]
      }
    }
    $.get(url, function(data) {
      $('#'+identificador).remove()
      $('#create,#edit').remove()
      $('#forModals').append(data)
    }).done(function() {
      $('.modal').modal('hide')
      $('#'+identificador).modal('show')
      $('#'+identificador).on('hidden.bs.modal', function () {
        modalHidden(identificador)
      })
    })
    mayusculas()
  }
  function mayusculas() {
    $(':text').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  }
  return {
    loadModal  : primerModal,
    moreModal : segundoModal
  }
}());