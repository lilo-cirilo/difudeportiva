// window._ = require('lodash');
// window.Popper = require('popper.js').default;

window.$ = window.jQuery = require('jquery');
window.Swal = require('sweetalert2');
window.moment = require('moment');
window.jsreport = require('jsreport-browser-client-dist');
// window.popper = window.Popper = popper;
// require('popper.js');
require('bootstrap')
require('jquery-validation/dist/jquery.validate.min.js');
require('jquery-validation/dist/additional-methods.min.js');
require('jquery-validation/dist/localization/messages_es.min.js');
require('jquery-ui/ui/widgets/autocomplete.js');

// require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
require('@coreui/coreui/dist/js/coreui.min.js');
require('@fortawesome/fontawesome-free/js/all.min.js')
require('datatables.net');
require('datatables.net-buttons/js/dataTables.buttons.js');
require('./mixins/buttons.server-side-jonhy.js');
require('datatables.net-buttons/js/buttons.colVis.js');
require('datatables.net-buttons/js/buttons.flash.js');
require('datatables.net-buttons/js/buttons.html5.js');
require('datatables.net-buttons/js/buttons.print.js');
require('datatables.net-fixedcolumns');
require('datatables.net-fixedheader');
require('datatables.net-rowreorder');
require('datatables.net-colreorder');
require('datatables.net-rowgroup');
require('datatables.net-responsive');
require('datatables.net-select' );
require('datatables.net-bs4');
require('datatables.net-buttons-bs4');
require('datatables.net-fixedcolumns-bs4');
require('datatables.net-fixedheader-bs4');
require('datatables.net-rowreorder-bs4');
require('datatables.net-colreorder-bs4');
require('datatables.net-rowgroup-bs4');
require('datatables.net-responsive-bs4');
require('datatables.net-select-bs4' );
require('datatables.net-autofill-bs4');
require('datatables.net-keytable-bs4');
require('datatables.net-scroller-bs4');
require('bootstrap-fileinput');
require('bootstrap-fileinput/js/locales/es.js');
require('bootstrap-datepicker');
require('bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min');
require('./mixins/Metodos.js');
require('select2');
require('select2/dist/js/i18n/es.js');
require('inputmask/dist/inputmask/jquery.inputmask.js');
require('icheck');
require('block-ui');
$.blockUI.defaults.css.border = 'none';
$.blockUI.defaults.css.padding = '15px';
$.blockUI.defaults.css.backgroundColor = '#000';
$.blockUI.defaults.css['-webkit-border-radius'] = '10px';
$.blockUI.defaults.css['-moz-border-radius'] = '10px';
$.blockUI.defaults.css.opacity = .5;
$.blockUI.defaults.css.color = '#fff';
$.blockUI.defaults.baseZ = 10000;
$.blockUI.defaults.fadeIn= 700;
$.blockUI.defaults.fadeOut= 700;
$.blockUI.defaults.message = `<div align="center">
                              <img style="width:128px; height:128px;" src="/images/preloader/loading.gif">
                              <br />
                              <p class="animated infinite pulse" style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">Procesando...</p>
                              </div>`
                              // <div align="center"><p class="animated infinite pulse" style="/*! color:#f0f0f0; */ margin-top:5px;margin-left:15px; font-family:arial; font-size: larger; ">CARGANDO</p><i class="fas fa-spinner fa-10x fa-pulse"></i><br></div>

$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
// $(document).ajaxSend((e, r, s) => { $.blockUI; console.log('evento', e); console.log('request', r); console.log('settings', s) }).ajaxStop($.unblockUI);

$.validator.setDefaults({
  errorElement: 'span',
  errorPlacement: function (error, element) {
    error.addClass('invalid-feedback');
    element.closest('.form-group').append(error);
  },
  highlight: function (element, errorClass, validClass) {
    $(element).removeClass('is-valid').addClass('is-invalid');
    $(element).closest('.form-group').children('span').children('.selection').children('.select2-selection').removeClass('select2-valid').addClass('select2-error');
  },
  unhighlight: function (element, errorClass, validClass) {
    $(element).removeClass('is-invalid').addClass('is-valid');
    $(element).closest('.form-group').children('span').children('.selection').children('.select2-selection').removeClass('select2-error').addClass('select2-valid');
  }
})

$.extend($.validator.messages, {
  required: 'Este campo es obligatorio.',
  remote: 'Por favor, rellena este campo.',
  email: 'Por favor, escribe una dirección de correo válida.',
  url: 'Por favor, escribe una URL válida.',
  date: 'Por favor, escribe una fecha válida.',
  dateISO: 'Por favor, escribe una fecha (ISO) válida.',
  number: 'Por favor, escribe un número válido.',
  digits: 'Por favor, escribe sólo dígitos.',
  creditcard: 'Por favor, escribe un número de tarjeta válido.',
  equalTo: 'Por favor, escribe el mismo valor de nuevo.',
  extension: 'Por favor, escribe un valor con una extensión aceptada.',
  maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
  minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
  rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
  range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
  max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
  min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
  nifES: 'Por favor, escribe un NIF válido.',
  nieES: 'Por favor, escribe un NIE válido.',
  cifES: 'Por favor, escribe un CIF válido.'
});