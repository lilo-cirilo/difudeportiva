(function ($, DataTable) {
  "use strict";

  var _buildParams = function (dt, action) {
    var params = dt.ajax.params();
    params.action = action;
    params._token = $('meta[name="csrf-token"]').attr('content');

    return params;
  };

  var _downloadFromUrl = function (url, params) {
    var postUrl = url + '/export';
    var xhr = new XMLHttpRequest();
    xhr.open('POST', postUrl, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
      if (this.status === 200) {
        var filename = "";
        var disposition = xhr.getResponseHeader('Content-Disposition');
        if (disposition && disposition.indexOf('attachment') !== -1) {
          var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
          var matches = filenameRegex.exec(disposition);
          if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
        }
        var type = xhr.getResponseHeader('Content-Type');

        var blob = new Blob([this.response], { type: type });
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
          window.navigator.msSaveBlob(blob, filename);
        } else {
          var URL = window.URL || window.webkitURL;
          var downloadUrl = URL.createObjectURL(blob);

          if (filename) {
            // use HTML5 a[download] attribute to specify filename
            var a = document.createElement("a");
            // safari doesn't support this yet
            if (typeof a.download === 'undefined') {
              window.location = downloadUrl;
            } else {
              a.href = downloadUrl;
              a.download = filename;
              document.body.appendChild(a);
              a.click();
            }
          } else {
            window.location = downloadUrl;
          }

          setTimeout(function () {
            URL.revokeObjectURL(downloadUrl);
          }, 100); // cleanup
        }
      }
    };
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send($.param(params));
  };

  var _buildUrl = function (dt, action) {
    var url = dt.ajax.url() || window.location.href;
    var params = dt.ajax.params();
    params.action = action;

    if (url.indexOf('?') > -1) {
      return url + '&' + $.param(params);
    }

    return url + '?' + $.param(params);
  };

  DataTable.ext.buttons.excel = {
    className: 'buttons-excel',

    text: function (dt) {
      return '<i class="far fa-file-excel"></i> ' + dt.i18n('buttons.excel', 'Excel');
    },

    action: function (e, dt, button, config) {
      $.ajax({
        url: dt.ajax.url() || window.location.href,
        //dataType: 'json',
        data: { draw: 2, length: -1, action:'customExcel' },
        //contentType: 'application/json',
        success: function (res) {
          var datos = {};
          datos.titulo = document.title;
          datos.cabeceras = res.cabeceras
          datos.datos = res.data.map(d => Object.values(d)).map(e => e.map(f => f != null ? f : '-'));
          window.varx = datos
          window.vary = res
          jsreport.serverUrl = "http://187.157.97.110:3001";
          var request = {
            template: {
              shortid: "HJ5VttAyS"
            },
            data: datos
          };
          jsreport.renderAsync(request).then(function (res2) {
            res2.download(res.name+'.xlsx');
          });
        }
      })
      // var url = _buildUrl(dt, 'excel');
      // window.location = url;
    }
  };

  DataTable.ext.buttons.postExcel = {
    className: 'buttons-excel',

    text: function (dt) {
      return '<i class="far fa-file-excel"></i> ' + dt.i18n('buttons.excel', 'Excel');
    },

    action: function (e, dt, button, config) {
      $.ajax({
        url: dt.ajax.url() || window.location.href,
        type : 'POST',
        data: { draw: 2, length: -1, action:'customExcel' },
        //contentType: 'application/json',
        success: function (res) {
          var datos = {};
          datos.titulo = document.title;
          datos.cabeceras = res.cabeceras
          datos.datos = res.data.map(d => Object.values(d)).map(e => e.map(f => f != null ? f : '-'));
          window.varx = datos
          window.vary = res
          jsreport.serverUrl = "http://187.157.97.110:3001";
          var request = {
            template: {
              shortid: "HJ5VttAyS"
            },
            data: datos
          };
          jsreport.renderAsync(request).then(function (res2) {
            res2.download(res.name+'.xlsx');
          });
        }
      })
    }
  };

  DataTable.ext.buttons.phpexcel = {
    className: 'buttons-excel',

    text: function (dt) {
        return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.excel', 'Excel');
    },

    action: function (e, dt, button, config) {
        var url = _buildUrl(dt, 'excel');
        window.location = url;
    }
  };

  DataTable.ext.buttons.phppostExcel = {
      className: 'buttons-excel',

      text: function (dt) {
          return '<i class="fa fa-file-excel-o"></i> ' + dt.i18n('buttons.excel', 'Excel');
      },

      action: function (e, dt, button, config) {
          var url = dt.ajax.url() || window.location.href;
          var params = _buildParams(dt, 'excel');

          _downloadFromUrl(url, params);
      }
  };

  DataTable.ext.buttons.export = {
    extend: 'collection',

    className: 'buttons-export',

    text: function (dt) {
      return '<i class="fas fa-download"></i> ' + dt.i18n('buttons.export', 'Exportar') + '&nbsp;<span class="caret"/>';
    },

    buttons: ['csv', 'excel', 'pdf']
  };

  DataTable.ext.buttons.csv = {
    className: 'buttons-csv',

    text: function (dt) {
      return '<i class="fas fa-file-csv"></i> ' + dt.i18n('buttons.csv', 'CSV');
    },

    action: function (e, dt, button, config) {
      var url = _buildUrl(dt, 'csv');
      window.location = url;
    }
  };

  DataTable.ext.buttons.postCsv = {
    className: 'buttons-csv',

    text: function (dt) {
      return '<i class="far fa-file-excel"></i> ' + dt.i18n('buttons.csv', 'CSV');
    },

    action: function (e, dt, button, config) {
      var url = dt.ajax.url() || window.location.href;
      var params = _buildParams(dt, 'csv');

      _downloadFromUrl(url, params);
    }
  };

  DataTable.ext.buttons.pdf = {
    className: 'buttons-pdf',

    text: function (dt) {
      return '<i class="far fa-file-pdf"></i> ' + dt.i18n('buttons.pdf', 'PDF');
    },

    action: function (e, dt, button, config) {
      var url = _buildUrl(dt, 'pdf');
      window.location = url;
    }
  };

  DataTable.ext.buttons.postPdf = {
    className: 'buttons-pdf',

    text: function (dt) {
      return '<i class="far fa-file-pdf"></i> ' + dt.i18n('buttons.pdf', 'PDF');
    },

    action: function (e, dt, button, config) {
      var url = dt.ajax.url() || window.location.href;
      var params = _buildParams(dt, 'pdf');

      _downloadFromUrl(url, params);
    }
  };

  DataTable.ext.buttons.print = {
    className: 'buttons-print',

    text: function (dt) {
      return '<i class="fas fa-print"></i> ' + dt.i18n('buttons.print', 'Imprimir');
    },

    action: function (e, dt, button, config) {
      var url = _buildUrl(dt, 'print');
      window.location = url;
    }
  };

  DataTable.ext.buttons.reset = {
    className: 'buttons-reset',

    text: function (dt) {
      return '<i class="fas fa-undo"></i> ' + dt.i18n('buttons.reset', 'Reiniciar');
    },

    action: function (e, dt, button, config) {
      dt.search('');
      dt.columns().search('');
      dt.draw();
    }
  };

  DataTable.ext.buttons.reload = {
    className: 'buttons-reload',

    text: function (dt) {
      return '<i class="fas fa-sync"></i> ' + dt.i18n('buttons.reload', 'Recargar');
    },

    action: function (e, dt, button, config) {
      dt.draw(false);
    }
  };

  DataTable.ext.buttons.create = {
    className: 'buttons-create',

    text: function (dt) {
      return '<i class="fas fa-plus"></i> ' + dt.i18n('buttons.create', 'Nuevo');
    },

    action: function (e, dt, button, config) {
      window.location = window.location.href.replace(/\/+$/, "") + '/create';
    }
  };

  DataTable.ext.buttons.new = {
    className: 'buttons-create',

    text: function (dt) {
      return '<i class="fas fa-plus"></i> ' + dt.i18n('buttons.create', 'Nuevo');
    },

    action: function (e, dt, button, config) {
      modal.loadModal(window.location.href.replace(/\/+$/, "") + '/create', 'new');
    }
  };

  if (typeof DataTable.ext.buttons.copyHtml5 !== 'undefined') {
    $.extend(DataTable.ext.buttons.copyHtml5, {
      text: function (dt) {
        return '<i class="far fa-copy"></i> ' + dt.i18n('buttons.copy', 'Copiar');
      }
    });
  }

  if (typeof DataTable.ext.buttons.colvis !== 'undefined') {
    $.extend(DataTable.ext.buttons.colvis, {
      text: function (dt) {
        return '<i class="fas fa-eye"></i> ' + dt.i18n('buttons.colvis', 'Columnas a mostrar');
      }
    });
  }
})(jQuery, jQuery.fn.dataTable);
