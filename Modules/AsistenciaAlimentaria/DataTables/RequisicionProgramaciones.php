<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionCantidadDotacion;
use App\Models\Region;

class RequisicionProgramaciones extends CustomDataTable{

    private $regiones = [];
    private $acciones = [];
    private $beneficiarios = [];

  public function dataTable($query){
    $tabla = datatables($query)
    ->editColumn('folio',function($query){
      $requisicion = '';
      if(RequisicionProgramacion::where('programacion_id',$query->programacion_id)->count() > 0)
        $requisicion = 'esrequisicion';
      return $query->folio.'<span id="'.$query->programacion_id.'" class="pdi '.$requisicion.'"></span>';
    })
    ->editColumn('estado',function($item){
        $var = '';
        switch ($item->estado) {
            case 'ENVIADA':
                $var='warning';
                break;
            case 'RECIBIDA':
                $var='success';
                break;
            case 'NO RECIBIDA':
                $var='danger';
                break;
            default:
                $var='light';
                break;
        }
        return "<span class='badge badge-pill badge-$var'> $item->estado</span>";
    })
    /* ->addColumn('dotaciones',function($query){
      $estado = Requisicion::find($this->requisicion->id)->estadoprograma->estado->nombre;
      if(strtoupper($estado)!='ENVIADA')
        return "<button class='btn btn-dark btn-sm' onClick='modal.loadModal(\"/alimentarios/requisicion/{$this->requisicion->id}/programacion/$query->programacion_id/dotaciones/create\",\"edit\")'><i class='fas fa-dolly-flatbed'></i></button>";
      else
        return "<button class='btn btn-dark btn-sm' onClick='modal.loadModal(\"/alimentarios/requisicion/{$this->requisicion->id}/programacion/$query->programacion_id/dotaciones\",\"edit\")'><i class='fas fa-dolly-flatbed'></i></button>";
    })
    ->addColumn('cancelar',function($query){
        return "<button class='btn btn-danger btn-sm' onClick=''></button>";
    }) */
    ->addColumn('dotaciones',function($query){
        $estado = Requisicion::find($this->requisicion->id)->estadoprograma->estado->nombre;
        if(strtoupper($estado)!='ENVIADA')
            return "<button class='btn btn-dark btn-sm' onClick='modal.loadModal(\"/alimentarios/requisicion/{$this->requisicion->id}/programacion/$query->programacion_id/dotaciones/create\",\"edit\")'><i class='fas fa-dolly-flatbed'></i></button>";
        else
        return "<button class='btn btn-dark btn-sm' onClick='modal.loadModal(\"/alimentarios/requisicion/{$this->requisicion->id}/programacion/$query->programacion_id/dotaciones\",\"edit\")'><i class='fas fa-dolly-flatbed'></i></button>";
    })
    ->addColumn('cancelar',function($query){
        if($query->estado != 'RECIBIDA' && !$query->motivo_cancelacion) {
            $r = route('alimentarios.programaciones.destroy',[$query->requisicion_id, $query->programacion_id]);
            return "<button class='btn btn-danger btn-sm' onClick='swalCancelacion(\"$r\",\"DELETE\",\"-seleccions\")'><i class='fas fa-ban'></i></button>";
        }
        return $query->motivo_cancelacion;
    })
    ->filterColumn('cancelar',function($query,$keywords){
        if($keywords==0)
          $query->whereNotNull('alim_programacion_requisiciones.motivo_cancelacion');
        else if($keywords==1)
          $query->whereNull('alim_programacion_requisiciones.motivo_cancelacion');
    })
    ->rawColumns(['folio','dotaciones','estado','cancelar'])
    ->with('regiones', $this->regiones)
    ->with('acciones', $this->acciones)
    ->with('beneficiarios', $this->beneficiarios);
    return $tabla;
  }

  public function query(){
    $model = Programacion::join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
    ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
    ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
    ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
    ->join('alim_programacion_requisiciones','alim_programacion_requisiciones.programacion_id','alim_programacion.id')
    ->join('alim_requisiciones','alim_requisiciones.id','alim_programacion_requisiciones.requisicion_id')
    ->join('estados_programas','estados_programas.id','alim_programacion_requisiciones.estado_programa_id')
    ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
    ->leftJoin('alim_requisiciones_datoscarga','alim_requisiciones_datoscarga.programacion_requisicion_id','alim_programacion_requisiciones.id')
    ->leftJoin('alim_datoscarga','alim_datoscarga.id','alim_requisiciones_datoscarga.datocarga_id')
    ->leftJoin('alim_choferesproveedor','alim_choferesproveedor.id','alim_datoscarga.choferproveedor_id')
    ->leftJoin('alim_entregasproveedor','alim_entregasproveedor.requisicion_datocarga_id','alim_requisiciones_datoscarga.id')
    ->where('cat_ejercicios.anio',config('asistenciaalimentaria.anioActual'))
    ->where('alim_programacion_requisiciones.requisicion_id',$this->requisicion->id)
    ->where(function($q){
      $q->whereNull('alim_programacion_requisiciones.deleted_at')
        ->orWhere(function($qu){
          $qu->whereNotNull('alim_programacion_requisiciones.motivo_cancelacion')
              ->whereNotNull('alim_programacion_requisiciones.deleted_at');
        });
    });

    $select=[
      'alim_requisiciones.id as requisicion_id',
      'alim_datoscarga.placas',
      'alim_datoscarga.ruta',
      'alim_choferesproveedor.nombre as chofer',
      DB::raw('date_format(alim_datoscarga.fecha,"%d/%m/%Y") as fechacarga'),
      DB::raw('date_format(alim_entregasproveedor.fecha_entrega,"%d/%m/%Y") as fechaentrega'),
      DB::raw('
        if(
            alim_entregasproveedor.observaciones is null,
            (
                select concat(per.nombre," ",per.primer_apellido," ",per.segundo_apellido," - ",car.nombre) from alim_comites ac
                inner join personas per on per.id = ac.persona_id
                inner join cargos_programas cp on cp.id = ac.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where ac.id = alim_entregasproveedor.recibe_comite_id
            ),
            alim_entregasproveedor.observaciones
        ) as recibio
      '),
      'cat_ejercicios.anio as ejercicio',
      'alim_programacion_requisiciones.requisicion_id as requisicion',
      'alim_programacion_requisiciones.motivo_cancelacion',
      'cat_estados.nombre as estado',
      'alim_requisiciones.num_bimestre as bimestre',
      'alim_requisiciones.num_entrega as parte',
      'alim_alimentarios.foliodif as folio',
      DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as essedesol'),
      'cat_regiones.id as cve_region',
      'cat_regiones.nombre as region',
      'cat_distritos.id as cve_distrito',
      'cat_distritos.nombre as distrito',
      'cat_municipios.id as cve_municipio',
      'cat_municipios.nombre as municipio',
      'cat_localidades.cve_localidad',
      'cat_localidades.nombre as localidad',
      'alim_alimentarios.sublocalidad as sublocalidad',
      'alim_programacion.id as programacion_id',
      DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        inner join cargos_programas cp on cp.id = c.cargo_programa_id
        inner join cat_cargos car on car.id = cp.cargo_id
        where c.programacion_id = alim_programacion.id and car.nombre LIKE 'PRESIDENTE(A)' and c.deleted_at is null) as pre"),
      DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        inner join cargos_programas cp on cp.id = c.cargo_programa_id
        inner join cat_cargos car on car.id = cp.cargo_id
        where c.programacion_id = alim_programacion.id and car.nombre LIKE 'VOCAL DE ABASTO Y ALMACÉN' and c.deleted_at is null) as vocal1"),
      DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        inner join cargos_programas cp on cp.id = c.cargo_programa_id
        inner join cat_cargos car on car.id = cp.cargo_id
        where c.programacion_id = alim_programacion.id and car.nombre LIKE 'VOCAL DE ORIENTACIÓN ALIMENTARIA Y SALUD' and c.deleted_at is null) as vocal2"),
      DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        inner join cargos_programas cp on cp.id = c.cargo_programa_id
        inner join cat_cargos car on car.id = cp.cargo_id
        where c.programacion_id = alim_programacion.id and car.nombre LIKE 'SECRETARIO(A)' and c.deleted_at is null) as vocal3"),
      DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        inner join cargos_programas cp on cp.id = c.cargo_programa_id
        inner join cat_cargos car on car.id = cp.cargo_id
        where c.programacion_id = alim_programacion.id and car.nombre LIKE 'TESORERO(A)' and c.deleted_at is null) as vocal4"),
      DB::raw("(select id from alim_recibos r where r.programacion_requisicion_id = alim_programacion_requisiciones.id and r.deleted_at is null) as ficha_pago")
    ];

    if($this->requisicion->programa_id == config('asistenciaalimentaria.desayunosId')){
        $model->join('alim_desayunos','alim_desayunos.alimentario_id','alim_alimentarios.id')
        ->join('cat_escuelas','cat_escuelas.id','alim_desayunos.escuela_id');
        array_push($select,
            'cat_escuelas.nombre as escuela',
            'cat_escuelas.clave as cve_esc'
        );
    }
    if($this->requisicion->programa_id == config('asistenciaalimentaria.sujetosId')){
        $model->join('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id');
        array_push($select,
            'alim_sujetos.nombre_asociacion'
        );
    }

    $i = 0;
    foreach ( TipoAccion::where('programa_id',$this->requisicion->programa_id)->get() as $subprograma) {
        $i++;
        array_push($select,
            DB::raw("(select cantidad_beneficiarios from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null) as beneficiarios$i"),
            DB::raw("(select cantidad_dotaciones from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null) as dotacion$i")
        );
    }
    array_push($select,DB::raw("(select sum(cantidad_beneficiarios) from alim_cantidadesdotaciones_requisiciones cd
        inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
        where pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null) as beneficiariosT"));
    array_push($select,DB::raw("(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
        inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
        where pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null) as dotacionT"));

    $model->select($select);

    foreach (Region::All() as $region) {
        $aux = clone $model;
        $this->regiones[$region->id] = $aux->where('cat_regiones.id',$region->id)->count();
    }
    foreach (TipoAccion::where('programa_id',$this->requisicion->programa_id)->get() as $accion) {
        $this->acciones[$accion->id] = RequisicionCantidadDotacion::wherehas('dotacion',function($query) use($accion){
            $query->where('tipoaccion_id',$accion->id);
        })->whereHas('requisicionprogramacion',function($query){
            $query->where('requisicion_id',$this->requisicion->id);
        })->sum('cantidad_dotaciones');
        $this->beneficiarios[$accion->id] = RequisicionCantidadDotacion::wherehas('dotacion',function($query) use($accion){
            $query->where('tipoaccion_id',$accion->id);
        })->whereHas('requisicionprogramacion',function($query){
            $query->where('requisicion_id',$this->requisicion->id);
        })->sum('cantidad_beneficiarios');
    }
    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
        'url' =>route('alimentarios.requisicion.programaciones.datos',$this->requisicion->id),
        'type'=>'POST'
        ])
    // ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      'dotaciones'      => ['data' => 'dotaciones', 'searchable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false],
      'cancelar_folio'  => ['data' => 'cancelar', 'exportable' => false, 'printable' => false],
      'ficha_pago'      => ['title' => 'Ficha Pago', 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false],
      'requisicion'     => ['name' => 'alim_programacion_requisiciones.requisicion_id', 'data' => 'requisicion', 'title' => 'Requisición', 'searchable' => false],
      'estado'          => ['name' => 'cat_estados.nombre'],
      'folio'           => ['name' => 'alim_alimentarios.foliodif', 'data' => 'folio', 'title' => 'FolioDIF'],
      'sedesol'         => ['name' => 'alim_alimentarios.essedesol', 'data' => 'essedesol', 'title' => 'Sedesol','exportable' => true,'visible' => false],
      'cve_region'      => ['name' => 'cat_regiones.id', 'visible' => false],
      'region'          => ['name' => 'cat_regiones.nombre'],
      'cve_distrito'    => ['name' => 'cat_distritos.id', 'visible' => false],
      'distrito'        => ['name' => 'cat_distritos.nombre'],
      'cve_municipio'   => ['name' => 'cat_municipios.id', 'visible' => false],
      'municipio'       => ['name' => 'cat_municipios.nombre'],
      'cve_localidad'   => ['name' => 'cat_localidades.cve_localidad', 'visible' => false],
      'localidad'       => ['name' => 'cat_localidades.nombre'],
      'sublocalidad'    => ['name' => 'alim_alimentarios.sublocalidad'],
      'pre'             => ['title' => 'Presidente(a)','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false],
      'placas'          => ['name' => 'alim_datoscarga.placas', 'visible' => false],
      'ruta'            => ['name' => 'alim_datoscarga.ruta', 'visible' => false],
      'chofer'          => ['name' => 'alim_choferesproveedor.nombre', 'data' => 'chofer', 'visible' => false],
      'fecha carga'     => ['data' => 'fechacarga', 'searchable' => false, 'visible' => false],
      'fecha entrega'   => ['data' => 'fechaentrega', 'searchable' => false, 'visible' => false],
      'recibió'         => ['data' => 'recibio', 'searchable' => false, 'visible' => false]
    ];
    if($this->requisicion->programa_id==config('asistenciaalimentaria.ccncId')){
        $columns['vocal1']  = ['title' => 'Vocal de abasto y almacén','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns['vocal2']  = ['title' => 'Vocal de orientación alimentaria y salud','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
    }
    if($this->requisicion->programa_id==config('asistenciaalimentaria.sujetosId')){
        $columns['vocal3']  = ['title' => 'SECRETARIO(A)','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns['vocal4']  = ['title' => 'TESORERO(A)','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns['nombre_asociacion'] = ['title' => 'Nombre Asociación', 'visible' => false, 'searchable'=>false, 'orderable'=>false];
    }
    if($this->requisicion->programa_id == config('asistenciaalimentaria.desayunosId')){
        $columns['vocal3']  = ['title' => 'SECRETARIO(A)','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns['vocal4']  = ['title' => 'TESORERO(A)','orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns['escuela'] = ['title' => 'Escuela', 'name' => 'cat_escuelas.nombre as escuela'];
        $columns['cve_esc'] = ['title' => 'Clave Escuela', 'name' => 'cat_escuelas.clave as cve_esc'];
    }

    $i = 0;
    foreach (  TipoAccion::where('programa_id',$this->requisicion->programa_id)->get() as $subprograma) {
        $i++;
        $columns["beneficiarios$i"] = ['title' => 'Benef. '.explode('(',explode(')',$subprograma->nombre)[0])[1], 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
        $columns["dotacion$i"] = ['title' => 'Dot. '.explode('(',explode(')',$subprograma->nombre)[0])[1], 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
    }
    $columns["beneficiariosT"] = ['title' => 'Benef. Total', 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
    $columns["dotacionT"] = ['title' => 'Dot. Total', 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
    return $columns;
  }

  protected function getBuilderParameters(){
    $estado = Requisicion::find($this->requisicion->id)->estadoprograma->estado->nombre;
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        //'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'initComplete' => ' function () {
          var api = this.api()
          api.columns().every(function () {
              var column = this;
              if(api.table().init().columns[column[0]].searchable){
                  var input = document.createElement("input")
                  $(input).addClass("column-filter")
                  $(input).addClass("text-center")
                  $(input).attr("placeholder","Filtrar "+column.header().textContent.toLowerCase())
                  $(input).appendTo($(column.footer()).empty())
                  .on("change", function () {
                      column.search($(this).val(), false, false, true).draw()
                  });
              }
          });
      }',
      'buttons' => [
        'postExcel','reset', 'reload',
        [
          'text'   => '<i class="far fa-paper-plane"></i> Enviar al proveedor',
          'action' => 'function() {
            if('.strtoupper($estado).'!="ENVIADA")
              enviarProveedor("'.route('alimentarios.'.($this->requisicion->programa_id == config('asistenciaalimentaria.ccncId') ? 'ccnc' : ($this->requisicion->programa_id == config('asistenciaalimentaria.sujetosId') ? 'sujetos' : 'desayunos')).'.requisiciones.update',[$this->requisicion->id]).'")
          }'
        ],
        [
            'extend'=>'colvis',
            'collectionLayout' => 'fixed three-column',
            'text' => '<i class="fas fa-columns"></i> Columnas',
            'columns' => [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22]
        ],
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ],
    ];

    return $builderParameters;
  }

  protected function filename(){
    return 'Requisición al ' . date('Ymd');
  }
}
