<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Sujeto;
use App\Models\CargoPrograma;
use App\Models\AniosPrograma;
use App\Models\Empleado;
use Modules\AsistenciaAlimentaria\Entities\Desayuno;
use Modules\AsistenciaAlimentaria\Entities\Financiamiento;

class PlaneacionAlimentarios extends CustomDataTable{

  public function dataTable($query){
    $cargosPrograma=CargoPrograma::where('programa_id',$this->anio_programa->programa_id)->count();
    $tabla = datatables()->eloquent($query)
    ->editColumn('estado',function($query){
      $color = 'warning';
      switch ($query->estado) {
        case 'ALTA':
          $color = 'info';
          break;
        case 'ACTIVA':
          $color = 'success';
          break;
        case 'CANCELADA':
          $color = 'danger';
          break;
        case 'BAJA':
          $color = 'danger';
          break;
        case 'ACTIVA - PENDIENTE':
          $color = 'info';
          break;
        default:
          break;
      }
      $ruta = route('alimentarios.estados');
      return "<button class='btn btn-block btn-outline-$color btn-sm' onClick='modal.loadModal(\"$ruta\",\"estados\",{programacion_id:$query->programacion_id,vista:true})'></i>$query->estado</button>";
    })
    ->addColumn('tBenef',function ($alimentario){
      $beneficiarios = $alimentario->numbeneficiarios;
      $ruta = route('alimentarios.programacion.beneficiarios.index',$alimentario->programacion_id);
      $aux =(object)['btn'=>$beneficiarios==0?'danger':($beneficiarios<30?'warning':'info'),'icon'=>$beneficiarios<30?'edit':'eye'];
      if(strpos($alimentario->estado,"BLOQUEADA")!==false)
        $aux->btn='secondary';
      return "<button class='btn btn-block btn-outline-$aux->btn btn-sm' onClick='modal.loadModal(\"$ruta\",\"list\")'>$beneficiarios/30 &nbsp; <i class='fas fa-$aux->icon'></i></button>";
    })
    ->addColumn('convenio',function ($alimentario){
      $convenio = $alimentario->programacion->last()->convenio;//? $alimentario->programacion->last()->convenio : false;
      $ruta = $convenio ? route('alimentarios.programacion.convenios.show',['programacion_id'=>$alimentario->programacion_id,'convenio_id'=>$convenio->id]) : route('alimentarios.programacion.convenios.create',$alimentario->programacion_id);
      $aux =(object)['btn'=>$convenio?'info':'danger','icon'=>$convenio?'eye':'edit'];
      return "<button class='btn btn-block btn-outline-$aux->btn btn-sm' onClick='modal.loadModal(\"$ruta\",\"create\")'><i class='fas fa-$aux->icon'></i></button>";
    })
    ->addColumn('comite',function ($alimentario) use ($cargosPrograma){
      $comite=$alimentario->numcomites;//$comite = $alimentario->programacion->last()->comite->count();
      $ruta = route('alimentarios.programacion.comites.index',$alimentario->programacion_id);
      $aux = $comite==0?'danger':($cargosPrograma > $comite?'warning':'info');
      $total = $cargosPrograma;
      return "<button class='btn btn-block btn-outline-$aux btn-sm' onClick='modal.loadModal(\"$ruta\",\"progShow\")'>$comite/$total &nbsp; <i class='fas fa-eye'></i></button>";
    })
    ->filterColumn('essedesol',function($query,$keywords){
        $search = strtolower($keywords)=='si' ? 1 : (strtolower($keywords)=='no'? 0 : 3 );
        $query->orwhere('alim_alimentarios.essedesol',$search);
    })
    ->filterColumn('esampliacion',function($query,$keywords){
        $search = strtolower($keywords)=='si' ? 1 : (strtolower($keywords)=='no'? 0 : 3 );
        $query->orwhere('alim_programacion.esampliacion',$search);
    })
    ->filterColumn('esdesarrollo',function($query,$keywords){
        if (strtolower($keywords)=='si')
            $query->orwhereNotNull('alim_sujetos.dedesarrollo_programa_id');
        else
            $query->orwhereNull('alim_sujetos.dedesarrollo_programa_id');
    })
    ->filterColumn('financiamiento',function($query,$keywords){
        if (strtolower($keywords)=='si')
            $query->orwhere('alim_cat_financiamientos.nombre',"PRODUCTO FINANCIERO");
        else
            $query->orwhere('alim_cat_financiamientos.nombre','!=',"PRODUCTO FINANCIERO");
    })
    ->addColumn('progReqs',function($query){
      $ruta = route('alimentarios.programacion.requisiciones',$query->programacion_id);
      return "<button class='btn btn-block btn-outline-light btn-sm' onClick='modal.loadModal(\"$ruta\",\"progReq\")'><i class='far fa-file-pdf'></i> Recibos</button>";
		})
		->addColumn('extension',function($alimentario) {
      $desayuno = Desayuno::where('alimentario_id',$alimentario->id)->first();
      return  $desayuno ? ($desayuno->escuela->padre ? $desayuno->escuela->padre->nombre : '') : '';
    });
    /* if ($this->programa_id == config('asistenciaalimentaria.sujetosId')) {
      $tabla->addColumn('nombre_asociacion',function ($alimentario) {
        return strtoupper(Sujeto::where('alimentario_id',$alimentario->id)->first()->nombre_asociacion);
      });
    } */
    /* ->addColumn('show',function($alimentario){
      return '<a onClick="cargarModal('."'".5."')".'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </a>';
    })
    ->addColumn('edit',function($alimentario){
      if ($alimentario->estado == null)
      return '<a onClick="cargarModal('."'".5."')".'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</a>';
      return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
    })
    ->addColumn('destroy',function($alimentario){
      if ($alimentario->estado == null)
      return '<a class="btn btn-xs btn-danger" onclick="eliminaralimentario('."'".5."')".'"><i class="fa fa-remove"> Eliminar</a>';
      return '<a class="btn btn-xs btn-success"><i class="fa fa-check"> Reactivar</a>';
    }) */
    $tabla->rawColumns(['convenio','tBenef','comite','estado','progReqs']);

    return $tabla;
  }

  public function query(){
    $planeacion = Alimentario::join('alim_programacion','alim_programacion.alimentario_id','alim_alimentarios.id')
    ->join('estados_programas','estados_programas.id','alim_programacion.estado_programa_id')
    ->leftjoin('alim_cat_financiamientos','alim_cat_financiamientos.id','alim_programacion.financiamiento_id')
    ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
    ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
    ->leftjoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id');
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.sujetosId')) {
      $planeacion->leftJoin('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id');
      //Si el usuario pertenece a otra área que no sea ASISTENCIA ALIMENTARIA
      if(!auth()->user()->hasRoles(['SUPERADMIN'])) {
        $area = Empleado::join('cat_areas','cat_areas.id','empleados.area_id')
        ->where('cat_areas.nombre','like','%DESARROLLO FAMILIAR%')
        ->where('empleados.persona_id',auth()->user()->persona->id)
        ->first() ? true : false;
        if($area) {
          $planeacion->whereNotNull('alim_sujetos.dedesarrollo_programa_id');
        } else {
          $planeacion->whereNull('alim_sujetos.dedesarrollo_programa_id');
        }
      }
    }
    $planeacion->where('alim_programacion.anios_programa_id',$this->anio_programa->id);
    if(auth()->user()->regionusuario)
      $planeacion->where('cat_regiones.id',auth()->user()->regionusuario->region_id);
    $select = [
      'alim_alimentarios.id',
      'alim_programacion.id as programacion_id',
      'alim_alimentarios.foliodif',
      'cat_estados.nombre as estado',
      'cat_municipios.id as clv_mun',
      'cat_municipios.nombre as municipio',
      'cat_localidades.cve_localidad as clv_loc',
      'cat_localidades.nombre as localidad',
      'cat_distritos.nombre as distrito',
      'cat_regiones.nombre as region',
      'alim_alimentarios.sublocalidad',

      DB::raw('(select count(*) from alim_beneficiarios where programacion_id=alim_programacion.id and alim_beneficiarios.deleted_at is null) as numbeneficiarios'),
      DB::raw('(select count(*) from alim_convenios where programacion_id=alim_programacion.id and alim_convenios.deleted_at is null) as convenio'),
      DB::raw('(select count(*) from alim_comites where programacion_id=alim_programacion.id and alim_comites.deleted_at is null) as numcomites')
    ];
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.ccncId')){
        array_push($select,
            DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as essedesol'),
            DB::raw('if(alim_programacion.esampliacion = 1,"SI","NO") as esampliacion'),
            DB::raw('(select ifnull(clave,"NO") from alim_alimentarios aa left join alim_desayunos ad on aa.id = ad.alimentario_id left join cat_escuelas e on e.id=ad.escuela_id where aa.id = alim_alimentarios.id and ad.deleted_at is null ) as escuela')
        );
    }
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.sujetosId')){
        array_push($select,
            DB::raw('if(alim_sujetos.dedesarrollo_programa_id is not null,"SI","NO") as esdesarrollo'),
            DB::raw('if(alim_programacion.esampliacion = 1,"SI","NO") as esampliacion'),
            DB::raw('if(alim_cat_financiamientos.nombre  = "PRODUCTO FINANCIERO","SI","NO") as financiamiento'),
           'alim_sujetos.nombre_asociacion'
        );
    }
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.desayunosId')){
        $planeacion->join('alim_desayunos','alim_desayunos.alimentario_id','alim_alimentarios.id')
        ->join('cat_escuelas','cat_escuelas.id','alim_desayunos.escuela_id')
        ->leftJoin('cat_nivelesescuela','cat_nivelesescuela.id','cat_escuelas.nivel_id');
        array_push($select,
            'cat_escuelas.nombre as escuela',
            'cat_escuelas.clave as cve_escuela',
            'cat_nivelesescuela.nombre as nivel'
        );
    }
    /* if($this->anio_programa->programa_id == config('asistenciaalimentaria.sujetosId')){
        array_push($select,
            DB::raw('if(alim_sujetos.dedesarrollo_programa_id = 1,"SI","NO") as esdesarrollo')
        );
    } */
    // if($this->request->has('action')){
      $i = 0;
      foreach ( CargoPrograma::where('programa_id',$this->anio_programa->programa_id)->get() as $cargo) {
        $as = 'cargo'.($i++);
        array_push($select,DB::raw("(select concat_ws(' ',nombre,primer_apellido,segundo_apellido)
        from personas p inner join alim_comites c on p.id = c.persona_id
        where c.programacion_id = alim_programacion.id and cargo_programa_id=$cargo->id and c.deleted_at is null) as $as"));
      }
      $i = 0;
      foreach ( TipoAccion::where('programa_id',$this->anio_programa->programa_id)->get() as $subprograma) {
        $as = 'subprograma'.($i++);
        array_push($select,DB::raw("(select count(*)
        from alim_beneficiarios b inner join alim_tiposaccion ta on ta.id = b.tipoaccion_id
        where ta.id={$subprograma->id} and b.programacion_id=alim_programacion.id and b.deleted_at is null) as $as"));
      }
    // }
    $planeacion->select($select);
    return $planeacion;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      'folio'                => ['data' => 'foliodif', 'name' => 'alim_alimentarios.foliodif'], //0
      'estado'               => ['data' => 'estado', 'name' => 'cat_estados.nombre'],           //1
      'region'               => ['name' => 'cat_regiones.nombre', 'visible'=>false],            //2
      'distrito'             => ['name' => 'cat_distritos.nombre'],                             //3
      'municipio'            => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],     //4
      'clave municipio'      => ['data' => 'clv_mun', 'name' => 'cat_municipios.id','visible'=>false],
      'localidad'            => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
      'clave localidad'      => ['data' => 'clv_loc', 'name' => 'cat_localidades.cve_localidad','visible'=>false],
      'sublocalidad'         => ['data' => 'sublocalidad', 'name' => 'alim_alimentarios.sublocalidad','visible'=>false],//8
    ];
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.ccncId')){
        $columns['sedesol'] = ['data' => 'essedesol','visible'=>false];
        $columns['ampliacion'] = ['data' => 'esampliacion','visible'=>false];
        $columns['escuela'] = ['data' => 'escuela', 'searchable'=>false, 'visible'=>false];
    }

    if($this->anio_programa->programa_id == config('asistenciaalimentaria.desayunosId')){
        $columns['escuela']     = ['data' => 'escuela', 'name'=>'cat_escuelas.nombre'];
        $columns['cve_escuela'] = ['data' => 'cve_escuela', 'name'=>'cat_escuelas.clave'];
        $columns['nivel']       = ['data' => 'nivel', 'name'=>'cat_nivelesescuela.nombre'];
        $columns['extension']   = ['title' => 'Extensión', 'data' => 'extension', 'searcheable'=> false,'orderable'  => false,'exportable' => false];
    }
    if($this->anio_programa->programa_id == config('asistenciaalimentaria.sujetosId')){
        $columns['nombre_asociacion'] = ['data' => 'nombre_asociacion','name' => 'alim_sujetos.nombre_asociacion'];
        $columns['ampliacion'] = ['data' => 'esampliacion', 'visible'=>false];
        $columns['desarrollo'] = ['data' => 'esdesarrollo' , 'visible'=>false];
        $columns['producto_financiero'] = ['data' => 'financiamiento'];
    }
    $columns['Beneficiarios'] = ['data' => 'tBenef' , 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false];
    $columns['comite']        = ['data' => 'comite' , 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false];
    $columns['convenio']      = ['data' => 'convenio', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false];
    if(auth()->user()->hasRoles(['SUPERADMIN','REGIONAL']))
      $columns['recibos']       = ['data' => 'progReqs', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false];
    // if($this->request->has('action')){
      //'datos de comite'
      $i = 0;
      foreach ( CargoPrograma::where('programa_id',$this->anio_programa->programa_id)->get() as $cargo) {
        $cargo = $cargo->cargo;
        $as = 'cargo'.($i++);
        $columns[$as] = ['title' => $cargo->nombre, 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
      }
      //'las 6 categorias',
      $i = 0;
      foreach (  TipoAccion::where('programa_id',$this->anio_programa->programa_id)->get() as $subprograma) {
        $as = 'subprograma'.($i++);//str_replace([' ','(',')','-'],[''],$subprograma->nombre);
        $columns[$as] = ['title' => explode('(',explode(')',$subprograma->nombre)[0])[1], 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true, 'visible' => false];
      }
    // }
    //'columna para sujetos'
    if ($this->programa_id == config('asistenciaalimentaria.sujetosId')) {
      $columns['esdesarrollo'] = ['data' => 'esdesarrollo' , 'orderable' => false, 'searchable' => true, 'exportable' => false, 'printable' => false];
    }
    return $columns;
  }

  protected function getBuilderParameters(){
    $nameaux = $this->programa_id == config('asistenciaalimentaria.sujetosId') ? 'alim_sujetos.dedesarrollo_programa_id' : '';
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'initComplete' => ' function () {
            var api = this.api()
            api.columns().every(function () {
                var column = this;
                if(api.table().init().columns[column[0]].searchable){
                    var input = document.createElement("input")
                    $(input).addClass("column-filter")
                    $(input).addClass("text-center")
                    $(input).attr("placeholder","Filtrar "+column.header().textContent.toLowerCase())
                    $(input).appendTo($(column.footer()).empty())
                    .on("change", function () {
											column.search($(this).val(), false, false, true).draw()
                    });
                }
            });
        }',
      'drawCallback'=>"function(){
        params = window.LaravelDataTables.alimentarios.ajax.params().columns.filter(o=>
          o.search!=undefined
        ).filter(o=>
          o.search.value!=''
        ).map(o=>{
          let name= o.name || o.data
              if (name=='esdesarrollo')
                name='alim_sujetos.dedesarrollo_programa_id'
              else if (name=='financiamiento')
                name='alim_cat_financiamientos.nombre'
              else if (name=='esampliacion')
                name='alim_programacion.esampliacion'
              else if (name=='essedesol')
                name ='alim_alimentarios.essedesol'
              return {name:name ,value:o.search.value}})
        $.ajax({
          url: '".route('alimentarios.'.($this->anio_programa->programa->id == config('asistenciaalimentaria.ccncId') ? 'ccnc' : ($this->anio_programa->programa->id == config('asistenciaalimentaria.desayunosId') ? 'desayunos' : 'sujetos')).'.beneficiarios.consulta')."',
          type: 'POST',
          data: {filtros:params},
          success: function(response) {
            $('#text-benef').html(response);
          },
          error: function(response){
            console.log('fail')
          }
        });
      }",
      'buttons' => [
        'pdf' , 'excel',
        'reset', 'reload',
        [
            'extend'=>'colvis',
            'collectionLayout' => 'fixed three-column',
            'text' => '<i class="fas fa-columns"></i> Columnas',
            'columns' => [0,1,2,3,4,5,6,7,8,9,10,11,12]
        ],
        [
            'extend'=>'colvisGroup',
            'text' => '<i class="fas fa-eye"></i> Inf. Cargos',
            'show' => [12,13,14,15,16,17,18,19,20],
            'hide' => [21,22,23,24,25,26,27,28,29,30]
        ],
        [
            'extend'=>'colvisGroup',
            'text' => '<i class="fas fa-eye"></i> Inf. Programas',
            'show' => [21,22,23,24,25,26,27,28,29,30],
            'hide' => [12,13,14,15,16,17,18,19,20]
        ],
        [
            'extend' => 'colvisRestore',
            'text' => '<i class="fas fa-eye-slash"></i> Restaurar columnas'
				],
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'stateSave'  => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
      //'select' => ['style'=> 'multi']
    ];
    if(auth()->user()->hasRoles(['SUPERADMIN'])) {
			array_push($builderParameters['buttons'],
				[
					'text'=>'<i class="fas fa-lock"></i> Bloquear filas encontradas',
					'action' => "function(){
						params = window.LaravelDataTables.alimentarios.ajax.params().columns.filter(o=>
								o.search != undefined
						)
						.filter(o=>
								o.search.value != ''
						).map(o=>{
              let name= o.name || o.data
              if (name=='esdesarrollo')
                name='alim_sujetos.dedesarrollo_programa_id'
              else if (name=='financiamiento')
                name='alim_cat_financiamientos.nombre'
              else if (name=='esampliacion')
                name='alim_programacion.esampliacion'
              else if (name=='essedesol')
                name ='alim_alimentarios.essedesol'
              return {name:name ,value:o.search.value}
            })
            var data = {}
            data.filtros = params
            if( $('#desarrollo').prop('checked') ){
                data.reqFor = 'desarrollo'
            }
						console.log(params);
						transaccion.miniAjax('".route('alimentarios.programacion.lock',$this->anio_programa->programa_id)."',\"POST\",{filtros:params})
					}"
				],
				[
					'text'=>'<i class="fas fa-unlock-alt"></i> Desbloquear filas encontradas',
					'action' => "function(){
						params = window.LaravelDataTables.alimentarios.ajax.params().columns.filter(o=>
								o.search != undefined
						)
						.filter(o=>
								o.search.value != ''
						).map(o=>{
              let name= o.name || o.data
              if (name=='esdesarrollo')
                name='alim_sujetos.dedesarrollo_programa_id'
              else if (name=='financiamiento')
                name='alim_cat_financiamientos.nombre'
              else if (name=='esampliacion')
                name='alim_programacion.esampliacion'
              else if (name=='essedesol')
                name ='alim_alimentarios.essedesol'
              return {name:name ,value:o.search.value}
            })
            var data = {}
            data.filtros = params
            if( $('#desarrollo').prop('checked') ){
                data.reqFor = 'desarrollo'
            }
						console.log(params);
						transaccion.miniAjax('".route('alimentarios.programacion.unlock',$this->anio_programa->programa_id)."',\"POST\",{filtros:params})
					}"
				]
			);
		}
    return $builderParameters;
  }

  protected function filename(){
    return 'CCNC al ' . date('Ymd');
  }
}
