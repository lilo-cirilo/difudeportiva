<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Beneficiario;
use App\Models\AniosPrograma;
use Modules\AsistenciaAlimentaria\Entities\RegionesUsuarios;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

/*
  Este datatable se divide en general y por programación,
  algunos apartados están manipulados para que aparezcan cuando el datatable haya sido solicitado
  por medio de una petición desde una programación
*/
class Beneficiarios extends CustomDataTable{
    /**
     * Construye el dataTable
     * @param mixed $query Resultado obtenido a partir del método query()
     * @return \Yajra\DataTables\DataTableAbstract El dataTable
     */

    protected $printPreview = 'asistenciaalimentaria::beneficiarios.reporte.export';

 /*    public function printPreview(){
        $data = $this->getDataForPrint();
        return view($this->printPreview, compact('data'));
    } */

    public function snappyPdf() {
        $snappy = resolve('snappy.pdf.wrapper');

        $header = view('asistenciaalimentaria::beneficiarios.reporte.header',['programacion'=>$this->programacion]);

        $snappy->setTimeout(600);

        $snappy->setOptions([
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => isset($this->programacion->alimentario) ? ($this->programacion->alimentario->programa_id == config('asistenciaalimentaria.desayunosId') ? '52mm': '48mm') : '48mm',
            'margin-bottom' => '20mm',
            'header-html'=> $header,
            'footer-center'=>'Pagina [page] de [toPage]',
            'header-spacing'=>5,
            'footer-spacing'=>1,
            'page-size'=>'Letter',
            'footer-font-size'=>9
        ]);
        //->setOrientation('landscape');

        return $snappy->loadHTML($this->printPreview())->download($this->getFilename() . '.pdf');
    }

    public function dataTable($query){
        $dt = datatables($query)
        ->editColumn('nombre',function($beneficiario) {
          return strtoupper($beneficiario->nombre);
        })
        ->editColumn('primer_apellido',function($beneficiario) {
          return strtoupper($beneficiario->primer_apellido);
        })
        ->editColumn('segundo_apellido',function($beneficiario) {
          return strtoupper($beneficiario->segundo_apellido);
        })
        ->editColumn('curp',function($beneficiario) {
          return strtoupper($beneficiario->curp);
        })
        ->editColumn('tipoaccion',function($beneficiario){
          return strtoupper($beneficiario->tipoaccion);//strtoupper(AniosPrograma::withTrashed()->where('id',$beneficiario->anio_programa_id)->first()->programa->nombre);
        })
        ->editColumn('embarazo',function ($beneficiario){
          $genero = substr($beneficiario->curp,10,1);
          if($genero == 'M') {
            if($beneficiario->embarazo == 1)
                  return '<span class="badge badge-success">SI</span>';
            else if($beneficiario->embarazo == 0)
                  return '<span class="badge badge-info">NO</span>';
          } else if($genero == 'H') {
            return '<span class="badge badge-warning">NO APLICA</span>';
          } else
            return '<span class="badge badge-danger">CURP INCORRECTA</span>';
        });
        //if(auth()->user()->hasRoles(['ADMIN'])) {
          return $dt->addColumn('ver',function($beneficiario){
            //Para la ruta se utiliza el prefijo beneficiario que en realidad es la persona_id, y alim es el beneficiario. Esto solo aplica para este botón
            $ruta = route('alimentarios.programacion.beneficiarios.edit',[$beneficiario->programacion_id,$beneficiario->persona_id]);///alimentarios/programacion/".$beneficiario->programacion_id."/beneficiario/$beneficiario->persona_id
            if($this->programacion)
              return "<button class='btn btn-info btn-sm' onClick='modal.moreModal(\"$ruta\",\"edit\",{alim:$beneficiario->id})'><i class='fa fa-eye'></i></button>";
            return "<button class='btn btn-info btn-sm' onClick='modal.loadModal(\"$ruta\",\"edit\",{alim:$beneficiario->id})'><i class='fa fa-eye'></i></button>";
          })
          ->addColumn('baja',function($beneficiario){
            return "<button class='btn btn-danger btn-sm' onClick='transaccion.formAjax(\"/alimentarios/programacion/".$beneficiario->programacion_id."/beneficiarios/$beneficiario->id\",\"DELETE\",\"-beneficiarios\")'><i class='fa  fa-thumbs-down'></i></button>";
          })
          ->rawColumns(['embarazo','subprograma','ver','baja']);
        //}
        return $dt->rawColumns(['embarazo']);
    }

    /**
     * Genera la fuente de datos para el dataTable
     * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
    */
    public function query(){
      $beneficiarios_ = Beneficiario::
                        join('alim_programacion','alim_programacion.id','alim_beneficiarios.programacion_id')
                ->join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
                ->join('programas','programas.id','anios_programas.programa_id')
                ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
                ->join('personas_programas','personas_programas.id','alim_beneficiarios.persona_programa_id')
                ->join('personas','personas.id','personas_programas.persona_id')
                ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
                ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
                ->join('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
                ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
                ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
                ->join('alim_tiposaccion','alim_tiposaccion.id','alim_beneficiarios.tipoaccion_id')
                ->where('anio',\Carbon\Carbon::now()->format('Y'));
            if(auth()->user()->regionusuario)
              $beneficiarios_->where('cat_regiones.id',auth()->user()->regionusuario->region_id);
            if($this->programacion)
              $beneficiarios_->where('alim_programacion.id',$this->programacion->id);
            else
              $beneficiarios_->where('programas.id',$this->programa);
            $beneficiarios_->select([
                            'alim_beneficiarios.id as id',
                            'alim_tiposaccion.nombre as tipoaccion',
                            'personas.id as persona_id',
                            'personas.nombre as nombre',
                            'personas.num_acta_nacimiento',
                            'primer_apellido',
                            'segundo_apellido',
                            'curp',
                            'cat_localidades.nombre as localidad',
                            'cat_municipios.nombre as municipio',
                            'cat_distritos.nombre as distrito',
                            'cat_regiones.nombre as region',
                            'esta_embarazada_lactando as embarazo',
                            'alim_beneficiarios.programacion_id as programacion_id',
                            'personas_programas.anios_programa_id as anio_programa_id',
                            'alim_alimentarios.foliodif',
                            DB::raw('TIMESTAMPDIFF(year,fecha_nacimiento,CURRENT_DATE()) as anios'),
                            DB::raw('(TIMESTAMPDIFF(month,fecha_nacimiento,CURRENT_DATE())-(TIMESTAMPDIFF(year,fecha_nacimiento,CURRENT_DATE())*12)) as meses')
                          ]);
                        //   ->groupBy('alim_tiposaccion.nombre');
                        //   ->groupBy(DB::raw('2,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18'));
            if($this->request->get('action')=='pdf'){
                $beneficiarios_->orderBy('alim_tiposaccion.id','asc')->orderBy('personas.primer_apellido','asc')->orderBy('personas.segundo_apellido','asc')->orderBy('personas.nombre','asc');
            }
      return $beneficiarios_;
    }

    /**
     * Método pata configurar el constructor html
     * @return \Yajra\DataTables\Html\Builder Constructor de html
     */
    public function html(){
      $html = $this
      ->builder()
      ->columns($this->getColumns());
      //Como el datatable es dinámico por programación, se debe hacer referencia a la correspondiente. Sino, entraría a actualizar el único datatable de la vista en cuestión.
      if($this->programacion) {
        $html->ajax([
          'url' => route('alimentarios.programacion.beneficiarios.index',$this->programacion->id),
          'type' => 'GET'
        ]);
      } else {
        $html->minifiedAjax();
      }
      $html->parameters($this->getBuilderParameters());
      return $html;
    }

    /**
     * Define las columnas que mostrara el html
     * @return array Arreglo con la configuración de columnas
     */
    protected function getColumns(){
      $columns = [];
      $columns['PrimerApe']  = ['title' => 'Primer apellido', 'data' => 'primer_apellido', 'name' => 'personas.primer_apellido'];
      $columns['SegundoApe'] = ['title' => 'Segundo apellido','data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'];
      $columns['Nombre']     = ['data' => 'nombre', 'name' => 'personas.nombre'];
      $columns['CURP']       = ['title' => 'CURP','data' => 'curp', 'name' => 'personas.curp'];
      $columns['numDeActa']  = ['title'=>'# acta','data' => 'num_acta_nacimiento', 'name' => 'personas.num_acta_nacimiento'];
      $columns['Edad']       = ['data'=>'anios','searchable'=>false];
      $columns['Meses']      = ['data'=>'meses','visible'=>false,'searchable'=>false];
      $columns['embarazo']   = ['title' => 'Embarazo/lactando', 'data' => 'embarazo', 'name' => 'esta_embarazada_lactando', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false];
      $columns['tipoaccion'] = ['title' => 'Pertenece a', 'data' => 'tipoaccion', 'name' => 'alim_tiposaccion.nombre','exportable'=>false,'visible'=>false];
      //Para cuando el datatable se refleje de forma general, y no por programación.
      if(!$this->programacion){
        $columns['foliodif'] = ['title' => 'Folio alimentario', 'data' => 'foliodif', 'name' => 'alim_alimentarios.foliodif'];
        $columns['localidad']  = ['data'=>'localidad', 'name'=>'cat_localidades.nombre'];
        $columns['municipio']  = ['data'=>'municipio', 'name'=>'cat_municipios.nombre'];
        $columns['distrito']   = ['data'=>'distrito', 'name'=>'cat_distritos.nombre'];
        $columns['region']     = ['data'=>'region', 'name'=>'cat_regiones.nombre'];
      }
      //if(!auth()->user()->hasRoles(['ADMIN'])) {
      $columns['ver']  = ['title' => 'Más información','data' => 'ver' , 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false];
      $columns['baja'] = ['title' => 'Dar de baja', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false];
      //}
      return $columns;
    }

    /**
   * Define los parámetros de configuración para la librería de jquery dataTables
   * @return Array Parametros de inicializacion para la librería
   */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'lBtip',
            'buttons' => [
                'pdf' , 'phpexcel', 'reset', 'reload','csv'
            ],
            'lengthMenu' => [  5, 10, 20 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'rowGroup' => [
                'dataSrc' => 'tipoaccion'
            ],
            // 'stateSave' => true,
            'select'    => 'multiple'
            // "order" => [[ 8, "desc" ]]
        ];
        if( $this->programacion ){
            $rutaElim=route('alimentarios.programacion.beneficiarios.destroy',[$this->programacion->id,'benef_id']);
            array_push($builderParameters['buttons'],[
                'text'=>'<i class="fas fa-trash"></i> Eliminar seleccionados',
                'action'=>"function () {
                    eliminacionesFallidas = 0;
                    let count = LaravelDataTables.beneficiarios.rows({ selected: true }).count()
                    let datos = LaravelDataTables.beneficiarios.rows({ selected: true }).data().pluck('id')
                    if(count > 0)
                        Swal.fire({
                            title: 'Confirmacion',
                            text: '¿Esta seguro de eliminar '+count+' beneficiarios',
                            type: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#28a745',
                            cancelButtonColor: '#dc3545',
                            confirmButtonText: 'SI',
                            cancelButtonText: 'NO'
                        }).then( (result) => {
                            if(result.value){
                                $.blockUI({message:'<i class=\"fas fa-spinner fa-pulse fa-3x\"></i></br><h4>Eliminando <label id=\"eliminacionActual\">0</label> de <label id=\"eliminacionTotal\">'+count+'</label></h4>'})
                                eliminarBenefRec('$rutaElim',0,count,datos)
                            }
                        })
                    else
                        Swal.fire({
                            title: 'Error',
                            text: 'Debes seleccionar filas primero',
                            type: 'info'
                        })
                }"
                ]);
        }
        return $builderParameters;
    }

    /**
     * Define un nombre para el archivo que se exportara
     * @return string Nombre del archivo, sin extension
     */
    protected function filename(){
        return 'Beneficiarios al ' . date('Ymd');
    }
}
