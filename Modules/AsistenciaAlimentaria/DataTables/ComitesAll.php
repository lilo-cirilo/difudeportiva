<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Comite;

class ComitesAll extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    ->filterColumn('persona',function($query,$keyword){
        $query->where(function($q) use($keyword){
            $q->where('personas.nombre','like',"%$keyword%")
            ->where('personas.primer_apellido','like',"%$keyword%")
            ->where('personas.segundo_apellido','like',"%$keyword%");
        });
    });
    //->rawColumns(['persona','acciones']);
  }
  
  public function query(){
    $model = Comite::join('personas','personas.id','alim_comites.persona_id')
            ->join('cargos_programas','cargos_programas.id','alim_comites.cargo_programa_id')
            ->join('cat_cargos','cat_cargos.id','cargos_programas.cargo_id')
            ->join('alim_programacion','alim_programacion.id','alim_comites.programacion_id')
            ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
            ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
            ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
            ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
            ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
    ->select([
        'alim_comites.id',
        'cat_cargos.nombre as cargo',
        'cat_regiones.nombre as region',
        'cat_distritos.nombre as distrito',
        'cat_municipios.nombre as municipio',
        'cat_localidades.nombre as localidad',
        'alim_alimentarios.sublocalidad'
    ])
    ->selectRaw('CONCAT_WS(" ",personas.nombre,primer_apellido,segundo_apellido) as persona, personas.id as persona_id');
    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
        'global' => false
    ])
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns = [
        'persona'   => ['orderable' => false],
        'cargo '    => ['data' => 'cargo', 'name' => 'cat_cargos.nombre'],
        'region'    => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
        'distrito'  => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
        'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
        'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
        'sublocalidad' => ['data' => 'sublocalidad', 'name' => 'alim_alimentarios.sublocalidad']
    ];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        //'new',
        //'pdf',
        //'excel',
        /* [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => ''
        ], */
        /* [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => ''
          ] */
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ],
      'select' => ['style'=>'os','selector' => 'td:first-child'],
      'order'  => [[ 1, 'asc' ]],
      'drawCallback' => 'function() { 
        $(".icheck").each( function () {  
          var aux = $(this).iCheck({
            checkboxClass: "icheckbox_square-green",
            increaseArea: "10%"
          });
          aux.on("ifToggled", function(event){
            cambiarSeleccion($(this).attr("name"),$(this).prop("checked"),$(this).attr("programacion_id"));
          })
        });
      }'
    ];
    if($this->tipo == 'refrendo')
      $builderParameters['dom']='<"toolbar">tip';
    return $builderParameters;
  }

  protected function filename(){
    return 'CCNC al ' . date('Ymd');
  }
}
