<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

class Distribuciones extends CustomDataTable
{

    public function dataTable($query){
        return datatables($query)
        ->filterColumn('sedesol',function($query,$keywords){
            $search = strtolower($keywords)=='si' ? 1 : (strtolower($keywords)=='no'? 0 : 3 );
            $query->orwhere('alim_alimentarios.essedesol',$search);
        })
        ->filterColumn('desarrollo',function($query,$keywords){
            if (strtolower($keywords)=='si')
                $query->orwhereNotNull('alim_sujetos.dedesarrollo_programa_id');
            else
                $query->orwhereNull('alim_sujetos.dedesarrollo_programa_id');
        });
    }

    public function query(){
        $model = RequisicionProgramacion::join('alim_requisiciones', 'alim_requisiciones.id', 'alim_programacion_requisiciones.requisicion_id')
            ->join('alim_cat_tiposentrega', 'alim_cat_tiposentrega.id', 'alim_requisiciones.tipo_entrega_id')
            ->join('alim_programacion', 'alim_programacion.id', 'alim_programacion_requisiciones.programacion_id')
            ->join('alim_alimentarios', 'alim_alimentarios.id', 'alim_programacion.alimentario_id')
            ->join('anios_programas', 'anios_programas.id', 'alim_programacion.anios_programa_id')
            ->join('cat_ejercicios', 'cat_ejercicios.id', 'anios_programas.ejercicio_id')
            ->join('alim_recibos', 'alim_recibos.programacion_requisicion_id', 'alim_programacion_requisiciones.id') //no se por que estaba comentado
            ->join('estados_programas', 'estados_programas.id', 'alim_programacion.estado_programa_id')
            ->join('cat_estados', 'cat_estados.id', 'estados_programas.estado_id')
            ->join('cat_municipios', 'cat_municipios.id', 'alim_alimentarios.municipio_id')
            ->leftjoin('cat_localidades', 'cat_localidades.id', 'alim_alimentarios.localidad_id')
            ->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')
            ->join('cat_regiones', 'cat_regiones.id', 'cat_distritos.region_id')
            ->leftJoin('alim_requisiciones_datoscarga', 'alim_requisiciones_datoscarga.programacion_requisicion_id', 'alim_programacion_requisiciones.id')
            ->leftJoin('alim_datoscarga', 'alim_datoscarga.id', 'alim_requisiciones_datoscarga.datocarga_id')
            ->leftJoin('alim_choferesproveedor', 'alim_choferesproveedor.id', 'alim_datoscarga.choferproveedor_id')
            ->leftJoin('alim_entregasproveedor', 'alim_entregasproveedor.requisicion_datocarga_id', 'alim_requisiciones_datoscarga.id')
            ->leftJoin('alim_pagosrecibos','alim_pagosrecibos.recibo_id','alim_recibos.id')
            ->where('alim_requisiciones.programa_id',$this->programa_id);
        if($this->programa_id == config('asistenciaalimentaria.sujetosId')) {
            $model->leftJoin('alim_sujetos','alim_sujetos.alimentario_id','alim_alimentarios.id');
        }
        $select = [
            'cat_ejercicios.anio as ejercicio',
            'alim_alimentarios.foliodif as foliococina',
            'alim_recibos.id as foliorecibo',
            DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as sedesol'),
            'alim_cat_tiposentrega.nombre as contrato',
            'alim_requisiciones.num_bimestre as bimestre',
            'alim_requisiciones.num_entrega as entrega',
            'alim_requisiciones.periodo as meses',
            'cat_regiones.id as cvereg',
            'cat_regiones.nombre as region',
            'cat_distritos.id as cvedto',
            'cat_distritos.nombre as distrito',
            'cat_municipios.id as cvemun',
            'cat_municipios.nombre as municipio',
            'cat_localidades.cve_localidad as cveloc',
            'cat_localidades.nombre as localidad',
            'alim_alimentarios.sublocalidad',
            'cat_estados.nombre as estado',
            'alim_datoscarga.placas',
            'alim_datoscarga.ruta',
            'alim_choferesproveedor.nombre as chofer',
            'alim_datoscarga.fecha as fechacarga',
            'alim_entregasproveedor.fecha_entrega as fechaentrega',
            DB::raw('
                if(
                    alim_entregasproveedor.observaciones is null,
                    (
                        select concat(per.nombre," ",per.primer_apellido," ",per.segundo_apellido," - ",car.nombre) from alim_comites ac
                        inner join personas per on per.id = ac.persona_id
                        inner join cargos_programas cp on cp.id = ac.cargo_programa_id
                        inner join cat_cargos car on car.id = cp.cargo_id
                        where ac.id = alim_entregasproveedor.recibe_comite_id
                    ),
                    alim_entregasproveedor.observaciones
                ) as recibio
            '),
            DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
                from personas p inner join alim_comites c on p.id = c.persona_id
                inner join cargos_programas cp on cp.id = c.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where c.programacion_id = alim_programacion.id and car.nombre LIKE 'PRESIDENTE(A)' and c.deleted_at is null) as pre"),
            DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
                from personas p inner join alim_comites c on p.id = c.persona_id
                inner join cargos_programas cp on cp.id = c.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where c.programacion_id = alim_programacion.id and car.nombre LIKE 'VOCAL DE ABASTO Y ALMACÉN' and c.deleted_at is null) as vocal1"),
            DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
                from personas p inner join alim_comites c on p.id = c.persona_id
                inner join cargos_programas cp on cp.id = c.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where c.programacion_id = alim_programacion.id and car.nombre LIKE 'VOCAL DE ORIENTACIÓN ALIMENTARIA Y SALUD' and c.deleted_at is null) as vocal2"),
            DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
                from personas p inner join alim_comites c on p.id = c.persona_id
                inner join cargos_programas cp on cp.id = c.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where c.programacion_id = alim_programacion.id and car.nombre LIKE 'SECRETARIO(A)' and c.deleted_at is null) as secre"),
            DB::raw("(select concat_ws(' ',p.nombre,primer_apellido,segundo_apellido)
                from personas p inner join alim_comites c on p.id = c.persona_id
                inner join cargos_programas cp on cp.id = c.cargo_programa_id
                inner join cat_cargos car on car.id = cp.cargo_id
                where c.programacion_id = alim_programacion.id and car.nombre LIKE 'TESORERO(A)' and c.deleted_at is null) as teso"),
            // 'alim_recibos.id as fichapago'
        ];
        $i = 0;
        $queryInversion="";
        $queryCuotaRecuperacion="";
        foreach (TipoAccion::where('programa_id', $this->programa_id)->get() as $subprograma) {
            $i++;
            $inversion = "((select cantidad_dotaciones from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null)
            *
            (select sum(costo) FROM alim_detallesdotacion INNER JOIN alim_costos_productos on alim_detallesdotacion.costo_producto_id = alim_costos_productos.id where dotacion_id = {$subprograma->id}))";
            $queryInversion = $queryInversion.$inversion.' + ';
            $queryCuotaRecuperacion=$queryCuotaRecuperacion."(select cantidad_beneficiarios * cuota_recuperacion * periodo from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            inner join alim_requisiciones r on r.id = pr.requisicion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null) + ";
            array_push(
                $select,
                DB::raw("(select cantidad_beneficiarios from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null) as beneficiarios$i"),
                DB::raw("(select cantidad_dotaciones from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            inner join alim_dotaciones d on d.id=cd.dotacion_id
            where d.tipoaccion_id={$subprograma->id} and pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null and pr.deleted_at is null) as dotacion$i"),
                DB::raw("$inversion as inversion$i")
            );
        }
        $queryInversion=substr($queryInversion, 0, -3);
        $queryCuotaRecuperacion=substr($queryCuotaRecuperacion, 0, -3);
        array_push($select, DB::raw("(select sum(cantidad_beneficiarios) from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            where pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null) as benef"));
        array_push($select, DB::raw("(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            where pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null) as dot"));
        array_push($select,DB::raw("(".$queryInversion.") as inversionT"));
        array_push($select,DB::raw("(".$queryCuotaRecuperacion.") as recuperar"));
        array_push($select,DB::raw("if (alim_pagosrecibos.recibo_id is null,0,alim_pagosrecibos.importe_total) as pagado"));
        if($this->programa_id == config('asistenciaalimentaria.sujetosId')) {
            array_push($select, DB::raw('if(alim_sujetos.dedesarrollo_programa_id is not null,"SI","NO") as desarrollo'));
        }
        $model->select($select);
        return $model;
    }

    public function html(){
        return $this
            ->builder()
            ->columns($this->getColumns())
            ->ajax([
                // 'url' => 'window.location.href',
                'global' => false,
                'type' => 'POST'
            ])
            // ->minifiedAjax()
            ->parameters($this->getBuilderParameters());
    }

    protected function getColumns(){
        $cTitleAux = $this->programa_id == config('asistenciaalimentaria.ccncId') ? 'Es sedesol' : ($this->programa_id==config('asistenciaalimentaria') ? 'Es desarrollo' : null );
        $cDataAux = $this->programa_id == config('asistenciaalimentaria.ccncId') ? 'sedesol' : ($this->programa_id==config('asistenciaalimentaria') ? 'desarrollo' : null );

        $columns = [
            ['name'=>'alim_alimentarios.foliodif',          'title' => 'Folio Cocina',          'data' => 'foliococina'],
            // ['name'=>'alim_recibos.id',                     'title' => 'Folio Recibo',          'data' => 'foliorecibo'],
            ['title'=>$cTitleAux,                           'data'  => $cDataAux],
            ['name'=>'cat_ejercicios.anio',                 'title' => 'Ejercicio',             'data' => 'ejercicio'],
            ['name'=>'cat_estados.nombre',                  'title' => 'Estado del alimentario','data' => 'estado'], // de la CCNC',
            ['name'=>'alim_cat_tiposentrega.nombre',        'title' => 'Tipo Entrega',          'data' => 'contrato'],
            ['name'=>'alim_requisiciones.num_bimestre',     'title' => 'Bimestre',              'data' => 'bimestre'],
            ['name'=>'alim_requisiciones.num_entrega',      'title' => 'N° Entrega',            'data' => 'entrega'],
            ['name'=>'alim_requisiciones.periodo',          'title' => 'N° de Meses',           'data' => 'meses'],
            ['name'=>'cat_regiones.id',                     'title' => 'Clave de Region',       'data' => 'cvereg'],
            ['name'=>'cat_regiones.nombre',                 'title' => 'Region',                'data' => 'region'],
            ['name'=>'cat_distritos.id',                    'title' => 'Clave de Distrito',     'data' => 'cvedto'],
            ['name'=>'cat_distritos.nombre',                'title' => 'Distrito',              'data' => 'distrito'],
            ['name'=>'cat_municipios.id',                   'title' => 'Clave de Municipio',    'data' => 'cvemun'],
            ['name'=>'cat_municipios.nombre',               'title' => 'Municipio',             'data' => 'municipio'],
            ['name'=>'cat_localidades.id',                  'title' => 'Clave de Localidad',    'data' => 'cveloc'],
            ['name'=>'cat_localidades.nombre',              'title' => 'Localidad',             'data' => 'localidad'],
            ['name'=>'alim_alimentarios.sublocalidad',      'title' => 'Sub-localidad',         'data' => 'sublocalidad'],
            ['name'=>'alim_datoscarga.fecha',               'title' => 'Fecha de Carga',        'data' => 'fechacarga'], // 'Fecha de Carga',
            ['name'=>'alim_choferesproveedor.nombre',       'title' => 'Quien Entrego',         'data' => 'chofer'], // 'Quien Entrego',
            ['name'=>'alim_datoscarga.placas',              'title' => 'Placas vehículo',       'data' => 'placas'], // 'Placas',
            ['name'=>'alim_datoscarga.ruta',                'title' => 'N° de Ruta',            'data' => 'ruta'], // 'No de Ruta',
            ['name'=>'alim_entregasproveedor.fecha_entrega','title' => 'Fecha de Entrega',      'data' => 'fechaentrega'], // 'Fecha Entrega',
            ['title' => 'Quien Recibió',                            'data' => 'recibio', 'orderable' => false, 'searchable' => false,], // 'Quien Recibe',
            ['title' => 'Presidente(a)',                            'data' => 'pre',     'orderable' => false, 'searchable' => false,],
            ['title' => 'Secretario(a)',                            'data' => 'secre',   'orderable' => false, 'searchable' => false,],
            ['title' => 'Tesorero(a)',                              'data' => 'teso',    'orderable' => false, 'searchable' => false,],
            ['title' => 'Vocal de Abasto y Almacén',                'data' => 'vocal1',  'orderable' => false, 'searchable' => false,],
            ['title' => 'Vocal de Orientación Alimentaria y Salud', 'data' => 'vocal2',  'orderable' => false, 'searchable' => false,]
            // ['data'=>'Folio Cancelado'],
            // 'CArgo Recibe',
            // 'Entregado',
            // 'Motivo no entregado',
            // 'Observa',
            // 'Validado Financiero',
            // 'No procede',
            // 'Observación no Procede',
            // 'No oficio Finan',
        ];
        $i = 0;
        foreach (TipoAccion::where('programa_id', $this->programa_id)->get() as $subprograma) {
            $i++; //str_replace([' ','(',')','-'],[''],$subprograma->nombre);
            array_push(
                $columns,
                ['data' => "beneficiarios$i", 'title' => 'beneficiarios ' . explode('(', explode(')', $subprograma->nombre)[0])[1], 'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true],
                ['data' => "dotacion$i",      'title' => 'dot. ' . explode('(', explode(')', $subprograma->nombre)[0])[1],          'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true],
                ['data' => "inversion$i",     'title' => 'inversion ' . explode('(', explode(')', $subprograma->nombre)[0])[1],     'orderable' => false, 'searchable' => false, 'exportable' => true, 'printable' => true]
            );
        }
        array_push(
            $columns,
            ['title' => 'Total de Beneficiarios', 'data' => 'benef', 'orderable' => false, 'searchable' => false],
            ['title' => 'Total de Dotaciones',    'data' => 'dot'  , 'orderable' => false, 'searchable' => false],
            ['title' => 'Inversion total',    'data' => 'inversionT'  , 'orderable' => false, 'searchable' => false],
            ['title' => 'Monto a recuperar',  'data' => 'recuperar'  , 'orderable' => false, 'searchable' => false],
            ['title' => 'Monto recuperado',   'data' => 'pagado'  , 'orderable' => false, 'searchable' => false]
        );
        if($this->programa_id==config('asistenciaalimentaria.desayunosId')){
            unset($columns[1]);
        }
        return $columns;
    }

    protected function getBuilderParameters(){
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Bti', //'lBtip',
            'buttons' => [
                'reload',
                'reset',
                'excel'
            ],
            'colReorder' => true,
            'rowId'             =>'foliococina',
            'stateSave'         => true,
            'keys'        => true,
            'scrollY' =>        '500px',
            'scrollX' =>        true,
            'scrollCollapse' => true,
            'scroller' =>       ['loadingIndicator' => true],
            'deferRender' =>    true,
            'serverSide' =>    true,
            'fixedColumns' => ['leftColumns' => 1],
            'columnDefs' => [
                ['className' => 'text-center', 'targets' => '_all'],
            ],
            'initComplete' => ' function () {
                var api = this.api()
                api.columns().every(function () {
                    var column = this;
                    if(api.table().init().columns[column[0]].searchable){
                        var input = document.createElement("input")
                        $(input).addClass("column-filter")
                        $(input).addClass("text-center")
                        $(input).attr("placeholder","Filtro")//"Filtrar "+column.header().textContent.toLowerCase())
                        $(input).appendTo($(column.footer()).empty())
                        .on("change", function () {
                            column.search($(this).val(), false, false, true).draw()
                        });
                    }
                });
            }',
        ];
        return $builderParameters;
    }

    protected function filename(){
        return 'distribuciones ' . date('Ymd');
    }
}
