<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use App\Models\Programa;

class SubProgramas extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    ->addColumn('seleccionado',function ($alimentario){
      $aux = $alimentario->activa ? "checked='$alimentario->activa'" : ""; 
      return "<input type='checkbox' class='icheck' name='$alimentario->id' id='$alimentario->id' $aux programacion_id='$alimentario->activa'>";
    })->rawColumns(['seleccionado']);
  }
  
  public function query(){
    return Programa::where('codigo',$this->request->programa_id)
    ->select(
      'id',
      'nombre',
      'deleted_at as estado'
    );
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    return [
      'nombre' => ['name' => 'programas.nombre'],
      'estado'
    ];
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      'buttons' => [
        //'new',
        'pdf',
        'excel',
        /* [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => ''
        ], */
        /* [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => ''
          ] */
      ],
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ],
      'select' => ['style'=>'os','selector' => 'td:first-child'],
      'order'  => [[ 1, 'asc' ]],
      'drawCallback' => 'function() { 
        $(".icheck").each( function () {  
          var aux = $(this).iCheck({
            checkboxClass: "icheckbox_square-green",
            increaseArea: "10%"
          });
          aux.on("ifToggled", function(event){
            cambiarSeleccion($(this).attr("name"),$(this).prop("checked"),$(this).attr("programacion_id"));
          })
        });
      }'
    ];
    if($this->tipo == 'refrendo')
      $builderParameters['dom']='<"toolbar">tip';
    return $builderParameters;
  }

  protected function filename(){
    return 'CCNC al ' . date('Ymd');
  }
}
