<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;


use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;

class RequisicionProgramacionesSelec extends CustomDataTable{

  protected $printPreview = 'asistenciaalimentaria::requisicion.reporte.export';

 /*    public function printPreview(){
        $data = $this->getDataForPrint();
        return view($this->printPreview, compact('data'));
    } */

    public function snappyPdf() {
        $snappy = resolve('snappy.pdf.wrapper');

        $header = view('asistenciaalimentaria::requisicion.reporte.header',['requisicion'=>$this->requisicion]);

        $snappy->setTimeout(600);

        $snappy->setOptions([
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '48mm',
            'margin-bottom' => '20mm',
            'header-html'=> $header,
            'footer-center'=>'Pagina [page] de [toPage]',
            'header-spacing'=>6,
            'footer-spacing'=>1,
            'page-size'=>'Letter',
            'footer-font-size'=>9
        ]);
        //->setOrientation('landscape');

        return $snappy->setOrientation('landscape')->loadHTML($this->printPreview())->download($this->getFilename() . '.pdf');
    }


  public function dataTable($query){
    $tabla = datatables($query)
    ->editColumn('folio',function($query){
      $requisicion = '';
      if(RequisicionProgramacion::where('programacion_id',$query->programacion_id)->where('requisicion_id',$this->requisicion->id)->count() > 0)
        $requisicion = 'esrequisicion';
      return "$query->folio <span id='$query->programacion_id' class='pdi $requisicion'></span>";
    })
    ->filterColumn('esampliacion',function($query,$keywords){
      $search = strtolower($keywords)=='si' ? 1 : (strtolower($keywords)=='no'? 0 : 3 );
      $query->orwhere('alim_programacion.esampliacion',$search);
  })
    ->filterColumn('essedesol',function($query,$keywords){
        $search = strtolower($keywords)=='si' ? 1 : (strtolower($keywords)=='no'? 0 : 3 );
        $query->orwhere('alim_alimentarios.essedesol',$search);
    })

    ->filterColumn('esdesarrollo',function($query,$keywords){
        if( strtolower($keywords)=='si')
            $query->whereNotNull('alim_sujetos.dedesarrollo_programa_id');
        else
            $query->whereNull('alim_sujetos.dedesarrollo_programa_id');
    })
    ->filterColumn('financiamiento',function($query,$keywords){
        if (strtolower($keywords)=='si')
            $query->orwhere('alim_cat_financiamientos.nombre',"PRODUCTO FINANCIERO");
        else
            $query->orwhere('alim_cat_financiamientos.nombre','!=',"PRODUCTO FINANCIERO");
    })

    ->rawColumns(['folio']);
    return $tabla;
  }

  public function query(){
    $model = Programacion::GetValidForRequisicion($this->requisicion,$this->request)

    ->select([
      'alim_alimentarios.foliodif as folio',
      DB::raw('if(alim_alimentarios.essedesol = 1,"SI","NO") as essedesol'),
      DB::raw('if(dedesarrollo_programa_id is null,"NO","SI") as esdesarrollo'),
      'cat_municipios.nombre as municipio',
      'cat_localidades.nombre as localidad',
      'cat_distritos.nombre as distrito',
      'alim_alimentarios.sublocalidad as sublocalidad',
      'cat_regiones.nombre as region',
      'alim_programacion.id as programacion_id',
      DB::raw('if(alim_programacion.esampliacion = 1,"SI","NO") as esampliacion'),
      DB::raw('if(alim_cat_financiamientos.nombre  = "PRODUCTO FINANCIERO","SI","NO") as financiamiento'),
    ]);
    return $model;

  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
        'data' => "function (d){
            if( $('#desarrollo').prop('checked') ){
                d.reqFor = 'desarrollo';
            }
        }"
    ])
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      //'programacion_id' => ['data' => 'programacion_id', 'name' => 'alim_programacion.id', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false, 'visible' => false],
      'folio'        => ['data' => 'folio', 'name' => 'alim_alimentarios.foliodif'],
      'region'       => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
      'distrito'     => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
      'municipio'    => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
      'localidad'    => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
      'sublocalidad' => ['data' => 'sublocalidad', 'name' => 'alim_alimentarios.sublocalidad'],

    ];
    switch ($this->requisicion->programa_id) {
        case config('asistenciaalimentaria.ccncId'):
            $columns['essedesol']      = ['data' => 'essedesol', 'title'=>'Sedesol'];
            $columns['ampliacion'] = ['data' => 'esampliacion'];
            break;
        case config('asistenciaalimentaria.sujetosId'):
            $columns['esdesarrollo']   = ['data' => 'esdesarrollo', 'title'=>'Desarrollo'];
            $columns['financiamiento']   = [ 'title'=>'Producto Financiero'];
            $columns['ampliacion'] = ['data' => 'esampliacion'];

            break;
        default:
            # code...
            break;
    }

    return $columns;
  }

  protected function getBuilderParameters(){
    $nameaux = $this->requisicion->programa_id == config('asistenciaalimentaria.sujetosId') ? 'alim_sujetos.dedesarrollo_programa_id' : 'null';
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'initComplete'=>'function(){
        this.api().columns().every(function () {
            var column = this;
            var input = document.createElement("input");
            $(input).appendTo($(column.footer()).empty())
            .on("change", function () {
                column.search($(this).val()).draw();
            });
        });
        $("#desarrollo").change(function(){LaravelDataTables.seleccion.ajax.reload()}
        )
      }',
      'drawCallback'=>'function(){
        $(".esrequisicion").closest("tr").addClass("selected");
        var api = this.api();
        api.$("td").click( function () {
          if(this.attributes[1] == undefined) {
            if(this.closest("tr").classList[1] === undefined)
              transaccion.miniAjax("'.route('alimentarios.programaciones.store',$this->requisicion->id).'","POST",{programacion_id:this.closest("tr").firstChild.children[0].id})
            else
              transaccion.miniAjax("/alimentarios/requisicion/'.$this->requisicion->id.'/programaciones/"+this.closest("tr").firstChild.children[0].id,"DELETE")
          }
          //console.log(this.classList[1])
          //console.log(this.firstChild.children[0].id);
        });
      }',
      'buttons' => [
        'reset', 'reload',
        [
         'text'=>'<i class="far fa-hand-pointer"></i> Seleccionar filas encontradas',
         'action' => "function(){
            var params = window.LaravelDataTables.seleccion.ajax.params().columns.filter(o=>
                o.search.value!=''
            ).map(o=>{
              let name= o.name
              if (name=='desarrollo')
                name='alim_sujetos.dedesarrollo_programa_id'
              else if (name=='financiamiento')
                name='alim_cat_financiamientos.nombre'
              else if (name=='esampliacion')
                name='alim_programacion.esampliacion'
              return {name:name ,value:o.search.value}
            })
            var data = {}
            data.filtros = params
            if( $('#desarrollo').prop('checked') ){
                data.reqFor = 'desarrollo'
            }
            transaccion.miniAjax('".route('alimentarios.programaciones.store',$this->requisicion->id)."',\"POST\",data)
          }"
        ],
        [
          'text'=>'<i class="fa fa-trash"></i> Quitar selección a todas las filas',
          'action' => 'function(){
             transaccion.miniAjax("/alimentarios/requisicion/'.$this->requisicion->id.'/programaciones/undefined","DELETE")
          }'
        ],
        [
          'text'=>'<i class="fas fa-ban text-danger"></i> Programaciones incompletas',
          'action' => 'function(){
             modal.loadModal("/alimentarios/requisicion/'.$this->requisicion->id.'/programaciones/list","no-select")
          }'
        ]
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]/* ,
      'select' => ['style'=> 'multi'] */
    ];

    return $builderParameters;
  }

  protected function filename(){
    return 'Requisición al ' . date('Ymd');
  }
}
