<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Licitacion;

use Illuminate\Support\Facades\DB;
 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Modules\AsistenciaAlimentaria\Entities\Dotacion;
use Modules\AsistenciaAlimentaria\Entities\LicitacionCantidadDotacion;

class Dotaciones extends CustomDataTable {
  public function dataTable($query){
    return datatables($query)
            ->addColumn('eliminar', function($q){
                $r = route('alimentarios.dotacion.destroy', $q->id);
                return "<button class='btn btn-danger' onClick='transaccion.formAjax(\"$r\",\"DELETE\",\"-dotacions\")'><i class='fas fa-trash'></i></button>";
            })
            ->rawColumns(['eliminar']);
  }
  public function query(){
    $model = Dotacion::join('cat_ejercicios','cat_ejercicios.id','alim_dotaciones.ejercicio_id')
                    ->join('alim_tiposaccion','alim_tiposaccion.id','alim_dotaciones.tipoaccion_id')
                    ->join('programas','programas.id','alim_tiposaccion.programa_id');
    if($this->licitacion_id) {
      $lic_id = $this->licitacion_id;
      $model->whereNotIn('alim_dotaciones.id',LicitacionCantidadDotacion::select(['dotacion_id'])->where('licitacion_id','=',$lic_id)->get()->toArray()
      );
    }
    $model->select([
        'alim_dotaciones.id as dotacion_id',
        'cat_ejercicios.anio',
        'alim_tiposaccion.nombre as subprograma',
        'alim_dotaciones.cuota_recuperacion',
        'programas.nombre as programa'
    ]);
    return $model;
        
  }
  public function html(){
    $html = $this
    ->builder()
    ->columns($this->getColumns());
    //Como el datatable es dinámico por programación, se debe hacer referencia a la correspondiente. Sino, entraría a actualizar el único datatable de la vista en cuestión.
    if($this->licitacion_id) {
      $html->ajax([
        'url' => route('alimentarios.dotacion.index'),
        'type' => 'GET',
        'data' => "function(d){ d.lic_id = $this->licitacion_id }"
      ]);
    } else {
      $html->minifiedAjax();
    }
    $html->parameters($this->getBuilderParameters());
    return $html;
  }
  protected function getColumns(){
    $columns =
    [
        'id'                    => ['name' => 'alim_dotaciones.id', 'data' => 'dotacion_id'],
        'anio'                  => ['name' => 'cat_ejercicios.anio'],
        'programa'              => ['name' => 'programas.nombre'],
        'subprograma'           => ['name' => 'alim_tiposaccion.nombre'],
        'cuota_recuperacion'    => ['name' => 'alim_dotaciones.cuota_recuperacion'],
    ];
    if(!$this->licitacion_id)
      $columns['eliminar'] = ['data' => 'eliminar'];
    return $columns;
  }
  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
      ],
      'deferRender'=> true,
      'rowId' => 'dotacion_id',
      'dom' => 'lBtip',
      'initComplete'=>'function(){}',
      'buttons' => [
        'reset', 'reload'
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ],
      'select' => ['style'=> 'single']
    ];
    if(!$this->licitacion_id)
      array_push($builderParameters['buttons'],'new');
    return $builderParameters;
  }
    
  protected function filename(){
    return 'Requisición al ' . date('Ymd');
  }
}