<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Licitacion;

use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Modules\AsistenciaAlimentaria\Entities\LicitacionCantidadDotacion;
use Modules\AsistenciaAlimentaria\Entities\Licitacion;

class CantidadesDotacionesLicitacion extends CustomDataTable {
  public function dataTable($query){
    return datatables($query)
            ->addColumn('eliminar', function($q){
                $r = route('alimentarios.licitacion.dotaciones.destroy', [$q->licitacion_id, $q->id]);
                return "<button class='btn btn-danger' onClick='transaccion.formAjax(\"$r\",\"DELETE\",\"-dotacions\")'><i class='fas fa-trash'></i></button>";
            })
            ->rawColumns(['eliminar']);
  }
  public function query(){
    $model = LicitacionCantidadDotacion::join('alim_dotaciones','alim_dotaciones.id','alim_cantidadesdotaciones_licitaciones.dotacion_id')
                    ->join('cat_ejercicios','cat_ejercicios.id','alim_dotaciones.ejercicio_id')
                    ->join('alim_tiposaccion','alim_tiposaccion.id','alim_dotaciones.tipoaccion_id')
                    ->join('programas','programas.id','alim_tiposaccion.programa_id')
                    ->where('alim_cantidadesdotaciones_licitaciones.licitacion_id',$this->licitacion_id)
                    ->select([
                        'alim_cantidadesdotaciones_licitaciones.id',
                        'alim_cantidadesdotaciones_licitaciones.licitacion_id',
                        'cat_ejercicios.anio',
                        'alim_tiposaccion.nombre as subprograma',
                        'alim_cantidadesdotaciones_licitaciones.cantidad',
                        'programas.nombre as programa'
                    ]);
    return $model;
        
  }
  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->ajax([
      'url' => route('alimentarios.licitacion.dotaciones.index',$this->licitacion_id),
      'type' => 'GET',
    ])
    ->parameters($this->getBuilderParameters());
  }
  protected function getColumns(){
    $columns =
    [
        'anio'                  => ['name' => 'cat_ejercicios.anio'],
        'programa'              => ['name' => 'programas.nombre'],
        'subprograma'           => ['name' => 'alim_tiposaccion.nombre'],
        'cantidad'              => ['name' => 'alim_cantidadesdotaciones_licitaciones.cantidad'],
        'eliminar'              => ['data' => 'eliminar', 'searchable' => false, 'exportable' => false],
    ];
    return $columns;
  }
  protected function getBuilderParameters(){
    /* $crear = route('alimentarios.proveedor.requisicion.ordenCarga.create',[$this->proveedor_id,$this->requisicion->id]); */
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles'],
      ],
      'deferRender'=> true,
      'rowId' => 'req_prog_id',
      'dom' => 'lBtip',
      'initComplete'=>'function(){}',
      'drawCallback'=>'function(){}',
      'buttons' => [
        'reset', 'reload',
        [
          'text'    => '<i class="fas fa-plus"></i> Agregar dotación',
          'action'  => "function() { modal.moreModal('".route('alimentarios.dotacion.index')."','cantidad_dotaciones_t',{lic_id:$this->licitacion_id}) }"
        ]
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
          [ 'className' => 'text-center', 'targets' => '_all' ]
      ],
    ];
    
    return $builderParameters;
  }
    
  protected function filename(){
    return 'Cantidad dotación licitación al ' . date('Ymd');
  }
}