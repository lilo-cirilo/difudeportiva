<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Proveedor;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Carbon\Carbon;

use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;

class Requisiciones extends CustomDataTable{
  /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->addColumn('orden',function ($requisicion){
            $ruta = route('alimentarios.proveedor.requisicion.ordenCarga.index',[$this->proveedor_id,$requisicion->id]);
            return "<a href='$ruta' class='btn btn-warning btn-xs btn-block'><i class='fas fa-truck'></i></a>";
        })
        ->addColumn('entrega',function ($requisicion){
            $ruta = route('alimentarios.proveedor.requisicion.entregas.index',[$this->proveedor_id,$requisicion->id]);
            return "<a href='$ruta' class='btn btn-success btn-xs btn-block'><i class='fas fa-people-carry'></i></a>";
        })
        ->editColumn('especial',function($requisicion){
            return $requisicion->especial == 1 ? '<i class="fas fa-check-circle"></i>':'<i class="fas fa-times-circle"></i>';
        })
        ->editColumn('estado',function($requisicion){
            $ruta = route('alimentarios.proveedor.requisiciones.show',[$this->proveedor_id,$requisicion->id]);
            $total = $requisicion->programacionrequisicion->count();
            $avance = RequisicionProgramacion::whereHas('estadoPrograma',function($q){
                $q->whereHas('estado',function($qu){
                    $qu->where('nombre','!=','REVISION');
                });
            })->whereHas('requisicion',function($query)use($requisicion){$query->where('id',$requisicion->id);})->count();
            $progreso = round($avance*100/$total,1);
            return "<div class='progress' onClick='location.href=\"$ruta\"'>
                        <div class='progress-bar progress-bar-striped progress-bar-animated' role='progressbar'
                        style='width: $progreso%' aria-valuenow='10' aria-valuemin='0' aria-valuemax='100'>$progreso%</div>
                    </div>";
        })
        ->rawColumns(['orden','entrega','especial','estado']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \Modules\AsistenciaAlimentaria\entities\Requisicion $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Requisicion $model)
    {
        return $model->newQuery()
        ->join('programas','programas.id','alim_requisiciones.programa_id')
        ->join('alim_licitaciones','alim_licitaciones.id','alim_requisiciones.licitacion_id')
        ->join('cat_ejercicios','cat_ejercicios.id','alim_licitaciones.ejercicio_id')
        ->join('alim_cat_tiposentrega','alim_cat_tiposentrega.id','alim_requisiciones.tipo_entrega_id')
        ->join('estados_programas','estados_programas.id','alim_requisiciones.estado_programa_id')
        ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
        ->where('cat_estados.nombre','ENVIADA')
        ->select([
            'alim_requisiciones.id as id',
            'anio',
            'programas.nombre as programa',
            'num_oficio as num_oficio',
            'fecha_oficio as fecha_oficio',
            'observaciones as observaciones',
            'num_bimestre as num_bimestre',
            'num_entrega as num_entrega',
            'alim_cat_tiposentrega.nombre as tipoentrega',
            'especial as especial',
            'estado_programa_id as estado_programa_id',
            'periodo as periodo',
        ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id'=>['data'=>'id','name'=>'alim_requisiciones.id'],
            'programa'=>['data'=>'programa','name'=>'programas.nombre'],
            'bimestre'=>['data'=>'num_bimestre','name'=>'num_bimestre'],
            'entrega'=>['data'=>'num_entrega','name'=>'num_entrega'],
            'oficio'=>['data'=>'num_oficio','name'=>'num_oficio'],
            'fecha'=>['data'=>'fecha_oficio','name'=>'fecha_oficio'],
            'especial'=>['data'=>'especial','name'=>'especial'],
            'Tipo'=>['data'=>'tipoentrega','name'=>'alim_cat_tiposentrega.nombre'],
            //'periodo'=>['data'=>'periodo','name'=>'periodo'],
            'observaciones'=>['data'=>'observaciones','name'=>'observaciones'], 
            'Orden de carga'=>['data'=>'orden','searchable'=>false,'orderable'=>false],
            'Entregas'=>['data'=>'entrega','searchable'=>false,'orderable'=>false],
            'estado'=>['data'=>'estado','searchable'=>false,'orderable'=>false],
        ];
    }
    protected function getBuilderParameters(){
        $builderParameters = [
        'language' => [
            'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
            'buttons'=>['colvis'=>'Columnas visibles']
        ],
        'dom' => 'lBtip',
        'drawCallback'=>'',
        'buttons' => [
            'reset', 'reload'
        ],
        'lengthMenu' => [  10, 5, 20 ],
        'responsive' => true,
        'columnDefs' => [
            [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
    
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'ejemploeloquent_' . time();
    }
}