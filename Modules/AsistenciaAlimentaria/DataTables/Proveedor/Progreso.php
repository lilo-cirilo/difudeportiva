<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Proveedor;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

class Progreso extends CustomDataTable{
    
    public function dataTable($query){
        return datatables($query)->editColumn('estado',function($item){
            $var = '';
            switch ($item->estado) {
                case 'ENVIADA':
                    $var='warning';
                    break;
                case 'RECIBIDA':
                    $var='success';
                    break;
                case 'NO RECIBIDA':
                    $var='danger';
                    break;
                default:
                    $var='light';
                    break;
            }
            return "<span class='badge badge-pill badge-$var'> $item->estado</span>";
        })->rawColumns(['estado']);
    }
    
    public function query(){
        $model = Programacion::join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
        ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
        ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
        ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
        ->join('alim_programacion_requisiciones','alim_programacion_requisiciones.programacion_id','alim_programacion.id')
        ->join('alim_requisiciones','alim_requisiciones.id','alim_programacion_requisiciones.requisicion_id')
        ->join('alim_recibos','alim_recibos.programacion_requisicion_id','alim_programacion_requisiciones.id')
        ->join('estados_programas','estados_programas.id','alim_programacion_requisiciones.estado_programa_id')
        ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
        ->where('alim_programacion_requisiciones.requisicion_id',$this->requisicion_id)
        ->whereNull('alim_programacion_requisiciones.deleted_at')
        ->select([
            'alim_programacion_requisiciones.id as req_prog_id',
            'alim_recibos.id as recibo',
            'alim_alimentarios.foliodif as cocina',
            'cat_municipios.nombre as municipio',
            'cat_localidades.nombre as localidad',
            'cat_distritos.nombre as distrito',
            'alim_alimentarios.sublocalidad as sublocalidad',
            'cat_regiones.nombre as region',
            'alim_requisiciones.num_bimestre as bimestre',
            'alim_requisiciones.num_entrega as entrega',
            'alim_requisiciones.periodo',
            DB::raw('(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            where pr.programacion_id=alim_programacion.id and cd.deleted_at is null) as dot_totales'),
            'cat_estados.nombre as estado'
        ]);
        
        
        return $model;
        
    }
    
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->parameters($this->getBuilderParameters());
    }
    
    protected function getColumns(){
        $columns =
        [
            'estado'        => ['name' => 'cat_estados.nombre'],
            'recibo'        => ['name' => 'alim_recibos.id'],
            'cocina'        => ['name' => 'alim_alimentarios.foliodif', 'title'=>'f. dIf'],
            'region'        => ['name' => 'cat_regiones.nombre'],
            'distrito'      => ['name' => 'cat_distritos.nombre'],
            'municipio'     => ['name' => 'cat_municipios.nombre'],
            'localidad'     => ['name' => 'cat_localidades.nombre'],
            'sublocalidad'  => ['name' => 'alim_alimentarios.sublocalidad'],
            'bimestre'      => ['name' => 'alim_requisiciones.num_bimestre'],
            'entrega'       => ['name' => 'alim_requisiciones.num_entrega'],
            'periodo'       => ['name' => 'alim_requisiciones.periodo'],
            'dot_totales'   => ['searchable' => false, 'orderable'=>false],
        ];
        return $columns;
    }
    
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'buttons'=>['colvis'=>'Columnas visibles'],
                'select' => [
                    'rows' => [
                        '_' => "hay %d filas seleccionadas",
                        '0' => "Da click para seleccionar una fila",
                        '1' => "1 fila seleccionada"
                    ]
                ]
            ],
            'deferRender'=> true,
            'rowId' => 'req_prog_id',
            'dom' => 'lBtip',
            'initComplete'=>'function(){
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on("change", function () {
                        column.search($(this).val()).draw();
                    });
                });
            }',
            /* 'drawCallback'=>'function(){
                $("tr:contains(ENVIADA)").addClass("table-warning")
                $("tr:contains(NO RECIBIDA)").addClass("table-danger")
                $("tr:contains(RECIBIDA):NOT(.table-danger)").addClass("table-success")
            }', */
            'buttons' => [
                'reset', 'reload',
            ],
            'lengthMenu' => [  10, 5, 20 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        
        return $builderParameters;
    }
    
    protected function filename(){
        return 'Requisición al ' . date('Ymd');
    }
}
