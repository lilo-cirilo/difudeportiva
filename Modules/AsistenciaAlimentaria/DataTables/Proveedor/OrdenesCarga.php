<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Proveedor;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\DatoCarga;

class OrdenesCarga extends CustomDataTable{
    
    public function dataTable($query){
        return datatables($query)->addColumn('cancelar',function($ordenCarga){
            $aux = DatoCarga::join('alim_requisiciones_datoscarga','alim_requisiciones_datoscarga.datocarga_id','alim_datoscarga.id')
            ->leftJoin('alim_entregasproveedor','alim_entregasproveedor.requisicion_datocarga_id','alim_requisiciones_datoscarga.id')
            ->whereNotNull('fecha_entrega')->where('alim_datoscarga.id',$ordenCarga->id)->count();
            if($aux == 0)
                return "<button class='btn btn-sm btn-block btn-danger' onClick='cancelarOrdenCarga($ordenCarga->id)'><i class='fas fa-ban'></i> CANCELAR</button>";
            else
                return "<button class='btn btn-sm btn-block btn-danger disabled'><i class='fas fa-ban'></i> CANCELAR</button>";
        })->rawColumns(['cancelar']);
    }
    
    public function query(){
        return DatoCarga::join('alim_choferesproveedor','alim_choferesproveedor.id','alim_datoscarga.choferproveedor_id')
        ->select([
            'alim_datoscarga.id',
            'placas',
            'ruta',
            'fecha'
        ])
        ->selectRaw('concat_ws(" ",nombre,primer_apellido,segundo_apellido) as chofer');
    }
    
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->parameters($this->getBuilderParameters());
    }
    
    protected function getColumns(){
        $columns =
        [
            'id',
            'placas',
            'ruta',
            'fecha',
            'chofer'   => ['searchable'=>false,'orderable'=>false],
            'cancelar' => ['searchable'=>false,'orderable'=>false]
        ];
        return $columns;
    }
    
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'buttons'=>['colvis'=>'Columnas visibles'],
                'select' => [
                    'rows' => [
                        '_' => "hay %d filas seleccionadas",
                        '0' => "Da click para seleccionar una fila",
                        '1' => "1 fila seleccionada"
                    ]
                ]
            ],
            'deferRender'=> true,
            'rowId' => 'req_prog_id',
            'dom' => 'lBtip',
            /* 'initComplete'=>'function(){
                this.api().columns().every(function () {
                    var column = this;
                    var input = document.createElement("input");
                    $(input).appendTo($(column.footer()).empty())
                    .on("change", function () {
                        column.search($(this).val()).draw();
                    });
                });
            }', */
            'buttons' => [
                'reset', 'reload',
            ],
            'lengthMenu' => [  10, 5, 20 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        
        return $builderParameters;
    }
    
    protected function filename(){
        return 'Requisición al ' . date('Ymd');
    }
}