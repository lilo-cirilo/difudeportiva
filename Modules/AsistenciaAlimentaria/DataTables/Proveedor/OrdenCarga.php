<?php

namespace Modules\AsistenciaAlimentaria\DataTables\Proveedor;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

class OrdenCarga extends CustomDataTable{
    
    public function dataTable($query){
        $tabla = datatables($query);
        return $tabla;
    }
    
    public function query(){
        $model = Programacion:://join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
        join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
        //->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
        ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
        ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
        ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
        ->join('alim_programacion_requisiciones','alim_programacion_requisiciones.programacion_id','alim_programacion.id')
        ->join('alim_requisiciones','alim_requisiciones.id','alim_programacion_requisiciones.requisicion_id')
        ->join('alim_recibos','alim_recibos.programacion_requisicion_id','alim_programacion_requisiciones.id')
        ->join('estados_programas','estados_programas.id','alim_programacion_requisiciones.estado_programa_id')
        ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
        ->where(function($q){
            $q->where('cat_estados.nombre','REVISION')
            ->orWhere('cat_estados.nombre','CANCELADA');
        })
        ->where('alim_programacion_requisiciones.requisicion_id',$this->requisicion->id)
        ->whereNull('alim_programacion_requisiciones.deleted_at')
        ->select([
            'alim_programacion_requisiciones.id as req_prog_id',
            'alim_recibos.id as recibo',
            'alim_alimentarios.foliodif as cocina',
            'cat_municipios.nombre as municipio',
            'cat_localidades.nombre as localidad',
            'cat_distritos.nombre as distrito',
            'alim_alimentarios.sublocalidad as sublocalidad',
            'cat_regiones.nombre as region',
            'alim_requisiciones.num_bimestre as bimestre',
            'alim_requisiciones.num_entrega as entrega',
            'alim_requisiciones.periodo',
            DB::raw('(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
            inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
            where pr.id=alim_programacion_requisiciones.id and cd.deleted_at is null) as dot_totales')
            ]);
            
            
            return $model;
            
        }
        
        public function html(){
            return $this
            ->builder()
            ->columns($this->getColumns())
            ->parameters($this->getBuilderParameters());
        }
        
        protected function getColumns(){
            $columns =
            [
                //'programacion_id' => ['data' => 'programacion_id', 'name' => 'alim_programacion.id', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false, 'visible' => false],
                'recibo'        => ['name' => 'alim_recibos.id','className'=>'noselect'],
                'cocina'        => ['name' => 'alim_alimentarios.foliodif', 'title'=>'f. dIf'],
                'region'        => ['name' => 'cat_regiones.nombre'],
                'distrito'      => ['name' => 'cat_distritos.nombre'],
                'municipio'     => ['name' => 'cat_municipios.nombre'],
                'localidad'     => ['name' => 'cat_localidades.nombre'],
                'sublocalidad'  => ['name' => 'alim_alimentarios.sublocalidad'],
                'bimestre'      => ['name' => 'alim_requisiciones.num_bimestre'],
                'entrega'       => ['name' => 'alim_requisiciones.num_entrega'],
                'periodo'       => ['name' => 'alim_requisiciones.periodo'],
                'dot_totales'   => ['searchable' => false, 'orderable'=>false],
            ];
            return $columns;
        }
        
        protected function getBuilderParameters(){
            $crear = route('alimentarios.proveedor.requisicion.ordenCarga.create',[$this->proveedor_id,$this->requisicion->id]);
            $builderParameters =
            [
                'language' => [
                    'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                    'buttons'=>['colvis'=>'Columnas visibles'],
                    'select' => [
                        'rows' => [
                            '_' => "hay %d filas seleccionadas",
                            '0' => "Da click para seleccionar una fila",
                            '1' => "1 fila seleccionada"
                        ]
                    ]
                ],
                'deferRender'=> true,
                'rowId' => 'req_prog_id',
                'dom' => 'lBtip',
                'initComplete'=>'function(){
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                        .on("change", function () {
                            column.search($(this).val()).draw();
                        });
                    });
                }',
                'drawCallback'=>'function(){selectItems()}',
                    'buttons' => [
                        'reset', 'reload',
                        [
                            'text'=>'<i class="far fa-hand-pointer"></i> Orden de carga',
                            'action' => "function(){
                                modal.loadModal(\"$crear\",\"create\")
                            }"
                        ],
                    ],
                    'lengthMenu' => [  10, 5, 20 ],
                    'responsive' => true,
                    'columnDefs' => [
                        [ 'className' => 'text-center', 'targets' => '_all' ]
                    ],
                    'select' => ['style'=> 'multi']
                ];
                
                return $builderParameters;
            }
            
            protected function filename(){
                return 'Requisición al ' . date('Ymd');
            }
        }
        