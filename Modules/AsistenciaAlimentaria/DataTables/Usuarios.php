<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;
use Illuminate\Support\Facades\DB;

use App\Models\Persona;
use App\Models\Usuario;
use Modules\AsistenciaAlimentaria\Entities\RegionesUsuarios;

class Usuarios extends CustomDataTable{
  
  public function dataTable($query){
    return datatables($query)
    ->addColumn('region',function ($usuario){
      $ru = RegionesUsuarios::where('access_usuario_id',$usuario->id)->first();
      return ($ru ? $ru->region->nombre : '');
    })
    ->addColumn('rol',function ($usuario){
      $usuario = Usuario::join('usuarios_roles','usuarios_roles.usuario_id','usuarios.id')
                        ->join('cat_roles','cat_roles.id','usuarios_roles.rol_id')
                        ->where('usuarios_roles.modulo_id', config('asistenciaalimentaria.moduloId'))
                        ->where('usuarios.id',$usuario->id)
                        ->get(['cat_roles.nombre as nombre']);
      $roles = '';
      foreach($usuario as $uroles)
      {
        $roles = $roles.' '.$uroles->nombre.' ';
      }
      return $roles;
    })
    ->addColumn('asignar',function ($usuario){
      return "<button class='badge badge-secondary btn-sm' onClick='modal.loadModal(\"/alimentarios/usuarios/".$usuario->id."\",\"edit\")'><i class='fas fa-indent'></i></button>";
    })->rawColumns(['asignar']);
  }
  
  public function query(){
    $model = Usuario::join('personas','personas.id','usuarios.persona_id')
                    ->join('usuarios_roles','usuarios_roles.usuario_id','usuarios.id')
                    ->where('usuarios_roles.modulo_id',config('asistenciaalimentaria.moduloId'))
                    ->groupBy(DB::raw('1,2,3,4'))
                    ->select([
                            'usuarios.id',
                            'personas.nombre as nombre',
                            'primer_apellido',
                            'segundo_apellido'
                          ]);
    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns = [];
    $columns['nombre']  = ['data' => 'nombre','name' => 'personas.nombre'];
    $columns['primer_apellido']  = ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'];
    $columns['segundo_apellido']  = ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'];
    $columns['rol']  = ['data' => 'rol', 'name' => 'rol'];
    $columns['region']  = ['data' => 'region', 'name' => 'region'];
    $columns['asignar'] = ['data' => 'asignar' , 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        /* [
            'extends'=>'new',
            'text'=>'<i class="fas fa-plus"></i> Agregar',
            'action'=>'function(){
                modal.loadModal(window.location.href+"/usuarios/create","create");
            }'
        ], */
        'pdf',
        'excel',
        'reset',
        'reload'
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ]
    ];
    return $builderParameters;
  }

  protected function filename(){
    return 'UsuariosAlimentarios al ' . date('Ymd');
  }
}
