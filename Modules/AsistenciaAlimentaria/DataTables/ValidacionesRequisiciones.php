<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use Modules\AsistenciaAlimentaria\Entities\Requisicion;

class ValidacionesRequisiciones extends CustomDataTable{
  
  public function dataTable($query){
    $tabla = datatables($query)
    ->addColumn('listado',function($requisicion){
        if($requisicion->estado != 'REVISION'){
          return "<button class='btn btn-secondary disabled' ><i class='fas fa-th-list'></i></button>";
        }
      return '<a href="/alimentarios/requisicion/'.$requisicion->id.'/programaciones/create" class="btn btn-secondary btn-success"><i class="fa fa-th-list" aria-hidden="true"></i></a>';
    })
    ->addColumn('pertenecientes',function($requisicion){
      return '<a href="/alimentarios/requisicion/'.$requisicion->id.'/programaciones" class="btn btn-secondary btn-info"><i class="fa fa-th-list" aria-hidden="true"></i></a>';
    })
    ->editColumn('especial',function($requisicion){
      return $requisicion->especial == 1 ? '<span class="badge badge-info">SI</span>' : '<span class="badge badge-warning">NO</span>';
    })
    ->editColumn('fecha_oficio',function($requisicion){
      return Carbon::parse($requisicion->fecha_oficio)->format('d/m/Y');
    })
    ->editColumn('estado',function($requisicion){
      $color = 'warning';
      switch ($requisicion->estado) {
        case 'ENVIADA':
          $color = 'info';
          break;
        case 'RECIBIDA':
          $color = 'success';
          break;
        case 'CANCELADA':
          $color = 'danger';
          break;
        case 'PAGADA':
          $color = 'dark';
          break;
        default:
          break;
      }
      return '<span class="badge badge-'.$color.'">'.$requisicion->estado.'</span>';
    })
    ->editColumn('periodo',function($requisicion){
      return ($requisicion->periodo == 1 ? '1 MES' : '2 MESES');
    })
    ->addColumn('redirect',function($requisicion){
      return "<a class='btn btn-xs btn-success' href='".route('alimentarios.validacion.entregas.index',[$this->oficio_validacion_id,$requisicion->id])."'><i class='fas fa-check'></i> Ir</a>";
    })
    ->rawColumns(['redirect','especial','estado']);
    return $tabla;
  }
  
  public function query(){
    return Requisicion::join('estados_programas','estados_programas.id','alim_requisiciones.estado_programa_id')
                      ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
                      ->join('alim_cat_tiposentrega','alim_cat_tiposentrega.id','alim_requisiciones.tipo_entrega_id')
                      ->join('alim_licitaciones','alim_licitaciones.id','alim_requisiciones.licitacion_id')
                      ->where('alim_requisiciones.programa_id',$this->programa_id)
                      ->select(['alim_requisiciones.*','cat_estados.nombre as estado','alim_cat_tiposentrega.nombre as tipoentrega','alim_licitaciones.num_licitacion as numlicitacion']);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      
      'entrega'  =>  ['title' => 'Tipo entrega', 'data' => 'tipoentrega', 'name' => 'alim_cat_tiposentrega.nombre'],
      'num_entrega'  =>  ['title' => '# Entrega', 'data' => 'num_entrega', 'name' => 'num_entrega'],
      'observaciones' =>  ['data' => 'observaciones', 'name' => 'observaciones'],
      'redirect' => ['title' => 'Ir a validación', 'data' => 'redirect'],
      'bimestre'  =>  ['data' => 'num_bimestre', 'name' => 'num_bimestre'],
      'oficio' =>  ['title' => '#Oficio', 'data' => 'num_oficio', 'name' => 'num_oficio'],
      'fechaoficio' =>  ['title' => 'Fecha oficio', 'data' => 'fecha_oficio', 'name' => 'fecha_oficio'],
      'estado_programa' => ['title' => 'Estado', 'data' => 'estado', 'name' => 'cat_estados.nombre'],
      'periodo' =>  ['data' => 'periodo', 'name' => 'periodo'],
      'especial' =>  ['title' => 'Es especial', 'data' => 'especial', 'name' => 'especial'],
      'numlicitacion' => ['title' => '# Licitación', 'data' => 'numlicitacion', 'name' => 'alim_licitaciones.num_licitacion'],
    ];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'drawCallback'=>'function(){
       }',
      'buttons' => [
        'pdf' , 'excel',
        'reset', 'reload'
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];
    
    return $builderParameters;
  }

  protected function filename(){
    return 'ValidaciónRequisición al ' . date('Ymd');
  }
}
