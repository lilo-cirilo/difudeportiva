<?php
namespace Modules\AsistenciaAlimentaria\DataTables;

use Illuminate\Support\Facades\DB;
 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Modules\AsistenciaAlimentaria\Entities\Dotacion;
use Modules\AsistenciaAlimentaria\Entities\Programacion;

class CuotasRecuperacion extends CustomDataTable {
  
  public function dataTable($query){
    return datatables($query);
  }

  public function query(){
    $query = Programacion::join('anios_programas as ap','ap.id','alim_programacion.anios_programa_id')
                          ->join('cat_ejercicios as ce','ce.id','ap.ejercicio_id')
                          ->join('alim_alimentarios as aa','aa.id','alim_programacion.alimentario_id')
                          ->join('cat_municipios as cm','cm.id','aa.municipio_id')
                          ->leftJoin('cat_localidades as cl','cl.id','aa.localidad_id')
                          ->join('cat_distritos as cd','cd.id','cm.distrito_id')
                          ->join('cat_regiones as cr','cr.id','cd.region_id')
                          ->join('estados_programas as ep','ep.id','alim_programacion.estado_programa_id')
                          ->join('cat_estados as ces','ces.id','ep.estado_id');
    $select = [
      'ce.anio as ejercicio',
      'aa.foliodif',
      'cr.id as cve_region',
      'cr.nombre as region',
      'cd.id as cve_distrito',
      'cd.nombre as distrito',
      'cm.id as cve_municipio',
      'cm.nombre as municipio',
      'cl.cve_localidad',
      'cl.nombre as localidad',
      'aa.sublocalidad',
      'ces.nombre as estado'
    ];
    $dotaciones = Dotacion::join('alim_tiposaccion as ta','ta.id','tipoaccion_id')
                          ->join('cat_ejercicios as ce','ce.id','ejercicio_id')
                          ->where('ce.anio',config('asistenciaalimentaria.anioActual'))
                          ->where('programa_id',$this->programa_id)->get(['alim_dotaciones.id']);
    $i = 1;
    $inversion = 'select ';
    foreach ($dotaciones  as $dotacion) {
      array_push(
        $select,
        DB::raw(
          "(select ifnull(sum(cdr.cantidad_dotaciones),0)
          from alim_cantidadesdotaciones_requisiciones as cdr inner join alim_programacion_requisiciones as pr on pr.id = cdr.programacion_requisicion_id
          where cdr.dotacion_id=$dotacion->id and pr.programacion_id=alim_programacion.id and pr.deleted_at is null) as dot$i"
        ),
        DB::raw(
          "(select (dot$i * costo) from alim_detallesdotacion as dd
          inner join alim_costos_productos as cp on cp.id = dd.costo_producto_id
          where dd.dotacion_id=$dotacion->id and dd.deleted_at is null) as dd$i"
        )
      );
      if($i==1) {
        $inversion="$inversion dd$i";
      } else {
        $inversion="$inversion +dd$i";
      }
      $i++;
    }
    array_push(
      $select,
      DB::raw("(select ifnull(sum(cdr.cantidad_dotaciones),0)
      from alim_cantidadesdotaciones_requisiciones as cdr inner join alim_programacion_requisiciones as pr on pr.id = cdr.programacion_requisicion_id
      where pr.programacion_id=alim_programacion.id and pr.deleted_at is null) as dotTotal"),
      DB::raw("($inversion) as inversion"),
      DB::raw("(
        select ifnull(sum(cuota_sum),0)
        from
        (select pr.programacion_id as progr_id, ((d.cuota_recuperacion*r.periodo)*cdr.cantidad_beneficiarios) as cuota_sum from alim_programacion_requisiciones as pr
        inner join alim_requisiciones r on r.id = pr.requisicion_id
        inner join alim_cantidadesdotaciones_requisiciones as cdr on cdr.programacion_requisicion_id = pr.id
        inner join alim_dotaciones as d on d.id = cdr.dotacion_id
        inner join alim_tiposaccion as ta on ta.id = d.tipoaccion_id
        and r.deleted_at is null and d.deleted_at is null and ta.deleted_at is null) as t
        where progr_id = alim_programacion.id
			) as totalRecuperar"),
			DB::raw("(
				select ifnull(sum(r.importe_total),0) from alim_recibos as r
				inner join alim_programacion_requisiciones as pr on pr.id = r.programacion_requisicion_id
				where pr.programacion_id = alim_programacion.id and pr.deleted_at is null
			) as recuperado"),
			DB::raw("(select (totalRecuperar-recuperado)) as adeudo"),
			DB::raw("(
				select count(*) from alim_requisiciones as r
				inner join alim_programacion_requisiciones as pr on pr.requisicion_id = r.id
				where pr.programacion_id = alim_programacion.id
				and pr.deleted_at is null
			) as Num_entregas"),
			DB::raw("(
				select GROUP_CONCAT(CONCAT('(','B:',num_bimestre,'-','p:',num_entrega,')')) from alim_requisiciones as r
				inner join alim_programacion_requisiciones as pr on pr.requisicion_id = r.id
				where pr.programacion_id = alim_programacion.id and pr.deleted_at is null
			) as entregas")
    );

    $query->where('ce.anio',config('asistenciaalimentaria.anioActual'))
          ->where('ap.programa_id','=',$this->programa_id);
    return $query->select($select);
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns = [];
    $columns['ejercicio'] = ['name' => 'ce.anio'];
    $columns['foliodif'] = ['name' => 'aa.foliodif'];
    $columns['cve_region'] = ['name' => 'cr.id', 'data' => 'cve_region'];
    $columns['region'] = ['name' => 'cr.nombre', 'data' => 'region'];
    $columns['cve_distrito'] = ['name' => 'cd.id', 'data' => 'cve_distrito'];
    $columns['distrito'] = ['name' => 'cd.nombre', 'data' => 'distrito'];
    $columns['cve_municipio'] = ['name' => 'cm.id', 'data' => 'cve_municipio'];
    $columns['municipio'] = ['name' => 'cm.nombre', 'data' => 'municipio'];
    $columns['cve_localidad'] = ['name' => 'cl.cve_localidad'];
    $columns['localidad'] = ['name' => 'cl.nombre', 'data' => 'localidad'];
    $columns['sublocalidad'] = ['name' => 'aa.sublocalidad'];
    $columns['estado'] = ['name' => 'ces.nombre', 'data' => 'estado'];

    $dotaciones = Dotacion::join('alim_tiposaccion as ta','ta.id','tipoaccion_id')
                          ->join('cat_ejercicios as ce','ce.id','ejercicio_id')
                          ->where('ce.anio',config('asistenciaalimentaria.anioActual'))
                          ->where('programa_id',$this->programa_id)->get();
    $i = 1;
    foreach ($dotaciones  as $dotacion) {
      $as = explode(')',explode('(',$dotacion->tipoaccion->nombre)[1])[0];
      $columns["dot$i"] = ['title' => "DOT. DE $as", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
      $i++;
    }
    $columns['dotTotal'] = ['title' => "DOT. TOTAL", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
    $columns['inversion'] = ['title' => "Inversión", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
		$columns['totalRecuperar'] = ['title' => "Total a Recuperar", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
		$columns['recuperado'] = ['title' => "Recuperado", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
		$columns['adeudo'] = ['title' => "Adeudo", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
		$columns['Num_entregas'] = ['title' => "Núm. Entregas", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
		$columns['entregas'] = ['title' => "Entregas", 'searchable' => false, /* 'exportable' => false, 'printable' => false */];
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =[
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'lBtip',
      'buttons' => [
        'reset','reload','pdf','excel',
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => false,
      'scrollX' =>        true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
      ],
      'order'  => [[ 1, 'asc' ]],
      'drawCallback' => 'function() {}'
    ];
    return $builderParameters;
  }
}