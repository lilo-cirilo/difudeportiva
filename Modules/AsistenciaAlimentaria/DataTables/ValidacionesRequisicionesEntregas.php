<?php

namespace Modules\AsistenciaAlimentaria\DataTables;

use Illuminate\Support\Facades\DB;

 use Modules\AsistenciaAlimentaria\DataTables\CustomDataTable;

use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\EntregaProveedor;
use Modules\AsistenciaAlimentaria\Entities\ValidacionEntrega;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;

class ValidacionesRequisicionesEntregas extends CustomDataTable{
  
  public function dataTable($query){
    $tabla = datatables($query)
    ->editColumn('estado',function($item){
        $var = '';
        switch ($item->estado) {
            case 'ENVIADA':
                $var='warning';
                break;
            case 'RECIBIDA':
                $var='success';
                break;
            case 'NO RECIBIDA':
                $var='danger';
                break;
            default:
                $var='light';
                break;
        }
        return "<span class='badge badge-pill badge-$var'> $item->estado</span>";
    })
    ->addColumn('aprobada',function($item){
        $aprobada = 3;
        // if (isset($item->programacionrequisicion->requisicionDatoCarga)) {
            if(isset($item->entregaproveedor_id) && isset(EntregaProveedor::find($item->entregaproveedor_id)->validacion)){//isset($item->programacionrequisicion->requisicionDatoCarga->entregaproveedor) && isset($item->programacionrequisicion->requisicionDatoCarga->entregaproveedor->validacion)) {
                $aprobada = EntregaProveedor::find($item->entregaproveedor_id)->validacion->validacion_aprobada;
            }
        // }
        $var = '';
        $nombre = '';
        switch ($aprobada) {
            case 0:
                $var='warning';
                $nombre='NO APROBADO';
                break;
            case 1:
                $var='success';
                $nombre='APROBADO';
                break;
            default:
                $var='light';
                $nombre='SIN VALIDAR';
                break;
        }
        return "<span class='badge badge-pill badge-$var'> $nombre</span>";
    })
    ->editColumn('folio',function($query){
      $requisicion = '';
      if(RequisicionProgramacion::where('programacion_id',$query->programacion_id)->count() > 0)
        $requisicion = 'esrequisicion';
      return $query->folio.'<span id="'.$query->programacion_id.'" class="pdi '.$requisicion.'"></span>';
    })
    ->addColumn('validar',function($requisicion){
      $ruta = route('alimentarios.validacion.entregas.create',[$this->ofi_id, $requisicion->requisicion_id]);
      return "<button class='btn btn-xs btn-success' onClick='modal.loadModal(\"$ruta\",\"edit\",{prog_req_id:$requisicion->programacion_requisicion_id})'><i class='fas fa-check-double'></i> Validar</a>";
    })
    ->rawColumns(['folio','validar','estado','aprobada']);
    return $tabla;
  }
  
  public function query(){
    $model = Programacion::join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
    ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
    ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
    ->leftJoin('cat_localidades','cat_localidades.id','alim_alimentarios.localidad_id')
    ->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')
    ->join('cat_regiones','cat_regiones.id','cat_distritos.region_id')
    ->join('alim_programacion_requisiciones','alim_programacion_requisiciones.programacion_id','alim_programacion.id')
    ->join('alim_requisiciones','alim_requisiciones.id','alim_programacion_requisiciones.requisicion_id')
    ->join('estados_programas','estados_programas.id','alim_programacion_requisiciones.estado_programa_id')
    ->join('cat_estados','cat_estados.id','estados_programas.estado_id')
    ->leftJoin('alim_requisiciones_datoscarga','alim_requisiciones_datoscarga.programacion_requisicion_id','alim_programacion_requisiciones.id')
    ->leftJoin('alim_entregasproveedor','alim_entregasproveedor.requisicion_datocarga_id','alim_requisiciones_datoscarga.id')
    ->leftJoin('alim_recibos','alim_recibos.programacion_requisicion_id','alim_programacion_requisiciones.id')
    ->where('alim_programacion_requisiciones.requisicion_id',$this->requisicion->id)
    ->whereNull('alim_programacion_requisiciones.deleted_at')
    ->orderBy('cat_estados.nombre','desc');

    if($this->requisicion->programa_id == config('asistenciaalimentaria.desayunosId')){
      $model->join('alim_desayunos','alim_desayunos.alimentario_id','alim_alimentarios.id')
      ->join('cat_escuelas','cat_escuelas.id','alim_desayunos.escuela_id');
    }

    $select=[
      'cat_ejercicios.anio as ejercicio',
      'alim_recibos.id as foliorecibo',
      'alim_programacion_requisiciones.requisicion_id as requisicion',
      'alim_requisiciones.num_bimestre as bimestre',
      'alim_requisiciones.num_entrega as entrega',
      'alim_alimentarios.foliodif as foliodif',
      'cat_regiones.id as cve_region',
      'cat_regiones.nombre as region',
      'cat_distritos.id as cve_distrito',
      'cat_distritos.nombre as distrito',
      'cat_municipios.id as cve_municipio',
      'cat_municipios.nombre as municipio',
      'cat_localidades.cve_localidad',
      'cat_localidades.nombre as localidad',
      'alim_alimentarios.sublocalidad as sublocalidad',
      'alim_programacion.id',
      'alim_requisiciones.periodo',
      'alim_requisiciones.id as requisicion_id',
      'alim_entregasproveedor.id as entregaproveedor_id',
      'alim_programacion_requisiciones.id as programacion_requisicion_id',
      'cat_estados.nombre as estado',
      DB::raw('(select sum(cantidad_dotaciones) from alim_cantidadesdotaciones_requisiciones cd
      inner join alim_programacion_requisiciones pr on pr.id = cd.programacion_requisicion_id
      where pr.programacion_id=alim_programacion.id and cd.deleted_at is null) as dot_totales')
    ];

    if($this->requisicion->programa_id == config('asistenciaalimentaria.desayunosId')){
      array_push($select,
        'cat_escuelas.nombre as escuela',
        'cat_escuelas.clave as cve_escuela'
      );
    }

    $model->select($select);

    return $model;
  }

  public function html(){
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->parameters($this->getBuilderParameters());
  }

  protected function getColumns(){
    $columns =
    [
      'foliorecibo'         => ['name' => 'alim_recibos.id', 'data' => 'foliorecibo', 'title' => 'Folio Recibo'],
      'folio'               => ['name' => 'alim_alimentarios.foliodif', 'data' => 'foliodif', 'title' => 'FolioDIF'],
      'validar'             => ['data' => 'validar', 'searchable'=>false],
      'entrega'             => ['name' => 'alim_requisiciones.num_entrega', 'data' => 'entrega'],
      'estado'              => ['name' => 'cat_estados.nombre', 'data' => 'estado'],
      'pago'                => ['searchable' => false, 'data' => 'aprobada'],
      'region'              => ['name' => 'cat_regiones.nombre'],
      'cve_region'          => ['name' => 'cat_regiones.id', 'visible'=>false],
      'bimestre'            => ['name' => 'alim_requisiciones.num_bimestre', 'data' => 'bimestre'],
      'periodo'             => ['name' => 'alim_requisiciones.periodo'],
      'Total dotaciones'    => ['data' => 'dot_totales', 'searchable'=>false],
      'distrito'            => ['name' => 'cat_distritos.nombre'],
      'cve_distrito'        => ['name' => 'cat_distritos.id', 'visible'=>false],
      'municipio'           => ['name' => 'cat_municipios.nombre'],
      'cve_municipio'       => ['name' => 'cat_municipios.id', 'visible'=>false],
      'localidad'           => ['name' => 'cat_localidades.nombre'],
      'cve_localidad'       => ['name' => 'cat_localidades.cve_localidad', 'visible'=>false],
      'sublocalidad'        => ['name' => 'alim_alimentarios.sublocalidad'],
      'ejercicio'           => ['name' => 'cat_ejercicios.anio', 'data' => 'ejercicio'],
      'requisicion'         => ['name' => 'alim_programacion_requisiciones.requisicion_id', 'data' => 'requisicion', 'title' => 'Requisición'],
    ];

    if($this->requisicion->programa_id == config('asistenciaalimentaria.desayunosId')){
      $coumns['escuela']     = ['name' => 'cat_escuelas.nombre'];
      $coumns['cve_escuela'] = ['name' => 'cat_escuelas.clave'];
    }
    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'buttons'=>['colvis'=>'Columnas visibles']
      ],
      'dom' => 'lBtip',
      'initComplete' => ' function () {
        this.api().columns().every(function () {
            var column = this;
            if(column[0] < 17){
                var input = document.createElement("input");
                $(input).addClass("column-filter")
                $(input).addClass("text-center")
                $(input).attr("placeholder","Filtrar "+column.header().textContent.toLowerCase())
                $(input).appendTo($(column.footer()).empty())
                .on("change", function () {
                    column.search($(this).val(), false, false, true).draw();
                });
            }
        });
      }',
      'buttons' => [
        'excel','reset', 'reload',
        [
          'extend'=>'colvis',
          'collectionLayout' => 'fixed three-column',
          'text' => '<i class="fas fa-columns"></i> Columnas visibles',
          // 'columns' => [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]
        ],
      ],
      'lengthMenu' => [  10, 5, 20 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ],
    ];
    
    return $builderParameters;
  }

  protected function filename(){
    return 'Requisición al ' . date('Ymd');
  }
}
