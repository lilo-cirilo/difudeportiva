<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use Carbon\Carbon;
use App\Models\Persona;
use App\Models\Programa;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Comite;
use Modules\AsistenciaAlimentaria\DataTables\Comites;
use Modules\AsistenciaAlimentaria\DataTables\ComitesAll;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class ComiteController extends Controller{
  use AlimentariosTrait;
  
  function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
  }

  public function index($programacion_id, Request $request, Comites $tabla) {
    $programacion=Programacion::findOrFail($programacion_id);
    return $tabla->with(['programacion'=>$programacion])
          ->render('asistenciaalimentaria::modal',['vista'=>'asistenciaalimentaria::comites.fragmentos.list','tipo'=>$request->get('id'),'programacion'=>$programacion]);
    /* return response()->json([
      'body'=>view::make('asistenciaalimentaria::modal')->with(['vista'=>'asistenciaalimentaria::comites.fragmentos.show']),
      'hola'=>200
      ]); */
  }
  
  public function create(Request $request,$programacion_id) {
    $array = [
      'tipo'=>$request->get('id'),
      'vista'=>'asistenciaalimentaria::beneficiarios.fragmentos.create_edit',
      'programacion'=>Programacion::find($programacion_id),
      'cargo_programa_id'=>$request->get('cargo_programa_id')
    ];
    if($request->persona_id)
      $array['persona'] = Persona::find($request->persona_id);
    return view::make('asistenciaalimentaria::modal',$array);
  }
  
  public function store($programacion_id, Request $request) {
    $valid = $this->puedeModificar($programacion_id);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    try {
      $datosPersona = $request->except(['cargo_programa_id']);
      $datosComite  = $request->only(['cargo_programa_id']);
      $datosComite['programacion_id']         = $programacion_id;
      $datosComite['usuario_id']              = auth()->User()->id;
      $datosPersona['fecha_nacimiento']       = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
      $datosPersona['segundo_apellido']       = $datosPersona['segundo_apellido']  ?? '';
      //primero comparamos nombre, apellidos y curp para ver si ya esta la persona
      //si ahora no tiene curp, comparamos nombres, apellidos,fecha de nacimiento, municipio
      DB::beginTransaction();
      $persona = null;
      try {
        $persona = Persona::firstOrCreate([
          'nombre'=>$datosPersona['nombre'],
          'primer_apellido'=>$datosPersona['primer_apellido'],
          'segundo_apellido'=>$datosPersona['segundo_apellido'],
          'curp'=>$datosPersona['curp']
        ],$datosPersona);
      } catch (\Exception $th) {
        $aux=Persona::where([
          ['nombre',$datosPersona['nombre']],
          ['primer_apellido',$datosPersona['primer_apellido']],
        ])->whereRaw('substring(curp,5,6) = ?',substr($datosPersona['curp'],5,6))
        ->whereRaw('substring(curp,17,2) = ?',substr($datosPersona['curp'],-2))->get();
        if(count($aux)==0 || count($aux)>1){
          if($th->errorInfo[0] == "22007")
            return new JsonResponse(['message'=>'Error al convertir fechas'],500);
          if($th->errorInfo[0] == "")
            return new JsonResponse(['message'=>'Conflicto de personas, por favor acudir a u. informatica'],409);
          return new JsonResponse(['message'=>$th->getMessage()],500);
        }
        $persona = $aux->first();
			}
			$validar_persona = $this->validarPersona($persona->id);
      if($validar_persona)
        return new JsonResponse(['message'=>"Esta persona esta en otro comité del folio {$validar_persona->alimentario->foliodif} de {$validar_persona->anioprograma->programa->nombre}"],409);//throw new \Exception('Esta persona esta en otro comité');// return new JsonResponse(['message'=>'Esta persona esta en otro comite'],409);
      $datosComite['persona_id']   = $persona->id;
      $persona->update($datosPersona);
      $new=Comite::create($datosComite);
      auth()->user()->bitacora(request(), [
          'tabla' => 'alim_comites',
          'registro' => $new->id . '',
          'campos' => json_encode($datosComite) . '',
          'metodo' => request()->method()
      ]);
      DB::commit();
      return response()->json(['success' => true],200);
    }catch(\Exception $e) {
      DB::rollBack();
      if(isset($e->errorInfo) && $e->errorInfo[0] == "23000")
        return new JsonResponse(['message'=>'Cargo ocupado','detalles'=>$e->getMessage()],409);
      return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }
  
  public function edit(Request $request,$programacion_id,$comite_id) {
    return view::make('asistenciaalimentaria::modal',
                [
                  'tipo'=>$request->get('id'),
                  'vista'=>'asistenciaalimentaria::beneficiarios.fragmentos.create_edit',
                  //'programacion_id'=>$programacion_id,
                  'programacion'=>Programacion::find($programacion_id),
                  'cargo_programa_id'=>Comite::find($comite_id)->cargo_programa_id,
                  'comite_id'=>$comite_id
                ]); 
  }

  public function update(Request $request,$programacion_id, $comite_id){
    $valid = $this->puedeModificar($programacion_id);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    try {
      //identificamos los datos de la persona y comite
      $datosPersona = $request->except(['cargo_programa_id']);
      $datosComite  = $request->only(['cargo_programa_id']);
      $datosComite['programacion_id']         = $programacion_id;
      $datosComite['usuario_id']              = auth()->User()->id;
      $datosPersona['fecha_nacimiento']       = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
      $datosPersona['segundo_apellido']       = $datosPersona['segundo_apellido']  ?? '';
      //primero comparamos nombre, apellidos y curp para ver si ya esta la persona
      //si ahora no tiene curp, comparamos nombres, apellidos,fecha de nacimiento, municipio
      DB::beginTransaction();
      $persona = null;
      try {
        $persona = Persona::firstOrCreate([
          'nombre'=>$datosPersona['nombre'],
          'primer_apellido'=>$datosPersona['primer_apellido'],
          'segundo_apellido'=>$datosPersona['segundo_apellido'],
          'curp'=>$datosPersona['curp']
        ],$datosPersona);
      } catch (\Exception $th) {
        $aux=Persona::where([
          ['nombre',$datosPersona['nombre']],
          ['primer_apellido',$datosPersona['primer_apellido']],
        ])->whereRaw('substring(curp,5,6) = ?',substr($datosPersona['curp'],5,6))
        ->whereRaw('substring(curp,17,2) = ?',substr($datosPersona['curp'],-2))->get();
        if(count($aux)==0 || count($aux)>1){
          if($th->errorInfo[0] == "22007")
            return new JsonResponse(['message'=>'Error al convertir fechas'],500);
          if($th->errorInfo[0] == "")
            return new JsonResponse(['message'=>'Conflicto de personas, por favor acudir a u. informatica'],409);
        }
        $persona = $aux->first();
      }
      //una vez con la persona verificamos que esta no este en ningún otro comite del año actual
      $validar_persona = $this->validarPersona($persona->id);
      if($validar_persona)
        return new JsonResponse(['message'=>"Esta persona esta en otro comité del folio {$validar_persona->alimentario->foliodif} de {$validar_persona->anioprograma->programa->nombre}"],409);
      //y si es una persona valida, se crea eñ nuevo 
      $datosComite['persona_id']   = $persona->id;
      $persona->update($datosPersona);
      $new=Comite::create($datosComite);
      auth()->user()->bitacora(request(), [
          'tabla' => 'alim_comites',
          'registro' => $new->id . '',
          'campos' => json_encode($datosComite) . '',
          'metodo' => request()->method()
      ]);
      //obtenemos el registro de la persona anterior en el cargo y lo eliminamos
      $anterior = Comite::findorfail($comite_id);
      auth()->user()->bitacora(request(), [
          'tabla' => 'alim_comites',
          'registro' => $anterior->id . '',
          'campos' => '',
          'metodo' => 'DELETE'
      ]);
      $anterior->delete();
      DB::commit();
      return response()->json(['success' => true],200);
    }catch(\Exception $e) {
      DB::rollBack();
      if(isset($e->errorInfo) && $e->errorInfo[0] == "23000")
        return new JsonResponse(['message'=>'Este cargo ya esta ocupado'],409);
      return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }

  public function show($programa_id, Request $request, ComitesAll $tabla) {
    return $tabla
          ->render('asistenciaalimentaria::comites.todos',['programa'=>Programa::find($programa_id)]);
  }

  public function destroy($programacion,$comite){
    $valid = $this->puedeModificar($programacion);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    try {
      DB::beginTransaction();
        $comite_baja = Comite::where('persona_id',$comite)->where('programacion_id',$programacion)->first();
        $comite_baja->delete();
        auth()->user()->bitacora(request(), [
            'tabla' => 'alim_beneficiarios',
            'registro' => $comite_baja->id . '',
            'campos' => '',
            'metodo' => request()->method()
        ]);
      DB::commit();
      return response()->json(array(200,'success'));
    } catch(\Exception $e) {
      DB::rollBack();
      return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }

  public function validarPersona($persona_id){
    return Programacion::has('comite.persona')->with('anioprograma.ejercicio','comite.persona')
    ->whereHas('anioprograma.ejercicio',function($query){
      $query->where('anio',config('asistenciaalimentaria.anioActual'));
    })
    ->whereHas('comite.persona',function($query) use ($persona_id){
      $query->where('id',$persona_id);
    })
    ->first();
  }

  public function persona($programacion_id,$persona_id,Request $request) {
    try {
      DB::beginTransaction();
      $p = Persona::find($persona_id);
      $data = $request->all();
      $data['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
      $p->update($data);
      DB::commit();
      return response()->json(['success' => true],200);
    }catch(\Exception $e) {
      DB::rollBack();
      return new JsonResponse(['message'=>$e],500);
    }
  }
}
