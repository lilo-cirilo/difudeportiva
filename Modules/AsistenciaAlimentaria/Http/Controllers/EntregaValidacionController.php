<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\Entities\Requisicion;
use Modules\AsistenciaAlimentaria\DataTables\ValidacionesRequisicionesEntregas;

use Modules\AsistenciaAlimentaria\Entities\ValidacionEntrega;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\OficioValidacion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Comite;

use App\Models\Programa;
use View;
// use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;

class EntregaValidacionController extends Controller
{
    function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,VALIDADOR');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($ofi_id, $req_id, ValidacionesRequisicionesEntregas $tabla)
    {
        return $tabla->with(['requisicion'=>Requisicion::find($req_id),'ofi_id'=>$ofi_id])->render('asistenciaalimentaria::requisicion.validacion.Programaciones');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($of_val_id,$req_id, Request $request){
        $oficio  = OficioValidacion::find($of_val_id);
        $progReq = RequisicionProgramacion::find($request->prog_req_id);
        $comite  = Comite::with(['cargoprograma:id,cargo_id','cargoprograma.cargo:id,nombre'])
        ->where('programacion_id',$progReq->programacion_id)->whereHas('cargoprograma',function($q){
            $q->whereHas('cargo',function($qu){
                $qu->where('nombre','not like','%CONTRALORIA%');
            });
        })->get();
        // return new JsonResponse($comite);
        return view::make('asistenciaalimentaria::modal',[
            'tipo'    => $request->get('id'),
            'vista'   => 'asistenciaalimentaria::requisicion.validacion.forms.create_validacion',
            'oficio'  => $oficio,
            'progReq' => $progReq,
            'comites'  => $comite,
            'tiposaccion' => TipoAccion::where('programa_id',$oficio->programa_id)->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($of_val, $req_id, Request $request)
    {
        try {
            DB::beginTransaction();
                $data = $request->only(['num_oficio']);
                $data['entregaproveedor_id'] = $request->entregaproveedor_id;
                $data['oficiovalidacion_id'] = $of_val;
                $data['validacion_aprobada'] = $request->estatus;
                $data['usuario_id']   = auth()->user()->id;
                $ve = ValidacionEntrega::create($data);
                auth()->user()->bitacora(request(), [
                    'tabla' => 'alim_validacionesentregas',
                    'registro' => $ve->id . '',
                    'campos' => json_encode($data) . '',
                    'metodo' => request()->method()
                ]);
            DB::commit();
            return new JsonResponse(['message'=>"Entrega con núm. oficio #{$request->input('entregaproveedor_id')}, validada."],200);
        }catch (Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($ofi_id, $req_id, $ent_id)
    {
        try {
            DB::beginTransaction();
                ValidacionEntrega::find($ent_id)->delete();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'alim_validacionesentregas',
                    'registro' => $ent_id.'',
                    'campos' =>  '',
                    'metodo' => request()->method()
                ]);
            DB::commit();
            return new JsonResponse(['message'=>"Eliminado con éxito"],200);
        }catch (Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>$e->getmessage()],500);
        }
    }
}
