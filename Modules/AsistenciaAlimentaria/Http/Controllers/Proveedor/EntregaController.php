<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Proveedor;


use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\AsistenciaAlimentaria\DataTables\Proveedor\Entrega;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionDatoCarga;
use Modules\AsistenciaAlimentaria\Entities\EntregaProveedor;
use Modules\AsistenciaAlimentaria\Entities\Requisicion;

use App\Models\EstadoPrograma;

use Carbon\Carbon;
use View;

class EntregaController extends Controller{
    public function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,PROVEEDOR');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($pro_id,$req_id,Entrega $tabla){
        return $tabla->with([
                'requisicion'=>Requisicion::find($req_id),
                'proveedor_id'=>$pro_id
            ])
            ->render('asistenciaalimentaria::proveedor.entregacarga',[
                'requisicion_id'=>$req_id,
                'proveedor_id'=>$pro_id
            ]);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($pro_id, $req_id, Request $request){
        $programacion = RequisicionProgramacion::find($request->get('req_prog_id'));
        return view::make('asistenciaalimentaria::modal',[
            'tipo'=>$request->get('id'),
            'vista'=>'asistenciaalimentaria::proveedor.modals.datos_entrega',
            'requisicion_id' => $req_id,
            'proveedor_id'=>$pro_id,
            'programacionrequisicion'=>$programacion
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($pro_id, $req_id,Request $request){
        $req = Requisicion::find($req_id);
        try{
            DB::beginTransaction();
            $datos=[
                'requisicion_datocarga_id'  => $request->requisicion_datocarga_id,
                'fecha_entrega'             => Carbon::createFromFormat('d/m/Y',$request->fecha_entrega)->format('Y-m-d'),
                'usuario_id'                => auth()->user()->id
            ];
            if($request->has('recibe_comite_id'))
                $datos['recibe_comite_id'] = $request->recibe_comite_id;
            else{
                $datos['observaciones'] = "$request->nombre $request->primer_apellido $request->segundo_apellido - $request->cargo";
            }
            $e = EntregaProveedor::firstOrCreate(['requisicion_datocarga_id'=>$request->requisicion_datocarga_id],$datos);
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_entregasproveedor',
                'registro' => $e->id . '',
                'campos' => json_encode($datos) . '',
                'metodo' => request()->method()
            ]);
            //se actualiza la requisicion programacion
            $estado= EstadoPrograma::with('estado')->where('programa_id',$req->programa_id)->wherehas('estado',function($q) use($request){
                $q->where('nombre',($request->has('entregado') ? 'RECIBIDA' : 'NO RECIBIDA'));
            })->first();
            $r = RequisicionDatoCarga::find($request->requisicion_datocarga_id)->programacionrequisicion;
            $r->update([
                'estado_programa_id' => $estado->id
            ]);
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_programacion_requisiciones',
                'registro' => $r->id . '',
                'campos' => "{estado_programa_id:$estado->id}",
                'metodo' => "PUT"
            ]);
            DB::commit();
            return new JsonResponse(['message'=>'Estatus actualizado con exito']);
        }catch(Exception $e){
            DB::rollback();
            return new JsonResponse(['message'=>'error:'.$e->getMessage()],500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($ofi_id, $req_id, $ent_id, Request $request)
    {
        $req = Requisicion::find($req_id);
        try{
            DB::beginTransaction();
            $datos=[
                'requisicion_datocarga_id'  => $request->requisicion_datocarga_id,
                'fecha_entrega'             => Carbon::createFromFormat('d/m/Y',$request->fecha_entrega)->format('Y-m-d'),
                'usuario_id'                => auth()->user()->id
            ];
            if($request->has('recibe_comite_id'))
                $datos['recibe_comite_id'] = $request->recibe_comite_id;
            else{
                $datos['observaciones'] = "$request->nombre $request->primer_apellido $request->segundo_apellido - $request->cargo";
            }
            $e = EntregaProveedor::find($ent_id);
            $e->recibe_comite_id = null;
            $e->observaciones = null;
            $e->save();
            $e->update($datos);
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_entregasproveedor',
                'registro' => $e->id . '',
                'campos' => json_encode($datos) . '',
                'metodo' => 'PUT'
            ]);
            //se actualiza la requisicion programacion
            $estado= EstadoPrograma::with('estado')->where('programa_id',$req->programa_id)->wherehas('estado',function($q) use($request){
                $q->where('nombre',($request->has('entregado') ? 'RECIBIDA' : 'NO RECIBIDA'));
            })->first();
            $r = RequisicionDatoCarga::find($request->requisicion_datocarga_id)->programacionrequisicion;
            $r->update([
                'estado_programa_id' => $estado->id
            ]);
            auth()->user()->bitacora(request(), [
                'tabla' => 'alim_programacion_requisiciones',
                'registro' => $r->id . '',
                'campos' => "{estado_programa_id:$estado->id}",
                'metodo' => "PUT"
            ]);
            DB::commit();
            return new JsonResponse(['message'=>'Se actualizado con exito']);
        }catch(Exception $e){
            DB::rollback();
            return new JsonResponse(['message'=>'error:'.$e->getMessage()],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
