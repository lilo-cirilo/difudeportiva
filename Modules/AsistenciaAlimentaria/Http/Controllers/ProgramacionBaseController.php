<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use App\Models\Estado;
use App\Models\Empleado;
use App\Models\Programa;

use App\Models\Ejercicio;
use Illuminate\Http\Request;
use App\Models\AniosPrograma;
use Illuminate\Http\Response;
use App\Models\EstadoPrograma;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\DataTables\Cocinas;

use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Beneficiario;
use Modules\AsistenciaAlimentaria\Entities\Programacion;

use Modules\AsistenciaAlimentaria\Entities\Financiamiento;

use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;
use Modules\AsistenciaAlimentaria\DataTables\Programaciones;
use Modules\AsistenciaAlimentaria\Entities\MotivosCancelacion;
use Modules\AsistenciaAlimentaria\Entities\RequisicionProgramacion;
use Modules\AsistenciaAlimentaria\DataTables\PlaneacionAlimentarios;

class ProgramacionBaseController extends Controller
{
  use AlimentariosTrait;

  private $programa;

  function __construct($programa = null)
  {
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
    if ($programa)
      $this->programa = Programa::find($programa);
  }


  public function index(Programaciones $tabla)
  {
    return $tabla->with('programa_id', $this->programa->id)->render('asistenciaalimentaria::programacion.index');
  } //lista de programaciones-ejercicios

  /* public function create(Cocinas $tabla){
    $anioAct = Programacion::join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
    ->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')->where('cat_ejercicios.anio',date('Y'))->count();

    if($anioAct > 0)
      return new JsonResponse(['message'=>'Programacion actual ya registrada'],409);

    return $tabla->with(['tipo'=>'refrendo','anio'=>date('Y'),'programa_id'=>config('asistenciaalimentaria.ccncId')])->render('asistenciaalimentaria::programacion.crear_editar',['anio'=>date('Y')]);
  }//refrendo de alimentarios-vista de seleccion
 */
  public function store(Request $request)
  {
    $this->acceso(['SUPERADMIN']);
    try {
      $ejercicio      = Ejercicio::FirstOrCreate(['anio' => config('asistenciaalimentaria.anioActual')]);
      $anioPrograma   = AniosPrograma::FirstOrCreate(['programa_id' => $this->programa->id, 'ejercicio_id' => $ejercicio->id]);
      $estado         = Estado::FirstOrCreate(['nombre' => 'ALTA']);
      $estadoPrograma = EstadoPrograma::FirstOrCreate(['estado_id' => $estado->id, 'programa_id' => $this->programa->id, 'orden' => 1], ['usuario_id' => auth()->user()->id]);
      $financiamiento = Financiamiento::where('nombre', 'like', '%' . config('asistenciaalimentaria.' . 'financiamiento' . $this->programa->id) . '%')->first();
      $user = auth()->user()->id;
      switch ($request->input('alimentario_id')) {
        case 'All':
          $aux = Alimentario::select(DB::raw("$anioPrograma->id as anios_programa_id, $estadoPrograma->id as estado_programa_id, $financiamiento->id as financiamiento_id, $user as usuario_id"), 'id as alimentario_id')->get()->toArray();
          Programacion::where('anios_programa_id', $anioPrograma->id)->forceDelete();
          Programacion::insert($aux);
          break;
        case 'Last':
          $ejercicioAnterior      = Ejercicio::Where('anio', config('asistenciaalimentaria.anioActual') - 1)->FirstOrFail();
          $anioProgramaAnterior   = AniosPrograma::Where([['programa_id', $this->programa->id], ['ejercicio_id', $ejercicioAnterior->id]])->FirstOrFail();
          $aux                    = Programacion::Where('anios_programa_id', $anioProgramaAnterior->id)
            ->select(DB::raw("$anioPrograma->id as anios_programa_id, $financiamiento->id as financiamiento_id, $user as usuario_id"), 'alimentario_id', 'estado_programa_id')->get()->toArray();
          Programacion::where('anios_programa_id', $anioPrograma->id)->forceDelete();
          Programacion::insert($aux);
          break;
        case 'Active':
          $ejercicioAnterior      = Ejercicio::Where('anio', config('asistenciaalimentaria.anioActual') - 1)->FirstOrFail();
          $anioProgramaAnterior   = AniosPrograma::Where([['programa_id', $this->programa->id], ['ejercicio_id', $ejercicioAnterior->id]])->FirstOrFail();
          $estadoActivo           = Estado::Where('nombre', 'like', 'ACTIV%')->firstOrFail();
          $estadoProgramaActivo   = EstadoPrograma::Where([['estado_id', $estadoActivo->id], ['programa_id', $this->programa->id]])->firstOrFail();
          $aux                    = Programacion::Where([['anios_programa_id', $anioProgramaAnterior->id], ['estado_programa_id', $estadoProgramaActivo->id]])
            ->select(DB::raw("$anioPrograma->id as anios_programa_id, $financiamiento->id as financiamiento_id, $user as usuario_id"), 'alimentario_id', 'estado_programa_id')->get()->toArray();
          Programacion::where('anios_programa_id', $anioPrograma->id)->forceDelete();
          Programacion::insert($aux);
          break;
        default:
          $alimentario    = Alimentario::FindOrFail($request->input('alimentario_id'));
          $registro       = Programacion::FirstOrCreate([
            'anios_programa_id'   => $anioPrograma->id,
            'estado_programa_id'  => $estadoPrograma->id,
            'financiamiento_id'   => $request->input('financiamiento_id'),
            'alimentario_id'      => $alimentario->id,
            'usuario_id'          => auth()->user()->id
          ]);
          return response()->json(200);
          break;
      }
      return response()->json(200);
    } catch (\Exception $e) {
      return new JsonResponse(['message' => $e->getMessage()], 500);
    }
  } //para seleccionar cocina-darle check

  public function edit($anio, Cocinas $tabla)
  {
    if ($anio < config('asistenciaalimentaria.anioActual'))
      return new JsonResponse(['message' => 'No se puede modificar esta programacion'], 403);
    else {
      return $tabla->with(['tipo' => 'refrendo', 'anio' => $anio, 'programa_id' => config('asistenciaalimentaria.ccncId')])->render('asistenciaalimentaria::programacion.crear_editar', ['anio' => $anio]);
    }
  } //para editar la seleccion una vez echa-reseleccionar cocinas

  public function destroy($id, Request $request)
  {
    switch ($request->input('alimentario_id')) {
      case 'All':
        # code...
        break;
      case 'Last':
        # code...
        break;
      case 'Active':
        # code...
        break;
      default:
        Programacion::findorfail($id)->forcedelete();
        return response()->json(true);
        break;
    }
  } //para deseleccionar cocinas-darle uncheck

  public function show($anio, PlaneacionAlimentarios $tabla)
  {
    $ejercicio = Ejercicio::where('anio', config('asistenciaalimentaria.anioActual'))->first();
    $ap = AniosPrograma::where([['programa_id', $this->programa->id], ['ejercicio_id', $ejercicio->id]])->first();
    return $tabla->with('anio_programa', $ap)->with('programa_id', $this->programa->id)->render('asistenciaalimentaria::programacion.show', ['programa' => $this->programa->nombre, 'anio' => $anio]);
  } //programacion actual

  public function tiposfinanciamiento(Request $request)
  {
    $financiamientos = Financiamiento::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
      ->take(10)
      ->get()
      ->toArray();

    return response()->json($financiamientos);
  }

  public function changeStatus($programacion, Request $request)
  {
    $this->acceso(['SUPERADMIN']);
    try {
      DB::beginTransaction();
      $p = Programacion::find($programacion);
      $p->estado_programa_id = $request->estado_programa_id;
      $p->save();
      if ($request->input('motivo')) {
        $data = $request->only(['motivo']);
        $data['programacion_id']  = $p->id;
        $data['usuario_id']       = auth()->user()->id;
        MotivosCancelacion::create($data);
      }
      auth()->user()->bitacora(request(), [
        'tabla' => 'alim_programacion',
        'registro' => $p->id . '',
        'campos' => "{estado_programa_id : $request->estado_programa_id}",
        'metodo' => request()->method()
      ]);

      DB::commit();
      return response()->json(array(200, 'success'));
    } catch (\Exception $e) {
      DB::rollBack();
      return new JsonResponse(['message' => 'Algo salió mal'], 500);
    }
  }

  public function queryBenef(Request $request)
  {
    try {
      DB::beginTransaction();
      $planeacion = Beneficiario::join('alim_programacion', 'alim_programacion.id', 'alim_beneficiarios.programacion_id')
        ->join('alim_alimentarios', 'alim_programacion.alimentario_id', 'alim_alimentarios.id')
        ->join('estados_programas', 'estados_programas.id', 'alim_programacion.estado_programa_id')
        ->join('cat_estados', 'cat_estados.id', 'estados_programas.estado_id')
        ->join('programas', 'programas.id', 'alim_alimentarios.programa_id')
        ->join('cat_municipios', 'cat_municipios.id', 'alim_alimentarios.municipio_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', 'alim_alimentarios.localidad_id')
        ->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', 'cat_distritos.region_id')
        ->join('alim_tiposaccion', 'alim_tiposaccion.id', 'alim_beneficiarios.tipoaccion_id')
        ->where('alim_alimentarios.programa_id', $this->programa->id)
        ->whereNull('alim_programacion.deleted_at');
      foreach ($request->input('filtros', []) as $filtro) {
        if ($filtro['name'] == 'alim_alimentarios.essedesol') {
          if (strtoupper($filtro['value']) == 'SI') {
            $planeacion->whereNotNull($filtro['name']);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $planeacion->whereNull($filtro['name']);
          }
        } else if ($filtro['name'] == 'alim_cat_financiamientos.nombre') {
          if (strtoupper($filtro['value']) == 'SI') {
            $planeacion->where($filtro['name'], 'PRODUCTO FINANCIERO');
          } else if (strtoupper($filtro['value']) == 'NO') {
            $planeacion->where($filtro['name'], '!=', 'PRODUCTO FINANCIERO');
          }
        } else if ($filtro['name'] == 'alim_programacion.esampliacion') {
          if (strtoupper($filtro['value']) == 'SI') {
            $planeacion->where($filtro['name'], 1);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $planeacion->where($filtro['name'], 0);
          }
        } else
          $planeacion->where($filtro['name'], 'LIKE', "%{$filtro['value']}%");
      }
      if ($this->programa->id == config('asistenciaalimentaria.sujetosId')) {
        $planeacion->leftJoin('alim_sujetos', 'alim_sujetos.alimentario_id', 'alim_alimentarios.id');
        //Si el usuario pertenece a otra área que no sea ASISTENCIA ALIMENTARIA
        if (!auth()->user()->hasRoles(['SUPERADMIN'])) {
          $area = Empleado::join('cat_areas', 'cat_areas.id', 'empleados.area_id')
            ->where('cat_areas.nombre', 'like', '%DESARROLLO FAMILIAR%')
            ->where('empleados.persona_id', auth()->user()->persona->id)
            ->first() ? true : false;
          if ($area) {
            $planeacion->where('alim_sujetos.dedesarrollo_programa_id', 1);
          } else {
            $planeacion->where('alim_sujetos.dedesarrollo_programa_id', '!=', 1);
          }
        }
      }
      if (auth()->user()->regionusuario)
        $planeacion->where('cat_regiones.id', auth()->user()->regionusuario->region_id);
      DB::commit();
      return view::make('asistenciaalimentaria::layouts.fragmentos.yield-aside')
        ->with([
          'acciones' => $planeacion->groupBy('alim_tiposaccion.nombre')->get([
            'alim_tiposaccion.nombre as accion',
            DB::raw('count(*) as beneficiarios')
          ])->toArray()
        ])
        ->render();
    } catch (\Exception $e) {
      DB::rollBack();
      return new JsonResponse(['message' => 'Algo salió mal ' . $e->getMessage()], 500);
    }
  }

  public function lock($programa_id, Request $request)
  {
    try {
      DB::beginTransaction();
      $query = Programacion::join('alim_alimentarios', 'alim_alimentarios.id', 'alim_programacion.alimentario_id')
        ->join('anios_programas', 'anios_programas.id', 'alim_programacion.anios_programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', 'anios_programas.ejercicio_id')
        ->join('cat_municipios', 'cat_municipios.id', 'alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades', 'cat_localidades.id', 'alim_alimentarios.localidad_id')
        ->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', 'cat_distritos.region_id')
        ->join('estados_programas', 'estados_programas.id', 'alim_programacion.estado_programa_id')
        ->join('cat_estados', 'cat_estados.id', 'estados_programas.estado_id')
        ->where('alim_alimentarios.programa_id', $programa_id)
        ->where('cat_estados.nombre', '=', 'ACTIVA')
        ->where('cat_ejercicios.anio', config('asistenciaalimentaria.anioActual'));

      if ($programa_id == config('asistenciaalimentaria.sujetosId'))
        $query->leftJoin('alim_sujetos', 'alim_sujetos.alimentario_id', 'alim_alimentarios.id');

      foreach ($request->input('filtros', []) as $filtro) {
        if ($filtro['name'] == 'alim_alimentarios.essedesol') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->where($filtro['name'], 1);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->where($filtro['name'], 0);
          }
        } else if ($filtro['name'] == 'alim_cat_financiamientos.nombre') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->where($filtro['name'], 'PRODUCTO FINANCIERO');
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->where($filtro['name'], '!=', 'PRODUCTO FINANCIERO');
          }
        } else if ($filtro['name'] == 'alim_programacion.esampliacion') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->where($filtro['name'], 1);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->where($filtro['name'], 0);
          }
        } else
          $query->where($filtro['name'], 'LIKE', "%{$filtro['value']}%");
      }

      $estado_programa_id = EstadoPrograma::where([
        ['programa_id', $programa_id],
        ['estado_id', Estado::where('nombre', 'ACTIVA - BLOQUEADA')->first()->id]
      ])->first()->id;

      $ids_pluck = $query->get(['alim_programacion.id as id'])->pluck('id');
      Programacion::whereIn('id', $ids_pluck)->update(['estado_programa_id' => $estado_programa_id]);
      $final = $ids_pluck[count($ids_pluck) - 1];
      auth()->user()->bitacora(request(), [
        'tabla' => 'alim_programacion',
        'registro' => '0',
        'campos' => "se bloquean $ids_pluck[0] ... $final",
        'comentario' => json_encode($ids_pluck),
        'metodo' => request()->method()
      ]);
      DB::commit();
      return new JsonResponse(['message' => 'Programaciones actualizadas ha ACTIVA - BLOQUEADA'], 200);
    } catch (Exception $e) {
      DB::rollback();
      return new JsonResponse(['message' => $e->getmessage()], 500);
    }
  }

  public function unlock($programa_id, Request $request)
  {
    try {
      DB::beginTransaction();
      $query = Programacion::join('alim_alimentarios', 'alim_alimentarios.id', 'alim_programacion.alimentario_id')
        ->join('anios_programas', 'anios_programas.id', 'alim_programacion.anios_programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', 'anios_programas.ejercicio_id')
        ->join('cat_municipios', 'cat_municipios.id', 'alim_alimentarios.municipio_id')
        ->leftJoin('cat_localidades', 'cat_localidades.id', 'alim_alimentarios.localidad_id')
        ->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', 'cat_distritos.region_id')
        ->join('estados_programas', 'estados_programas.id', 'alim_programacion.estado_programa_id')
        ->join('cat_estados', 'cat_estados.id', 'estados_programas.estado_id')
        ->where('alim_alimentarios.programa_id', $programa_id)
        ->where('cat_estados.nombre', '=', 'ACTIVA - BLOQUEADA')
        ->where('cat_ejercicios.anio', config('asistenciaalimentaria.anioActual'));

      if ($programa_id == config('asistenciaalimentaria.sujetosId'))
        $query->leftJoin('alim_sujetos', 'alim_sujetos.alimentario_id', 'alim_alimentarios.id');
      foreach ($request->input('filtros', []) as $filtro) {
        if ($filtro['name'] == 'alim_alimentarios.essedesol') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->whereNotNull($filtro['name']);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->whereNull($filtro['name']);
          }
        } else if ($filtro['name'] == 'alim_cat_financiamientos.nombre') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->where($filtro['name'], 'PRODUCTO FINANCIERO');
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->where($filtro['name'], '!=', 'PRODUCTO FINANCIERO');
          }
        } else if ($filtro['name'] == 'alim_programacion.esampliacion') {
          if (strtoupper($filtro['value']) == 'SI') {
            $query->where($filtro['name'], 1);
          } else if (strtoupper($filtro['value']) == 'NO') {
            $query->where($filtro['name'], 0);
          }
        } else
          $query->where($filtro['name'], 'LIKE', "%{$filtro['value']}%");
      }

      $estado_programa_id = EstadoPrograma::where([
        ['programa_id', $programa_id],
        ['estado_id', Estado::where('nombre', 'ACTIVA')->first()->id]
      ])->first()->id;

      $ids_pluck = $query->get(['alim_programacion.id as id'])->pluck('id');
      Programacion::whereIn('id', $ids_pluck)->update(['estado_programa_id' => $estado_programa_id]);
      $final = $ids_pluck[count($ids_pluck) - 1];
      auth()->user()->bitacora(request(), [
        'tabla' => 'alim_programacion',
        'registro' => '0',
        'campos' => "se desbloquean $ids_pluck[0] ... $final",
        'comentario' => json_encode($ids_pluck),
        'metodo' => request()->method()
      ]);
      DB::commit();
      return new JsonResponse(['message' => 'Programaciones actualizadas ha ACTIVA'], 200);
    } catch (Exception $e) {
      DB::rollback();
      return new JsonResponse(['message' => $e->getmessage()], 500);
    }
  }
}
