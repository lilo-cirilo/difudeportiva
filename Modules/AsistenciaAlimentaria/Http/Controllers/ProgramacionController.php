<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

use View;
use App\Models\EstadoPrograma;

class ProgramacionController extends Controller {
  function searchProgramas($programacion, Request $request) {
    $programa_padre = Programacion::findOrFail($programacion);
    $programa = TipoAccion::where('programa_id',$programa_padre->anioprograma->programa->id);
                    //->where('nombre','like','%'.$request['search'].'%')
    $edad = $request['edad'];
    $meses = $request['meses'];
    if($programa_padre->anioprograma->programa->id == config('asistenciaalimentaria.ccncId')) {
      if($edad == 0 && $meses > 0) {
        $programa->where('nombre','like','%6 MESES A 11 MESES%');
        if($meses >= 6)
          $programa->orWhere('nombre','like','%1 AÑO A 5 AÑOS 11 MESES%');
      } else if($edad == 5 || $edad == 6) {
        $programa->where('nombre','like','%1 AÑO A 5 AÑOS 11 MESES%')
                  ->orWhere('nombre','like','%6 A 12 AÑOS%');
      } else if($edad <= 5 && $edad >= 1) {
        $programa->where('nombre','like','%1 AÑO A 5 AÑOS 11 MESES%');
        if($edad == 1)
          $programa->orWhere('nombre','like','%6 MESES A 11 MESES%');
      } else if($edad <= 12 && $edad >= 6) {
        $programa->where('nombre','like','%6 A 12 AÑOS%');
        if($edad == 6)
          $programa->orWhere('nombre','like','%1 AÑO A 5 AÑOS 11 MESES%');
      } else if($edad > 12) {
        $programa->where('nombre','not like','%6 MESES A 11 MESES%')
                ->where('nombre','not like','%1 AÑO A 5 AÑOS 11 MESES%')
                ->where('nombre','not like','%6 A 12 AÑOS%');
      }
    } else if($programa_padre->anioprograma->programa->id == config('asistenciaalimentaria.sujetosId')) {
      if($edad < 60) {
        $programa->where('nombre','not like','%60%');
      }
      if($edad > 12)
        $programa->where('nombre','not like','%NIÑOS Y NIÑAS DE 1 A 12 AÑOS%');
      if ($edad >= 59 && $edad <= 66) {
        $programa->orWhere('nombre','like','%ADULTOS MAYORES(DE 60 A  65 AÑOS)%')
        ->orWhere('nombre','like','%ADULTOS MAYORES (60 EN ADELANTE)%');
      } else { 
        $programa->where('nombre','not like','%ADULTOS MAYORES(DE 60 A  65 AÑOS)%')
        ->where('nombre','not like','%ADULTOS MAYORES (60 EN ADELANTE)%');
      }
    }
    if(($edad < 9) || (strtoupper($request['genero']) == 'M')) {
      $programa->where('nombre','not like','%MUJER%')
                ->where('nombre','not like','%MADRE%');
    }
    return $programa->get(['id','nombre']);
  }

  public function estados(Request $request) {
    if($request->input('vista')) {
      return view::make('asistenciaalimentaria::modal')
                  ->with([
                      'vista' => 'asistenciaalimentaria::estados',
                      'lg' => true,
                      'programacion'=>Programacion::find($request->programacion_id),
                      'tipo'=>$request->get('id')
                  ])
                  ->render();
    }
    $estados = EstadoPrograma::join('cat_estados','cat_estados.id','estados_programas.estado_id')
                ->where('orden','>',0)
                ->where('programa_id',$request->programa_id)
                ->where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                ->get(['estados_programas.id as id', 'cat_estados.nombre'])
                ->toArray();
    return response()->json($estados);
  }
}