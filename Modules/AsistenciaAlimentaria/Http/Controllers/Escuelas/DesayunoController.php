<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Escuelas;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use App\Models\Programa;
use App\Models\Municipio;
use Modules\AsistenciaAlimentaria\Entities\Escuela;
use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Desayuno;
use Modules\AsistenciaAlimentaria\Entities\Programacion;

use Modules\AsistenciaAlimentaria\DataTables\Cocinas;

use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class DesayunoController extends Controller{
    use AlimentariosTrait;

    public function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
    }

    public function index(Cocinas $tabla) {
        return $tabla->with(['tipo'=>'lista','programa_id'=>config('asistenciaalimentaria.desayunosId')])->render('asistenciaalimentaria::alimentarios.index',['programa'=>Programa::find(config('asistenciaalimentaria.desayunosId'))]);
    }

    public function create(Request $request){
        $this->acceso(['SUPERADMIN']);
        return view('asistenciaalimentaria::alimentarios.create')
              ->with('programa',config('asistenciaalimentaria.desayunosId'))
              ->with('alimentario', $request->alim ? Alimentario::find($request->alim) : null);
    }

    public function store(Request $request){
        try {
            //validamos que no exista una cocina en esa (sub)localidad y municipio
            $conta = Alimentario::join('alim_desayunos','alim_desayunos.alimentario_id','alim_alimentarios.id')
            ->where([
                //['alim_alimentarios.programa_id',config('asistenciaalimentaria.desayunosId')],
                ['alim_desayunos.escuela_id',$request->input('escuela_id')]
            ])->get();
            if(count($conta)>0){
              $cocina = $conta[0];
              return new JsonResponse(['message'=>"escuela seleccionada ocupada en alimentario con folio $cocina->foliodif de ".$cocina->programa->nombre],409);
            }
            //Separamos los datos de la cocina y programacion
            $datosAlimentario = $request->except(['financiamiento_id','solicitud_id','escuela_id','padre_id']); 
            $datosAlimentario['essedesol'] = $request->input('essedesol') ? 1 : 0;
            $datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
            //Obtenemos el id de la region y generamos un folio en base al numero de cocinas en la region y el año de creacion a 2 digitos
            $prefolio = Municipio::find($datosAlimentario['municipio_id'])->distrito->region_id;
            $folio = $prefolio.(Alimentario::join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')->where('cat_distritos.region_id',$prefolio)->count() + 1).substr(date('Y'),-2);
            //Obtenemos al usuario logueado
            $datosAlimentario['usuario_id'] = auth()->user()->id;
            $datosAlimentario['foliodif'] = $folio;
            //empezamos a escribir en la bd
            DB::beginTransaction();
      
						$alimentario = Alimentario::Create($datosAlimentario);
						auth()->user()->bitacora(request(), [
							'tabla' => 'alim_alimentarios',
							'registro' => $alimentario->id . '',
							'campos' => json_encode($datosAlimentario) . '',
							'metodo' => request()->method(),
						]);
            //completamos los datos de la programacion
            $datosProgramacion['alimentario_id'] = $alimentario->id;
            $datosProgramacion['anios_programa_id'] = Programa::find($datosAlimentario['programa_id'])->aniosprogramas->where('ejercicio.anio',config('asistenciaalimentaria.anioActual'))->first()->id;
            $datosProgramacion['estado_programa_id'] = Programa::find($datosAlimentario['programa_id'])->estadoprogramas->where('estado.nombre','ALTA')->first()->id;
            $datosProgramacion['usuario_id'] = $datosAlimentario['usuario_id'];
            
						$prog = Programacion::create($datosProgramacion);
						auth()->user()->bitacora(request(), [
							'tabla' => 'alim_programacion',
							'registro' => $prog->id . '',
							'campos' => json_encode($datosProgramacion) . '',
							'metodo' => request()->method(),
						]);
            //guardamos los cambios en la bd
      
            //En caso de existir en el request escuela_id se añadará en alim_desayunos
						$escuela_p = Escuela::find($request->input('escuela_id'));
            if ($request->input('padre_id')) {
							$escuela_p->extension = 1;
							$escuela_p->padre_id = $request->input('padre_id');
							if(auth()->user()->id != 91) {//91 usuario maai
								auth()->user()->bitacora(request(), [
										'tabla' => 'cat_escuelas',
										'registro' => $escuela_p->id . '',
										'campos' => "{extension:1, padre_id: {$request->input('padre_id')}}",
										'metodo' => 'PUT',
										'comentario' => 'NO OFICIAL'
								]);
							}
						} else {
							$escuela_p->extension = 0;
							$escuela_p->padre_id = null;
						}
						$escuela_p->save();
						$datosDesayuno = $request->only(['escuela_id']);
						$datosDesayuno['alimentario_id'] = $alimentario->id;
						$datosDesayuno['tipodistribucion_id'] = 1;
						$datosDesayuno['usuario_id'] = $datosAlimentario['usuario_id'];
						$desayuno_n = Desayuno::Create($datosDesayuno);
						auth()->user()->bitacora(request(), [
								'tabla' => 'alim_desayunos',
								'registro' => $desayuno_n->id . '',
								'campos' => json_encode($datosDesayuno) . '',
								'metodo' => request()->method(),
						]);
            DB::commit();
            return response()->json(['success' => true, 'id' => $alimentario->id]);
        }catch(\Exeption $e) {
            DB::rollBack();
            return new JsonResponse([$e->getMessage()], 500);
        }
		}
		
		public function update($alimentario_id,Request $request){
			try {
				//validamos que no exista una escuela en alimentarios
				$alimentario = Alimentario::find($alimentario_id);
				//Separamos los datos de la cocina y programacion
				$datosAlimentario = $request->except(['financiamiento_id','solicitud_id','escuela_id','padre_id']); 
				$datosAlimentario['essedesol'] = $request->input('essedesol') ? 1 : 0;
				$datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
				DB::beginTransaction();
				
				$datosAlimentario['sublocalidad'] = $datosAlimentario['sublocalidad'] ?? null;
				$alimentario->update($datosAlimentario);
				auth()->user()->bitacora(request(), [
					'tabla' => 'alim_alimentario',
					'registro' => $alimentario->id . '',
					'campos' => json_encode($datosAlimentario) . '',
					'metodo' => request()->method(),
				]);
				$datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
				$prog = Programacion::where('alimentario_id',$alimentario_id)->first();
				$prog->update($datosProgramacion);
				auth()->user()->bitacora(request(), [
					'tabla' => 'alim_programacion',
					'registro' => $prog->id . '',
					'campos' => json_encode($datosProgramacion) . '',
					'metodo' => request()->method(),
				]);
				//En caso de existir en el request escuela_id se añadará en alim_desayunos
				$escuela_p = Escuela::find($request->input('escuela_id'));
				if ($request->input('padre_id')) {
					$escuela_p->extension = 1;
					$escuela_p->padre_id = $request->input('padre_id');
					if(auth()->user()->id != 91) {//91 usuario maai
						auth()->user()->bitacora(request(), [
							'tabla' => 'cat_escuelas',
							'registro' => $escuela_p->id . '',
							'campos' => "{extension:1, padre_id: {$request->input('padre_id')}}",
							'metodo' => 'PUT',
							'comentario' => 'NO OFICIAL'
						]);
					}
				} else {
					$escuela_p->extension = 0;
					$escuela_p->padre_id = null;
				}
				$escuela_p->save();
				$datosDesayuno = $request->only(['escuela_id']);
				$des = Desayuno::where('alimentario_id',$alimentario_id)->first();
				$des->update($datosDesayuno);
				auth()->user()->bitacora(request(), [
					'tabla' => 'alim_desayunos',
					'registro' => $des->id . '',
					'campos' => json_encode($datosDesayuno) . '',
					'metodo' => request()->method(),
				]);
				DB::commit();
				return response()->json(['success' => true, 'id' => $alimentario->id]);
			}catch(\Exeption $e) {
				DB::rollBack();
				return new JsonResponse(['message'=>'Algo no salio bien'],500);
			}
		}

    public function show($id){
        //Sólo se hizo como prueba
        $alimentarios = Alimentario::findOrFail($id);
        return response()->json(array('success' => true, 'alimentarios' => $alimentarios));
    }

    public function escuelas(Request $request){
        if ($request->ajax()) {
						$escuelas = Escuela::join('cat_nivelesescuela', 'cat_nivelesescuela.id', 'cat_escuelas.nivel_id')
								->leftJoin('cat_subsistemasescuela','cat_subsistemasescuela.id','cat_escuelas.subsistema_id')
                ->where(function($q) use($request){
                    $q->where('cat_escuelas.nombre', 'LIKE', "%$request->search%")
                    ->orWhere('cat_escuelas.clave','like',"%$request->search%");
								})
								->orWhere(function($q) use($request){
									if($request->search) {
										$exp = explode(" ", $request->search);
										$q->where('cat_subsistemasescuela.nombre', 'LIKE', "%$exp[0]%");
										if(isset($exp[1]))
												$q->where('cat_escuelas.nombre', 'LIKE', "%$exp[1]%");
									}
								})
                //->where('municipio_id', $request->input('municipio_id'))
                //->where('localidad_id', $request->input('localidad_id'))
                ->select(['cat_escuelas.id', DB::raw('concat_ws(" - ",cat_subsistemasescuela.nombre,cat_escuelas.nombre,cat_nivelesescuela.nombre,cat_escuelas.clave) as nombre')])
                ->paginate(10);

            return response()->json($escuelas);
        }
    }

    public function tiposDistribucion(Request $request){
        if ($request->ajax()) {
            $tiposdistribucion = TipoDistribucion::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                ->take(10)
                ->get()
                ->toArray();
            return response()->json($tiposdistribucion);
        }
    }
}
