<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AsistenciaAlimentaria\DataTables\ValidacionesRequisiciones;
use App\Models\Programa;
use Modules\AsistenciaAlimentaria\Entities\OficioValidacion;

class ValidacionRequisicionController extends Controller
{
    function __construct(){
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,VALIDADOR');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($oficio,ValidacionesRequisiciones $tabla)
    {
        $oficiov = OficioValidacion::find($oficio);
        return $tabla->with(['programa_id'=>$oficiov->programa->id,'oficio_validacion_id'=>$oficiov->id])->render('asistenciaalimentaria::requisicion.index',['programa'=>$oficiov->programa]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('asistenciaalimentaria::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('asistenciaalimentaria::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
