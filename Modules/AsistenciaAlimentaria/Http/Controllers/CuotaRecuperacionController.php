<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Routing\Controller;

use App\Models\Programa;
use Modules\AsistenciaAlimentaria\DataTables\CuotasRecuperacion;

class CuotaRecuperacionController extends Controller {

  function index($programa_id, CuotasRecuperacion $tabla) {
    return $tabla->with(['programa_id'=>$programa_id])->render('asistenciaalimentaria::requisicion.list_cuotasrecuperacion',['programa' => Programa::find($programa_id)]);
  }

}