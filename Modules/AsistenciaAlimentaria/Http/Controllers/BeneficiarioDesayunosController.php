<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\AsistenciaAlimentaria\Http\Controllers\BeneficiarioBaseController;

class BeneficiarioDesayunosController extends BeneficiarioBaseController
{
    function __construct(){
        parent::__construct(config('desayunosId'));
    }
}
