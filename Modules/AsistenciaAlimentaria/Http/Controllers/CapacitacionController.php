<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CapacitacionController extends Controller {
    public function index() {
        return view('asistenciaalimentaria::requisiciones.index');
    }
}