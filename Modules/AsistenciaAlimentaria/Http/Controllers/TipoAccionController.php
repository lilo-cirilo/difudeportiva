<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;

class TipoAccionController extends Controller{
    public function getTiposAccion($programa, Request $request) {
        return TipoAccion::where('programa_id',$programa)
                        ->where('nombre','like',"%{$request->input('search')}%")
                        ->get()
                        ->toArray();
    }
}