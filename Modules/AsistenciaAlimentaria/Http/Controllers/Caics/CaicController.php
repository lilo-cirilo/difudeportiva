<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Caics;

use App\Models\Programa;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\EstadoPrograma;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Desayuno;

use Modules\AsistenciaAlimentaria\DataTables\Cocinas;

use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class CaicController extends Controller{    
  use AlimentariosTrait;
  public function __construct()
  {
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
  }

  public function index(Cocinas $tabla) {
    return $tabla->with(['tipo'=>'lista','programa_id'=>config('asistenciaalimentaria.lecheId')])->render('asistenciaalimentaria::alimentarios.index',['programa'=>Programa::find(config('asistenciaalimentaria.lecheId'))]);
  }
  
  public function create(Request $request) {
    $this->acceso(['SUPERADMIN']);
    return view('asistenciaalimentaria::alimentarios.create')
          ->with('programa',config('asistenciaalimentaria.lecheId'))
          ->with('alimentario', $request->alim ? Alimentario::find($request->alim) : null);
  }
  
  public function store(Request $request){
    try {
      //validamos que no exista una cocina en esa (sub)localidad y municipio
      $conta = Alimentario::where([['municipio_id',$request->input('municipio_id')],['localidad_id',$request->input('localidad_id')],['programa_id',config('asistenciaalimentaria.lecheId')]])
      ->where(function($query) use ($request){
        if($request->input('sublocalidad'))
          $query->where('sublocalidad',$request->input('sublocalidad'));
        else
          $query->whereNull('sublocalidad');
        })->get();
      if(count($conta)>0){
        $cocina = $conta[0];
        new JsonResponse(['message'=>"la cocina $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de ".$cocina->programa->nombre],500);
      }
      //Separamos los datos de la cocina y programacion
      $datosAlimentario = $request->except(['financiamiento_id','solicitud_id','escuela_id']); 
      $datosAlimentario['essedesol'] = $request->input('essedesol') ? 1 : 0;
      $datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
      //Obtenemos el id de la region y generamos un folio en base al numero de cocinas en la region
      $prefolio = Municipio::find($datosAlimentario['municipio_id'])->distrito->region_id;
      $folio = $prefolio.'/'.(Alimentario::join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')->join('cat_distritos','cat_distritos.id','cat_municipios.distrito_id')->where('cat_distritos.region_id',$prefolio)->count() + 1);
      //Obtenemos al usuario logueado
      $datosAlimentario['usuario_id'] = auth()->user()->id;
      $datosAlimentario['foliodif'] = $folio;
      //empezamos a escribir en la bd
      DB::beginTransaction();

      $alimentario = Alimentario::Create($datosAlimentario);
      //completamos los datos de la programacion
      $datosProgramacion['alimentario_id'] = $alimentario->id;
      $datosProgramacion['anios_programa_id'] = Programa::find($datosAlimentario['programa_id'])->aniosprogramas->where('ejercicio.anio',config('asistenciaalimentaria.anioActual'))->first()->id;
      $datosProgramacion['estado_programa_id'] = Programa::find($datosAlimentario['programa_id'])->estadoprogramas->where('estado.nombre','ALTA')->first()->id;
      $datosProgramacion['usuario_id'] = $datosAlimentario['usuario_id'];
      
      Programacion::create($datosProgramacion);
      //guardamos los cambios en la bd

      //En caso de existir en el request escuela_id se añadará en alim_desayunos
      if ($request->input('escuela_id')) {
        $datosDesayuno = $request->only(['escuela_id']);
        $datosDesayuno['alimentario_id'] = $alimentario->id;
        $datosDesayuno['tipodistribucion_id'] = 1;
        $datosDesayuno['usuario_id'] = $datosAlimentario['usuario_id'];
        Desayuno::Create($datosDesayuno);
      }
      DB::commit();
      return response()->json(['success' => true, 'folio' => $alimentario->id]);
    }catch(\Exeption $e) {
      DB::rollBack();
      new JsonResponse(['message'=>'Algo no salio bien'],403);
    }
  }
  
  public function show($id){
    //Sólo se hizo como prueba
    $alimentarios = Alimentario::findOrFail($id);
    return response()->json(array('success' => true, 'alimentarios' => $alimentarios));
  }
  
  public function edit(){
    
  }
  
  public function update($alimentario_id,Request $request){
    try {
      //validamos que no exista una cocina en esa (sub)localidad y municipio
      $conta = Alimentario::where([['municipio_id',$request->input('municipio_id')],['localidad_id',$request->input('localidad_id')],['programa_id',config('asistenciaalimentaria.lecheId')]])
                          ->where('id','!=',$alimentario_id)
      ->where(function($query) use ($request){
        if($request->input('sublocalidad'))
          $query->where('sublocalidad',$request->input('sublocalidad'));
        else
          $query->whereNull('sublocalidad');
        })->get();
      if(count($conta)>0){
        $cocina = $conta[0];
        new JsonResponse(['message'=>"la cocina $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de ".$cocina->programa->nombre],500);
      }
      //Separamos los datos de la cocina y programacion
      $datosAlimentario = $request->except(['financiamiento_id','solicitud_id','escuela_id']);
      $datosAlimentario['essedesol'] = $request->input('essedesol') ? 1 : 0;
      $datosProgramacion = $request->only(['financiamiento_id','solicitud_id']);
      //empezamos a escribir en la bd
      DB::beginTransaction();
      Programacion::where('alimentario_id',$alimentario_id)->first()->update($datosProgramacion);
      //guardamos los cambios en la bd
      $alimentario = Alimentario::find($alimentario_id);
      $alimentario->update($datosAlimentario);
      //En caso de existir en el request escuela_id se añadará en alim_desayunos
      if ($request->input('escuela_id')) {
        $desayuno = Desayuno::where('alimentario_id',$alimentario_id)->first();
        $datosDesayuno = $request->only(['escuela_id']);
        if (!$desayuno) {
          $datosDesayuno['alimentario_id'] = $alimentario->id;
          $datosDesayuno['tipodistribucion_id'] = 1;
          $datosDesayuno['usuario_id'] = auth()->user()->id;
          Desayuno::Create($datosDesayuno);
        } else {
          $desayuno->update($datosDesayuno);
        }
      }
      DB::commit();
      return response()->json(['success' => true]);
    }catch(\Exeption $e) {
      DB::rollBack();
      new JsonResponse(['message'=>'Algo no salio bien'],403);
    }
  }
}
