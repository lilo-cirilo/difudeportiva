<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Licitacion;

use View;
use App\Models\Ejercicio;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Licitacion;
use Modules\AsistenciaAlimentaria\DataTables\Licitacion\Dotaciones;
use Modules\AsistenciaAlimentaria\Entities\LicitacionCantidadDotacion;
use Modules\AsistenciaAlimentaria\DataTables\Licitacion\CantidadesDotacionesLicitacion;

class CantidadDotacionLicitacionController extends Controller
{
    function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,SUPERADMIN');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($lic_id, CantidadesDotacionesLicitacion $tabla, Request $request)
    {
      return $tabla
      ->with(['licitacion_id'=>$lic_id])
      ->render('asistenciaalimentaria::modal',
                [
                  'vista'=>'asistenciaalimentaria::licitaciones.fragmentos.cantidad_dotacion',
                  'tipo'=>$request->get('id'),
                  'licitacion'=>Licitacion::find($lic_id)
                ]
              );
    }

    /**
     * Show the form for creating a new resource.
     * Funciona tanto para la ista de creación de una licitación así como de sus cantidades dotaciones.
     * @return Response
     */
    public function create(Request $request, Dotaciones $tabla)
    {
      return $tabla->with('licitacion_id')->render('asistenciaalimentaria::modal',[
        'vista' => 'asistenciaalimentaria.licitacion.fragmentos.cantidad_dotacion',
        'tipo'  => $request->input('id'),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($lic_id, Request $request)
    { 
      try {
        DB::beginTransaction();
          $data = $request->all();
          $data['usuario_id'] = auth()->user()->id;
          $lic = LicitacionCantidadDotacion::withTrashed()->where('licitacion_id',$lic_id)->where('dotacion_id',$data['dotacion_id'])->first();
          if($lic) {
            $lic->cantidad = $data['cantidad'];
            $lic->save();
            $lic->restore();
          } else {
            $data['licitacion_id'] = $lic_id;
            LicitacionCantidadDotacion::create($data);
          }
        DB::commit();
        return new JsonResponse(['success'=>true],200);
      } catch(\Exception $e) {
        DB::rollBack();
        return new JsonResponse(['message'=>$e->getMessage()],500);
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($lic_id, $id)
    {
      try {
        DB::beginTransaction();
          LicitacionCantidadDotacion::destroy($id);
        DB::commit();
        return response()->json(array(200,'success'));
      } catch (\Exception $th) {
        DB::rollBack();
        return new JsonResponse(['message'=>'Algo salió mal '.$th->getMessage(),500]);
      }
    }
}
