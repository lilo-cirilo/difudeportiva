<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Licitacion;

use View;
use App\Models\Ejercicio;
use Illuminate\Http\Request;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Licitacion;
use Modules\AsistenciaAlimentaria\DataTables\Licitacion\Licitaciones;

class LicitacionController extends Controller
{
    function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('rolModuleV2:alimentarios,SUPERADMIN');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Licitaciones $table)
    {
        return $table->render('asistenciaalimentaria::licitaciones.index');
    }

    /**
     * Show the form for creating a new resource.
     * Funciona tanto para la ista de creación de una licitación así como de sus cantidades dotaciones.
     * @return Response
     */
    public function create(Request $request)
    {
        return view::make('asistenciaalimentaria::modal')
                    ->with([
                        'vista'      =>  'asistenciaalimentaria::licitaciones.create',
                        'tipo'       =>  $request->get('id'),
                        'lg'         =>  $request->get('id') == 'new' ? true : false
                    ])
                    ->render();
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
                $data = $request->all();
                $data['usuario_id'] = auth()->user()->id;
                $data['ejercicio_id'] = Ejercicio::where('anio',config('asistenciaalimentaria.anioActual'))->first()->id;
                Licitacion::firstOrCreate(array('ejercicio_id'=>$data['ejercicio_id']),$data);
            DB::commit();
            return response()->json(array(200,'success'));
        } catch(\Exception $e) {
            DB::rollBack();
            return new JsonResponse(['message'=>'Algo salió mal '.$e->getMessage()],500);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('asistenciaalimentaria::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit(Request $request)
    {
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function licitaciones(Request $request) {
        $entregas = Licitacion::join('modulos_proveedores','modulos_proveedores.id','alim_licitaciones.modulo_proveedor_id')
        ->join('recm_cat_proveedors','recm_cat_proveedors.id','modulos_proveedores.proveedor_id')
        ->join('cat_ejercicios','cat_ejercicios.id','alim_licitaciones.ejercicio_id')
        ->select(['alim_licitaciones.id',DB::raw('concat_ws(" - ",num_licitacion,cat_ejercicios.anio,recm_cat_proveedors.nombre) as nombre')])
        ->get()->toArray();

        return response()->json($entregas);
    }
}
