<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Ccnc;

use App\Models\Programa;
use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\EstadoPrograma;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\Desayuno;

use Modules\AsistenciaAlimentaria\DataTables\Cocinas;

use Modules\AsistenciaAlimentaria\Entities\Alimentario;
use Modules\AsistenciaAlimentaria\Entities\Escuela;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class CocinaController extends Controller
{
	use AlimentariosTrait;
	public function __construct()
	{
		$this->middleware(['auth', 'authorized']);
		$this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
	}

	public function index(Cocinas $tabla)
	{
		return $tabla->with(['tipo' => 'lista', 'programa_id' => config('asistenciaalimentaria.ccncId')])->render('asistenciaalimentaria::alimentarios.index', ['programa' => Programa::find(config('asistenciaalimentaria.ccncId'))]);
	}

	public function create(Request $request)
	{
		$this->acceso(['SUPERADMIN']);
		return view('asistenciaalimentaria::alimentarios.create')
			->with('programa', config('asistenciaalimentaria.ccncId'))
			->with('alimentario', $request->alim ? Alimentario::find($request->alim) : null);
	}

	public function store(Request $request){
		try {
			//validamos que no exista una cocina en esa (sub)localidad y municipio
			$conta = Alimentario::where([['municipio_id', $request->input('municipio_id')], ['localidad_id', $request->input('localidad_id')], ['programa_id', config('asistenciaalimentaria.ccncId')]])
				->where(function ($query) use ($request) {
					if ($request->input('sublocalidad'))
						$query->where('sublocalidad', $request->input('sublocalidad'));
					else
						$query->whereNull('sublocalidad');
				})->get();
			if (count($conta) > 0) {
				$cocina = $conta[0];
				return new JsonResponse(['message' => "la cocina $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de " . $cocina->programa->nombre], 409);
			}
			//Separamos los datos de la cocina y programacion
			$datosAlimentario = $request->except(['financiamiento_id', 'solicitud_id', 'escuela_id', 'padre_id']);
			$datosAlimentario['essedesol'] = $request->input('essedesol','off')=='on' ? 1 : 0;
			$datosProgramacion = $request->only(['financiamiento_id', 'solicitud_id']);
			//Obtenemos el id de la region y generamos un folio en base al numero de cocinas en la region
			$prefolio = Municipio::find($datosAlimentario['municipio_id'])->distrito->region_id;
			$folio = $prefolio . (Alimentario::join('cat_municipios', 'cat_municipios.id', 'alim_alimentarios.municipio_id')->join('cat_distritos', 'cat_distritos.id', 'cat_municipios.distrito_id')->where('cat_distritos.region_id', $prefolio)->count() + 1) . substr(date('Y'), -2);
			//Obtenemos al usuario logueado
			$datosAlimentario['usuario_id'] = auth()->user()->id;
			$datosAlimentario['foliodif'] = $folio;
			//empezamos a escribir en la bd
			DB::beginTransaction();

			$alimentario = Alimentario::Create($datosAlimentario);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_alimentarios',
				'registro' => $alimentario->id . '',
				'campos' => json_encode($datosAlimentario) . '',
				'metodo' => request()->method(),
			]);
			//completamos los datos de la programacion
			$datosProgramacion['alimentario_id'] = $alimentario->id;
			$datosProgramacion['anios_programa_id'] = Programa::find($datosAlimentario['programa_id'])->aniosprogramas->where('ejercicio.anio', config('asistenciaalimentaria.anioActual'))->first()->id;
			$datosProgramacion['estado_programa_id'] = Programa::find($datosAlimentario['programa_id'])->estadoprogramas->where('estado.nombre', 'ALTA')->first()->id;
			$datosProgramacion['esampliacion'] = $request->input('esampliacion', 'off') == 'on' ? 1 : 0;
			$datosProgramacion['usuario_id'] = $datosAlimentario['usuario_id'];

			$prog = Programacion::create($datosProgramacion);
			auth()->user()->bitacora(request(), [
				'tabla' => 'alim_programacion',
				'registro' => $prog->id . '',
				'campos' => json_encode($datosProgramacion) . '',
				'metodo' => request()->method(),
			]);
			//guardamos los cambios en la bd

			//En caso de existir en el request escuela_id se añadará en alim_desayunos
			if ($request->input('escuela_id')) {
				$conta = Alimentario::join('alim_desayunos', 'alim_desayunos.alimentario_id', 'alim_alimentarios.id')
					->where([
						['alim_desayunos.escuela_id', $request->input('escuela_id')]
					])->get();
				if (count($conta) > 0) {
					$cocina = $conta[0];
					return new JsonResponse(['message' => "escuela existente en alimentario con folio $cocina->foliodif"], 409);
				}
				$escuela_p = Escuela::find($request->input('escuela_id'));
				if ($request->input('padre_id')) {
					$escuela_p->extension = 1;
					$escuela_p->padre_id = $request->input('padre_id');
					if (auth()->user()->id != 91) { //91 usuario maai
						auth()->user()->bitacora(request(), [
							'tabla' => 'cat_escuelas',
							'registro' => $escuela_p->id . '',
							'campos' => "{extension:1, padre_id: {$request->input('padre_id')}}",
							'metodo' => 'PUT',
							'comentario' => 'NO OFICIAL'
						]);
					}
				} else {
					$escuela_p->extension = 0;
					$escuela_p->padre_id = null;
				}
				$escuela_p->save();
				$datosDesayuno = $request->only(['escuela_id']);
				$datosDesayuno['alimentario_id'] = $alimentario->id;
				$datosDesayuno['tipodistribucion_id'] = 1;
				$datosDesayuno['usuario_id'] = $datosAlimentario['usuario_id'];
				$desayuno_n = Desayuno::Create($datosDesayuno);
				auth()->user()->bitacora(request(), [
					'tabla' => 'alim_desayunos',
					'registro' => $desayuno_n->id . '',
					'campos' => json_encode($datosDesayuno) . '',
					'metodo' => request()->method(),
				]);
			}
			DB::commit();
			return response()->json(['success' => true, 'folio' => $alimentario->id]);
		} catch (\Exeption $e) {
			DB::rollBack();
			return new JsonResponse(['message' => 'Algo no salio bien'], 500);
		}
	}

	public function show($id)
	{
		//Sólo se hizo como prueba
		$alimentarios = Alimentario::findOrFail($id);
		return response()->json(array('success' => true, 'alimentarios' => $alimentarios));
	}

	public function edit()
	{ }

	public function update($alimentario_id, Request $request)
	{
		try {
			//validamos que no exista una cocina en esa (sub)localidad y municipio
			$conta = Alimentario::where([['municipio_id', $request->input('municipio_id')], ['localidad_id', $request->input('localidad_id')], ['programa_id', config('asistenciaalimentaria.ccncId')]])
				->where('id', '!=', $alimentario_id)
				->where(function ($query) use ($request) {
					if ($request->input('sublocalidad'))
						$query->where('sublocalidad', $request->input('sublocalidad'));
					else
						$query->whereNull('sublocalidad');
				})->get();
			if (count($conta) > 0) {
				$cocina = $conta[0];
				return new JsonResponse(['message' => "la cocina $cocina->foliodif se encuentra en este mismo, municipio, localidad, sublocalidad de " . $cocina->programa->nombre], 409);
			}
			//Separamos los datos de la cocina y programacion
			$datosAlimentario = $request->except(['financiamiento_id', 'solicitud_id', 'escuela_id', 'padre_id']);
			$datosAlimentario['essedesol'] = $request->input('essedesol','off')=='on' ? 1 : 0;
			$datosAlimentario['sublocalidad'] = $datosAlimentario['sublocalidad'] ?? null;
			$datosProgramacion = $request->only(['financiamiento_id', 'solicitud_id']);
			$datosProgramacion['esampliacion'] = $request->input('esampliacion', 'off') == 'on' ? 1 : 0;
			//empezamos a escribir en la bd
			DB::beginTransaction();
				Programacion::where('alimentario_id', $alimentario_id)->first()->update($datosProgramacion);
				//guardamos los cambios en la bd
				$alimentario = Alimentario::find($alimentario_id);
				$alimentario->update($datosAlimentario);
				auth()->user()->bitacora(request(), [
					'tabla' => 'alim_alimentario',
					'registro' => $alimentario_id . '',
					'campos' => json_encode($datosAlimentario) . '',
					'metodo' => request()->method(),
				]);
				//En caso de existir en el request escuela_id se añadará en alim_desayunos
				if ($request->input('escuela_id')) {
					$desayuno = Desayuno::where('alimentario_id', $alimentario_id)->first();
					$conta = Alimentario::join('alim_desayunos', 'alim_desayunos.alimentario_id', 'alim_alimentarios.id')
						->where([
							['alim_desayunos.escuela_id', $request->input('escuela_id')],
							['alim_alimentarios.id', '!=', $desayuno->alimentario_id]
						])->get();
					if (count($conta) > 0) {
						$cocina = $conta[0];
						return new JsonResponse(['message' => "escuela existente en alimentario con folio $cocina->foliodif"], 409);
					}
					$escuela_p = Escuela::find($request->input('escuela_id'));
					if ($request->input('padre_id')) {
						$escuela_p->extension = 1;
						$escuela_p->padre_id = $request->input('padre_id');
						if (auth()->user()->id != 91) { //91 usuario maai
							auth()->user()->bitacora(request(), [
								'tabla' => 'cat_escuelas',
								'registro' => $escuela_p->id . '',
								'campos' => "{extension:1, padre_id: {$request->input('padre_id')}}",
								'metodo' => 'PUT',
								'comentario' => 'NO OFICIAL'
							]);
						}
					} else {
						$escuela_p->extension = 0;
						$escuela_p->padre_id = null;
					}
					$escuela_p->save();
					$datosDesayuno = $request->only(['escuela_id']);
					if (!$desayuno) {
						$datosDesayuno['alimentario_id'] = $alimentario->id;
						$datosDesayuno['tipodistribucion_id'] = 1;
						$datosDesayuno['usuario_id'] = auth()->user()->id;
						$des = Desayuno::Create($datosDesayuno);
						auth()->user()->bitacora(request(), [
							'tabla' => 'alim_desayunos',
							'registro' => $des->id . '',
							'campos' => json_encode($datosDesayuno) . '',
							'metodo' => 'POST',
						]);
					} else {
						$desayuno->update($datosDesayuno);
						auth()->user()->bitacora(request(), [
							'tabla' => 'alim_desayunos',
							'registro' => $desayuno->id . '',
							'campos' => json_encode($datosDesayuno) . '',
							'metodo' => request()->method(),
						]);
					}
				}
			DB::commit();
			return response()->json(['success' => true]);
		} catch (\Exeption $e) {
			DB::rollBack();
			return new JsonResponse(['message' => 'Algo no salio bien'], 500);
		}
	}
}
