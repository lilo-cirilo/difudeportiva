<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Modules\AsistenciaAlimentaria\Entities\Convenio;
use Modules\AsistenciaAlimentaria\Entities\TipoConvenio;

class ConvenioController extends Controller {

  function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,ADMINISTRATIVO');
  }
  
  public function create(Request $request,$programacion_id) {
    return view::make('asistenciaalimentaria::modal')->with(['tipo'=>$request->get('id'),'vista'=>'asistenciaalimentaria::convenios.fragmentos.crear_editar','programacion_id'=>$programacion_id])->render();
    /* return response()->json([
      'body' =>  view::make('asistenciaalimentaria::modal')->with(['tipo'=>$request->get('id'),'vista'=>'asistenciaalimentaria::convenios.fragmentos.crear_editar','programacion_id'=>$programacion_id])
      ->render()
    ], 200); */
  }
  public function show(Request $request,$programacion_id,$convenio_id) {
    return view::make('asistenciaalimentaria::modal')->with(['tipo'=>$request->get('id'),'vista'=>'asistenciaalimentaria::convenios.fragmentos.show','convenio'=>Convenio::findOrFail($convenio_id)])->render();
    /* return response()->json([
      'body' =>  view::make('asistenciaalimentaria::modal')->with(['tipo'=>$request->get('id'),'vista'=>'asistenciaalimentaria::convenios.fragmentos.show','convenio'=>Convenio::findOrFail($convenio_id)])
      ->render()
    ], 200); */
  }

  public function store($programacion_id, Request $request) {
    if($request->ajax()) {
      try {
        $datos = $request->except(['ubicacion_digital_archivo']);
        $datos['programacion_id']         = $programacion_id;
        $datos['usuario_id']              = auth()->User()->id;
        $datos['tiene_acta_aceptacion']   = $datos['tiene_acta_aceptacion'] == 'on' ? 1 : 0;
        $datos['tiene_acta_comite']       = $datos['tiene_acta_comite'] == 'on' ? 1 : 0;
        if($request->has('tiene_acta_contraloria'))
        $datos['tiene_acta_contraloria']  = $datos['tiene_acta_contraloria'] == 'on' ? 1 : 0;
        DB::beginTransaction();
          $datos['ubicacion_digital_archivo'] = null;
          if($request->has('ubicacion_digital_archivo')){
            $url = Storage::putFile('public/asistenciaAlimentaria/convenios', $request->file('ubicacion_digital_archivo'));
            $partes = explode('public', $url);
            $datos['ubicacion_digital_archivo'] = 'storage'.$partes[1];
          }
          $new=Convenio::create($datos);
          auth()->user()->bitacora(request(), [
              'tabla' => 'alim_convenios',
              'registro' => $new->id . '',
              'campos' => json_encode($datos) . '',
              'metodo' => request()->method()
          ]);
        DB::commit();
        return response()->json(['success' => true],200);
      } catch(Exception $e) {
        DB::rollBack();
        return new JsonResponse(['message'=>'Algo salió mal'],500);
      }
    }
  }

  public function tiposconvenio(Request $request) {
    if($request->ajax()){
      $convenios = TipoConvenio::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                  ->take(10)
                  ->get()
                  ->toArray();

      return response()->json($convenios);
    }
  }
}