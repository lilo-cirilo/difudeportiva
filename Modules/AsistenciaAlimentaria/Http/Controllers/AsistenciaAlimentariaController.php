<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use App\Models\Region;
use App\Models\Programa;
use App\Models\Ejercicio;

use App\Models\Municipio;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AsistenciaAlimentaria\DataTables\Usuarios;
use Modules\AsistenciaAlimentaria\Entities\Alimentario;

use Modules\AsistenciaAlimentaria\Entities\Financiamiento;
use Modules\AsistenciaAlimentaria\Entities\TipoDistribucion;
use Modules\AsistenciaAlimentaria\DataTables\Proveedor\Requisiciones;

class AsistenciaAlimentariaController extends Controller{
  /**
  * Create a new controller instance.
  * @return void
  */
  public function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN',['only' => ['index']]);
  }
  public function home(Requisiciones $tabla){
    if(auth()->user()->hasRoles(['PROVEEDOR']))
      return redirect()->route('alimentarios.proveedor.requisiciones.index',config('asistenciaalimentaria.anioActual'));

    $munXregion = [];
    $puntos = [];
    foreach (Region::All() as $region) {
      $aux = [];
      foreach ($region->distritos as $distrito) {
        foreach ($distrito->municipios as $mun) {
          $aux2 = [];
          $aux2[0] = $mun->id; 
          $aux2[1] = $mun->nombre; 
          $aux2[2] = $mun->latitud; 
          $aux2[3] = $mun->longitud;
          array_push($aux,$aux2);
        }
      }
      $munXregion[$region->nombre]=$aux;
    }
    $puntos['cocinas'] = Alimentario::where('programa_id',config('asistenciaalimentaria.ccncId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%');
            });
        });
    })->get(['foliodif','latitud','longitud']);
    $puntos['sujetos'] = Alimentario::where('programa_id',config('asistenciaalimentaria.sujetosId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%');
            });
        });
    })->get(['foliodif','latitud','longitud']);
    $puntos['escuelas'] = Alimentario::where('programa_id',config('asistenciaalimentaria.desayunosId'))->whereHas('programacion',function($q){
        $q->whereHas('estadoprograma',function($qu){
            $qu->whereHas('estado',function($que){
                $que->where('nombre','like','%activa%');
            });
        });
    })->get(['foliodif','latitud','longitud']);
    return view('asistenciaalimentaria::mapa',['regiones'=>json_encode($munXregion),'puntos'=>json_encode($puntos)]);
  }

  public function index(Usuarios $tabla){
    return $tabla->render(
      'asistenciaalimentaria::configuracion.config',
      [
        'anios'=>Ejercicio::OrderBy('anio','desc')->take(3)->get(),
        'programas'=>Programa::where('codigo','MOD ALIMENTARIO')->get(),
        'tipos_finan'=>Financiamiento::all()
      ]
    );
  }//vista de configuración

  public function update(Request $request){
    $munXregion = [];
    foreach (Region::All() as $region) {
      $aux = [];
      foreach ($region->distritos as $distrito) {
        foreach (array_pluck($distrito->municipios->toArray(),'id') as $mun) {
          array_push($aux,$mun);
        }
      }
      $munXregion[$region->nombre]=$aux;
    }
    return view('asistenciaalimentaria::index',['regiones'=>json_encode($munXregion)]);
  }//actualizar configuraciones

  public function show($anio){
    $main = Programa::where('nombre','ASISTENCIA ALIMENTARIA')->firstOrFail();
    return response()->json(Programa::join('anios_programas','anios_programas.programa_id','programas.id')->join('cat_ejercicios','cat_ejercicios.id','anios_programas.ejercicio_id')
    ->where([['padre_id',$main->id],['anio',$anio]])
    ->get());
  }//regresa la lista de programas en alimentarios según el año

  public function create(Request $request) { 
    return view('asistenciaalimentaria::alimentarios.create')
            ->with('financiamientos',Financiamiento::all())
            ->with('distribuciones',TipoDistribucion::all())
            ->with('prefix',$request->input('prefix',''));
  }
}
