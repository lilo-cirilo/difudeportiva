<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers\Sujetos;

use Modules\AsistenciaAlimentaria\Http\Controllers\RequisicionBaseController;

class RequisicionController extends RequisicionBaseController{
  
  public function __construct() {
    parent::__construct(config('asistenciaalimentaria.sujetosId'));
  }

}
