<?php

namespace Modules\AsistenciaAlimentaria\Http\Controllers;

use View;
use Carbon\Carbon;
use App\Models\Persona;
use Illuminate\Http\Request;
use App\Models\AniosPrograma;
use Illuminate\Http\Response;

use App\Models\PersonasPrograma;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AsistenciaAlimentaria\Entities\TipoAccion;
use Modules\AsistenciaAlimentaria\Entities\Beneficiario;
use Modules\AsistenciaAlimentaria\Entities\Programacion;
use Modules\AsistenciaAlimentaria\DataTables\Beneficiarios;
use Modules\DesayunosEscolaresRegistro\Entities\ExtraDatosPersona;
use Modules\AsistenciaAlimentaria\Traits\AlimentariosTrait;

class BeneficiarioController extends Controller{
  use AlimentariosTrait;

  public function __construct(){
    $this->middleware(['auth', 'authorized']);
    $this->middleware('rolModuleV2:alimentarios,SUPERADMIN,REGIONAL,OPERATIVO,CAPTURISTA,ADMINISTRATIVO');
  }
  /**
   * Display a listing of the resource.
   * @return Response
   * Retorna el dataTable de la programación actual
   */
  public function index($programacion_id,Beneficiarios $tabla,Request $request){
    return $tabla->with(['programacion'=>Programacion::findOrFail($programacion_id)])
                    ->render('asistenciaalimentaria::modal',
                              [
                                'vista'=>'asistenciaalimentaria::beneficiarios.index',
                                'programacion'=> Programacion::find($programacion_id),
                                'tipo'=>$request->get('id')
                              ]
                            );
  }

  public function create($programacion,Request $request){
    $valid = $this->puedeModificar($programacion);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    // $b = Beneficiario::find($request['alim']);
    return view::make('asistenciaalimentaria::modal')
                ->with([
                    'vista_beneficiario' => 'asistenciaalimentaria::beneficiarios.fragmentos.datos_beneficiario',
                    'vista' => 'asistenciaalimentaria::beneficiarios.fragmentos.create_edit',
                    'programacion'=>Programacion::find($programacion),
                    'beneficiario'=>null,
                    'persona' => null,
                    'tipo'=>$request->get('id')
                ])
                ->render();
  }
  /**
   * Store a newly created resource in storage.
   * @param  Request $request
   * @return Response
   * Agrega un nuevo de beneficiario
   */
  public function store($programacion,Request $request){
    $valid = $this->puedeModificar($programacion);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    try {
        DB::beginTransaction();
          $esta_programacion = Beneficiario::join('personas_programas','personas_programas.id','alim_beneficiarios.persona_programa_id')
                                            ->join('alim_programacion','alim_programacion.id','alim_beneficiarios.programacion_id')
                                            ->join('personas','personas.id','personas_programas.persona_id')
                                            ->where('alim_programacion.id',$programacion)
                                            ->where('curp',$request['curp'])
                                            ->first();
          $otra_programacion = Beneficiario::join('personas_programas','personas_programas.id','alim_beneficiarios.persona_programa_id')
                                            ->join('alim_programacion','alim_programacion.id','alim_beneficiarios.programacion_id')
                                            ->join('alim_alimentarios','alim_alimentarios.id','alim_programacion.alimentario_id')
                                            ->join('cat_municipios','cat_municipios.id','alim_alimentarios.municipio_id')
                                            ->join('anios_programas','anios_programas.id','alim_programacion.anios_programa_id')
                                            ->join('programas','programas.id','anios_programas.programa_id')
                                            ->join('personas','personas.id','personas_programas.persona_id')
                                            ->where('curp',$request['curp'])
                                            ->first(['foliodif','cat_municipios.nombre as municipio','programas.nombre as programa']);
          if($esta_programacion)
            return new JsonResponse(['message'=>'Beneficiario ya pertenece a esta programacion'],409);
          if($otra_programacion)
            return new JsonResponse(['message'=>'Beneficiario ya pertenece a la programacion con folio: '.$otra_programacion->foliodif.', municipio: '.$otra_programacion->municipio.' del programa: '.$otra_programacion->programa],410);
          //Se actualiza o crea una persona
          $request['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
					$data_persona = $request->except(['esta_embarazada_lactando','anio_programa_id']);
					if(!$request->segundo_apellido)
						$data_persona['segundo_apellido'] = '';
          $data_persona['usuario_id'] = auth()->user()->id;
          //validar persona
          $persona = Persona::updateOrCreate([
            'curp'=>$data_persona['curp']
          ],$data_persona);
          //
          //$persona = Persona::updateOrCreate(['curp'=>$request->curp],$data_persona);
          if($persona) {
            auth()->user()->bitacora(request(), [
                'tabla' => 'personas',
                'registro' => $persona->id . '',
                'campos' => json_encode($data_persona) . '',
                'metodo' => request()->method()
            ]);
          }
          $p = Programacion::findOrFail($programacion);
          $data_persona_programa = [];
          $data_persona_programa['persona_id'] = $persona->id;
          $data_persona_programa['usuario_id'] = auth()->user()->id;
          $data_persona_programa['anios_programa_id'] = $p->anioprograma->id;
          $persona_programa = PersonasPrograma::where('persona_id',$data_persona_programa['persona_id'])
                                              ->where('anios_programa_id',$data_persona_programa['anios_programa_id'])
                                              ->withTrashed()
                                              ->first();
          if($persona_programa) {
            $persona_programa->restore();
          } else {
            $persona_programa = PersonasPrograma::create($data_persona_programa);
          }
          //$persona_programa = PersonasPrograma::updateOrCreate(['persona_id'=>$persona->id,'anios_programa_id'=>$p->anioprograma->id],$data_persona_programa);
          auth()->user()->bitacora(request(), [
              'tabla' => 'personas_programas',
              'registro' => $persona_programa->id . '',
              'campos' => json_encode($data_persona_programa) . '',
              'metodo' => request()->method()
          ]);
          $data_beneficiario_alimentario = [];
          if($request['curp'][10] == 'M')
            $data_beneficiario_alimentario = $request->only(['esta_embarazada_lactando']);
          else
            $data_beneficiario_alimentario['esta_embarazada_lactando'] = 0;
          $data_beneficiario_alimentario['tipoaccion_id'] = $request->tipoaccion_id;
          $data_beneficiario_alimentario['programacion_id'] = $programacion;
          $data_beneficiario_alimentario['persona_programa_id'] = $persona_programa->id;
          $data_beneficiario_alimentario['usuario_id'] = auth()->user()->id;
          $beneficiario_alimentario = Beneficiario::where('persona_programa_id',$data_beneficiario_alimentario['persona_programa_id'])
                                              ->where('programacion_id',$data_beneficiario_alimentario['programacion_id'])
                                              ->withTrashed()
                                              ->first();
          if($beneficiario_alimentario) {
            $beneficiario_alimentario->restore();
            $beneficiario_alimentario->update($data_beneficiario_alimentario);
          } else {
            $beneficiario_alimentario = Beneficiario::create($data_beneficiario_alimentario);
          }
          //$beneficiario_alimentario = Beneficiario::updateOrCreate(['persona_programa_id'=>$persona_programa->id,'programacion_id'=>$programacion],$data_beneficiario_alimentario);
          auth()->user()->bitacora(request(), [
              'tabla' => 'alim_beneficiarios',
              'registro' => $beneficiario_alimentario->id . '',
              'campos' => json_encode($data_beneficiario_alimentario) . '',
              'metodo' => request()->method()
          ]);
          $subprograma = TipoAccion::find($request->tipoaccion_id)->nombre;
          $is_programa = Programacion::findOrFail($programacion)->anioprograma->programa->id;
          if($subprograma == 'NIÑOS Y NIÑAS DE 1 A 12 AÑOS' || $subprograma == 'DESAYUNOS ESCOLARES  (6 A 12 AÑOS)' || $subprograma == 'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (1 AÑO A 5 AÑOS 11 MESES)' || $is_programa == config('asistenciaalimentaria.desayunosId')) {
            if($request->peso != null && $request->talla != null && $request->fecha_peso != null && $request->fecha_talla != null && $request->tiporopa_id != null) {
              $dataextra_beneficiario_alimentario = $request->only(['peso','talla','fecha_talla','fecha_peso','grado','tiporopa_id']);
              $dataextra_beneficiario_alimentario['fecha_talla'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_alimentario['fecha_talla'])->format('Y-m-d');
              $dataextra_beneficiario_alimentario['fecha_peso'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_alimentario['fecha_peso'])->format('Y-m-d');
              $dataextra_beneficiario_alimentario['usuario_id'] = auth()->user()->id;
              $dataextra_beneficiario_alimentario['persona_id'] = $persona->id;
              $extra_datos = ExtraDatosPersona::create(
                $dataextra_beneficiario_alimentario
              );
              auth()->user()->bitacora(request(), [
                  'tabla' => 'extradatospersonas',
                  'registro' => $extra_datos->id . '',
                  'campos' => json_encode($dataextra_beneficiario_alimentario) . '',
                  'metodo' => request()->method()
              ]);
            }
          }
        DB::commit();
        return response()->json(array('success' => true));
    } catch(\Exception $e) {
        DB::rollBack();
        return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }
  /**
   * Show the specified resource.
   * @return Response
   */
  public function show($programa,Beneficiarios $tabla){
    return $tabla->with(['programa'=>$programa])->render('asistenciaalimentaria::beneficiarios.show');
  }
  /**
   * Show the form for editing the specified resource.
   * @return Response
   */
  public function edit($programacion,$beneficiario,Request $request){
    $match = Persona::find($beneficiario);
    $b = Beneficiario::find($request['alim']);
    return view::make('asistenciaalimentaria::modal')
                ->with([
                    'vista_beneficiario' => 'asistenciaalimentaria::beneficiarios.fragmentos.datos_beneficiario',
                    'vista' => 'asistenciaalimentaria::beneficiarios.fragmentos.create_edit',
                    'programacion'=>Programacion::find($programacion),
                    'beneficiario'=>$b,
                    'persona' => $match,
                    'tipo'=>$request->get('id')
                ])
                ->render();;
  }
  /**
   * Update the specified resource in storage.
   * @param  Request $request
   * @return Response
   * Actualiza beneficiario
   */
  public function update($programacion,$beneficiario,Request $request){
    // $valid = $this->puedeModificar($programacion);
    // if($valid[0]==false){
    //     return new JsonResponse(['message'=>$valid['message']],403);
    // }
    try {
      DB::beginTransaction();
        $request['fecha_nacimiento'] = Carbon::createFromFormat('d/m/Y',$request['fecha_nacimiento'])->format('Y-m-d');
				$data_persona = $request->except(['esta_embarazada_lactando','anio_programa_id']);
				if(!$request->segundo_apellido)
					$data_persona['segundo_apellido'] = '';
        $persona = Persona::join('personas_programas','personas_programas.persona_id','personas.id')
                          ->join('alim_beneficiarios','alim_beneficiarios.persona_programa_id','personas_programas.id')
                          ->where('alim_beneficiarios.id',$beneficiario)
                          ->where('alim_beneficiarios.programacion_id',$programacion)
                          ->first(['personas.*']);
        $persona->update($data_persona);
        auth()->user()->bitacora(request(), [
            'tabla' => 'personas',
            'registro' => $persona->id . '',
            'campos' => json_encode($data_persona) . '',
            'metodo' => request()->method()
        ]);
        $p = Programacion::findOrFail($programacion);
        $data_persona_programa = [];
        $data_persona_programa['anios_programa_id'] = $p->anioprograma->id;
        $persona_programa = PersonasPrograma::join('alim_beneficiarios','alim_beneficiarios.persona_programa_id','personas_programas.id')
                                            ->where('alim_beneficiarios.id',$beneficiario)
                                            ->where('alim_beneficiarios.programacion_id',$programacion)
                                            ->first(['personas_programas.*']);
        $persona_programa->update($data_persona_programa);
        auth()->user()->bitacora(request(), [
            'tabla' => 'personas_programas',
            'registro' => $persona_programa->id . '',
            'campos' => json_encode($data_persona_programa) . '',
            'metodo' => request()->method()
        ]);
        $data_beneficiario_alimentario = [];
        if($request['curp'][10] == 'M')
          $data_beneficiario_alimentario = $request->only(['esta_embarazada_lactando']);
        else
          $data_beneficiario_alimentario['esta_embarazada_lactando'] = 0;
        $beneficiario_alimentario = Beneficiario::where('programacion_id',$programacion)
          ->where('alim_beneficiarios.id',$beneficiario)
          ->first();
        $data_beneficiario_alimentario['tipoaccion_id'] = $request->tipoaccion_id;
        $beneficiario_alimentario->update($data_beneficiario_alimentario);
        auth()->user()->bitacora(request(), [
            'tabla' => 'alim_beneficiarios',
            'registro' => $beneficiario_alimentario->id . '',
            'campos' => json_encode($data_beneficiario_alimentario) . '',
            'metodo' => request()->method()
        ]);
        $subprograma = TipoAccion::find($request->tipoaccion_id)->nombre;
        $is_programa_desayunos_id = Programacion::findOrFail($programacion)->anioprograma->programa->id;
        if($subprograma == 'NIÑOS Y NIÑAS DE 1 A 12 AÑOS' || $subprograma == 'DESAYUNOS ESCOLARES  (6 A 12 AÑOS)' || $subprograma == 'ATENCIÓN A MENORES DE 5 AÑOS EN RIESGO NO ESCOLARIZADOS (1 AÑO A 5 AÑOS 11 MESES)' || $is_programa_desayunos_id == config('asistenciaalimentaria.desayunosId')) {
          if($request->peso != null && $request->talla != null && $request->fecha_peso != null && $request->fecha_talla != null && $request->tiporopa_id != null) {
            $dataextra_beneficiario_alimentario = $request->only(['peso','talla','fecha_talla','fecha_peso','grado','tiporopa_id']);
            $dataextra_beneficiario_alimentario['fecha_talla'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_alimentario['fecha_talla'])->format('Y-m-d');
            $dataextra_beneficiario_alimentario['fecha_peso'] = Carbon::createFromFormat('d/m/Y',$dataextra_beneficiario_alimentario['fecha_peso'])->format('Y-m-d');
            $dataextra_beneficiario_alimentario['usuario_id'] = auth()->user()->id;
            $dataextra_beneficiario_alimentario['persona_id'] = $persona->id;
            $extra_datos = ExtraDatosPersona::create(
              //['persona_id' => $persona->id],
              $dataextra_beneficiario_alimentario
            );
            auth()->user()->bitacora(request(), [
                'tabla' => 'extradatospersonas',
                'registro' => $extra_datos->id . '',
                'campos' => json_encode($dataextra_beneficiario_alimentario) . '',
                'metodo' => request()->method()
            ]);
          }
        }
      DB::commit();
      return response()->json(array('success' => true));
    } catch(\Exception $e) {
        DB::rollBack();
        return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }
  /**
   * Remove the specified resource from storage.
   * @return Response
   */
  public function destroy($programacion,$beneficiario){
    $valid = $this->puedeModificar($programacion);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    try {
      DB::beginTransaction();
        $beneficiario_baja = Beneficiario::where('id',$beneficiario)->where('programacion_id',$programacion)->first();
				$per_prog = PersonasPrograma::find($beneficiario_baja->persona_programa_id);
				if($per_prog)
					$per_prog->delete();
        $beneficiario_baja->delete();
        auth()->user()->bitacora(request(), [
            'tabla' => 'alim_beneficiarios',
            'registro' => $beneficiario_baja->id . '',
            'campos' => '',
            'metodo' => request()->method()
        ]);
      DB::commit();
      return response()->json(array(200,'success'));
    } catch(\Exception $e) {
      DB::rollBack();
      return new JsonResponse(['message'=>$e->getMessage()],500);
    }
  }
  //Renderiza la vista para editar o crear un beneficiario
  /* public function search($programacion,$beneficiario,Request $request){
    $valid = $this->puedeModificar($programacion);
    if($valid[0]==false){
        return new JsonResponse(['message'=>$valid['message']],403);
    }
    $match = Persona::find($beneficiario);
    $b = Beneficiario::find($request['alim']);
    return view::make('asistenciaalimentaria::modal')
                ->with([
                    'vista_beneficiario' => 'asistenciaalimentaria::beneficiarios.fragmentos.datos_beneficiario',
                    'vista' => 'asistenciaalimentaria::beneficiarios.fragmentos.create_edit',
                    'programacion'=>Programacion::find($programacion),
                    'beneficiario'=>$b,
                    'persona' => $match,
                    'tipo'=>$request->get('id')
                ])
                ->render();
  } */
  //Autocomplete de un beneficiario
  public function find($id) {
    $xt = ExtraDatosPersona::where('persona_id',$id)->first();
    $persona = null;
    if($xt)
      $persona = Persona::join('extradatospersonas','extradatospersonas.persona_id','personas.id')
                        ->where('persona_id',$id)
                        ->orderBy('extradatospersonas.created_at','desc')
                        ->first();
    else
      $persona = Persona::find($id);
    return
        [
            'persona'=>$persona,
            'mun'=>$persona->municipio->nombre,
            'loc'=>$persona->localidad ? $persona->localidad->nombre : 'No registrada',
            'etn'=>$persona->etnia->nombre,
            'bienestar'=>(Persona::join('bienestarpersonas','bienestarpersonas.persona_id','personas.id')
                                ->where('persona_id',$id)->whereNull('bienestarpersonas.deleted_at')->first() ? 1 : 0)
        ];
  }
}
