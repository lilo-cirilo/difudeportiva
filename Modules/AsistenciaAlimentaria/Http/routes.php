<?php

Route::group([
  'middleware' => 'web',
  'prefix' => 'alimentarios',
  'namespace' => 'Modules\AsistenciaAlimentaria\Http\Controllers',
  'where'=> ["cocina_id" => "[0-9]+","sujeto_id"=>"[0-9]+","p_id"=>"[0-9]+"]
  ], function(){
    //Seccion para el apartado general
    Route::get('/','AsistenciaAlimentariaController@home')->name('alimentarios.home');                                                                              //Home
    Route::resource('/configuracion','AsistenciaAlimentariaController',['as'=>'alimentarios','only'=>['index','show','update']]);                                   //Configuracion
    Route::resource('/configuracion/usuarios','UsuarioController',['as'=>'alimentarios','only'=>['index','show','update']]);                                        //Configuracion de regiones por usuario
    Route::resource('peticiones', 'PeticionController',['as'=>'asistenciaalimentaria','only'=>['index','show','update','destroy']]);                                //Peticiones
    Route::resource('programas', 'ProgramaController',['as'=>'asistenciaalimentaria']);                                                                             //Programas
    Route::resource('programacion/{programacion}/convenios', 'ConvenioController',['as'=>'alimentarios.programacion', 'except'=>['index']]);                        //Convenios
    Route::resource('programacion/{programacion}/beneficiarios','BeneficiarioController', ['as'=>'alimentarios.programacion', 'except'=>['show']]);                 //Beneficiarios
    Route::get('programa/{programa}/beneficiarios','BeneficiarioController@show')->name('alimentarios.programa.beneficiarios');                                     //Todos los beneficiarios
    Route::resource('programacion/{programacion}/comites', 'ComiteController',['as'=>'alimentarios.programacion', 'except'=>['show']]);                             //Comites
    Route::get('programa/{programa}/comites', 'ComiteController@show')->name('alimentarios.programa.comites');                                                      //Todos los miembros de Comites
    // Route::resource('programa/{programa}/programacionrequisicion', 'ComiteController@show')->name('alimentarios.programa.programaciones');                       //Todas las programaciones en requisiciones
    Route::put('programacion/{programacion}/miembro/{miembro}', 'ComiteController@persona',['as'=>'alimentarios.programacion']);                                    //
    Route::get('programacion/{programacion}/programas', 'ProgramacionController@searchProgramas')->name('alimentarios.programas.search');                           //Encontrar programas según la programación
    Route::get('estados', 'ProgramacionController@estados')->name('alimentarios.estados');                                                                          //Para los estados_programas de cada programación
    // Route::get('programacion/{programacion}/beneficiario/{beneficiario}','BeneficiarioController@search')->name('alimentarios.beneficiario.search');                //Encontrar beneficiario y renderizar el formulario
    Route::get('persona/{persona}','BeneficiarioController@find')->name('alimentarios.persona.find');                                                               //Encontrar beneficiario y renderizar el formulario
    Route::resource('/licitaciones','Licitacion\LicitacionController', ['as' => 'alimentarios']);                                                                   //Licitaciones
    Route::resource('/usuarios','UsuarioController',['as' => 'alimentarios']);                                                                                      //Usuarios
    Route::resource('/requisicion/{requisicion}/programaciones','RequisicionProgramacionController',['as'=>'alimentarios','except'=>['edit','show','update']]);     //Programaciones dentro de requisiciones
    Route::post('requisicion/{requisicion}/programaciones/dt','RequisicionProgramacionController@index')->name('alimentarios.requisicion.programaciones.datos');
    Route::get('/requisicion/{requisicion}/recibos','RequisicionProgramacionController@generarRecibos')->name('alimentarios.requisicion.recibos');                  //genera todos los recibos de una requisicion
    Route::get('/programacion/{programacion}/requisiciones','RequisicionProgramacionController@programacionRequisiciones')->name('alimentarios.programacion.requisiciones'); //requisiciones de una programación
    Route::get('/programacion/{programacion}/requisicion/{requisicion}/recibo','RequisicionProgramacionController@generarRecibo')->name('alimentarios.programacion.requisicion.recibo');     //genera el recibo de una requisicionprogramacion especifica
    Route::get('/requisicion/{requisicion}/programaciones/list','RequisicionProgramacionController@list',['as'=>'alimentarios'])->name('alimentarios.programacion.requisiciones.not');
    Route::resource('/requisicion/{requisicion}/programacion/{programacion}/dotaciones','RequisicionProgramacionDotacionController',['as'=>'alimentarios.programacion.requisicion','except'=>['edit','show','update']]);
    Route::resource('/licitacion/{licitacion}/dotaciones','Licitacion\CantidadDotacionLicitacionController',['as'=>'alimentarios.licitacion','except'=>['edit','show','update']]);
    Route::post('/requisicion/{requisicion}/programacion/{programacion}/dotaciones/uall','RequisicionProgramacionDotacionController@updateDotaciones',['as'=>'alimentarios'])->name('alimentarios.dotaciones.uall');
    Route::resource('/dotacion','Licitacion\DotacionController',['as'=>'alimentarios','except'=>['update','edit']]);
    Route::resource('/validacion/oficios/{oficio}/requisiciones', 'ValidacionRequisicionController',['as'=>'alimentarios.validacion','only'=>['index']]);
    Route::resource('/validacion/oficios/{oficio}/requisiciones/{requisicion}/entregas', 'EntregaValidacionController',['as'=>'alimentarios.validacion','only'=>['index','create','store','destroy']]);
    /* Route::resource('/capacitaciones','CapacitacionController',['as'=>'alimentarios']);                                             //Capacitaciones
    Route::resource('requisiciones', 'RequisicionController', ['as'=>'alimentarios']); */
    Route::resource('/ccnc','Ccnc\CocinaController',['as'=>'alimentarios','parameters'=>['ccnc'=>'cocina_id']]);                            //Cocinas
    Route::group(['prefix' => 'ccnc','namespace' => '\Ccnc'], function () {                                                                 //Seccion para lo relacionado con ccnc
      Route::resource('/programacion','ProgramacionController',['as'=>'alimentarios.ccnc','parameters' => ['programacion' => 'p_id']]);                                                  //programacion
      Route::post('/beneficiarios/consulta','ProgramacionController@queryBenef')->name('alimentarios.ccnc.beneficiarios.consulta');
      Route::resource('/requisiciones', 'RequisicionController',['as'=>'alimentarios.ccnc','parameters'=>['requisicion'=>'r_id']]);
      Route::resource('/validacion/oficios', 'OficioController',['as'=>'alimentarios.ccnc.validacion','only'=>['index','store','create','destroy']]);
    });
    //Seccion para lo relacionado con desayunos escolares 
    Route::resource('/desayunos','Escuelas\DesayunoController',['as'=>'alimentarios','parameters'=>['desayunos'=>'p_id']]);                                                //Desayunos
    Route::group(['prefix'=>'desayunos','namespace'=>'\Escuelas'], function() {
        Route::resource('/programacion','ProgramacionController',['as'=>'alimentarios.desayunos','parameters'=>['programacion'=>'p_id']]);
        Route::post('/beneficiarios/consulta','ProgramacionController@queryBenef')->name('alimentarios.desayunos.beneficiarios.consulta');
        Route::resource('/requisiciones', 'RequisicionController',['as'=>'alimentarios.desayunos','parameters'=>['requisicion'=>'r_id']]);
        Route::resource('/validacion/oficios', 'OficioController',['as'=>'alimentarios.desayunos.validacion','only'=>['index','store','create','destroy']]);
      });
    //Seccion para lo relacionado con sujetos vulnerables
    Route::resource('/sujetos','Sujetos\SujetoController',['as'=>'alimentarios','parameters'=>['sujetos'=>'sujeto_id']]);                                                     //Sujetos
    Route::group(['prefix'=>'sujetos','namespace'=>'\Sujetos'], function() {
      Route::resource('/programacion','ProgramacionController',['as'=>'alimentarios.sujetos','parameters'=>['programacion'=>'p_id']]);
      Route::post('/beneficiarios/consulta','ProgramacionController@queryBenef')->name('alimentarios.sujetos.beneficiarios.consulta');
      Route::resource('/requisiciones', 'RequisicionController',['as'=>'alimentarios.sujetos','parameters'=>['requisicion'=>'r_id']]);
      Route::resource('/validacion/oficios', 'OficioController',['as'=>'alimentarios.sujetos.validacion','only'=>['index','store','create','destroy']]);
    });
    //Seccion para lo relacionado con leche
    Route::resource('/caics','Caics\CaicController',['as'=>'alimentarios','parameters'=>['caics'=>'p_id']]);                                                     //Sujetos
    Route::group(['prefix'=>'caics','namespace'=>'\Caics'], function() {
      Route::resource('/programacion','ProgramacionController',['as'=>'alimentarios.caics','parameters'=>['programacion'=>'p_id']]);
      Route::post('/beneficiarios/consulta','ProgramacionController@queryBenef')->name('alimentarios.caics.beneficiarios.consulta');
      Route::resource('/requisiciones', 'RequisicionController',['as'=>'alimentarios.caics','parameters'=>['requisicion'=>'r_id']]);
      Route::resource('/validacion/oficios', 'OficioController',['as'=>'alimentarios.caics.validacion','only'=>['index','store','create','destroy']]);
    });
    //Relacionado con el proveedor
    Route::group(['prefix'=>'proveedor/{proveedor}','namespace'=>'\Proveedor'],function(){
        Route::get('requisiciones', 'ProveedorController@index')->name('alimentarios.proveedor.requisiciones.index');
        Route::get('requisiciones/{requisicion}', 'ProveedorController@show')->name('alimentarios.proveedor.requisiciones.show');
        Route::resource('/requisiciones/{requisiciones}/ordenCarga', 'OrdenCargaController',['as'=>'alimentarios.proveedor.requisicion','only'=>['index','create','store']]);
        Route::resource('/requisiciones/{requisicion}/entregas', 'EntregaController',['as'=>'alimentarios.proveedor.requisicion']);
        Route::resource('/conductores', 'ConductorController',['as'=>'alimentarios.proveedor']);
        Route::get('/chofer/search', 'ConductorController@search')->name('alimentarios.proveedor.chofer.search');
        Route::get('/chofer/{conductor}/find', 'ConductorController@find')->name('alimentarios.proveedor.chofer.find');
        Route::resource('ordencarga', 'OrdenesCargaController',['as'=>'alimentarios.proveedor','only'=>['index','destroy']]);
    });
    //Encontrar en listas
    Route::get('/escuelas','Escuelas\DesayunoController@escuelas')->name('alimentarios.escuelas');                                     //Escuelas
    Route::get('/distribucion','Escuelas\DesayunoController@tiposdistribucion')->name('alimentarios.tiposdistribucion');               //Distribuciones
    Route::get('/convenios','ConvenioController@tiposconvenio')->name('alimentarios.tiposconvenio');                                   //Convenios
    Route::get('/financiamientos','ProgramacionBaseController@tiposfinanciamiento')->name('alimentarios.tiposfinanciamiento');
		Route::put('/programacion/{programacion}/status','ProgramacionBaseController@changeStatus')->name('alimentarios.programacion.status');
		Route::post('/{programa}/programacion/lock','ProgramacionBaseController@lock')->name('alimentarios.programacion.lock');		//Bloquear (CAMBIAR A ACTIVA BLOQUEADA) programaciones
		Route::post('/{programa}/programacion/unlock','ProgramacionBaseController@unlock')->name('alimentarios.programacion.unlock');		//Desbloquear (CAMBIAR A ACTIVA) programaciones
    Route::get('/entregas','RequisicionBaseController@tiposentrega')->name('alimentarios.tiposentrega');
    Route::get('/select/licitacion','Licitacion\LicitacionController@licitaciones')->name('alimentarios.catlicitaciones');
    //TiposAccion
    Route::get('/subprogramas/{subprograma}/tiposaccion','TipoAccionController@getTiposAccion')->name('alimentarios.subprogramas.tiposaccion');
    //Reporte de cuotas de recuperación
    Route::get('/programa/{programa}/cuotasRecuperacion','CuotaRecuperacionController@index')->name('alimentarios.cuotasrecuperacion');
    //Reporte de distribuciones
    Route::get('programa/{programa}/distribuciones', 'DistribucionController@index')->name('alimentarios.programa.distribuciones');
    Route::post('programa/{programa}/distribuciones', 'DistribucionController@index')->name('alimentarios.programa.distribuciones.datos');
  }
);
