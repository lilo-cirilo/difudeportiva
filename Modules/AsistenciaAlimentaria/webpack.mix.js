let mix = require('laravel-mix');

mix.js('Resources/src/js/app.js', './../../public/modules/asistenciaalimentaria/js/app.js')
  .scripts([
    'Resources/src/js/modals.js',
    'Resources/src/js/transacciones.js',
    'Resources/src/js/validaciones.js',
    'Resources/src/js/enjoyhint.min.js',
    'Resources/src/js/validarCurp.js'
  ], './../../public/modules/asistenciaalimentaria/js/all.js')
  .sass('Resources/src/sass/app.scss', './../../public/modules/asistenciaalimentaria/css/app.css')
  // .options({
  //     processCssUrls: false
  // })
  .setPublicPath('./../../public/modules/asistenciaalimentaria');
  //.setPublicPath('Resources/assets/');