<?php

namespace Modules\AsistenciaAlimentaria\Traits;
use Illuminate\Support\Facades\Auth;
use Modules\AsistenciaAlimentaria\Entities\Programacion;

trait AlimentariosTrait{
	public function acceso(array $roles){
		if (!Auth::user()->hasRolesModulo($roles, config('asistenciaalimentaria.moduloId'))) {
			abort(403, 'No cuentas con los permisos necesarios.');
		}
	}

	public function puedeModificar($prog_id){
        $prog = Programacion::find($prog_id);
        if(strpos($prog->estadoprograma->estado->nombre,"BLOQUEADA")!==false){
            return [false, 'message'=>'Cocina boqueada'];
        }
        $prs = $prog->programacionrequisiciones()->whereHas('requisicionDatoCarga',function($q){
            $q->whereNotNull('id')
            ->whereHas('entregaproveedor',function($qu){
                $qu->whereNotNull('id')
                ->whereHas('validacion',function($que){
                    $que->whereNotNull('id');
                });
            });
        })->count();
        if($prog->programacionrequisiciones->count() > 0 && $prs==0)
            return [false, 'message'=>'Esta programacion tiene una requisicion en proceso'];

        return [true];
    }
}
