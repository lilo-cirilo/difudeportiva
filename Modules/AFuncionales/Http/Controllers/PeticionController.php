<?php

namespace Modules\AFuncionales\Http\Controllers;

use App\Models\Programa;
use Illuminate\Http\Request;
use App\Models\MotivosPrograma;
use App\Models\EstadosSolicitud;
use App\Models\PeticionesPersonas;
use Illuminate\Support\Facades\DB;
use App\DataTables\PeticionesDataTable;
use App\Models\BeneficiosprogramasSolicitud;
use App\Http\Controllers\PeticionBaseController;

class PeticionController extends PeticionBaseController
{


  public function __construct()
  {
    $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
  }

  public function index(PeticionesDataTable $dataTable)
  {
    //si quisiera que fueran que todos vieran todo de afuncionales se usa esto pero falta obtener los subprogramas de cada uno xD
    $progs = [];
    foreach (Programa::where('tipo', 'SUBPROGRAMA')->where(function ($q) {
      $q->where('nombre', 'like', 'apoyos funcionales')->orwhere('nombre', 'like', 'trabajo social');
    })->get() as $p) {
      // dd(DB::select('call getProgramasHijos(?)',[$p->id]));
      // dd(array_map( function($o){ return $o->programa_id; }, DB::select('call getProgramasHijos(?)',[$p->id])));
      $progs = array_merge($progs, array_map(function ($o) {
        if ($o->programa_tipo == "PRODUCTO") return $o->programa_id;
      }, DB::select('call getProgramasHijos(?)', [$p->id])));
    };
    // dd($progs);
    //por ahora se mostrara según el area que entre sea trabajo social y apoyos funcionales asi que solo pasamos el area del usuario logueado
    //Las peticiones se filtrarán de acuerdo al programa afuncionales, mas bien al area correspondiente
    // dd(auth()->user()->persona->empleado->area_id);
    // auth()->user()->persona->empleado->area_id
    return $dataTable
      ->with([/* 'area_id'=>Programa::where([['nombre','INCLUSIÓN MOTRIZ'],['tipo','PROGRAMA']])->first()->id, */
        'programas_id' => $progs,
        'modulo' => $this->getModulo()
      ])
      ->render('afuncionales::peticiones.index', ['motivos' => MotivosPrograma::with('motivo')->where('programa_id', 3)->get()]);
  }

  public function update($peticion_id, Request $request)
  {
    $peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);

    //Si esta tratando de cambiar el estatus a LISTA DE ESPERA        
    if ($request['estatus'] == 5) {
      //Verifico que todos los beneficiarios esten validados
      //Ya sea que esten aprobados o rechazados
      $beneficiarios_no_validados = PeticionesPersonas::where('beneficiosprogramas_solicitud_id', $peticion->id)->where('evaluado',0)->count();
      //Si faltan por validar le digo que termine de hacer su chamba
      if ($beneficiarios_no_validados > 0) {
        return response()->json(['errors' => array(['code' => 422, 'message' => 'Existe(n) ' . $beneficiarios_no_validados . ' beneficiarios(s) en estatus de validación'])], 422);
      }
    }


    try {
      DB::beginTransaction();
      $status_solicitud = EstadosSolicitud::updateOrCreate([
        'beneficioprograma_solicitud_id' => $peticion_id,
        'statusproceso_id' => $request['estatus'],
        'motivo_programa_id' => $request['motivo_bitacora'],
        'observacion' => $request['observacion']
      ], [
        'usuario_id' => auth()->user()->id
      ]);

      //Agregando a la bitácora
      auth()->user()->bitacora(request(), [
        'tabla' => 'estados_solicitudes',
        'registro' => $status_solicitud->id . '',
        'campos' => json_encode($status_solicitud) . '',
        'metodo' => 'POST'
      ]);

      DB::commit();
      return response()->json(['success' => true], 200);
    } catch (\Exception $e) {
      DB::rollBack();
      return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
    }
  }

  public function getModulo()
  {
    return 'afuncionales';
  }
}
