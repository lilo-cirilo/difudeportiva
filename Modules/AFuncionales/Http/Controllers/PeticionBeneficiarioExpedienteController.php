<?php

namespace Modules\AFuncionales\Http\Controllers;

use App\Models\DocumentosPersona;
use App\Models\Persona;
use App\Models\ProgramasSolicitud;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use View;

class PeticionBeneficiarioExpedienteController extends Controller {    

    public function store(Request $request) {

    }
    
    

    

    //Validar los documentos de una persona requeridos de un programa
    private function validarExpediente($peticion, $persona){
        $status = true;
        
        //Si le falta algún documento por check entonces el estatus del beneficiario es VALIDANDO
        foreach($peticion->programa->aniosprogramas()->last()->documentosprogramas as $documentoprograma){//->latest()->first()->documentosprogramas as $documentoPrograma){
            if(!$documentoPrograma->documentospersonas->contains('persona_id',$persona->id)){
                $status = false;
                break;
            }
        }
        dd($true);
        if($status){
            $peticion->solicitudespersonas->where('persona_id',$persona->id)->first()->evaluado = true;
        }else {
            $peticion->solicitudespersonas->where('persona_id',$persona->id)->first()->evaluado = null;
        }
        $peticion->solicitudespersonas->where('persona_id',$persona->id)->first()->save();
        auth()->user()->bitacora(request(), [
            'tabla' => 'solicitudes_personas',
            'registro' => $peticion->solicitudespersonas->where('persona_id',$persona->id)->first()->id . '',
            'campos' => json_encode($peticion->solicitudespersonas->where('persona_id',$persona->id)->first()) . '',
            'metodo' => 'PUT'
        ]);
    }
}