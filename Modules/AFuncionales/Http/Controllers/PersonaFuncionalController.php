<?php

namespace Modules\AFuncionales\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Persona;
use App\Models\Afuncionalespersona;
use App\Models\Afuncionalessilla;
use Illuminate\Support\Facades\DB;

class PersonaFuncionalController extends Controller {

    public function __construct(){
        $this->middleware(['auth', 'authorized', 'roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA']);
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('afuncionales::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $persona = Persona::where('curp',$request->curp)->first();
                $request['usuario_id'] = $request->user()->id;
                $request['persona_id'] = $persona->id;
                $personaFuncional = Afuncionalespersona::create($request->all());
                auth()->user()->bitacora(request(), [
                    'tabla' => 'afuncionalespersonas',
                    'registro' => $personaFuncional->id . '',
                    'campos' => json_encode($personaFuncional) . '',
                    'metodo' => request()->method()
                ]);
                if($request->has('tiposilla')) {
                    $request['usuario_id'] = $request->user()->id;
                    $request['afuncionalespersona_id'] = $personaFuncional->id;
                    $personaSilla = Afuncionalessilla::create($request->all());
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'afuncionales_sillas',
                        'registro' => $personaSilla->id . '',
                        'campos' => json_encode($personaSilla) . '',
                        'metodo' => request()->method()
                    ]);
                }
                DB::commit();
                return response()->json(['status' => 'ok', 'persona_id' => $persona->id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }                    
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return view('afuncionales::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($persona_funcional_id, Request $request) {        
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                $personaFuncional = Afuncionalespersona::find($persona_funcional_id);
                $personaFuncional->limitacion_id = $request->limitacion_id;
                $personaFuncional->discapacidad_id = $request->discapacidad_id;
                $personaFuncional->tiempodiscapacidad = $request->tiempodiscapacidad;
                $personaFuncional->usuario_id = $request->user()->id;
                $personaFuncional->save();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'afuncionalespersonas',
                    'registro' => $personaFuncional->id . '',
                    'campos' => json_encode($personaFuncional) . '',
                    'metodo' => request()->method()
                ]);
                
                if($request->has('tiposilla')) {                    
                    $request['usuario_id'] = $request->user()->id;
                    Afuncionalessilla::updateOrCreate(
                        ['afuncionalespersona_id' => $personaFuncional->id], $request->all()
                    );                    
                    auth()->user()->bitacora(request(), [
                        'tabla' => 'afuncionales_sillas',
                        'registro' => $personaFuncional->afuncionalessilla->id . '',
                        'campos' => json_encode($personaFuncional->afuncionalessilla) . '',
                        'metodo' => request()->method()
                    ]);
                }               
            
                DB::commit();
                return response()->json(['status' => 'ok', 'persona_id' => $personaFuncional->persona->id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }
}