@extends('afuncionales::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<style>
</style>
@endpush

@section('content-subtitle', 'Registrar Salida de Productos')

@section('li-breadcrumbs')
    <li> <a href="{{ route('afuncionales.productos.salidas') }}">Salidas</a></li>
    @if (isset($salida))
        <li class="active">Editar salida</li>
    @else
        <li class="active">Nueva salida</li>
    @endif
@endsection

@section('content')

    <div class="box box-primary shadow">
        <div class="box-header with-border">
            <h3 class="box-title">Registrar salida de productos</h3>
        </div>            
        <div class="row">
            <form id="form-salida" action="{{ isset($salida) ? route('afuncionales.productos.salidas.update') : route('afuncionales.productos.salidas.store') }}" 
                method="POST">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">                                                            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-8">
                                <div class="form-group">
                                    <label for="fechasalida">Fecha de salida* :</label>               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    <input type="text" class="form-control pull-right" id="fechasalida" name="fechasalida" value="{{ isset($salida) ? $salida->get_formato_fecha() : '' }}">
                                    </div>
                                </div>
                            </div>                                

                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-4">
                                <div class="form-group">
                                    <label for="tiposalida_id">Tipo de salida* :</label>               
                                    <select id="tiposalida_id" class="form-control select2"  name="tiposalida_id" style="width: 100%;">
                                        @foreach($tipos_salida as $tipo)
                                            <option value="{{$tipo->id}}">{{ $tipo->tipo }}</option>
                                        @endforeach
                                    </select>                                                        
                                </div>
                            </div>                           
                        </div>    
                        <div class="row">                           
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label>Solicitante* :</label>                                        
                                    <div class="input-group">
                                        <select id="persona_id" name="persona_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
                                            @if( isset($salida) ) 
                                                <option selected value="{{ $salida->persona->empleado->id }}">{{ $salida->persona->get_nombre_completo() }}</option>
                                            @endif
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-empleados','persona_id');">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="empleado_id">Empleado que autoriza* :</label>                                      
                                    <div class="input-group">
                                        <select id="empleado_id" name="empleado_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
                                                @if( isset($salida) ) 
                                                <option selected value="{{ $salida->empleado->id }}">{{ $salida->empleado->persona->get_nombre_completo() }}</option>
                                            @endif
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar empleado" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-empleados','empleado_id');">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>

                        <div class="row">                                            
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                <div class="form-group">
                                    <label for="areas_producto_id">Producto* :</label>  
                                    <select id="areas_producto_id" class="form-control select2"  name="areas_producto_id" style="width: 100%;"></select>   
                                </div>
                            </div>                                        
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-2" style="display: none;">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad* :</label>  
                                    <input type="text" id="cantidad" name="cantidad" class="form-control">                                                                
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="" style="color:white">Agregar:</label>
                                    <button type="button" class="btn btn-block btn-success" onclick="agregar_producto()"><i class="fa fa-plus"></i> Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label>Adjuntar Recibo/Comprobante* :</label>
                            <input id="recibo" type="file" name="recibo" data-msg-placeholder="Seleccione un archivo">
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Lista de productos</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>                            
                            <th class="text-center">CANTIDAD</th>
                            <th class="text-center">PRODUCTO</th>
                            <th class="text-center">FOLIO</th>
                            <th class="text-center">OPCIONES</th>
                        </tr>
                    </thead>
                    <tbody id="tblProductos">
                        @if(isset($salida))
                            @foreach($salida->detallesalidasproductos as $detalle)                                
                                @foreach($detalle->productosfoliados as $productofoliado)
                                        <tr data-folio="{{ $productofoliado->folio }}" data-area-producto="{{ $detalle->areasproducto->id }}">           
                                        <td class="text-center">1</td>             
                                        <td class="text-center">{{$detalle->areasproducto->producto->producto}}</td>
                                        <td class="text-center">{{$productofoliado->folio}}</td>
                                        <td class="text-center"><button class="btn btn-danger btn-sm" onclick="eliminarProducto(this, true,'{{ $productofoliado->folio }}')">Eliminar</button></td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>        
        <div class="box-footer">
            <div class="pull-right">
                <button type="button" class="btn btn-md btn-success pull-right" onclick="create_edit_salida()"><i class="fa fa-database"></i> Guardar</button>
            </div>      
        </div>
    </div>

<div class="modal fade" id="modal-empleados">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-empleados')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-empleados">Tabla Empleados:</h4>
            </div>
            <div class="modal-body" id="modal-body-empleados">            
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_empleado" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_empleado" name="btn_buscar_empleado">Buscar</button>
                    </span>
                </div>            
                
                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'empleados', 'name' => 'empleados', 'style' => 'width: 100%']) !!}

            </div>
            <div class="modal-footer" id="modal-footer-empleados">
                <div class="pull-right">                    
                    <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-empleados')"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>      
        </div>
    </div>
</div>

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.0/js/locales/es.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">    
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};    
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    var modal_persona = $('#modal-persona');
    var modal_title_persona = $('#modal-title-persona');
    var modal_body_persona = $('#modal-body-persona');
    var modal_footer_persona = $('#modal-footer-persona');
    var btn_buscar = $('#btn_buscar');
    var search = $('#search');        
    var select = undefined;

    function init_modal_empleado() {
		var table = $('#empleados').dataTable();
		datatable_empleados = $(table).DataTable();

		$('#search_empleado').keypress(function(e) {
			if(e.which === 13) {
				datatable_empleados.search($('#search_empleado').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_empleados.search($('#search_empleado').val()).draw();
		});

		$('#modal-empleados').on('shown.bs.modal', function(event) {
			datatable_empleados.ajax.reload();
		});
	}

    $(".modal").modal({
        backdrop: "static"
    });
    $(".modal").modal('hide');

    function create_edit_salida(){
        $('#persona_id').rules('add', { required: true });
        $('#fechasalida').rules('add', { required: true });
        $('#tiposalida_id').rules('add', { required: true });
        $('#empleado_id').rules('add', { required: true });
        $('#areas_producto_id').rules('remove', 'required');
        //$('#cantidad').rules('remove', 'required');
        if($('#form-salida').valid()){
            if($("#tblProductos tr").length == 0){              
                swal(
                    'Error',
                    'Aún no ha registrado productos',
                    'error'
                )
                return;
            }
            block();
            var form = $('#form-salida');
            var formData = new FormData(form[0]);
            formData.append("salida", JSON.stringify(getSalida()));

            $.ajax({
                url: form.attr('action'),
                type: form.attr('method'),
                data: formData,
                processData: false, 
                contentType: false,
                success: function(response) {
                    unblock();
                    swal({
                        type: 'success',
                        title: '¡Correcto!',
                        text: 'Salida registrada',
                        showConfirmButton: false,
                        timer: 2000,
                        allowEscapeKey: false,
				        allowOutsideClick: false
                    }).then((result) => {
                        if (result.dismiss === swal.DismissReason.timer) {
                            block();
                            location.href = "{{route('afuncionales.productos.salidas')}}";
                        }
                    })
                },
                error: function(response) {
                    unblock();
                }
            });
        }
    }

    function getSalida(){
        var salida = {};
        salida.id = "{{$salida->id or ''}}";
        salida.fechasalida = $("#fechasalida").val();
        salida.tipossalida_id = $("#tiposalida_id").val();
        salida.empleado_id = $("#empleado_id").val();
        salida.persona_id = $("#persona_id").val();
        salida.recibo = '';
        salida.detalle = {};                    
        
        for(var i=0; i<$("#tblProductos tr").length; i++){
            if(salida.detalle[$("#tblProductos tr").eq(i).attr("data-area-producto")] === undefined) {
                salida.detalle[$("#tblProductos tr").eq(i).attr("data-area-producto")] = [];
            }
            salida.detalle[$("#tblProductos tr").eq(i).attr("data-area-producto")].push(
                $("#tblProductos tr").eq(i).attr("data-folio")
            );            
        }
        return salida;
    }    

    function seleccionar_empleado(id, nombre) {
		select.html("<option value='" + id + "'selected>" + nombre + "</option>");
        select.trigger('change');
		$('#modal-empleados').modal('hide');		
	};
    
    function abrir_modal(modal, el) {        
        $("#"+modal).modal('show');        		
        select = $('#' + el);
	};

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    }

    $('[data-toggle="tooltip"]').tooltip();    

    $('#tiposalida_id').select2({
        language: 'es',
        placeholder: 'SELECCIONE UNA OPCIÓN',
        minimumResultsForSearch: Infinity
    }).change(function(event){
        $(this).valid();
    });

    $('#tiposalida_id').select2("enable",false);
    $('#tiposalida_id').val('2');
    $('#tiposalida_id').trigger('change');

    $('#fechasalida').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd-mm-yyyy',
        startDate: '01-01-1900',
		endDate: '0d'
    }).change(function(event) {
		$("#fechasalida").valid();	
    });;

    $("#areas_producto_id").select2({
		language: 'es',
		ajax: {
			url: '{{ route('afuncionales.apoyos.select') }}',
            delay: 500,
			dataType: 'JSON',
			type: 'GET',
			data: function(params) {
				return {
					search: params.term
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: $.map(data.data, function(item) {
						return {
							id: item.folio,
              text: item.nombre.replace(/ENTREGA DE/g, '').trim() + ' ' + item.folio,
              slug: item.nombre.replace(/ENTREGA DE/g, '').trim(),
              results: item
						}
					})
				};
			},
			cache: true
		}
	}).change(function(event) {
		$("#areas_producto_id").valid();	
    });

    $("#empleado_id, #persona_id").change(function(event) {
		$(this).valid();	
    });    

    function agregar_producto(){
        $('#fechasalida').rules('remove', 'required');
        $('#tiposalida_id').rules('remove', 'required');
        $('#empleado_id').rules('remove', 'required');
        $('#persona_id').rules('remove', 'required');
        //$('#cantidad').rules('add', { required: true, minlength: 1, pattern_numero: '' });
        $('#areas_producto_id').rules('add', { required: true });
        if($('#form-salida').valid()){
	    	agregarFila();
	    }else{
		    swal(
  			    '¡Advertencia!',
  			    'Debe seleccionar un producto',
  			    'warning'
		    )
	    }
    }

    function agregarFila(){
        var tr = $('[data-folio="' + $("#areas_producto_id").select2('data')[0].results.folio + '"]');
        //var cantidad = parseInt($("#cantidad").val());
        if(tr.length > 0){
            swal({
				title: '¡Verifique!',
                text: 'Ya ha seleccionado este producto',
				type: 'info',
				showCancelButton: false,
                showConfirmButton: true,
				cancelButtonColor: '#d33',
				confirmButtonText: 'Regresar'				
			});
        }else{
            $("#tblProductos").append(
                '<tr data-area-producto="' + $("#areas_producto_id").select2('data')[0].results.id + '" ' +
                'data-folio="'+$("#areas_producto_id").select2('data')[0].results.folio + '">' +
                '<td class="text-center">1</td>' +
                '<td class="text-center">' + $("#areas_producto_id").select2('data')[0].results.nombre + '</td>' + 
                '<td class="text-center">' + $("#areas_producto_id").select2('data')[0].results.folio + '</td>' +
                '<td class="text-center"><button class="btn btn-danger btn-sm" onclick="eliminarProducto(this)">Eliminar</button></td>' + 
                '</tr>'
            );
        }
        $("#areas_producto_id").empty();
  	    $('#cantidad').val('');
    }  

    function eliminarProducto(btn, eliminar_en_bd, folio){
        if(eliminar_en_bd){
            swal({
                title: '¿Está seguro de eliminar este producto?',
                text: 'El producto se eliminará definitivamente de la salida',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'NO',
                confirmButtonText: 'SI'
            }).then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ URL::to('afuncionales/productos/salidas/detalle/destroy') }}",
                        type: 'POST',
                        data: {_method: 'delete', folio: folio},
                        success: function(response) {
                            $(btn).parent().parent().remove();
                            swal({
                                title: '¡Eliminado!',
                                text: 'El producto ha sido eliminado.',
                                type: 'success',
                                timer: 3000
                            });
                        },
                        error: function(response) {
                        }
                    });                    
                }
            });
        }else{
            swal({
                title: '',
                text: '¿Esta seguro de eliminar este producto de la salida?',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'NO',
                confirmButtonText: 'SI'
            }).then((result) => {
                if(result.value) {
                    $(btn).parent().parent().remove();
                }
            });
        }        
    }

    

        $('#form-salida').validate({
            rules:  {
                areas_producto_id: {
                    required: true
                },
                cantidad: {
                    required: true
                },
                fechasalida: {
                    required: true
                },
                tiposalida_id: {
                    required: true
                },
                empleado_id: {
                    required: true
                },
                persona_id: {
                    required: true
                }
            }
        });

        @if(isset($salida) && $salida->recibo)

            var reci = '{{ asset($salida->get_url_comprobante()) }}'

            var type = reci.split('.')[reci.split('.').length-1].toLowerCase();

            if(type == 'pdf'){
                type = 'pdf';
            }else{
                type = 'image';
            }

            $("#recibo").fileinput({
                initialPreview: [ '{{ asset($salida->get_url_comprobante()) }}' ],
                initialPreviewAsData: true,          
                initialPreviewFileType: type, 
                initialPreviewConfig: [
                    {caption: "Comprobante", url: '{{ asset($salida->get_url_comprobante()) }}'}
                ],
                allowedFileExtensions: ["jpg", "png", "pdf"],
                initialPreviewShowDelete: false,
                fileActionSettings: {
                    showDrag: false
                },
                language: "es",
                showUpload: false,
                showCancel: false,
                dropZoneEnabled: false,
                browseClass: 'btn btn-info',
                removeClass: 'btn btn-danger margin-3'
            });
        @else
            $("#recibo").fileinput({
                language: "es",
                showUpload: false,
                showCancel: false,
                dropZoneEnabled: false,
                browseClass: 'btn btn-info',
                removeClass: 'btn btn-danger margin-3',
                fileActionSettings: {
                    showRemove: false,
                    showUpload: false,
                    showDownload: false,
                    showZoom: true,
                    showDrag: false,
                },
                allowedFileExtensions: ["jpg", "png", "pdf"]
            });
        @endif       
        
        init_modal_empleado();
</script>
@endpush