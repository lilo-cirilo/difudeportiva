<script type="text/javascript">
    $('#modal-beneficiario').on('shown.bs.modal', function (event) {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            Beneficiario.set_tab($(e.target).attr("href"));
            if($(e.target).attr("href") === "#tab_Discapacidad"){                
                $('#btn_editar_beneficiario').show();
            }else if($(e.target).attr("href") === "#tab_Expediente"){                
                $('#btn_editar_beneficiario').show();
            }else{
                $('#btn_editar_beneficiario').hide();
            }
        });
    }); 

    var Beneficiario = (() => {

        let tab = "";

        var set_tab = (valor) => {
            tab = valor;
        }

        var editar = (persona_id) => {
            
            if(tab === "#tab_Discapacidad"){
                block();
                var url = window.location.href + '/beneficiarios/' + persona_id + '/discapacidad/0/edit';
                $.get(url, function(data) {
                    $("#tab_Discapacidad .row").first().append(data.discapacidad);
                    $("#tab_Discapacidad .row").first().children().first().hide();
                    $('#btn_editar_beneficiario').hide();                    
                    $('#modal-footer-beneficiario').html(
                        '<button type="submit" class="btn btn-success" onclick="Beneficiario.guardar_discapacidad();"><i class="fa fa-database"></i> Guardar</button>'+
                        '<button type="button" class="btn btn-danger" onclick="Beneficiario.cancelar_discapacidad(' + persona_id + ');"><i class="fa fa-reply"></i> Regresar</button>'
                    )
                    discapacidad_init();
                    unblock();
                });          
            } else if(tab === "#tab_Expediente"){
                block();
                var url = window.location.href + '/beneficiarios/' + persona_id + '/edit';
                $.get(url, function(data) {
                    $("#tab_Expediente .row").first().append(data.expediente);
                    $("#tab_Expediente .row").first().children().first().hide();
                    $('#btn_editar_beneficiario').hide();                    
                    $('#modal-footer-beneficiario').html(
                        '<button type="submit" class="btn btn-success" onclick="Beneficiario.guardar_expediente(' + persona_id + ');"><i class="fa fa-database"></i> Guardar</button>'+
                        '<button type="button" class="btn btn-danger" onclick="Beneficiario.cancelar_expediente(' + persona_id + ');"><i class="fa fa-reply"></i> Regresar</button>'
                    )
                    expediente_init();
                    unblock();
                });
            }
        }

        var discapacidad_init = () => {
            $('#form-persona-funcional').validate({
                rules: 	{
                    discapacidad_id: {
                        required: true
                    },
                    limitacion_id: {
                        required: true
                    },
                    peso: {
                        required: true,
                        pattern_peso: 'Ingrese sólo números'
                    },
                    altura: {
                        required: true,
                        pattern_altura: 'Ingrese sólo números'
                    },
                    tiempodiscapacidad: {
                        required: true,
                        pattern_años: 'Ingrese sólo números'
                    },
                    tiposilla: {
                        required: true
                    },
                    terreno: {
                        required: true
                    }
                }
            });

            $('#limitacion_id, #tiposilla, #terreno, #motivo').select2({
                language: 'es',
                placeholder: 'SELECCIONE UNA OPCIÓN',
                minimumResultsForSearch: Infinity
            }).change(function(event) {
                $(this).valid();
            });

            $('#discapacidad_id').select2({
                language: 'es',
                ajax: {
                    url: '{{ route('discapacidades.select') }}',
                    dataType: 'JSON',
                    type: 'GET',
                    delay: 500,
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data, params) {
                        var datos = [];
                        for(var j=0; j<data.length; j++){
                            var found = false;
                            for(var i=0; i<datos.length; i++){
                                if(datos[i].text == data[j].padre){
                                    datos[i].children.push({
                                        "id": data[j].id,
                                        "text": data[j].nombre                                    
                                    });
                                    found = true;
                                    break;
                                }
                            }
                            if(!found){
                                datos.push({
                                    text: data[j].padre,
                                    children: [{
                                        "id": data[j].id,
                                        "text": data[j].nombre
                                    }]
                                });
                            }
                        }
                        params.page = params.page || 1;
                        
                        return {
                            results: datos
                        };
                    },
                    cache: true
                }
            }).change(function(event) {
                $('#discapacidad_id').valid();
            });
        }

        var expediente_init = () => {
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });
        }

        var guardar_discapacidad = () => {
            var form = $('#form-persona-funcional');
            if(form.valid()) {
                block();
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: form.serialize(),
                    success: function(response) {
                        mostrar_persona(response.persona_id, 'afuncionales', () => {                            
                            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                                Beneficiario.set_tab($(e.target).attr("href"));
                                if($(e.target).attr("href") === "#tab_Discapacidad"){                
                                    $('#btn_editar_beneficiario').show();
                                }else if($(e.target).attr("href") === "#tab_Expediente"){                
                                    $('#btn_editar_beneficiario').show();
                                }else{
                                    $('#btn_editar_beneficiario').hide();
                                }
                            });                            
                            if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
                            init_tabla_apoyos();
                            $('#modal-beneficiario .nav.navbar-nav a[href="#tab_Discapacidad"]').tab('show');
                            $('#modal-footer-beneficiario').html(
                                '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + response.persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
                            );                            
                        }
                        });
                        datatable_beneficiarios.ajax.reload(null, false);                        
                        unblock();                    
                        swal(        
                            'Información registrada',
                            '',
                            'success'
                        )          
                    },
                    error: () => {
                        unblock();
                        swal(        
                            'Error',
                            '',
                            'error'
                        )
                    }	    
                });
            }
	    }

        var guardar_expediente = (persona_id) => {
            var form = $('#form-persona-expediente');
            var url = window.location.href + '/beneficiarios/' + persona_id;
            block();
            $.ajax({
                url: url,
                type: 'PUT',
                data: form.serialize(),
                success: function(response) {
                    mostrar_persona(response.persona_id, 'afuncionales', () => {                        
                        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                            Beneficiario.set_tab($(e.target).attr("href"));
                            if($(e.target).attr("href") === "#tab_Discapacidad"){                
                                $('#btn_editar_beneficiario').show();
                            }else if($(e.target).attr("href") === "#tab_Expediente"){                
                                $('#btn_editar_beneficiario').show();
                            }else{
                                $('#btn_editar_beneficiario').hide();
                            }
                        });
                        if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
                            init_tabla_apoyos();
                        }
                        $('#modal-beneficiario .nav.navbar-nav a[href="#tab_Expediente"]').tab('show');
                        $('#modal-footer-beneficiario').html(
                            '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + response.persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
                        );
                    });
                    datatable_beneficiarios.ajax.reload(null, false);                        
                    unblock();                    
                    swal(        
                        'Información registrada',
                        '',
                        'success'
                    )          
                },
                error: () => {
                    unblock();
                    swal(
                        'Error',
                        '',
                        'error'
                    )
                }
            });
            
	    }

        var cancelar_discapacidad = (persona_id) => {
            $("#tab_Discapacidad .row").first().children().first().show();
            $("#tab_Discapacidad .row").first().children().last().remove();
            $('#modal-footer-beneficiario').html(
                '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
            );
        }

        var cancelar_expediente = (persona_id) => {
            $("#tab_Expediente .row").first().children().first().show();
            $("#tab_Expediente .row").first().children().last().remove();
            $('#modal-footer-beneficiario').html(
                '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
            );
        }

        var set_persona = (id, nombre, modal) => {
            $("#form_f").attr("action", window.location.href + '/beneficiarios/' + id + '/entrega');
            
            $('#form_f').validate({
                rules:  {
                    apoyo: {
                        required: true
                    },
                    fecha: {
                        required: true
                    },
                    recibo: {
                        required: true
                    },
                    empleado_id: {
                        required: true
                    }
                }
            });

            $('#form_r').validate({
                rules:  {
                    motivo: {
                        required: true
                    }
                }
            });

            $('#apoyo').select2({
                language: 'es',
                ajax: {
                    delay: 500,
                    url: '{{ route('afuncionales.apoyos.select') }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term,
                            programa: {{$peticion->beneficio->anioprograma->programa->id}},
                            page : params.page || 1
                        };
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data.data, function(item) {
                                return {
                                    id: item.id,
                                    text: item.nombre.replace('ENTREGA DE ','') + ' ' + item.folio,
                                    slug: item.nombre.replace('ENTREGA DE ',''),
                                    results: item
                                }
                            }),
                            pagination: {
                              more : data.current_page < data.last_page
                            }
                        };
                    },
                    cache: true
                }
            });

            $('#fecha_entrega').datepicker({
                autoclose: true,
                language: 'es',
                format: 'dd-mm-yyyy',
                startDate: '01-01-1900',
                endDate: '0d'
            }).change(function(event) {
                $('#fecha_entrega').valid();
            });

            $("#empleado_id").select2({
                language: 'es',
                minimumInputLength: 2,
                ajax: {
                    delay: 500,
                    url: '{{ route('empleados.select') }}',
                    dataType: 'JSON',
                    type: 'GET',
                    data: function(params) {
                        return {
                            search: params.term
                        };
                    },
                    processResults: function(data, params) {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, function(item) {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(function(event) {
                $("#empleado_id").valid();	
            });
            $("#form_f, #form_r")[0].reset();
            $('#apoyo, #empleado_id, #motivo').val(null).trigger('change');
            var validator = $( "#form_f" ).validate();
            validator.resetForm();
            validator = $( "#form_r" ).validate();
            validator.resetForm();
            $("#entregaSolicitante, #rechazoSolicitante").html('<label>Solicitante:</label> '+ nombre);
            $("#"+modal).modal("show");
        }

        var entregar = () => {
            if($('#form_f').valid()) {
                block();            
                var form = $('#form_f');            
                var formData = new FormData(form[0]);
                formData.append("type","Entregado");
                formData.append("folio",$("#apoyo").select2('data')[0].results.folio);
                formData.append("areas_producto_id",$("#apoyo").select2('data')[0].results.areas_producto_id);                
                $.ajax({
                    url: form.attr('action'),
                    type: "POST",
                    data: formData,
                    processData: false, 
                    contentType: false,           
                    success:  function (response) {
                        unblock();
                        datatable_beneficiarios.ajax.reload(null, false);
                        $(".modal").modal("hide");
                        $('#arc2').val('');
                        $('#apoyo').val(null).trigger('change');

                        if(response.status === "FINALIZADO"){
                            app.set_bloqueo(false);
                            swal({
                                title: 'Entrega registrada',
                                text: "",
                                type: 'success',
                                showCancelButton: false,                                
                                confirmButtonText: 'Aceptar'
                            }).then((result) => {
                                location.reload();
                                /* if (result.value) {
                                    swal(
                                        'Petición Finalizada',
                                        '',
                                        'success'
                                    )
                                } */
                            });
                        }else{
                            swal({
                                title: 'Entrega registrada',
                                text: "",
                                type: 'success',
                                showCancelButton: false,                                
                                confirmButtonText: 'Aceptar'
                            });
                        }                       
                    }
                }); 
            }
        }

        return {
            editar,
            set_tab,
            guardar_discapacidad,
            guardar_expediente,
            cancelar_discapacidad,
            cancelar_expediente,
            set_persona,
            entregar
        }

    })();
    
</script>