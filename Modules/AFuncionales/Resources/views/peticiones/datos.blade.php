<div class="box box-primary">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:30%">
                                <i class="fa fa-id-card margin-r-5"></i>
                                Folio:
                        </th>
                        <td>{{ $beneficio->solicitud->folio }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-user margin-r-5"></i>
                            Remitente
                        </th>
                        <td>
                            @if($beneficio->solicitud->solicitudpersonales)
                                {{ $beneficio->solicitud->solicitudpersonales->persona->nombreCompleto() }}
                            @elseif($beneficio->solicitud->solicitudmunicipales)
                                {{ $beneficio->solicitud->solicitudmunicipales->persona->get_nombre_completo() }}
                                <br>
                                {{ $beneficio->solicitud->solicitudmunicipales->municipio->nombre }}
                            @elseif($beneficio->solicitud->solicitudregionales)
                                {{ $beneficio->solicitud->solicitudregionales->persona->get_nombre_completo() }}
                                <br>
                                {{ $beneficio->solicitud->solicitudregionales->region->nombre }}
                            @elseif($beneficio->solicitud->solicituddependencias)
                                {{ $beneficio->solicitud->solicituddependencias->dependencia->nombre }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-tags margin-r-5"></i>
                            Tipo:
                        </th>
                        <td>{{ $beneficio->solicitud->tiposremitente->tipo }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-file-text-o margin-r-5"></i>
                            Asunto:
                        </th>
                        <td>{{ $beneficio->solicitud->asunto }}</td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:40%">
                            <i class="fa fa-calendar margin-r-5"></i>
                            Fecha de vinculación:
                        </th>
                        <td>{{ $beneficio->statussolicitudes->where("statusproceso_id",3)->first()->created_at }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-wheelchair margin-r-5"></i>
                            Apoyo Solicitado:
                        </th>
                        <td id="programa">{{ $beneficio->programa->nombre }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-list-ol margin-r-5"></i>
                            Cantidad
                        </th>
                        <td>{{ $beneficio->cantidad }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-signal margin-r-5"></i>
                            Estatus:</th>
                        <td id="lblStatus">
                            @if($beneficio->statusActual() == "VINCULADO" || $beneficio->statusActual() == "VALIDANDO")
                                @if($beneficio->statusActual() == "VINCULADO")
                                    NUEVO
                                @else
                                    {{ $beneficio->statusActual() }}
                                @endif
                                <button type="button" class="btn btn-success pull-right btn-sm" onclick="listaEspera()">
                                    <i class="fa fa-mail-forward"></i>
                                    LISTA DE ESPERA
                                </button>
                            @else
                                {{ $beneficio->statusActual() }}
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    @if($beneficio->statusActual() == "CANCELADO")
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:30%">
                                    <i class="fa fa-id-card margin-r-5"></i>
                                    Motivo de cancelación:
                            </th>
                            <td>{{ $beneficio->statussolicitudes->where("statusproceso_id",7)->first()->motivo->motivo }}</td>
                        </tr>
                    </table>
                </div>
            </div>
    
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:40%">
                                <i class="fa fa-calendar margin-r-5"></i>
                                Observación:
                            </th>
                            <td>{{ $beneficio->statussolicitudes->where("statusproceso_id",7)->first()->observacion }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    @endif
</div>