@extends('afuncionales::layouts.master')
@push('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
.row {
	margin-right: 0px;
	margin-left: 0px;
}

#modal-body-beneficiario {
    min-height: 550px;
}

input {
	text-transform: uppercase;
}

.modal-body {
    overflow-y: auto;
    max-height: calc(100vh - 210px);
}

</style>

@endpush

@section('content-title', 'Petición')
@section('content-subtitle', "{$beneficio->solicitud->folio}")
@section('li-breadcrumbs')
    <li><a href="{{ route('afuncionales.peticiones.index') }}">Peticiones</a></li>
    <li class="active">Seguimiento</li>
@endsection

@section('content')

@include('afuncionales::peticiones.datos')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="box box-primary">
            
            <div class="box-header with-border">
                @if($beneficio->statusActual() == "VINCULADO" || $beneficio->statusActual() == "VALIDANDO" || $beneficio->statusActual() == "CANCELADO")
                    <h4 class="box-title">Lista de solicitantes</h4>
                @else
                    <h4 class="box-title">Lista de beneficiarios</h4>
                @endif
            </div>

            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="searchBeneficiario" name="searchBeneficiario" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscarBeneficiario" name="btn_buscarBeneficiario">Buscar</button>
                    </span>
                </div>
                {!! $sdt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'table_beneficiarios', 'name' => 'table_beneficiarios', 'style' => 'width: 100%']) !!}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-beneficiario">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-beneficiario"></h4>
            </div>
            <div class="modal-body" id="modal-body-beneficiario"></div>
            <div class="modal-footer" id="modal-footer-beneficiario"></div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-comprobante">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-comprobante">Comprobante de entrega</h4>
            </div>
            <div class="modal-body" id="modal-body-comprobante">
                <img id="comprobante" class="img-responsive" src="" alt="Comprobante">
            </div>
            <div class="modal-footer" id="modal-footer-comprobante"></div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-entrega">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header label-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-entrega">Datos de la entrega</h4>
            </div>
            <div class="modal-body" id="modal-body-entrega"></div>
            <div class="modal-footer" id="modal-footer-entrega">
                <button type="button" onclick="cerrar_entrega();" class="btn btn-danger"><i class="fa fa-close"></i> Cerrar</button>
            </div>      
        </div>
    </div>
</div>

@if($beneficio->statusActual() == "VINCULADO" || $beneficio->statusActual() == "VALIDANDO")
    @include('afuncionales::peticiones.modals')
@endif

@if($beneficio->statusActual() == "LISTA DE ESPERA")
    @include('afuncionales::peticiones.modals_entrega')
@endif

@stop 
@push('body')

<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $sdt->html()->scripts() !!}
{!! $pdt->html()->scripts() !!}

@include('personas.js.persona')
<script type="text/javascript">
    $(".modal").modal({
        keyboard: false,
        backdrop: "static"
    });
    $(".modal").modal('hide');
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    app.to_upper_case();
	app.agregar_bloqueo_pagina();    

    function persona_create_edit_success(response) {
		app.set_bloqueo(false);
		unblock();
        modal_personas.modal("hide");
        modal_persona.modal("hide");
        if(response.estatus_AFuncionales === "nuevo" || $.inArray(parseInt(response.id), solicitantes) < 0 ){
            swal(
                '¡Correcto!',
                'Información registrada',
                'success'
            );
            buscarPersona(response.id, "", true);
        }else{
            datatable_beneficiarios.ajax.reload(null, false);
            swal(
                '¡Correcto!',
                'Información actualizada',
                'success'
            );
            mostrarBeneficiario(response.id, "#tab_Datos", false);
        }        
	};

	function persona_create_edit_error(response) {
		unblock();
        if(response.status === 422) {
			swal({
				title: 'Error al registrar al solicitante',				
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Regresar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	};

    var modal_persona = $('#modal-persona');
    var modal_personas = $('#modal-personas');
    var modal_title_persona = $('#modal-title-persona');
    var modal_body_persona = $('#modal-body-persona');
    var modal_footer_persona = $('#modal-footer-persona');

    var modal_beneficiario = $('#modal-beneficiario');
    var modal_title_beneficiario = $('#modal-title-beneficiario');
    var modal_body_beneficiario = $('#modal-body-beneficiario');
    var modal_footer_beneficiario = $('#modal-footer-beneficiario');

    var modal_discapacidad = $('#modal-discapacidad');
    var modal_title_discapacidad = $('#modal-title-discapacidad');
    var modal_body_discapacidad = $('#modal-body-discapacidad');
    var modal_footer_discapacidad = $('#modal-footer-discapacidad');

    var modal_expediente = $('#modal-expediente');
    var modal_title_expediente = $('#modal-title-expediente');
    var modal_body_expediente = $('#modal-body-expediente');
    var modal_footer_expediente = $('#modal-footer-expediente');

    var idCancelar;
    var motivos_entrega = $('#motivo');
    var recibo_entrega = $('#recibo');
    var fecha_entrega = $('#fecha_entrega');

    var personas = $('#personas');
    var btn_buscar = $('#btn_buscar');
    var search = $('#search');    

    var en_espera;
    var no_validados;
    var solicitantes;

    $(".modal").modal({
        backdrop: "static"
    });
    $(".modal").modal('hide');

    $('#modal-beneficiario').on('shown.bs.modal', function (event) {
		if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
			agregar_tabla_apoyos();
		}
	}); 

    modal_persona.on('shown.bs.modal', function(event) {
        modal_body_persona.scrollTop(0);
	}); 

    function init_modal_persona() {
		var table = $('#personas').dataTable();
		datatable_personas = $(table).DataTable();
		search.keypress(function(e) {
			if(e.which === 13) {
				datatable_personas.search(search.val()).draw();
			}
		});
		btn_buscar.on('click', function() {
			datatable_personas.search(search.val()).draw();
		});
		modal_personas.on('shown.bs.modal', function(event) {
			datatable_personas.ajax.reload();
		});
	}

    function abrir_modal_personas() {        
		modal_personas.modal('show');
	}

    function agregar_tabla_beneficiarios() {
        var table = $('#table_beneficiarios').dataTable();
		datatable_beneficiarios = $(table).DataTable();

        datatable_beneficiarios.on( 'xhr', function () {
            var json = datatable_beneficiarios.ajax.json();
            no_validados = json.no_validados;
            en_espera = json.en_espera;
            solicitantes = json.personas;
        } );

        $('#searchBeneficiario').keypress(function(e) {
			if(e.which === 13) {
				datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
			}
		});

		$('#btn_buscarBeneficiario').on('click', function() {
			datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
		});          
	}

    function agregar_tabla_apoyos() {
		datatable_apoyos = $("#apoyos-otorgados").DataTable({
			language: {
				url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			},
			dom: 'itp',
			lengthMenu: [ [3], [3] ],
			responsive: true,
			autoWidth: true,
			processing: true,
			columnDefs: [
				{ className: 'text-center', 'targets': '_all' }
			]
		});            
	}

    function seleccionar_persona(id, nombre){
        buscarPersona(id, "", true);
    }

    $("form").submit(function(e){
        e.preventDefault();
    });

    $(function () {
        var date = $('#fecha_entrega').datepicker({
            autoclose: true,
            language: 'es',
            format: 'dd-mm-yyyy',
            startDate: '01-01-1900',
		    endDate: '0d'
        }).change(function(event) {
            $('#fecha_entrega').valid();
        });
    });
    
    function buscarPersona(id, tab, nuevo) {
        if(nuevo && $.inArray(parseInt(id), solicitantes) >= 0){
            swal(
                '¡Verifique!',
                'La persona ya se encuentra registrada como solicitante de esta petición.',
                'info'
            );
            return;
        }
        
        modal_personas.modal('hide');
        block();
        $.get('/personas/search?tipo=create_edit&id=' + id, function(data) {})
        .done(function(data) {        
            modal_body_persona.html(data.html); 
            persona.init();
            persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();      

            $.get("/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+data.id, function(data) {                              
                modal_body_beneficiario.html(data.beneficiario);                
                modal_title_beneficiario.text($("#modal-beneficiario p").first().text());
                modal_footer_beneficiario.html(
                    '<button type="button" class="btn btn-warning pull-left" data-dismiss="modal" onclick="editar()"><i class="fa fa-pencil"></i> Editar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarBeneficiario()"><i class="fa fa-ban"></i> Cancelar</button>' +
                    '<button type="button" class="btn btn-success" id="addBeneficiario" onclick="agregarBeneficiario()" style="display: none"><i class="fa fa-database"></i> Aceptar</button>' +
                    '<button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar(\'atras\')" style="display: none"><i class="fa fa-arrow-left"></i> </button>'+
                    '<button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar(\'adelante\')"><i class="fa fa-arrow-right"></i> </button>'
                );                
                    
                modal_title_discapacidad.text($("#modal-beneficiario p").first().text());
                modal_body_discapacidad.html(data.discapacidad);
                modal_footer_discapacidad.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_funcional_create_edit(1);"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-reply"></i> Regresar</button>'                                          
                );

                modal_title_expediente.text($("#modal-beneficiario p").first().text());
                modal_body_expediente.html(data.expediente);
                modal_footer_expediente.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_expediente_create_edit(1);"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-reply"></i> Regresar</button>'                                          
                );

                $('a[data-toggle="tab"]').on('click', function(){
                    if ($(this).parent('li').hasClass('disabled')) {
                        return false;
                    }
                });

                if(tab === "#tab_Expediente"){
                    $('#modal-beneficiario .nav-tabs a[href="'+tab+'"]').tab('show');
                    $("#btnRegresar").css("display","");
                    $("#addBeneficiario").css("display","");
                    $("#btnAvanzar").css("display","none");
                }else if (tab === "#tab_Discapacidad"){ 
                    $('#modal-beneficiario .nav-tabs a[href="'+tab+'"]').tab('show');
                    $("#btnRegresar").css("display","");
                }

                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });           

                persona_script_init();                     
                modal_beneficiario.modal('show');
                unblock();            
            });
        });        
    }

    function agregar_persona() {
		$.get('/personas/search?tipo=create_edit', function(data) {
		})
		.done(function(data) {
		})
		.fail(function(data) {			
            modal_title_persona.text('Agregar Persona:');
            modal_body_persona.html(data.responseJSON.html);
            modal_footer_persona.html(
                '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>'+
                '<button type="submit" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'                
            );
            app.to_upper_case();
			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();            
			modal_persona.modal('show');
		});
	}

    function avanzar(direccion){
        var tab = $('#modal-beneficiario .nav-tabs li.active a').attr("href");
        
        if(tab === "#tab_Datos"){
            if($('#modal-beneficiario #curpBeneficiario').text()){
                $('#modal-beneficiario .nav-tabs a[href="#tab_Discapacidad"]').tab('show');
                $("#btnRegresar").css("display","");
            }else {
                swal({
                    title: 'Error',
                    text : 'Debe ingresar la CURP',
                    type : 'error',
                    toast: true,
                    position: 'center'
                });
            }
        } else if(tab === "#tab_Discapacidad"){
            if(direccion === "adelante"){
                if($("#tab_Discapacidad p").first().text()){
                    $("#addBeneficiario").css("display","inline");
                    $("#btnAvanzar").css("display","none");                
                    $('#modal-beneficiario .nav-tabs a[href="#tab_Expediente"]').tab('show');
                }else{
                    swal(
                        'Error',
                        "Debe ingresar los datos sobre la discapacidad",
                        'error'
                    )                    
                }                
            }else{
                $("#addBeneficiario").css("display","none");
                $("#btnRegresar").css("display","none");
                $('#modal-beneficiario .nav-tabs a[href="#tab_Datos"]').tab('show');
            }
            
        } else{
            if(direccion === "atras"){
                $("#addBeneficiario").css("display","none");
                $("#btnAvanzar").css("display","");
                $('#modal-beneficiario .nav-tabs a[href="#tab_Discapacidad"]').tab('show');                
            }
        }
    }
    
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    
    function mostrarBeneficiario(persona_id, tab, nuevaPersona){
        block();
        $.get("/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+persona_id, function(data) {
            modal_body_beneficiario.html(data.beneficiario);                
            modal_title_beneficiario.text($("#modal-beneficiario p").first().text());

            @if($beneficio->statusActual() == "VINCULADO" || $beneficio->statusActual() == "VALIDANDO")
            if(nuevaPersona){
                modal_footer_beneficiario.html(
                    '<button type="button" class="btn btn-warning pull-left" data-dismiss="modal" onclick="editar()"><i class="fa fa-pencil"></i> Editar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarBeneficiario()"><i class="fa fa-ban"></i> Cancelar</button>' +
                    '<button type="button" class="btn btn-success" id="addBeneficiario" onclick="agregarBeneficiario()" style="display: none"><i class="fa fa-database"></i> Aceptar</button>' +
                    '<button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar(\'atras\')" style="display: none"><i class="fa fa-arrow-left"></i> </button>'+
                    '<button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar(\'adelante\')"><i class="fa fa-arrow-right"></i> </button>'
                );
            }else{
                if(data.estatus !== 0){
                    modal_footer_beneficiario.html(
                        '<button type="button" class="btn btn-warning pull-left" onclick="editar()"><i class="fa fa-pencil"></i> Editar</button>'+
                        '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>' +
                        '<button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar(\'atras\')" style="display: none"><i class="fa fa-arrow-left"></i> </button>'+
                        '<button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar(\'adelante\')"><i class="fa fa-arrow-right"></i> </button>'
                    );
                }else{
                    modal_footer_beneficiario.html(
                        '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>' +
                        '<button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar(\'atras\')" style="display: none"><i class="fa fa-arrow-left"></i> </button>'+
                        '<button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar(\'adelante\')"><i class="fa fa-arrow-right"></i> </button>'
                    );
                }
                
            }
            @else
            modal_footer_beneficiario.html(                
                '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>' +
                '<button type="button" class="btn btn-primary" id="btnRegresar" onclick="avanzar(\'atras\')" style="display: none"><i class="fa fa-arrow-left"></i> </button>'+
                '<button type="button" class="btn btn-primary" id="btnAvanzar" onclick="avanzar(\'adelante\')"><i class="fa fa-arrow-right"></i> </button>'
            );
            @endif

                                               
            modal_title_discapacidad.text($("#modal-beneficiario p").first().text());
            modal_body_discapacidad.html(data.discapacidad);

            if(nuevaPersona){
                modal_footer_discapacidad.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_funcional_create_edit(1);"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-reply"></i> Regresar</button>'                    
                )
                modal_footer_expediente.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_expediente_create_edit(1);"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-reply"></i> Regresar</button>'                        
                )
            }else{
                modal_footer_discapacidad.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_funcional_create_edit();"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-close"></i> Cerrar</button>'                    
                )
                modal_footer_expediente.html(
                    '<button type="submit" class="btn btn-success" onclick="persona_expediente_create_edit();"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-close"></i> Cerrar</button>'                    
                )
            }            
            modal_title_expediente.text($("#modal-beneficiario p").first().text());
            modal_body_expediente.html(data.expediente);
            
            $('a[data-toggle="tab"]').on('click', function(){
                if ($(this).parent('li').hasClass('disabled')) {
                    return false;
                }
            });

            persona_script_init();

            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            if(tab === "#tab_Expediente"){
                $('#modal-beneficiario .nav-tabs a[href="'+tab+'"]').tab('show');
                $("#btnRegresar").css("display","");                
                $("#btnAvanzar").css("display","none");
            } else if (tab === "#tab_Discapacidad"){ 
                $('#modal-beneficiario .nav-tabs a[href="'+tab+'"]').tab('show');
                $("#btnRegresar").css("display","");
            }
                
            modal_beneficiario.modal('show');
            if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
    			agregar_tabla_apoyos();
		    }
            unblock();
        });
    }

    function editar(){        
        var tab = $('#modal-beneficiario .nav-tabs li.active a').attr("href");
        var persona_id = $('#modal-beneficiario #curpBeneficiario').attr("data-persona-id");

        if(tab === "#tab_Datos"){  
            swal(
                '¡Permiso denegado!',
                'No cuenta con los permisos para realizar esta acción.',
                'info'
            ); 
            /*block();         
            $.get('/personas/search?tipo=create_edit&id=' + persona_id, function(data) {
                modal_title_persona.text('Actualizar Datos');
                modal_body_persona.html(data.html);                
                modal_footer_persona.html(
                    '<button type="button" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'+
                    '<button type="button" class="btn btn-danger" onclick="cancelarEditar();"><i class="fa fa-reply"></i> Regresar</button>'                                     
                );             
                app.to_upper_case();
                persona.init();
                persona.editar_fotografia();
			    persona.agregar_fecha_nacimiento();
			    persona.agregar_inputmask();
			    persona.agregar_select_create();
			    persona.agregar_validacion();
                modal_persona.modal('show');
                persona_script_init();
                unblock();
            });*/

        } else if(tab === "#tab_Discapacidad"){
            modal_discapacidad.modal('show');
        } else{
            modal_expediente.modal('show');
        }
    }

    function cancelarEditar(){
        modal_persona.modal('hide');
        modal_discapacidad.modal('hide');
        modal_expediente.modal('hide');
        modal_beneficiario.modal('show');
    }

    function persona_expediente_create_edit(nuevo){
        var form = $('#form-persona-expediente'); 
        block();
        $.ajax({
            url: form.attr('action'),				
			type: "POST",
			data: form.serialize(),
			success: function(response) {                    
                modal_expediente.modal('hide');
                if(nuevo){
                    buscarPersona(response.persona_id,'#tab_Expediente',true);
                }else{
                    mostrarBeneficiario(response.persona_id, '#tab_Expediente');
                    datatable_beneficiarios.ajax.reload(null, false);
                }
                unblock();
                swal(        
                    'Expediente actualizado',
                    '',
                    'success'
                )          
            }		    
		});
    }

    function cancelarBeneficiario(){
        swal({
          title: '¿Desea cancelar el registro del solicitante?',
          text: '',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Sí, cancelar',
          cancelButtonText: 'No, regresar',
          reverseButtons: true
        }).then((result) => {
            if (result.value) {
                modal_beneficiario.modal('hide');
            }
        });
    }
    
    function eliminarBeneficiario(id, nombre){
        idCancelar = id;
        $("#lblSolicitante").text(nombre);
        $('#motivo').val(null).trigger('change');
        $("#modal-cancelar").modal("show");
    }

    function cancelarSolicitante(){
        if($('#form_cancelar').valid()) {
            block();
            $("#modal-cancelar").modal("hide");
            $.ajax({
                type: "DELETE",
                url: "/afuncionales/peticiones/{{ $beneficio->solicitud->id }}/programas/{{ $beneficio->programa->id }}/beneficiarios/"+idCancelar,                    
                data: "motivo="+$("#motivo").find(":selected").text(),
                success: function(response){   
                    datatable_beneficiarios.ajax.reload(null, false);
                    unblock();
                    swal(                                
                        '¡Correcto!',
                        'Solicitante cancelado',
                        'success'
                    )
                },
                error: function(response){
                    if(response.status === 409){
                        unblock();
                        swal(                                
                            '¡Ocurrió un error inesperado!',
                            response.responseJSON.message,
                            'error'
                        )
                    }                    
                }
            });
        }
    }

    function agregarBeneficiario(){
        block();
        $.ajax({
			url: $("#formBeneficiario").attr('action'),
			type: 'POST',
			data: "curp="+$("#curpBeneficiario").text(),
			success: function(response) {
                modal_beneficiario.modal('hide');      
                datatable_beneficiarios.ajax.reload(null, false);
                unblock();
                swal(                            
                    '¡Correcto!',
                    'Solicitante registrado',
                    'success'
                )                                                  
			},
            error: function(response){
                unblock();
                swal(
                    'Error',
                    response.responseJSON.errors[0].message,
                    'error'
                )               
            }
		});
    }
    
    function persona_funcional_create_edit(nuevo) {	
		var form = $('#form-persona-funcional');
		if(form.valid()) {
            block();
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize()+"&curp="+$("#curp").val(),
				success: function(response) {                    
                    modal_discapacidad.modal('hide');
                    if(nuevo){
                        buscarPersona(response.persona_id,'#tab_Discapacidad',true);
                    }else{
                        datatable_beneficiarios.ajax.reload(null, false);
                        mostrarBeneficiario(response.persona_id, '#tab_Discapacidad');
                    }
                    unblock();                    
                    swal(        
                        '¡Correcto!',
                        'Información registrada',
                        'success'
                    )          
                },
                error: function(){
                    unblock();
                }		    
			});
		}
	}       

    function persona_script_init() {
        $('#form-persona-funcional').validate({
			rules: 	{
				discapacidad_id: {
					required: true
				},
				limitacion_id: {
					required: true
				},
				peso: {
					required: true,
                    pattern_peso: 'Ingrese sólo números'
				},
                altura: {
					required: true,
                    pattern_altura: 'Ingrese sólo números'
				},
				tiempodiscapacidad: {
					required: true,
					pattern_años: 'Ingrese sólo números'
				},
				tiposilla: {
					required: true
				},
				terreno: {
					required: true
				}
			}
		});

        $('#limitacion_id, #tiposilla, #terreno, #motivo').select2({
            language: 'es',
            placeholder: 'SELECCIONE UNA OPCIÓN',
            minimumResultsForSearch: Infinity
        }).change(function(event) {
			$(this).valid();
		});

        $('#discapacidad_id').select2({
			language: 'es',
			ajax: {
				url: '{{ route('discapacidades.select') }}',
				dataType: 'JSON',
				type: 'GET',
                delay: 500,
				data: function(params) {
					return {
						search: params.term
					};
				},
				processResults: function(data, params) {
                    var datos = [];
                    for(var j=0; j<data.length; j++){
                        var found = false;
                        for(var i=0; i<datos.length; i++){
                            if(datos[i].text == data[j].padre){
                                datos[i].children.push({
                                    "id": data[j].id,
                                    "text": data[j].nombre                                    
                                });
                                found = true;
                                break;
                            }
                        }
                        if(!found){
                            datos.push({
                                text: data[j].padre,
                                children: [{
                                    "id": data[j].id,
                                    "text": data[j].nombre
                                }]
                            });
                        }
                    }
					params.page = params.page || 1;
                    
					return {
						results: datos
					};
				},
				cache: true
			}
		}).change(function(event) {
			$('#discapacidad_id').valid();
		});
    }

        

    function listaEspera(){        
        if(no_validados > 0) {
            swal(
                'Error',
                'Existe(n) ' + no_validados + ' solicitante(s) en estatus de validación',
                'error'
            )
            return;
        }

        if(datatable_beneficiarios.data().count() === 0 || en_espera === 0){
            swal(
                'Error',
                'No existen solicitantes',
                'error'
            )
            return;
        }
        
        swal({
            title: 'La petición cambiará a lista de espera',
            text: 'No podrá editar ni agregar solicitantes. ¿Desea continuar?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí'
        }).then((result) => {
            if(result.value) {
                $.ajax({
				    url: "{{ route('afuncionales.peticiones.programas.update', ['peticion_id'=>$beneficio->solicitud->id,'programa_id'=>$beneficio->programa->id]) }}",
				    type: "PUT",
				    data: "estatus_id=5",
				    success: function(response) {
                        swal({
                            title: 'El estatus de la petición ha sido cambiado',
                            text: '',
                            type: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        }).then((result) => {           
                            app.set_bloqueo(false);                 
                            location.reload();
                        });;                        
				    }
			    });                
            }
        });
    }

    
    function setPersona(id, nombre, modal) {
        $("#form_f, #form_r")[0].reset();
        $('#apoyo, #empleado_id, #motivo').val(null).trigger('change');
        var validator = $( "#form_f" ).validate();
        validator.resetForm();
        validator = $( "#form_r" ).validate();
        validator.resetForm();
        $("#form_f").attr("action", "/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+id+"/entrega");
        $("#form_r").attr("action", "/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+id+"/entrega");
        $("#entregaSolicitante, #rechazoSolicitante").html('<label>Solicitante:</label> '+ nombre);
        $("#"+modal).modal("show");
    }

    $(document).ready(function (){
        agregar_tabla_beneficiarios();
        $('[data-toggle="tooltip"]').tooltip();

         $('#form_f').validate({
            rules:  {
                apoyo: {
                    required: true
                },
                fecha: {
                    required: true
                },
                recibo: {
                    required: true
                },
                empleado_id: {
                    required: true
                }
            }
        });

        $('#form_r').validate({
            rules:  {
                motivo: {
                    required: true
                }
            }
        });

        $('#form_cancelar').validate({
            rules:  {
                motivo: {
                    required: true
                }
            }
        });

        $('#apoyo').select2({
            language: 'es',
            ajax: {
                delay: 500,
                url: '{{ route('afuncionales.apoyos.select') }}',
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term,
                        programa: {{$beneficio->programa->id}}
                    };
                },
                processResults: function(data, params) {                    
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.folio,
                                text: item.producto + ' ' + item.folio,
                                slug: item.producto,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $("#empleado_id").select2({
		    language: 'es',
		    minimumInputLength: 2,
		    ajax: {
                delay: 500,
			    url: '{{ route('empleados.select') }}',
			    dataType: 'JSON',
			    type: 'GET',
			    data: function(params) {
				    return {
					    search: params.term
				    };
			    },
			    processResults: function(data, params) {
				    params.page = params.page || 1;
				    return {
					    results: $.map(data, function(item) {
						    return {
							    id: item.id,
							    text: item.nombre,
							    slug: item.nombre,
							    results: item
						    }
					    })
				    };
			    },
			    cache: true
		    }
	    }).change(function(event) {
		    $("#empleado_id").valid();	
        });

        var motivos = $("#motivo");
        $.ajax({
            type: "GET",
            url: "/afuncionales/motivos",
            success: function(res){
                $(res).each(function(key,value) {
                    motivos.append("<option id='" + value.id + "' value=" + value.id + ">" + value.motivo +"</option>");                        
                });
                $('#motivo').select2({
                    language: 'es',
                    placeholder: 'SELECCIONE UNA OPCIÓN',
                    minimumResultsForSearch: Infinity
                }).change(function(event){
                    $(this).valid();
                });
            }
        });

        @if($beneficio->statusActual() != "VINCULADO" && $beneficio->statusActual() != "VALIDANDO")
            $('.dt-button.button-dt.tool').first().remove();
        @endif
    });

    function rechazar(){
        if($('#form_r').valid()) {
            block();
            var form = $('#form_r');
            var formData = new FormData(form[0]);
            formData.append("type","Cancelado");                    
            $.ajax({
                url: $('#form_r').attr('action'),               
                type: "POST",
                data: formData,
                processData: false, 
                contentType: false,
                success:  function (response) {
                    unblock();
                    datatable_beneficiarios.ajax.reload(null, false);
                    $(".modal").modal("hide");
                    if(response.status === "FINALIZADO"){
                        app.set_bloqueo(false);
                        $("#lblStatus").text("FINALIZADO");
                        swal({
                            title: 'Entrega rechazada',
                            text: "",
                            type: 'success',
                            showCancelButton: false,                                
                            confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            if (result.value) {
                                swal(
                                    'Petición Finalizada',
                                    '',
                                    'success'
                                )
                            }
                        });
                    } else{
                        swal(        
                            'Entrega rechazada',
                            '',
                            'success'
                        )
                    }
                }
            });
        }
    }

    function entregar() {
        if($('#form_f').valid()) {
            block();            
            var form = $('#form_f');            
            var formData = new FormData(form[0]);
            formData.append("type","Entregado");
            formData.append("folio",$("#apoyo").select2('data')[0].results.folio);
            formData.append("areas_producto_id",$("#apoyo").select2('data')[0].results.areas_producto_id);
            $.ajax({
                url: form.attr('action'),
                type: "POST",
                data: formData,
                processData: false, 
                contentType: false,           
                success:  function (response) {
                    unblock();
                    datatable_beneficiarios.ajax.reload(null, false);
                    $(".modal").modal("hide");
                    $('#arc2').val('');
                    $('#apoyo').val(null).trigger('change');

                    if(response.status === "FINALIZADO"){
                        app.set_bloqueo(false);
                        $("#lblStatus").text("FINALIZADO");
                        swal({
                            title: 'Entrega registrada',
                            text: "",
                            type: 'success',
                            showCancelButton: false,                                
                            confirmButtonText: 'Aceptar'
                        }).then((result) => {
                            if (result.value) {
                                swal(
                                    'Petición Finalizada',
                                    '',
                                    'success'
                                )
                            }
                        });
                    }else{
                        swal({
                            title: 'Entrega registrada',
                            text: "",
                            type: 'success',
                            showCancelButton: false,                                
                            confirmButtonText: 'Aceptar'
                        });
                    }                       
                }
            }); 
        }
    }

    function maximizar(url){
        $("#comprobante").attr("src",url);
        $("#modal-comprobante").modal('show');
    }

    function consultar_entrega(id){
        block();
        $.get("/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+id+"/entrega", function(data) {                              
            $("#modal-body-entrega").html(data.entrega);
            unblock();
            $("#modal-entrega").modal('show');            
        });        
    }

    function reactivar_solicitante(id, nombre){
        @if($beneficio->statusActual() == "VINCULADO" || $beneficio->statusActual() == "VALIDANDO")
            var tipo = "solicitante";
        @else
            var tipo = "beneficiario";
        @endif
        swal({
            title: '¿Está seguro de reactivar al solicitante?',
            text: nombre,
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
            cancelButtonText: 'No',
            confirmButtonText: 'Sí, reactivar'
        }).then((result) => {
            if(result.value) {
                $.ajax({
				    url: "/afuncionales/peticiones/{{$beneficio->solicitud->id}}/programas/{{$beneficio->programa->id}}/beneficiarios/"+id,
				    type: "PUT",
                    data: "tipo=" + tipo,
				    success: function(response) {
                        app.set_bloqueo(false);
                        datatable_beneficiarios.ajax.reload(null, false);
                        swal({
                            title: '¡Correcto!',
                            text: 'Solicitante reactivado',
                            type: 'success',
                            timer: 2500,
                            showConfirmButton: false
                        });                        
				    }
			    });                
            }
        });
    }

    function cerrar_entrega(){
        $("#modal-entrega").modal('hide');
    }

    init_modal_persona();
        
</script>
@endpush