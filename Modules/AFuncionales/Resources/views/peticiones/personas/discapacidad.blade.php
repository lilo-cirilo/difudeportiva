<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <form 
        data-toggle="validator" role="form" id="form-persona-funcional" 
        action="{{ isset($persona->afuncionalespersona) ? route('afuncionales.peticiones.beneficiarios.discapacidad.update', ['peticion'=>$peticion->id, 'beneficiario'=>$persona->id, 'discapacidad'=>0 ] ) : route('afuncionales.peticiones.beneficiarios.discapacidad.store', ['peticion'=>$peticion->id, 'beneficiario'=>$persona->id ]) }}" 
        method="{{ isset($persona->afuncionalespersona) ? 'PUT' : 'POST' }}">            
    
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="discapacidad_id">Discapacidad</label>               
                        <select id="discapacidad_id" class="form-control select2"  name="discapacidad_id" style="width: 100%;">
                            @if(isset($persona->afuncionalespersona))
                                <option value="{{ $persona->afuncionalespersona->discapacidad_id }}" selected>{{ $persona->afuncionalespersona->discapacidad->nombre }}</option>
                            @endif
                        </select>
                    </div>
                </div>                                
            </div>
            <div class="row">
                {{-- <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="limitacion_id">Tipo de limitación en la actividad</label>                                        
                        <select id="limitacion_id" class="form-control select2"  name="limitacion_id" style="width: 100%;">
                            <option value=""></option>
                            @foreach($limitaciones as $limitacion)
                                <option value="{{$limitacion->id}}" {{ (isset($persona->afuncionalespersona) && $persona->afuncionalespersona->limitacion_id == $limitacion->id) ? 'selected' : '' }}>{{ $limitacion->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div> --}}
                                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="tiempodiscapacidad">Tiempo con discapacidad</label>
                        <div class="input-group">
                            <input type="text" autocomplete="off" class="form-control" id="tiempodiscapacidad" name="tiempodiscapacidad" value="{{ $persona->afuncionalespersona->tiempodiscapacidad or '' }}">
                            <span class="input-group-addon">año(s)</span>
                        </div>
                    </div>
                </div>
            </div> 
            
            @if(strpos($peticion->beneficio->nombre, "SILLA") !== false)
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="tiempodiscapacidad">Tiempo con discapacidad</label>
                            <div class="input-group">
                                <input type="text" autocomplete="off" class="form-control" id="tiempodiscapacidad" name="tiempodiscapacidad" value="{{ $persona->afuncionalespersona->tiempodiscapacidad or '' }}">
                                <span class="input-group-addon">año(s)</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="altura">Altura</label>
                            <div class="input-group">
                                <input type="text" autocomplete="off" class="form-control" id="altura" name="altura" value="{{ $persona->afuncionalespersona->afuncionalessilla->altura or '' }}">
                                <span class="input-group-addon">metros</span>
                            </div>
                        </div>
                    </div>
                                
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group">
                            <label for="peso">Peso</label>                            
                            <div class="input-group">
                                <input type="text" autocomplete="off" class="form-control" id="peso" name="peso" value="{{ $persona->afuncionalespersona->afuncionalessilla->peso or '' }}">
                                <span class="input-group-addon">kg.</span>
                            </div>
                        </div>
                    </div>
                </div>
                            
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="tiposilla">Silla de ruedas</label>
                            <select id="tiposilla" class="form-control select2"  name="tiposilla" style="width: 100%;">
                                @foreach ($sillas as $silla)
                                  <option value="{{ $silla->id }}"> {{ $silla->nombre }} </option>
                                @endforeach{{-- <option value=""></option>
                                <option value="HOSPITALARIA INFANTIL" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->tiposilla == 'HOSPITALARIA INFANTIL') ? 'selected' : '' }}>HOSPITALARIA INFANTIL</option>
                                <option value="HOSPITALARIA ADULTO" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->tiposilla == 'HOSPITALARIA ADULTO') ? 'selected' : '' }}>HOSPITALARIA ADULTO</option>    
                                <option value="PCI" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->tiposilla == 'PCI') ? 'selected' : '' }}>PCI</option>
                                <option value="PCA" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->tiposilla == 'PCA') ? 'selected' : '' }}>PCA</option> --}}
                            </select>
                        </div>
                    </div>
                                
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="terreno">Tipo de terreno</label>
                            <select id="terreno" class="form-control select2"  name="terreno" style="width: 100%;">
                                <option value=""></option>
                                <option value="ARENA" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->terreno == 'ARENA') ? 'selected' : '' }}>ARENA</option>
                                <option value="CAMINO PAVIMENTADO" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->terreno == 'CAMINO PAVIMENTADO') ? 'selected' : '' }}>CAMINO PAVIMENTADO</option>    
                                <option value="GRAVA SUELTA" {{ (isset($persona->afuncionalespersona->afuncionalessilla) && $persona->afuncionalespersona->afuncionalessilla->terreno == 'GRAVA SUELTA') ? 'selected' : '' }}>GRAVA SUELTA</option>
                            </select>
                        </div>
                    </div>
                </div>
            @endif  
        </div>
    </form>
</div>