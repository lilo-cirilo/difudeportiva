<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active disabled"><a href="#tab_Datos" data-toggle="tab">Datos Generales</a></li>
        <li class="disabled"><a href="#tab_Discapacidad" data-toggle="tab">Discapacidad</a></li>
        <li class="disabled"><a href="#tab_Expediente" data-toggle="tab">Expediente</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_Datos">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form class="form-horizontal" id="formBeneficiario" action="{{ route('afuncionales.peticiones.programas.beneficiarios.store', ['peticion_id'=>$beneficio->solicitud_id, 'programa_id'=>$beneficio->programa_id]) }}">
                        <div class="row">
                            <label class="col-sm-2 control-label">Nombre:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ $persona->nombreCompleto() }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Género:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->genero == 'F' ? 'FEMENINO' : 'MASCULINO' }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Fecha de nacimiento:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->get_formato_fecha_nacimiento() }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">CURP:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static" id="curpBeneficiario" data-persona-id="{{ $persona->id }}">{{ $persona->curp }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Clave electoral:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->clave_electoral }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-label">Teléfono:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->numero_local }}</p>
                            </div>
                            <label class="col-sm-4 control-label">Celular:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->numero_celular }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-2 control-label">Domicilio:</label>
                            <div class="col-sm-10">
                                <p class="form-control-static">{{ $persona->calle . ' ' . $persona->numero_exterior . (isset($persona->numero_interior) ? '-'.$persona->numero_interior : '') . ' ' . $persona->colonia }}
                                    <br> {{ (isset($persona->localidad) ? $persona->localidad->nombre.', ' : '') . ' ' . $persona->municipio->nombre . (isset($persona->codigopostal) ? ', C.P. ' . $persona->codigopostal : '') }}</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h5 class="box-title">Historial de apoyos</h5>
                        </div>
                        <div class="box-body">
                            <table id="apoyos-otorgados" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">PETICIÓN</th>
                                        <th class="text-center">FECHA</th>
                                        <th class="text-center">PROGRAMA</th>
                                        <th class="text-center">PRODUCTO</th>
                                    </tr>   
                                </thead>
                                <tbody>
                                    @foreach($persona->solicitudespersonas->where("entregado",1)->take(5) as $beneficioOtorgado)
                                        <tr>
                                            <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->solicitud->folio }}</td>
                                            <td class="text-center">{{ $beneficioOtorgado->salida_producto->get_formato_fecha() }}</td>
                                            <td class="text-center">{{ $beneficioOtorgado->programas_solicitud->programa->nombre }}</td>
                                            <td class="text-center">{{ $beneficioOtorgado->salida_producto->detallesalidasproductos->first()->areasproducto->producto->producto }}</td>
                                        </tr>
                                    @endforeach
                                    {{-- @foreach($historial as $apoyo_en_bd_fea)
                                        <tr>
                                            <td class="text-center">{{ $apoyo_en_bd_fea->folio }}</td>
                                            <td class="text-center">{{ Carbon\Carbon::parse($apoyo_en_bd_fea->cp_fec_atendida)->format('d-m-Y') }}</td>
                                            <td class="text-center">{{ strtoupper($apoyo_en_bd_fea->producto) }}</td>
                                            <td class="text-center">{{ strtoupper($apoyo_en_bd_fea->cp_unimed) }}</td>
                                        </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
        <div class="tab-pane disabled" id="tab_Discapacidad">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form class="form-horizontal">
                        <div class="row">
                            <label class="col-sm-4 control-label">Discapacidad:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $persona->afuncionalespersona->discapacidad->nombre or '' }}</p>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <label class="col-sm-4 control-label">Tipo de limitación en la actividad:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ $persona->afuncionalespersona->limitacion->nombre or '' }}</p>
                            </div>
                        </div> --}}
                        <div class="row">
                            <label class="col-sm-4 control-label">Tiempo con discapacidad:</label>
                            <div class="col-sm-8">
                                <p class="form-control-static">{{ isset($persona->afuncionalespersona) ? $persona->afuncionalespersona->tiempodiscapacidad . ' año(s)' : '' }}</p>
                            </div>
                        </div>
                        @if(strpos($beneficio->programa->nombre, "SILLA") !== false)
                        <div class="row">
                            <label class="col-sm-4 control-label">Altura:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ isset($persona->afuncionalespersona->afuncionalessilla) ? $persona->afuncionalespersona->afuncionalessilla->altura . ' metros' : '' }}</p>
                            </div>
                            <label class="col-sm-3 control-label">Peso:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">{{ isset($persona->afuncionalespersona->afuncionalessilla) ? $persona->afuncionalespersona->afuncionalessilla->peso . ' kg' : '' }}</p>
                            </div>
                        </div>

                        <div class="row">
                            <label class="col-sm-4 control-label">Tipo de silla de ruedas:</label>
                            <div class="col-sm-3">
                                <p class="form-control-static">{{ $persona->afuncionalespersona->afuncionalessilla->tiposilla or '' }}</p>
                            </div>
                            <label class="col-sm-3 control-label">Tipo de terreno:</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">{{ $persona->afuncionalespersona->afuncionalessilla->terreno or '' }}</p>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <div class="tab-pane disabled" id="tab_Expediente">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <form role="form">
                        <div class="box-body">                        
                            @for($i = 0; $i < $beneficio->programa->aniosprogramas->last()->documentosprogramas->count(); $i++)
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                         <div class="form-group">
                                            <h5>
                                                @if($beneficio->programa->aniosprogramas->last()->documentosprogramas[$i]->documentospersonas->contains('persona_id',$persona->id))                                                                                 
                                                    <i class="icon fa fa-check"></i>
                                                @else
                                                    <i class="icon fa fa-close"></i>
                                                @endif
                                                {{ $beneficio->programa->aniosprogramas->last()->documentosprogramas[$i]->documento->nombre }}      
                                            </h5>
                                        </div>
                                    </div>
                                    
                                    @if(($i+1) < $beneficio->programa->aniosprogramas->last()->documentosprogramas->count())
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <h5>     
                                                    @if($beneficio->programa->aniosprogramas->last()->documentosprogramas[$i+1]->documentospersonas->contains('persona_id',$persona->id))
                                                        <i class="icon fa fa-check"></i>      
                                                    @else
                                                        <i class="icon fa fa-close"></i>
                                                    @endif                                    
                                                    {{ $beneficio->programa->aniosprogramas->last()->documentosprogramas[$i+1]->documento->nombre }}      
                                                </h5>
                                            </div>
                                        </div>
                                        <?php $i = $i+1; ?>                                        
                                    @endif  
                                </div>                              
                            @endfor
                        </div>
                    </form>
                </div>
            </div>            
        </div>        
    </div>
</div>