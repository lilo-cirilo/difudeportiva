@extends('afuncionales::layouts.master')

@section('content-subtitle', 'Beneficiarios')

@section('li-breadcrumbs')
    <li class="active">Beneficiarios</li>
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
</style>
@endpush

@section('content')

    <div class="box box-primary shadow">
            <div class="box-header">
              <h3 class="box-title">Historial de beneficiarios</h3>
            </div>
            <div class="box-body">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </span>
                </div>

                {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiarios', 'name' => 'beneficiarios', 'style' => 'width: 100%']) !!}
          </div>
        </div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
<script type="text/javascript">
$(document).ready(function() {
  var table = $('#beneficiarios').dataTable();

		datatable_beneficiarios = $(table).DataTable();

    $('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_beneficiarios.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_beneficiarios.search($('#search').val()).draw();
		});          
	});

</script>
@endpush


