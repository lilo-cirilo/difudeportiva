@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@if(auth()->check())
  @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('message-url-all', '#')
@section('message-all', '')
@section('message-number')
@section('message-text', 'No tienes mensajes')

@section('task-number')
@section('task-text', 'No tienes tareas')
@section('logo', asset('images/AF.png'))
@section('mini-logo', asset('images/funcionalesIcono.png'))

@section('content-title', 'Apoyos Funcionales')

@push('head')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<style>
  .modal-dialog:not([role="document"]) > .modal-content > .modal-body {
    overflow-y: auto;
    max-height: calc(100vh - 210px);
  }

  .row {
      margin-right: 0px;
      margin-left: 0px;
    }

    .nav>li>a:hover, .nav>li>a:active, .nav>li>a:focus {
          background: #d46985;
      }

</style>
@endpush

@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="#"><i class="fa fa-dashboard"></i>Apoyos Funcionales</a></li>
  @yield('li-breadcrumbs')
</ol>
@endsection

@section('sidebar-menu')
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">Apoyos Funcionales</li>
      <li>
        <a href="{{ route('home') }}">
          <i class="fa fa-home"></i><span>IntraDIF</span>
        </a>
      </li>

      <li {{ ($ruta === 'afuncionales.peticiones.index' || $ruta == '') ? 'class=active' : '' }}>
        <a href="{{ route('afuncionales.peticiones.index') }}">
        <i class="fa fa-list-alt"></i> <span>Peticiones</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

      <li {{ $ruta === 'afuncionales.beneficiarios' ? 'class=active' : '' }}>
        <a href="{{ route('afuncionales.beneficiarios') }}">
        <i class="fa fa-list-alt"></i> <span>Beneficiarios</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>

      <li class="{{ ($ruta === 'afuncionales.productos.index' || $ruta === 'afuncionales.productos.entradas.index' || $ruta === 'afuncionales.productos.salidas' || $ruta === 'afuncionales.productos.inventario') ? 'treeview active menu-open' : 'treeview' }}">
        <a href="#">
          <i class="fa fa-database"></i> <span>Productos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li {{ ($ruta === 'afuncionales.productos.index') ? 'class=active' : '' }}><a href="{{ route('afuncionales.productos.index')}}"><i class="fa fa-list-alt"></i>Lista de productos</a></li>
          <li {{ ($ruta === 'afuncionales.productos.entradas.index') ? 'class=active' : '' }}><a href="{{ route( 'afuncionales.productos.entradas.index' ) }}"><i class="fa fa-shopping-cart"></i>Entradas de productos</a></li>
          <li {{ ($ruta === 'afuncionales.productos.salidas') ? 'class=active' : '' }}><a href="{{ route( 'afuncionales.productos.salidas' ) }}"><i class="fa fa-shopping-cart"></i>Salidas de productos</a></li>
          <li {{ ($ruta === 'afuncionales.productos.inventario') ? 'class=active' : '' }}><a href="{{ route('afuncionales.productos.inventario') }}"><i class="fa fa-clipboard"></i>Inventario</a></li>
        </ul>
      </li>

      <li class="header">Catálogos</li>
      @if(auth()->user()->hasRoles(['ADMINISTRADOR DE BENEFICIOS']))
        <li {{ ($ruta === 'afuncionales.programas.index') ? 'class=active' : '' }}>
            <a href="{{route('afuncionales.programas.index')}}">
            <i class="fa fa-cubes"></i> <span>Beneficios</span>
            <span class="pull-right-container">            
            </span>
            </a>
        </li>
      @endif
      
      <li {{ ($ruta === 'dependencias.index') ? 'class=active' : '' }}>
        <a href="{{ route('afuncionales.dependencias.index') }}">
          <i class="fa fa-university"></i> <span>Dependencias</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
    </ul>
@endsection

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
	function block() {
        baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
@endpush