export default {
  $_veeValidate: {
    validator: "new"
  },
  data() {
    return {
      step: 1,
      petition_id: null,
      victimas: [],
      vias: ['Presencial', 'Telefónica', 'Correo'],
      victims: {
        name: "",
        namepaterno: "",
        namematerno: "",
        age: null,
        gender: null,
        beneficios: ""
      },
      registration: {
        anonymous: null,
        name: "",
        namepaterno: "",
        namematerno: "",
        age: null,
        relationship: null,
        gender: null,
        numerotelefono: null,
        correo: null,

        via: null,
        addressestado: null,
        addressmunicipio: null,
        addresscolonia: null,
        addresscalle: null,
        addressnumero: null,
        description: null
      },
      rules: {
        name: [
          v => !!v || "Nombre es Obligatorio",
          v => v.length <= 46 || "Menos de 46 Caracteres",
          (v) => /^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+[\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\']+)$/.test(v) || "Debe ser un nombre válido"
        ],



        namepaterno: [
          v => !!v || "Por lo menos debe anexar un apellido",
          v => v.length <= 46 || "Menos de 46 Caracteres",
          (v) => /^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\'])+$/.test(v) || "Debe ser un Apellido válido"
        ],
        namematerno: [
          v => v.length <= 46 || "Menos de 46 Caracteres"
        ],
        age: [v => !!v || "Edad Requerida"],
        relationship: [v => !!v || "Parentesco Requerido"],
        gender: [v => !!v || "Sexo Obligatorio"],

        via: [v => !!v || "Via es obligatoria "],
        addresscalle: [v => !!v || "Calle Obligatorio"],
        addressnumero: [v => !!v || "Numero Obligatorio"],
        description: [v => !!v || "Observacion Obligatorio"]
      }
    };
  },






  methods: {
    validatesolicitante() {
      if (this.$refs.formsolicitante.validate()) {
        axios
          .post("/petitions/queryperson", {
            name: this.registration.name,
            namepaterno: this.registration.namepaterno,
            namematerno: this.registration.namematerno,
            age: this.registration.age,
            gender: this.registration.gender
          })
          .then(response => {
            if (response.data.length > 0) {
              var nodo = document.createElement("div");

              response.data.forEach(function (element) {
                var el = document.createElement("a");
                el.href = "/petitions/" + element.id;
                el.innerText = "" + element.folio;
                el.target ="_blank";
                nodo.appendChild(el);
                nodo.appendChild(document.createElement("br"));
              });

              this.$swal({
                title: "Se encontró a un denunciante con los mismos datos",
                text:
                  "" + response.data[0].namefull + " en los folios:",
                content: nodo,
                icon: "warning",
                buttons: true,
                buttons: ["Regresar", "Siguiente paso con estos datos"],
                dangerMode: true,
              }).then(willDelete => {
                if (willDelete) {
                  if (this.registration.correo) {
                    this.vias = ['Presencial', 'Telefónica', 'Correo'];
                  }
                  else {
                    this.vias = ['Presencial', 'Telefónica'];
                  }
                  this.step = 2;
                }
              });
            }
            else {
              axios
                .post("/respaldo/petition/store", {
                  name: this.registration.name,
                  namepaterno: this.registration.namepaterno,
                  namematerno: this.registration.namematerno,
                  age: this.registration.age,
                  relationship: this.registration.relationship,
                  gender: this.registration.gender,
                  numerotelefono: this.registration.numerotelefono,
                  correo: this.registration.correo,
                })
                .then(response => {
                  if (this.registration.correo) {
                    this.vias = ['Presencial', 'Telefónica', 'Correo'];
                  }
                  else {
                    this.vias = ['Presencial', 'Telefónica'];
                  }
                  this.step = 2;
                })
                .catch(e => {
                  console.log("Error al almacenar la copia de respaldo");
                });
            }
          })
          .catch(e => {
            this.$swal(
              "Error intentar mas tarde" + e
            );
          });
      }
    },



    validatesolicitud() {
      if (this.$refs.formsolicitud.validate()) {
        if (this.registration.anonymous == null || !this.registration.anonymous) {
          this.registration.anonymous = "false";
        }
        this.registration.addresscalle = this.registration.addresscalle.toUpperCase();
        axios
          .post("/respaldo/petition/update", {
            anonymous: this.registration.anonymous,
            name: this.registration.name,
            namepaterno: this.registration.namepaterno,
            namematerno: this.registration.namematerno,
            age: this.registration.age,
            relationship: this.registration.relationship,
            gender: this.registration.gender,
            numerotelefono: this.registration.numerotelefono,
            correo: this.registration.correo,

            via: this.registration.via,
            addressestado: this.registration.addressestado,
            addressmunicipio: this.registration.addressmunicipio,
            addresscolonia: this.registration.addresscolonia,
            addresscalle: this.registration.addresscalle,
            addressnumero: this.registration.addressnumero,
            description: this.registration.description,
            step: 2,
          })
          .then(response => {

            console.log("Actualizando correctamente el respaldo");
          })
          .catch(e => {
            console.log("error actualizando el respaldo");
          });

        axios
          .post("/petitions/store", {
            anonymous: this.registration.anonymous,
            name: this.registration.name,
            namepaterno: this.registration.namepaterno,
            namematerno: this.registration.namematerno,
            age: this.registration.age,
            relationship: this.registration.relationship,
            gender: this.registration.gender,
            numerotelefono: this.registration.numerotelefono,
            correo: this.registration.correo,

            via: this.registration.via,
            addressestado: this.registration.addressestado,
            addressmunicipio: this.registration.addressmunicipio,
            addresscolonia: this.registration.addresscolonia,
            addresscalle: this.registration.addresscalle,
            addressnumero: this.registration.addressnumero,
            description: this.registration.description
          })
          .then(response => {
            this.petition_id = response.data.petition_id;
            this.step = 3;
          })
          .catch(e => {
            this.$swal(
              "ya existe una solicitud con estos datos!"
            );
          });
      }
    },








    validatevictims() {
      if (this.$refs.formvictima.validate()) {
        axios
          .post("/victims/" + this.petition_id + "/store", {
            name: this.victims.name,
            namepaterno: this.victims.namepaterno,
            namematerno: this.victims.namematerno,
            age: this.victims.age,
            benefits: this.victims.beneficios,
            gender: this.victims.gender,

            addressestado: this.registration.addressestado,
            addressmunicipio: this.registration.addressmunicipio,
            addresscolonia: this.registration.addresscolonia,
            addresscalle: this.registration.addresscalle,
            addressnumero: this.registration.addressnumero,

          })
          .then(response => {
            this.victimas.push(response.data);
            this.victims.name = "",
            this.victims.namepaterno = "",
            this.victims.namematerno = "",
            this.victims.age = null,
            this.victims.gender = null,
            this.victims.beneficios = "",
            this.$refs.formvictima.resetValidation();

            this.$swal("Exito al crear la Victima!");

            axios
              .get("/respaldo/petition/cleartable")
              .then(response => {
                console.log("Eliminado los respaldos guardados");
              })
              .catch(e => {
                console.log("Error al eliminar los respaldos");
              });
          })
          .catch(e => {
            this.$swal(
              "ya existe la victima"
            );
          });
      }
    },





    SeleccionSolicitudAnonima: function (event) {
      if (event) {
        this.registration.anonymous = true;
        this.registration.name = "";
        this.registration.namepaterno = "";
        this.registration.namematerno = "";
        this.registration.age = null;
        this.registration.relationship = null;
        this.registration.gender = null;
        this.registration.numerotelefono = null;
        this.registration.correo = null;

        this.$refs.formsolicitante.resetValidation()

        this.vias = ['Presencial', 'Telefónica'];
        this.step = 2;
      }
    },


    finalizarProceso() {
      if (this.victimas.length < 1) {
        this.$swal("Agregue por lo menos una victima!");
      }
      else {
        swal({
          title: "Deseas salir?",
          text:
            "Una vez finalizes toda la información estara Visible para el ROL correspondiente",
          icon: "warning",
          buttons: true,
          dangerMode: true
        }).then(willDelete => {
          if (willDelete) {
            window.onbeforeunload = null;
            axios
              .post("/petitions/created/mail/" + this.petition_id + "")
              .then(response => {
              })
              .catch(e => {
                console.log("Error al enviar el correo");
              });

            window.location.href = "/petitions";
          }
        });
      }

    },

    actaulizarsolicitante() {
      if (this.$refs.formsolicitante.validate()) {
        axios
          .put("/petitions/" + this.petition_id + "", {
            name: this.registration.name,
            namepaterno: this.registration.namepaterno,
            namematerno: this.registration.namematerno,
            age: this.registration.age,
            relationship: this.registration.relationship,
            gender: this.registration.gender,
            numerotelefono: this.registration.numerotelefono,
            correo: this.registration.correo,
          })
          .then(response => {
            if (this.registration.correo) {
              this.vias = ['Presencial', 'Telefónica', 'Correo'];
            }
            else {
              this.vias = ['Presencial', 'Telefónica'];
            }
            this.step = 2;
          })
          .catch(e => {
            console.log("Error al almacenar la copia de respaldo");
          });
      }
    },
    actualizarsolicitud() {
      if (this.$refs.formsolicitud.validate()) {
        if (this.registration.anonymous == null || !this.registration.anonymous) {
          this.registration.anonymous = "false";
        }

        axios
          .put("/petitions/" + this.petition_id + "", {
            anonymous: this.registration.anonymous,
            name: this.registration.name,
            namepaterno: this.registration.namepaterno,
            namematerno: this.registration.namematerno,
            age: this.registration.age,
            relationship: this.registration.relationship,
            gender: this.registration.gender,
            numerotelefono: this.registration.numerotelefono,
            correo: this.registration.correo,

            via: this.registration.via,
            addressestado: this.registration.addressestado,
            addressmunicipio: this.registration.addressmunicipio,
            addresscolonia: this.registration.addresscolonia,
            addresscalle: this.registration.addresscalle,
            addressnumero: this.registration.addressnumero,
            description: this.registration.description
          })
          .then(response => {
            console.log("Actualizado correctamente");
            this.step = 3;
          })
          .catch(e => {
            console.log("Error al actualizar");
          });
      }
    },
    actualizarvictimasraiz(victima_actualizada) {
      this.victimas = victima_actualizada;
    }
  },
  created() {
    window.onbeforeunload = () => 'Puede perder los datos,desea realmente salir de aqui?'
  },



  beforeCreate() {
    axios
      .post("/respaldo/petition/getsaved")
      .then(response => {
        if (response.data) {
          this.$swal({
            title: "hay datos sin guardar, desea utilizarlos",
            text:
              "Nombre: " + response.data.name,
            icon: "warning",
            buttons: true,
            buttons: ["Comenzar nuevo", "Usar estos datos"],
            dangerMode: true,
          }).then(willDelete => {
            if (willDelete) {
              this.registration.anonymous = response.data.anonymous == "true" ? 1 : 0;
              this.registration.name = response.data.anonymous == "true" ? "" : response.data.name;
              this.registration.namepaterno = response.data.anonymous == "true" ? "" : response.data.namepaterno;
              this.registration.namematerno = response.data.anonymous == "true" ? "" : response.data.namematerno;
              this.registration.age = response.data.age;
              this.registration.relationship = response.data.relationship;
              this.registration.gender = response.data.gender;
              this.registration.numerotelefono = response.data.numerotelefono;
              this.registration.correo = response.data.correo;

              this.registration.via = response.data.via;
              this.registration.addressestado = response.data.addressestado;
              this.registration.addressmunicipio = response.data.addressmunicipio;
              this.registration.addresscolonia = response.data.addresscolonia;
              this.registration.addresscalle = response.data.addresscalle;
              this.registration.addressnumero = response.data.addressnumero;
              this.registration.description = response.data.description;
              this.step = response.data.step;
            }
          });
        }
        else {
          console.log("No existe");
        }
      })
      .catch(e => {
        console.log("error al cargar la persona guardada");
      });
  },
  
};