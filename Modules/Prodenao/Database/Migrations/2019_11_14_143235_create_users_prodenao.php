<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProdenao extends Migration
{
    //USO GLOBAL DE RECURSOS ___ TABLA USUARIOS donde USER_id es un usuario global de la intranet
    public function up()
    {
        Schema::create('pdn_user', function (Blueprint $table) {
            $table->increments('id');

            $table->foreign('user_id')->references('id')->on('usuarios');
            $table->integer('user_id')->unsigned()->index()->nullable();

            $table->foreign('subprocuracy_id')->references('id')->on('pdn_subprocuracies');
            $table->integer('subprocuracy_id')->unsigned()->index()->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('');
    }
}