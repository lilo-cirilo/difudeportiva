<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespaldoPerson extends Model
{
    protected $guarded =[];
    protected $table = 'respaldo_person';
}
