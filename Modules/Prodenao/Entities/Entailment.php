<?php

namespace App;

use App\Advisor;

use Illuminate\Database\Eloquent\Model;

class Entailment extends Model
{
    protected $guarded =[];

    public function advisor()
    {
        return $this->belongsTo('App\Advisor');
    } 

    public function mediation()
    {
        return $this->belongsTo('App\Mediation');
    }

    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    }
}
