<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'dateappointment'
    ];


    protected $guarded =[];
    
    public function mediation()
    {
        return $this->belongsTo('App\Mediation');
    } 

    public function expedient()
    {
        return $this->belongsTo('App\Expedient');
    } 
}
