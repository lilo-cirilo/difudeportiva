<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = [];

    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario');
    }

    public function subprocuracy()
    {
        return $this->belongsTo('Modules\prodenao\Entities\Subprocuracy');
    }

    /* public function affairs()
    {
        return $this->hasMany('App\Affair');
    }

    public function affair()
    {
        return $this->hasOne('App\Affair');
    } 

     public function getAllPermissionsAttribute() {
        $permissions = [];
          foreach (Permission::all() as $permission) {
            if (Auth::user()->can($permission->slug)) {
              $permissions[] = $permission->slug;
            }
          }
          return $permissions;
    } */
}