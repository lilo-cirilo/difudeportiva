<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded =[];
    
    public function petition()
    {
        return $this->belongsTo('App\Petition');
    }
}
