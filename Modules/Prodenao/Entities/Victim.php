<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Victim extends Model
{
    protected $appends = ['namefull'];
    protected $guarded =[];

    public function petition()
    {
        return $this->belongsTo('App\Petition');
    }

    public function actions()
    {
        return $this->belongsToMany('App\Actions');
    }

    function getNameFullAttribute(){
        return  $this->attributes['name'] .' '.
                $this->attributes['namepaterno'] .' '.
                $this->attributes['namematerno'];
    }
    
    function getAddressFullAttribute(){
        return  $this->attributes['addressestado'] . ' ,'.
                $this->attributes['addressmunicipio'] . ' ,'.
                $this->attributes['addresscolonia'] . ' ,'.
                $this->attributes['addresscalle'] . ' #'.
                $this->attributes['addressnumero'] ;
    }

    function getFolioAttribute(){
        return str_pad(  $this->attributes['id'], 6, "0", STR_PAD_LEFT);
    }
}
