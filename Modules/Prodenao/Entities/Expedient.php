<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expedient extends Model
{
    protected $guarded =[];

    public function affair()
    {
        return $this->belongsTo('App\Affair');
    } 
    public function entailments()
    {
        return $this->hasMany('App\Entailment');
    }

    public function actions()
    {
        return $this->hasMany('App\Action');
    }

    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function evidences()
    {
        return $this->hasMany('App\Evidence');
    }

    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

    public function meetings()
    {
        return $this->hasMany('App\Meeting');
    }

    public function accounts()
    {
        return $this->hasMany('App\Account');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }
}
