<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Expedient;

use Illuminate\Http\Request;

class ExpedientController extends Controller
{
    public function show(Petition $petition)
    {
        return view( 'expedients.show' ,compact('petition') );
    }
    public function update(Request $request ,Expedient $expedient)
    {
        $expediente = Expedient::find($expedient->id);
        $expediente->update( ['description' => $request->description] ); 
        return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Actializacion de Expediente Correcta' );
    }
}
