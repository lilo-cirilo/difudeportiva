<?php

namespace App\Http\Controllers;

use App\Expedient;

use App\Affair;
use App\Petition;
use App\Action;
use App\Victim;

use Illuminate\Http\Request;
class ActionController extends Controller
{
    public function store(Request $request, Expedient $expedient )
    {
        $accion = Action::create( ['accion' => $request->accion,
                        'institucion' => $request->institucion,
                        'responsable' => $request->responsable,
                        'expedient_id' => $expedient->id] );

        $accion->victims()->attach($request->beneficiarios);
        return $request;
    }

    public function index(Expedient $expedient)
    {
        $actions = Action::where('expedient_id','=',$expedient->id)->get();

        $acciones_victimas = collect();
        foreach( $actions as $action){
            $action->victims;
        }
        foreach( $actions as $action){
            $action->meetings;
        }
        return $actions;
    }

    public function getMeetings( Action $action )
    {
        return $action->meetings;
    }
    public function getAccounts( Action $action )
    {
        return $action->accounts;
    }
    public function getFiles( Action $action )
    {
        return $action->files;
    }
}
