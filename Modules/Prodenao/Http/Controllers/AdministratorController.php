<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artisan;

class AdministratorController extends Controller
{
    public function makeBackup()
    {
        try {
            Artisan::call('backup:run',['--only-db'=>true]);
            $output = Artisan::output();
            //dd($output);

          } catch (Exception $e) {
            toast('Falla con el servidor intente mas tarde!','top-right');
          }
        toast('Hemos creado el respaldo exitosamente!','success','top-right');
        return back();
    }
}
