<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RespaldoPerson;

class RespaldoPersonController extends Controller
{
    public function getActivePerson()
    {

    }
    public function store( Request $request )
    {
        RespaldoPerson::getQuery()->delete();

        RespaldoPerson::create( $request->all() );
        return "Creado respaldo";
    }
    public function update( Request $request )
    {
        RespaldoPerson::getQuery()->delete();
        $actual = RespaldoPerson::updateOrCreate($request->all());
        return "Actualizado correctamente";
    }

    public function cleartable()
    {
        RespaldoPerson::getQuery()->delete();
        return "Eliminados todos exitosamente";
    }
    public function getsavedperson()
    {
        $persona = RespaldoPerson::get()->first();
        return $persona;
    }
}
