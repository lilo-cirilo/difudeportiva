<?php

namespace App\Http\Controllers;

use App\Affair;
use App\Appointment;
use App\Meeting;
use App\Action;

use DB;

use Auth;
use Carbon\Carbon;

use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function store(Request $request ,Affair $affair)
    {
        $fecha = $request->dateappointment." ".$request->timeappointment;
        $dateappointment = Carbon::createFromFormat('Y-m-d H:i', $fecha);
        $request->merge(['dateappointment' => $dateappointment]);

        $cita = Appointment::create( $request->all() );
        if( $affair->mediation ){
            $cita->update( ['mediation_id' => $affair->mediation->id ]);
        }
        elseif( $affair->expedient ){
            $cita->update( [ 'expedient_id'=> $affair->expedient->id ] );
        }
        $affair->update([ 'step'=>3 ]); 

        return "Creada cita con éxito";
    }

    public function index(){
        $affairs = Affair::where( 'user_id',Auth::user()->id )->get();
        $appointments_finished = collect();
        $appointments = collect();
        foreach( $affairs as $affair){
            if( $affair->mediation)
            {
                $casos = $affair->mediation->appointments;
                if( $casos )
                {
                    foreach( $casos as $cita ){
                        if( $cita->dateappointment > Carbon::now() )
                        {
                            $cita->{"rest"} = $cita->dateappointment->diffForHumans(Carbon::now());
                            $cita->{"format"} = $cita->dateappointment->format('d \\d\\e F Y h:i A');
                            $appointments->push($cita);
                        }
                        else{
                            $cita->{"rest"} = $cita->dateappointment->diffForHumans(Carbon::now());
                            $cita->{"format"} = $cita->dateappointment->format('d \\d\\e F Y h:i A');
                            $appointments_finished->push($cita);
                        }
                    }
                }
            }
            if( $affair->expedient)
            {
                $casos = $affair->expedient->meetings;
                if( $casos )
                {
                    foreach( $casos as $cita ){
                        $cita->push( $cita->expedient->affair->petition );
                        $cita->push( $cita->action );
                        if( $cita->dateappointment > Carbon::now() )
                        {
                            $cita->{"rest"} = $cita->dateappointment->diffForHumans(Carbon::now());
                            $cita->{"format"} = $cita->dateappointment->format('d \\d\\e F Y h:i A');
                            $appointments->push($cita);
                        }
                        else{
                            $cita->{"rest"} = $cita->dateappointment->diffForHumans(Carbon::now());
                            $cita->{"format"} = $cita->dateappointment->format('d \\d\\e F Y h:i A');
                            $appointments_finished->push($cita);
                        }
                    }
                }
            }
        }
        $appointments = $appointments->sortByDesc('dateappointment')->reverse();
        $appointments_finished = $appointments_finished->sortByDesc('dateappointment');
        $today = Carbon::now();
        
        return view("appointments.index" ,compact("appointments","appointments_finished","today"));
    }

    public function calendar()
    {
        return view( "appointments.calendar" );
    }

    public function getactive(Affair $affair){
        $cita = $affair->expedient->appointments->where( "completed",0 )->first();
        return $cita;
    }

    public function savereport(Request $request, Appointment $appointment )
    {
        if ($request->hasFile('reporte')) {
            $url = $request->reporte->store('public/reportsown'); 
            $appointment->update( [ 'url_reporte'=> $url] );
            $appointment->update( [ 'report_complet'=> 1] );
            $appointment->update( [ 'report_name'=> $request->name] );
            return "guardado correctamente";
        }
        return "No Llego alguna imagen";
    }

    public function setstate(Affair $affair){
        $cita = $affair->expedient->appointments->where( "completed",0 )->first();
        $cita->update( [ "completed"=> 1 ] );
        return "cambiado estado de cita";
    }

}
