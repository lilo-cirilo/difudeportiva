<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Victim;
Use Alert;
Use DB;

use Illuminate\Http\Request;

class VictimController extends Controller
{

    public function index()
    {
        $victims = DB::table('victims')
            ->join('petitions', 'petitions.id', '=', 'victims.petition_id')
            ->select('victims.*','petitions.addressestado',
                                'petitions.addressmunicipio',
                                'petitions.addresscolonia',
                                'petitions.addresscalle',
                                'petitions.addressnumero')
            ->get();
        return view( 'victims.index' ,compact('victims') );
    }
    public function create(Petition $petition)
    {
        return view( 'victims.create' ,compact('petition') );
    }

    public function store(Request $request,Petition $petition)
    {
        $slug = $request->name.$request->namepaterno. $request->namematerno. $request->age .$request->gender;
        $request->request->add( ['slug' => $slug] );

        $this->validate($request, [
            'slug' => 'unique:victims',
        ]);

        $tempo = Victim::create($request->all());
        $tempo->update( ['petition_id' => $petition->id] );
        
        return $tempo;
    }
    public function reports(Request $request){
        $victims = DB::table('victims')
            ->join('petitions', 'petitions.id', '=', 'victims.petition_id')
            ->select('victims.*','petitions.addressestado',
                                'petitions.addressmunicipio',
                                'petitions.addresscolonia',
                                'petitions.addresscalle',
                                'petitions.addressnumero')
            ->get();
        if( $request->addressmunicipio ){
        }
        if( $request->gender ){
            $victims = $victims->where('gender','Mujer');
        }
        if( $request->benefits ){
            $victims = $victims->filter(function ($item) {
                return data_get($item, 'id') >= 6;
            });
        }
        return $victims;
    }

    public function update(Request $request, Victim $victim){
        $victim->update( $request->all() );
        $victimas = Victim::where( 'petition_id' , $victim->petition_id )->get();
        return $victimas;
    }

}
