<?php

namespace App\Http\Controllers;

use App\User;
use App\Petition;
use App\Affair;
use Illuminate\Http\Request;
use Caffeinated\Shinobi\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        $users = User::with('subprocuracy')->get();
        return view( 'users.index' , compact( 'users' ) );
    }


    public function show( User $user )
    {
        return view('users.show',compact('user') );
    }

    public function edit( User $user )
    {
        $roles = Role::get();
        return view('users.edit',compact('user', 'roles') );
    }

    public function update(Request $request, User $user)
    {
        $user->update( $request->all() );
        $user->roles()->sync( $request->get( 'roles' ) );
        return redirect()->route( 'users.edit' ,$user->id ) -> with( 'info' ,'Usuario Actaulizado con exito' );
    }

    public function destroy(User $user)
    {
        $contenidos = Affair::where('user_id',$user->id)->get();

        $affairs = collect();
        foreach( $contenidos as $cont ){
            $aux = $cont;
            if( !$cont->petition->completed ){
                $affairs->push( $aux );
            }
        }
        return $affairs;
    }
    public function eliminardelsistema(User $user){

        $contenidos = Affair::where('user_id',$user->id)->get();
        foreach( $contenidos as $contenido ){
            Affair::where('id',$contenido->id)
            ->update( ['user_id'=>10] );

            Petition::where('id',$contenido->petition_id)
            ->update( ['assignedaffair'=>0] );
        }
        $user->delete();
        return "Eliminado";
    }
}
