<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;
use App\Action;

use Image;

class FileController extends Controller
{

    public function index()
    {
    }

    public function store(Request $request,Action $action)
    {
        $attachments = $request->file('attachments');

        foreach( (array)$attachments as $attachment )
        {
            $url = $attachment->store('public/files');
            $evidencie = File::create( ['url' =>$url] );

            $ruta_sin_public = substr($url, 7, strlen($url));
            $link = public_path('storage/'.$ruta_sin_public);
            $img = Image::make($link)->resize(1280, 720)->save($link);
            $evidencie->update( ['action_id'=> $action->id ,
                                'observations'=> $request->observations,
                                'expedient_id' => $action->expedient_id ] );
            
        }
        return "Creado exitosamente";
    }

    public function show($id)
    {
    }
}
