<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Affair;
use App\Evidence;
use Image;

use Illuminate\Http\Request;

class EvidenceController extends Controller
{
    public function store(Request $request,Affair $affair )
    {

        
        $attachments = $request->file('attachments');

        foreach( (array)$attachments as $attachment )
        {
            $url = $attachment->store('public/evidences');
            $evidencie = Evidence::create( ['url' =>$url] );

            $ruta_sin_public = substr($url, 7, strlen($url));
            $link = public_path('storage/'.$ruta_sin_public);
            $img = Image::make($link)->resize(1280, 720)->save($link);
            
            if( $affair->advisor ){
                $evidencie->update( ['advisor_id' => $affair->advisor->id,'observations'=>$request->observations] );
            }
            elseif( $affair->mediation ){
                $evidencie->update( ['mediation_id' => $affair->mediation->id,'observations'=>$request->observations] );
            }
            elseif( $affair->expedient ){
                $evidencie->update( ['expedient_id' => $affair->expedient->id,'observations'=>$request->observations] );
            }
        }
        return "Exito al guardar la evidencia " ;
    }
}
