<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Advisor;

use Illuminate\Http\Request;

class AdvisorController extends Controller
{
    public function show(Petition $petition)
    {
        return view( 'advisors.show' ,compact('petition') );
    }
    public function update(Request $request ,Advisor $advisor)
    {
        $advi = Advisor::find($advisor->id);
        $advi->update( ['description' => $request->description] );
        return redirect()->route( 'petitions.index' ) -> with( 'info' ,'Actializacion de Asesoria Correcta' );
    }
}
