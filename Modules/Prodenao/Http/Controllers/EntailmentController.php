<?php

namespace App\Http\Controllers;

use App\Petition;
use App\Affair;
use App\Entailment;

use Illuminate\Http\Request;

class EntailmentController extends Controller
{
    public function store(Request $request,Affair $affair )
    {

        $vinculacion = Entailment::create( $request->all() );
        if( $affair->advisor ){
            $vinculacion->update( [ 'advisor_id'=> $affair->advisor->id ] );
        }
        elseif( $affair->mediation ){
            $vinculacion->update( [ 'mediation_id'=> $affair->mediation->id ] );
        }
        elseif( $affair->expedient ){
            $vinculacion->update( [ 'expedient_id'=> $affair->expedient->id ] );
        }
        $affair->update([ 'step'=>2 ]);
        return 'Exito al crear el entailment';
    }

    public function getactive(Affair $affair){
        $vinculacion = $affair->expedient->entailments->where( "completed",0 )->first();
        return $vinculacion;
    }

    public function savereport(Request $request, Entailment $entailment )
    {
        if ($request->hasFile('reporte')) {
            $url = $request->reporte->store('public/reportsown'); 
            $entailment->update( [ 'url_reporte'=> $url] );
            $entailment->update( [ 'report_complet'=> 1] );
            $entailment->update( [ 'report_name'=> $request->name] );
            return "guardado correctamente";
        }
        return "Noo Llego una imagen";
    }

    public function setstate(Affair $affair){
        $vinculacion = $affair->expedient->entailments->where( "completed",0 )->first();
        $vinculacion->update( [ "completed"=>1 ] );
        return "cambiado estado de vinculacion";
    }
}
