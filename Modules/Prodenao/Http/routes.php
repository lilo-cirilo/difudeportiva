<?php

Route::group(['middleware' => 'web', 'prefix' => 'prodenao', 'namespace' => 'Modules\Prodenao\Http\Controllers'], function () {
    Route::get('/', 'ProdenaoController@index');
});

Route::get('/', function () {
    return view('welcome')->name('home');
});


Route::get('/home', 'HomeController@index')->name('home');

//Roles
Route::middleware(['auth'])->group(function () {
    //CRUD DE ROLES
    Route::post('roles/store', 'RoleController@store')->name('roles.store')
        ->middleware('permission:roles.create');

    Route::get('roles', 'RoleController@index')->name('roles.index')
        ->middleware('permission:roles.index');

    Route::get('roles/create', 'RoleController@create')->name('roles.create')
        ->middleware('permission:roles.create');

    Route::put('roles/{role}', 'RoleController@update')->name('roles.update')
        ->middleware('permission:roles.edit');

    Route::get('roles/{role}', 'RoleController@show')->name('roles.show')
        ->middleware('permission:roles.show');

    Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy')
        ->middleware('permission:roles.destroy');

    Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit')
        ->middleware('permission:roles.edit');


    //CRUD DE users
    Route::get('users', 'UserController@index')->name('users.index')
        ->middleware('permission:users.index');

    Route::put('users/{user}', 'UserController@update')->name('users.update')
        ->middleware('permission:users.edit');

    Route::get('users/{user}', 'UserController@show')->name('users.show')
        ->middleware('permission:users.show');

    Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy')
        ->middleware('permission:users.destroy');

    Route::any('users/definitive/{user}', 'UserController@eliminardelsistema')->name('users.definitivedestroy')
        ->middleware('permission:users.destroy');

    Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit')
        ->middleware('permission:users.edit');


    //CRUD DE petitions
    Route::post('petitions/store', 'petitionController@store')->name('petitions.store')
        ->middleware('permission:petitions.create');

    Route::get('petitions', 'petitionController@index')->name('petitions.index')
        ->middleware('permission:petitions.index');

    Route::get('petitions/search', 'petitionController@search')->name('petitions.search');

    Route::get('petitions/create', 'petitionController@create')->name('petitions.create')
        ->middleware('permission:petitions.create');

    Route::put('petitions/{petition}', 'petitionController@update')->name('petitions.update')
        ->middleware('permission:petitions.edit');

    Route::any('petitions/finish/{petition}', 'petitionController@updateconclusion')->name('petitions.finish');
    Route::any('affair/finish/{petition}', 'AffairController@updateconclusion')->name('affairs.finish');
    Route::any('affair/demit/{petition}', 'AffairController@demitconclusion')->name('affairs.demited');


    Route::get('appointment/active/{affair}', 'AppointmentController@getactive')->name('cita.active');
    Route::get('entailment/active/{affair}', 'EntailmentController@getactive')->name('vinculacion.active');

    Route::any('petitions/queryperson', 'petitionController@existpersononexistspetitions')->name('petitions.queryperson');

    //RUTAS DE SUBIR EVIDENCIA DE ENTAILMENT Y DE APPOINTMENT
    Route::any('entailment/save/{entailment}', 'EntailmentController@savereport')->name('entailment.savefile');
    Route::any('appointment/save/{appointment}', 'AppointmentController@savereport')->name('appointment.savefile');

    //RUTAS Cambiar los pasos de el AFFAIR
    Route::any('appointment/release/{affair}', 'AppointmentController@setstate')->name('cita.release');
    Route::any('entailment/release/{affair}', 'EntailmentController@setstate')->name('vinculacion.realese');
    Route::any('affair/assignstep/{affair}', 'AffairController@assignstep')->name('vinculacion.asignarpaso');



    Route::get('petitions/{petition}', 'petitionController@show')->name('petitions.show')
        ->middleware('permission:petitions.show');

    Route::delete('petitions/{petition}', 'petitionController@destroy')->name('petitions.destroy')
        ->middleware('permission:petitions.destroy');

    Route::get('petitions/{petition}/edit', 'petitionController@edit')->name('petitions.edit')
        ->middleware('permission:petitions.edit');

    Route::get('section/process', 'petitionController@process')->name('petitionsprocess.index');
    Route::get('section/pendients', 'petitionController@pendients')->name('petitionspendients.index');


    //VICTIMAS ALMACENAMIENTO Y FORMULARIO DE CREACION
    Route::any('victims/{petition}/create', 'victimController@create')->name('victims.create')
        ->middleware('permission:petitions.create');
    Route::any('victims/{petition}/store', 'victimController@store')->name('victims.store')
        ->middleware('permission:petitions.create');
    Route::put('victims/{victim}', 'victimController@update')->name('victims.update');
    Route::get('victims', 'victimController@index')->name('victims.index')
        ->middleware('permission:petitions.create');
    Route::post('victims/reports', 'victimController@reports')->name('victims.reports');



    //RUTAS PARA ASIGNAR AL ABOGADO
    Route::get('subprocuracy/lawers', 'LawerController@index')->name('lawers.index');
    Route::get('petitions/{petition}/editlawer', 'petitionController@editlawer')->name('petitions.editlawer')
        ->middleware('permission:petitions.editlawer');

    //CRUD DE CASOS( AFFAIRS ) //AGREGAR PERMISOS
    Route::post('affairs/store', 'AffairController@store')->name('affairs.store')
        ->middleware('permission:affairs.store');

    //Reasignar Abogado en caso de ser eliminado
    Route::any('affair/reasign/{user?}', 'AffairController@reasignlawer')->name('affairs.reasign');
    Route::any('lawers/get', 'LawerController@getLawers')->name('lawer.get');
    Route::any('lawers/except/{user}', 'LawerController@getLawerExcept')->name('lawer.except');

    //RUTAS PARA Abogado asigne el SEGUIMIENTO
    Route::get('affairs/{petition}/types', 'affairController@types')->name('affairs.types');

    //OBTENER Y EN BASE A LA SELECCION CREAR, ASESORIA, MEDIACION...
    Route::post('affairs/{petition}/define', 'affairController@defineOptionType')->name('affairs.define');


    //PETICIONES******CASO ABOGADO******SEGUIMIENTOS*****************

    //ASESORIA CRUD    
    Route::get('advisors/{petition}', 'AdvisorController@show')->name('advisors.show');
    Route::post('advisors/{advisor}/update', 'AdvisorController@update')->name('advisors.update');

    //Mediacion CRUD 
    Route::get('mediations/{petition}', 'MediationController@show')->name('mediations.show');
    Route::post('mediations/{mediation}/update', 'MediationController@update')->name('mediations.update');

    //Expedientes CRUD    
    Route::get('expedients/{petition}', 'ExpedientController@show')->name('expedients.show');
    Route::post('expedients/{expedient}/update', 'ExpedientController@update')->name('expedients.update');


    //SECCION******CASOS******COMPLEMENTOS*****************

    //VINCULACIONES
    Route::post('Entailments/store/{affair}', 'EntailmentController@store')->name('entailments.store');

    //Reportes
    Route::post('Reports/store/{affair}', 'ReportController@store')->name('reports.store');

    //Evidencias
    Route::post('Evidences/store/{affair}', 'EvidenceController@store')->name('evidences.store');

    //Citas
    Route::post('appointments/store/{affair}', 'AppointmentController@store')->name('appointments.store');
    Route::get('appointments/calendar', 'AppointmentController@calendar')->name('appointments.calendar');
    Route::get('appointments/index', 'AppointmentController@index')->name('appointments.index');

    //Direccion ruta para obtener Municipios y localidades
    Route::get('address/municipios', 'AddressController@indexMunicipios')->name('address.municipios');
    Route::get('address/localidades/{id}', 'AddressController@indexLocalidades')->name('address.municipios');


    Route::get('respaldo', 'AdministratorController@makeBackup')->name('administrador.crear_respaldo');

    //SECCION******CASOS******EXPEDIENTE NUEVA FORMA DE CREARLOS*****************

    Route::post('actions/store/{expedient}', 'ActionController@store')->name('actions.store');
    Route::any('actions/index/{expedient}', 'ActionController@index')->name('actions.index');
    Route::any('actions/meetings/{action}', 'ActionController@getMeetings')->name('action_meetings.index');
    Route::any('actions/accounts/{action}', 'ActionController@getAccounts')->name('action_acount.index');
    Route::any('actions/files/{action}', 'ActionController@getFiles')->name('action_file.index');

    Route::post('meetings/store/{action}', 'MeetingController@store')->name('meetings.store');
    Route::any('meetings/index', 'MeetingController@index')->name('meetings.index');
    Route::any('meeting/update/report/{meeting}', 'MeetingController@updatereport')->name('meetings.updatereport');

    Route::post('accounts/store/{action}', 'AccountController@store')->name('accounts.store');
    Route::any('accounts/index', 'AccountController@index')->name('accounts.index');

    Route::post('files/store/{action}', 'FileController@store')->name('files.store');
    Route::any('files/index', 'FileController@index')->name('files.index');

    Route::any('respaldo/petition/store', 'RespaldoPersonController@store')->name('respaldopetition.store');
    Route::any('respaldo/petition/update', 'RespaldoPersonController@update')->name('respaldopetition.update');
    Route::any('respaldo/petition/getsaved', 'RespaldoPersonController@getsavedperson')->name('respaldopetition.get');
    Route::any('respaldo/petition/cleartable', 'RespaldoPersonController@cleartable')->name('respaldopetition.cleartable');

    Route::any('petitions/created/mail/{petition}', 'PetitionController@sendmail')->name('petition.mail');
    Route::any('petitions/assign/mail/{petition}', 'PetitionController@assignmail')->name('petition.assingmail');
    Route::any('petitions/reassign/mail/{petition}', 'PetitionController@reassignmail')->name('petition.reassingmail');
});