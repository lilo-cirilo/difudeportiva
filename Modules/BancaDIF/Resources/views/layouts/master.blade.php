<!DOCTYPE html>

<html lang="es">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
    <title>Fondo Rotatorio </title>
    <!-- Main styles for this application-->
    {{--  <link href="{{ Module::asset('bancadif:css/app.css') }}" rel="stylesheet">  --}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('bancadif/css/app.css') }}" rel="stylesheet">
  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    
    @include('bancadif::layouts.includes.navbar')

    <div class="app-body">
      
      @include('bancadif::layouts.includes.sidebar')
      
      <main class="main">
          @include('bancadif::layouts.includes.breadcrumb')

        <div class="container-fluid">
          <div class="animated fadeIn">

            @yield('content')
          
          </div>
        </div>

      </main>
    </div>
    <footer class="app-footer">
      <div>
        <span>DIF Oaxaca &copy; Copyright 2019.</span>
      </div>
      <div class="ml-auto">
        <span>Unidad de Informática v1.0.1  r12112019</span>
      </div>
    </footer>
    <script>
      sessionStorage.setItem('bancadif_token', '{{$bancadif_token}}');
      sessionStorage.setItem('area_id', '{{Auth::user()->persona->empleado->area_id}}');
      sessionStorage.setItem('usuario_id', '{{Auth::user()->id}}');
      sessionStorage.setItem('reporter_url', '{{config("app.reporter_url")}}');
      sessionStorage.setItem('roles', '@json($roles)');
    </script>
    {{--  <script src="{{ Module::asset('bancadif:js/app.js') }}"></script>  --}}
    <script src="{{ asset('bancadif/js/app.js') }}"></script>
  </body>
</html>
