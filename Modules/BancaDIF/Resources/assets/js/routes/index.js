import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'

import Inicio from '../components/Inicio.vue'
import Home from '../components/Home.vue'
import AgregarArea from '../components/AgregarArea.vue'
import CrearEnlace from '../components/CrearEnlace.vue'
import AdminFinancieros from '../components/AdminFinancieros.vue'
import AdminEnlace from '../components/AdminEnlace.vue'
import AgregarAsignacion from '../components/AgregarAsignacion.vue'
import CrearVale from '../components/CrearVale'
import CrearViatico from '../components/CrearViatico'
import CrearGasto from '../components/CrearGasto'
import Viaticos from '../components/Viaticos'
import EnlacesFinancieros from '../components/EnlacesFinancieros'
import ConsultaTramites from '../components/ConsultaTramites'
import SubirComprobacion from '../components/SubirComprobacion'
import VistaComprobacion from '../components/VistaComprobacion'
import ComprobacionFinancieros from '../components/ComprobacionFinancieros'
import NoAutorizado from '../components/403'
import PagosCaja from '../components/PagosCaja'
import PagosBanco from '../components/PagosBanco'
import CrearTramite from '../components/ext/CrearTramite'
import Reportes from '../components/Reportes'
import PagoDetalle from '../components/PagoDetalle'
import Proveedores from '../components/Proveedores'
import Reintegros from '../components/Reintegros'
// Vue.component('example-component', require('./../components/Home.vue').default)

const router = new VueRouter({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Inicio,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'FINANCIERO', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/home',
      name: 'index',
      component: Home,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            next('/home')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'FINANCIERO'])){
          next('/enlaces');
          return;
        }
        next();
      }
    },
    {
      path: '/agregarArea',
      name: 'AgregarArea',
      component: AgregarArea
    },
    {
      path: '/crearenlace',
      name: 'CrearEnlace',
      component: CrearEnlace
    },
    {
      path: '/adminfinan/:id',
      name: 'AdminFinancieros',
      component: AdminFinancieros,
      props: true
    },
    {
      path: '/agregarasignacion',
      name: 'AgregarAsignacion',
      component: AgregarAsignacion
    },
    {
      path: '/adminenlace',
      name: 'AdminEnlace',
      component: AdminEnlace,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/adminenlace')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'FINANCIERO'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/crearvale',
      name: 'CrearVale',
      component: CrearVale,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/crearviatico',
      name: 'CrearViatico',
      component: CrearViatico,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/crearviatico')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/creargasto',
      name: 'CrearGasto',
      component: CrearGasto,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/viaticos',
      name: 'Viaticos',
      component: Viaticos
    },
    {
      path: '/enlaces',
      name: 'EnlacesFinancieros',
      component: EnlacesFinancieros,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'FINANCIERO'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/tramites/:tipopago',
      name: 'ConsultaTramites',
      component: ConsultaTramites,
      props: true,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/tramites/:idpago/comprobacion',
      name: 'SubirComprobacion',
      component: SubirComprobacion,
      props: true,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/comprobacion/:idcomprobacion',
      name: 'VistaComprobacion',
      component: VistaComprobacion,
      props: true,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'ENLACE', 'CAPTURISTA','PRESUPUESTAL'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/403',
      name: 'NoAutorizado',
      component: NoAutorizado
    },
    {
      path: '/presupuesto/comprobacion/:tipo',
      name: 'ComprobacionFinancieros',
      component: ComprobacionFinancieros,
      props: true,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next(`${to.path}`)
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'FINANCIERO','PRESUPUESTAL'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/caja',
      name: 'PagosCaja',
      component: PagosCaja,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/caja')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'FINANCIERO','CAJA'])){
          return next('/403');
        }
        next();
      }
		},
		{
      path: '/bancos',
      name: 'PagosBanco',
      component: PagosBanco,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/bancos')
          })
        }
        else if(!store.getters.has(['ADMINISTRADOR','BANCOS','FINANCIERO'])){
          return next('/403');
        }
        next();
      }
		},
    {
      path: '/creartramite',
      name: 'CrearTramite',
      component: null
		},
		{
      path: '/reportes',
      name: 'Reportes',
      component: Reportes,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/reportes')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR', 'FINANCIERO','CAJA'])){
          return next('/403');
        }
        next();
      }
    },
		{
      path: '/pagodetalle',
      name: 'PagoDetalle',
      component: PagoDetalle,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/pagodetalle')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR'])){
          return next('/403');
        }
        next();
      }
		},
		{
      path: '/proveedores',
      name: 'Proveedores',
      component: Proveedores,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/proveedores')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR','FINANCIERO'])){
          return next('/403');
        }
        next();
      }
    },
    {
      path: '/reintegros',
      name: 'Reintegros',
      component: Reintegros,
      beforeEnter: (to, from, next) => {
        if(!store.getters.loadedRoles){
          Vue.nextTick(function () {
            return next('/reintegros')
          })
        }
        else if(! store.getters.has(['ADMINISTRADOR','CAJA'])){
          return next('/403');
        }
        next();
      }
    },
  ],
});

// router.beforeEach((to, from, next) => {
//   if (to.meta.requiresFINANCIERO){
//     if(store.getters.has(['FINANCIERO'])){
//       next();
//     }
//     else {
//       next('/403');
//     }
//   }
//   else{
//     next();
//   }

// });

export default router;