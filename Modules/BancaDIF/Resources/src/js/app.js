import Vue from 'vue'
import {ServerTable, ClientTable, Event} from 'vue-tables-2'
import Multiselect from 'vue-multiselect'
import Notifications from 'vue-notification'
import VeeValidate, { Validator } from 'vee-validate'
import VueRouter from 'vue-router'
import es from 'vee-validate/dist/locale/es'

import router from './routes'
import store from './store'

try {
  window.$ = window.jQuery = require('jquery')
  window.convertir = require('numero-a-letras')
} catch (e) {
  console.log(e);
}

require('bootstrap')
require('toastr/build/toastr.min.css');
require('perfect-scrollbar/dist/perfect-scrollbar.min.js')
require('@coreui/coreui/dist/js/coreui.min.js')
import 'vue-multiselect/dist/vue-multiselect.min.css'

Vue.use(VueRouter)
Vue.use (ClientTable , { theme: 'bootstrap4',template: 'default' })
Vue.use(VeeValidate,{
  classes: true,
  classNames: {
    valid: "is-valid",
    invalid: "is-invalid"
  }
})
Vue.use(Notifications)
Validator.localize('es', es)
Vue.component('multiselect', Multiselect)


const app = new Vue({
  el: '#app',
  store,
  router,
  created(){
    store.dispatch('fetchRoles');
  },
})