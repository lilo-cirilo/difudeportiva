
var myMixin = {
  created: function () {
    this.hello()
  },
  methods: {
    hello: function () {
      console.log('hello from mixin!')
    },
    buscarEmpleado(search){
      if(search.length < 3) return;
      axios.get(`/fondorotatorio/empleados/${search}`).then(response => { 
        return response.data.data.map(item => {
          item.nombre_completo = `${item.persona.nombre} ${item.persona.primer_apellido} ${item.persona.segundo_apellido}`
          return item;
        });
      }).catch(error => { console.log(error); return []; });
    },
    buscarPrograma(search, area_id){
      if(search.length < 3) return;
      let url = `/fondorotatorio/programas/${search}`;
      if(area_id == 15) url += '/true';
      axios.get(url).then(response => { 
        return response.data.data;
      }).catch(error => { console.log(error); return []; });
    }
  }
}

export default myMixin