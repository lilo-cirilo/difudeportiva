<?php

namespace Modules\BancaDIF\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PaymentsTest extends TestCase
{

    /**
     * A store viatico estatal test.
     * @group viatico
     * @return void
     */
    public function test_store_a_new_viatico_estatal_payment()
    {
        $response = $this->withHeaders([
            'bancadif-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvX2lkIjoxLCJtb2R1bG9faWQiOjI1fQ.AXTFceM527rKcJjyUAsdUYv4mksNKSZJOzXAc7jcAwg',
        ])->json('POST', '/fondorotatorio/pagos', [
            'asignacion_id' => 1,
            'solicitante_empleado_id'=> 175,
            'monto_total'=> 1700,
            'descripcion'=> 'VIATICO ESTATAL',
            'metodopago_id'=> null,
            'tipopago_id'=> 1,
            'tramite'=> 1,
            'autorizo'=> 50,
            'vobo'=> 42,
            'programa_id'=> 1,
            'detalle'=>  [
                ['partida_id'=> 121,'numero'=> 374,'etiqueta'=> '374 - VIATICOS EN EL PAIS','monto'=> 700],
                ['partida_id'=> 117,'numero'=> 370,'etiqueta'=> 'PASAJES TERRESTRES','monto'=> 1000]],
            'estado_programa_id'=> 1,
            'viatico'=> true,
            'estatal'=> true,
            'clavepresupuestal_id'=> 6,
            'fecha_salida'=> '2019-06-20',
            'fecha_regreso'=> '2019-06-30',
            'transporte_id'=> 4,
            'placas'=> 'TKY5696',
            'gastos_alimentarios'=> false,
            'num_comidas'=> 0,
            'origen_municipio_id'=> 67,
            'destino_municipio_id'=> 184,
            'localidad'=> [
                'id'=> 4419,
                'nombre'=> 'SAN JUAN BAUTISTA TUXTEPEC',
                'municipio_id'=> 184,
                'latitud'=> '18.086111',
                'longitud'=> '-96.123889',
                'cve_localidad'=> '0001',
                'created_at'=> '2018-05-25 10:05:38',
                'updated_at'=>'2018-05-25 10:05:38',
                'deleted_at'=>null],
            'localidad_id'=>4419,
            'fondo_id' => 1]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
    
    /**
     * A store viatico nacional test.
     *
     * @return void
     */
    public function test_store_a_new_viatico_naional_payment()
    {
        $response = $this->withHeaders([
            'bancadif-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvX2lkIjoxLCJtb2R1bG9faWQiOjI1fQ.AXTFceM527rKcJjyUAsdUYv4mksNKSZJOzXAc7jcAwg',
        ])->json('POST', '/fondorotatorio/pagos', [
            'solicitante_empleado_id' => 82,
            'monto_total' => 2500,
            'descripcion' => 'VIATICO NACIONAL',
            'metodopago_id' => null,
            'tipopago_id' => 1,
            'tramite' => 1,
            'autorizo' => 50,
            'vobo' => 42,
            'programa_id' => 1,
            'detalle' => [
                ['partida_id' => 121, 'numero' => 374, 'etiqueta' => '374 - VIATICOS EN EL PAIS', 'monto' => 1500],
                ['partida_id' => 117, 'numero' => 370, 'etiqueta' => 'PASAJES TERRESTRES', 'monto' => 1000]],
            'estado_programa_id' => 1,
            'viatico' => true,
            'estatal' => false,
            'clavepresupuestal_id'=> 6,
            'clave' => 6,
            'fecha_salida' => '2019-06-24',
            'fecha_regreso' => '2019-06-25',
            'transporte_id' => 2,
            'placas' => null,
            'gastos_alimentarios' => false,
            'num_comidas' => 0,
            'destinociudad' => 'cdmx',
            'destinoentidad_id' => 68,
            'origenentidad_id' => 20,
            'origenregion_id' => 8,
            'fondo_id' => 1]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
    
    /**
     *  A store vale test..
     *
     * @return void
     */
    public function test_store_a_new_vale_payment()
    {
        $response = $this->withHeaders([
            'bancadif-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvX2lkIjoxLCJtb2R1bG9faWQiOjI1fQ.AXTFceM527rKcJjyUAsdUYv4mksNKSZJOzXAc7jcAwg',
        ])->json('POST', '/fondorotatorio/pagos', [
            'solicitante_empleado_id' => 82,
            'monto_total' => 1000,
            'descripcion' => 'VALE PARA COMPRA',
            'metodopago_id' => null,
            'tipopago_id' => 2,
            'autorizo' => 50,
            'vobo' => 42,
            'programa_id' => 1,
            'detalle' => [['partida_id' => 278,'partida' => '205 - MATERIALES, UTILES Y EQUIPOS MENORES DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACIONES','monto' => '1000']],'estado_programa_id' => 1,
            'fondo_id' => 1]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }

    /**
     *  A store gasto test.
     *
     * @return void
     */
    public function test_store_a_new_gasto_payment()
    {
        $response = $this->withHeaders([
            'bancadif-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvX2lkIjoxLCJtb2R1bG9faWQiOjI1fQ.AXTFceM527rKcJjyUAsdUYv4mksNKSZJOzXAc7jcAwg',
        ])->json('POST', '/fondorotatorio/pagos', [
            'solicitante_empleado_id' => 82,
            'monto_total' => 308.04,
            'descripcion' => 'COMIDA DEL DIA',
            'metodopago_id' => 1,
            'tipopago_id' => 3,
            'autorizo' => 50,
            'vobo' => 42,
            'programa_id' => 1,
            'detalle' => [['partida_id' => 125, 'monto' => 308.04]],
            'estado_programa_id' => 1,
            'viatico' => false,
            'estatal' => true,
            'clavepresupuestal_id'=> 6,
            'origen_municipio_id' => 67,
            'destino_municipio_id' => 67,
            'fecha_salida' => '2019-06-20',
            'fecha_regreso' => '2019-06-21',
            'localidad' => ['id' => 2376, 'nombre' => 'OAXACA DE JUÁREZ', 'municipio_id' => 67, 'latitud' => '17.067778', 'longitud' => '-96.720000', 'cve_localidad' => '0001', 'created_at' => '2018-05-25 10:05:34', 'updated_at' => '2018-05-25 10:05:34', 'deleted_at' => null],
            'localidad_id' => 2376,
            'gastos_alimentarios' => true,
            'num_comidas' => '3',
            'fondo_id' => 1]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
}
