<?php

namespace Modules\BancaDIF\Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploadTest extends TestCase
{
    /**
     * A basic test example.
     * @group files
     * @return void
     */
    public function test_pago_file_upload()
    {
        Storage::fake('local');
        $file = UploadedFile::fake()->create('public/bancadif/pagos/document2.pdf', 1024);

        $response = $this->withHeaders([
            'bancadif-token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c3VhcmlvX2lkIjoxLCJtb2R1bG9faWQiOjI1fQ.AXTFceM527rKcJjyUAsdUYv4mksNKSZJOzXAc7jcAwg',
        ])->json('POST', '/fondorotatorio/comprobacion/pagos/185/archivos', [
            'file' => $file
        ]);

        // Assert the file was stored...
        Storage::disk('local')->assertExists("public/bancadif/pagos/{$file->hashName()}");

        $response->assertStatus(201);
        // Assert a file does not exist...
        // Storage::disk('comprobaciones')->assertMissing('document.pdf');
    }
}
