<?php

namespace Modules\BancaDIF\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PagoCreateRequest extends FormRequest
{
    protected $redirectAction = '\Modules\BancaDIF\Http\Controllers\PagosController@mostrarErrores';
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = request();
        if($request->tipopago_id == 1){
            \Log::debug($request->all());
            //reglas para viatico
            return [
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required|numeric',
                'descripcion' => 'required|string',
                'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required|numeric',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'num_tarjeta' => 'required_if:transferencia,true|numeric',
                // 'banco_id' => 'required_if:transferencia,true',
                'estado_programa_id' => 'required|integer',
                'clave' => 'required|integer',
                'estatal' => 'required|boolean',
                'origen_municipio_id' => 'required_if:estatal,true|integer',
                'destino_municipio_id' => 'required_if:estatal,true|integer',
                'origenentidad_id' => 'required_if:estatal,false|integer',
                'destinoentidad_id' => 'required_if:estatal,false|integer',
                'origenregion_id' => 'required_if:estatal,false|integer',
                'destinociudad' => 'required_if:estatal,false',
                'fecha_salida' => 'required|date',
                'fecha_regreso' => 'required|date',
                'localidad' => 'required_if:estatal,true',
                'transporte_id' => 'required|integer',
                'placas' => 'required_if:transporte_id,3',
            ];
        }
        if($request->tipopago_id == 2){
            //reglas para vale
            return [
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required|numeric',
                'descripcion' => 'required|string',
                'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array|size:1',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required|numeric',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'num_tarjeta' => 'required_if:transferencia,true|numeric',
                // 'banco_id' => 'required_if:transferencia,true',
                'estado_programa_id' => 'required|integer',
                'clave' => 'required|integer'
            ];
        }
        if($request->tipopago_id == 3){
            return [
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required|numeric',
                'descripcion' => 'required|string',
                'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required|numeric',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'num_tarjeta' => 'required_if:transferencia,true|numeric',
                // 'banco_id' => 'required_if:transferencia,true',
                'estado_programa_id' => 'required|integer',
                'clave' => 'required|integer',
                'estatal' => 'required|boolean',
                'origen_municipio_id' => 'required|integer',
                'destino_municipio_id' => 'required|integer',
                'fecha_salida' => 'required|date',
                'fecha_regreso' => 'required|date',
                'localidad' => 'required',
                'num_comidas' => 'required|integer',
            ];
        }
    }

    public function response(array $errors){
        return new JsonResponse(['data' => null, 'message' => $errors], 422);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
