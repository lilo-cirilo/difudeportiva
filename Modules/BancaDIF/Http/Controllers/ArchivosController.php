<?php

namespace Modules\BancaDIF\Http\Controllers;

use Validator;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Modules\BancaDIF\Traits\Util;

use Modules\BancaDIF\Entities\Archivo;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\Reintegro;
use Modules\BancaDIF\Entities\Proveedor;

class ArchivosController extends Controller
{
    use Util;

    public function obtenerArchivo($idpago)
    {
        $pago = Pago::with(['detalles.archivos', 'detalles.partida', 'detalles.reintegros', 'estatuslast', 'tipo', 'empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'viatico'])->find($idpago);
        if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago.', 404);

        // if($pago->tipo->nombre == 'VIATICO' || $pago->tipo->nombre == 'GASTOS EN ALIMENTACIÓN') $pago->load(['empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'viatico']);

        $estatusPagado = $this->estatusBancaDIF('PRE-COMPROBADO');
        $pago->estatusPagado = $estatusPagado;

        return $this->jsonResponse($pago, 'Ok');
    }

    public function subirArchivo(Request $request, $idpago, $idpartida)
	{
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:8000|mimes:pdf',
            'partida' => 'required|integer',
            'beneficiario' => 'required|string',
            'folio' => 'required',
            'fecha' => 'required|date',
            'importe' => 'required|numeric',
            'reintegro' => 'numeric',
            'isr' => 'required|numeric',
        ]);
        if ($validator->fails()) { \Log::debug($validator->errors()); return $this->jsonResponse(null, $validator->errors(), 480); }

        $pago = Pago::with('estatuslast')->find($idpago);
        if(!$pago){ return $this->jsonResponse(null, 'No se encontro el pago', 404); }

        $estatusPagado = $this->estatusBancaDIF('PAGADO');
        $estatusRechazado = $this->estatusBancaDIF('RECHAZADO');

        if($pago->estatuslast->estado_programa_id != $estatusPagado[0]['id'] && $pago->estatuslast->estado_programa_id != $estatusRechazado[0]['id'] )
            return $this->jsonResponse(null, 'El estatus debe ser PAGADO para subir su comprobación', 480);

        if(isset($request->reintegro) && $this->esGastoComprobar($pago) === true)
            return $this->jsonResponse(null, 'Este es un Gasto a Comprobar, no puedes agregarle un Reintegro', 403);

        $detalle = $pago->detalles->where('partida_id', $request->partida)->first();
        if($detalle == null) return $this->jsonResponse(null, 'No se encontro la partida en este pago', 404);

        try{
            DB::beginTransaction();

            $cantidadReal = $request->importe;
            if(isset($request->reintegro)) {
                $reintegro = Reintegro::create([
                    'pago_id' => $pago->id,
                    'folio' => $this->folioReinegro(),
                    'cantidad' => $request->reintegro,
                    'detalle_id' => $detalle->id,
                    'usuario_id' => $request->usuario_id
                ]);
                if($detalle->partida_id == 121)
                    $cantidadReal -= $request->reintegro;

                $fondo = $pago->fondo;
                $fondo->saldo_disponible += $request->reintegro;
                $fondo->save();
            }

            $file = $request->file('file');
            $path = $file->store('public/bancadif/pagos');

            if($request->tipo == 'PROVEEDOR'){
                $proveedor = Proveedor::where('rfc', $request->rfc)->first();
            }else {
                $proveedor = Proveedor::firstOrCreate(['rfc' => $request->rfc], ['nombre' => $request->beneficiario, 'tipo' => 'EMPLEADO']);
            }

            if(!$proveedor) return $this->jsonResponse(null, 'No encontramos el proveedor.', 404);

            $archivo = Archivo::create([
                'url' => $path,
                'nombre' => $file->getClientOriginalName(),
                'mime' => $file->getClientMimeType(),
                'size' => $file->getClientSize(),
                'usuario_id' => $request->usuario_id,
                'beneficiario' => $request->beneficiario,
                'folio_comprobante' => $request->folio,
                'fecha' => $request->fecha,
                'importe' => $cantidadReal,
                'isr' => $request->isr,
                'detallespago_id' => $detalle->id,
                'proveedor_id' => $proveedor->id
            ]);

            DB::commit();

            $pago->load('detalles.archivos', 'detalles.partida', 'detalles.reintegros', 'empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'empleado.area', 'fondo.historialenlace.empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'viatico');
            $estatusPagado = $this->estatusBancaDIF('PRE-COMPROBADO');
            $pago->estatusPagado = $estatusPagado;

            $pago->reintegro = $reintegro ?? null;
            $pago->direccion = DB::select('call getDireccion(?)', [$pago->fondo->area_id])[0] ?? 'Dirección no encontrada';

            return $this->jsonResponse($pago, 'Archivo guardado correctamente.', 201);
        }catch(\Exception $e){
            \Log::debug($e);
            DB::rollBack();

            return $this->jsonResponse(null, $e->getMessage(), 500);
        }
    }

    public function subirSinArchivo(Request $request, $idpago, $idpartida) {
      $validator = Validator::make($request->all(), [
        'partida' => 'required|integer',
        'beneficiario' => 'required|string',
        'folio' => 'required',
        'fecha' => 'required|date',
        'importe' => 'required|numeric',
        'reintegro' => 'numeric',
        'isr' => 'required|numeric',
      ]);
      if ($validator->fails()) { \Log::debug($validator->errors()); return $this->jsonResponse(null, $validator->errors(), 480); }

      $pago = Pago::with('estatuslast')->find($idpago);
      if(!$pago){ return $this->jsonResponse(null, 'No se encontro el pago', 404); }

      $estatusPagado = $this->estatusBancaDIF('PAGADO');
      $estatusRechazado = $this->estatusBancaDIF('RECHAZADO');

      if($pago->estatuslast->estado_programa_id != $estatusPagado[0]['id'] && $pago->estatuslast->estado_programa_id != $estatusRechazado[0]['id'] ){ return $this->jsonResponse(null, 'El estatus debe ser PAGADO para subir su comprobación', 480); }

      if(isset($request->reintegro) && $this->esGastoComprobar($pago) === true)
        return $this->jsonResponse(null, 'Este es un Gasto a Comprobar, no puedes agregarle un Reintegro', 403);

      $detalle = $pago->detalles->where('partida_id', $request->partida)->first();
      if($detalle == null) return $this->jsonResponse(null, 'No se encontro la partida en este pago', 404);

      try{
        DB::beginTransaction();

        $cantidadReal = $request->importe;
        if(isset($request->reintegro)) {
            $reintegro = Reintegro::create([
                'pago_id' => $pago->id,
                'folio' => $this->folioReinegro(),
                'cantidad' => $request->reintegro,
                'detalle_id' => $detalle->id,
                'usuario_id' => $request->usuario_id
            ]);
            if($detalle->partida_id == 121)
                $cantidadReal -= $request->reintegro;

            $fondo = $pago->fondo;
            $fondo->saldo_disponible += $request->reintegro;
            $fondo->save();
        }

        if($request->tipo == 'PROVEEDOR'){
            $proveedor = Proveedor::where('rfc', $request->rfc)->first();
        }else {
            $proveedor = Proveedor::firstOrCreate(['rfc' => $request->rfc], ['nombre' => $request->beneficiario, 'tipo' => 'EMPLEADO']);
        }

        if(!$proveedor) return $this->jsonResponse(null, 'No encontramos el proveedor.', 404);

        $archivo = Archivo::create([
            'url' => '',
            'nombre' => '',
            'mime' => '',
            'size' => 0,
            'usuario_id' => $request->usuario_id,
            'beneficiario' => $request->beneficiario,
            'folio_comprobante' => $request->folio,
            'fecha' => $request->fecha,
            'importe' => $cantidadReal,
            'isr' => $request->isr,
            'detallespago_id' => $detalle->id,
            'proveedor_id' => $proveedor->id
        ]);

        DB::commit();

        $pago->load('detalles.archivos', 'detalles.partida', 'detalles.reintegros', 'empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'empleado.area', 'fondo.historialenlace.empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'viatico');
        $estatusPagado = $this->estatusBancaDIF('PRE-COMPROBADO');
        $pago->estatusPagado = $estatusPagado;

        $pago->reintegro = $reintegro ?? null;
        $pago->direccion = DB::select('call getDireccion(?)', [$pago->fondo->area_id])[0] ?? 'Dirección no encontrada';

        return $this->jsonResponse($pago, 'Archivo guardado correctamente.', 201);
      }catch(\Exception $e){
        \Log::debug($e);
        DB::rollBack();

        return $this->jsonResponse(null, $e->getMessage(), 500);
      }
    }

    public function eliminarArchivo($idpago, $idarchivo)
    {
        $pago = Pago::with('estatuslast')->find($idpago);
        if(!$pago){ return $this->jsonResponse(null, 'No se encontro el pago', 404); }

        $archivo = Archivo::find($idarchivo);
        if(!$archivo){ return $this->jsonResponse(null, 'No se encontro el archivo indicado', 404); }

        $estatusPagado = $this->estatusBancaDIF('PAGADO');
        $estatusRechazado = $this->estatusBancaDIF('RECHAZADO');
        if($pago->estatuslast->estado_programa_id != $estatusPagado[0]['id'] && $pago->estatuslast->estado_programa_id != $estatusRechazado[0]['id']){ return $this->jsonResponse(null, 'El estatus debe ser PAGADO para subir su comprobación', 480); }

        try{
            DB::beginTransaction();

            $file = Storage::delete($archivo->url);
            $archivo->delete();

            DB::commit();
            return $this->jsonResponse($archivo, 'Archivo eliminado correctamente.', 200);
        }catch(\Exception $e){
            \Log::debug($e);
            DB::rollBack();
            return $this->jsonResponse(null, $e->getMessage(), 500);
        }
    }

    public function eliminarReintegro($idreintegro)
    {
        $reintegro = Reintegro::find($idreintegro);
        if(!$reintegro) return $this->jsonResponse(null, 'No se encontro el reintegro', 404);

        try{
            DB::beginTransaction();

            $fondo = $reintegro->pago->fondo;
            $fondo->saldo_disponible -= $reintegro->cantidad;
            $fondo->save();

            $reintegro->delete();

            DB::commit();
            return $this->jsonResponse(null, 'Reintegro eliminado correctamente');
        }catch(\Exception $e){
            DB::rollBack();
            return $this->jsonResponse(null, $e->getMessage(), 500);
        }
    }
}
