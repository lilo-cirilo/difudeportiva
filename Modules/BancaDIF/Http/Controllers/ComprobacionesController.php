<?php

namespace Modules\BancaDIF\Http\Controllers;

use App\Models\Area;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Modules\BancaDIF\Entities\Comprobaciones;
use Modules\BancaDIF\Entities\Estatus;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use Modules\BancaDIF\Entities\ComprobacionesPagos;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\Viatico;

use Modules\BancaDIF\Traits\Util;
use App\Models\Empleado;
use Modules\BancaDIF\Entities\AsignacionPago;
use Modules\BancaDIF\Entities\ComprobacionCaja;
use Modules\BancaDIF\Entities\ComprobacionCajaHistorial;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Reintegro;

class ComprobacionesController extends Controller
{
    use Util;

    public function __construct()
    {
        $this->middleware('auth', ['only' => 'index']);
        $this->middleware('rolModuleV2:fondorotatorio,FINANCIERO,ENLACE,ADMINISTRADOR,CAPTURISTA', ['only' => 'index']);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {			
			// return $request->all();
        $estatusList = $this->estatusBancaDIF();
        try{
            DB::beginTransaction();
            $numero = Comprobaciones::withTrashed()->count() + 1;
            $tipoAsignacion = TiposAsignaciones::where('nombre','RECUPERACION')->first()->id;
            
            $comprobacion = Comprobaciones::create([
                'folio' => "COMP/$numero/2019",
                'fondorotatorio_id' => $request->listComprobaciones[0]['fondo_id'],
                'tipo_asignacion_id' => $tipoAsignacion,
                'usuario_id' => auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
                'estado_modulo_id' => $estatusList->where('estatus','NUEVO')->first()['id'],
                'monto_total' => $request->sumatoria,
                'tipopago_id' => $request->tipo ? $request->tipo : 1, // pendiente, como saber que tipo de formato se va ah imprimir, cuando es un gasto ah comprobar
            ]);
        
            foreach($request->listComprobaciones as $comp){
               $pagocomp = ComprobacionesPagos::create([
                'pago_id' =>$comp['id'],
                'comprobacion_id' =>$comprobacion->id ,
               ]);

               $pago = EstadoPago::create([
                'pago_id' => $comp['id'],
                'estado_programa_id' => $estatusList->where('estatus','ENVIADO A COMPROBACION')->first()['id'],
                'fecha' =>  new \Carbon\Carbon,
                'usuario_id' =>   auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
               ]);
            }
						DB::commit();
						$usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'siaf_comprobaciones',
                'registro' => $comprobacion->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method()    
            ]);
            return new JsonResponse(['data' => $comprobacion, 'message' => "Se guardo correctamente"], 200);
        }catch(\Exception $e){
					\Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 422);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bancadif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
		{
			$comprobacion = Comprobaciones::with('estatus.estatus','asignacion','pagos.estatuslast.estadoModulo.estatus','pagos.detalles.archivos','pagos.detalles.archivos.proveedor','pagos.detalles.partida','usuario.persona.empleado.nivel','pagos.observaciones', 'caja')->find($id);
			$fondo = FondoRotatorioArea::with('area','historialenlace')->find($comprobacion->fondo->id);
			$comprobacion->respFondo = $fondo->historialenlace->empleado->persona->nombre.' '.$fondo->historialenlace->empleado->persona->primer_apellido.' '.$fondo->historialenlace->empleado->persona->segundo_apellido;
			$comprobacion->areaFondo = $fondo->area->nombre;
			
			// $area = DB::select('call getAreas(?)',[$comprobacion->fondo->area_id])[0];
			$direccion = DB::select('call getPadreSiaf(?)',[$comprobacion->fondo->area_id])[0];
			// $direccion = DB::select('call getDireccion(?)',[$comprobacion->fondo->area_id])[0];
			$comprobacion->unidad_ejecutora = Viatico::with('clave')->where('pago_id',$comprobacion->pagos[0]['id'])->first();
			foreach ($comprobacion->pagos as $p) {
				
				// $reintegro = Reintegro::where('pago_id',$p->id)->get()->sum('cantidad');
				$p->reintegro = Reintegro::where('pago_id',$p->id)->get()->sum('cantidad');
					foreach ($p->detalles as $a) {
						$a->area = DB::select('call getAreas(?)',[Empleado::find($p->solicitante_empleado_id)->area_id ])[0];
					}
			}

			return new JsonResponse(['data' =>$comprobacion ,'direccion'=>$direccion,'message' => "tudo bem"], 200);
    }
    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function lista($idarea)
    {
			$area = Area::find($idarea);
			if($area == null) return new JsonResponse(['data' => null, 'message' => "No se encontro el area con ID: $idarea"], 404);
			$fondo = $this->areaFondo($idarea)->id;
			$list = collect([]);
			$datos = Comprobaciones::with('estatus.estatus','usuario.persona')->where('fondorotatorio_id',$fondo)->get();
			
			foreach ($datos as $d) {
					$list->prepend(['id' => $d->id, 'solicitante'=>$d->usuario->persona->nombre.' '.$d->usuario->persona->primer_apellido.' '.$d->usuario->persona->segundo_apellido, 'monto'=> $d->monto_total, 'fecha'=>  $d->created_at->toDateString(), 'folio'=>$d->folio,'estatus'=>$d->estatus->estatus->nombre]);
			}
			return new JsonResponse(['data' => $list, 'message' => "Se guardo correctamente"], 200); 
    }

    public function mandarComprobacion($idpago)
    {

        $pago = Pago::find($idpago);
        if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);

        if($pago->tipopago_id != 2) {
          $pagoArchivos = Pago::with('detalles.archivos')->doesntHave('detalles.archivos')->find($idpago);
          if($pagoArchivos) return $this->jsonResponse(null, 'No tienes archivos o te faltan para mandar', 404);
        }

        $estatusPrecomprobado = $this->estatusBancaDIF('PRE-COMPROBADO');
        if(!$estatusPrecomprobado) return $this->jsonResponse(null, 'No se encontro el estatus PRE-COMPROBADO', 404);

        $pago->estatuslast()->create(['estado_programa_id' => $estatusPrecomprobado[0]['id'], 'fecha' => new \Carbon\Carbon, 'usuario_id' => request()->usuario_id]);
        return $this->jsonResponse($pago->load('estatuslast'), 'Pago actualizado correctamente');
    }

    // funcion para actualizar el estatus de las comprobaciones y de los pagos que tenga

    public function changeComprobacion(Request $request)
    {
        // return $request->all();
        $estatusList = $this->estatusBancaDIF();
				$comprobacion = Comprobaciones::with('pagos')->find($request->id);
        if($comprobacion == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        try{
            DB::beginTransaction();

            $comprobacion->estado_modulo_id = $estatusList->where('estatus',$request->cambio)->first()['id'];
						$comprobacion->save();  
						
						if($request->cambio == 'NUEVO')
						{
							foreach ($comprobacion->pagos as $d) {
								$pago = EstadoPago::create([
									'pago_id' => $d['id'],
									'estado_programa_id' => $estatusList->where('estatus','ENVIADO A COMPROBACION')->first()['id'],
									'fecha' =>  new \Carbon\Carbon,
									'usuario_id' =>   auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
								 ]);
							}
						}
            
            DB::commit();

            // auth()->user()->bitacora(request(), [
            //     'tabla' => 'siaf_cat_tipospago',
            //     'registro' => $tipoPago->id . '',
            //     'campos' => json_encode($request) . '',
            //     'metodo' => request()->method()
            // ]);
            return new JsonResponse(['data' => $request->cambio, 'message'=>"Se actualizo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }
    public function listall($tipo)
    {
      $estatusList = $this->estatusBancaDIF();
      $list = collect([]);
      $gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first()->id;
      // $datos = Comprobaciones::with('estatus.estatus','usuario.persona')->where('estado_modulo_id','>=',$estatusList->where('estatus','ENVIADO')->first()['id'])->where('estado_modulo_id','!=',$estatusList->where('estatus','RECHAZADO')->first()['id'])->get();
      if($tipo == 'COMPROBACION'){
        $datos = Comprobaciones::with('estatus.estatus','usuario.persona')->get();
        foreach ($datos as $d) {
          $list->prepend(['id' => $d->id,'revision'=>$d->pagos->first()->revison ?$d->pagos->first()->revison->fecha : '', 'finalizado'=>$d->pagos->first()->finalizado ? $d->pagos->first()->finalizado->fecha :'','solicitante'=>$d->usuario->persona->nombre.' '.$d->usuario->persona->primer_apellido.' '.$d->usuario->persona->segundo_apellido, 'monto'=> $d->monto_total, 'fecha'=>  $d->created_at->toDateString(), 'folio'=>$d->folio,'estatus'=>$d->estatus->estatus->nombre, 'tipo'=>"COMPROBACION"]);
        }
      }else {
        $listGastos = AsignacionPago::with('pago.estatuslast')->whereHas('asignacion', function ($q) use ($gasto){ $q->where('tipo_asignacion_id', $gasto); })->get();
        foreach ($listGastos as $g) {
          if($g->pago->estatuslast->estadoModulo->estatus->nombre != 'NUEVO'){
						$reintegros = Reintegro::where('pago_id', $g->pago->id)->get()->sum('cantidad');
						$list->prepend(['revision'=>$g->pago->revison ?$g->pago->revison->fecha : '','finalizado'=>$g->pago->finalizado ? $g->pago->finalizado->fecha :'','rechazado'=>$g->pago->rechazado ? $g->pago->rechazado->fecha : '','id' => $g->pago->id, 'solicitante'=>$g->pago->empleado->persona->nombre.' '.$g->pago->empleado->persona->primer_apellido.' '.$g->pago->empleado->persona->segundo_apellido, 'monto'=> number_format($g->pago->monto_total,2), 'fecha'=>  $g->pago->fecha_elaboracion->toDateString(), 'folio'=>$g->pago->folio,'estatus'=>$g->pago->estatuslast->estadoModulo->estatus->nombre, 'tipo'=>"PAGO",'justificacion'=>$g->pago->descripcion, 'reintegro'=> number_format($reintegros,2), 'saldo'=>number_format(($g->pago->monto_total-$reintegros),2)]);
					}
        }
      }
      
      return new JsonResponse(['data' => $list, 'message' => "Se guardo correctamente"], 200); 
    }

    public function cambioStatus(Request $request)
    {
        $estatusList = $this->estatusBancaDIF();
        $comprobacion = Comprobaciones::find($request->id);
        // return $comprobacion->folio;
        if($comprobacion == null)
          return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        try{
            DB::beginTransaction();

            $comprobacion->estado_modulo_id = $estatusList->where('estatus',$request->status)->first()['id'];
            $comprobacion->save();  

            if($request->status != 'RECHAZADO'){ // para el caso que ya cancelo ordenes de pago individualmente
                $listaPagos = ComprobacionesPagos::where('comprobacion_id',$request->id)->get();
                foreach($listaPagos as $pago){
                    $pago = EstadoPago::create([
                    'pago_id' => $pago['pago_id'],
                    'estado_programa_id' => $estatusList->where('estatus',$request->status)->first()['id'],
                    'fecha' =>  new \Carbon\Carbon,
                    'usuario_id' =>   auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
                    ]);
                }
            }
            if($request->status == 'FINALIZADO'){
               $pagoCaja = ComprobacionCaja::create([
                    'folio'=> ComprobacionCaja::withTrashed()->count() ?$comprobacion->folio."/CAJA/".(ComprobacionCaja::withTrashed()->count() + 1) :"$comprobacion->folio/CAJA/1",
                    'comprobacion_id'=> $comprobacion->id,
                    'monto'=> $request->total,
                ]);
                
                $historico = ComprobacionCajaHistorial::create([
                    'comprobacioncaja_id' => $pagoCaja->id,
                    'estado_modulo_id' => $estatusList->where('estatus','EN CAJA')->first()['id'],
                    'usuario_id' => auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
                ]);
            }
            
            DB::commit();

            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'siaf_comprobaciones',
                'registro' => $comprobacion->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method()    
            ]);
            return new JsonResponse(['data' => $request->status, 'message'=>"Se actualizo correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
    }

    public function removePago( Request $request )
    {
        $estatusList = $this->estatusBancaDIF();
        $pago = ComprobacionesPagos::where('pago_id',$request->idPago)->where('comprobacion_id',$request->idcomprobacion)->first();
        if($pago == null)
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        try{
            DB::beginTransaction();
							$pago->delete();  
							EstadoPago::create([
								'pago_id' =>$request->idPago,
								'estado_programa_id' => $estatusList->where('estatus','PAGADO')->first()['id'],
								'fecha' =>  new \Carbon\Carbon,
								'usuario_id' =>  auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id,
							]);
						DB::commit();  
						$usuario = $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);							 
						$usuario->bitacora($request, [
							'tabla' => 'siaf_comprobaciones_estados',
							'registro' => $pago->id . '',
							'campos' => json_encode($request->all()) . '',
							'metodo' => $request->method()    
						]);
            return new JsonResponse(['data' => $pago, 'message'=>"Se elimino correctamente"]);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => 'Error: ' . $e->getMessage()],409);
        }
		}
		public function cambioestado($idpago, $estado)
		{
			$pago = Pago::find($idpago);
				if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);
				
			$estatusList = $this->estatusBancaDIF();
			$estadoId = $estatusList->where('estatus',$estado)->first()['id'];
				if(!$estadoId) return $this->jsonResponse(null, 'No se encontro el estado', 404);

			EstadoPago::create([
				'pago_id' =>$pago->id,
				'estado_programa_id' => $estadoId,
				'fecha' =>  new \Carbon\Carbon,
				'usuario_id' => request()->usuario_id,
			]);
			return $this->jsonResponse($pago->load('estatuslast'), 'Pago actualizado correctamente');
		}

}
