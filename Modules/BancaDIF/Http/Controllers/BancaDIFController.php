<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \Firebase\JWT\JWT;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Models\Area;
use App\Models\AreasResponsable;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use App\Models\Banco;
use App\Models\Empleado;
use App\Models\Persona;
use App\Models\Programa;
use App\Models\RecmCatPartida;
use App\Models\Municipio;
use App\Models\Localidad;
use App\Models\Modulo;
use App\Models\Usuario;
use App\Models\Nivel;
use App\Models\Tiposempleado;
use Modules\BancaDIF\Entities\Tabulador;
use Modules\BancaDIF\Entities\ClavePresupuesto;
use Modules\BancaDIF\Entities\HistorialAreaEnlace;
use Modules\BancaDIF\Entities\Tramite;
use Modules\BancaDIF\Traits\Util;

class BancaDIFController extends Controller
{
    use Util;

    public function __construct()
    {
        $this->middleware('auth', ['only' => 'index']);
        $this->middleware('rolModuleV2:fondorotatorio,FINANCIERO,ENLACE,ADMINISTRADOR,CAPTURISTA,CAJA,PRESUPUESTAL,BANCOS', ['only' => 'index']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $usuario = auth()->user();
        $modulo = Modulo::where('nombre', 'like', 'FONDO ROTATORIO')->first();
        $roles = $usuario->usuarioroles->where('modulo_id', $modulo->id);
        $token = [
            'usuario_id' => $usuario->id,
            'modulo_id' => $modulo->id
        ];
        $encoded = JWT::encode($token, config('app.jwt_bancadif_token'), 'HS256');
        $modulo = Modulo::where('nombre', 'like', 'FONDO ROTATORIO')->first();
        $roles = \App\Models\UsuariosRol::select('id', 'modulo_id', 'rol_id', 'usuario_id')->where('modulo_id', $modulo->id)->where('usuario_id', $usuario->id)->with('rol:id,usuario_id,nombre')->get();

        foreach($roles as $role){
            $role->name = $role->rol->nombre;
        }
        if(count($roles) <= 0) array_push($roles, ['id' => 0, 'name' => 'VISITANTE']);
        return view('bancadif::index', ['bancadif_token' => $encoded, 'roles' => $roles]);
    }

    public function permissions(){
        $usuario = \App\Models\Usuario::find(request()->usuario_id);
        $modulo = Modulo::where('nombre', 'like', 'FONDO ROTATORIO')->first();
        $roles = \App\Models\UsuariosRol::select('id', 'modulo_id', 'rol_id', 'usuario_id')->where('modulo_id', $modulo->id)->where('usuario_id', $usuario->id)->with('rol:id,usuario_id,nombre')->get();

        if(count($roles) <= 0) return new JsonResponse([['id' => 0, 'name' => 'VISITANTE']]);

        foreach($roles as $role){
            $role->name = $role->rol->nombre;
        }
        return new JsonResponse($roles);
    }

    public function fondo($area_id)
    {
        $area = Area::find($area_id);
        if(!$area) return new JsonResponse(['data' => null, 'message' => 'No se encontro la área'], 404);

        $techo = $this->areaFondo($area_id);
        if(!$techo) return new JsonResponse(['data' => null, 'message' => 'No cuenta con fondo rotatorio. Si tiene dudas consulte a Finanzas'], 404);

        return new JsonResponse(['data' => $techo, 'message' => 'Ok']);;
    }

    public function areas($id)
    {
        $area = Area::find($id);
        if($area == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontro la área'], 404);
        }
        $techo = $this->areaFondo($area->id);
        if($techo == null){
            return new JsonResponse(['data' => null, 'message' => 'No cuenta con fondo rotatorio'], 404);
        }
        $total = $techo->asignaciones->sum('monto');
        $comprobado = $techo->asignaciones->where('tipo.nombre','RECUPERACION')->sum('monto');
        $arr = $techo->toArray();
        $arr['total'] = $total;
        $arr['comprobado'] = $comprobado;
        $arr['area'] = $area->nombre;
        // $arr['areaenlace'] = $techo->areaenlace;
        $areaenlace = $techo->areaenlace;
        $arr['areaenlace'] = null;
        if($areaenlace != null){
            $arr['areaenlace'] = [
                'fondorotatorioarea_id' => $areaenlace->fondorotatorioarea_id,
                'enlacefinanciero_id' => $areaenlace->enlacefinanciero_id,
                'empleado_id' => $areaenlace->empleado_id,
                'nombre_completo' => $areaenlace->empleado->persona->nombre . ' ' . $areaenlace->empleado->persona->primer_apellido . ' ' . $areaenlace->empleado->persona->segundo_apellido,
                'banco' => $areaenlace->enlacefinanciero->banco->nombre,
                'nombre_cuenta' => $areaenlace->enlacefinanciero->nombre_cuenta,
                'clabe' => $areaenlace->enlacefinanciero->clabe
            ];
        }
        return new JsonResponse(['data' => $arr, 'message' => 'ok']);
    }

    // Dado el parametro, regresa las areas hijas con sus enlaces
    public function areasenlaces($area_id)
    {
        try{
            if(Usuario::find(request()->usuario_id)->hasRolesStrModulo(['FINANCIERO'], 'fondorotatorio')) {
                $fondos = FondoRotatorioArea::with(['area', 'areaenlace', 'asignaciones'])->get();
                $data = [];
                $data['area'] = 'SISTEMA DIF OAXACA';
                $data['hijos'] = [];
                foreach($fondos as $fondo){
                    array_push($data['hijos'], [
                        'idfondo' => $fondo->id,
                        'nombre' => $fondo->area->nombre,
                        'techo' => $fondo->techo_presupuestal,
                        'enlacefinanciero' => $fondo->areaenlace ? $fondo->areaenlace->empleado->persona->nombre . ' ' . $fondo->areaenlace->empleado->persona->primer_apellido . ' ' . $fondo->areaenlace->empleado->persona->segundo_apellido : null,
                        'asignaciones' => $fondo->asignaciones,
                        'saldo_disponible' =>$fondo->saldo_disponible
                    ]);
                }
                \Log::debug('FINANCIERO');
                \Log::debug($data);
                return new JsonResponse(['data' => $data, 'message' => 'Ok']);
            }
            $area = Area::find($area_id);
            if($area == null){
                return new JsonResponse(['data' => null, 'message' => "No se encontro la área con ID: $area_id"], 404);
            }
            $areashijas = DB::select('call getAreasHijas(?)', [$area_id]);
            if($areashijas == null){
                return new JsonResponse(['data' => null, 'message' => 'No se encontraron areas con ese ID']);
            }
            $ids = array_map(function($item){ return $item->id_hijo; }, $areashijas);
            array_push($ids, $area->id);
            $fondos = FondoRotatorioArea::with(['area', 'areaenlace', 'asignaciones'])->whereIn('area_id', $ids)->get();
            $data = [];
            $data['area'] = $area->nombre;
            $data['hijos'] = [];
            foreach($fondos as $fondo){
                array_push($data['hijos'], [
                    'idfondo' => $fondo->id,
                    'nombre' => $fondo->area->nombre,
                    'techo' => $fondo->techo_presupuestal,
                    'enlacefinanciero' => $fondo->areaenlace ? $fondo->areaenlace->empleado->persona->nombre . ' ' . $fondo->areaenlace->empleado->persona->primer_apellido . ' ' . $fondo->areaenlace->empleado->persona->segundo_apellido : null,
                    'asignaciones' => $fondo->asignaciones
                ]);
            }
            return new JsonResponse(['data' => $data, 'message' => 'Ok']);
        }catch(\Exception $e){
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()]);
        }
    }

    public function areasSinfondo()
    {
        $areas = Area::doesntHave('areasfondo')->get();
        return new JsonResponse(['data' => ['total' => $areas->count(), 'areas' => $areas], 'message' => 'ok']);
    }

    public function bancos()
    {
        return new JsonResponse(['data' => Banco::all(), 'message' => 'ok']);
    }

    public function niveles()
    {
        $niveles = Nivel::orderBy('id', 'desc')->get();
        return new JsonResponse(['data' => $niveles, 'message' => 'Niveles ok']);
    }

    public function tipoempleado()
    {
        $tipos = Tiposempleado::all();
        return new JsonResponse(['data' => $tipos, 'message' => 'Tipos empleado Ok']);
    }

    public function uareas($area)
    {
        $areas = Area::where('nombre', 'like', "%$area%")->get();
        return new JsonResponse(['data' => $areas, 'message' => 'Areas ok']);
    }

    public function localidadMunicipio($localidad)
    {
        $localidades = Localidad::with('municipio')->where('nombre', 'like', "%$localidad%")->take(10)->get();
        foreach($localidades as $localidad){
            $localidad->displayName = $localidad->nombre . ' - ' . $localidad->municipio->nombre;
        }
        return new JsonResponse(['data' => $localidades, 'message' => 'Localidades ok']);
    }

    public function programas($nombre, $fondo = false)
    {
        if($fondo)
            return new JsonResponse(['data' =>
                Programa::whereIn('tipo', ['SUBPROGRAMA', 'SERVICIO'])
                    ->where('nombre', 'like', "%$nombre%")
                    ->orWhereHas('areasprograma', function($query){
                        $query->where('area_id', 15); })
                    ->get(), 'message' => 'ok']);
        return new JsonResponse(['data' => Programa::where('tipo', 'like', 'SUBPROGRAMA')->where('nombre', 'like', "%$nombre%")->get(), 'message' => 'ok']);

    }

    public function firmas($area_id, $id_empleado)
    {
        try{
            $area = Area::find($area_id);
            if($area == null){
                return new JsonResponse(['data' => null, 'message' => "No se encontro la área con ID: $area_id"], 404);
            }

						$fondo = $this->areaFondo($area->id);
						$director = AreasResponsable::where('empleado_id',$id_empleado)->whereHas('area',function ($q){$q->whereIn('tipoarea_id',[1,2,6]);})->count();
						if($director > 0)
							$autoriza = DB::select('call getDireccionSiaf(?)', [AreasResponsable::where('empleado_id',$id_empleado)->first()->area_id])[0];
						else if( Area::where('id',$fondo->area_id)->where('tipoarea_id',2)->count() > 0)
							$autoriza = DB::select('call getDireccionSiaf(?)', [$fondo->area_id])[0];
						else
							$autoriza = DB::select('call getPadreSiaf(?)', [$area_id])[0];

					  $voboArea = DB::select('call getDireccionSiaf(?)', [$fondo->area_id]);
            $clave = ClavePresupuesto::where('area_id', $voboArea[0]->direccion_id)->with(['area', 'unidad'])->first();
            $respFondo = HistorialAreaEnlace::where('fondorotatorioarea_id',$fondo->id)->first();
            if(!$respFondo) return $this->jsonResponse(null, 'No se encontro al enlace', 404);
            $nombFondo = $respFondo->empleado->persona->nombre.' '.$respFondo->empleado->persona->primer_apellido.' '.$respFondo->empleado->persona->segundo_apellido;
            $areaFondo = Area::find($fondo->area_id)->nombre;

            return new JsonResponse(['data' => [ 'autorizo' => $autoriza, 'clave' => $clave, 'respFondo' =>	$nombFondo,'areaFondo'=>$areaFondo, 'respIdEmpleado'=>$respFondo->empleado->id], 'message' => 'Ok']);
        }catch(\Exception $e){
            \Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 422);
        }
    }

    public function partidas($partida = null)
    {
        if(request()->query('viaticos')){
            $partidas = RecmCatPartida::whereIn('partida_especifica_c', [241, 373, 369, 370, 371, 443])->get();
            return new JsonResponse(['data' => $partidas, 'message' => 'ok']);
        }
        if(is_numeric($partida)){
            $partidas = RecmCatPartida::where('partida_especifica_c', '=', $partida)->get();
        }else{
            $partidas = RecmCatPartida::where('partida_especifica', 'like', "%$partida%")->get();
        }
        return new JsonResponse(['data' => $partidas, 'message' => 'ok']);
    }

    public function municipios($municipio, $region_id = null)
    {
        $municipios = Municipio::with('distrito.region')->where('nombre', 'like', "%$municipio%")->take(10)->get();
        if($region_id != null)
            // $municipios = Municipio::with('distrito.region')->where('nombre', 'like', "%$municipio%")->where('distrito_id', $distrito_id)->take(10)->get();
            $municipios = Municipio::with('distrito.region')->where('nombre', 'like', "%$municipio%")->whereHas('distrito.region', function($query) use($region_id){
                $query->where('region_id', $region_id);
            })->take(10)->get();
        return new JsonResponse(['data' => $municipios, 'message' => 'ok']);
    }

    public function localidades($municipios_ids, $localidad)
    {
        $ids = explode(',', $municipios_ids);
        $localidades = Localidad::where('nombre', 'like', "%$localidad%")->whereIn('municipio_id', $ids)->get();
        return new JsonResponse(['data' => $localidades, 'message' => 'ok']);
    }

    public function cuotas()
    {
        $cuotas = Tabulador::get();
        $str = '[';
        // DB::table('users')->insert([
        //     'name' => str_random(10),
        //     'email' => str_random(10).'@gmail.com',
        //     'password' => bcrypt('secret'),
        // ]);
        foreach($cuotas as $cuota){
            $str .= "['origen' => '" . $cuota->origen . "', 'destino' => '". $cuota->destino ."', 'estado' => '" . $cuota->estado . "', 'cuota' => " . $cuota->cuota . "],";
        }
        $str .= ']';
        return $str;
    }

    public function cuotasMunicipio($origen_id, $destino_id)
    {
        $region = Municipio::with('distrito.region')->where('id', $origen_id)->first();
        $region2 = Municipio::with('distrito.region')->where('id', $destino_id)->first();
        $cuota = Tabulador::where('origen', $region->distrito->region->nombre)->where('destino', $region2->distrito->region->nombre)->first();
        if($cuota == null){
            return new JsonResponse(['data' => $cuota, 'message' => 'No se encontro la cuota con esos datos.'], 404);
        }
        return new JsonResponse(['data' => $cuota, 'message' => 'ok']);
    }

}