<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\Reintegro;

class ReportesController extends Controller
{
	
    /**
     * Solo gastos por comprobar
     * @return Response
     */
    public function gastos()
    {
		$pagos = Pago::whereHas('asginacionpago.tipo',function ($q){$q->where('nombre','GASTO A COMPROBAR');})->get();
		$pagolist = collect([]);
		// $estatusList = $this->estatusBancaDIF();  
		foreach ($pagos as $pago) {
			$reintegro = Reintegro::where('pago_id',$pago->id)->get()->sum('cantidad');
			$fechaPago =  EstadoPago::where('pago_id',$pago->id)->where('estado_programa_id',6)->count() > 0 ? EstadoPago::where('pago_id',$pago->id)->where('estado_programa_id',6)->first()->fecha : ''; // fecha de cuando se pago
			$fechaFin = EstadoPago::where('pago_id',$pago->id)->where('estado_programa_id',12)->count() > 0 ? EstadoPago::where('pago_id',$pago->id)->where('estado_programa_id',12)->first()->fecha : ''; // fecha de cuando se finalizo el pago
			
			$pagolist->prepend(['solicitante'=> $pago->empleado->persona->nombre .' '. $pago->empleado->persona->primer_apellido .' '. $pago->empleado->persona->segundo_apellido, 'monto'=> number_format($pago->monto_total,2), 'reintegro'=> number_format($reintegro,2),'folio'=>$pago->folio,'estatus'=>$pago->estatuslast->estadoModulo->estatus->nombre,'fecha'=>  $pago->fecha_elaboracion->toDateString(),'descripcion' => $pago->descripcion, 'pagado' => $pago->metodopago_id ? ($pago->metodo->nombre == 'PAGO BANCO' ? 'BANCO':'CAJA') :'CAJA', 'fechaPagado'=>$fechaPago, 'fechaFin'=>$fechaFin, 'tipo' => $pago->asginacionpago->first()->tipo->nombre, 'area'=>$pago->fondo->area->nombre]);
		}
		return new JsonResponse(['data' => $pagolist],200);
	}
		
	public function pagoReintegro($fechaInicio, $fechaFin)
	{	
		
		$pagos = Reintegro::whereBetween('created_at', [$fechaInicio, $fechaFin])->whereHas('pago.asginacionpago.tipo',function ($q){$q->where('nombre','GASTO A COMPROBAR');})->get();
					
		$pagolist = collect([]);
		foreach ($pagos as $pagor) {
			$pagolist->prepend(['solicitante'=> $pagor->pago->empleado->persona->nombre .' '. $pagor->pago->empleado->persona->primer_apellido .' '. $pagor->pago->empleado->persona->segundo_apellido, 'monto'=> number_format($pagor->pago->monto_total,2), 'reintegro'=> number_format($pagor->cantidad,2),'folio'=>$pagor->pago->folio,'fecha'=>  $pagor->pago->fecha_elaboracion->toDateString(), 'tipo' => $pagor->pago->asginacionpago->first()->tipo->nombre, 'cuenta'=>$pagor->pago->cuenta->count() > 0 ? $pagor->pago->cuenta->first()->num_cuenta :'', 'fechareintegro' => $pagor->created_at->toDateString(), 'folireintegro' => $pagor->folio, 'pagadoen' => $pagor->pago->metodo ? $pagor->pago->metodo->nombre : ""]);
		}
		return new JsonResponse( $pagolist);
	}

    
}
