<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Modules\BancaDIF\Entities\AsignacionPago;
use Modules\BancaDIF\Traits\Util;

class AsignacionesPagosController extends Controller
{
    use Util;
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('bancadif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('bancadif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('bancadif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('bancadif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function listapagos($id) 
    {
        $lista = AsignacionPago::with('pago.comprobacion','pago.estatuslast','pago.empleado.persona')->where('asignacion_id',$id)->get();
        if($lista->count() == 0)
            return new JsonResponse(['data' => [], 'message' => 'No tiene ningun pago']);

        $estatusList = $this->estatusBancaDIF(); // lista de estatus
        $pagolist = collect([]);
        foreach ($lista as $p) {
            if($p->pago->estatuslast->estado_programa_id == $estatusList->where('estatus','PRE-COMPROBADO')->first()['id'] && $p->pago->comprobacion == null)
                $pagolist->prepend(['id' => $p->pago->id, 'monto'=> $p->pago->monto_total, 'solicitante'=> $p->pago->empleado->persona->nombre .' '. $p->pago->empleado->persona->primer_apellido .' '. $p->pago->empleado->persona->segundo_apellido, 'folio'=>$p->pago->folio, 'estatus'=>$p->pago->estatuslast->estadoModulo->estatus->nombre, 'fecha'=>  $p->pago->fecha_elaboracion->toDateString(), 'fondo_id'=>$p->pago->fondo_id]);
        }

       return new JsonResponse(['data' => $pagolist, 'message' => 'Ok']);
    }
}
