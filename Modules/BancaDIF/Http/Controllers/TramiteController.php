<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Modules\BancaDIF\Entities\Tramite;
use Modules\BancaDIF\Entities\TramiteDetalle;
use Modules\BancaDIF\Entities\AreaSQL;
use Modules\BancaDIF\Entities\ComprobacionCaja;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\Estatus;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\AsignacionPago;
use App\Models\Area;

class TramiteController extends Controller
{
    public function crearTramite(Request $request)
    {
        $now = new \Carbon\Carbon();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);

        $areaM = Area::where('nombre', 'like', $request->area)->first();
        if(!$areaM)
            return new JsonResponse(['data' => null, 'message' => 'No encontramos el área en los registros'], 404);

        if($areaM->tipoarea_id == 1 || $areaM->tipoarea_id == 6)
            $area = AreaSQL::where('Direccion', 'like', $request->area)->whereNull('Coordinacion')->whereNull('Departamento')->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 2 || $areaM->tipoarea_id == 3)
            $area = AreaSQL::where('Coordinacion', 'like', $request->area)->whereNull('Departamento')->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 4)
            $area = AreaSQL::where('Departamento', 'like', $request->area)->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 5)
            $area = AreaSQL::where('Oficina', 'like', $request->area)->first();
        if(!isset($area))
            return new JsonResponse(['data' => null, 'message' => 'No encontramos el área'], 404);

        $pagoCaja = ComprobacionCaja::where('comprobacion_id', $request->comprobacion_id)->first();
        
        try{
            DB::connection('sqlsrv')->beginTransaction();

            $tramite = Tramite::create([
                'Cp_Ejercicio'              => $now->year,
                'Cp_CtaContable'            => null,
                'Cp_Benefi'                 => 'AMADA DE LA CRUZ SANTIAGO MORA',
                'Cp_Impte'                  => $request->importe,
                'Cp_Observa'                => null,
                'Cp_Status'                 => 1,
                'Cp_IdTipoTramite'          => 62,
                'Cp_TipoTramite'            => 'FONDO ROTATORIO',
                'Cp_FechaRecep'             => $now->format('Ymd H:i:s'),
                'Cp_StatusValid'            => 0,
                'Cp_Tipo'                   => 'ASIGNACION ORDINARIA DE OPERACION',
                'Cp_RFC'                    => 'SAMA710913',
                'Cp_StatusCancela'          => 0,
                'Cp_ObservCancela'          => null,
                'Cp_FechaHoraMovs'          => $now->format('Ymd H:i:s'),
                'Cp_UsuarioRecep'           => $usuario->usuario,
                'Cp_UsuarioValid'           => null,
                'Cp_Aceptado'               => 1,
                'CP_usuario'                => $usuario->usuario,
                'Cp_IdAreaFinan'            => $area->Cp_Id,
                'cp_ejerciciovale'          => 0,
                'Cp_NoVale'                 => 0,
                'Cp_TotalReintegro'         => 0,
                'cp_ejercicio_solicitud'    => 0,
                'cp_nosolicitudpago'        => 0,
                'cp_envio_bancos_pago'      => 0,
                'cp_envio_bancos_fecha'     => null,
                'cp_bancos_idmemo_pago'     => 0,
                'cp_bancos_fechamemo_pago'  => null,
                'cp_cedula'                 => 0,
                'cp_añocedula'              => 0,
                'cp_selecc'                 => 0,
                'cp_areasolicita'           => $area->Direccion . '-' . $area->Coordinacion . '-' . $area->Departamento . '-' . $area->Oficina,
                'cp_idsolicitante'          => 0,
                'cp_nombresolicita'         => '',
                'cp_id_memo_puenteo'        => 0,
                'cp_tipofactura_inicial'    => 1,
                'cp_tipofactura_final'      => 1,
                'cp_foliocomprobacion'      => $request->foliocomprobacion,
                'cp_foliopago'              => $pagoCaja->folio,
            ]);

            $detallesTramite = [];
            foreach($request->detalles as $detalle) {
                Log::debug($detalle);
                $fecha = new \Carbon\Carbon($detalle['fecha']);
                $detalleTramite = TramiteDetalle::create([
                    'Cp_IdId'                   => $tramite->Cp_Ind,
                    'Cp_Tipo'                   => 'F',
                    'Cp_IdVale'                 => null,
                    'Cp_EjercicioVale'          => null,
                    'Cp_NoVale'                 => 0,
                    'Cp_Folio'                  => $detalle['folio'],
                    'Cp_Solicitante'            => $detalle['solicitante'],
                    'Cp_Area'                   => $area->Direccion . '-' . $area->Coordinacion . '-' . $area->Departamento . '-' . $area->Oficina,
                    'Cp_IdChe'                  => null,
                    'Cp_CtaContable'            => 'SAMA710913',
                    'Cp_Ejercicio'              => $now->year,
                    'Cp_Viatico'                => null,
                    'Cp_NoFactura'              => $detalle['factura'],
                    'Cp_Fecha'                  => $fecha->format('Ymd'),
                    'Cp_Neto'                   => $detalle['importe'],
                    'Cp_Retenido'               => null,
                    'Cp_Observa'                => null,
                    'Cp_StatusTemp'             => 0,
                    'CP_status'                 => 0,
                    'Cp_StatusReintegro'        => 0,
                    'Cp_Distrino'               => null,
                    'CP_Origen'                 => null,
                    'Cp_Dias'                   => null,
                    'Cp_Rfc'                    => $detalle['rfc'],
                    'Cp_TipoInversion'          => 'ASIGNACION ORDINARIA DE OPERACION',
                    'Cp_Pedido'                 => null,
                    'Cp_Obra'                   => null,
                    'Cp_Partida'                => null,
                    'Cp_StatusRetenido'         => 0,
                    'Cp_FechaHoraMovs'          => $now->format('Ymd H:i:s'),
                    'Cp_Usuario'                => $usuario->usuario,
                    'Cp_IdTransferencia'        => 0,
                    'Cp_Concepto'               => '',
                    'Cp_Reintegro'              => 0,
                    'cp_soporte_contabilidad'   => 0,
                    'cp_selecc_recibe'          => 0
                ]);
                array_push($detallesTramite, $detalleTramite);
            }

            DB::connection('sqlsrv')->commit();
            
            $pagoCaja->no_tramite = $tramite->Cp_Ind;
            $pagoCaja->save();
            $tramite->detalleTramite = $detallesTramite;
            
            return new JsonResponse(['data' => $tramite, 'message' => 'Tramite creado correctamente'], 201);

        }catch(\Exception $e){
            DB::connection('sqlsrv')->rollBack();
            Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    public function crearTramiteGasto(Request $request)
    {
        Log::debug($request->all());
        $pago = Pago::with(['empleado', 'tipo', 'detalles.archivos'])->find($request->pago_id);
        if(!$pago) return new JsonResponse(['data' => null, 'message' => 'No se encontró el pago'], 404);

        $now = new \Carbon\Carbon();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);

        $areaM = Area::where('nombre', 'like', $request->area)->first();
        if(!$areaM)
            return new JsonResponse(['data' => null, 'message' => 'No encontramos el área en los registros'], 404);

        if($areaM->tipoarea_id == 1)
            $area = AreaSQL::where('Direccion', 'like', $request->area)->whereNull('Coordinacion')->whereNull('Departamento')->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 2 || $areaM->tipoarea_id == 3)
            $area = AreaSQL::where('Coordinacion', 'like', $request->area)->whereNull('Departamento')->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 4)
            $area = AreaSQL::where('Departamento', 'like', $request->area)->whereNull('Oficina')->first();
        if($areaM->tipoarea_id == 5)
            $area = AreaSQL::where('Oficina', 'like', $request->area)->first();
        if(!$area)
            return new JsonResponse(['data' => null, 'message' => 'No encontramos el área'], 404);
        
        try{
            DB::connection('sqlsrv')->beginTransaction();

            $fechaPago = new \Carbon\Carbon($pago->fecha_elaboracion);

            $tramite = Tramite::create([
                'Cp_Ejercicio'              => $now->year,
                'Cp_CtaContable'            => null,
                'Cp_Benefi'                 => $pago->empleado->persona->nombre . ' ' . $pago->empleado->persona->primer_apellido . ' ' . $pago->empleado->persona->segundo_apellido,
                'Cp_Impte'                  => $request->importe,
                'Cp_Observa'                => null,
                'Cp_Status'                 => 1,
                'Cp_IdTipoTramite'          => 5,
                'Cp_TipoTramite'            => 'GASTOS A COMPROBAR',
                'Cp_FechaRecep'             => $fechaPago->format('Ymd H:i:s'),
                'Cp_StatusValid'            => 0,
                'Cp_Tipo'                   => 'ASIGNACION ORDINARIA DE OPERACION',
                'Cp_RFC'                    => $pago->empleado->rfc,
                'Cp_StatusCancela'          => 0,
                'Cp_ObservCancela'          => null,
                'Cp_FechaHoraMovs'          => $now->format('Ymd H:i:s'),
                'Cp_UsuarioRecep'           => $usuario->usuario,
                'Cp_UsuarioValid'           => null,
                'Cp_Aceptado'               => 1,
                'CP_usuario'                => $usuario->usuario,
                'Cp_IdAreaFinan'            => $area->Cp_Id,
                'cp_ejerciciovale'          => 0,
                'Cp_NoVale'                 => 0,
                'Cp_TotalReintegro'         => 0,
                'cp_ejercicio_solicitud'    => 0,
                'cp_nosolicitudpago'        => 0,
                'cp_envio_bancos_pago'      => 0,
                'cp_envio_bancos_fecha'     => null,
                'cp_bancos_idmemo_pago'     => 0,
                'cp_bancos_fechamemo_pago'  => null,
                'cp_cedula'                 => 0,
                'cp_añocedula'              => 0,
                'cp_selecc'                 => 0,
                'cp_areasolicita'           => $area->Direccion . '-' . $area->Coordinacion . '-' . $area->Departamento . '-' . $area->Oficina,
                'cp_idsolicitante'          => 0,
                'cp_nombresolicita'         => '',
                'cp_id_memo_puenteo'        => 0,
                'cp_tipofactura_inicial'    => 3,
                'cp_tipofactura_final'      => 3,
                'cp_foliocomprobacion'      => null,
                'cp_foliopago'              => $pago->folio,
            ]);

            $detalleTramite = TramiteDetalle::create([
                'Cp_IdId'                   => $tramite->Cp_Ind,
                'Cp_Tipo'                   => 'F',
                'Cp_IdVale'                 => null,
                'Cp_EjercicioVale'          => null,
                'Cp_NoVale'                 => 0,
                'Cp_Folio'                  => null,
                'Cp_Solicitante'            => '',
                'Cp_Area'                   => $area->Direccion . '-' . $area->Coordinacion . '-' . $area->Departamento . '-' . $area->Oficina,
                'Cp_IdChe'                  => null,
                'Cp_CtaContable'            => $pago->empleado->rfc,
                'Cp_Ejercicio'              => $now->year,
                'Cp_Viatico'                => $pago->tipo->nombre == 'VIATICO' ? 1 : null,
                'Cp_NoFactura'              => $pago->folio,
                'Cp_Fecha'                  => $fechaPago->format('Ymd'),
                'Cp_Neto'                   => $pago->monto_total,
                'Cp_Retenido'               => null,
                'Cp_Observa'                => null,
                'Cp_StatusTemp'             => 0,
                'CP_status'                 => 0,
                'Cp_StatusReintegro'        => 0,
                'Cp_Distrino'               => null,
                'CP_Origen'                 => null,
                'Cp_Dias'                   => null,
                'Cp_Rfc'                    => $pago->empleado->rfc,
                'Cp_TipoInversion'          => 'ASIGNACION ORDINARIA DE OPERACION',
                'Cp_Pedido'                 => null,
                'Cp_Obra'                   => null,
                'Cp_Partida'                => null,
                'Cp_StatusRetenido'         => 0,
                'Cp_FechaHoraMovs'          => $now->format('Ymd H:i:s'),
                'Cp_Usuario'                => $usuario->usuario,
                'Cp_IdTransferencia'        => 0,
                'Cp_Concepto'               => '',
                'Cp_Reintegro'              => 0,
                'cp_soporte_contabilidad'   => 0,
                'cp_selecc_recibe'          => 0
            ]);

            DB::connection('sqlsrv')->commit();

            $estatusTramite = Estatus::with('estatus')
                ->whereHas('modulo', function ($q){
                    $q->where('nombre','FONDO ROTATORIO');
                })
                ->whereHas('estatus', function($query) {
                    $query->where('nombre', 'like', 'TRÁMITE CREADO');
                })->get();

            $asignacion = AsignacionPago::where('pago_id', $pago->id)->first();
            if(!$asignacion) return new JsonResponse(['data' => null, 'message' => 'No se encontro la asignación'], 404);

            $asignacion->no_tramite = $tramite->Cp_Ind;
            $asignacion->save();

            EstadoPago::create([
                'pago_id' => $pago->id,
                'estado_programa_id' => $estatusTramite[0]->id,
                'fecha' => $now,
                'observacion' => null,
                'usuario_id' => $usuario->id
            ]);
            
            $pago->load('estatuslast.estadoModulo.estatus');
            $tramite->detalleTramite = $detalleTramite;
            $tramite->pago = $pago;
            
            return new JsonResponse(['data' => $tramite, 'message' => 'Tramite creado correctamente'], 201);

        }catch(\Exception $e){
            DB::connection('sqlsrv')->rollBack();
            Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }
}
