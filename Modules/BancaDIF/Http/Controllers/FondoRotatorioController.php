<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Validator;
use App\Models\Region;
use App\Models\Entidad;
use Modules\BancaDIF\Entities\Tabulador;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Asignacion;

class FondoRotatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        try{
            $fondos = FondoRotatorioArea::has('area')->with(['areaenlace', 'asignaciones'])->get();
            $areas = [];
            foreach($fondos as $fondo){
                $datos = [
                    'id' => $fondo->id, 
                    'area' => ['area_id' => $fondo->area->id, 'nombre' => $fondo->area->nombre], 
                    'techo_presupuestal' => $fondo->techo_presupuestal,
                    'enlace' => [
                        'empleado_id' => $fondo->areaenlace ? $fondo->areaenlace->empleado->id : null, 
                        'persona_id' => $fondo->areaenlace ? $fondo->areaenlace->empleado->persona->id : null, 
                        'nombre' => $fondo->areaenlace ? $fondo->areaenlace->empleado->persona->nombre . ' ' . $fondo->areaenlace->empleado->persona->primer_apellido . ' ' . $fondo->areaenlace->empleado->persona->segundo_apellido : null
                    ],
                    'financiero' => $fondo->areaenlace ? $fondo->areaenlace->enlacefinanciero->toArray() : null,
                    'asignaciones' => $fondo->asignaciones
                ];
                $request = request();
                $datos['financiero']['banco'] = $fondo->areaenlace ? $fondo->areaenlace->enlacefinanciero->banco->nombre : null;
                array_push($areas, $datos);
            }
            return new JsonResponse(['data' => $areas, 'message' => 'Ok']);
        }catch(\Exception $e){
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    public function store(Request $request)
    {
        try{
            $validator = Validator::make($request->all(), [
                'area_id' => 'required|integer',
                'techo_presupuestal' => 'required'
            ]);
            if ($validator->fails()) {
                return new JsonResponse($validator->errors(), 480);
            }
            $fondo = FondoRotatorioArea::create([
                'area_id' => $request->area_id,
                'techo_presupuestal' => $request->techo_presupuestal,
                'usuario_id' => $request->usuario_id,
                'saldo_disponible' => 0
            ]);
            return new JsonResponse(['data' => $fondo, 'message' => 'Guardado Correctamente']);
        }catch(\Exception $e){
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    public function update(Request $request, $fondo)
    {
        try{
            $validator = Validator::make($request->all(), [
                'area_id' => 'required|integer',
                'techo_presupuestal' => 'required',
                'usuario_id' => 'required|integer'
            ]);
            if ($validator->fails()) {
                return new JsonResponse($validator->errors(), 480);
            }
            $fondoRotatorio = FondoRotatorioArea::find($fondo);
            if($fondoRotatorio == null){
                return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
            }
            $fondoRotatorio->fill($request->all());
            $fondoRotatorio->save();
            return new JsonResponse($fondoRotatorio);
        }catch(\Exception $e){
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    public function destroy($fondo)
    {
        $fondoRotatorio = FondoRotatorioArea::find($fondo);
        if($fondoRotatorio == null){
            return new JsonResponse(['data' => null, 'message' => 'No existe el dato'], 404);
        }
        $fondoRotatorio->delete();
        return new JsonResponse(['data' => $fondoRotatorio, 'message' => 'Eliminado']);
    }

    public function regiones()
    {
        $regiones = Region::all();
        return new JsonResponse(['data' => $regiones, 'message' => 'Ok']);
    }

    public function entidades($entidad)
    {
        $entidades = Tabulador::where('cuota', '>', 999)->where('destino', 'like', "%$entidad%")->get();
        return new JsonResponse(['data' => $entidades, 'message' => 'Ok']);
    }

}
