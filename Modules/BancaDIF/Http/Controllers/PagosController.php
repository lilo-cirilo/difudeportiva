<?php

namespace Modules\BancaDIF\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\DetallePago;
use Modules\BancaDIF\Entities\EstadoPago;
use Modules\BancaDIF\Entities\Transferencia;
use Modules\BancaDIF\Entities\Viatico;
use Modules\BancaDIF\Entities\TipoPago;
use App\Models\Area;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Estatus;
use Modules\BancaDIF\Entities\TipoTransporte;
use Modules\BancaDIF\Entities\Transporte;
use Modules\BancaDIF\Entities\Asignacion;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use App\Models\Empleado;
use Modules\BancaDIF\Entities\Reintegro;
use Modules\BancaDIF\Entities\EmpleadoTarjeta;

use Modules\BancaDIF\Traits\Util;
use Modules\BancaDIF\Traits\PagoValidate;
use Modules\BancaDIF\Patterns\PagoFactory\PagoFactory;
use Modules\BancaDIF\Patterns\Strategy\CrearPago;
use App\Models\AreasResponsable;
use Modules\BancaDIF\Entities\AsignacionPago;
use Modules\BancaDIF\Entities\HistorialAreaEnlace;
use Modules\BancaDIF\Entities\MetodoPago;

class PagosController extends Controller
{
    use Util, PagoValidate;

    /**
     * Crear un nuevo pago y su detalle en la base de datos
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $errors = $this->validateStorePago($request);
        if($errors != null)
            return new JsonResponse(['data' => null, 'message' => $errors], 480);
            
        $empleado = Empleado::find($request->solicitante_empleado_id);

        if(!in_array($empleado->tiposempleado->tipo, ['BASE', 'CONFIANZA NOMBRAMIENTO', 'CONTRATO ESPECIAL', 'MANDOS MEDIOS', 'CONTRATO', 'CONFIANZA CONTRATO', 'CONTRATO CONFIANZA ADICIONAL', 'CONTRATO CONFIANZA UNIDADES MOVILES', 'NOMBRAMIENTO CONFIANZA UNIDADES MOVILES']))
            return $this->jsonResponse(null, 'No puede viaticar este tipo de contrato', 403);

        if($request->tipopago_id == 1)
            if(!$this->validarViaticos($empleado, $request->fecha_salida, $request->fecha_regreso))
                return $this->jsonResponse(null, 'La persona seleccionada ya tiene viáticos en esa fecha', 403);

        $fondo = $this->areaFondo($request->area_id);
        if($fondo == null || $fondo == '') $this->jsonResponse(null, 'No se encontro el fondo de esta área', 404);

        try{
            DB::beginTransaction();
            $fecha = new \Carbon\Carbon;
            $folio = $this->nuevoFolio($request->area_id, $request->tipopago_id);
            $metodopago = null;
            if(isset($request->asignacion_id) && $request->asignacion_id != 1) {
                $vobo = DB::select('call getDireccion(?)', [$fondo->area_id])[0];
                $metodopago = MetodoPago::where('nombre', 'PAGO BANCO')->get()->first();
                if($metodopago == null) $this->jsonResponse(null, 'No encontramos el metodo de pago', 404);
            }
            $pago = Pago::create([
                'fecha_elaboracion' => $fecha,
                'solicitante_empleado_id'=> $request->solicitante_empleado_id,
                'monto_total' => "$request->monto_total",
                'descripcion' => $request->descripcion,
                'metodopago_id' => isset($request->asignacion_id) && $metodopago != null ? $metodopago->id : null,
                'tipopago_id' => $request->tipopago_id,
                'autorizo_empleado_id' => $request->autorizo,
                'vobo_empleado_id' => isset($request->asignacion_id) && $request->asignacion_id != 1 ? $vobo->empleado_id : $request->vobo,
                'programa_id' => $request->programa_id,
                'usuario_id' => $request->usuario_id,
                'folio' => $folio,
                'fondo_id' =>  $fondo->id
            ]);
            
            if(isset($request->asignacion_id)){
                $pago->asginacionpago()->attach($request->asignacion_id);
                if($request->asignacion_id != 1)
                    $tarjeta = EmpleadoTarjeta::firstOrCreate([
                        'num_tarjeta' => $request->tarjeta,
                        'banco_id' => $request->banco_id,
                        'empleado_id' => $request->solicitante_empleado_id
                    ]);
            }
            
            $estatus = $this->estatusBancaDIF('NUEVO');
            $estadoPago = EstadoPago::create([
                'pago_id' => $pago->id,
                'estado_programa_id' => $estatus[0]['id'],
                'fecha' => $fecha,
                'usuario_id' => $request->usuario_id
            ]);
            
            $detallePago = [];
            foreach ($request->detalle as $item) {
                $detalle = DetallePago::create([
                    'pago_id' => $pago->id,
                    'partida_id' => $item['partida_id'],
                    'monto' => $item['monto'] . '',
                    'usuario_id' => $request->usuario_id
                ]);
                array_push($detallePago, $detalle);
            }
            $data = $request->all();
            $data['pago_id'] = $pago->id;
            $crearPago = new CrearPago;
            $tramite = $crearPago->crearPagos($pago->tipopago_id, $data);

            $response = ['pago' => $pago, 'detallePago' => $detallePago, 'estadoPago' => $estadoPago, 'viatico' => $tramite];

            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'siaf_pagos',
                'registro' => $pago->id . '',
                'campos' => json_encode($pago) . '',
                'metodo' => $request->method()    
            ]);
            return new JsonResponse([
                'created' => true, 
                'message' => 'Creado correctamente', 
                'data' => $response], 201);
        }catch(\Exception $e){
            DB::rollBack();
            \Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function viaticos()
    {
        $tipo = TipoPago::where('nombre', 'like', 'VIATICO')->first();
        if($tipo == null)
            return new JsonResponse(['data' => $tipo, 'message' => 'No se encontro el tipo VIATICO']);
        $pagos = Pago::where('tipopago_id', $tipo->id)->with(['empleado:id,persona_id,area_id', 'empleado.persona' => function($query){ $query->select('id', 'nombre', 'primer_apellido', 'segundo_apellido'); }, 'detalles', 'viatico.cuota'])->paginate(20);
        return new JsonResponse(['data' => $pagos, 'message' => 'Ok']);
    }

    public function gastos($idarea)
    {
        $area = Area::find($idarea);
        if($area == null) return new JsonResponse(['data' => null, 'message' => "No se encontro el area con ID: $idarea"], 404);

        // return $this->areaFondo($idarea)->id;
        
        // \Log::debug($area);
        // $areashijas = DB::select('call getAreasHijas(?)', [$area->id]);
        // \Log::debug($areashijas);
        // $ids = [$area->id];
        // foreach($areashijas as $item){
            // array_push($ids, $item->id_hijo);
        // }
        // \Log::debug($ids);
        // $confondo = FondoRotatorioArea::where('area_id', $idarea)->first()->id;
        // \Log::debug($confondo);
        // $noids = [];
        // $filtro = [];
        // if($confondo->count() > 0 && $confondo->first()->area_id != $idarea)
        //     array_push($noids, $confondo->first()->area_id);
        // \Log::debug($noids);
        // foreach($confondo as $fondo){
        //     $subareas = DB::select('call getAreasHijas(?)', [$fondo->area_id]);
        //     \Log::debug($subareas);
        //     foreach($subareas as $subarea){
        //         if (!in_array($subarea->id_hijo, $noids)) {
        //             array_push($noids, $subarea->id_hijo);
        //         }
        //     }
        // }
        // $filtro = array_values(array_diff($ids, $noids));
        // \Log::debug($ids);
        // \Log::debug($noids);
        // \Log::debug($filtro);
        // return $filtro;
        $tipoViatico = TipoPago::where('nombre', 'like', 'VIATICO')->first()->id;
        $tipoVale = TipoPago::where('nombre', 'like', 'VALE')->first()->id;
        $tipoGasto = TipoPago::where('nombre', 'like', 'GASTOS EN ALIMENTACIÓN')->first()->id;
        \Log::debug($tipoGasto);
        $estatus = Estatus::with('estatus')
            ->whereHas('modulo', function($query){ $query->where('nombre', 'FONDO ROTATORIO'); })
            ->whereHas('estatus', function($query){ $query->where('nombre', 'NUEVO'); })
            ->first()->id;        
        \Log::debug($estatus);
        
        // $listaPagos = Pago::with('empleado.persona','estatuslast.estadoModulo.estatus')->whereHas('empleado', function($query) use($ids) { $query->whereIn('area_id', $ids); })->get();
        $listaPagos = Pago::with('empleado.persona','estatuslast.estadoModulo.estatus')->where('fondo_id', $this->areaFondo($idarea)->id)->get();
        \Log::debug($listaPagos);
        $viaticos = collect([]);
        $vales = collect([]);
        $gastos = collect([]);
        
        foreach ($listaPagos as $pago) {
            if($pago->estatuslast != null && $pago->estatuslast->estado_programa_id != $estatus)
            {
                if ($pago->tipopago_id ==  $tipoViatico )
                    $viaticos->prepend(['id' => $pago->id, 'solicitante'=> $pago->empleado->persona->nombre .' '. $pago->empleado->persona->primer_apellido .' '. $pago->empleado->persona->segundo_apellido, 'monto'=> $pago->monto_total, 'fecha'=>  $pago->fecha_elaboracion->toDateString(), 'folio'=>$pago->folio,'estatus'=>$pago->estatuslast->estadoModulo->estatus->nombre]);
                else if ($pago->tipopago_id ==  $tipoVale)
                    $vales->prepend(['id' => $pago->id, 'solicitante'=> $pago->empleado->persona->nombre .' '. $pago->empleado->persona->primer_apellido .' '. $pago->empleado->persona->segundo_apellido, 'monto'=> $pago->monto_total, 'fecha'=>  $pago->fecha_elaboracion->toDateString(), 'folio'=>$pago->folio,'estatus'=>$pago->estatuslast->estadoModulo->estatus->nombre]);
                else if($pago->tipopago_id ==  $tipoGasto)
                    $gastos->prepend(['id' => $pago->id, 'solicitante'=> $pago->empleado->persona->nombre .' '. $pago->empleado->persona->primer_apellido .' '. $pago->empleado->persona->segundo_apellido, 'monto'=> $pago->monto_total, 'fecha'=>  $pago->fecha_elaboracion->toDateString(), 'folio'=>$pago->folio,'estatus'=>$pago->estatuslast->estadoModulo->estatus->nombre]);
            }
        }
        // $viaticos = Pago::with('empleado.persona','estatuslast')->where('tipopago_id', $tipoViatico)->whereHas('empleado', function($query) use($filtro) { $query->whereIn('area_id', $filtro); })->get();
        // $vales =    Pago::with('empleado.persona','estatuslast')->where('tipopago_id', $tipoVale)->whereHas('empleado', function($query) use($filtro) { $query->whereIn('area_id', $filtro); })->get();
        // $gastos =   Pago::with('empleado.persona','estatuslast')->where('tipopago_id', $tipoGasto)->whereHas('empleado', function($query) use($filtro) { $query->whereIn('area_id', $filtro); })->get();
        return new JsonResponse(['data' => ['vales' => $vales, 'viaticos' => $viaticos, 'gastos' => $gastos], 'message' => 'Ok']);
    }

    public function pagar(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pago_id' => 'required',
            'area_id' => 'required',
            'transferencia' => 'required',
            'titular' => 'required_if:transferencia,true',
            'num_cuenta' => 'required_if:transferencia,true',
            'banco_id' => 'required_if:transferencia,true'
        ]);
        if ($validator->fails()) {
            return new JsonResponse(['data' => null, 'message' => $validator->errors()], 480);
        }
        $fondo = $this->areaFondo($request->area_id);
        if($fondo == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontro el fondo rotatorio'], 404);
        }
        $pago = Pago::with('estatus')->find($request->pago_id);
        if($pago == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontro el pago'], 404);
        }
        if($pago->estatus->last()->estadoModulo->estatus->nombre != 'ENVIADO'){
            return new JsonResponse(['data' => null, 'message' => 'El estatus del pago no cumple los requisitos (' . $pago->estatus->last()->estadoModulo->estatus->nombre . ')'], 401);
        }
        if($pago->monto_total <= $fondo->saldo_disponible){
            try{
                DB::beginTransaction();
                $estatus = Estatus::with('estatus')
                    ->whereHas('modulo', function($query){ $query->where('nombre', 'FONDO ROTATORIO'); })
                    ->whereHas('estatus', function($query){ $query->where('nombre', 'PAGADO'); })
                    ->first();
                $pago->metodopago_id = $request->transferencia ? 2 : 1;
                $pago->save();
                $pago->estatus()->create([
                    'estado_programa_id' => $estatus->id,
                    'fecha' => \Carbon\Carbon::now(),
                    'usuario_id' => $request->usuario_id
                ]);
                $fondo->saldo_disponible = $fondo->saldo_disponible - $pago->monto_total;
                $fondo->save();
                if($request->transferencia){
                    $transferencia = Transferencia::create([
                        'pago_id' => $pago->id,
                        'titular' => $request->titular,
                        'num_tarjeta' => $request->num_cuenta,
                        'banco_id' => $request->banco_id,
                        'usuario_id' => $request->usuario_id
                    ]);
                }
                DB::commit();
                $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
                $usuario->bitacora($request, [
                    'tabla' => 'siaf_estados_pagos',
                    'registro' => $pago->id . '',
                    'campos' => json_encode($pago) . '',
                    'metodo' => $request->method()    
                ]);
                if($request->transferencia){
                    $usuario->bitacora($request, [
                        'tabla' => 'siaf_transferenciaspagos',
                        'registro' => $transferencia->id . '',
                        'campos' => json_encode($transferencia) . '',
                        'metodo' => $request->method()
                    ]);
                }
                return new JsonResponse(['data' => $pago->load('estatus'), 'message' => 'Ok']);
            }catch(Exception $e){
                DB::rollBack();
                return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
            }
        }
        return new JsonResponse(['data' => null, 'message' => 'No tienes saldo disponible'], 480);
    }

    public function show($id)
    {
        $pago = Pago::with('vobo.persona','empleado.persona','empleado.nivel','empleado.area', 'empleado.tarjeta.banco','autoriza.persona','autoriza.nivel','viatico.estatal.municipio.distrito.region','viatico.nacional','detalles.partida','tipo','metodo','usuario','programa','detalles.archivos','estatuslast.estadoModulo.estatus','observaciones')->find($id);
        $director = AreasResponsable::where('empleado_id',$pago->empleado->id)->whereHas('area',function ($q){$q->whereIn('tipoarea_id',[1,2]);})->count();
        $pago->tipo_asignacion = $pago->asginacionpago->first()->tipo->nombre;
        if($director > 0)
            $autoriza = DB::select('call getDireccionSiaf(?)', [AreasResponsable::where('empleado_id',$pago->empleado->id)->first()->area_id])[0];
        else if(Area::where('tipoarea_id',2)->where('id',$pago->fondo->area_id)->count() > 0)
            $autoriza = DB::select('call getDireccionSiaf(?)', [$pago->fondo->area_id])[0];
        else
            // $autoriza = DB::select('call getUnidad(?)', [$pago->empleado->area_id])[0];
            $autoriza = DB::select('call getPadreSiaf(?)', [$pago->empleado->area_id])[0];
        
        $pago->director = $autoriza;
        $pago->dirArea = DB::select('call getDireccion(?)', [$pago->fondo->area_id])[0];
        
        $pago->areaFondo = Area::find($pago->fondo->area_id)->nombre;
        if ($pago->empleado->area->tipoarea_id >= 5 && $pago->empleado->area_id != 68 && $pago->empleado->area->padre_id < 68) {
            $pago->empleado->area->nombre = $this->areaMenorOficina($pago->empleado->area_id)['nombre'];
        }

        return new JsonResponse(['data' => $pago, 'message' => 'ok']);
    }

    public function rechazarPago(Request $request)
    {        
        $estatusList = $this->estatusBancaDIF();
        $pago = Pago::find($request->id);
        if($pago == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontro el pago'], 404);
        }
        try{
            DB::beginTransaction();
            
            $pago->estatus()->create([
                'estado_programa_id' => $estatusList->where('estatus',$request->estatus)->first()['id'],
                'fecha' => \Carbon\Carbon::now(),
                'usuario_id' => $request->usuario_id,
                'observacion' => $request->comentario ? $request->comentario : null
            ]);
            DB::commit();
            // $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            // $usuario->bitacora($request, [
            //     'tabla' => 'siaf_estados_pagos',
            //     'registro' => $pago->id . '',
            //     'campos' => json_encode($pago) . '',
            //     'metodo' => $request->method()    
            // ]);
            // return new JsonResponse(['data' => $pago->load('estatus'), 'message' => 'Ok']);
            return new JsonResponse(['data' => $pago->load('estatuslast'), 'message' => 'Ok']);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
    }

    public function realizados($tipo, $empleado_id)
    {
        $empleado = Empleado::find($empleado_id);

        if($empleado == null){
            return new JsonResponse(['data' => null, 'message' => 'No se encontro el empleado'], 404);
        }
        \Log::debug($tipo);
        $tipopago = TipoPago::where('nombre', 'like', $tipo)->first();
        \Log::debug($tipopago);
        $estatus = Estatus::with('estatus')->whereHas('modulo', function($query){ $query->where('nombre', 'FONDO ROTATORIO'); })->whereHas('estatus', function($query){ $query->where('nombre', 'NUEVO'); })->first();
        \Log::debug($estatus);
        
        $nuevos = Pago::where('solicitante_empleado_id', $empleado->id)->where('tipopago_id', $tipopago->id)->with('estatuslast')->whereHas('estatuslast', function($query) use($estatus){ $query->where('estado_programa_id', $estatus->id); })->get();
        \Log::debug($nuevos);

        $nuevos->map(function($nuevo){
            $nuevo->estatus = 'NUEVO';
        });

        $estatus = Estatus::with('estatus')->whereHas('modulo', function($query){ $query->where('nombre', 'FONDO ROTATORIO'); })->whereHas('estatus', function($query){ $query->where('nombre', 'ENVIADO'); })->first();
        \Log::debug($estatus);
        
        $enviados = Pago::where('solicitante_empleado_id', $empleado->id)->where('tipopago_id', $tipopago->id)->with('estatuslast')->whereHas('estatuslast', function($query) use($estatus){ $query->where('estado_programa_id', $estatus->id); })->get();
        \Log::debug($enviados);

        $enviados->map(function($nuevo){
            $nuevo->estatus = 'ENVIADO';
        });

        $estatus = Estatus::with('estatus')->whereHas('modulo', function($query){ $query->where('nombre', 'FONDO ROTATORIO'); })->whereHas('estatus', function($query){ $query->where('nombre', 'PAGADO'); })->first();
        \Log::debug($estatus);

        $pagados = Pago::where('solicitante_empleado_id', $empleado->id)->where('tipopago_id', $tipopago->id)->with('estatuslast')->whereHas('estatuslast', function($query) use($estatus){ $query->where('estado_programa_id', $estatus->id); })->get();
        \Log::debug($pagados);

        $pagados->map(function($nuevo){
            $nuevo->estatus = 'PAGADO';
        });

        return new JsonResponse(['data' => ['nuevos' => $nuevos, 'enviados' => $enviados, 'pagados' => $pagados], 'message' => 'Ok']);
    }
    // buscar los pagos por area para la comprobacion
    public function buscar($tipo,$folio,$area)
    {
        $estatusList = $this->estatusBancaDIF();        
				$fol = strtoupper(str_replace("-","/",$folio));
				$confondo = $this->areaFondo($area)->id;
        // $confondo = FondoRotatorioArea::where('area_id', $fondo)->first()->id;
        $valor = $tipo == 1 ? [1,3] : [2];
        $pagos = Pago::doesntHave('comprobacion')->with('empleado.persona','estatuslast.estadoModulo.estatus','comprobacion','detalles.archivos','asginacionpago')->whereIn('tipopago_id', $valor)->where('folio','like',"%$fol%")->where('fondo_id', $confondo)->get();
				

        $pagolist = collect([]);
        $sumatoria = 0;
        foreach ($pagos as $pago) {
					if($pago->estatuslast != null && $pago->estatuslast->estado_programa_id == $estatusList->where('estatus','PRE-COMPROBADO')->first()['id'] && $pago->asginacionpago->first()->tipo_asignacion_id != 4){
						foreach ($pago->detalles as $detalle) {
							foreach ($detalle->archivos as $archivo) {
								$sumatoria += $archivo->importe;
							}
						}
						$reintegro = Reintegro::where('pago_id',$pago->id)->get()->sum('cantidad');
						$pagolist->prepend(['id' => $pago->id, 'solicitante'=> $pago->empleado->persona->nombre .' '. $pago->empleado->persona->primer_apellido .' '. $pago->empleado->persona->segundo_apellido, 'monto'=> $sumatoria == 0 ? ($pago->monto_total - $reintegro) : $sumatoria, 'folio'=>$pago->folio,'estatus'=>$pago->estatuslast->estadoModulo->estatus->nombre,'fecha'=>  $pago->fecha_elaboracion->toDateString(), 'fondo_id'=>$pago->fondo_id]);
					}
					$sumatoria = 0;
        }

        return new JsonResponse(['data' => $pagolist, 'message' => 'Ok']);
    }

    public function tramites($area_id, $tipopago_id)
    {
      $fondo = $this->areaFondo($area_id);
      if($fondo == null) return new JsonResponse(['data' => null, 'message' => 'No se encontro el fondo de esa area'], 404);

      $tipoGasto = TiposAsignaciones::where('nombre', 'GASTO A COMPROBAR')->first();
      // if($tipoGasto == null) return new JsonResponse(['data' => null, 'message' => 'No se encuentra el tipo de Asignacion GASTO A COMPROBAR'], 404);

      $asignaciones = Asignacion::with('tipo');
      $areaAtn = Area::where('nombre', 'like', 'COORDINACIÓN DE ATENCIÓN CIUDADANA Y VINCULACIÓN SOCIAL')->first();
      if($area_id != $areaAtn->id){
        $asignaciones = $asignaciones->where('fecha', 'like', \Carbon\Carbon::now()->format('Y-m-d'));
      }
      $asignaciones = $asignaciones->where('fondorotatorioarea_id', $fondo->id)
        ->where('tipo_asignacion_id', $tipoGasto->id)
        ->with('gasto')
        ->whereHas('gasto', function($query) use($tipopago_id){ $query->where('tipo_pago_id', $tipopago_id)->where('saldo_disponible', '>', 0); })
        ->get();

      $estatus = $this->estatusBancaDIF();
      $pagos = Pago::with('estatuslast', 'detalles.partida', 'empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'viatico', 'asginacionpago')->where('tipopago_id', $tipopago_id)->where('fondo_id', $fondo->id)->get();
      return new JsonResponse(['data' => ['fondo' => $fondo, 'asignaciones' => $asignaciones, 'pagos' => $pagos, 'estatus' => $estatus], 'message' => 'Ok']);
    }

    public function enviar(Request $request, $id)
    {
        $pago = Pago::find($id);
        if (!$pago) return $this->jsonResponse(null, 'No se encontró el pago.', 404);
        $nuevo = $this->estatusBancaDIF('NUEVO');
        $enviado = $this->estatusBancaDIF('ENVIADO');
        if($pago->estatuslast->estado_programa_id != $nuevo[0]['id']) return $this->jsonResponse(null, 'No puede cambiar este pago.', 403);
        $pago->estatuslast()->create(['estado_programa_id' => $enviado[0]['id'], 'fecha' => new \Carbon\Carbon, 'usuario_id' => $request->usuario_id]);
        $pago->load('estatuslast');
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
        $usuario->bitacora($request, [
            'tabla' => 'siaf_estados_pagos',
            'registro' => $pago->estatuslast->id . '',
            'campos' => json_encode($pago->estatuslast) . '',
            'metodo' => $request->method()
        ]);
        return $this->jsonResponse($pago, 'Actualizado correctamente.', 201);
    }    
    /* Datos adicionales del pago
    * regresa datos del transporte, clave presupuestal, y la cuota diaria
    */
    public function pagoAdicional($pagoId)
    {
        $pago = Pago::find($pagoId);
        if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);
        $comision = Viatico::with('clave.unidad','transporte.detalle')->where('pago_id',$pagoId)->first();
        $cantidad = DetallePago::where('pago_id',$pagoId)->where('partida_id',121)->count() > 0 ? DetallePago::where('pago_id',$pagoId)->where('partida_id',121)->first()->monto : DetallePago::where('pago_id',$pagoId)->sum('monto');
        if ($pago->empleado->area->tipoarea_id >= 5 && $pago->empleado->area_id != 68 && $pago->empleado->area->padre_id < 68) {//hardcodear
            $nuevaArea = $this->areaMenorOficina($pago->empleado->area_id);
            return $this->jsonResponse(['comision'=>$comision, 'monto'=> $cantidad, 'nuevaArea' => $nuevaArea]);
        }
        return $this->jsonResponse(['comision'=>$comision, 'monto'=> $cantidad]);
    }
    
    public function actualizarFecha(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'fecha_elaboracion' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->jsonResponse(['data' => null, 'message' => $validator->errors()], 480);
        }

        $pago = Pago::find($id);
        if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);

        $pago->fecha_elaboracion = $request->fecha_elaboracion;
        $pago->save();

        return $this->jsonResponse($pago, 'Actualizado correctamente');
    }

    public function reintegro(Request $request, $id)
    {
        $pago = Pago::with('empleado.persona:id,nombre,primer_apellido,segundo_apellido', 'empleado.area')->find($id);
        if(!$pago) return $this->jsonResponse(null, 'No encontramos el pago', 404);

        $area = DB::select('call getDireccion(?)', [$pago->empleado->area_id]);

        $fondo = $this->areaFondo($pago->empleado->area_id);
				$nuevoFolio = '';
        if(isset($request->tipo) && $request->tipo == 'CAJA') {
          $gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first();
          $ultimoReintegro = Reintegro::whereHas('pago.asginacionpago', function($query) use($gasto) {
            $query->where('tipo_asignacion_id', $gasto->id);
          })->get()->last();
          if($ultimoReintegro){
            $string = explode('/', $ultimoReintegro->folio);
            $string[2]++;
            $nuevoFolio = implode('/', $string);
          }else {
            $nuevoFolio = 'CG/2019/1';
          }
        }else {
          $ultimoReintegro = Reintegro::get()->last();
          if(!$ultimoReintegro){
            $nuevoFolio = 0;
          }else {
            $nuevoFolio = '' . (int)$ultimoReintegro->folio + 1;
          }
        }

        $reintegro = Reintegro::create([
          'pago_id' => $pago->id,
          'cantidad' => $request->cantidad,
          'folio' => $nuevoFolio,
          'usuario_id' => auth()->user() ? auth()->user()->id : \App\Models\Usuario::find($request->usuario_id)->id
        ]);
				
        if(isset($request->tipo) && $request->tipo != 'CAJA') {
          //suma a su saldo disponible
          $saldo = FondoRotatorioArea::find($fondo->id);
          $saldo->saldo_disponible += $request->cantidad;
          $saldo->save();
        }
				
        return $this->jsonResponse(['pago' => $pago, 'reintegro' => $reintegro, 'area' => $area, 'fondo' => $fondo->load('areaenlace.empleado.persona')], 'Reintegro creado correctamente', 201);
    }

    public function modificarViatico(Request $request, $id)
    {
        $pago = Pago::with(['viatico', 'tipo', 'detalles'])->find($id);
        if(!$pago && $pago->tipo->nombre != 'VIATICO') return $this->jsonResponse(null, 'No se encontro el pago', 404);

        try{
            DB::beginTransaction();
            
            $pago2 = Pago::find($id);
            $monto_total = 0.0;

            if($request->fecha_salida != null && $request->fecha_regreso != null){
                
                $pago->viatico->fecha_salida = $request->fecha_salida;
                $pago->viatico->fecha_regreso = $request->fecha_regreso;
                $pago->viatico->save();

                $detalle = $pago->detalles->where('partida_id', 121)->first();\Log::debug($detalle->monto / $request->diasAnterior);
                $detalle->monto = ($detalle->monto / $request->diasAnterior) * $request->numDias;
                $detalle->save();
                // \Log::debug($detalle);
            }else{
                $detalle = $pago->detalles->where('partida_id', 121)->first();
            }

            $monto_total += $detalle->monto;

            foreach($request->detalles as $d){
                if($d['partida_id'] != 121){
                    $detalleAnterior = $pago->detalles->where('id', $d['id'])->first();
                    $detalleAnterior->monto = $d['monto'];
                    $detalleAnterior->save();
                }
            }

            foreach($pago->load('detalles')->detalles as $d){
                if($d->partida_id != 121)
                    $monto_total += $d->monto;
            }
            $pago2->solicitante_empleado_id = $request->empleado_id;
            $pago2->monto_total = $monto_total;
            $pago2->save();

            DB::commit();
            return $this->jsonResponse($pago2->load(['viatico', 'tipo', 'detalles']), 'Pago actualizado correctamente', 200);
        }catch(\Exception $e){
            DB::rollBack();
            \Log::debug($e);
            return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 409);
        }
		}
		
		public function reintegroTotal(Request $request, $id, $tipo )
		{
			$estatusList = $this->estatusBancaDIF();
			$pago = Pago::find($id);
			if(!$pago) return $this->jsonResponse(null, 'No se encontro el pago', 404);

			$asignacion = AsignacionPago::where('pago_id',$id)->whereHas('asignacion.tipo', function ($q){$q->where('nombre','GASTO A COMPROBAR');})->count();
			if($asignacion >0 && $tipo != 'CAJA') return $this->jsonResponse(null, 'No se puede realizar reintegro total de GASTOS A COMPROBAR', 404);

			$reintegros = Reintegro::where('pago_id', $id)->get()->sum('cantidad');

			$totalReintegro = $pago->monto_total - $reintegros;
			$nuevoFolio = '';

			if($tipo == 'CAJA') {
				$gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first();
				$ultimoReintegro = Reintegro::whereHas('pago.asginacionpago', function($query) use($gasto) {$query->where('tipo_asignacion_id', $gasto->id);})->get()->last();
				if($ultimoReintegro){
					$string = explode('/', $ultimoReintegro->folio);
					$string[2]++;
					$nuevoFolio = implode('/', $string);
				}else {
					$nuevoFolio = 'CG/2019/1';
				}
			}else {
				$ultimoReintegro = Reintegro::get()->last();
				if(!$ultimoReintegro){
					
				}else {
					$nuevoFolio = '' . (int)$ultimoReintegro->folio + 1;
				}
			}

			$reintegro = Reintegro::create([
				'pago_id' => $pago->id,
				'cantidad' => $totalReintegro,
				'folio' => $nuevoFolio,
				'usuario_id' => $request->usuario_id,
			]);
			$resFondo = '';
			if($tipo == 'PAGO'){
				$fondo = FondoRotatorioArea::with('area')->find($pago->fondo_id);
				$fondo->saldo_disponible += $totalReintegro;
				$fondo->save();
				$resFondo = $fondo->historialenlace->empleado->persona->nombre.' '.$fondo->historialenlace->empleado->persona->primer_apellido. ' '.$fondo->historialenlace->empleado->persona->segundo_apellido;
			}

			$pago->estatus()->create([
				'estado_programa_id' => $estatusList->where('estatus','REINTEGRO TOTAL')->first()['id'],
				'fecha' => \Carbon\Carbon::now(),
				'usuario_id' => $request->usuario_id,
				'observacion' => null
			]);

			$area = DB::select('call getDireccion(?)', [$pago->empleado->area_id]);
			$empleado = $pago->empleado->persona->nombre.' '.$pago->empleado->persona->primer_apellido.' '.$pago->empleado->persona->segundo_apellido;
			

			return $this->jsonResponse(['pago' => $pago, 'reintegro' => $reintegro, 'area' => $area, 'empleado' => $empleado, 'areaE' => $pago->empleado->area->nombre, 'resFondo' =>$resFondo], 'Reintegro creado correctamente', 200);
		}

    public function actualizarMetodo(Request $request, $idpago)
    {
			$pago = Pago::find($idpago);
			if(!$pago) return $this->jsonResponse(null, 'No encontramos el pago', 404);

			$metodopago = MetodoPago::where('nombre', 'PAGO CAJA')->get()->first();
			if(!$metodopago) return $this->jsonResponse(null, 'No encontramos el metodo de pago', 404);

			try {
					DB::beginTransaction();

					$usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
					$usuario->bitacora($request, [
							'tabla' => 'siaf_pagos',
							'registro' => $pago->id,
							'campos' => json_encode($pago) . '',
							'metodo' => $request->method()
					]);
					
					$pago->metodopago_id = $metodopago->id;
					$pago->save();

					DB::commit();
					return $this->jsonResponse($pago, 'Pago Actualizado correctamente');

			}catch(\Exception $e) {
					DB::rollBack();
					\Log::debug($e);
					return new JsonResponse(['data' => null, 'message' => 'Error al actualizar el metodo de pago'], 409);
			}
	}

	public function reporteArea()
	{
		$fondos = FondoRotatorioArea::with('area')->get();
		$pagosAreas = collect([]);
		foreach($fondos as $f){
			$pagos = Pago::with('detalles')->where('fondo_id',$f->id)->where('tipopago_id',1)->get(); // pagos por fondo
			$sumatoriaPagos = 0;
			$reintegrosPago = 0;
			$totalPagos = 0;
			$julio = 0;
			$agosto = 0;
			$septiembre = 0;
			$julioR = 0;
			$agostoR = 0;
			$septiembreR = 0;
			$juliop = 0;
			$septiembrep = 0;
			$agostop = 0;
			foreach($pagos as $p){ // recorrido de los pago para sumar las cantidades y reintegros de cada pago
				if($p->estatuslast->estadoModulo->estatus->nombre != 'NUEVO' && $p->estatuslast->estadoModulo->estatus->nombre != 'RECHAZADO' && $p->estatuslast->estadoModulo->estatus->nombre != 'REINTEGRO TOTAL' && $p->estatuslast->estadoModulo->estatus->nombre != 'ENVIADO'){
					// $reintegro = Reintegro::where('pago_id',$p->id)->get()->sum('cantidad');
					foreach ($p->detalles as $d) {
						if($d->partida_id == 121){
							switch ($p->fecha_elaboracion->month) {
								case 7:
									$julio += $d->monto;
									// $julioR += $reintegro;
									$juliop ++;
									break;
								case 8:
									$agosto += $d->monto;
									// $agostoR += $reintegro;
									$agostop ++;
									break;
								case 9:
									$septiembre += $d->monto;
									// $septiembreR += $reintegro;
									$septiembrep ++;
									break;
							}
							$sumatoriaPagos += $d->monto;
						}
					}
					// $reintegrosPago += $reintegro;
					$totalPagos++;
				}
			}
			$pagosAreas->prepend(['solicitante'=> $f->area->nombre, 'monto'=> $sumatoriaPagos, 'reintegro'=>$reintegrosPago, 'numPagos'=>$totalPagos, 'julio'=>$julio, 'ReintregosJulio'=>$julioR, 'numPagosJulio'=>$juliop,'agosto'=>$agosto, 'ReintregosAgosto'=>$agostoR, 'numPagosAgosto'=>$agostop,'septiembre'=>$septiembre, 'ReintregosSeptiembre'=>$septiembreR, 'numPagosSeptiembre'=>$septiembrep]);
		}
		return $this->jsonResponse($pagosAreas, 'Reporte');
	}

}
