<?php

Route::group(['prefix' => 'fondorotatorio', 'namespace' => 'Modules\BancaDIF\Http\Controllers'], function()
{
    Route::group(['middleware' => 'web'], function(){
        Route::get('/', 'BancaDIFController@index');
        // Route::get('/spa/{vue_capture?}', 'BancaDIFController@index')->where('vue_capture', '[\/\w\.-]*');
    });

    Route::group(['middleware' => 'apiBancaDIF'], function(){
        Route::get('/permissions', 'BancaDIFController@permissions');
        Route::get('/fondo/{area_id}', 'BancaDIFController@fondo');
        Route::get('/areas/{id}', 'BancaDIFController@areas')->where('id', '[0-9]+');
        Route::get('/areas/{area_id}/enlaces', 'BancaDIFController@areasenlaces')->where('area_id', '[0-9]+');
        Route::get('/areas/sinfondo', 'BancaDIFController@areasSinfondo');
        Route::get('/bancos', 'BancaDIFController@bancos');
        Route::get('/utils/empleados/niveles', 'BancaDIFController@niveles');
        Route::get('/utils/empleados/tipoempleado', 'BancaDIFController@tipoempleado');
        Route::get('/utils/areas/{area}', 'BancaDIFController@uareas');
        Route::get('/utils/localidades/{localidad}', 'BancaDIFController@localidadMunicipio');
        Route::get('/programas/{nombre}/{fondo?}', 'BancaDIFController@programas');
        Route::get('/firmas/{area_id}/{area_empleado}', 'BancaDIFController@firmas');
        Route::get('/partidas/{partida?}', 'BancaDIFController@partidas');
        Route::get('/municipios/{municipio}/{region_id?}', 'BancaDIFController@municipios');
        Route::get('/municipios/{municipios_ids}/localidades/{localidad}', 'BancaDIFController@localidades');
        Route::get('/cuotas', 'BancaDIFController@cuotas');
        Route::get('/cuotas/{origen_id}/{destino_id}', 'BancaDIFController@cuotasMunicipio');
        Route::get('/rotatorio', 'FondoRotatorioController@index');
        Route::post('/rotatorio', 'FondoRotatorioController@store');
        Route::put('/rotatorio/{fondo}', 'FondoRotatorioController@update');
        Route::delete('/rotatorio/{fondo}', 'FondoRotatorioController@destroy');
        Route::post('/asignaciones', 'FondoRotatorioController@asignacion');
        Route::get('/regiones', 'FondoRotatorioController@regiones');
        Route::get('/entidades/{entidad}', 'FondoRotatorioController@entidades');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'empleados'], function(){
        Route::get('/noenlace', 'EmpleadoController@empleadosNoenlace');
        Route::get('/{nombre}', 'EmpleadoController@empleados');
        Route::get('/rfc/{rfc}', 'EmpleadoController@empleadosRFC');
        Route::post('/', 'EmpleadoController@store');
    });

    Route::group(['middleware' => 'apiBancaDIF','prefix' => 'caja'], function(){
        Route::get('/busqueda/{id}', 'ComprobacionCajaController@findComprobacion'); 
        Route::get('/list', 'ComprobacionCajaController@lista'); 
        Route::post('/cambioEstatus', 'ComprobacionCajaController@changeStatus'); 
        Route::put('/pago/{pagoId}', 'ComprobacionCajaController@pagarPago'); 
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'asignaciones'], function(){
        Route::post('/', 'AsignacionesController@store');
        Route::delete('/{asignacion_id}', 'AsignacionesController@destroy');
        Route::get('/lista/{areaId}', 'AsignacionesController@list'); // solo te regresa las asignaciones de su area
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'pagos'], function(){
        Route::get('/', 'PagosController@index');
        Route::post('/', 'PagosController@store');
        Route::get('/viaticos', 'PagosController@viaticos');
        Route::get('/areas/{idarea}', 'PagosController@gastos');
        Route::post('/pagar', 'PagosController@pagar');
        Route::get('/show/{idpago}', 'PagosController@show');
        Route::post('/rechazar', 'PagosController@rechazarPago');
        Route::get('/{tipo}/realizados/{empleado_id}', 'PagosController@realizados');
        Route::get('/tramites/{area_id}/{tipopago_id}', 'PagosController@tramites');
        Route::get('/buscar/{tipo}/{folio}/{area}', 'PagosController@buscar');
        Route::put('/{id}/fecha', 'PagosController@actualizarFecha');
        Route::post('/{id}/enviar', 'PagosController@enviar');
        Route::get('/adicional/{id}', 'PagosController@pagoAdicional');
        Route::post('/{id}/reintegro', 'PagosController@reintegro');
        Route::put('/{id}/viatico', 'PagosController@modificarViatico');
        Route::put('/reintegroT/{id}/{tipo}', 'PagosController@reintegroTotal');
        Route::put('/{idpago}/metodopago', 'PagosController@actualizarMetodo')->where('idpago', '[0-9]+');
        Route::get('/reporteArea', 'PagosController@reporteArea');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'tipoPago'], function(){
        Route::get('/lista', 'TipoPagoController@show');
        Route::get('/editar/{pago}', 'TipoPagoController@edit');
        Route::delete('/eliminar/{pago}', 'TipoPagoController@destroy');
        Route::post('/save', 'TipoPagoController@store');
        Route::post('/update', 'TipoPagoController@update');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'metodoPago'], function(){
        Route::get('/lista', 'MetodoPagoController@show');
        Route::get('/editar/{pago}', 'MetodoPagoController@edit');
        Route::delete('/eliminar/{pago}', 'MetodoPagoController@destroy');
        Route::post('/save', 'MetodoPagoController@store');
        Route::post('/update', 'MetodoPagoController@update');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'enlaceFinanciero'], function(){
        Route::get('/lista', 'EnlaceFinancieroController@show');
        Route::get('/editar/{pago}', 'EnlaceFinancieroController@edit');
        Route::delete('/eliminar/{pago}', 'EnlaceFinancieroController@destroy');
        Route::post('/save', 'EnlaceFinancieroController@store');
        Route::post('/update', 'EnlaceFinancieroController@update');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'tipoAsignacion'], function(){
        Route::get('/lista', 'TiposAsignacionesController@lista');
    });

    Route::group(['middleware' => 'apiBancaDIF', 'prefix' => 'comprobacion'], function(){
        Route::post('/save', 'ComprobacionesController@store');
        Route::get('/pagos/{idpago}/archivos', 'ArchivosController@obtenerArchivo');
        Route::post('/pagos/{idpago}/archivos/{idpartida}', 'ArchivosController@subirArchivo')->where(['idpago' => '[0-9]+', 'idpartida' => '[0-9]+']);
        Route::post('/pagos/{idpago}/sinarchivos/{idpartida}', 'ArchivosController@subirSinArchivo')->where(['idpago' => '[0-9]+', 'idpartida' => '[0-9]+']);
        Route::delete('/pagos/{idpago}/archivos/{idarchivo}', 'ArchivosController@eliminarArchivo');
        Route::put('/pagos/{idpago}', 'ComprobacionesController@mandarComprobacion');
        Route::put('/cambioestado/{idpago}/{estado}', 'ComprobacionesController@cambioestado');
        Route::get('/lista/{id_usuario}', 'ComprobacionesController@lista');
        Route::get('/show/{id_comprobacion}', 'ComprobacionesController@edit');
        Route::get('/{tipo}/list', 'ComprobacionesController@listall');
        Route::post('/change', 'ComprobacionesController@changeComprobacion');
        Route::post('/quitarPago', 'ComprobacionesController@removePago');
        Route::post('/cambioEstatus', 'ComprobacionesController@cambioStatus'); // cambio de estatus de la comprobacion
        Route::delete('/reintegro/{idreintegro}', 'ArchivosController@eliminarReintegro');
        Route::post('/creartramitesiaf', 'TramiteController@crearTramite');
        Route::post('/creartramitegasto', 'TramiteController@crearTramiteGasto');
    });

    Route::group(['prefix' => 'asignacionesPagos'], function(){
        Route::get('/listapagos/{tipo}', 'AsignacionesPagosController@listapagos'); // para obtener los gastos de una asignacion en especifica (Gastos a comprobar)
    });

    Route::group(['prefix' => 'banco'], function(){
        Route::get('/', 'BancosController@bancos');
        Route::get('/lista', 'BancosController@lista');
        Route::get('/cuentas/{search}', 'BancosController@cuentas');
        Route::put('/pago/{pagoId}/{bancoId}/{fecha}/{usuarioId}', 'BancosController@pagarPago'); 
    });

    Route::group(['prefix' => 'reportes'], function(){
        Route::get('/gastos', 'ReportesController@gastos');
        Route::get('/pagosReintegro/{fechaInicio}/{fechaFin}', 'ReportesController@pagoReintegro');
    });
		
    Route::group(['prefix' => 'proveedores'], function(){
        Route::post('/save', 'ProveedoresController@store');
        Route::get('/lista/{dato}', 'ProveedoresController@lista');
        Route::get('/edit/{id}', 'ProveedoresController@edit');
        Route::put('/update/{id}', 'ProveedoresController@update');
        Route::get('/buscar/{proveedor}', 'ProveedoresController@buscar');
    });
    
    
    Route::group(['prefix' => 'reintegros'], function(){
        Route::get('/lista/{userId}', 'ReintegrosController@show');
        Route::get('/find/{idReintegro}', 'ReintegrosController@edit');        
    });

});
