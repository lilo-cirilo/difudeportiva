<?php

namespace Modules\BancaDIF\Traits;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\JsonResponse;

use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Asignacion;
use Modules\BancaDIF\Entities\Pago;

use Modules\BancaDIF\Rules\DifferenceRule;

trait PagoValidate{

    public function validateStorePago(Request $request)
    {
        $reglas = null;
        if($request->tipopago_id == 1){
            //reglas para viatico
            $reglas = [
                'asignacion_id' => 'integer|exists:siaf_asignaciones_gastos,asignacion_id',
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required',
                'descripcion' => 'required|string',
                // 'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'tarjeta' => [new DifferenceRule(1)],
                // 'banco_id' => 'required_unless::asignacion_id,1',
                'estado_programa_id' => 'required|integer',
                'clavepresupuestal_id' => 'required|integer',
                'estatal' => 'required|boolean',
                'origen_municipio_id' => 'required_if:estatal,true|integer',
                'municipiosDestino' => 'required_if:estatal,true|array',
                'origenentidad_id' => 'required_if:estatal,false|integer',
                'destinoentidad_id' => 'required_if:estatal,false|integer',
                'origenregion_id' => 'required_if:estatal,false|integer',
                'destinociudad' => 'required_if:estatal,false',
                'fecha_salida' => 'required|date',
                'fecha_regreso' => 'required|date',
                'localidades' => 'required_if:estatal,array',
                'transporte_id' => 'required|integer|exists:siaf_cat_tipostransportes,id',
                'placas' => 'required_if:transporte_id,4',
                'fondo_id' => 'required|integer'
            ];
        }
        if($request->tipopago_id == 2){
            //reglas para vale
            $reglas = [
                'asignacion_id' => 'integer|exists:siaf_asignaciones_gastos,asignacion_id',
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required',
                'descripcion' => 'required|string',
                // 'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array|size:1',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'num_tarjeta' => 'required_if:transferencia,true|numeric',
                // 'banco_id' => 'required_if:transferencia,true',
                'estado_programa_id' => 'required|integer',
                'clavepresupuestal_id' => 'required|integer',
                'fondo_id' => 'required|integer'
            ];
        }
        if($request->tipopago_id == 3){
            $reglas = [
                'asignacion_id' => 'integer|exists:siaf_asignaciones_gastos,asignacion_id',
                'tramite' => 'required|integer',
                'solicitante_empleado_id' => 'required|integer',
                'monto_total' => 'required',
                'descripcion' => 'required|string',
                // 'metodopago_id' => 'required|integer',
                'tipopago_id' => 'required|integer',
                'autorizo' => 'required|integer',
                'vobo' => 'required|integer',
                'programa_id' => 'required|integer',
                'detalle' => 'required|array',
                'detalle.*.partida_id' => 'required|integer',
                'detalle.*.monto' => 'required',
                // 'transferencia' => 'boolean',
                // 'titular' => 'required_if:transferencia,true|string',
                // 'num_tarjeta' => 'required_if:transferencia,true|numeric',
                // 'banco_id' => 'required_if:transferencia,true',
                'estado_programa_id' => 'required|integer',
                'clavepresupuestal_id' => 'required|integer',
                'estatal' => 'required|boolean',
                'origen_municipio_id' => 'required|integer',
                'municipiosDestino' => 'required|array',
                'fecha_salida' => 'required|date',
                'fecha_regreso' => 'required|date',
                'localidades' => 'required|array',
                'num_comidas' => 'required|integer',
                'fondo_id' => 'required|integer'
            ];
        }
        $validator = Validator::make($request->all(), $reglas);
        if ($validator->fails()) {
            \Log::debug($validator->errors());
            return $validator->errors();
        }
        return null;
    }

    public function validarViaticos($empleado, $fecha_salida, $fecha_regreso)
    {
        $viaticos = Pago::where('solicitante_empleado_id', $empleado->id)
            ->whereHas('viatico', function($query) use($fecha_salida, $fecha_regreso){ 
                $query->where(function($query) use($fecha_salida, $fecha_regreso){
                    $query->where('fecha_salida', '<=', $fecha_salida)
                        ->orWhere(function ($query) use($fecha_salida, $fecha_regreso){
                            $query->where('fecha_regreso', '<=', $fecha_regreso);
                            $query->where('fecha_salida', '>=', $fecha_salida);});
                })->whereBetween('fecha_regreso', [$fecha_salida, $fecha_regreso]); })
            ->count();
        if(!$viaticos) return true;
        return false;
    }

    public function validateSaldoPago(FondoRotatorioArea $fondo = null, $tramite, $monto_total)
    {
        if($tramite != 1){
            $asignacion = Asignacion::with('gasto')->find($tramite);
            if($asignacion->gasto->saldo_disponible < $monto_total){
                return 'No tiene saldo suficiente. ($'. $asignacion->gasto->saldo_disponible .')';
            }
            $asignacion->gasto->saldo_disponible -= $monto_total;
            $asignacion->gasto->save();
        }else{
            if($fondo->saldo_disponible < $monto_total){
                return 'No tiene fondo suficiente. ($'. $fondo->saldo_disponible .')';
            }
            $fondo->saldo_disponible -= $monto_total;
            $fondo->save();
        }
        return null;
    }
}