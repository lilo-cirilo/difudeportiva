<?php

namespace Modules\BancaDIF\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use App\Models\Area;
use Modules\BancaDIF\Entities\Pago;
use Modules\BancaDIF\Entities\FondoRotatorioArea;
use Modules\BancaDIF\Entities\Estatus;
use Modules\BancaDIF\Entities\TiposAsignaciones;
use Modules\BancaDIF\Entities\Reintegro;

trait Util
{
    /**
    * Encontrar recursivamente el area con fondo rotatorio
    */
    public function areaFondo($area_id)
    {
        $fondo = FondoRotatorioArea::where('area_id', $area_id)->first();
        if($fondo == null && $area_id != null){
            return $this->areaFondo(Area::find($area_id)->padre_id);
        }
        return $fondo;
    }

    public function nuevoFolio($area_id, $tipopago_id)
    {
        $areas = DB::select('call getAreasHijas(?)', [$area_id]);
        $ids = [];
        foreach($areas as $area){
            array_push($ids, $area->id_hijo);
        }
        array_push($ids, $area_id);
        $fondo = $this->areaFondo($area_id);
        // $pago = Pago::where('tipopago_id', $tipopago_id)->whereHas('empleado', function($query) use($ids){ $query->whereIn('area_id', $ids); })->get()->last();
        $pago = Pago::where('tipopago_id', $tipopago_id)->where('fondo_id', $fondo->id)->get()->last();
        if($pago == null){
            $area = Area::find($fondo->area_id);
            if($tipopago_id == 1)
                $c = 'C';
            if($tipopago_id == 2)
                $c = 'V';
            if($tipopago_id == 3)
                $c = 'G';
            return $area->siglas . '/' . \Carbon\Carbon::now()->format('Y') . '/' . $c . '/1';
        }
        $string = explode('/', $pago->folio);
        $string[3]++;
        return implode('/', $string);
    }

    public function jsonResponse($data = null, $message = '', $status = 200)
    {
        return new JsonResponse(['data' => $data, 'message' => $message], $status);
    }

    // Metodo para obtener la direrccion que tiene fondo 
    public function areasHijas($areaId)
    {
        $area_id = $this->areaFondo($areaId);
        $areas = DB::select('call getAreas(?)', [$area_id->area_id]);
        $ids = [];
        array_push($ids, $area_id->area_id);
        foreach($areas as $area){
            // $fondo = FondoRotatorioArea::where('area_id', $area_id)->first();
            if(FondoRotatorioArea::where('area_id', $area->area_id)->first() == null)
                array_push($ids, $area->area_id);
        }
        return $ids;
    }

    //Funcion que regresa los estatus del modulo bancadif
    public function estatusBancaDIF(string $estatus = null)
    {
        $sql = Estatus::with('estatus')->whereHas('modulo', function ($q){$q->where('nombre','FONDO ROTATORIO');});
        if($estatus != null){
            $sql->whereHas('estatus', function($query) use($estatus){ $query->where('nombre', $estatus); });
        }
        $estatusLista = $sql->get();
        $status = collect([]);
        foreach ($estatusLista as $s) {
            $status->prepend(['id' => $s->id, 'estatus'=> $s->estatus->nombre]);
        }
        return $status;
    }

    public function areaMenorOficina($area_id){
        $area = Area::find($area_id);
        if($area->tipoarea_id >= 5)
            return $this->areaMenorOficina($area->padre_id);
        return $area;
    }

    public function esGastoComprobar(Pago $pago)
    {
        $p = $pago->asginacionpago()->get()->first();
        if(!$p) return 'No encontrado';

        if($p->tipo->nombre == 'GASTO A COMPROBAR')
            return true;
        else
            return false;
    }

    public function folioReinegro()
    {
        $gasto = TiposAsignaciones::where('nombre','GASTO A COMPROBAR')->first();
        // $ultimoReintegro = Reintegro::whereHas('pago.asginacionpago', function($query) use($gasto) {
        //     $query->where('tipo_asignacion_id', '!=', $gasto->id);
        // })->get()->last();
        $ultimoReintegro = Reintegro::where('folio', 'NOT LIKE', '%CG/2019%' )->get()->last();
        if(!$ultimoReintegro)
            return '1';
        else 
            return '' . (int)$ultimoReintegro->folio + 1;
    }
}
?>