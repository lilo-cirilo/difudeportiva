<?php

namespace Modules\BancaDIF\Patterns\Strategy;

use Modules\BancaDIF\Entities\Viatico;
use Modules\BancaDIF\Entities\TipoTransporte;
use Modules\BancaDIF\Entities\Transporte;
use Modules\BancaDIF\Entities\Tabulador;
use App\Models\Entidad;

class ViaticoPago implements PagoStrategy{

    public function create($data)
    {
        $transporte_id = $this->findCreateTransporte($data);
        $viatico = Viatico::create([
            'pago_id' => $data['pago_id'],
            'fecha_salida' => $data['fecha_salida'],
            'fecha_regreso' => $data['fecha_regreso'],
            'gastos_alimentarios' => false,
            'num_comidas' => 0,
            'usuario_id' => $data['usuario_id'],
            'tipo' => $data['estatal'] ? 'ESTATAL' : 'NACIONAL',
            'clavepresupuestal_id' => $data['clavepresupuestal_id'],
            'transporte_id' => $transporte_id,
            'presentarse' => strtoupper($data['presentarse'])
        ]);
        if($data['estatal']){
            foreach($data['localidades'] as $localidad){
                $viatico->estatal()->create([
                    'origenmunicipio_id' => $data['origen_municipio_id'],
                    'destinomunicipio_id' => $localidad['municipio_id'],
                    'destinolocalidad_id' => $localidad['id'],
                    'localidad' => $localidad['nombre'],
                ]);
            }
        }else{
            $tabulador = Tabulador::find($data['destinoentidad_id']);
            $entidad = Entidad::where('entidad', 'like', $tabulador->estado)->first();
            $viatico->nacional()->create([
                'origenentidad_id' => $data['origenentidad_id'],
                'destinoentidad_id' => $entidad->id,
                'origenregion_id' => $data['origenregion_id'],
                'destinociudad' => strtoupper($data['destinociudad']),
            ]);
        }
        return $viatico->load(['estatal', 'nacional']);
    }

    public function findCreateTransporte($data){
        $transporte = TipoTransporte::find($data['transporte_id']);
        if($transporte == null)
            throw new Exception('No se encontró el transporte seleccionado');
        if($transporte->nombre == 'VEHÍCULO OFICIAL' || $transporte->nombre == 'OTRO')
            $auto = Transporte::firstOrCreate([
                'tipotransporte_id' => $transporte->id,
                'placas' => $data['placas']
            ]);
        return isset($auto) ? $auto->id : $transporte->id;
    }

}