<?php

namespace Modules\BancaDIF\Patterns\Strategy;

use Modules\BancaDIF\Patterns\Strategy\ViaticoPago;
use Modules\BancaDIF\Patterns\Strategy\ValePago;
use Modules\BancaDIF\Patterns\Strategy\GastoComisionPago;
use Modules\BancaDIF\Entities\Pago;

class CrearPago{

    public $tipospagos = [
        1 => ViaticoPago::class,
        2 => ValePago::class,
        3 => GastoComisionPago::class,
    ];

    public function crearPagos(string $tipopago, $pago)
    {
        $estrategia = new $this->tipospagos[$tipopago];
        $pagocontext = new PagoContext($estrategia);
        return $pagocontext->createPago($pago);
    }
}