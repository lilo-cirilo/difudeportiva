<?php

namespace Modules\BancaDIF\Patterns\Strategy;

interface PagoStrategy{

    public function create($data);
}