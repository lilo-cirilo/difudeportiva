<?php

namespace Modules\BancaDIF\Patterns\Strategy;
use Modules\BancaDIF\Entities\Viatico;

class GastoComisionPago implements PagoStrategy{

    public function create($data)
    {
        $viatico = Viatico::create([
            'pago_id' => $data['pago_id'],
            'fecha_salida' => $data['fecha_salida'],
            'fecha_regreso' => $data['fecha_regreso'],
            'gastos_alimentarios' => $data['gastos_alimentarios'],
            'num_comidas' => $data['num_comidas'],
            'usuario_id' => $data['usuario_id'],
            'tipo' => 'ESTATAL',
            'clavepresupuestal_id' => $data['clavepresupuestal_id'],
            'transporte_id' => 1,
            'presentarse' => $data['presentarse']
        ]);
        foreach($data['localidades'] as $localidad){
            $viatico->estatal()->create([
                'origenmunicipio_id' => $data['origen_municipio_id'],
                'destinomunicipio_id' => $localidad['municipio_id'],
                'destinolocalidad_id' => $localidad['id'],
                'localidad' => $localidad['nombre'],
            ]);
        }
        return $viatico->load(['estatal', 'nacional']);
    }
}