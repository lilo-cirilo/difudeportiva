<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViaticoEstatal extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_viaticoestatal';
    // protected $primaryKey = null;
    protected $fillable = ['origenmunicipio_id', 'destinomunicipio_id', 'destinolocalidad_id', 'localidad'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function municipio (){
        return $this->hasOne('App\Models\Municipio','id','destinomunicipio_id');
    }
}
