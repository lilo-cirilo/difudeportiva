<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnidadEjecutora extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_unidadesejecutoras';
    protected $fillable = [];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
