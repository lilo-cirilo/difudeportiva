<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComprobacionesPagos extends Model
{
	use SoftDeletes;
	protected $table = 'siaf_comprobaciones_estados';
	protected $fillable = ['comprobacion_id', 'pago_id'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

}
