<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Archivo extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_archivos';
    protected $fillable = ['url', 'nombre', 'mime', 'size', 'usuario_id', 'beneficiario', 'folio_comprobante', 'fecha', 'importe', 'isr', 'detallespago_id', 'proveedor_id'];
		protected $dates = ['created_at', 'updated_at', 'deleted_at'];
		

		public function proveedor()  
    {
      return $this->belongsTo('Modules\BancaDIF\Entities\Proveedores', 'proveedor_id','id');
    }

}
