<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'siaf_cat_proveedores';
    protected $fillable = ['rfc', 'nombre', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'localidad_id', 'municipio_id', 'codigo_postal', 'email'];

}
