<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;

class TramiteDetalle extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'TbFacProveedor';
    protected $primaryKey = 'Cp_Id';
    protected $guarded = [];

    public $timestamps = false;
}