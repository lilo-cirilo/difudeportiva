<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CuentasBancos extends Model
{
	use SoftDeletes;
	protected $table = 'siaf_num_cuentas';
	protected $fillable = ['ejercicio', 'num_cuenta','concepto','banco_id'];
	protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
