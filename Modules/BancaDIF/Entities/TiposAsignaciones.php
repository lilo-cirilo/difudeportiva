<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TiposAsignaciones extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_cat_tipos_asignaciones';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['nombre'];

}
