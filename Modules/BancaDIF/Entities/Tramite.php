<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'TbRecepcion';
    protected $primaryKey = 'Cp_Ind';
    protected $guarded = [];
    
    public $timestamps = false;
}
