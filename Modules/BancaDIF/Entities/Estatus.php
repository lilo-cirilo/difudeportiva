<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estatus extends Model
{
    use SoftDeletes;
    protected $table = 'estados_modulos';
    protected $fillable = [];

    public function estatus(){
        return $this->belongsTo('Modules\BancaDIF\Entities\Estado', 'estado_id', 'id');
    }

    public function modulo(){
        return $this->belongsTo('App\Models\Modulo', 'modulo_id', 'id')->where('nombre', 'FONDO ROTATORIO');
    }
}
