<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transporte extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_transportes';
    protected $fillable = ['tipotransporte_id', 'placas'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function detalle(){
        return $this->hasOne('Modules\BancaDIF\Entities\TipoTransporte', 'id','tipotransporte_id');
    }
}
