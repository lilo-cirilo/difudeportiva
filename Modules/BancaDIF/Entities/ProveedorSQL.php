<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;

class ProveedorSQL extends Model
{
	protected $connection = 'sqlsrv_proveedor';
	protected $table = 'provedor';
  protected $primaryKey = 'cp_id';
  protected $guarded = [];
    
  public $timestamps = false;
}
