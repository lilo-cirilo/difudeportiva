<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ViaticoNacional extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_viaticonacional';
    // protected $primaryKey = null;
    protected $fillable = ['origenentidad_id' ,'destinoentidad_id', 'origenregion_id', 'destinociudad'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
