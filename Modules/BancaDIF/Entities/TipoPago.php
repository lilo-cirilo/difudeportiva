<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_cat_tipospago';
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $fillable = ['nombre'];
}
