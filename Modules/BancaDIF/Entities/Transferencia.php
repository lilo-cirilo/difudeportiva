<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transferencia extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_transferenciaspago';
    protected $fillable = ['pago_id', 'titular', 'num_tarjeta', 'banco_id', 'usuario_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
