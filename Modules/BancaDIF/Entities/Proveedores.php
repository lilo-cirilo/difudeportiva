<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedores extends Model
{
	use SoftDeletes;
	protected $table = 'siaf_cat_proveedores';
	protected $fillable = ['rfc','nombre','calle','numero_exterior','numero_interior','colonia','localidad_id','municipio_id','codigo_postal','email','tipo'];
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
