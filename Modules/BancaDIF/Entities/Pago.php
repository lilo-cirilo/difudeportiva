<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_pagos';
    protected $fillable = ['fecha_elaboracion', 'solicitante_empleado_id', 'monto_total', 'descripcion', 'metodopago_id', 'tipopago_id', 'autorizo_empleado_id', 'vobo_empleado_id', 'programa_id', 'usuario_id', 'folio', 'fondo_id'];
    protected $dates = ['fecha_elaboracion', 'created_at', 'updated_at', 'deleted_at'];

    public function detalles()
    {
      return $this->hasMany('Modules\BancaDIF\Entities\DetallePago', 'pago_id', 'id');
    }

    public function viatico()
    {
      return $this->hasOne('Modules\BancaDIF\Entities\Viatico', 'pago_id', 'id');
    }

    public function empleado()
    {
      return $this->belongsTo('App\Models\Empleado', 'solicitante_empleado_id', 'id');
    }

    public function vobo()
    {
      return $this->belongsTo('App\Models\Empleado', 'vobo_empleado_id', 'id');
    }

    public function estatus()
    {
      return $this->hasMany('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id');
    }

    public function autoriza()
    {
      return $this->belongsTo('App\Models\Empleado', 'autorizo_empleado_id', 'id');
    }

    public function tipo()
    {
      return $this->belongsTo('Modules\BancaDIF\Entities\TipoPago', 'tipopago_id', 'id');
    }

    public function metodo() // metodo de pago
    {
      return $this->belongsTo('Modules\BancaDIF\Entities\MetodoPago', 'metodopago_id', 'id');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }
    
    public function programa(){
    	return $this->belongsTo('App\Models\Programa');
    }
    
    public function estatuslast()
    {
      return $this->hasOne('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id')->latest();
    }

    public function asginacionpago()
    {
      return $this->belongsToMany('Modules\BancaDIF\Entities\Asignacion', 'siaf_asignaciones_pagos', 'pago_id','asignacion_id')->withPivot('no_tramite');
    }

    public function tipoasignacion()
    {
      return $this->hasMany('Modules\BancaDIF\Entities\AsignacionPago', 'pago_id', 'id');
		}
		
		public function observaciones()
    {
      return $this->hasMany('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id')->whereNotNull('siaf_estados_pagos.observacion');
    }

    // public function archivos()
    // {
    //     return $this->belongsToMany('Modules\BancaDIF\Entities\Archivo', 'siaf_detallespago', 'pago_id','detallespago_id');
    // }

    public function comprobacion()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\ComprobacionesPagos', 'id','pago_id');
    }
		
    public function fondo()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\FondoRotatorioArea', 'fondo_id', 'id');
		}
		// estatus revision
		public function revison()
    {
      return $this->hasOne('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id')->select('fecha')->where('estado_programa_id',15)->oldest();
		}
		// estatus finalizado
		public function finalizado()
    {
      return $this->hasOne('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id')->select('fecha')->where('estado_programa_id',12)->oldest();
		}
		// estatus rechazado
		public function rechazado()
    {
      return $this->hasOne('Modules\BancaDIF\Entities\EstadoPago', 'pago_id', 'id')->select('fecha')->where('estado_programa_id',9)->oldest();
		}
		// datos de la cuenta,
		public function cuenta()
    {
      return $this->belongsToMany('Modules\BancaDIF\Entities\CuentasBancos', 'siaf_pagos_cuentas', 'pago_id','numcuenta_id');
    }

}
//1	COMP/1/2019	4	2	103	4	1250	2		2019-08-07 13:46:29	2019-08-07 13:46:29	