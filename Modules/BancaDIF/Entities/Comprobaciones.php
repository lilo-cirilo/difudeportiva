<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comprobaciones extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_comprobaciones';
    protected $fillable = ['folio', 'fondorotatorio_id', 'tipo_asignacion_id', 'usuario_id','estado_modulo_id','monto_total','tipopago_id'];
    protected $hidden = [ 'updated_at', 'deleted_at'];

    public function estatus(){
        return $this->belongsTo('Modules\BancaDIF\Entities\Estatus', 'estado_modulo_id', 'id');
    }
    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }
    public function asignacion(){
    	return $this->belongsTo('Modules\BancaDIF\Entities\TiposAsignaciones','tipo_asignacion_id','id');
    }
    public function pagos(){
        return $this->belongsToMany('Modules\BancaDIF\Entities\Pago','siaf_comprobaciones_estados','comprobacion_id','pago_id')->where('siaf_comprobaciones_estados.deleted_at',null);
        // return $this->belongsToMany('App\Models\RecmPresentacionProducto','recm_orden_compra_productos','orden_compra_id','presentacion_producto_id')->withPivot('cantidad','precio_unitario_real','subtotal');
    }
    public function fondo(){
    	return $this->belongsTo('Modules\BancaDIF\Entities\FondoRotatorioArea','fondorotatorio_id','id');
    }

    public function caja(){
        return $this->hasOne('Modules\BancaDIF\Entities\ComprobacionCaja', 'comprobacion_id', 'id');
    }
}
