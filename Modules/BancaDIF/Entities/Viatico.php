<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Viatico extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_comisionesoficiales';
    protected $fillable = ['pago_id', 'fecha_salida', 'fecha_regreso', 'gastos_alimentarios', 'num_comidas', 'usuario_id', 'tipo', 'clavepresupuestal_id', 'transporte_id','presentarse'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function estatal(){
        return $this->hasMany('Modules\BancaDIF\Entities\ViaticoEstatal', 'viatico_id', 'id');
    }

    public function nacional(){
        return $this->hasOne('Modules\BancaDIF\Entities\ViaticoNacional', 'viatico_id', 'id');
    }
    public function clave(){
        // return $this->belongsTo('Modules\BancaDIF\Entities\UnidadEjecutora', 'clavepresupuestal_id','id');
        return $this->belongsTo('Modules\BancaDIF\Entities\ClavePresupuesto', 'clavepresupuestal_id','id');
    }
    public function transporte(){
        return $this->hasOne('Modules\BancaDIF\Entities\Transporte', 'id','transporte_id');
    }
}
