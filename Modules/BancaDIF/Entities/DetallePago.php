<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DetallePago extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_detallespago';
    protected $fillable = ['pago_id', 'partida_id', 'monto', 'usuario_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function partida(){
    	return $this->belongsTo('App\Models\RecmCatPartida');
	}

    public function archivos(){
        return $this->hasMany('Modules\BancaDIF\Entities\Archivo', 'detallespago_id', 'id');
    }

    public function reintegros(){
        return $this->hasMany('Modules\BancaDIF\Entities\Reintegro', 'detalle_id', 'id');
    }
}
