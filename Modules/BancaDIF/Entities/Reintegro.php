<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reintegro extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_reintegros';
    protected $fillable = ['pago_id', 'folio', 'cantidad', 'detalle_id', 'usuario_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function pago() {
      return $this->belongsTo('Modules\BancaDIF\Entities\Pago', 'pago_id', 'id');
    }
}
