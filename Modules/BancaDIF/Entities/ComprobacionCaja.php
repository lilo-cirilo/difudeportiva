<?php

namespace Modules\BancaDIF\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ComprobacionCaja extends Model
{
    use SoftDeletes;
    protected $table = 'siaf_comprobacion_caja';
    protected $fillable = ['folio', 'comprobacion_id', 'monto', 'no_tramite'];
    protected $hidden = [ 'updated_at', 'deleted_at'];

    public function estatuslast()
    {
        return $this->hasOne('Modules\BancaDIF\Entities\ComprobacionCajaHistorial', 'comprobacioncaja_id', 'id')->latest();
    }

    public function comprobacion()
    {
        return $this->belongsTo('Modules\BancaDIF\Entities\Comprobaciones', 'comprobacion_id', 'id')->latest();
    }
}
