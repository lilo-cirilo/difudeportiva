let mix = require('laravel-mix');

// mix.js('Resources/src/js/app.js', 'Resources/assets/js/')
//   .sass('Resources/src/sass/app.scss', 'Resources/assets/css/')
  // .options({
  //     processCssUrls: false
  // })
  // .setPublicPath('./../../public/bancadif');
  // .setPublicPath('Resources/assets/');

mix.js('Resources/assets/js/app.js', './../../public/bancadif/js')
  .sass('Resources/assets/sass/app.scss', './../../public/bancadif/css')
  .setPublicPath('./../../public/bancadif');

// mix.browserSync('intradif.test:90');