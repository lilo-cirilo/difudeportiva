<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Instructor;
use Modules\UnidadDeportiva\Entities\RFC\RfcBuilder;

class InstructorUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $InstructorNuevo = new Instructor;
        $InstructorNuevo->rfc = $request->rfc;
        $InstructorNuevo->nombre = $request->nombre;
        $InstructorNuevo->apellido_uno = $request->apellido_uno;
        $InstructorNuevo->apellido_dos = $request->apellido_dos;
        $InstructorNuevo->correo = $request->correo;
        $InstructorNuevo->telefono = $request->telefono;
        $InstructorNuevo->fecha_nacimiento = $request->fecha_nacimiento;
        $InstructorNuevo->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $instructores = Instructor::all();
        return [
                'instructores' => $instructores
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $llave = $request->id;
        $instructorAModificar = Instructor::find($llave);
        $instructorAModificar->rfc = $request->rfc;
        $instructorAModificar->nombre = $request->nombre;
        $instructorAModificar->apellido_uno = $request->apellido_uno;
        $instructorAModificar->apellido_dos = $request->apellido_dos;
        $instructorAModificar->correo = $request->correo;
        $instructorAModificar->telefono = $request->telefono;
        $instructorAModificar->save();
        return [
                 'respuesta' => 'Los datos se actualizaron correctamente'
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $llave = $request->id;
        $instructorAEliminar = Instructor::find($llave);
        Instructor::destroy($llave);
        return [
                 'respuesta' => $instructorAEliminar
        ];
    }

    public function getRFC(Request $request){
        $builder = new RfcBuilder();
        $rfc = $builder->name($request->nombre)
        ->firstLastName($request->apeuno)
        ->secondLastName($request->apedos)
        ->birthday($request->dia, $request->mes, $request->anio)
        ->build()
        ->toString();
        return [
            'respuesta' => $rfc
        ];

        //Personas morales
        // $builder = new RfcBuilder();
        // $rfc = $builder->legalName('AUTOS PULLMAN, S.A. DE C.V.')
        //  ->creationDate(30, 9, 1964)
        //  ->build()
        //  ->toString();
    }

    public function consultarInstructor(Request $request){
        $llave = $request->rfc;
        $instructor = Instructor::where('rfc','=',$llave)->get();
        return [ 'respuesta' => $instructor ];
    }
}
