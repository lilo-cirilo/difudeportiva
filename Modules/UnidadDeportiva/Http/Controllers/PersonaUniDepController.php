<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Persona;
use Modules\UnidadDeportiva\Entities\Socio;
use Illuminate\Support\Facades\DB;

class PersonaUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $newPerson = new Persona;
        $newPerson->nombre = $request->nombre;
        $newPerson->primer_apellido = $request->primer_apellido;
        $newPerson->segundo_apellido = $request->segundo_apellido;
        $newPerson->municipio_id = $request->municipio_id;
        $newPerson->colonia = $request->colonia;
        $newPerson->calle = $request->calle;
        $newPerson->numero_exterior = $request->numero_exterior;
        $newPerson->numero_interior = $request->numero_interior;
        $newPerson->codigopostal = $request->codigopostal;
        $newPerson->curp = $request->curp;
        $newPerson->genero = $request->genero;
        $newPerson->fecha_nacimiento = $request->fecha_nacimiento;
        $newPerson->telefono = $request->telefono;
        $newPerson->email = $request->email;
        $newPerson->tipo_sangre = $request->sangre;
        $newPerson->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    public function addSolicitantes(Request $request){
        $newSolicitante = new Solicitante;
        $newSolicitante->persona = $request->persona;
        $newSolicitante->servicio = $request->servicio;
        $newSolicitante->modalidad = $request->modalidad;
        $newSolicitante->fecha_ingreso = $request->fecha_ingreso;
        $newSolicitante->save();
        return [
            'respuesta' => 'los datos se guardaron'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // $solicitantes = Socio::select('socios.id','socios.servicio','personas.nombre','personas.primer_apellido', 'personas.segundo_apellido', 'personas.curp')
        //                  ->join('personas', 'personas.curp', '=', 'socios.persona')
        //                  ->where('socios.estado', '=', 'solicitante')
        //                  ->get();
        // return [
        //         'solicitantes' => $solicitantes
        // ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $llave = $request->id;
        $personaAModificar = Persona::find($llave);
        $personaAModificar->municipio_id = $request->municipio_id;
        $personaAModificar->colonia = $request->colonia;
        $personaAModificar->calle = $request->calle;
        $personaAModificar->numero_exterior = $request->numero_exterior;
        $personaAModificar->numero_interior = $request->numero_interior;
        $personaAModificar->codigopostal = $request->codigopostal;
        $personaAModificar->telefono = $request->telefono;
        $personaAModificar->email = $request->email;
        $personaAModificar->save();
        return [
                 'respuesta' => $personaAModificar
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    
    public function buscarCorreo(Request $request){
        $correo = Persona::where('email', $request->correo)->get();
        return [
            'correo' => $correo
    ];
    }
}
