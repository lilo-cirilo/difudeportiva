<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\asistencia;

class AsistenciaUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'curp' => 'required',
            'fecha' => 'required',
        ]);
        
        $asistenciaNueva = new asistencia;
        $asistenciaNueva->curp = $request->curp;
        $asistenciaNueva->fechaHora = $request->fecha;
        $asistenciaNueva->motivo = $request->motivo;
        $asistenciaNueva->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }
    public function getEstado(Request $request){
        $asistencia = Asistencia::select('motivo')
                        ->where('curp','=',$request->curp)
                        ->orderBy('id')
                        ->get();
        return [ 'estado' => $asistencia ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('unidaddeportiva::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
