<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Grupo;
use Modules\UnidadDeportiva\Entities\DetalleDG;
use Modules\UnidadDeportiva\Entities\GrupoSocio;
use Illuminate\Support\Facades\DB;

class GrupoUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $datos= [$request->nombre,intval($request->servicio),$request->rfc,$request->hora_inicio,$request->hora_fin];
        
        $GrupoNuevo = new Grupo;
        $GrupoNuevo->servicio = intval($request->servicio);
        $GrupoNuevo->rfc = $request->rfc;
        $GrupoNuevo->nombre = $request->nombre;
        $GrupoNuevo->hora_inicio = $request->hora_inicio;
        $GrupoNuevo->hora_fin = $request->hora_fin;
        $GrupoNuevo->capacidad = $request->capacidad;
        $GrupoNuevo->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $grupos = DB::table('grupos as g')
                         ->join('instructors as i', 'g.rfc', '=', 'i.rfc')
                         ->join('servicios as s','g.servicio','=','s.id')
                         ->select('g.nombre','i.nombre as ins','s.nombre as ser','g.id','g.rfc','g.capacidad','g.servicio','g.hora_inicio','g.hora_fin')
                         ->whereNull('g.deleted_at')
                         ->get();
        // $grupos = Grupo::all();
        return [
                'grupos' => $grupos
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $llave = $request->id;
        $GrupoAModificar = Grupo::find($llave);
        $GrupoAModificar->rfc = $request->rfc;
        $GrupoAModificar->nombre = $request->nombre;
        $GrupoAModificar->hora_inicio = $request->hora_inicio;
        $GrupoAModificar->hora_fin = $request->hora_fin;
        $GrupoAModificar->capacidad = $request->capacidad;
        $GrupoAModificar->save();
        return [
                 'respuesta' => 'Los datos se actualizaron correctamente'
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $llave = $request->id;
        $grupoAEliminar = Grupo::find($llave);
        Grupo::destroy($llave);
        return [
                 'respuesta' => $grupoAEliminar
        ];
    }
    public function buscarGrupoNombre(Request $request)
    {
        return [
                 'respuesta' => $request
        ];
    }

    public function consultarGrupo(Request $request){
        
        $datos = ['servicio' => $request->servicio, 
                  'hora_inicio' => $request->hora_inicio,
                  'rfc' => $request->rfc];
        $grupo = Grupo::where($datos)->get();
        $datos2 = ['hora_inicio' => $request->hora_inicio,
                  'rfc' => $request->rfc];
        $grupo2 = Grupo::where($datos2)->get();
        return [ 'respuesta' => [$grupo,$grupo2] ];
    }

    // public function addDias(Request $request){
    //     $id = DB::table('grupos')->where('nombre','=',$request->nombre)
    //     ->where('servicio','=',$request->servicio)->get();
    //     $dias = $request->dias;
    //     for ($i=0; $i < count($dias); $i++) { 
    //         $users = DB::table('dias_servicios')->insert(
    //             ['id_servicio' => $id[0]->id, 'id_dia' => $dias[$i]]
    //         );
    //     }
    //     return [ 'respuesta' => [$dias, $id] ];
    // }
    public function addDias(Request $request){
        $id = DB::table('grupos')->where('nombre','=',$request->nombre)
        ->where('servicio','=',$request->servicio)->get();
        $semana = ['lunes','martes','miercoles','jueves','viernes'];
        $dias = $request->dias;
        $diasNuevos = new DetalleDG;
        $diasNuevos->id_grupo = $id[0]->id;
        for ($i=0; $i < count($dias); $i++) {
            for ($j=0; $j < 5; $j++) {
                if ($dias[$i]-1==$j) {
                    if($j==0){$diasNuevos->lunes = true;}
                    if($j==1){$diasNuevos->martes = true;}
                    if($j==2){$diasNuevos->miercoles = true;}
                    if($j==3){$diasNuevos->jueves = true;}
                    if($j==4){$diasNuevos->viernes = true;}
                }
            }
        }
        $diasNuevos->save();
        return [ 'respuesta' => [$dias, $diasNuevos] ];
    }

    public function getDias(Request $request){
        $dias = DB::table('detalle_dias_grupos')->where('id_grupo','=',$request->id_grupo)->get();
        $respuesta = [];
        $contador = 0;
        if($dias[0]->lunes == true){$respuesta[$contador]=1; $contador++;}
        if($dias[0]->martes == true){$respuesta[$contador]=2; $contador++;}
        if($dias[0]->miercoles == true){$respuesta[$contador]=3; $contador++;}
        if($dias[0]->jueves == true){$respuesta[$contador]=4; $contador++;}
        if($dias[0]->viernes == true){$respuesta[$contador]=5; $contador++;}
        // for ($i=0; $i < count($dias); $i++) {
        //     $respuesta[$i] = $dias[$i]->id_dia;
        // }
        return ['respuesta' => $respuesta];
    }

    public function updateDias(Request $request){
        $diasActual = DB::table('detalle_dias_grupos')->where('id_grupo','=',$request->id_servicio)->get();
        $dias = $request->dias;
        // $diasNuevos = new DetalleDS;
        // $diasA->id_servicio = $id[0]->id;
        for ($i=0; $i < count($dias); $i++) {
            for ($j=0; $j < 5; $j++) {
                if ($dias[$i]-1==$j) {
                    if($j==0){$diasActual->lunes = true;}
                    if($j==1){$diasActual->martes = true;}
                    if($j==2){$diasActual->miercoles = true;}
                    if($j==3){$diasActual->jueves = true;}
                    if($j==4){$diasActual->viernes = true;}
                }
            }
        }
        $diasActual->save();
        return [ 'respuesta' => [$dias, $diasActual] ];
    }

    public function obtGrupos(Request $request){
        $usuarios =  GrupoSocio::where('id_grupo','=',$request->id)->get();
        $socios = DB::table('socio_grupo as sg')->select('sg.curp','id_grupo','p.nombre AS pnombre',
        'p.primer_apellido AS pap1','p.segundo_apellido AS pap2','i.nombre AS inombre','i.apellido_uno AS iap1',
        'i.apellido_dos AS iap2')
        ->join('personas as p','sg.curp','=','p.curp')
        ->join('grupos as g','sg.id_grupo','=','g.id')
        ->join('instructors as i','g.rfc','=','i.rfc')
        ->where('id_grupo','=',$request->id)->get();
        return ['respuesta' => [$usuarios,$socios]];
    }
}
