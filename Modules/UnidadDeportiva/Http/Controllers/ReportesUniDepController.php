<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class ReportesUniDepController extends Controller
{
    public function index(){return view('unidaddeportiva::index');}
    public function create(){return view('unidaddeportiva::create');}
    public function store(Request $request){}
    public function show(){}
    public function edit(){return view('unidaddeportiva::edit');}
    public function update(Request $request){}
    public function destroy(Request $request){}

    //////1.-Por Sexo/////
    public function obtPorSexo(){
        $genero = DB::select('sELECT COUNT(curp) as total, genero FROM personas AS p
        INNER JOIN socios AS s ON s.persona = p.curp where s.estado = "activo"
        GROUP BY p.genero');
        $generosTotal = [count($genero)];
        $generosNombr = [count($genero)];
        for ($i=0; $i < count($genero); $i++) {
            $generosTotal[$i] = $genero[$i]->total;
            $generosNombr[$i] = ($genero[$i]->genero == 'F') ? 'Mujer' : 'Hombre';
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    //////2.-Por mes/////
    public function obtPorMes(){
        $total = [12];
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre',
        'Noviembre','Diciembre'];
        for ($i=0; $i < 12; $i++) { 
            $users = DB::table('socios')->select(DB::raw('count(id) as total'))
                    ->where('estado','=','activo')
                    ->whereMonth('created_at','=', $i + 1)
                    ->get();
                    $valor = $users[0]->total;
            $total[$i] = $valor;
        }
        $arreglo = [$meses,$total];
        return ['respuesta' => $arreglo];
    }
    //////3.-Por año/////
    public function obtPorAño(){
        $total = [12];
        $nombre = [12];
        for ($i=2017; $i < 2029; $i++) { 
            $users = DB::table('socios')->select(DB::raw('count(id) as total'))
                    ->where('estado','=','activo')
                    ->whereYear('created_at','=', $i)
                    ->get();
                    $valor = $users[0]->total;
            $total[$i - 2017] = $valor;
            $nombre[$i - 2017] = $i;
        }
        $arreglo = [$nombre,$total];
        return ['respuesta' => $arreglo];
    }
    //////4.-Por municipio/////
    public function obtPorMunicipio(){
        $municipio = DB::select('seLECT COUNT(cm.nombre) AS total, cm.nombre FROM personas AS p
                                INNER JOIN cat_municipios AS cm ON municipio_id = cm.id
                                INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo"
                                GROUP BY cm.nombre');
        $generosTotal = [count($municipio)];
        $generosNombr = [count($municipio)];
        for ($i=0; $i < count($municipio); $i++) {
            $generosTotal[$i] = $municipio[$i]->total;
            $generosNombr[$i] = $municipio[$i]->nombre;
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    //////5.-Por colonia/////
    public function obtPorColonia(){
        $colonia = DB::select('seLECT COUNT(p.colonia) AS total, p.colonia FROM personas AS p
                                INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo"
                                GROUP BY p.colonia');
        $generosTotal = [count($colonia)];
        $generosNombr = [count($colonia)];
        for ($i=0; $i < count($colonia); $i++) {
            $generosTotal[$i] = $colonia[$i]->total;
            $generosNombr[$i] = $colonia[$i]->colonia;
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    //////6.-Por edad/////
    public function obtPorEdad(){
        $edad = DB::select('select COUNT(edad_actual)AS total, edad_actual FROM (SELECT p.nombre, 
                            YEAR(CURDATE()) - YEAR(p.fecha_nacimiento) + IF(DATE_FORMAT(CURDATE(),"%m-%d") > 
                            DATE_FORMAT(p.fecha_nacimiento,"%m-%d"), 0 , -1 ) AS edad_actual
                            FROM personas AS p 
                            INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo")AS listar
                            GROUP BY edad_actual');
        $generosTotal = [count($edad)];
        $generosNombr = [count($edad)];
        for ($i=0; $i < count($edad); $i++) {
            $generosTotal[$i] = $edad[$i]->total;
            $generosNombr[$i] = $edad[$i]->edad_actual;
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    //////7.-Por Sexo, mes/////
    public function obtPorSexoMes(){}
    //////8.-Por Sexo, año/////
    public function obtPorSexoAño(){}
    //////9.-Por Sexo, municipio/////
    public function obtPorSexoMunicipio(){
        $sexo  = DB::select('sELECT cm.nombre, SUM(CASE WHEN genero = "F" THEN 1 ELSE 0 END)AS mujeres,
                        SUM(CASE WHEN genero = "M" THEN 1 ELSE 0 END)AS hombres FROM personas AS p
                        INNER JOIN cat_municipios AS cm ON municipio_id = cm.id
                        INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo"
                        GROUP BY cm.nombre');
        $generosTotHo = [count($sexo)];
        $generosTotMu = [count($sexo)];
        $generosNombr = [count($sexo)];
        for ($i=0; $i < count($sexo); $i++) {
            $generosTotHo[$i] = $sexo[$i]->hombres;
            $generosTotMu[$i] = $sexo[$i]->mujeres;
            $generosNombr[$i] = $sexo[$i]->nombre;
        }
        $completo = [$generosNombr,$generosTotHo,$generosTotMu];
        return ['respuesta' => $completo]; 
    }
    //////10.-Por Sexo, colonia/////
    public function obtPorSexoColonia(){
        $sexo  = DB::select('seLECT p.colonia, SUM(CASE WHEN genero = "F" THEN 1 ELSE 0 END)AS mujeres,
                            SUM(CASE WHEN genero = "M" THEN 1 ELSE 0 END)AS hombres FROM personas AS p
                            INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo"
                            GROUP BY p.colonia');
        $generosTotHo = [count($sexo)];
        $generosTotMu = [count($sexo)];
        $generosNombr = [count($sexo)];
        for ($i=0; $i < count($sexo); $i++) {
            $generosTotHo[$i] = $sexo[$i]->hombres;
            $generosTotMu[$i] = $sexo[$i]->mujeres;
            $generosNombr[$i] = $sexo[$i]->colonia;
        }
        $completo = [$generosNombr,$generosTotHo,$generosTotMu];
        return ['respuesta' => $completo]; 
    }
    //////11.-Por Sexo, edad/////
    public function obtPorSexoEdad(){
        $sexo  = DB::select('select edad_actual, SUM(CASE WHEN genero = "F" THEN 1 ELSE 0 END)AS mujeres,
                            SUM(CASE WHEN genero = "M" THEN 1 ELSE 0 END)AS hombres FROM (SELECT p.nombre, YEAR(CURDATE()) - YEAR(p.fecha_nacimiento) + IF(DATE_FORMAT(CURDATE(),"%m-%d") > DATE_FORMAT(p.fecha_nacimiento,"%m-%d"), 0 , -1 ) AS edad_actual, genero
                            FROM personas AS p 
                            INNER JOIN socios AS s ON p.curp = s.persona WHERE s.estado = "activo")AS listar
                            GROUP BY edad_actual');
        $generosTotHo = [count($sexo)];
        $generosTotMu = [count($sexo)];
        $generosNombr = [count($sexo)];
        for ($i=0; $i < count($sexo); $i++) {
            $generosTotHo[$i] = $sexo[$i]->hombres;
            $generosTotMu[$i] = $sexo[$i]->mujeres;
            $generosNombr[$i] = $sexo[$i]->edad_actual;
        }
        $completo = [$generosNombr,$generosTotHo,$generosTotMu];
        return ['respuesta' => $completo];
    }
    //////12.-Por mes, municipio/////
    public function obtPorMesMunicipio(){}
    //////13.-Por mes, colonia/////
    public function obtPorMesColonia(){}
    //////14.-Por mes, edad/////
    public function obtPorMesEdad(){}
    //////15.-Por año, municipio/////
    public function obtPorAñoMunicipio(){}
    //////16.-Por año, colonia/////
    public function obtPorAñoColonia(){}
    //////17.-Por año, edad/////
    public function obtPorAñoEdad(){}
    //////18.-Por municipio, edad/////
    public function obtPorMunicipioEdad(){}
    //////19.-Por colonia, edad/////
    public function obtPorColoniaEdad(){}
    //////20.-Por Sexo, mes, municipio/////
    public function obtPorSexoMesMunicipio(){}
    //////21.-Por Sexo, mes, colonia/////
    public function obtPorSexoMesColonia(){}
    //////22.-Por Sexo, mes, edad/////
    public function obtPorSexoMesEdad(){}
    //////23.-Por Sexo, año, municipio/////
    public function obtPorSexoAñoMunicipio(){}
    //////24.-Por Sexo, año, colonia/////
    public function obtPorSexoAñoColonia(){}
    //////25.-Por Sexo, año, edad/////
    public function obtPorSexoAñoEdad(){}
    //////26.-Por mes, municipio, edad/////
    public function obtPorMesMunicipioEdad(){}
    //////27.-Por mes, colonia, edad/////
    public function obtPorMesColoniaEdad(){}
    //////28.-Por año, municipio, edad/////
    public function obtPorAñoMunicipioEdad(){}
    //////29.-Por año, colonia, edad/////
    public function obtPorAñoColoniaEdad(){}
    //////30.-Por Sexo, mes, municipio, edad/////
    public function obtPorSexoMesMunicipioEdad(){}
    //////31.-Por Sexo, mes, colonia, edad/////
    public function obtPorSexoMesColoniaEdad(){}
    //////32.-Por Sexo,año, municipio, edad/////
    public function obtPorSexoAñoMunicipioEdad(){}
    //////33.-Por Sexo,año,colonia,edad/////
    public function obtPorSexoAñoColoniaEdad(){}
    //////34.-Por Discapacidad/////////
    public function obtPorDiscapacidad(){
        $discapacidad = DB::select('seLECT COUNT(cd.nombre) as total,cd.nombre FROM personas AS p
                                    INNER JOIN socios AS s ON p.curp = s.persona
                                    INNER JOIN discapacidad_socios AS ds ON ds.persona = s.persona
                                    INNER JOIN cat_discapacidades AS cd ON cd.id = ds.discapacidad
                                    WHERE s.estado = "activo"
                                    GROUP BY cd.nombre');
        $generosTotal = [count($discapacidad)];
        $generosNombr = [count($discapacidad)];
        for ($i=0; $i < count($discapacidad); $i++) {
            $generosTotal[$i] = $discapacidad[$i]->total;
            $generosNombr[$i] = $discapacidad[$i]->nombre;
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    ///////35.-Por sexo y discapacidad//////////////////////
    public function obtPorSexoDiscapacidad(){
        $discapacidad = DB::select('seLECT cd.nombre, SUM(CASE WHEN genero = "F" THEN 1 ELSE 0 END)AS mujeres,
                                    SUM(CASE WHEN genero = "M" THEN 1 ELSE 0 END)AS hombres FROM personas AS p
                                    INNER JOIN socios AS s ON p.curp = s.persona
                                    INNER JOIN discapacidad_socios AS ds ON ds.persona = s.persona
                                    INNER JOIN cat_discapacidades AS cd ON cd.id = ds.discapacidad
                                    WHERE s.estado = "activo"
                                    GROUP BY cd.nombre');
        $generosTotHo = [count($discapacidad)];
        $generosTotMu = [count($discapacidad)];
        $generosNombr = [count($discapacidad)];
        for ($i=0; $i < count($discapacidad); $i++) {
            $generosTotHo[$i] = $discapacidad[$i]->hombres;
            $generosTotMu[$i] = $discapacidad[$i]->mujeres;
            $generosNombr[$i] = $discapacidad[$i]->nombre;
        }
        $completo = [$generosNombr,$generosTotHo,$generosTotMu];
        return ['respuesta' => $completo]; 
    }
    /////////36.-Por Discapacidad y Municipio/////////////
    public function obtPorDiscapacidadMunicipio(){}
    /////////37.-Por Discapacidad y Colonia/////////////
    public function obtPorDiscapacidadColonia(){}
    /////////38.-Por Discapacidad y Edad/////////////
    public function obtPorDiscapacidadEdad(){}
}
