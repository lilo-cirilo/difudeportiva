<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Incidencia;

class IncidenciaUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'curp' => 'required',
            'fecha' => 'required',
        ]);
        
        $incidenciaNueva = new Incidencia;
        $incidenciaNueva->curp = $request->curp;
        $incidenciaNueva->fecha_reporte = $request->fecha;
        $incidenciaNueva->descripcion = $request->descripcion;
        $incidenciaNueva->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $incidencias = Incidencia::all();
        return ['incidencias' => $incidencias];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
