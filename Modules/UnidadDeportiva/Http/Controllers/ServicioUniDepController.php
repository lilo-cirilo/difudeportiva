<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Servicio;
use Modules\UnidadDeportiva\Entities\Area;
use Modules\UnidadDeportiva\Entities\DetalleDS;
use Illuminate\Support\Facades\DB;

class ServicioUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $servicioNuevo = new servicio;
        $servicioNuevo->nombre = $request->nombre;
        $servicioNuevo->area = $request->area;
        $servicioNuevo->descripcion = $request->descripcion;
        $servicioNuevo->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // $servicios = Servicio::all();
        // $servicios = DB::table('servicios')->select('id', 'nombre','area','descripcion')->get();
        $servicios = DB::table('servicios')
                         ->join('areas', 'servicios.area', '=', 'areas.id')
                         ->select('servicios.id','servicios.nombre', 'areas.nombre AS area', 'servicios.descripcion','servicios.area as areaservicio')
                         ->whereNull('servicios.deleted_at')
                         ->get();
//         SELECT s.id,s.nombre, a.nombre AS area, s.descripcion FROM servicios AS s
// INNER JOIN areas AS a ON s.area = a.id ORDER BY s.id
        return [
                'servicios' => $servicios
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $llave = $request->id;
        $servicioAModificar = servicio::find($llave);
        $servicioAModificar->nombre = $request->nombre;
        $servicioAModificar->descripcion = $request->descripcion;
        $servicioAModificar->save();
        return [
                 'respuesta' => 'los datos se actualizaron correctamente'
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $llave = $request->id;
        $servicioAEliminar = servicio::find($llave);
        servicio::destroy($llave);
        return [
                 'respuesta' => $servicioAEliminar
        ];
    }

    public function areaPertenciente(Request $request)
    {
        $llave = $request->id;
        $areaBuscada = servicio::find($llave);
        // ->value('email');
        return [
                 'respuesta' => $areaBuscada->area
        ];
    }

    public function consultarServicio(Request $request){
        
        $datos = ['nombre' => $request->nombre, 
                  'area' => $request->area];
        $servicio = Servicio::where($datos)->get();
        return [ 'respuesta' => $servicio ];
    }

    public function addDias(Request $request){
        $id = DB::table('servicios')->where('nombre','=',$request->nombre)
        ->where('area','=',$request->area)->get();
        $semana = ['lunes','martes','miercoles','jueves','viernes'];
        $dias = $request->dias;
        $diasNuevos = new DetalleDS;
        $diasNuevos->id_servicio = $id[0]->id;
        for ($i=0; $i < count($dias); $i++) {
            for ($j=0; $j < 5; $j++) {
                if ($dias[$i]-1==$j) {
                    if($j==0){$diasNuevos->lunes = true;}
                    if($j==1){$diasNuevos->martes = true;}
                    if($j==2){$diasNuevos->miercoles = true;}
                    if($j==3){$diasNuevos->jueves = true;}
                    if($j==4){$diasNuevos->viernes = true;}
                }
            }
        }
        $diasNuevos->save();
        return [ 'respuesta' => [$dias, $diasNuevos] ];
    }
    
    public function getDias(Request $request){
        $dias = DB::table('detalle_dias_servicios')->where('id_servicio','=',$request->id_servicio)->get();
        $respuesta = [];
        $contador = 0;
        if($dias[0]->lunes == true){$respuesta[$contador]=1; $contador++;}
        if($dias[0]->martes == true){$respuesta[$contador]=2; $contador++;}
        if($dias[0]->miercoles == true){$respuesta[$contador]=3; $contador++;}
        if($dias[0]->jueves == true){$respuesta[$contador]=4; $contador++;}
        if($dias[0]->viernes == true){$respuesta[$contador]=5; $contador++;}
        // for ($i=0; $i < count($dias); $i++) {
        //     $respuesta[$i] = $dias[$i]->id_dia;
        // }
        return ['respuesta' => $respuesta];
    }

    public function updateDias(Request $request){
        $diasActual = DB::table('detalle_dias_servicios')->where('id_servicio','=',$request->id_servicio)->get();
        $dias = $request->dias;
        // $diasNuevos = new DetalleDS;
        // $diasA->id_servicio = $id[0]->id;
        for ($i=0; $i < count($dias); $i++) {
            for ($j=0; $j < 5; $j++) {
                if ($dias[$i]-1==$j) {
                    if($j==0){$diasActual->lunes = true;}
                    if($j==1){$diasActual->martes = true;}
                    if($j==2){$diasActual->miercoles = true;}
                    if($j==3){$diasActual->jueves = true;}
                    if($j==4){$diasActual->viernes = true;}
                }
            }
        }
        $diasActual->save();
        return [ 'respuesta' => [$dias, $diasActual] ];
    }

    public function obtServicio(Request $request){
        $nombre = Servicio::where('nombre','=',$request->id)->get();
        return ['respuesta' => $nombre->nombre];
    }
}
