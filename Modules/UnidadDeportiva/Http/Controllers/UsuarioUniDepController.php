<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\UnidadDeportiva\Entities\Persona;
use Modules\UnidadDeportiva\Entities\Usuario;
use Modules\UnidadDeportiva\Entities\UsuarioRol;

class UsuarioUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $usuario = new Usuario;
        $usuario->usuario = $request->usuario;
        $usuario->password = bcrypt($request->password);
        $usuario->email = strtolower($request->email);
        $usuario->remember_token = \Carbon\Carbon::now()->toDateTimeString();
        $usuario->autorizado = 1;
        $usuario->save();
        $usuario1 = Usuario::where('usuario','=',$request->usuario)->get();
        $usuarioRol = new UsuarioRol;
        $usuarioRol->usuario_id = $usuario1[0]->id;
        $usuarioRol->modulo_id = 16;
        $usuarioRol->rol_id = $request->rol_id;
        $usuarioRol->responsable = 1;
        $usuarioRol->autorizado = 1 ;
        $usuarioRol->save();
        return ['respuesta' => '$request->password'];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request)
    {
        $usuario = Usuario::select('usuarios.id','usuarios.usuario','cr.nombre','cr.id as rol','usuarios.email')
        ->join('usuarios_roles as us','us.usuario_id','=','usuarios.id')
        ->join('cat_roles as cr','cr.id','=','us.rol_id')
        ->where('usuario','!=',$request->id)->get();
        return ['usuarios' => $usuario];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $usuario = Usuario::where('id','=',$request->id)->get();
        $usuario[0]->usuario = $request->usuario;
        $usuario[0]->password = bcrypt($request->password);
        $usuario[0]->email = strtolower($request->email);
        $usuario[0]->remember_token = \Carbon\Carbon::now()->toDateTimeString();
        $usuario[0]->autorizado = 1;
        $usuario[0]->save();
        return ['respuesta' => '$request->password'];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $llave = $request->id;
        $usuarioAEliminar = Usuario::find($llave);
        Usuario::destroy($llave);
        return ['respuesta' => $usuarioAEliminar];
    }

    public function listarRoles(){
        $roles = DB::table('cat_roles')->where('nombre','!=','SUPERADMIN')->get();
        return ['respuesta' => $roles];
    }

    public function consultarUsuario(Request $request){
        $registro = DB::table('usuarios')->where('usuario','=',$request->usuario)->get();
        $correos = DB::table('usuarios')->where('email','=',$request->correo)->get();
        return ['respuesta' => [count($registro),count($correos)]];
    }
}
