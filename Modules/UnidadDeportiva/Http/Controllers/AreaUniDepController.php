<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Area;

class AreaUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nombre' => 'required',
            'capacidad' => 'required',
        ]);
        
        $areaNueva = new area;
        $areaNueva->nombre = $request->nombre;
        $areaNueva->capacidad = $request->capacidad;
        $areaNueva->descripcion = $request->descripcion;
        $areaNueva->save();
        return [
                 'respuesta' => 'Los datos se insertaron correctamente'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $areas = Area::all();
        return [
                'areas' => $areas
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $llave = $request->id;
        $areaAModificar = area::find($llave);
        $areaAModificar->nombre = $request->nombre;
        $areaAModificar->capacidad = $request->capacidad;
        $areaAModificar->descripcion = $request->descripcion;
        $areaAModificar->save();
        return [
                 'respuesta' => 'Los datos se actualizaron correctamente'
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request)
    {
        $llave = $request->id;
        $areaAEliminar = area::find($llave);
        area::destroy($llave);
        return [
                 'respuesta' => $areaAEliminar
        ];
    }

    public function consultarArea(Request $request){
        $llave = $request->nombre;
        $area = Area::where('nombre','=',$llave)->get();
        return [ 'respuesta' => $area ];
    }
    public function consultarAreaPorId(Request $request){
        $area = Area::find($request->area);
        return [ 'respuesta' => $area ];
    }
}
