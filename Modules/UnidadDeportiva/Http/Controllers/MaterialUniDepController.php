<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Material;
use Modules\UnidadDeportiva\Entities\Area;
use Illuminate\Support\Facades\DB;

class MaterialUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $material = new Material;
        $material->nombre = $request->nombre;
        $material->cantidad = $request->cantidad;
        $material->area = $request->area;
        $material->descripcion = $request->descripcion;
        $material->save();
        return [ 'respuesta' => 'se insertó correctamente' ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        // $material = Material::all();
        $material = DB::table('materials as m')->select('m.id','m.nombre','m.cantidad','a.nombre as area','m.descripcion')
        ->join('areas as a','a.id','=','m.area')->get();
        return [
                'materiales' => $material
        ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $material = Material::find($request->id);
        $material->nombre = $request->nombre;
        $material->cantidad = $request->cantidad;
        $material->descripcion = $request->descripcion;
        $material->save();
        return [ 'respuesta' => 'se insertó correctamente' ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function consultar(Request $request){
        $nombre = $request->nombre;
        $area = $request->area;
        $material = Material::where('nombre','=',$nombre)->where('area','=',$area)->get();
        return [ 'respuesta' => $material ];
    }
}
