<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\Municipio;
use App\Models\Localidad;
use App\Models\Discapacidad;
use Modules\UnidadDeportiva\Entities\CodigoPostal;
use Modules\UnidadDeportiva\Entities\Pago;
use Modules\UnidadDeportiva\Entities\Contacto;
use Modules\UnidadDeportiva\Entities\Socio;
use Modules\UnidadDeportiva\Entities\GrupoSocio;
use Modules\UnidadDeportiva\Entities\Area;
use Modules\UnidadDeportiva\Entities\Servicio;
use Modules\UnidadDeportiva\Entities\Grupo;
use Modules\UnidadDeportiva\Entities\DiscapacidadSocio;
use Illuminate\Support\Facades\DB;

class UnidaddeportivaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('unidaddeportiva::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function listarMunicipios(){
        $municipios = Municipio::all();
        
        return [
                'municipios' => $municipios
        ];
    }

    public function listarCps(){
        $cps = DB::table('cat_codigos_postales as cp')
                         ->join('cat_municipios as m', 'm.nombre', '=', 'cp.D_mnpio')
                         ->select('cp.id','cp.d_codigo','cp.d_asenta','m.id as id_municipio','m.nombre')
                         ->get();
        return [
                'cps' => $cps
        ];
    }

    public function listarColonias(){
        $colonias = Localidad::all();
        
        return [
                'colonias' => $colonias
        ];
    }

    public function listarDiscapacidad(){
        $discapacidad = Discapacidad::all();
        
        return [
                'discapacidad' => $discapacidad
        ];
    }

    public function listarDiscapacidadReportes(){
        $discapacidad = Discapacidad::join('discapacidad_socios as s','cat_discapacidades.id','=','s.discapacidad')
        ->get();
        
        return [
                'discapacidad' => $discapacidad
        ];
    }

    public function listarContactos(Request $request){
        $contactos = Contacto::where('socio','=',$request->curp)->get();
        
        return [
                'contactos' => $contactos
        ];
    }

    public function addPagos(Request $request){
        $newPago = new Pago;
        $newPago->folio = $request->folio;
        $newPago->curp = $request->curp;
        $newPago->id_servicio = $request->id_servicio;
        $newPago->fecha_pago = $request->fecha_pago;
        $newPago->motivo = $request->motivo;
        $newPago->monto = $request->monto;
        $newPago->mes = $request->mes;
        $newPago->save();
        return [
            'respuesta' => 'los datos se guardaron'
        ];
    }

    public function addContactos(Request $request){
        $arreglo = $request->contactos;
        $curp = $request->curp;
        $indice = 0;
        for ($i = 0; $i < count($arreglo); $i++) {
            $newContacto = new Contacto;
            if($i == 0){$newContacto->tipo = 'principal';}
            else{$newContacto->tipo = 'secundario';}
            $newContacto->socio = $curp;
            $newContacto->nombre = $arreglo[$i]["nombre"];
            $newContacto->telefono = $arreglo[$i]["telefono"];
            $newContacto->save();
        }
        return [
            'respuesta' => $arreglo
        ];
    }

    private function addOneContact($curp, $nombre, $telefono){
        
    }
    /////////////////PARA LA GRAFICA////////////////
    public function obtRecuentoSolicitudesPorMes(){
        $arreglo = [12];
        for ($i=0; $i < 12; $i++) { 
            $users = DB::table('socios')->select(DB::raw('count(id) as total'))
                    ->whereMonth('created_at','=', $i + 1)
                    ->get();
                    $valor = $users[0]->total;
            $arreglo[$i] = $valor;
        }
        return ['respuesta' => $arreglo];
    }
    /////////////////PARA LA GRAFICA////////////////
    public function obtRecuentoPagosPorMes(){
        $arreglo = [12];
        for ($i=0; $i < 12; $i++) { 
            $users = DB::table('pagos')->select(DB::raw('count(id) as total'))
                    ->whereMonth('created_at','=', $i + 1)
                    ->where('motivo','=','inscripcion')
                    ->get();
                    $valor = $users[0]->total;
            $arreglo[$i] = $valor;
        }
        return ['respuesta' => $arreglo];
    }
    /////////////////PARA LA GRAFICA////////////////
    public function obtPorSexo(){
        $genero = DB::select('sELECT COUNT(curp) as total, genero FROM personas AS p
        INNER JOIN socios AS s ON s.persona = p.curp where s.estado = "activo"
        GROUP BY p.genero');
        $generosTotal = [count($genero)];
        $generosNombr = [count($genero)];
        for ($i=0; $i < count($genero); $i++) {
            $generosTotal[$i] = $genero[$i]->total;
            $generosNombr[$i] = ($genero[$i]->genero == 'F') ? 'Mujer' : 'Hombre';
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    /////////////////PARA LA GRAFICA////////////////
    public function obtPorAño(){
        $genero = DB::select('sELECT COUNT(curp) as total, genero FROM personas AS p inner join socios as s
        on s.persona = p.curp where s.estado = "activo" group by p.genero');
        // ->join('socios as s','s.persona','=','p.curp', 'where s.estado = "activo"')
        // // ->where('s.estado','=',activo)
        // ->groupBy('p.genero')->get();
        // INNER JOIN socios AS s ON s.persona = p.curp where s.estado = "activo"
        // GROUP BY p.genero');
        $generosTotal = [count($genero)];
        $generosNombr = [count($genero)];
        for ($i=0; $i < count($genero); $i++) {
            $generosTotal[$i] = $genero[$i]->total;
            $generosNombr[$i] = ($genero[$i]->genero == 'F') ? 'Mujer' : 'Hombre';
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    public function obtPorMes(){
        $genero = DB::select('select COUNT(curp) as total, genero FROM personas AS p
        INNER JOIN socios AS s ON s.persona = p.curp
        GROUP BY p.genero');
        $generosTotal = [count($genero)];
        $generosNombr = [count($genero)];
        for ($i=0; $i < count($genero); $i++) {
            $generosTotal[$i] = $genero[$i]->total;
            $generosNombr[$i] = ($genero[$i]->genero == 'F') ? 'Mujer' : 'Hombre';
        }
        $completo = [$generosNombr,$generosTotal];
        return ['respuesta' => $completo];
    }
    ///////////PARA UNA GRAFICA ESTÁTICA////////////
    public function countServiciosPorColonia(){
        // SELECT colonia, COUNT(colonia)AS total from(SELECT colonia,se.nombre FROM personas AS p
        // INNER JOIN socios AS s ON p.curp = s.persona
        // INNER JOIN servicios AS se ON se.id = s.servicio
        // GROUP BY colonia,se.nombre)lista
        // GROUP BY colonia

        $prueba = DB::select('select colonia,COUNT(colonia) as total FROM personas AS p 
        INNER JOIN socios AS s ON p.curp = s.persona where s.estado = "activo" GROUP BY colonia');

        $prueba1 = DB::select('select colonia, COUNT(colonia)AS total 
        from(SELECT colonia,se.nombre FROM personas AS p
         INNER JOIN socios AS s ON p.curp = s.persona
         INNER JOIN servicios AS se ON se.id = s.servicio
         GROUP BY colonia,se.nombre)lista
         GROUP BY colonia');

        $colService = DB::table('personas as p')->select('colonia','se.nombre')
        ->join('socios as s','s.persona','=','p.curp')
        ->join('servicios as se','se.id','=','s.servicio')
        ->groupBy('colonia','se.nombre')->get();

        
        // $contador = $colService->select('colonia',DB::raw('count(colonia) as total'))
        // ->groupBy('colonia')->get();

        $nombres = [count($prueba)];
        $valores = [count($prueba)];
        for ($i=0; $i < count($prueba); $i++) {
            $nombres[$i] = $prueba[$i]->colonia;
            $valores[$i] = $prueba[$i]->total;
        }
        $completo = [$nombres,$valores];
        return ['respuesta' => $completo];
    }
    ///////////PARA UNA GRAFICA//////NO LA ESTOY OCUPANDO/////////
    public function countInstructoresPorGrupo(){
        $dato = DB::table('grupos as g')->select('g.rfc',DB::raw('count(g.rfc)'))
            ->join('instructors as i','g.rfc','=','i.rfc')
            ->groupBy('i.rfc')->get();
            return ['respuesta' => $dato];
    }
    ////////////////AGREGAR UN USUARIO A UN GRUPO//////////////////
    public function addusuariogrupo(Request $request){
        $grupoSocio = new GrupoSocio;
        $grupoSocio->curp = $request->curp;
        $grupoSocio->id_grupo = $request->grupo;
        $grupoSocio->save();
        for ($i=0; $i < count($request->discapacidades); $i++) {
            $discapacidad = new DiscapacidadSocio;
            $discapacidad->persona = $request->curp;
            $discapacidad->discapacidad = $request->discapacidades[$i];
            $discapacidad->save();
        }
        return ['respuesta' => $grupoSocio];
    }
    ///////////PRUEBA PARA VER EL ARREGLO////////////
    public function uno(Request $request){
        for ($i=0; $i < count($request->discapacidades); $i++) {
            $discapacidad = new DiscapacidadSocio;
            $discapacidad->persona = $request->curp;
            $discapacidad->discapacidad = $request->discapacidades[$i];
            $discapacidad->save();
        }
        return ['respuesta' => '$grupoSocio'];
    }
    //////////////PARA SABER SI YA SE INSCRIBIÓ EN ESE GRUPO/////////////////////
    public function consultagruposocio(Request $request){
        $grupoSocio = GrupoSocio::where('curp','=',$request->curp)
        ->where('id_grupo','=',$request->grupo)->get();
        return ['respuesta' => $grupoSocio];
    }
    /////////////////PARA LAS CREDENCIALES, SABER CUAL MES FUE EL QUE PAGÓ////////////
    public function consultaUltimoPago(Request $request){
        $pagoUno = Pago::where('curp','=',$request->curp)->get();
        if(count($pagoUno)==1){
            return ['respuesta' => [$pagoUno[0]->fecha_pago, 0]];
        }else{
            $pago = Pago::where('curp','=',$request->curp)->where('motivo','=','mensualidad')
            ->orderBy('fecha_pago', 'desc')->limit(1)->get();
            return ['respuesta' => [$pago[0]->fecha_pago,$pago[0]->mes]];
        }
        
    }
    /////////////////////LO UTILIZO PARA SACAR EL HORARIO DE LOS SOCIOS////////////
    public function obtenerDatosGrupos(Request $request){
        $prueba = DB::select('select gs.curp,id_grupo,nombre as grupo,lunes,martes,miercoles,jueves,viernes,
        hora_inicio,hora_fin FROM 
        (SELECT g.id ,g.nombre,lunes,martes,miercoles,jueves,viernes,hora_inicio,
        hora_fin FROM detalle_dias_servicios AS d
        INNER JOIN grupos AS g ON g.servicio = d.id_servicio) lista
        INNER JOIN socio_grupo AS gs ON gs.id_grupo = lista.id');

        // $respuesta = DB::table($prueba)->where('curp','=','MEEC860427HOCNSR07');
        $respuesta = [];
        $contador = 0;
        for ($i=0; $i < count($prueba) ; $i++) { 
            if ($prueba[$i]->curp == $request->curp) {
                $respuesta[$contador] = $prueba[$i];
                $contador++;
            }
        }
        return ['respuesta' => $respuesta];
    }
    /////////////////////////LAS VECES QUE UN SOCIO HA SACADO CREDENCIAL///////////
    public function consultaContador(Request $request){
        $contador = DB::table('detalle_count_credencial')
        ->where('id_socio','=',$request->id)->get();
        return ['respuesta' => $contador];
    }
    ///////////////////////REGISTRO DE UNA NUEVA CREDENCIAL SOLICITADA POR EL SOCIO///////////////////
    public function addcontador(Request $request){
        // $numero = DB::table('detalle_count_credencial')->where('id_socio','=',$request->id)->last();
        // $numeroAdd = $numero + 1;
        $insert = DB::table('detalle_count_credencial')->insert(
            ['id_socio'=>$request->id,'numero'=>1]);
        return ['respuesta' => 'Se insertó'];
    }
    /////////////////////////DATOS PARA EL DASHBOARD///////////////////////////////
    public function obtDatos(){
        $totalSocios = Socio::where('estado','=','activo')->count();
        $totalSolici = Socio::where('estado','=','solicitante')->count();
        $totalAreas  = Area::all()->count();
        $totalServi  = Servicio::all()->count();
        return ['respuesta' => [$totalSocios, $totalSolici, $totalAreas, $totalServi]];
    }
    /////////////////PARA LA PARTE DE REGISTRO DE SOCIOS/////////////
    public function listarGruposServicios(Request $request){
        $grupos = Grupo::where('servicio','=',$request->grupo)->get();
        return ['respuesta' => $grupos];
    }
    ////////////////PARA SABER SI YA ESTÁ LLENO EL GRUPO////////////
    public function consultarEstatusCapacidad(Request $request){
        $estatus = GrupoSocio::where('id_grupo','=',$request->grupo)->count();
        $total = Grupo::select('capacidad')->where('id','=',$request->grupo)->get();
        $respuesta = 'nada';
        if ($estatus == $total[0]->capacidad) {
            $respuesta = 'igual';
        }
        return ['respuesta' => $respuesta];
    }
    /////////////////SOLO PARA SABER QUE ROL TIENE EL QUE ESTÁ LOGUEADO/////////////////7
    public function obtRol(Request $request){
        $rol = DB::table('cat_roles')->where('id','=',$request->id)->get();
        return ['respuesta' => $rol[0]->nombre];
    }
}
