<?php

namespace Modules\Unidaddeportiva\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UnidadDeportiva\Entities\Persona;
use Modules\UnidadDeportiva\Entities\Socio;
use Modules\UnidadDeportiva\Entities\Servicio;
use Illuminate\Support\Facades\DB;

class SocioUniDepController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('unidaddeportiva::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('unidaddeportiva::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $newSolicitante = new Socio;
        $newSolicitante->persona = $request->persona;
        $newSolicitante->servicio = $request->servicio;
        $newSolicitante->fecha_ingreso = $request->fecha_ingreso;
        $newSolicitante->estado = $request->estado;
        $newSolicitante->save();
        return [
            'respuesta' => 'los datos se guardaron'
        ];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $socio = Socio::select('socios.id','socios.servicio','personas.nombre','personas.primer_apellido', 'personas.segundo_apellido', 'personas.curp','personas.tipo_sangre','personas.telefono','servicios.nombre as nomservicio','contactos.nombre as contactonombre','contactos.telefono as telcon')
                         ->join('personas', 'personas.curp', '=', 'socios.persona')
                         ->join('servicios','servicios.id','=','socios.servicio')
                         ->join('contactos','contactos.socio','=','personas.curp')
                         ->where('socios.estado', '=', 'activo')
                         ->where('contactos.tipo','=','principal')
                         ->whereNull('personas.deleted_at')
                         ->get();
        return [
                'socios' => $socio
        ];
    }

    public function showSolicitudes()
    {
        $solicitantes = Socio::select('socios.id','socios.servicio','personas.nombre','personas.primer_apellido', 'personas.segundo_apellido', 'personas.curp')
                         ->join('personas', 'personas.curp', '=', 'socios.persona')
                         ->where('socios.estado', '=', 'solicitante')
                         ->whereNull('personas.deleted_at')
                         ->get();
        return [
                'solicitantes' => $solicitantes
        ];
    }

    public function getSocio(Request $request){

        // $socio = Socio::find($request->id);
        $socio = Socio::select('p.calle','p.codigopostal','p.colonia','p.created_at','socios.persona',
        'p.email','fecha_ingreso','fecha_nacimiento','p.genero','socios.id','m.nombre as nommunicipio','p.nombre',
        'numero_exterior','numero_interior','primer_apellido','segundo_apellido','m.id as id_municipio',
        's.nombre as nomservicio','p.telefono')
                        ->join('personas as p','p.curp','=','socios.persona')
                        ->join('servicios as s','s.id','=','socios.servicio')                        
                        ->join('cat_municipios as m','m.id','=','p.municipio_id')
                        ->where('socios.id','=',$request->id)
                        ->get();
        return [ 'socio' => $socio ];
    }

    public function getVigencia(Request $request){

        // $socio = Socio::find($request->id);
        $socio = Socio::select('p.calle','p.codigopostal','p.colonia','p.created_at','socios.persona',
        'p.email','fecha_ingreso','fecha_nacimiento','p.genero','socios.id','m.nombre as nommunicipio','p.nombre',
        'numero_exterior','numero_interior','primer_apellido','segundo_apellido','motivo','fecha_pago',
        's.nombre as nomservicio','p.telefono')
                        ->join('personas as p','p.curp','=','socios.persona')
                        ->join('servicios as s','s.id','=','socios.servicio')
                        ->join('cat_municipios as m','m.id','=','p.municipio_id')
                        ->join('pagos','pagos.curp','=','socios.persona')
                        ->where('socios.id','=',$request->id)
                        ->get();
        return [ 'socio' => $socio ];
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('unidaddeportiva::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    public function updateEstado(Request $request){
        $llave = $request->id;
        $socioAModificar = Socio::find($llave);
        $socioAModificar->estado = $request->estado;
        $socioAModificar->save();
        return [
                 'respuesta' => 'Se actualizó el estado'
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscaCurp(Request $request){
        $curp = Persona::where('curp','=',$request->curp)->get();
        $servicio = Servicio::all();
        return ['respuesta' => [$curp, $servicio]];
    }
}
