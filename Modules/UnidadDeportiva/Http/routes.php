<?php

Route::group(['middleware' => 'web', 'prefix' => 'unidaddeportiva', 'namespace' => 'Modules\Unidaddeportiva\Http\Controllers'], function()
{
    Route::get('/', 'UnidaddeportivaController@index');

    Route::get('/listarAreas',        'AreaUniDepController@show');
	Route::post('/agregarAreas',      'AreaUniDepController@store');
    Route::post('/eliminarAreas',     'AreaUniDepController@destroy');
    Route::post('/actualizarAreas',   'AreaUniDepController@update');
    Route::post('/consultarArea',     'AreaUniDepController@consultarArea');
    Route::post('/consultarAreaPorId','AreaUniDepController@consultarAreaPorId');

    Route::get('/listarServicios',           'ServicioUniDepController@show');
	Route::post('/agregarServicios',         'ServicioUniDepController@store');
    Route::post('/eliminarServicios',        'ServicioUniDepController@destroy');
    Route::post('/actualizarServicios',      'ServicioUniDepController@update');
    Route::post('/consultarServicio',        'ServicioUniDepController@consultarServicio');
    Route::post('/agregarDiasDeServicios',   'ServicioUniDepController@addDias');
    Route::post('/obtDiasServicios',         'ServicioUniDepController@getDias');
    Route::post('/actualizarDiasDeServicios','ServicioUniDepController@updateDias');
    Route::post('/obtServicio',              'ServicioUniDepController@obtServicio');
    Route::post('/actualizarDiasDeGrupos',   'ServicioUniDepController@updateDias');
    Route::get('/buscarAreaServicio',        'ServicioUniDepController@areaPertenciente');

    Route::get('/listarInstructores',     'InstructorUniDepController@show');
	Route::post('/agregarInstructores',   'InstructorUniDepController@store');
    Route::post('/eliminarInstructores',  'InstructorUniDepController@destroy');
    Route::post('/actualizarInstructores','InstructorUniDepController@update');
    Route::post('/generarRfc',            'InstructorUniDepController@getRFC');
    Route::post('/consultarInstructor',   'InstructorUniDepController@consultarInstructor');

    Route::get('/listarGrupos',        'GrupoUniDepController@show');
	Route::post('/agregarGrupos',      'GrupoUniDepController@store');
    Route::post('/eliminarGrupos',     'GrupoUniDepController@destroy');
    Route::post('/actualizarGrupos',   'GrupoUniDepController@update');
    Route::post('/consultarGrupo',     'GrupoUniDepController@consultarGrupo');
    Route::post('/agregarDiasDeGrupos','GrupoUniDepController@addDias');
    Route::post('/obtDiasGrupos',      'GrupoUniDepController@getDias');
    Route::post('/obtGrupos',          'GrupoUniDepController@obtGrupos');

    Route::get('/listarMateriales',     'MaterialUniDepController@show');
	Route::post('/agregarMateriales',   'MaterialUniDepController@store');
    Route::post('/eliminarMateriales',  'MaterialUniDepController@destroy');
    Route::post('/actualizarMateriales','MaterialUniDepController@update');
    Route::post('/consultarMaterial',   'MaterialUniDepController@consultar');
    
    Route::post('/agregarSolicitantes','SocioUnidepController@store');
    Route::post('/actualizarEstado',   'SocioUnidepController@updateEstado');
    Route::get('/listarSolicitudes',   'SocioUnidepController@showSolicitudes');
    Route::get('/listarSocios',        'SocioUnidepController@show');
    Route::post('/getSocio',           'SocioUnidepController@getSocio');
    Route::post('/getVigencia',        'SocioUnidepController@getVigencia');
    Route::post('/BuscaCurp',          'SocioUnidepController@buscaCurp');
    Route::post('/getSocioParaFoto',   'SocioUnidepController@getSocioParaFoto');

    Route::get('/listarcps',                 'UnidadDeportivaController@listarCps');
    Route::get('/listarmunicipios',          'UnidadDeportivaController@listarMunicipios');
    Route::get('/listarcolonias',            'UnidadDeportivaController@listarColonias');
    Route::get('/listardiscapacidad',        'UnidadDeportivaController@listarDiscapacidad');
    Route::get('/listardiscapacidadReportes','UnidadDeportivaController@listarDiscapacidadReportes');
    Route::post('/listarContactos',          'UnidadDeportivaController@listarContactos');
    Route::post('/agregarPago',              'UnidadDeportivaController@addPagos');
    Route::post('/agregarContacto',          'UnidadDeportivaController@addContactos');
    Route::post('/consultarUltimoPago',      'UnidadDeportivaController@consultaUltimoPago');
    Route::post('/obtenerDatosGrupos',       'UnidadDeportivaController@obtenerDatosGrupos');
    Route::get('/consultarFechasSolicitudes','UnidadDeportivaController@obtRecuentoSolicitudesPorMes');
    Route::get('/consultarFechasPagos',      'UnidadDeportivaController@obtRecuentoPagosPorMes');
    Route::get('/countPorSexo',              'UnidadDeportivaController@obtPorSexo');
    Route::post('/agregarusuarioagrupo',     'UnidadDeportivaController@addusuariogrupo');
    Route::post('/consultarGrupoSocio',      'UnidadDeportivaController@consultagruposocio');
    Route::get('/countServiciosPorColonia',  'UnidadDeportivaController@countServiciosPorColonia');
    Route::post('/consultaContador',         'UnidadDeportivaController@consultaContador');
    Route::post('/addcontador',              'UnidadDeportivaController@addcontador');
    Route::get('/obtDatos',                  'UnidadDeportivaController@obtDatos');
    Route::post('/listarGruposServicios',    'UnidadDeportivaController@listarGruposServicios');
    Route::post('/consultarEstatusCapacidad','UnidadDeportivaController@consultarEstatusCapacidad');
    Route::post('/obtRol',                   'UnidadDeportivaController@obtRol');

    Route::get('/listarPrestamos','PrestamoUnidepController@show');

    

    Route::post('/buscarCorreo',      'PersonaUnidepController@buscarCorreo');
    Route::post('/agregarPersonas',   'PersonaUnidepController@store');
    Route::post('/actualizarPersonas','PersonaUnidepController@update');

    Route::post('/reportarIncidencia','IncidenciaUniDepController@store');
    Route::get('/listarincidencias',  'IncidenciaUniDepController@show');

    Route::post('/ingresoegreso',   'AsistenciaUniDepController@store');
    Route::post('/getEstadoIngreso','AsistenciaUniDepController@getEstado');

    

    

    Route::post('/listarUsuarios',    'UsuarioUniDepController@show');
	Route::post('/agregarUsuarios',   'UsuarioUniDepController@store');
    Route::post('/eliminarUsuarios',  'UsuarioUniDepController@destroy');
    Route::post('/actualizarUsuarios','UsuarioUniDepController@update');
    Route::post('/consultarUsuario',  'UsuarioUniDepController@consultarUsuario');

    Route::get('/listarRoles','UsuarioUniDepController@listarRoles');
    Route::post('/Uno','UnidadDeportivaController@uno');

    Route::get('/a33','ReportesUniDepController@obtPorSexo');
    Route::get('/a32','ReportesUniDepController@obtPorMes');
    Route::get('/a31','ReportesUniDepController@obtPorAño');
    Route::get('/a30','ReportesUniDepController@obtPorMunicipio');
    Route::get('/a29','ReportesUniDepController@obtPorColonia');
    Route::get('/a28','ReportesUniDepController@obtPorEdad');
    Route::get('/a27','ReportesUniDepController@obtPorSexoMes');
    Route::get('/a26','ReportesUniDepController@obtPorSexoAño');
    Route::get('/a25','ReportesUniDepController@obtPorSexoMunicipio');
    Route::get('/a24','ReportesUniDepController@obtPorSexoColonia');
    Route::get('/a23','ReportesUniDepController@obtPorSexoEdad');
    Route::get('/a22','ReportesUniDepController@obtPorMesMunicipio');
    Route::get('/a21','ReportesUniDepController@obtPorMesColonia');
    Route::get('/a20','ReportesUniDepController@obtPorMesEdad');
    Route::get('/a19','ReportesUniDepController@obtPorAñoMunicipio');
    Route::get('/a18','ReportesUniDepController@obtPorAñoColonia');
    Route::get('/a17','ReportesUniDepController@obtPorAñoEdad');
    Route::get('/a16','ReportesUniDepController@obtPorMunicipioEdad');
    Route::get('/a15','ReportesUniDepController@obtPorColoniaEdad');
    Route::get('/a14','ReportesUniDepController@obtPorSexoMesMunicipio');
    Route::get('/a13','ReportesUniDepController@obtPorSexoMesColonia');
    Route::get('/a12','ReportesUniDepController@obtPorSexoMesEdad');
    Route::get('/a11','ReportesUniDepController@obtPorSexoAñoMunicipio');
    Route::get('/a10','ReportesUniDepController@obtPorSexoAñoColonia');
    Route::get('/a9','ReportesUniDepController@obtPorSexoAñoEdad');
    Route::get('/a8','ReportesUniDepController@obtPorMesMunicipioEdad');
    Route::get('/a7','ReportesUniDepController@obtPorMesColoniaEdad');
    Route::get('/a6','ReportesUniDepController@obtPorAñoMunicipioEdad');
    Route::get('/a5','ReportesUniDepController@obtPorAñoColoniaEdad');
    Route::get('/a4','ReportesUniDepController@obtPorSexoMesMunicipioEdad');
    Route::get('/a3','ReportesUniDepController@obtPorSexoMesColoniaEdad');
    Route::get('/a2','ReportesUniDepController@obtPorSexoAñoMunicipioEdad');
    Route::get('/a1','ReportesUniDepController@obtPorSexoAñoColoniaEdad');
    Route::get('/a0','ReportesUniDepController@obtPorDiscapacidad');
    Route::get('/a-1','ReportesUniDepController@obtPorSexoDiscapacidad');
});
