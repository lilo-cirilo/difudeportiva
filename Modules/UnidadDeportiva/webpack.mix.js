let mix = require('laravel-mix');

mix.js('Resources/assets/js/app.js', './../../public/unidep')
  .sass('Resources/assets/sass/app.scss', './../../public/unidep')
  // .options({
  //     processCssUrls: false
  // })
  .setPublicPath('./../../public/unidep');