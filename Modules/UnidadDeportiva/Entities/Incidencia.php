<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Incidencia extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];    
    protected $fillable = ['curp', 'fecha_reporte','descripcion'];
}
