<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class DiscapacidadSocio extends Model
{
    use SoftDeletes;
    protected $table = 'discapacidad_socios';
    protected $dates = ['deleted_at'];    
    protected $fillable = [];
}
