<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Contacto extends Model
{
    use SoftDeletes;   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['id', 'nombre', 'telefono', 'socio'];
}
