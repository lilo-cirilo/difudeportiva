<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Instructor extends Model
{
    use SoftDeletes;
    protected $table = 'instructors';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['rfc', 'nombre'];
}
