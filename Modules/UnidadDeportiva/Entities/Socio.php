<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Socio extends Model
{
    use SoftDeletes;
    protected $table = 'socios';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['id', 'curp', 'nombre']; 

    public function personas(){
        return $this->belongsTo('\Modules\UnidadDeportiva\Entities\Persona','curp','curp');
    }
}
