<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class grupoSocio extends Model
{
    use SoftDeletes;
    protected $table = 'socio_grupo';
    // protected $dates = ['deleted_at'];    
    protected $fillable = ['curp', 'id_grupo'];
}
