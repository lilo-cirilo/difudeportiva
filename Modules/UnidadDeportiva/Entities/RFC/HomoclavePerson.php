<?php

namespace Modules\UnidadDeportiva\Entities\RFC;


interface HomoclavePerson
{
    function getFullNameForHomoclave();
}