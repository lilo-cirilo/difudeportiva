<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Persona extends Model
{
    use SoftDeletes;
    protected $table = 'personas';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['nombre'];

    public function servicios(){
        return $this->hasMany('\Modules\UnidadDeportiva\Entities\Socio');
    }

}
