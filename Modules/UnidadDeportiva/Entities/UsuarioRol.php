<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class UsuarioRol extends Model
{
    use SoftDeletes;
    protected $table = 'usuarios_roles';   
    protected $dates = ['deleted_at'];
}
