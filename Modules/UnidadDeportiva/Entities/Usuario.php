<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Usuario extends Model
{
    use SoftDeletes;
    protected $table = 'usuarios';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['id', 'usuario'];
}
