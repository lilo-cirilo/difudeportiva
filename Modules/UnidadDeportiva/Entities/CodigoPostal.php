<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class codigoPostal extends Model
{
    use SoftDeletes;
    protected $table = 'cat_codigos_postales';
}
