<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Grupo extends Model
{
    use SoftDeletes;
    protected $table = 'grupos';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['id', 'rfc', 'nombre']; 
}
