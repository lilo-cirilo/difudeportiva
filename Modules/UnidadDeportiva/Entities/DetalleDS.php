<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class detalleDS extends Model
{
    use SoftDeletes;
    protected $table = 'detalle_dias_servicios';   
    protected $dates = ['deleted_at'];    
    protected $fillable = [];
}
