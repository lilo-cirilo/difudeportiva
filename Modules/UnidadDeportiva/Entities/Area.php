<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Area extends Model
{
    use SoftDeletes;
    protected $table = 'areas';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['id', 'nombre', 'capacidad'];

    public function servicios(){
        return $this->hasMany('\Modules\UnidadDeportiva\Entities\Servicio');
    }
}
