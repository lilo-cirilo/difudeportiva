<?php

namespace Modules\Unidaddeportiva\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Servicio extends Model
{
    use SoftDeletes;
    protected $table = 'servicios';   
    protected $dates = ['deleted_at'];    
    protected $fillable = ['nombre','area','descripcion'];

    public function areas(){
        return $this->belongsTo('\Modules\UnidadDeportiva\Entities\Area','area','id');
    }
}
