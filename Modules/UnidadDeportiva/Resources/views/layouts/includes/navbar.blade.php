<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">
    <img class="navbar-brand-full" src="{{ asset('images/ImagenBienestarB.png') }}" alt="IntraDIF">
    <img class="navbar-brand-minimized" src="{{ asset('images/ImagenBienestarA.png') }}" width="30" height="30" alt="CoreUI Logo">
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="nav navbar-nav d-md-down-none"></ul>
  <ul class="nav navbar-nav ml-auto"></ul>
  <button class="navbar-toggler aside-menu-toggler d-lg-none" type="button" data-toggle="aside-menu-show">
    <span class="navbar-toggler-icon"></span>
	</button>
	<nav class="navbar navbar-static-top">
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
				<li>
        @if(auth()->user())
          <div class="fa fa-home"></div>
          <a onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="logaut"><i class="fa fa-sign-out"></i> 
            <span>Cerrar sesión</span>
          </a>
        @endif
				</li>
			</ul>
		</div>
	</nav>
</header>
