<div class="sidebar" style="z-index: 0;">
  <nav class="sidebar-nav">
    @if(auth()->user())
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="unidaddeportiva#/home">
          <i class="nav-icon fa fa-home"></i> Inicio
        </a>
      </li>
      <li>
      <center>
      <div class="miClase">
      {{ auth()->user()->usuario }} 
      </div>
      </center>
      </li>
      <li class="nav-title">Administrar</li>
      @if(auth()->user()->hasRolesStrModulo(['POLICIA'], 'unidaddeportiva'))
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/escanear">
            <i class="nav-icon fa fa-qr-code"></i>Escaneo de QR</a>
        </li>
      @endif
      @if(auth()->user()->hasRolesStrModulo(['INSTRUCTOR'], 'unidaddeportiva'))
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="">
          <i class="fas fa-swatchbook"></i>Material Deportivo</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="/unidaddeportiva#/material">
              <i class="nav-icon fa fa-codepen"></i>Listar</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/unidaddeportiva#/addMaterial">
              <i class="nav-icon fa fa-plus"></i>Agregar</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="/unidaddeportiva#/prestamo">
              <i class="nav-icon fa fa-list-alt"></i>Prestamos</a>
          </li> -->
        </ul>
      </li>
      <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="">
            <i class="cil-layers"></i>Incidencias</a>
          <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="/unidaddeportiva#/reportar">
              <i class="nav-icon fa fa-th-large"></i>Reportar</a>
          </li>
          </ul>
        </li>
      @endif
      @if(auth()->user()->hasRolesStrModulo(['COORDINADOR'], 'unidaddeportiva'))
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="">
            <i class="cil-layers"></i>Instalaciones</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/area">
                <i class="nav-icon fa fa-th-large"></i>Area</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/servicio">
                <i class="nav-icon fa fa-cogs"></i>Servicio</a>
            </li>
          </ul>
        </li>
        <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="">
          <i class="fas fa-usere"></i>   Personal</a>
        <ul class="nav-dropdown-items">
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/instructor">
            <i class="nav-icon fa fa-user"></i>Instructor</a>
        </li>
        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="">
          <i class="fas fa-swatchbook"></i>Procesos Administrativos</a>
        <ul class="nav-dropdown-items">
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/grupo">
            <i class="nav-icon fa fa-group"></i>Grupo</a>
        </li>
        <!-- <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/escanear">
            <i class="nav-icon fa fa-qr-code"></i>Escaneo de QR</a>
        </li> -->
        <!-- <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/horario">
            <i class="nav-icon fa fa-sticky-note"></i>Generar horario</a>
        </li> -->
        </ul>
      </li>
      <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="">
            <i class="fa fa-fw fa-wrench"></i> Socios</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/socio">
                <i class="nav-icon fa fa-user"></i>Activos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/solicitud">
                <i class="nav-icon fa fa-sticky-note"></i>Solicitudes</a>
            </li>
          </ul>
        </li>
        {{ auth()->user()->rol }}
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="">
          <i class="fas fa-swatchbook"></i>Reportes</a>
        <ul class="nav-dropdown-items">
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/graficas">
            <i class="nav-icon fa fa-group"></i>Graficas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/incidencias">
            <i class="nav-icon fa fa-group"></i>Incidencias</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/unidaddeportiva#/reportes">
            <i class="nav-icon fa fa-group"></i>Reportes</a>
        </li>
        </ul>
      </li>
      @endif
      @if(auth()->user()->hasRolesStrModulo(['CAPTURISTA'], 'unidaddeportiva'))
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="">
            <i class="fa fa-usersd"></i> Socios</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/socio">
                <i class="nav-icon fa fa-user"></i>Activos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/solicitud">
                <i class="nav-icon fa fa-sticky-note"></i>Solicitudes</a>
            </li>
          </ul>
        </li>
        {{ auth()->user()->rol }}
        @endif
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'unidaddeportiva'))
        <li class="nav-item nav-dropdown">
          <a class="nav-link nav-dropdown-toggle" href="">
            <i class="fa fa-usersd"></i>Usuarios</a>
          <ul class="nav-dropdown-items">
            <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/usuario">
                <i class="nav-icon fa fa-user"></i>Listar</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" href="/unidaddeportiva#/rol">
                <i class="nav-icon fa fa-sticky-note"></i>Rol</a>
            </li> -->
          </ul>
        </li>
      @endif
      <li class="divider"></li>
    </ul>
    @endif
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
