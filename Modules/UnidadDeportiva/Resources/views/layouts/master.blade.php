<!DOCTYPE html>
<html lang="es">
  <head>
	<base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
	<meta name="author" content="Łukasz Holeczek">
	<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
	<meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
	<meta name="usuario" content="@if(auth()->user()) {{ auth()->user()->usuario }} @endif">
	<meta name="rol" content="@if(auth()->user()) {{ auth()->user()->usuarioroles }} @endif">
    <title>UNIDAD DEPORTIVA</title>
    {{-- Laravel Mix - CSS File --}}
    <link href="{{ asset('unidep/app.css') }}" rel="stylesheet">
	<link href="{{ asset('unidep/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('unidep/style.css') }}" rel="stylesheet">

    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
		@include('unidaddeportiva::layouts.includes.navbar')

		<div class="app-body">
				@include('unidaddeportiva::layouts.includes.sidebar')

				<main class="main">

					{{--  @include('unidaddeportiva::layouts.includes.breadcrumb')  --}}

					<div class="container-fluid background">
						<div class="animated fadeIn">

							@yield('content')

						</div>
					</div>

				</main>

				{{-- @include('unidaddeportiva::layouts.includes.aside') --}}

			</div>
			<footer class="app-footer">
				<div>
					<span>DIF Oaxaca &copy; Copyright 2020.</span>
				</div>
				<div class="ml-auto">
					<span>Unidad de Informática</span>
				</div>
			</footer>
			<script src="{{ asset('unidep/app.js') }}"></script>
   	 {{-- <script src="{{ asset('unidep/principal.js') }}"></script> --}}
  	</body>
</html>
<style>
 .background {
	/* -webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none; */
	background: url("{{ asset('../../images/TxPGrayDIF.png') }}");
	background-color: white;
  	background-size: cover;
  	background-position: center;
  	background-repeat: no-repeat;
  	min-height: calc(100vh - 101px);
  	/* color: #3862FD !important; */
  	/* display: flex;  */
	margin-top: 1%;
  	align-items: center;
  	justify-content: center;
  	position: relative;
  }
 .span{
    color: #ff1111;
  }
  .secundario{
    background-color: #fd3463;
    border-radius: 60px;
    color: #ffffff;
  }
  .boton{
    border-radius: 40px;
  }
  .principal{
    background-color: #fda4d3;
    border-radius: 40%;
    border-color: #000000;
    border-width: 5px;
	/* height: 100px;
	width: 100px;
	background-color: #bbb;
	border-radius: 50%;
	display: inline-block; */
  }
  .logaut{
	  background-color: #ffffff;
  }
  .miClase{
	  background-color: #fd3463;
	  text-aling: center;
  }
  </style>
  @push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
@endpush
