<?php

Route::group(['middleware' => 'web', 'prefix' => 'canna', 'namespace' => 'Modules\CANNA\Http\Controllers'], function(){
  Route::get('/', 'BeneficiariosController@index')->name('canna.index');
  Route::resource('beneficiarios', 'BeneficiariosController', ['as' => 'canna']);
  Route::resource('citas', 'CitasController', ['as' =>'canna']);
  Route::resource('beneficiarios.fichadiagnostica', 'FDiagnosticaController');
  Route::resource('beneficiarios.programatrabajo', 'PTrabajoController');
  //Route::resource('actividades', 'ActividadesController');

  //Route::get('/solicitudes', 'PeticionController@index')->name('canna.peticiones');
  //Route::get('/solicitudes/{id}', 'PeticionController@show')->name('canna.peticiones.show');
  //Route::get('programascanna.select', 'ActividadesController@select')->name('programascanna.select');
  Route::get('beneficiarioscanna.select', 'BeneficiariosController@select')->name('beneficiarioscanna.select');
  Route::get('citas/muestraCitasMes/{id}', 'CitasController@muestraCitasMes')->name('citas.muestraCitasMes');
  Route::get('/notificaciones', 'PeticionController@notificaciones')->name('canna.peticiones.notificaciones');

  Route::post('beneficiarios/{id}/cambiaestado', 'BeneficiariosController@cambia_estado')->name('canna.beneficiarios.cambiaestado');
  //Route::post('/solicitudes/{id}/cambiaestado', 'PeticionController@cambia_estado')->name('canna.peticiones.cambiaestado');
  //Route::post('/solicitudes/{id}/personas', 'PeticionPersonasController@store')->name('canna.peticiones.persona.store');

  //Route::delete('/solicitudes/{id}/personas/{persona}', 'PeticionPersonasController@destroy')->name('canna.peticiones.persona.destroy');
  Route::resource('/peticiones', 'PeticionController',['as'=>'canna','only'=>['index','show','update','destroy']]);
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['as'=>'canna', 'except' => ['index'] ]);
  Route::resource('/programas', 'ProgramaController',['as'=>'canna']);
});
