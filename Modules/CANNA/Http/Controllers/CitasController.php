<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Cita;
use App\Models\Programa;

use View;

class CitasController extends Controller
{

  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('canna::citas.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return response()->json([
          'body' => view::make('canna::citas.modal')
          ->render()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();
          $aux = $request->except(['fecha']);
          $aux['fecha'] = (\DateTime::createFromFormat('Y/m/d', $request->fecha))->format('Y-m-d');
          $aux['empleado_id'] = $request->User()->persona->empleado->id;
          $aux['asistencia'] = 0;
          $aux['programa_id'] = Programa::where('nombre', 'CANNA')->first()->id;

          $cita = Cita::create($aux);

          auth()->user()->bitacora(request(), [
              'tabla' => 'citas',
              'registro' => $cita->id . '',
              'campos' => json_encode($cita) . '',
              'metodo' => request()->method()
          ]);

          DB::commit();
          return response()->json(array(['success' => true, 'code' => 200], 200));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array(['success' => false, 'code' => 409, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()], 409));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, Request $request)
    {
      $cita = Cita::find($id);
      $fecha = (\DateTime::createFromFormat('Y-m-d', $cita->fecha))->format('d/m/Y');
      $fecha_actual = Date('Y-m-d');
      $mayor=0;
      if(strtotime($cita->fecha) >= strtotime($fecha_actual)){
        $mayor=1;
      }
      $title = $cita->persona->get_nombre_completo();
      $persona_id = $cita->persona_id;
      $start = date("g:i a",strtotime($cita->hora_inicio));
      $end = date("g:i a",strtotime($cita->hora_fin));
      $color = $cita->color;
      $observaciones = $cita->observaciones;
      $asistencia = $cita->asistencia;
      $aux = (object) array('id' => $cita->id, 'title' => $title, 'persona_id' =>$persona_id, 'date' => $fecha, 'start' => $start, 'end' => $end, 'color' => $color, 'observaciones' => $observaciones, 'asistencia' => $asistencia);
      return response()->json([
        'body' => view::make('canna::citas.modal')
        ->with('cita',$aux)->with('mayor', $mayor)
        ->render()
      ], 200);
    }

    public function muestraCitasMes(Request $request){
      $eventos = array();
      if($request->has('start'))
      {
        //dd($request->all());
        $programa = Programa::where('nombre', 'CANNA')->first()->id;
        $citas = Cita::whereBetween('fecha', [$request->start, $request->end])->where('programa_id', $programa)->get();
        foreach ($citas as $cita) {
          $fecha = (\DateTime::createFromFormat('Y-m-d', $cita->fecha))->format('Y-m-d');
          $id = $cita->id;
          $title = $cita->persona->get_nombre_completo();
          $start = $fecha . ' ' . $cita->hora_inicio;
          $end = $fecha . ' ' . $cita->hora_fin;
          $backgoundColor = $cita->color;
          $borderColor = $cita->color;
          if($cita->asistencia == 1){
            $backgoundColor = '#909089';
            $borderColor = '#909089';
          }

          array_push($eventos, array('id' => $id, 'title' => $title, 'start' => $start, 'end' => $end, 'backgroundColor' => $backgoundColor, 'borderColor' => $borderColor));
        }
      }
      return $eventos;

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('canna::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();
          $aux = $request->except(['fecha', 'observaciones', 'asistencia']);
          $aux['asistencia'] = 0;
          if($request->asistencia == 'true'){
            $aux['asistencia'] = 1;
          }
          if(null !== $request->fecha){
            $aux['fecha'] = (\DateTime::createFromFormat('d/m/Y', $request->fecha))->format('Y-m-d');
          }
          if(null !== $request->observaciones){
            $aux['observaciones'] = $request->observaciones;
          }
          Cita::find($id)->update($aux);
          $cita = Cita::find($id);

          auth()->user()->bitacora(request(), [
              'tabla' => 'citas',
              'registro' => $cita->id . '',
              'campos' => json_encode($cita) . '',
              'metodo' => request()->method()
          ]);

          DB::commit();
          return response()->json(array(['success' => true, 'cita' => $aux, 'code' => 200], 200));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array(['success' => false, 'code' => 409, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()], 409));
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();
          $cita = Cita::find($id);
          auth()->user()->bitacora(request(), [
              'tabla' => 'citas',
              'registro' => $cita->id . '',
              'campos' => json_encode($cita) . '',
              'metodo' => request()->method()
          ]);
          $cita->delete();

          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }
}
