<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\SolicitudesPersona;

class PeticionPersonasController extends Controller
{
  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('canna::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('canna::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($id, Request $request)
    {
      if($request->ajax()){
        try {
          DB::beginTransaction();

          $sp_bd = SolicitudesPersona::create([
            'programas_solicitud_id' => $id,
            'persona_id' => $request->persona_id
          ]);

          auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_personas',
              'registro' => $sp_bd->id . '',
              'campos' => json_encode($sp_bd) . '',
              'metodo' => request()->method()
          ]);


          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('canna::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('canna::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id, $persona, Request $request)
    {
      if ($request->ajax()) {
        try {
          DB::beginTransaction();

          $sp_bd = SolicitudesPersona::where([
            'programas_solicitud_id' => $id,
            'persona_id' => $persona
          ])->first();


          auth()->user()->bitacora(request(), [
              'tabla' => 'solicitudes_personas',
              'registro' => $sp_bd->id . '',
              'campos' => json_encode($sp_bd) . '',
              'metodo' => request()->method()
          ]);

          $sp_bd->delete();

          DB::commit();
          return response()->json(array('success' => true));
        } catch (\Exception $e) {
          DB::rollBack();
          return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
        }
      }
    }
}
