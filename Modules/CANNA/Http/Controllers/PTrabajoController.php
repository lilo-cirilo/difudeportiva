<?php

namespace Modules\CANNA\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

use App\Models\PreguntasPersona;
use App\Models\RespuestasAbiertas;
use App\Models\Pregunta;
use App\Models\AlumnosCanna;
use App\Models\Encuesta;

class PTrabajoController extends Controller
{

  public function __construct(){
     $this->middleware(['auth', 'authorized']);
     $this->middleware('roles:ADMINISTRADOR,TERAPEUTA');
  }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('canna::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create($id)
    {
        return view('canna::beneficiarios.programasTrabajo.create')
        ->with('alumno_id', $id)
        ->with('opcion', 'create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */

  public function store($id, Request $request){
   if($request->ajax()){
     try {
       DB::beginTransaction();

       $encuesta = Encuesta::where('encuesta', '=', 'PROGRAMA DE TRABAJO')->first();
       $alumno = AlumnosCanna::where('id', $id)->first();
       $empleado = $request->User()->persona->empleado;
       $fecha = Carbon::now();
       $fecha = $fecha->toDateString();

       $preguntas_array = array('VESTIDO', 'ALIMENTACIÓN', 'BAÑO', 'HIGIENE', 'MOTRICIDAD GRUESA',
                                 'MOTRICIDAD FINA', 'IMITACIÓN', 'SENSORIAL', 'INTERACCIÓN SOCIAL',
                                 'PARTICIPACIÓN SOCIAL', 'JUEGO', 'CONDUCTA', 'INTERESES', 'LENGUAJE RECEPTIVO',
                                 'LENGUAJE EXPRESIVO', 'LENGUAJE CORPORAL', 'ATENCIÓN', 'COGNICIÓN',
                                 'ESQUEMA CORPORAL', 'OR. TEMPORO-ESPACIAL', 'PERCEPCIÓN', 'ATENCIÓN CONJUNTA',
                                 'NOTAS');
       $long_secciones = array(4,4,5,3,6,1);

       $pp['persona_id'] = $alumno->persona_id;
       $pp['empleado_id'] = $empleado->id;
       $pp['fecha_encuesta'] = $fecha;

       $salto = 0;
       $aux = 0;
       for ($i=0; $i < count($preguntas_array); $i++) {
         if($aux >= $long_secciones[$salto]){
           $aux=0;
           $salto++;
         }
         $pregunta =Pregunta::where([
           ['pregunta', '=', $preguntas_array[$i]],
           ['encuesta_id', '=', $encuesta->id]
         ])->first();
         $pp['pregunta_id'] = $pregunta->id;
         $pp_bd = PreguntasPersona::create($pp);
         auth()->user()->bitacora(request(), [
             'tabla' => 'preguntas_personas',
             'registro' => $pp_bd->id . '',
             'campos' => json_encode($pp_bd) . '',
             'metodo' => request()->method()
         ]);

         $respuesta['preguntas_persona_id'] = $pp_bd->id;
         if(null !== $request->input($salto)[$aux]['value']) {
           $respuesta['respuesta'] = $request->input($salto)[$aux]['value'];
         }
         else{
           $respuesta['respuesta'] = '';
         }
         $ra_bd = RespuestasAbiertas::create($respuesta);
         auth()->user()->bitacora(request(), [
             'tabla' => 'respuestasabiertas',
             'registro' => $ra_bd->id . '',
             'campos' => json_encode($ra_bd) . '',
             'metodo' => request()->method()
         ]);

         $aux++;
       }

       DB::commit();
       return response()->json(array('success' => true));
     } catch (\Exception $e) {
       DB::rollBack();
       return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
     }
   }
  }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, $num)
    {
      $respuestas = RespuestasAbiertas::whereBetween('preguntas_persona_id', [$num, $num+22])->get();
      //return $respuestas;
        return view('canna::beneficiarios.programasTrabajo.create')
        ->with('alumno_id', $id)
        ->with('respuestas', $respuestas)
        ->with('opcion', 'show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('canna::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */

  public function Update(Request $request){
   if ($request->ajax()) {
     try {
       DB::beginTransaction();
       //dd($request->all());

       $long_secciones = array(4,4,5,3,6,1);
       $salto = 0;
       $aux = 0;

       for ($i=0; $i < count($request->input(6)); $i++) {
         if($aux >= $long_secciones[$salto]){
           $aux=0;
           $salto++;
         }

         if($request->input($salto)[$aux]['value'] == null)
           RespuestasAbiertas::where('id', $request->input(6)[$i])->update(['respuesta' => '']);
         else
           RespuestasAbiertas::where('id', $request->input(6)[$i])->update(['respuesta' => $request->input($salto)[$aux]['value']]);

           $ra_bd = RespuestasAbiertas::find($request->input(6)[$i]);
           auth()->user()->bitacora(request(), [
               'tabla' => 'respuestasabiertas',
               'registro' => $ra_bd->id . '',
               'campos' => json_encode($ra_bd) . '',
               'metodo' => request()->method()
           ]);

         $aux++;
       }

       DB::commit();
       return response()->json(array('success' => true));
     } catch (\Exception $e) {
       DB::rollBack();
       return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
     }

   }
  }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
