@extends('canna::layouts.master')

@push('head')
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
</style>
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li> <a href="{{route('canna.beneficiarios.index')}}">Beneficiarios</a></li>
    <li class="active">Seleccionado</li>
@endsection

@section('content')
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Beneficiario</h3>
          </div>
          <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr>
                      <th style="width:30%">
                        <i class="fa fa-id-card margin-r-5"></i>FOLIO
                      </th>
                      <td>{{$alumno->folio}}</td>
                    </tr>
                    <tr>
                      <th>
                        <i class="fa fa-user margin-r-5"></i>BENEFICIARIO
                      </th>
                      <td>{{$alumno->persona->obtenerNombreCompleto()}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr>
                      <th style="width:30%">DIAGNOSTICO
                      </th>
                      <td>{{$alumno->diagnostico}}</td>
                    </tr>
                    <tr>
                      <th>EDAD</th>
                      <td>{{$alumno->persona->get_edad()}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Tutor(es)</h3>
            <div class="pull-right box-tools">
              <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="table-responsive">
                <table class="table table_bordered">
                  <tbody>
                    <thead>
                      <tr>
                        <th style="width:30%">
                          <i class="fa fa-user margin-r-5"></i>NOMBRE
                        </th>
                        <th>
                          <i class="fa fa-phone-square margin-r-5"></i>CELULAR
                        </th>
                        <th>
                          <i class="fa fa-phone margin-r-5"></i>TELEFONO</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($tutores as $tutor)
                        <tr>
                          <td>{{$tutor->persona->obtenerNombreCompleto()}}</td>
                          <td>{{$tutor->persona->numero_celular}}</td>
                          <td>{{$tutor->persona->numero_local}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12">
        <div class="box box-primary collapsed-box">
          <div class="box-header with-border">
            <h3 class="box-title">Informacion medica</h3>
            <div class="pull-right box-tools">
              <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-plus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr>
                      <th>MEDICO</th>
                      <td>{{$alumno->medico}}</td>
                    </tr>
                    <tr>
                      <th>MEDICAMENTO</th>
                      <td style="padding-top: 0px;">
                        <select  id="medicamento" multiple readonly disabled>
                          @foreach (json_decode($alumno->medicamento) as $item)
                              <option value="" selected>{{ $item }}</option>
                          @endforeach
                        </select>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="table-responsive">
                <table class="table">
                  <tbody>
                    <tr>
                      <th>OBSERVACIONES</th>
                      <td>{{$alumno->observaciones}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="col-md-6">
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">FICHAS DIAGNÓSTICAS</h3>
            <div class="pull-right box-tools">
              <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-md-12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width :30px">#</th>
                    <th>FECHA</th>
                    <th>TERAPEUTA</th>
                    <th style="width : 50px">VER</th>
                  </tr>
                </thead>
                <tbody>
                  @for ($i=0 ; $i < count($fdiagnosticas) ; $i++)
                    <tr>
                      <td>{{ $i+1 }}</td>
                      <td>{{ $fdiagnosticas[$i]->fecha_encuesta }}</td>
                      <td>{{ $fdiagnosticas[$i]->empleado->persona->obtenerNombreCompleto()}}</td>
                      <td>
                        <a href="/canna/beneficiarios/{{$alumno->id}}/fichadiagnostica/{{$fdiagnosticas[$i]->id}}" class="btn btn-info" title="Visualizar">
                          <i class="fa fa-search"></i>
                        </a>
                      </td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
          <div class="box-footer">
            <div class="col-lg-2">
              <a href="/canna/beneficiarios/{{$alumno->id}}/fichadiagnostica/create" class="btn btn-primary btn-sm">AGREGAR</a>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">PROGRAMAS DE TRABAJO</h3>
            <div class="pull-right box-tools">
              <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box-body">
            <div class="col-md-12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width :30px">#</th>
                    <th>FECHA</th>
                    <th>TERAPEUTA</th>
                    <th style="width : 50px">VER</th>
                  </tr>
                </thead>
                <tbody>
                  @for ($i=0 ; $i < count($ptrabajo) ; $i++)
                    <tr>
                      <td>{{ $i+1 }}</td>
                      <td>{{ $ptrabajo[$i]->fecha_encuesta }}</td>
                      <td>{{ $ptrabajo[$i]->empleado->persona->obtenerNombreCompleto()}}</td>
                      <td>
                        <a href="/canna/beneficiarios/{{$alumno->id}}/programatrabajo/{{ $ptrabajo[$i]->id }}" class="btn btn-info" title="Visualizar">
                          <i class="fa fa-search"></i>
                        </a>
                      </td>
                    </tr>
                  @endfor
                </tbody>
              </table>
            </div>
          </div>
          <div class="box-footer">
            <div class="col-lg-2">
              <a href="/canna/beneficiarios/{{$alumno->id}}/programatrabajo/create" class="btn btn-primary btn-sm">AGREGAR</a>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
@stop

@push('body')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('#medicamento').select2();
  })
</script>
@endpush
