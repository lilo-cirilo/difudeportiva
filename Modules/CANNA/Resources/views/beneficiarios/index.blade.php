@extends('canna::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Beneficiarios</li>
@endsection

@section('content')
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary shadow">
          <div class="box-header with-border">
            <h3 class="box-title">Beneficiarios</h3>
          </div>

          <div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
    					<input type="text" id="search" name="search" class="form-control">
    					<span class="input-group-btn">
    						<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
    					</span>
    				</div>

            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'alumnoscanna', 'name' => 'alumnoscanna', 'style' => 'width: 100%']) !!}

          </div>
        </div>
      </div>
    </div>
  </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">
  function ver_alumno(id){
    window.location.href = "{{ URL::to('canna/beneficiarios') }}" + '/' + id;
  }
  function editar_alumno(id) {
    console.log('alumno ' + id );
  }

  function activar_desactivar(id, estado){
    mensaje = estado['estado'] ? '¿Esta seguro de reactivar al beneficiario?' : '¿Esta seguro de dar de baja al beneficiario?';
    console.log(mensaje);
    swal({
      title: mensaje,
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí',
      cancelButtonText: 'No, regresar',
      reverseButtons: true,
      allowEscapeKey: false,
      allowOutsideClick: false
    }).then((result) => {
      if (result.value) {
        block();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: "POST",
          url: "{{URL::to('canna/beneficiarios')}}" + '/' + id + '/cambiaestado',
          data: estado,
          dataType: 'json',
          success: function(data) {
            unblock();
            console.log(data);
            $("#alumnoscanna").DataTable().ajax.reload();
          },
          error : function(data){
            unblock();
            console.log(data);
          }
        });
      }
    });
  }

  var table = (function() {
  	var tabla = undefined,
  	btn_buscar = $('#btn_buscar'),
  	search = $('#search');

  	function init() {
  		search.keypress(function(e) {
  			if(e.which === 13) {
  				tabla.DataTable().search(search.val()).draw();
  			}
  		});

  		btn_buscar.on('click', function() {
  			tabla.DataTable().search(search.val()).draw();
  		});

  		tabla.on( 'draw.dt', function () {
  			$(".myToggle").each(function(){
        		$(this).bootstrapToggle();
        		$(this).change(function() {
      				activar_desactivar($(this).attr('id'),{estado : $(this).prop('checked') });
    			})
    		});
  		});
  	};

  	function set_table(valor) {
  		tabla = $(valor);
  	};

  	function get_table() {
  		return tabla;
  	};

  	return {
  		init: init,
  		set_table: set_table,
  		get_table: get_table
  	};
  })();

  table.set_table($('#alumnoscanna').dataTable());
  table.init();
</script>
@endpush
