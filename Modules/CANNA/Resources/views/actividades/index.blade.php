@extends('canna::layouts.master')

@push('head')
<style type="text/css">
</style>
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content-subtitle', '')

@section('li-breadcrumbs')
    <li class="active">Actividades</li>
@endsection

@section('content')
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Actividades</h3>
          </div>

          <div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
    					<input type="text" id="search" name="search" class="form-control">
    					<span class="input-group-btn">
    						<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
    					</span>
    				</div>

            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'actividades', 'name' => 'Actividades', 'style' => 'width: 100%']) !!}

          </div>
        </div>
      </div>
    </div>
  </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">

var table = (function() {
  var tabla = undefined,
  btn_buscar = $('#btn_buscar'),
  search = $('#search');

  function init() {
    search.keypress(function(e) {
      if(e.which === 13) {
        tabla.DataTable().search(search.val()).draw();
      }
    });

    btn_buscar.on('click', function() {
      tabla.DataTable().search(search.val()).draw();
    });
  };

  function set_table(valor) {
    tabla = $(valor);
  };

  function get_table() {
    return tabla;
  };

  return {
    init: init,
    set_table: set_table,
    get_table: get_table
  };
})();

table.set_table($('#Actividades').dataTable());
table.init();

function editar_actividad(id) {
  window.location.href = "{{ URL::to('canna/actividades') }}" + '/' + id + "/edit";
}

function eliminar_actividad(id) {
  swal({
    title: '¿Está seguro de eliminar la actividad?',
    text: '¡La actividad se eliminará de forma permanente!',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, eliminar',
    cancelButtonText: 'No, regresar',
    reverseButtons: true,
    allowEscapeKey: false,
    allowOutsideClick: false
  }).then((result) => {
    if (result.value) {
      $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });
      $.ajax({
        url: "{{ URL::to('canna/actividades') }}" + '/' + id,
        type: 'POST',
        data: {_method: 'delete'},
        success: function(response) {
          table.get_table().DataTable().ajax.reload(null, false);
          swal({
            title: '¡Eliminado!',
            text: 'El producto ha sido eliminado.',
            type: 'success',
            timer: 3000
          });
        },
        error: function(response) {
          if(response.status === 422){
            swal({
              title: 'Error al eliminar la actividad.',
              text: response.responseJSON.errors[0].message,
              type: 'error',
              confirmButtonColor: '#3085d6',
              confirmButtonText: 'Regresar',
              allowEscapeKey: false,
              allowOutsideClick: false
            });
          }
        }
      });
    }
  });
}
</script>
@endpush
