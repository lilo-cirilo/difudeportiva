@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>

@if (auth()->check())
@section('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm')
@section('user-name', auth()->user()->persona->nombre)
@section('user-job')
@section('user-log', auth()->user()->created_at)
@endif

@section('logo', asset('images/CANNA/ImagenCannaPrograma.png'))
@section('mini-logo', asset('images/CANNA/ImagenCannaIcono.png'))


@section('content-title')
  <img src="{{asset('images/CANNA/ImagenCannaTitular.png')}}" alt="">
@endsection

@push('head')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/blue.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('task-number')
@section('task-text', 'No tienes tareas')


@section('breadcrumbs')
<ol class="breadcrumb">
  <li><a href="{{ route('canna.index') }}"><i class="fa fa-dashboard"></i>CANNA</a></li>
  @yield('li-breadcrumbs')
</ol>
@endsection

@section('sidebar-menu')
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header active">CANNA</li>
      <li>
    		<a href="{{ route('home') }}">
    			<i class="fa fa-home"></i><span>IntraDIF</span>
    		</a>
    	</li>
      <li {{ ($ruta === 'canna.index') ? 'class=active' : '' }}>
    		<a href="{{ route('canna.index') }}">
    			<i class="fa fa-dashboard"></i><span>Dashboard</span>
    		</a>
    	</li>
      <li class="{{ ($ruta === 'canna.beneficiarios.index' || $ruta === 'canna.beneficiarios.create') ? 'treeview active menu-open' : 'treeview' }}">
        <a href="#">
          <i class="fa fa-group"></i> <span>Beneficiarios</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li {{ ($ruta === 'canna.beneficiarios.index') ? 'class=active' : '' }}><a href="{{ route('canna.beneficiarios.index')}}"><i class="fa fa-list-alt"></i>Lista de Beneficiarios</a></li>
          <li {{ ($ruta === 'canna.beneficiarios.create') ? 'class=active' : '' }}><a href="{{ route('canna.beneficiarios.create')}}"><i class="fa fa-floppy-o"></i>Nuevo Beneficiario</a></li>
        </ul>
      </li>
      <li {{ ($ruta === 'canna.citas.index') ? 'class=active' : '' }}>
        <a href="{{route('canna.citas.index')}}">
          <i class="fa fa-calendar"></i> <span>Calendario</span>
        </a>
      </li>
      
      <li class="treeview">
      <a href="#">
        <i class="fa fa-handshake-o"></i> <span>Peticiones</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('canna.peticiones.index') }}"><i class="fa fa-list-alt"></i> Lista de peticiones</a></li>
        <li><a href="{{ route('canna.programas.index') }}"><i class="fa fa-cubes"></i> Beneficios</a></li>
      </ul>
    </li>
    </ul>
@endsection

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		setTimeout(unblock_error, 120000);
	}

	function unblock() {
		$.unblockUI();
	}
</script>
@endpush
