@extends('canna::layouts.master')

@push('head')
<style type="text/css">
</style>
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content-subtitle', '')

@if (isset($notificaciones) && $notificaciones->count() > 0)
  @section('notification-text', 'Tienes ' . $notificaciones->count() . ($notificaciones->count() == 1 ? ' solicitud nueva' : ' solicitudes nuevas'))
  @section('notification-number',  $notificaciones->count() )
  @section('notification-all', 'Ver todas las solicitudes')

@else
  @section('notification-text', 'No tienes solicitudes')
@endif

@section('li-breadcrumbs')
    <li class="active">Solicitudes</li>
@endsection

@section('content')
	<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary shadow">
          <div class="box-header">
            <h3 class="box-title">Solicitudes</h3>
          </div>

          <div class="box-body">
            <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
    					<input type="text" id="search" name="search" class="form-control">
    					<span class="input-group-btn">
    						<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
    					</span>
    				</div>
            {{-- {{$solicitud->first()->statussolicitudes->first()->statusproceso->status}} --}}
            {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'peticiones', 'name' => 'peticiones', 'style' => 'width: 100%']) !!}
          </div>
        </div>
      </div>
    </div>
  </section>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">

function cancelar(id){
  swal({
    title: '¿Está seguro de cancelar la solicitud?',
    text: '',
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí',
    cancelButtonText: 'No',
    reverseButtons: true,
    allowEscapeKey: false,
    allowOutsideClick: false
  }).then((result) => {
    if (result.value) {
      block();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: "POST",
        url: "{{URL::to('canna/peticiones')}}" + "/" + id + '/cambiaestado',
        data: {status : 'CANCELADO'},
        dataType: 'json',
        success: function(data) {
          $("#peticiones").DataTable().ajax.reload();
          unblock();
          console.log(data);
        },
        error : function(data){
          unblock();
          console.log(data);
        }
      });
    }
  });
}

var table = (function() {
  var tabla = undefined,
  btn_buscar = $('#btn_buscar'),
  search = $('#search');

  function init() {
    search.keypress(function(e) {
      if(e.which === 13) {
        tabla.DataTable().search(search.val()).draw();
      }
    });

    btn_buscar.on('click', function() {
      tabla.DataTable().search(search.val()).draw();
    });
  };

  function set_table(valor) {
    tabla = $(valor);
  };

  function get_table() {
    return tabla;
  };

  return {
    init: init,
    set_table: set_table,
    get_table: get_table
  };
})();

table.set_table($('#peticiones').dataTable());
table.init();
</script>

@endpush
