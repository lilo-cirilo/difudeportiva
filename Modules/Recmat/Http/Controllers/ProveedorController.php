<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use App\Models\RecmCatProveedor;
use Validator;

class ProveedorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('recmat', ['only' => ['show', 'buscarProveedor']]);
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR,FINANCIERO,AREACAPTURISTA,USUARIORM,RMATERIALES', ['only' => ['show', 'buscarProveedor']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('recmat::proveedores.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'nombre' => 'required',
            'inscripcion' => 'required',
            'rfc' => 'required|unique:recm_cat_proveedors,rfc',
            'calle' => 'required',
            'ext' => 'required',
            'colonia' => 'required',
            'codigopostal' => 'required|digits:5',
            'ciudad' => 'required',
            'ciudad' => 'required',
            'telefono' => 'required|digits:10',
            'email' => 'required',
            'banco' => 'required',
            // 'cuenta' => 'required|digits:11',
            'clabe' => 'required'
        ]);
        if ($validacion->fails()) {
            return new JsonResponse($validacion->errors(), 422);
        }
        try{
            $proveedor = RecmCatProveedor::create([
                'nombre' => strtoupper($request->nombre),
                'rfc' => strtoupper($request->rfc),
                'calle' => strtoupper($request->calle),
                'numero' => strtoupper($request->ext),
                'colonia' => strtoupper($request->colonia),
                'codigopostal' => $request->codigopostal,
                'ciudad' => strtoupper($request->ciudad),
                'numero_cuenta' => $request->cuenta,
                'clabe' => $request->clabe,
                'no_inscripcion' => $request->inscripcion,
                'banco' => strtoupper($request->banco),
                'correo' => $request->email,
                'telefono' => $request->telefono,
                'usuario_id' => $request->usuario_id
            ]);
            return new JsonResponse($proveedor);
        }catch(\Exception $e){
            return new JsonResponse(['msg' => 'Algo salio mal', 'error' => $e->getMessage()], 422);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('recmat::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarProveedor($proveedor)
    {
        return new JsonResponse(RecmCatProveedor::where('nombre', 'like', "%$proveedor%")->take(10)->get());
    }
}