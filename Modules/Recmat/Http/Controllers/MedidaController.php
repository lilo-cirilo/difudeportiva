<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Models\Unidadmedida;

class MedidaController extends Controller
{
    public function index()
    {
        if(request()->wantsJson()){
            $medidas = Unidadmedida::all();
            foreach($medidas as $medida){
                $medida->editing = false;
            }
            return new JsonResponse($medidas);
        }
    }

    public function store(Request $request){
        $medida = Unidadmedida::create($request->all());
        if($medida->id){
            $medida->editing = false;
            return new JsonResponse($medida);
        }
        return new JsonResponse(['status' => 'error', 'msg' => 'Ocurrio un error', 'data' => $request->all()], 422);
    }

    public function update(Request $request, $id){
        $medida = Unidadmedida::findOrFail($id);
        if($medida){
            $medida->fill($request->all());
            $medida->save();
            return new JsonResponse($medida);
        }
        return new JsonResponse(['status' => 'error', 'msg' => 'Ocurrio un error, reintente'],403);
    }

    public function destroy($id){
        $medida = Unidadmedida::findOrFail($id);
        if($medida){
            $medida->delete();
            return new JsonResponse($medida);
        }
        return new JsonResponse(['status' => 'error', 'msg' => 'Ocurrio un error', 'data' => $id], 422);
    }
    
    public function searchMeasure($name)
    {
        if(request()->wantsJson()){
            $measures = Unidadmedida::where('unidadmedida', 'like', '%' . $name . '%')->get();
            return new JsonResponse($measures);
        }
        return new JsonResponse(['status' => 'error', 'msg' => 'Ocurrio un error', 'data' => $name], 403);

    }
}
