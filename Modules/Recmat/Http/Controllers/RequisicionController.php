<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;

use App\Models\RecmPresentacionProducto;
use App\Models\RecmRequisicion;
use App\Models\Empleado;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\RecmProductoRequisicion;
use App\Models\RecmHistorialRequisicion;
use App\Models\AreasResponsable;
use App\Models\RecmCatEstatus;
use App\Models\RecmAreaRol;
use App\Models\Nivel;

use NumerosEnLetras;
use App\Models\RecmOrdeneCompra;

class RequisicionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('recmat', ['only' => ['show', 'buscarPorFolio']]);
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR,FINANCIERO,AREACAPTURISTA,USUARIORM,RMATERIALES,MANTENIMIENTO', ['only' => ['show', 'buscarPorFolio']]);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'recmat'))
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->orderBy('id', 'DESC')->paginate(20));
        else if(auth()->user()->hasRolesStrModulo(['RMATERIALES'], 'recmat'))
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('estatus_id',2)->orderBy('id', 'DESC')->paginate(20));
        else if(auth()->user()->hasRolesStrModulo(['AREACAPTURISTA'], 'recmat'))
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('usuario_id', auth()->user()->id)->orderBy('id', 'ASC')->paginate(20));
        else if(auth()->user()->hasRolesStrModulo(['ALMACEN'], 'recmat'))
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra')->where('estatus_id','>',3)->where('tipo', 1)->orderBy('estatus_id', 'DESC')->paginate(20));
        else { // debe de entrar solo usuariorm
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra')->where('usuario_responsable', auth()->user()->id)->where('estatus_id','>',3)->orderBy('estatus_id', 'ASC')->paginate(20));
        }  
    }
    // para la visualizar todas las requisiciones admin marcelino
    public function listaRequisicionesAll ()
    {
        return new JsonResponse(RecmRequisicion::with('usuario','estatus','tipo')->where('estatus_id','>',3)->orderBy('created_at', 'DESC')->paginate(20)); 
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        try {
            DB::beginTransaction();
            $areaRol = ($request->tipoSelect['id'] == 1) ? RecmAreaRol::where('area_id',$request->area['area_id'])->firstOrFail() : Usuario::where('usuario','LOREVS')->firstOrFail(); // para saber que usuario de materiales puede ver la requisicion ya que estan asignados en areas
            $requisicion = new RecmRequisicion();

            $requisicion->justificacion = $request->justificacion;
            $requisicion->fecha_captura = date("Y-m-d H:i:s");
            $requisicion->partida_id    = $request->partida['id'];
            $requisicion->area_id       = $request->area['area_id'];
            $requisicion->areas_responsable_id = AreasResponsable::where('area_id', $request->area['area_id'])->first()->id;
            $requisicion->usuario_id    = auth()->user()->id;
            $requisicion->estatus_id    = 1;
            $requisicion->usuario_responsable   = ($request->tipoSelect['id'] == 1) ? $areaRol->usuario_id : $areaRol->id;
            $requisicion->tipo = $request->tipoSelect['id'];
        
            $requisicion->save();

            $requisicion->folio         = "RM-".$requisicion->id."-".date("Y");
            $requisicion->save();

            foreach ($request->listProductos as $value) {
                
	    		DB::table('recm_producto_requisicions')->insert([
	    			'requisicion_id'	        => $requisicion->id,
	    			'presentacion_producto_id'	=> $value['idPresentacion'],
	    			'observacion'           	=> $value['observacion'],
	    			'status_id'		            => 1, // poner el id del status correspondiente
                    'cantidad'                  => $value['cantidad'],
                    'precio'                    => (float)$value['precio'] > 100000 ? 0 : (float)$value['precio'],
                    'created_at'                => date('Y-m-d H:m:s'), 
                    'updated_at'                => date('Y-m-d H:m:s')
	    		]);
            };
            
            $historial = new RecmHistorialRequisicion();
            $historial->requisicion_id = $requisicion->id;
            $historial->status_id = 1; // poner el id correspondiente, este es de prueba
            $historial->usuario_id = auth()->user()->id;

            $historial->save();

            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'RecmRequisicion',
                'registro' => $requisicion->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$requisicion->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }

    }

    /**
     * Visualizar la requisicion, y para generar la orden de compra
     */
    public function show($id)
    {
        $requisicion = RecmRequisicion::where('id',$id)->with('productos','estatus','partida','tipo')->firstOrFail();       
        $user = Usuario::findOrFail($requisicion->usuario_id);
        $empleado = Empleado::where('persona_id',$user->persona_id)->with('area','nivel')->firstOrFail();
        $autoriza = $requisicion->areas_responsable_id != null ? AreasResponsable::withTrashed()->where('id',$requisicion->areas_responsable_id)->with('empleado','area')->firstOrFail() : AreasResponsable::where('area_id',$requisicion->area_id)->with('empleado','area')->firstOrFail();

        $sumtotal = 0;

        foreach($requisicion->productos as $producto){
            $sumtotal = (float)$sumtotal + (float)$producto->precioT;
            $producto['color'] ="text-yellow";
        }
        
        return new JsonResponse([
            'solicitante' => Usuario::where('id',$requisicion->usuario_id)->with('persona')->firstOrFail(),
            'empleado' => $empleado,
            'autoriza' => Persona::where('id',$autoriza->empleado->persona_id)->select('nombre','primer_apellido','segundo_apellido')->firstOrFail(), // datos juan
            'nivel'=>  Empleado::where('persona_id',$autoriza->empleado->persona_id)->with('nivel')->firstOrFail(),
            'areaResp' => $autoriza,
            'requisicion' => $requisicion,
            'sumTotal' => $sumtotal,
            'direccion' => Nivel::find($empleado->nivel_id),
            'responsable' => DB::select('call getPadre(?)',[$requisicion->area_id])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $requisicion = RecmRequisicion::findOrFail($request->id);
            $requisicion->justificacion = $request->justificacion;
            $requisicion->save();

            foreach($requisicion->productos as $producto){
                DB::table('recm_producto_requisicions')->where('id',$producto->pivot->id)->delete();
            }
            
            foreach($request->productos as $prod){
                $prodRec = new RecmProductoRequisicion();
                $prodRec->requisicion_id = $requisicion->id;
                $prodRec->presentacion_producto_id = $prod['pivot']['presentacion_producto_id'];
                $prodRec->status_id = 1;
                $prodRec->cantidad = $prod['cantidadT'];
                $prodRec->precio = $prod['pivot']['precio'];
                $prodRec->observacion = $prod['pivot']['observacion'];
                $prodRec->save();
            }
            
            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'RecmRequisicion',
                'registro' => $requisicion->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$requisicion->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function presentacion($id)
    {
        return new JsonResponse(RecmPresentacionProducto::where('producto_id',$id)->with('presentacion')->get());
    }

    public function precio($id)
    {
        return new JsonResponse(RecmPresentacionProducto::findOrFail($id));
    }

    public function cancel ($id,$msj)
    {
        // return $msj;
        $status = RecmCatEstatus::where('estatus','RECHAZADO')->select('id')->firstOrFail();
        $requisicion =  RecmRequisicion::find($id);
        $requisicion->estatus_id = $status->id;
        $requisicion->observacion = $msj;
        // $requisicion->usuario_id = auth()->user()->id;
        $requisicion->save();
        
        $historial = new RecmHistorialRequisicion();
        $historial->requisicion_id = $requisicion->id;
        $historial->status_id = $status->id;
        $historial->usuario_id = auth()->user()->id;

        auth()->user()->bitacora(request(), [
            'tabla' => 'RecmRequisicion',
            'registro' => $id . '',
            'campos' => json_encode($msj) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);

        return new JsonResponse($historial->save());
    }

    public function history($id)
    {
        return new JsonResponse(RecmHistorialRequisicion::where('requisicion_id',$id)->with('status','usuario')->orderBy('id','DESC')->get());
    }

    public function buscarPorFolio($token, $folio)
    {
        if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'recmat')) // administrador
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->take(15)->get());
        else if(auth()->user()->hasRolesStrModulo(['ALMACEN'], 'recmat')) // creo que es para almacen
           return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->where('estatus_id','>',3)->take(15)->get());
        else if(auth()->user()->hasRolesStrModulo(['MANTENIMIENTO'], 'recmat')) // usuarios de mantenimiento
           return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->where('estatus_id', '>',3)->where('usuario_responsable', auth()->user()->id)->take(15)->get());
        else if(auth()->user()->hasRolesStrModulo(['AREACAPTURISTA'], 'recmat')) // usuarios de las areas solicitantes
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->where('estatus_id', '>',3)->where('usuario_id', auth()->user()->id)->take(15)->get());
        else    // usuario de recursos materiales
            return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->where('estatus_id', '>',3)->where('usuario_responsable', auth()->user()->id)->take(15)->get());
        // 
    }

    public function changestatus(Request $request)
    {
        try {
            DB::beginTransaction();
            
            $partidas = collect([
                ['partida' => 324], //257
                ['partida' => 278], //205
                ['partida' => 336], //306
                ['partida' => 337], //307
                ['partida' => 347], //318
                ['partida' => 221], //323
                ['partida' => 102], //351
                ['partida' => 155], //509
                ['partida' => 175], //530
                ['partida' => 208], //555
                ['partida' => 154], //507
                ['partida' => 152], //505
            ]);
            $requisicion = RecmRequisicion::find($request->id);
            $valido = $partidas->firstWhere('partida', $requisicion->partida_id);

            if($requisicion->estatus_id == 1) // para que se valide cuando se envia a materiales y no despues de los cambios
            {
                if($valido != null && !auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'recmat'))
                    return response()->json(['success' => false, 'code' => 200, 'message'=>$valido['partida'] == 324 ? 'Se requiere dictamen Interno de la Unidad de Informatica' : 'Se requiere dictamen  de la DGTID gestionado por la Unidad de Informatica'], 200);
            }
           
            $requisicion->estatus_id    = $request->status;
            $requisicion->save();         
            
            $historial = new RecmHistorialRequisicion();
            $historial->requisicion_id = $requisicion->id;
            $historial->status_id = $request->status;
            $historial->usuario_id = auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'recmat') ?  $requisicion->usuario_id : auth()->user()->id;

            $historial->save();

            if($request->status == 7)
            {
                foreach($requisicion->ordencompra as $odc){
                    $ordenCompra = RecmOrdeneCompra::find($odc->id);
                    $ordenCompra->status_id = 8; // se envian las ordenes de compra a finanzas para validacion
                    $ordenCompra->save();
                }
            }

            DB::commit();

            auth()->user()->bitacora(request(), [
                'tabla' => 'RecmRequisicion',
                'registro' => $requisicion->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folio'=>$requisicion->folio], 200);
            
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }
    public function listOrdenMantenimiento()
    {
        return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('tipo', 2)->where('estatus_id','>',3)->orderBy('estatus_id', 'ASC')->paginate(20));
    }

    public function searchFolio($folio)
    {
        return new JsonResponse(RecmRequisicion::with('usuario','estatus','ordencompra','tipo')->where('folio', 'like', "%$folio%")->where('estatus_id', '>',3)->where('usuario_id', auth()->user()->id)->take(15)->get());
    }
}