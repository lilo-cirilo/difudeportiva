<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\RecmCatPartida;
use Illuminate\Http\JsonResponse;

class PartidaController extends Controller
{
    public function __construct()
    {
        $this->middleware('recmat', ['only' => ['show', 'searchProduct', 'store']]);
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR,FINANCIERO,AREACAPTURISTA,USUARIORM,RMATERIALES', ['only' => ['show', 'searchProduct', 'store']]);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('recmat::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('recmat::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return new JsonResponse(RecmCatPartida::select('partida_especifica_c','partida_especifica','tipo','concepto')->where('partida_especifica_c','>',200)->orderBy('partida_especifica_c', 'ASC')->paginate(20));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('recmat::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    
    public function searchProduct($nombre, $tipo)
    {
        $partidas = RecmCatPartida::where('tipo',$tipo)->where(function ($q) use ($nombre) {
            $q->where('partida_especifica', 'like',"%$nombre%")->orWhere('partida_especifica_c', $nombre);
        })->take(10)->get();
        return new JsonResponse($partidas);
    }
}
