<?php

namespace Modules\Recmat\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\RecmCatProducto;
use App\Models\RecmCatPresentacion;
use App\Models\RecmPresentacionProducto;
use App\Models\RecmCatPartida;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('recmat', ['only' => ['show', 'searchProduct', 'store','searchProductPartida']]);
        $this->middleware('rolModuleV2:recmat,ALMACEN,ADMINISTRADOR,FINANCIERO,AREACAPTURISTA,USUARIORM,RMATERIALES', ['only' => ['show', 'searchProduct', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RecmPresentacionProducto::with('partida')->paginate(10);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'producto' => 'required',
            'presentaciones' => 'required|array',
            'partida' => 'required|array'
        ]);
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), 422);
        }
        try{
            DB::beginTransaction();
            $productoNew = RecmCatProducto::firstOrCreate(['id' => $request->producto['id']], ['producto' => strtoupper($request->producto['producto']), 'usuario_id' => $request->usuario_id]);
            $productosNuevos = [];
            foreach($request->presentaciones as $key=>$presentacion){
                if(isset($presentacion['nuevo']) && $presentacion['nuevo'] &&  RecmCatPresentacion::where('presentacion',strtoupper($presentacion['presentacion']))->count() == 0 ){
                    $presentacion = RecmCatPresentacion::create([
                        'presentacion' => strtoupper($presentacion['presentacion']),
                        'usuario_id' => $request->usuario_id
                    ]);
                }
                foreach($request->partida as $index=>$partida){
                    $productoPresentacion = RecmPresentacionProducto::firstOrCreate([
                        'producto_id' => $productoNew->id, 
                        'presentacion_id' => $presentacion['id'], 
                        'partida_id' => $partida['id']
                        ], [
                        'precio' => $request->precios[$key]]);
                    if($productoPresentacion->wasRecentlyCreated){
                        array_push($productosNuevos, $productoPresentacion);
                    }
                }
            }
            DB::commit();
            return new JsonResponse($productosNuevos);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse($e->getError(), 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $producto = RecmCatProducto::findOrFail($id);
        $producto->fill($request->all());
        $producto->save();
        return new JsonResponse($producto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = RecmCatProducto::findOrFail($id);
        $producto->delete();
        return new JsonResponse(['status' => 'ok', 'data' => $producto]);
    }

    public function searchProduct($token, $nombre)
    {
        $productos = RecmPresentacionProducto::whereHas('producto', function($query) use($nombre){
            $query->where('producto', 'like', "%$nombre%");
        })->take(10)->get();
        // $productos = RecmCatProducto::where('producto', 'like', "%$nombre%")->with('presentaciones')->take(10)->get();
        return new JsonResponse($productos);
    }

    public function buscarPresentacion($token, $nombre)
    {
        return new JsonResponse(RecmCatPresentacion::where('presentacion', 'like', "%$nombre%")->take(10)->get());
    }

    public function searchProductPartida($nombre, $id)
    {
        $productos = RecmPresentacionProducto::whereHas('producto', function($query) use($nombre){
            $query->where('producto', 'like', "%$nombre%");
        })->where('partida_id',$id)->take(10)->get();
        return new JsonResponse($productos);
    }

    public function buscarProducto($token, $nombre)
    {
        return new JsonResponse(RecmCatProducto::where('producto', 'like', "%$nombre%")->take(10)->get());
    }

    public function buscarProductoPartida($nombre, $token)
    {
        $ids = RecmCatProducto::select('id')->where('producto', 'like', "$nombre")->get();
        
        return new JsonResponse(RecmCatPartida::with('productos')->whereHas('productos', function ($query) use($ids) {
            $query->whereIn('producto_id', $ids);
        })->get());
    }
}
