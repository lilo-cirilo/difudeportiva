<?php

Route::group(['middleware' => 'web', 'prefix' => 'recmat', 'namespace' => 'Modules\Recmat\Http\Controllers'], function()
{
    Route::get('/', 'RecmatController@index');
    
    Route::group(['middleware' => 'web', 'prefix' => 'admin'], function(){
        Route::get('/', 'AdminController@index');
        Route::get('/productos/{token}/{nombre}', 'ProductoController@buscarProducto');
        Route::get('/presentaciones/{token}/{nombre}', 'ProductoController@buscarPresentacion');
        Route::get('/partidas/{token}/{nombre}', 'AdminController@buscarPartida');
        Route::resource('/medidas', 'MedidaController');
        Route::get('/medidas/s/{nombre}', 'MedidaController@searchMeasure');
        Route::get('/productos/s/{nombre}', 'ProductoController@searchProduct');
        Route::get('/datos', 'AdminController@userDatos');
        Route::get('/acuse', 'AdminController@acuse');
        Route::get('/area/{id}', 'AdminController@areaFind');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'proveedores'], function(){
        Route::get('/', 'ProveedorController@index');
        Route::post('/', 'ProveedorController@store');
        Route::get('/{proveedor}', 'ProveedorController@buscarProveedor');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'almacen'], function(){
        Route::get('/', 'AlmacenController@index');
        Route::get('/entradas', 'AlmacenController@entradas')->middleware('recmat');
        Route::get('/entradas/{id}', 'AlmacenController@detalleEntrada');
        Route::post('/entradas', 'AlmacenController@guardarEntrada');
        Route::get('/salidas/requision/{requisicion_id}/productos', 'AlmacenController@productosDisponibles');
        Route::post('/salidas/requisicion/{requisicion_id}', 'AlmacenController@surtirRequisicion');
        Route::post('/salidas/requisicion/{requisicion_id}/enviar', 'AlmacenController@pasarCompra');
        Route::get('/entradas/{token}/{folio}', 'AlmacenController@buscarEntrada');
        Route::get('/salidas/{token}/{folio}', 'AlmacenController@buscarSalida');
        Route::get('/estatus/{id}/{tipo}', 'AlmacenController@changeStatus');
        Route::get('/folio/{id}/{tipo}', 'AlmacenController@searchEntrada');
        Route::get('/listSalida', 'AlmacenController@salidas');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'productos'], function(){
        Route::resource('/', 'ProductoController');
        Route::get('/{token}/{nombre}', 'ProductoController@searchProduct');
        Route::get('/partidaProd/{nombre}/{id}', 'ProductoController@searchProductPartida');
        Route::get('/{token}/partidas/{partida?}', 'ProductoController@productoPartida');
        Route::get('/partidas/{nombre}/{token}', 'ProductoController@buscarProductoPartida');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'partida'], function(){
        Route::resource('/', 'ProductoController');
        Route::get('/{nombre}/{tipo}', 'PartidaController@searchProduct');
        Route::get('/partidaList', 'PartidaController@show');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'requisicion'], function(){
        Route::get('/', 'RequisicionController@index');
        Route::post('/save', 'RequisicionController@store');
        Route::get('/presentacion/{id}', 'RequisicionController@presentacion');
        Route::get('/precio/{id}', 'RequisicionController@precio');
        Route::get('/listaRequisiciones', 'RequisicionController@index');
        Route::get('/listaRequisicionesAll', 'RequisicionController@listaRequisicionesAll');
        Route::get('/{id}', 'RequisicionController@show')->where('id', '[0-9]+');
        Route::get('/cancelar/{id}/{msj}', 'RequisicionController@cancel');
        Route::get('/historial/{id}', 'RequisicionController@history');
        Route::get('/{token}/{folio}', 'RequisicionController@buscarPorFolio');
        Route::post('/cambioestatus', 'RequisicionController@changestatus');
        Route::post('/update', 'RequisicionController@update');
        Route::get('/odMantenimiento', 'RequisicionController@listOrdenMantenimiento');
        Route::get('/searFolio/{folio}', 'RequisicionController@searchFolio');
    });

    Route::group(['middleware' => 'web', 'prefix' => 'ordendecompra'], function(){
        Route::resource('/', 'OrdendecompraController');
        Route::post('/save', 'OrdendecompraController@store');
        Route::post('/cambioestatus', 'OrdendecompraController@changestatus');
        Route::get('/{token}/{folio}', 'OrdendecompraController@buscarOrden');
        Route::get('find/{token}/{folio}', 'OrdendecompraController@show');
        Route::get('/lista', 'OrdendecompraController@listall');
        Route::get('/busqueda/{token}/{proveedor}', 'OrdendecompraController@searchProveedor');
    });

});
