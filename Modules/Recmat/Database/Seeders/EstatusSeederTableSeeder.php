<?php

namespace Modules\Recmat\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\RecmCatEstatus;

class EstatusSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        RecmCatEstatus::create( [
            ['id' => 1, 'estatus' => 'NUEVO', 'color' => 'label-info'],
            ['id' => 2, 'estatus' => 'ENVIADO A MATERIALES', 'color' => 'label-primary'],
            ['id' => 3, 'estatus' => 'RECHAZADO', 'color' => 'label-danger'],
            ['id' => 4, 'estatus' => 'PROCESO DE ENTREGA', 'color' => 'label-warning'],
            ['id' => 5, 'estatus' => 'EN COMPRA', 'color' => 'label-warning'],
            ['id' => 6, 'estatus' => 'ENTREGADO', '018-12-17 16:12:29', '2018-12-17 16:12:29', 'label-success'],
            ['id' => 7, 'estatus' => 'ENVIADO A REVISION', 'color' => 'label-warning'],
            ['id' => 8, 'estatus' => 'RECHAZADO COMPROBACIÓN', 'color' => 'label-danger'],
            ['id' => 9, 'estatus' => 'ENVIADO AFECTACION PRESUPUESTAL', 'color' => 'label-primary'],
            ['id' => 10, 'estatus' => 'SIN COBERTURA', 'color' => 'label-warning'],
            ['id' => 11, 'estatus' => 'ENVIADO A CLC', 'color' => 'label-info'],
            ['id' => 12, 'estatus' => 'PAGO DIRECTO A PROVEEDOR', 'color' => 'label-primary'],
            ['id' => 13, 'estatus' => 'CLC MODO TRADICIONAL', 'color' => 'label-primary'],
            ['id' => 14, 'estatus' => 'ENVIADO A BANCO PARA SPEI', 'color' => ''],
            ['id' => 15, 'estatus' => 'ENVIADO PAGO A FINANZAS', 'color' => ''],
            ['id' => 16, 'estatus' => 'SPEI ENVIADO', 'color' => ''],
            ['id' => 17, 'estatus' => 'REGISTRO EN CONTABILIDA', 'color' => 'label-success']
        ]);
    }
}
