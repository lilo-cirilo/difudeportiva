
  const WarehouseExit = Vue.component('warehouseExit', {
    template: `
      <div>
        <div class="box box-primary">
          <div class="box-header with-border">
              <h3 class="box-title">@{{msg}}</h3>
              
              <div class="box-tools">
                  <button type="button" class="btn btn-primary btn-sm" @click="reiniciar()"><i class="fa fa-send"></i> Reiniciar</button>
              </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="form-group">
              <label for="">Ingresa el folio de la orden de compra</label>
              <v-select :options="ordenes" label="folio" v-model="orden" :filterable="false" @search="buscarOrdenes"></v-select>
            </div>
  
            <form v-if="orden != null && orden.status_id == 4">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre</label>
                  <input type="text" class="form-control" v-model="orden.proveedor.nombre" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">RFC</label>
                  <input type="text" class="form-control" v-model="orden.proveedor.rfc" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Dirección</label>
                  <input type="text" class="form-control" v-model="direccion" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Correo</label>
                  <input type="email" class="form-control" v-model="orden.proveedor.correo" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Telefono</label>
                  <input type="text" class="form-control" v-model="orden.proveedor.telefono" disabled>
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Folio Factura</label>
                  <input type="text" class="form-control" v-model="folio_factura" disabled>
                </div>
              </div>
            </form>
  
            <div class="box" style="margin-top: 10px;" v-if="orden != null && orden.status_id == 4">
              <div class="box-header">
                <h3 class="box-title">Productos en la Orden de Compra</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Producto</th>
                      <th>Presentación</th>
                      <th style="width: 40px">Cantidad</th>
                      <th>Llego ?</th>
                    </tr>
                    <fila v-for="producto in orden.productos" :producto="producto"></fila>
                  </tbody>
                </table>
                <button class="btn btn-primary" @click.prevent="guardarEntrada()">Confirmar</button>
              </div>
              <!-- /.box-body -->
            </div>
            <div v-if="orden.productos.length && orden.status_id != 4">
                <h3 v-if="orden.status_id == 1">No Existe Entrada</h3>
                <h3 v-else>Ya fue generada la salida</h3>
            </div>
          </div>
        </div>
      </div>
    `,
    components: { fila },
    data() {
      return {
        msg: 'Almacen - Salida',
        usuario: @json($usuario),
        ordenes: [],
        orden: {productos:[]},
        folio_factura: null,
      }
    },
    computed: {
      direccion(){
        return this.orden != null ? this.orden.proveedor.calle + ' ' + this.orden.proveedor.numero + ' ' + this.orden.proveedor.colonia : '';
      }
    },
    mounted(){
      axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
      jsreport.serverUrl = '{{config('app.reporter_url')}}';
    },
    methods: {
      buscarOrdenes(search, loading){
        loading(true);
        this.peticionRequisicion(loading, search, this);
      },
      peticionRequisicion: _.debounce((loading, search, vm) => {
        fetch(`/recmat/ordendecompra/${localStorage.getItem('recm_token')}/${escape(search)}`)
          .then(res => {
            res.json()
              .then(json => {
                  vm.ordenes = json;
              });
            loading(false);
          });
        }, 350),
      countItemsQ(products){
        let total = 0;
        products.forEach(item => {
            total += parseInt(item.quantity);
        });
        return total;
      },
      guardarEntrada(){
        vm = this;
        {{-- axios.post('/recmat/almacen/entradas', { --}}
        axios.post(`/recmat/almacen/salidas/requisicion/${this.orden.requisicion.id}`, this.orden).then(response => {
            if(response.data.success){
                swal('Great !', 'Entrada guardada correctamente', 'success');
                {{-- this.entries.push(response.data); --}}
                var request = {
                  template:{
                    shortid: 'SJjE44sBE'
                  },
                  header: {
                    Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                  },
                  data:{
                    area: {
                      nombre: response.data.recibe.empleado.area.nombre,
                      solicitante: `${response.data.recibe.empleado.persona.nombre} ${response.data.recibe.empleado.persona.primer_apellido} ${response.data.recibe.empleado.persona.segundo_apellido}`
                    },
                    almacen: {
                        folio_almacen: response.data.salida.folio,
                        folio_requisicion: this.orden.requisicion.folio
                    },
                    productos: this.orden.productos,
                    entrego: 'GENARO ZARAGOZA PEREZ',
                    recibio: `${response.data.recibe.empleado.persona.nombre} ${response.data.recibe.empleado.persona.primer_apellido} ${response.data.recibe.empleado.persona.segundo_apellido}`
                  }
                };
                //display report in the new tab
                jsreport.render('_blank', request);
                this.orden = null;
            }
        }).catch(error => {
            console.log(error);
            swal('Oops', `Algo salio mal. ${error}`, 'error');
        });
        console.log('Form submitted');
        $('#addEntry').modal('hide');
        //this.nueva_entrada.entry.description = null;
        //this.nueva_entrada.entry.products = [];    
    },
    }
  });