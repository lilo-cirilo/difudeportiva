const Reimprimir = Vue.component('reimprimir', {
    template: `
    <div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Entradas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <v-select :options="entradas" label="folio" v-model="entrada" :filterable="false" @search="buscarEntrada"></v-select>
                <div class="row">
                    <div class="col-sm-12">
                        <form>
                            <div class="form-group">
                                <label for="">Folio:</label>
                                <input type="text" class="form-control" readonly v-model="entrada.folio">
                            </div>
                            <div class="form-group">
                                <label for="">Nombre:</label>
                                <input type="text" class="form-control" readonly v-model="entrada.nombre">
                            </div>
                            <div class="form-group">
                                <label for="">Fecha:</label>
                                <input type="text" class="form-control" readonly v-model="entrada.fecha_hora">
                            </div>
                            <div class="form-group">
                                <label for="">Proveedor:</label>
                                <input type="text" class="form-control" readonly v-model="entrada.proveedor.nombre">
                            </div>
                            <button class="btn btn-primary" @click.prevent="imprimirEntrada()">Imprimir</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Salidas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <v-select :options="salidas" label="folio" v-model="salida" :filterable="false" @search="buscarSalida"></v-select>
                <div class="row">
                    <div class="col-sm-12">
                        <form>
                            <div class="form-group">
                                <label for="">Folio:</label>
                                <input type="text" class="form-control" readonly v-model="salida.folio">
                            </div>
                            <div class="form-group">
                                <label for="">Nombre:</label>
                                <input type="text" class="form-control" readonly v-model="salida.recibe.usuario">
                            </div>
                            <div class="form-group">
                                <label for="">Fecha:</label>
                                <input type="text" class="form-control" readonly v-model="salida.fecha_hora">
                            </div>
                            <button class="btn btn-primary" @click.prevent="imprimirEntrada()">Imprimir</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>`,
    data(){
        return {
            usuario: @json($usuario),
            entradas: [],
            entrada: {
                folio: '',
                nombre: '',
                fecha_hora: '',
                proveedor: {
                    nombre: ''
                }
            },
            salidas: [],
            salida: {
                folio: '',
                fecha_hora: '',
                recibe: {
                    usuario: ''
                }
            }
        }
    },
    mounted(){
        jsreport.serverUrl = '{{env('REPORTER_URL', 'http://192.168.0.149:5488')}}';
    },
    methods: {
        buscarEntrada(search, loading){
            loading(true);
            this.buscarEntradas(loading, search, this);
        },
        buscarEntradas: _.debounce((loading, search, vm) => {
            fetch(`/recmat/almacen/entradas/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.entradas = json;
                        });
                    loading(false);
                });
            }, 350),
        imprimirEntrada(){
            var request = {
                template:{
                    shortid: 'r172YwWZ4'
                },
                header: {
                    Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                },
                data:{
                    descripcion: this.entrada.nombre,
                    folio: this.entrada.folio,
                    fecha_hora: this.entrada.fecha_hora,
                    proveedor: {
                        nombre: this.entrada.proveedor.nombre,
                        rfc: this.entrada.proveedor.rfc,
                        direccion: `${this.entrada.proveedor.calle} ${this.entrada.proveedor.numero} ${this.entrada.proveedor.colonia}`
                    },
                    productos: this.entrada.productos.map(item => {
                        return { id: item.id, quantity: item.pivot.cantidad, productoNombre: `${item.producto.producto} - ${item.presentacion.presentacion}`, precio: item.precio}
                    }),
                    nombre: `${this.entrada.usuario.persona.nombre} ${this.entrada.usuario.persona.primer_apellido} ${this.entrada.usuario.persona.segundo_apellido}`,
                    vistobueno: 'MARCELINO SANTOS ROJAS'
                }
            };
            //display report in the new tab
            jsreport.render('_blank', request);
        },
        buscarSalida(search, loading){
            loading(true);
            this.buscarSalidas(loading, search, this);
        },
        buscarSalidas: _.debounce((loading, search, vm) => {
            fetch(`/recmat/almacen/salidas/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.salidas = json;
                        });
                    loading(false);
                });
            }, 350),
    }
});