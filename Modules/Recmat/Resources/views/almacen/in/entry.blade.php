
const WarehouseEntry = Vue.component('warehouseEntry', {
    template: `
    <div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">@{{msg}}</h3>
                
                <div class="box-tools">
                    <button type="button" class="btn btn-primary btn-sm" @click="showNewentryModel()"><i class="fa fa-plus"></i> Agregar</button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <td><strong>#</strong></td>
                                <td><strong>Fecha</strong></td>
                                <td><strong>Descripción</strong></td>
                                <td><strong>Total Articulos</strong></td>
                                <td><strong>Ver</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="entry in entries">
                                <td>@{{entry.id}}</td>
                                <td>@{{entry.fecha_hora}}</td>
                                <td>@{{entry.nombre}}</td>
                                <td>@{{entry.totalArticulos}}</td>
                                <td><button class="btn btn-primary btn-sm" @click="showModal(entry)"><i class="fa fa-eye"></i></button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Limpiar</button>
            </div>
        </div>
        <!-- /.box -->
        
        <!-- Modal -->
        <div class="modal modal-primary fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detalle de los productos en esta entrada</h4>
                        <p>Entrada: <strong>@{{detalle_entrada.entrada}}</strong><br>Fecha: <strong>@{{detalle_entrada.fecha}}</strong></p>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover text-primary">
                                <thead>
                                    <tr>
                                        <td><strong>#</strong></td>
                                        <td><strong>Producto</strong></td>
                                        <td><strong>Presentacion</strong></td>
                                        <td><strong>Cantidad</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="prod in detalle_entrada.productos" v-if="detalle_entrada.productos.length > 0">
                                        <td>@{{prod.id}}</td>
                                        <td>@{{prod.producto.producto}}</td>
                                        <td>@{{prod.presentacion.presentacion}}</td>
                                        <td>@{{prod.pivot.cantidad}}</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Articulos</strong></td>
                                        <td><strong>@{{countItems(detalle_entrada.productos)}}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal modal-primary fade" id="addEntry" tabindex="-1" role="dialog" aria-labelledby="addEntryLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar nueva entrada de productos</h4>
                    </div>
                    <form @submit.prevent="saveEntry()">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="description" class="text-primary">Descripción</label>
                                <input type="text" class="form-control" name="description" placeholder="Identificar entrada" v-model="nueva_entrada.entry.description" v-validate="'required'">
                            </div>
                            <div class="form-group">
                                <label for="dateEntry" class="text-primary">Fecha</label>
                                <input type="datetime" class="form-control" id="dateEntry" placeholder="dd/mm/aaaa" v-model="nueva_entrada.entry.date">
                            </div>
                            <div class="form-group" v-show="! providerNotFound">
                                <label for="exampleInputFile" class="text-primary">Proveedor</label>
                                <v-select :options="providers" label="nombre" @search="findProvider" :filterable='false' v-model="nueva_entrada.providerFound"></v-select>
                            </div>
                            <div class="checkbox">
                                <label class="text-primary">
                                    <input type="checkbox" v-model="providerNotFound"> Nuevo Proveedor
                                </label>
                            </div>
                            <div class="form-group" v-show="providerNotFound">
                                <label for="" class="text-primary">Nombre</label>
                                <input type="text" class="form-control" placeholder="Nombre de la empresa o persona fisica" v-model="nueva_entrada.provider.name">
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">RFC</label>
                                        <input type="text" class="form-control" placeholder="RFC" v-model="nueva_entrada.provider.rfc">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">Calle</label>
                                        <input type="text" class="form-control" placeholder="Calle" v-model="nueva_entrada.provider.street">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">Num Exterior</label>
                                        <input type="text" class="form-control" v-model="nueva_entrada.provider.ext">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">Num Interior</label>
                                        <input type="text" class="form-control" v-model="nueva_entrada.provider.int">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">Colonia</label>
                                        <input type="text" class="form-control" v-model="nueva_entrada.provider.suburb">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">CP</label>
                                        <input type="number" class="form-control" v-model="nueva_entrada.provider.zipcode">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group" v-show="providerNotFound">
                                        <label for="" class="text-primary">Municipio</label>
                                        <v-select :options="municipios" label="nombre" v-model="nueva_entrada.provider.township" @search="buscarMunicipio"></v-select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 1em;">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-info" v-show="providerNotFound" :disabled="! providerNotFound || nueva_entrada.provider.isSaving">
                                            <i class="fa fa-save" v-show="! nueva_entrada.provider.isSaving"></i>
                                            <span v-show="! nueva_entrada.provider.isSaving">Guardar Proveedor</span>
                                            <i class="fa fa-refresh fa-spin fa-fw" v-show="nueva_entrada.provider.isSaving"></i>
                                            <span v-show="nueva_entrada.provider.isSaving">Guardando ...</span>
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="" class="text-primary">Buscar producto</label>
                                        <a href="/recmat/almacen#/productos" target="_blank">No se encuentra el producto ? </a>
                                        <v-select :options="products" label="productoNombre" v-model="product" @search="findProduct" :filterable='false'></v-select>
                                    </div>
                                </div>

                                <div class="col-sm-3 col-md-3"> 
                                    <div class="form-group">
                                        <label for="" class="text-primary">Cantidad</label>
                                        <input type="number" class="form-control" min="0" v-model="nueva_entrada.quantity">
                                    </div>                            
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <div class="form-group">
                                        <label for="" class="text-primary">Agregar</label>
                                        <button class="form-control btn btn-success" @click.prevent="addToNewProductsTable(product, nueva_entrada.quantity)"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive" style="margin-top: 1em;">
                                <table class="table table-bordered table-hover text-primary">
                                    <thead>
                                        <tr>
                                            <td><strong>#</strong></td>
                                            <td><strong>Producto</strong></td>
                                            <td><strong>Cantidad (pzs)</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="producto in nueva_entrada.products">
                                            <td>@{{producto.id}}</td>
                                            <td>@{{producto.productoNombre}}</td>
                                            <td>@{{producto.quantity}}</td>
                                        </tr>
                                        <!-- <tr>
                                            <td></td>
                                            <td>Total</td>
                                            <td>@{{totalNewProducts}}</td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    `,
    data(){
        return {
            msg: 'Almacen General - Entradas',
            usuario: @json($usuario),
            entries: [
                {id: 1, fecha: '2018-11-20', nombre: 'Primera entrada', totalArticulos: 100},
                {id: 2, fecha: '2018-11-21', nombre: 'Segunda entrada', totalArticulos: 102},
                {id: 3, fecha: '2018-11-22', nombre: 'Tercera entrada', totalArticulos: 103}
            ],
            detalle_entrada: {
                entrada: '',
                fecha: '',
                productos: [
                    {id: 1, producto: 'Lapiz Caja 10pz', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 2, producto: 'Lapicero Negro Caja 10pz', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 3, producto: 'Lapicero Azul Caja 10pz', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 4, producto: 'Goma Caja 10pz', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 5, producto: 'Hojas blancas Caja 20pz', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 6, producto: 'Toner HP', cantidad: 10, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                    {id: 7, producto: 'Mouse Optico HP', cantidad: 1, presentacion: { presentacion: 'presentacion'}, pivot: { cantidad: 1}},
                ]
            },
            providers: [],
            providerNotFound: false,
            products: [],
            municipios: [],
            nueva_entrada: {
                entry: {
                    description: null,
                    date: moment().format('YYYY-MM-DD HH:mm:ss')
                },
                providerFound: null,
                provider: {
                    name: '',
                    rfc: '',
                    street: '',
                    ext: '',
                    int: '',
                    suburb: '',
                    zipcode: 0,
                    township: '',
                    isSaving: false
                },
                quantity: 0,
                products: []
            },
            product: null,
            presentation: null
        }
    },
    watch: {
        product(val){
            console.log(val);
            this.product_presentation = val.presentaciones;
        }
    },
    computed: {
        /*totalNewProducts(){
            let total = 0;
            this.nueva_entrada.products.forEach(item => {
                total += parseInt(item.quantity);
            });
            return total;
        }*/
    },
    mounted(){
        axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
        jsreport.serverUrl = '{{config('app.reporter_url')}}';
        axios.get('/recmat/almacen/entradas').then(response => {
            this.entries = response.data;
            for(let i = 0; i < this.entries.length; i++){
                this.entries[i].totalArticulos = 0;
                for(let j = 0; j < this.entries[i].productos.length; j++){
                    this.entries[i].totalArticulos += this.entries[i].productos[j].pivot.cantidad;
                }
            }
        }).catch(error => { console.log(error) });
    },
    methods: {
        showModal(detail){
            this.detalle_entrada.entrada = detail.nombre;
            this.detalle_entrada.fecha = detail.fecha_hora;
            this.detalle_entrada.productos = detail.productos;
            $('#myModal').modal('show');
        },
        showNewentryModel(){
            $('#addEntry').modal('show');
        },
        countItems(products){
            let total = 0;
            products.forEach(item => {
                total += item.pivot.cantidad;
            });
            return total;
        },
        countItemsQ(products){
            let total = 0;
            products.forEach(item => {
                total += parseInt(item.quantity);
            });
            return total;
        },
        findIfExist(product, quantity, vueInstance){
            return new Promise((resolve, reject) => {
                this.nueva_entrada.products.forEach((item, index) => {
                    console.log(item);
                    console.log(product);
                    if(item.id === product.id){
                        console.log({result: 'No existe'});
                        resolve({result: 'Existe', index: index, vueInstance: vueInstance});
                    }
                });
                console.log({result: 'No existe'});
                resolve({result: 'No existe'});
            });
        },
        addToNewProductsTable(product, quantity){
            console.log(product);
            if(quantity <= 0){
                return;
            }
            this.findIfExist(product, quantity, this)
                .then(result => {
                    if(result.result == 'No existe'){
                        product.quantity = quantity;
                        product.producto = this.product.producto + ' - ' + product.presentacion;
                        this.nueva_entrada.products.push(product);
                        this.nueva_entrada.quantity = 0;
                        this.nueva_entrada.product = null;
                    }else{
                        this.nueva_entrada.products[result.index].quantity = parseInt(this.nueva_entrada.products[result.index].quantity) + parseInt(quantity);
                        this.nueva_entrada.quantity = 0;
                        this.nueva_entrada.product = null;
                    }
                })
                .catch(error => {
                    console.log('error');
                    console.log(error);
                });
        },
        saveEntry(){
            vm = this;
            axios.post('/recmat/almacen/entradas', {
                fecha: this.nueva_entrada.entry.date, 
                descripcion: this.nueva_entrada.entry.description,
                proveedor: this.nueva_entrada.providerFound != null ? this.nueva_entrada.providerFound : this.nueva_entrada.provider,
                totalArticulos: this.countItemsQ(this.nueva_entrada.products),
                productos: this.nueva_entrada.products,
                usuario_id: this.usuario.id
            }).then(response => {
                if(response.data.id){
                    swal('Great !', 'Entrada guardada correctamente', 'success');
                    this.entries.push(response.data);
                    var request = {
                        template:{
                            shortid: 'SyNiXNorV'
                        },
                        header: {
                            Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                        },
                        data:{
                            descripcion: this.nueva_entrada.entry.description,
                            {{-- folio: `EA - ${response.data.id} - ${moment().format('YYYY')}`, --}}
                            folio: response.data.folio,
                            fecha_hora: this.nueva_entrada.entry.date,
                            proveedor: {
                                nombre: this.nueva_entrada.providerFound != null ? this.nueva_entrada.providerFound.nombre : this.nueva_entrada.provider.name,
                                rfc: this.nueva_entrada.providerFound != null ? this.nueva_entrada.providerFound.rfc : this.nueva_entrada.provider.rfc,
                                direccion: this.nueva_entrada.providerFound != null ? `${this.nueva_entrada.providerFound.calle} ${this.nueva_entrada.providerFound.numero} ${this.nueva_entrada.providerFound.colonia}` : `${this.nueva_entrada.provider.street} ${this.nueva_entrada.provider.ext} ${this.nueva_entrada.provider.suburb}`
                            },
                            productos: this.nueva_entrada.products,
                            nombre: `${this.usuario.persona.nombre} ${this.usuario.persona.primer_apellido} ${this.usuario.persona.segundo_apellido}`,
                            vistobueno: 'MARCELINO SANTOS ROJAS'
                        }
                    };
                    //display report in the new tab
                    jsreport.render('_blank', request);
                }
            }).catch(error => {
                console.log(error);
                swal('Oops', `Algo salio mal. ${error}`, 'error');
            });
            console.log('Form submitted');
            $('#addEntry').modal('hide');
            //this.nueva_entrada.entry.description = null;
            //this.nueva_entrada.entry.products = [];    
        },
        showAddProduct(){
            $('#addProduct').modal('show');
        },
        findProvider(search, loading) {
            loading(true);
            this.search(loading, search, this);
        },
        search: _.debounce((loading, search, vm) => {
            fetch(`/recmat/proveedores/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.providers = json;
                        });
                    loading(false);
                });
        }, 350),
        findProduct(search, loading) {
            loading(true);
            this.searchP(loading, search, this);
        },
        searchP: _.debounce((loading, search, vm) => {
            fetch(`/recmat/productos/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.products = json.map(item => {
                                item.productoNombre = item.producto.producto + ' - ' + item.presentacion.presentacion;
                                return item;
                            });
                        });
                    loading(false);
                });
        }, 350),
        changedLabel: function(label) {
            console.log(label);
        },
        buscarMunicipio(search, loading) {
            loading(true);
            this.buscarMunicipioV(loading, search, this);
        },
        buscarMunicipioV: _.debounce((loading, search, vm) => {
            fetch(`/municipios/buscar/${localStorage.getItem('recm_token')}/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.municipios = json;
                        });
                    loading(false);
                });
        }, 350)
    }
});
