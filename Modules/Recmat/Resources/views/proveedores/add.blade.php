const AgregarProveedor = Vue.component('agregarProveedor', {
  template: `
    <div>
      <form action="#" @submit.prevent="guardar">
        <div class="row">
          <div class="col-sm-12 col-md-8">
            <div class="form-group">
              <label for="" class="text-primary">Nombre</label>
              <input type="text" class="form-control" placeholder="Nombre de la empresa o persona fisica" v-model="proveedor.nombre" v-validate.disable="'required'" name="nombre">
              <span>@{{ errors.first('nombre') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
              <label for="" class="text-primary">No. Inscripción</label>
              <input type="text" class="form-control" placeholder="Número de inscripción ante gobierno" v-model="proveedor.inscripcion" v-validate.disable="'required'" name="inscripcion">
              <span>@{{ errors.first('inscripcion') }}</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="" class="text-primary">RFC</label>
                <input type="text" class="form-control" placeholder="RFC" v-model="proveedor.rfc" v-validate.disable="'required'" name="rfc">
                <span>@{{ errors.first('rfc') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
              <div class="form-group">
                  <label for="" class="text-primary">Calle</label>
                  <input type="text" class="form-control" placeholder="Calle" v-model="proveedor.calle" v-validate.disable="'required'" name="calle">
                  <span>@{{ errors.first('calle') }}</span>
              </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="" class="text-primary">Número</label>
                <input type="text" class="form-control" v-model="proveedor.ext" v-validate.disable="'required'" name="ext">
                <span>@{{ errors.first('ext') }}</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label for="" class="text-primary">Colonia</label>
                <input type="text" class="form-control" v-model="proveedor.colonia" v-validate.disable="'required'" name="colonia">
                <span>@{{ errors.first('colonia') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
                <label for="" class="text-primary">CP</label>
                <input type="number" class="form-control" v-model="proveedor.codigopostal" v-validate.disable="'required|digits:5'" name="codigopostal">
                <span>@{{ errors.first('codigopostal') }}</span>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
                <label for="" class="text-primary">Ciudad</label>
                <input type="text" class="form-control" v-model="proveedor.ciudad" v-validate.disable="'required'" name="ciudad">
                <span>@{{ errors.first('ciudad') }}</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="" class="text-primary">Correo</label>
              <input type="email" class="form-control" v-model="proveedor.email" v-validate.disable="'required|email'" name="email">
              <span>@{{ errors.first('email') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-6">
            <div class="form-group">
              <label for="" class="text-primary">Telefono</label>
              <input type="number" class="form-control" v-model="proveedor.telefono" v-validate.disable="'required|digits:10'" name="telefono">
              <span>@{{ errors.first('telefono') }}</span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="" class="text-primary">Banco</label>
                <select class="form-control" v-model="proveedor.banco" v-validate.disable="'required'" name="banco">
                  <option value="BANAMEX">BANAMEX</option>
                  <option value="BANCOMER">BANCOMER</option>
                  <option value="SANTANDER">SANTANDER</option>
                  <option value="BANORTE">BANORTE</option>
                  <option value="SCOTIABANK">SCOTIABANK</option>
                  <option value="INBURSA">INBURSA</option>
                  <option value="HSBC">HSBC</option>
                </select>
                <span>@{{ errors.first('banco') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="" class="text-primary">Num Cuenta</label>
                <input type="text" class="form-control" v-model="proveedor.cuenta" v-validate.disable="'required'" name="cuenta">
                <span>@{{ errors.first('cuenta') }}</span>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="form-group">
                <label for="" class="text-primary">Clabe</label>
                <input type="text" class="form-control" v-model="proveedor.clabe" v-validate.disable="'required'" name="clabe">
                <span>@{{ errors.first('clabe') }}</span>
            </div>
          </div>
        </div>
        <div class="row" style="margin-bottom: 1em;">
          <div class="col-sm-12">
              <div class="pull-right">
                  <button type="submit" class="btn btn-info">
                      <i class="fa fa-save" v-show="! proveedor.guardando"></i>
                      <span v-show="! proveedor.guardando">Guardar Proveedor</span>
                      <i class="fa fa-refresh fa-spin fa-fw" v-show="proveedor.guardando"></i>
                      <span v-show="proveedor.guardando">Guardando ...</span>
                  </button>
              </div>
          </div>
        </div>
      </form>
    </div>`,
  data(){
    return {
      proveedor: {
        guardando: false,
        nombre: null,
        inscripcion: null,
        rfc: null,
        calle: null,
        ext: null,
        colonia: null,
        codigopostal: null,
        ciudad: null,
        telefono: null,
        email: null,
        banco: null,
        cuenta: null,
        clabe: null,
        usuario_id: null
      },
      usuario: @json(auth()->user())
    }
  },
  mounted() {
    this.proveedor.usuario_id = this.usuario.id;
  },
  methods: {
    guardar() {
      this.validar();
    },
    validar() {
      this.$validator.validate().then(valid => {
        if (!valid) {
          // do stuff if not valid.
          return;
        }
        console.log('Valido');
        axios.post('/recmat/proveedores', this.proveedor).then(response => {
          swal('Great !', `Proveedor registrado con el ID ${response.data.id}`, 'success');
        }).catch(error => { swal('Oops !', `Algo salio mal. ${error.response.data.error}`, 'error'); console.log(error); });
      });
    },
    resetear() {
      this.proveedor.nombre = null;
      this.proveedor.inscripcion = null;
      this.proveedor.rfc = null;
      this.proveedor.calle = null;
      this.proveedor.ext = null;
      this.proveedor.colonia = null;
      this.proveedor.codigopostal = null;
      this.proveedor.ciudad = null;
      this.proveedor.telefono = null;
      this.proveedor.email = null;
      this.proveedor.banco = null;
      this.proveedor.cuenta = null;
      this.proveedor.clabe = null;
    }
  }
});