const ListaPartida = Vue.component('listapartida', {
  template: `
  <div>
    @if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
    @endif

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Lista de partidas  <button type="button" class="btn btn-box-tool" @click.prevent="loadPartidas(1), busqueda = ''"><i class="fa fa-refresh"></i></button> </h3>
          <div class="box-tools">
            
            <div class="input-group input-group-sm" style="width: 150px;">
              <input type="text" name="table_search" class="form-control pull-right" placeholder="Buscar" v-model="busqueda">

              <div class="input-group-btn">
                <button type="submit" class="btn btn-default" @click.prevent="searchPartida()"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <th>#</th>
              <th>Nombre</th>
              <th>Tipo</th>
              <th>Descripcion</th>
            </thead>
            <tbody>
              <tr v-for="part in partidas">
                <td>@{{part.partida_especifica_c}}</td>
                <td>@{{part.partida_especifica}}</td>
                <td v-if="part.tipo == 1">Requisicion</td>
                <td v-else>Mantenimiento</td>
                <td><p class="text-justify">@{{part.concepto}}</p></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <nav aria-label="Page navigation example">
          <ul class="pagination">
              <li class="page-item" v-if="pagination.current_page > 1">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
              </li>
              <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
              </li>
              <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                  <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
              </li>
          </ul>
      </nav>
    </div>
  </div>
</div>`,
  data() {
    return {
      partidas: [],
      pagination:{},
      offset: 2,
      busqueda:'',
    }
  },
  mounted(){
    jsreport.serverUrl = '{{env('REPORTER_URL', 'http://192.168.0.10:3001')}}';
    axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    this.loadPartidas(1);
  },
  computed: { 
    isActive(){
        return this.pagination.current_page;
    },
    pageNumber(){
        if(!this.pagination.to){
            return [];
        }
        var desde = this.pagination.current_page - this.offset;
        if(desde < 1){
            desde = 1;
        }
        var hasta = desde + (this.offset * 2);
        if(hasta >= this.pagination.last_page) {
            hasta = this.pagination.last_page;
        }
        var pagesArray = [];
        while(desde <= hasta) {
            pagesArray.push(desde);
            desde++;
        }
        return pagesArray;
    }
  },
  methods:{
    loadPartidas(page){
      let me = this;
      block();
      var url = "/recmat/partida/partidaList?page="+page;
      axios.get(url).then(function (response) {
        unblock();
        me.partidas = response.data.data
        me.pagination = response.data;
      }).catch(function (error){
        unblock();
        console.log("Error "+error)
      })
    },
    cambiarPagina(page){
      this.loadPartidas(page);
    },
    searchPartida(){
      let me = this;
      if(me.busqueda == '')
        return swal({type: 'error', title:' Error', text: 'Campo de busqueda vacio'})
        
      block();
      var url = "/recmat/admin/partidas/"+localStorage.getItem('recm_token')+"/"+me.busqueda;
      axios.get(url).then(function (response) {
        unblock();
        if(response.data.length)
        {
          me.partidas = response.data
          me.pagination = {};
        }
        else
          swal({type: 'info', title:' Sin resultados', text: 'Sin resultados'})
        
      }).catch(function (error){
        unblock();
        console.log("Error "+error)
      })
    }
  }
});

