<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Entrada de Almacen</title>
    <style>
        @font-face {
            font-family: CenturyGothic;
            src: url('{{asset("/fonts/CenturyGothic.ttf")}}');
        }
        @font-face {
            font-family: TodaySHOP;
            src: url('{{assets("/fonts/today-shop/TodaySHOP-Regular.otf")}}');
            font-weight: normal;
        }
            
            @font-face {
            font-family: TodaySHOP;
            src: url('{{assets("/fonts/today-shop/TodaySHOP-Bold.otf")}}');           
            font-weight: bold;
        }
            
            @font-face {
            font-family: TodaySHOP;
            src: url('{{assets("/fonts/today-shop/TodaySHOP-Medium.otf")}}');
            font-weight: 500;
        }
            
            @font-face {
            font-family: TodaySHOP;
            src: url('{{assets("/fonts/today-shop/TodaySHOP-Light.otf")}}')            
            font-weight: 100;
        }
        body{
            font-family: CenturyGothic;
        }
        h1{
            display: inline-block;
        }
        .img-fluid{
            width: 300px;
            margin-left: 136px;
        }
        .provider{
            width: 500px;
        }
        .title, .value{
            display: inline-block;
            margin: 0;
            padding: 0;
            outline: 1px solid #AE75B1;
            word-wrap: break-word;
            font-size: 12px;
        }
        .title{
            width: 60px;
            padding-left: 5px;
        }
    </style>
  </head>
  <body>
    <div class="container" style="border: 1px solid #000">
        <div>
            <h1>Entrada de Almacen</h1>
            <img class="img-fluid" src="{{asset('/images/content.png')}}" alt="">
        </div>
        <div class="provider">
            <div class="row">
                <table class="dataProvider">
                    <tr>
                        <td>Nombre:</td>
                        <td>Luis Alberto Vasquez</td>
                    </tr>
                    <tr>
                        <td>RFC:</td>
                        <td>VAPL9209223N9</td>
                    </tr>
                    <tr>
                        <td>Calle:</td>
                        <td>VAPL9209223N9</td>
                        <td>Num Ext:</td>
                        <td>114</td>
                        <td>Num Int:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Colonia:</td>
                        <td>Miguel Aleman Valdez</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td>Folio:</td>
                        <td>1 / 2018</td>
                    </tr>
                    <tr>
                        <td>Fecha:</td>
                        <td>28/Nov/2018</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

  </body>
</html>