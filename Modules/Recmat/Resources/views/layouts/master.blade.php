@extends('vendor.admin-lte.layouts.main')

@if (auth()->check())
<!-- @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia())) -->
<!-- @section('user-name', auth()->user()->persona->nombre) -->
@section('user-job')
@section('user-profile', route('users.edit', auth()->user()->id))
<!-- @section('user-log', auth()->user()->created_at) -->
@endif

@push('head')
<link rel="stylesheet" href="/css/vue-multiselect.min.css">
<link rel="stylesheet" href="/css/vue-multiselect2.min.css">


@yield('mystyles')
@endpush

@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="header">NAVEGACIÓN</li>
  <li class="active">
    <a href="{{ route('home') }}">
      <i class="fa fa-home"></i>
      <span>Home</span>
    </a>
  </li>
    
  
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>Requisiciones</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','AREACAPTURISTA'], 'recmat'))
        <li><a href="/recmat/admin#/requisiciones"> <i class="fa fa-newspaper-o"></i> Requisicion</a></li>
      @endif
      <li><a href="/recmat/admin#/requisiciones/lista"> <i class="fa fa-database"></i> Consulta</a></li>
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','RMATERIALES'], 'recmat'))
        <li><a href="/recmat/admin#/requisiciones/listaOD"> <i class="fa fa-database"></i> Lista de requisiciones</a></li>
      @endif
      @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','USUARIORM','RMATERIALES','MANTENIMIENTO'], 'recmat'))
      {{-- <li><a href="/recmat/admin#/requisiciones"> <i class="fa fa-newspaper-o"></i> Servicio</a></li> --}}
      {{-- <li><a href="/recmat/admin#/requisiciones"> <i class="fa fa-newspaper-o"></i> Mantenimiento</a></li> --}}
      {{-- <li><a href="/recmat/admin#/requisiciones/lista"> <i class="fa fa-database"></i> Consulta</a></li> --}}
      <li><a href="/recmat/admin#/ordendecompraprint"> <i class="fa fa-print"></i> Orden de compra</a></li>
      <li><a href="/recmat/admin#/partidas/lista"> <i class="fa fa-list"></i> Lista Partidas</a></li>
      @endif
    </ul>
  </li>
  
  @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','USUARIORM','RMATERIALES','MANTENIMIENTO'], 'recmat'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>Productos</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/recmat/admin#/productos/crear">Agregar Producto</a></li>
      <li><a href="/recmat/admin#/productos">Listado</a></li>
    </ul>
  </li>
  
  @endif
 
  @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','ALMACEN'], 'recmat'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-archive"></i> <span>Almacen</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/recmat/almacen#/entrada"><i class="fa fa-caret-square-o-right"></i> Entradas</a></li>
      <li><a href="/recmat/almacen#/salida"><i class="fa fa-caret-square-o-left"></i> Salidas</a></li>
      {{-- <li><a href="/recmat/almacen#/reimpresion"><i class="fa fa-caret-square-o-left"></i> Reimpresion</a></li> --}}
      <li><a href="/recmat/almacen#/listado"><i class="fa fa-list"></i> Listado Entradas</a></li>
      <li><a href="/recmat/almacen#/listadoOut"><i class="fa fa-list"></i> Listado Salidas</a></li>
    </ul>
  </li>
  @endif
  @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','USUARIORM','RMATERIALES','MANTENIMIENTO'], 'recmat'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-archive"></i> <span>Proveedores</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/recmat/proveedores#/add"><i class="fa fa-caret-square-o-right"></i> Agregar</a></li>
    </ul>
  </li>
  @endif
  @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','MANTENIMIENTO'], 'recmat'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-archive"></i> <span>Mantenimiento</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/recmat/admin#/ordenmantenimiento/lista"><i class="fa fa-list"></i> Lista</a></li>
    </ul>
  </li>
  @endif
  @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','FINANCIERO'], 'recmat'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i> <span>Orden de compra</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="/recmat/admin#/ordendecompra"> <i class="fa fa-ticket"></i> Consulta</a></li>
    </ul>
  </li>
  @endif
  
    {{-- <li class="treeview">
      <a href="#">
        <i class="fa fa-gear"></i> <span>Otros</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('modules.access.create') }}"><i class="fa fa-circle-o"></i> Solicitar Acceso a Módulo</a></li>
          <li><a href="{{ route('modules.access.authorized.index') }}"><i class="fa fa-circle-o"></i> Autorizar Acceso Módulo</a></li>
        <li><a href=""></a></li>
      </ul>
    </li> --}}
</ul>
@endsection

@section('message-url-all', '#')
@section('message-all', 'Ver todos los mensajes')
@section('message-number')
@section('message-text', 'Tienes 1 mensages')
@section('message-list')
<!-- start message -->
<li>
  <a href="#">
      <div class="pull-left">
          <!-- User Image -->
          <img src="https://www.gravatar.com/avatar/?d=mm" class="img-circle" alt="User Image">
      </div>
      <!-- Message title and timestamp -->
      <h4>
          Support Team
          <small><i class="fa fa-clock-o"></i> 5 mins</small>
      </h4>
      <!-- The message -->
      <p>Why not buy a new awesome theme?</p>
  </a>
</li>
<!-- end message -->
@endsection

@section('notification-id', 'notificaciones')
@section('notification-link', 'notificaciones-link')
@section('notification-dropdown-ul', 'notificaciones-ul')
@section('notification-number')
{{-- @section('notification-text', 'Hola mundo') --}}
@section('notification-dropdown-id', 'notificaciones-titulo')
{{-- @section('notification-list')
<a href="/recmat">
    <i class="fa fa-refresh text-yellow" id="reinicio"></i> Recargue la pagina
</a>
@endsection --}}
@section('notification-dropdown-li-id', 'notificaciones-lista')

@section('task-number')
@section('task-text', 'Tienes 1 tarea')
@section('task-url-all')

@endsection
@section('task-list')
<li>
  <a href="#">
      <!-- Task title and progress text -->
      <h3>
          Design some buttons
          <small class="pull-right">20%</small>
      </h3>
      <!-- The progress bar -->
      <div class="progress xs">
          <!-- Change the css width attribute to simulate progress -->
          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              <span class="sr-only">20% Complete</span>
          </div>
      </div>
  </a>
</li>
@endsection

@push('body')
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/vue-router.js') }}"></script>
<script src="{{ asset('js/vue-select.js') }}"></script>
<script src="{{ asset('js/vue-multiselect.min.js') }}"></script>
<script src="{{ asset('js/vee-validate.js') }}"></script>
<script src="{{ asset('js/moment.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.all.js') }}"></script>
<script src="{{ asset('js/jsreport.js') }}"></script>
<script src="{{ asset('js/numero_letras.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.9/push.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/push.js/1.0.9/serviceWorker.min.js"></script>
<script src="{{ asset('js/socket.io.js') }}"></script>
<script src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="{{ asset('plugins/loaders/block.js') }}"></script>
<script src="{{ asset('js/vue-multiselect2.min.js') }}"></script>
<script>
  Push.Permission.GRANTED; // 'granted'
  // Push.Permission.get();
  Push.Permission.request(onGranted, onDenied);
  function onGranted(){
    Push.create("Hello world!", {
      body: "How's it hangin'?",
      icon: '/icon.png',
      timeout: 4000,
      onClick: function () {
          window.focus();
          this.close();
      }
    });
  }
  function onDenied(){
    console.log(':(');
  }
  var socket;

  
	function block() {
		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff',
			},
			baseZ: 10000,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
		});

		function unblock_error() {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}
	}

	function unblock() {
		$.unblockUI();
	}	

  // socket = io.connect('http://192.168.0.149:8081', { 'forceNew': true });
  // socket.on('bievenida', function(data) {
  //     console.log(data);
  //   Push.create("Hello world!", {
  //   body: data.msg,
  //   icon: '/icon.png',
  //   timeout: 4000,
  //   onClick: function () {
  //       window.focus();
  //       this.close();
  //   }
  // });

  // });
</script>
@yield('cdn-scripts')
@yield('other-scripts')

@endpush