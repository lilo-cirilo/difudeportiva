const NewOrdenCompra = Vue.component('newordencompra', {
  template: `
    <div>
      {{-- <div>User @{{ $route.params.id }}</div> --}}
      <form action="" autocomplete="off" data-vv-scope="formordencompra">
        <div class="row">
          <div class="col-md-2 text-center" :class="errors.first('formordencompra.fechaRequisicion') ? 'has-error' : ''">
            <div class="form-group">
              <label for="" class="text-primary">Fecha requisicion</label>
              <input type="text" class="form-control text-center" v-model="requisicion.requisicion.fecha_captura" v-validate="'required:true'" name="fechaRequisicion">
              <span class="text-danger">@{{ errors.first('formordencompra.fechaRequisicion') }}</span>
            </div>
          </div>
          <div class="col-md-2 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Folio</label>
              <input type="text" class="form-control text-center" v-model="requisicion.requisicion.folio" disabled>
              {{-- <p>@{{requisicion.requisicion.folio}}</p> --}}
            </div>
          </div>
          <div class="col-md-8 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Area</label>
              <input type="text" class="form-control text-center" v-model="requisicion.areaResp.area.nombre" disabled>
              {{-- <p>@{{requisicion.areaResp.area.nombre}}</p> --}}
            </div>
          </div>
          <div class="col-md-12"></div>
          <div class="col-md-3 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Cargo</label>
              <p>@{{requisicion.nivel.nivel.nivel}} </p>
            </div>
          </div>
          <div class="col-md-3 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Solicitante</label>
              {{-- <input type="text" class="form-control text-center" v-model="requisicion.solicitante.persona.nombre"> --}}
              <p>@{{requisicion.autoriza.nombre + ' ' + requisicion.autoriza.primer_apellido + ' '+ requisicion.autoriza.segundo_apellido}} </p>
            </div>
          </div>
          <template v-for="res in requisicion.responsable">
            <div class="col-md-3 text-center">
              <div class="form-group">
                <label for="" class="text-primary">Cargo Autoriza</label>
                <p>@{{res.cargo}} </p>
              </div>
            </div>
            <div class="col-md-3 text-center">
              <div class="form-group">
                <label for="" class="text-primary">Autoriza</label>
                {{-- <input type="text" class="form-control text-center" v-model="requisicion.solicitante.persona.nombre"> --}}
                <p>@{{res.responsable_nombre}} </p>
              </div>
            </div>
          </template>
          <div class="col-md-12"></div>
          <div class="col-md-5 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Partida</label>
              {{-- <input type="text" class="form-control text-center" v-model="requisicion.areaResp.area.nombre" disabled> --}}
              <p>@{{requisicion.requisicion.partida.partida_especifica_c + ' - '+ requisicion.requisicion.partida.partida_especifica}} </p>
            </div>
          </div>
          <div class="col-md-7 text-center">
            <div class="form-group">
              <label for="" class="text-primary">Justificación de la compra</label>
              {{-- <input type="text" class="form-control text-center" v-model="requisicion.areaResp.area.nombre" disabled> --}}
              <p>@{{requisicion.requisicion.justificacion}} </p>
            </div>
          </div>
          <div class="col-md-12">
            <h3>Datos del proveedor:</h3>
          </div>
          <div class="col-md-12" :class="errors.first('formordencompra.proveedor') ? 'has-error' : ''">
            <div class="form-group" v-show="! providerNotFound">
              <label class="text-primary">Proveedor</label>
              {{-- <v-select :options="providers" label="nombre" @search="findProvider" :filterable='false' v-model="ordenCompra.providerFound" v-validate="'required:true'" name="proveedor"></v-select>
							<span class="text-danger">@{{ errors.first('formordencompra.proveedor') }}</span> --}}
							<multiselect v-model="ordenCompra.providerFound" :options="providers" label="nombre" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="findProvider" selectLabel="Enter para seleccionar" deselectLabel="Enter para deseleccionar" :showNoOptions="false" placeholder="Selecciona">
								<template slot="noOptions">Comience a escribir para buscar.</template>
								<template slot="noResult"><em style="color:red;">No tenemos registro con lo que estas buscando</em></template>
							</multiselect>
            </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  <p class="text-primary">Nombre : @{{ordenCompra.providerFound.nombre}}</p>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  <p class="text-primary">RFC : @{{ordenCompra.providerFound.rfc}}</p>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  <p class="text-primary">Clabe : @{{ordenCompra.providerFound.clabe}}</p>
              </div>
          </div>
          <div class="col-md-6 text-center" :class="errors.first('formordencompra.fechaOD') ? 'has-error' : ''">
            <div class="form-group">
              <label for="" class="text-primary">Fecha orden de compra</label>
              <input type="text" class="form-control text-center" v-model="ordenCompra.fechaOD" v-validate="{required: true , date_format:'YYYY-MM-DD'}" name="fechaOD">
              <span class="text-danger">@{{ errors.first('formordencompra.fechaOD') }}</span>
            </div>
          </div>
          <div class="col-md-6 text-center" :class="errors.first('formordencompra.entregar') ? 'has-error' : ''">
            <div class="form-group">
              <label for="" class="text-primary">Entregar en:</label>
              <input type="text" class="form-control text-center" v-model="ordenCompra.entregar" v-validate="{required: true}" name="entregar">
              <span class="text-danger">@{{ errors.first('formordencompra.entregar') }}</span>
            </div>
          </div>

          <div class="form-group col-md-12" :class="errors.first('formordencompra.observacionOD') ? 'has-error' : ''" >
              <label for="observacionOD" class="text-primary">Observacion de la orden de compra</label>
              <textarea class="form-control" rows="3" name="observacionOD"  v-validate="'required:true'" v-model="ordenCompra.observacionOD"></textarea>
              <span class="text-danger">@{{ errors.first('formordencompra.observacionOD') }}</span>
          </div>

          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Lista de productos solicitados</h3>
              </div>
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Cantidad</th>
                      <th>Presentación</th>
                      <th>Producto</th>
                      <th>Observación</th>
                      {{-- <th>Precio</th> --}}
                      <th>Opción</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="(prod, index) in requisicion.requisicion.productos">
                      <td :class="prod.color">@{{prod.cantidadT}}</td>
                      <td :class="prod.color">@{{prod.presentacion.presentacion}}</td>
                      <td :class="prod.color">@{{prod.producto.producto}}</td>
                      <td :class="prod.color">@{{prod.pivot.observacion}}</td>
                      {{-- <td :class="prod.color">@{{parseFloat(prod.pivot.precio).toFixed(2)}}</td> --}}
                      <td :class="prod.color"><button class="btn btn-primary btn-sm" @click.prevent="addProducto(prod, index)"><i class="fa fa-plus"></i></button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Lista de productos para compra</h3>
              </div>
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                  <thead>
                    <tr>
                      <th>Cantidad</th>
                      <th>Presentación</th>
                      <th>Producto</th>
                      {{-- <th>Observación</th> --}}
                      <th>Precio</th>
                      <th>IVA</th>
                      <th>Subtotal</th>
                      <th>Total con IVA</th>
                      <th>Opción</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="(compra, index) in ordenCompra.listaProductos">
                      <td>@{{compra.cantidad}}</td>
                      <td>@{{compra.presentacion}}</td>
                      <td>@{{compra.producto}}</td>
                      <td>@{{parseFloat(compra.precioU).toFixed(2)}}</td> 
                      <td>@{{parseFloat(compra.ivaprod).toFixed(2)}}</td> 
                      <td>@{{parseFloat(compra.subtotal).toFixed(2)}}</td> 
                      <td>@{{parseFloat(compra.IVA).toFixed(2)}}</td> 
                      <td><button class="btn btn-primary btn-sm" @click.prevent="deleteProducto(index)"><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="row">
                {{-- <div class="col-md-8"></div> --}}
                <div class="col-md-3" :class="errors.first('formordencompra.subtotal') ? 'has-error' : ''">
                  <div class="form-group">
                      <p class="text-muted">Subtotal <small></small></p>
                      <input type="text" class="form-control" v-model="ordenCompra.subtotalIva" v-validate="{required: true , decimal:2}" name="subtotal" disabled>
                      <span class="text-danger">@{{ errors.first('formordencompra.subtotal') }}</span>
                  </div>
                </div>
                <div class="col-md-3" :class="errors.first('formordencompra.subtotalnoIva') ? 'has-error' : ''">
                  <div class="form-group">
                      <p class="text-muted">Subtotal <small>Sin IVA</small></p>
                      <input type="text" class="form-control" v-model="ordenCompra.subtotalNoIVA" v-validate="{required: true , decimal:2}" name="subtotalnoIva" disabled>
                      <span class="text-danger">@{{ errors.first('formordencompra.subtotalnoIva') }}</span>
                  </div>
                </div>
                
                <div class="col-md-3" :class="errors.first('formordencompra.iva') ? 'has-error' : ''">
                    <div class="form-group">
                        <p class="text-muted">IVA</p>
                        <input type="text" class="form-control" v-model="ordenCompra.totaliva" v-validate="{required: true , numeric:true}" name="iva" disabled>
                        <span class="text-danger">@{{ errors.first('formordencompra.iva') }}</span>
                    </div>
                </div>
                <div class="col-md-3" :class="errors.first('formordencompra.total') ? 'has-error' : ''">
                    <div class="form-group">
                        <p class="text-muted">Total</p>
                        <input type="text" class="form-control" v-model="ordenCompra.total" v-validate="{required: true , decimal:2}" name="total" disabled>
                        <span class="text-danger">@{{ errors.first('formordencompra.total') }}</span>
                    </div>
                </div>
            </div>
          </div>
        </div> {{-- Fin div row --}}
        <button type="button" class="btn btn-primary" @click.prevent="saveOrdenCompra()">Guardar</button>
      </form>
      {{-- Modal --}}
      <div class="modal fade" id="modalAddProduct" style="display: none;">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
              <h4 class="modal-title">Agregar Producto</h4>
            </div>
            <div class="modal-body">
              <form autocomplete="off" data-vv-scope="formproducto">
                <div class="col-md-6 text-center">
                  <div class="form-group">
                    <label for="" class="text-primary">Producto</label>
                    {{-- <input type="text" class="form-control text-center" v-model="productoAdd.nombre" disabled> --}}
                    <p>@{{productoAdd.nombre}}</p>
                  </div>
                </div>
                <div class="col-md-6 text-center">
                  <div class="form-group">
                    <label for="" class="text-primary">Observación</label>
                    {{-- <input type="text" class="form-control text-center" v-model="productoAdd.nombre" disabled> --}}
                    <p>@{{productoAdd.descripcion}}</p>
                  </div>
                </div>
                <div class="col-md-12 text-center" :class="errors.first('formproducto.producto') ? 'has-error' : ''" >
									<label for="producto">PRODUCTO</label>
									<multiselect v-model="productoAdd.producto" :options="products" label="productoNombre" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="findProduct" selectLabel="Enter para seleccionar" deselectLabel="Enter para deseleccionar" :showNoOptions="false" placeholder="Selecciona">
										<template slot="noOptions">Comience a escribir para buscar.</template>
										<template slot="noResult"><em style="color:red;">No tenemos registro con lo que estas buscando</em></template>
									</multiselect>
                  {{-- <v-select :options="products" label="productoNombre" v-model="productoAdd.producto" @search="findProduct" :filterable='false' name="producto" v-validate="'required:true'"></v-select>
                  <span class="text-danger">@{{ errors.first('formproducto.producto') }}</span> --}}
                    {{-- <v-select :options="presentaciones" label="prersentacion" @search="findPresentacion" :filterable='false' v-model="productoAdd.presentacion"></v-select> --}}
                </div>
                <div class="col-md-4 text-center" :class="errors.first('formproducto.cantidad') ? 'has-error' : ''">
                  <div class="form-group">
                    <label for="" class="text-primary">Cantidad</label>
                    <input type="text" class="form-control text-center" v-model="productoAdd.cantidad" v-validate="{required: true , numeric:true}"  name="cantidad">
                    <span class="text-danger">@{{ errors.first('formproducto.cantidad') }}</span>
                  </div>
                </div>
                <div class="col-md-4 text-center" :class="errors.first('formproducto.precio') ? 'has-error' : ''">
                  <div class="form-group">
                    <label for="" class="text-primary">Precio</label>
                    <input type="text" class="form-control text-center" v-model="productoAdd.precioUnitario" v-validate="{required: true , decimal:2}" name="precio">
                    <span class="text-danger">@{{ errors.first('formproducto.precio') }}</span>
                  </div>
                </div>
                <div class="col-md-4 text-center" :class="errors.first('formproducto.subtotal') ? 'has-error' : ''">
                  <div class="form-group">
                    <label for="" class="text-primary">Subtotal</label>
                    <input type="text" class="form-control text-center" v-model="productoAdd.subtotal" v-validate="{required: true , decimal:2}" name="subtotal">
                    <span class="text-danger">@{{ errors.first('formproducto.subtotal') }}</span>
                  </div>
                </div>
                <div class="col-md-12 text-center" :class="errors.first('formproducto.ivacheckbox') ? 'has-error' : ''">
                  <div class="form-group">
                    <label for="" class="text-primary">IVA</label>
                    <input type="checkbox" name="ivacheckbox" checked v-model="productoAdd.ivacheckbox">
                    <span class="text-danger">@{{ errors.first('formproducto.ivacheckbox') }}</span>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary" @click.prevent="addProductoOD()">Agregar</button>
            </div>
          </div>
        </div>
      </div>
        {{-- Modal fin --}}
    </div>`,
    {{-- Fin div principal --}}
    props: ['id'],
  data(){
    return {
      requisicion:{
        requisicion:{partida:{},productos:{}},
        areaResp:{area:{}},
        nivel:{nivel:{}},
        autoriza:{},
      },
      providers: [],
      providerNotFound: false,
      ordenCompra:{
        providerFound:'',
        observacionOD:'',
        listaProductos:[],
        subtotalIva:0,
        totaliva:0,
        subtotalNoIVA:0,
        iva:'16',
        total:0,
        fechaOD:'',
        requisicion_id:'',
        fechaRequisicion:'',
        entregar:''
      },
      productoAdd:{
        nombre:'',
        cantidad:'',
        descripcion:'',
        precioUnitario:'',
        subtotal:'',
        producto:'',
        ivacheckbox:true,
        class:'text-green',
        index:''
      },
      products: [],

    }
  },
  mounted() {
    axios.defaults.headers.common['Recm-Token'] = localStorage.getItem('recm_token');
    this.showRequisiciones()
  },
  methods: {
    showRequisiciones(){
      block();
      let me = this;
      var url = "/recmat/requisicion/"+this.$route.params.id;
      axios.get(url).then(function (response) {
          unblock();
         me.requisicion = response.data
      }).catch(function (error){
          unblock();
          console.log("Error "+error)
      })
    },
    findProvider(search, loading) {
			let me = this;
			if(search.length < 2) return;
			axios.get(`/recmat/proveedores/${escape(search)}`).then(response => { 
				{{-- console.log(response.data) --}}
				me.providers = response.data;
			}).catch(error => { console.log(error) });
		},
		
    addProducto(prod, index){
      let me = this;
      me.productoAdd.nombre = null,
      me.productoAdd.cantidad = null,
      me.productoAdd.descripcion = null,
      me.productoAdd.precioUnitario = null,
      me.productoAdd.subtotal = null,
      me.productoAdd.presentacion = null,
      me.productoAdd.idProducto = null,
      me.productoAdd.producto = '',
      me.productoAdd.index = '',
      me.products = [],
      me.$validator.reset();
      me.productoAdd.nombre = prod.producto.producto
      me.productoAdd.descripcion = prod.pivot.observacion
      me.productoAdd.idProducto = prod.producto.id
      me.productoAdd.index = index
      $('#modalAddProduct').modal('show');      
    },
    
    findProduct(search, loading) {
			let me = this;
			if(search.length < 2) return;
			axios.get(`/recmat/productos/partidaProd/${escape(search)}/${this.requisicion.requisicion.partida.id}`).then(response => { 
				{{-- console.log(response.data) --}}
				me.products = response.data.map(item => {
					item.productoNombre = item.producto.producto + ' - ' + item.presentacion.presentacion;
					return item;
				});
			}).catch(error => { console.log(error) });		
		},
    
    addProductoOD(){
      let me = this;
      this.$validator.validateAll("formproducto").then((result) => {
        if(result) {
          $('#modalAddProduct').modal('hide'); 
          let productoFind = this.ordenCompra.listaProductos;
            productoFind = productoFind.filter(p => {
                return p.producto == me.productoAdd.producto.producto.producto && p.presentacion == me.productoAdd.producto.presentacion.presentacion && p.precioU == me.productoAdd.precioUnitario;
                })
            if (productoFind.length > 0) {
                var preciold = productoFind[0].precio
                productoFind[0].cantidad = parseInt(productoFind[0].cantidad)  + parseInt(me.productoAdd.cantidad)
                productoFind[0].subtotal = parseFloat(parseFloat(productoFind[0].precioU)  * parseInt(productoFind[0].cantidad)).toFixed(2)
                productoFind[0].IVA = parseFloat(parseFloat(parseFloat(productoFind[0].precioU)  * parseInt(productoFind[0].cantidad))*1.16).toFixed(2)
                productoFind[0].ivaprod = (parseFloat(parseFloat(productoFind[0].precioU)  * parseInt(productoFind[0].cantidad))*.16).toFixed(2) 
            }
            else
            {
              me.ordenCompra.listaProductos.push({'presentacion_producto_id':me.productoAdd.producto.id,'producto':me.productoAdd.producto.producto.producto,'cantidad':me.productoAdd.cantidad,'precioU':me.productoAdd.precioUnitario,'subtotal':me.productoAdd.subtotal,'presentacion':me.productoAdd.producto.presentacion.presentacion,'IVA': me.productoAdd.ivacheckbox ? parseFloat(me.productoAdd.subtotal*1.16).toFixed(2) : parseFloat(me.productoAdd.subtotal).toFixed(2), 'ivaprod':me.productoAdd.ivacheckbox ?(me.productoAdd.subtotal*.16):0}); }
          
          me.ordenCompra.subtotalNoIVA = me.productoAdd.ivacheckbox ? parseFloat(me.ordenCompra.subtotalNoIVA).toFixed(2) : parseFloat(parseFloat(me.ordenCompra.subtotalNoIVA) + parseFloat(me.productoAdd.subtotal)).toFixed(2);
          me.ordenCompra.subtotalIva = me.productoAdd.ivacheckbox ? parseFloat(parseFloat(me.ordenCompra.subtotalIva) + parseFloat(me.productoAdd.subtotal)).toFixed(2) : parseFloat(me.ordenCompra.subtotalIva).toFixed(2);
          me.ordenCompra.total = parseFloat(parseFloat(me.ordenCompra.subtotalIva) + (parseFloat(me.ordenCompra.subtotalIva) * (this.ordenCompra.iva/100)) + parseFloat(me.ordenCompra.subtotalNoIVA)).toFixed(2);
          me.ordenCompra.totaliva =  parseFloat(parseFloat(me.ordenCompra.subtotalIva) * (.16)).toFixed(2);
          me.requisicion.requisicion.productos[me.productoAdd.index].color=me.productoAdd.class
          me.productoAdd.nombre = null,
          me.productoAdd.cantidad = null,
          me.productoAdd.descripcion = null,
          me.productoAdd.precioUnitario = null,
          me.productoAdd.subtotal = null,
          me.productoAdd.producto = null,
          me.productoAdd.ivacheckbox = true;
        }
        else
          me.alerta('error','Error','Existen campos vacios');
      });
    },
    alerta(type,msg,text){
      swal({
        type: type,
        title: msg,
        text: text,
      })
    },
    deleteProducto(index){
      let me = this;
      me.ordenCompra.listaProductos.splice(index,1);
      
      const producIva = me.ordenCompra.listaProductos.filter(p => p.IVA > 0).length <= 0 ? 0 : me.ordenCompra.listaProductos.filter(p => p.IVA > 0).map(e => Number.parseInt(e.subtotal)).reduce((valAnt,ValAct) => valAnt + ValAct);  {{-- Lista de productos con iva  --}}
      const producNoIva = me.ordenCompra.listaProductos.filter(p => p.IVA == 0).length <= 0 ? 0 :me.ordenCompra.listaProductos.filter(p => p.IVA == 0).map(e => Number.parseInt(e.subtotal)).reduce((valAnt,ValAct) => valAnt + ValAct); {{-- Lista de productos sin iva --}}
      me.ordenCompra.subtotalIva = producIva.toFixed(2);
      me.ordenCompra.subtotalNoIVA = producNoIva.toFixed(2);
      me.ordenCompra.total = ((producIva + ((producIva) * (this.ordenCompra.iva/100))) + producNoIva).toFixed(2);
      me.ordenCompra.totaliva = ((producIva) * (this.ordenCompra.iva/100)).toFixed(2);
    },
    saveOrdenCompra(){
      let me = this;
      this.$validator.validateAll("formordencompra").then((result) => {
        if(result) {
          if(me.ordenCompra.listaProductos.length)
          {
            me.ordenCompra.requisicion_id = me.requisicion.requisicion.id
            me.ordenCompra.fechaRequisicion = me.requisicion.requisicion.fecha_captura
            block();
            var url = "/recmat/ordendecompra/save";
            axios.post(url, me.ordenCompra).then(function (response) {
              unblock();
              if(response.data.success){
                Swal({type: 'success', title: 'Bien', text: 'Se guardo correctamente!', footer: 'Folio: '+response.data.folio})
                centavos = parseFloat(me.ordenCompra.total).toFixed(2).split(".");
                fecha = me.ordenCompra.fechaOD.split("-")
                {{-- generar pdf de orden de compra --}}
                {{-- jsreport.serverUrl = 'http://192.168.0.149:5488'; --}}
                jsreport.serverUrl = '{{config('app.reporter_url')}}';
                var request = {
                  template:{
                    {{-- shortid: "BkYjV4sBV" ordencompraone --}}
                    shortid: "r1mbRnsuV" 
                  },
                  header: {
                    Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
                  },
                  data:{
                    ordenCompra:{
                      folio: response.data.folio,
                      productos:me.ordenCompra.listaProductos,
                      subtotal:parseFloat(me.ordenCompra.subtotalIva).toFixed(2),
                      totalnoIVA:parseFloat(me.ordenCompra.subtotalNoIVA).toFixed(2),
                      totaIVA:parseFloat(me.ordenCompra.subtotalIva).toFixed(2),
                      total:parseFloat(me.ordenCompra.total).toFixed(2),
                      iva:parseFloat(me.ordenCompra.subtotalIva*(me.ordenCompra.iva/100)).toFixed(2),
                      autoriza:me.requisicion.autoriza.nombre + ' ' + me.requisicion.autoriza.primer_apellido + ' '+ me.requisicion.autoriza.segundo_apellido,
                      area:me.requisicion.areaResp.area.nombre,
                      observacion:me.ordenCompra.observacionOD,
                      importeLetra:numeroALetras(centavos[0], {plural: "PESOS", singular: "PESO", centPlural: "CENTAVOS", centSingular: "CENTAVO"}),
                      centavos:centavos[1]+'/100 M.N.',
                      dia:fecha[2],
                      mes:fecha[1],
                      anio:fecha[0],
                      entregar:me.ordenCompra.entregar
                    },
                    proveedor:{
                        nombre:me.ordenCompra.providerFound.nombre,
                        banco:me.ordenCompra.providerFound.banco,
                        domicilio:me.ordenCompra.providerFound.calle+' '+me.ordenCompra.providerFound.numero+' '+me.ordenCompra.providerFound.colonia,
                        clabe:me.ordenCompra.providerFound.clabe,
                        noInscripcion:me.ordenCompra.providerFound.no_inscripcion
                    }
                  }
                };
                jsreport.render('_blank', request);
                {{-- fin pdf de orden de compra --}}
                me.ordenCompra.listaProductos = [],
                me.ordenCompra.subtotalIva = 0,
                me.ordenCompra.subtotalNoIVA = 0,
                me.ordenCompra.total = 0,
                me.ordenCompra.totaliva = 0,
                me.ordenCompra.observacionOD = '',
                me.ordenCompra.providerFound = {},
                me.ordenCompra.fechaOD = '',
                me.ordenCompra.entregar = ''
              }
              else
                Swal({type: 'error',title: 'Oops...',text: response.data.message ,footer: 'Ocurrio un error'})
                  
              }).catch(function (error){
                  unblock();
                  Swal({type: 'error',title: 'Oops...',text: error,footer: 'Ocurrio un error'})
              })
          }
          else
            swal("Lista de productos vacia") 
        }
        else
          swal("Error en los campos") 
      });
    },
  },
  watch:{
    'productoAdd.precioUnitario':function(){
        let me = this;
        if(me.productoAdd.precioUnitario > 0 && me.productoAdd.cantidad > 0)
          me.productoAdd.subtotal = parseFloat(parseInt(me.productoAdd.cantidad) * parseFloat(me.productoAdd.precioUnitario)).toFixed(2)
        {{-- else 
          return  Swal({type: 'error',title: 'Oops...',text: 'error',footer: 'Existen campos vacios'}) --}}
    },
  }
});