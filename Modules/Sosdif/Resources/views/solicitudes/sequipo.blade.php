
@extends('sosdif::layouts.master')
@section('content-title', 'Solicitudes de Equipo')
@section('content-subtitle', 'Asignar un tecnico para cada solicitud, estado y las observaciones correspondientes')


@section('other-scripts')
<script>
$('#tecnico_id').select2({
        language: 'es',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('empleados.buscar') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      });
</script>
@endsection

@section('content')

<script type="text/javascript">


function asignar (id) {
  // body...
  var route='/sosdif/solicitudes/buscar/'+id;

  $.get(route, function (res){

        var res = res;

        $('#exampleInputEmail1').val(res.numero_solicitud)
        $('#exampleInputPassword1').val(res.solicitante_id)
        $('#servicio_id').val(res.servicio_id)
        $('#equipo_id').val(res.equipo_id)
        $('#fechainicio').val(res.fecha_inicio)
        $('#fechafin').val(res.fecha_fin)
        $('#asunto').val(res.asunto)
        $('#tecnico_id').val('')
        $('#estado_equipo').val('')
        $('#observaciones').val('')


        console.log(res)
    })
}

</script>

<section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Datos de la tabla </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">

              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Numero de Solicitud</th>
                  <th>Solicitante</th>
                  <th>Servicio</th>
                  <th>Equipo</th>
                  <th>Fecha de Inicio</th>
                  <th>Fecha final</th>
                  <th>Asunto</th>
                  <th>Tecnico</th>
                  <th>Estado del equipo</th>
                  <th>Observaciones</th>
                  @if(auth()->user()->sosdif())
                  <th>Acciones</th>
                  @endif
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($solicitudes as $solicitud)
                <tr>
                  <td>{{$solicitud->numero_solicitud}}</td>
                  <td>{{$solicitud->solicitante_id}}</td>
                 <td>{{$solicitud->tipossoporteservicios->tipo}}</td>
                 <td>{{$solicitud->catequipos['nombre']}}</td>
                  <td>{{$solicitud->fecha_inicio}}</td>
                  <td>{{$solicitud->fecha_fin}}</td>
                  <td>{{$solicitud->asunto}}</td>
                  @if(isset($solicitud->soportesolicitudesequipos))
                  <td>{{$solicitud->soportesolicitudesequipos->empleado->persona->nombre}}</td>
                  <td>{{$solicitud->soportesolicitudesequipos->estado_equipo}}</td>
                  <td>{{$solicitud->soportesolicitudesequipos->observaciones}}</td>
                  @else
                  <td></td>
                  <td></td>
                  <td></td>
                  @endif
                  <!--<td>
                     <form role="form" action="{{ route('ssequipos') }}" method="post">
                     </form>

                    <a class="btn btn-success" href="edit/{{$solicitud->id}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="#" role="button" onclick="event.preventDefault(); document.getElementById('delete-solicitud').submit();"><i class="fa fa-trash-o"></i></a> <!--forma para eliminar un registro y que sea de forma segura-->
                   <!-- <form action="{{url('sosdif/solicitudes/borra/' . $solicitud->id)}}" style="display: none;" id="delete-solicitud" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                    </form>-->
                    <!-- href hace referencia a la seccion formulario dentro de la misma pagina, para que al momento de dar clic en el boton se posisione en el formulario -->
                    @if(auth()->user()->sosdif())
                    <td>
                    <a type="button" class="btn bg-orange btn-flat margin" href="#formulario" onclick="asignar({{$solicitud->id}});" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    </td>
                    @endif 

                  

                    <!--<td><span class="label label-success">Iniciado</span><br>
                    <span class="label label-warning">Pendiente</span><br>-->
                  <td><span class="label label-info">Proceso</span><br> </td>
                    <!--<span class="label label-danger">Entregado</span></td>-->
                 </tr>
                @endforeach
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

        @if(auth()->user()->sosdif())
<div class="row" id="formulario">
  <div class="col-md-6">
          <!-- general form elements -->
          @if (session('status'))
             <div class="alert alert-success">
             {{ session('status') }}
              </div>
            @endif
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Asignar un tecnico para cada solicitud, estado y las observaciones correspondientes</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
              <h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{ route('ssequipos') }}" method="post">
              {{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de solicitud</label>
                  <input value="" readonly name="numero_solicitud" type="number" class="form-control" id="exampleInputEmail1" placeholder="Número de solicitud">
                
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Solicitante</label>
                  <input readonly name="solicitante_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante">
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Servicio a solicitar</label>
                  <select class="form-control" readonly name="servicio_id" id="servicio_id">
                  @foreach($tipos as $servicio)

                  @if(strtolower($servicio->tipo)!="prestamo")
                  <option value="{{ $servicio->id }}">{{ $servicio->tipo }}</option>
                  @endif
                  @endforeach
                  </select>
                </div>

               <div class="form-group">
                  <label for="exampleInputPassword1">Equipo</label>
                  <select readonly name="equipo_id" class="form-control select2" id="equipo_id" style="width: 100%;">
                    @foreach($cat_equipos as $key)
                    <option value="{{ $key->id }}">{{ $key->nombre }}</option>
                    @endforeach
                  </select>
              </div>
              
              <!--  <div class="form-group">
                  <label for="exampleInputPassword1">Tecnico</label>
                  <input name="tecnico_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante">
                </div>
              -->
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Inicio</label>
                  <input readonly name="fecha_inicio" type="date" class="form-control" id="fechainicio" placeholder="Fecha de Inicio">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Fin</label>
                  <input readonly name="fecha_fin" type="date" class="form-control" id="fechafin" placeholder="Fecha Final">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Asunto</label>
                  <input readonly name="asunto" type="text" class="form-control" id="asunto" placeholder="Asunto">
                </div>
               <!-- <div class="form-group">
                  <label for="exampleInputPassword1">Técnico</label>
                  <input name="tecnico_id" type="number" class="form-control" id="tecnico_id" placeholder="Tecnico">
                </div> -->
                <div class="form-group">
                  <label for="exampleInputPassword1">Tecnico</label>
                  <select name="tecnico_id" class="form-control select2" id="tecnico_id" style="width: 100%;"></select>
                  </div> 
                <div class="form-group">
                  <label for="exampleInputPassword1">Estado del equipo</label>
                  <input name="estado_equipo" type="text" class="form-control" id="estado_equipo" placeholder="Estado del equipo">
                </div> 
                <div class="form-group">
                  <label for="exampleInputPassword1">Observaciones</label>

                  <textarea name="observaciones" class="form-control" id="observaciones" placeholder="Observaciones"></textarea>
                </div>

                
                

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>
@else
<div>No tienes permiso</div>
@endif
<!--
<section class="content-header">
      <h1>
        Solicitudes de Equipo
        
      </h1>
      
    </section>
-->
    <!-- Main content -->
    
          
@stop



