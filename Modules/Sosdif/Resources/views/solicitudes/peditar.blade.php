
@extends('sosdif::layouts.master')

@section('content-title', 'Actualizar Solicitudes de prestamo')
@section('content-subtitle', 'Mostrar Solicitudes de prestamo')

@section('content')

<div class="row">
  <div class="col-md-6">
          <!-- general form elements -->
          @if (session('status'))
             <div class="alert alert-success">
             {{ session('status') }}
              </div>
            @endif
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Actualizar Solicitudes de Equipo / Prestamo</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
              <h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{url('sosdif/solicitudes/modificar' . $solicitud->id)}}" method="post">
              {{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->
              {{ method_field('PUT')}}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de solicitud</label>
                  <input value="{{$solicitud->numero_solicitud}}" readonly name="numero_solicitud" type="number" class="form-control" id="exampleInputEmail1" placeholder="Número de solicitud" value="{{$solicitud->numero_solicitud}}">
                
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Solicitante</label>
                  <input value="{{$solicitud->solicitante_id}}" name="solicitante_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante" value="{{$solicitud->solicitante_id}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Servicio a solicitar</label>
                  <input value="{{$solicitud->servicio_id}}" name="servicio_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Servicio" value="{{$solicitud->servicio_id}}">
                </div>
               <div class="form-group">
                  <label for="exampleInputPassword1">Equipo</label>
                  <input value="{{$solicitud->equipo_id}}" name="equipo_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Equipo" value="{{$solicitud->equipo_id}}">
                </div>
               <!-- <div class="form-group">
                  <label for="exampleInputPassword1">Tecnico</label>
                  <input value="{{$sequipos->tecnico_id}}" name="tecnico_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante" value="{{$sequipos->tecnico_id}}">
                </div>-->
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Inicio</label>
                  <input value="{{$solicitud->fecha_inicio}}" name="fecha_inicio" type="date" class="form-control" id="exampleInputPassword1" placeholder="Fecha de Inicio" value="{{$solicitud->fecha_inicio}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Fin</label>
                  <input value="{{$solicitud->fecha_fin}}" name="fecha_fin" type="date" class="form-control" id="exampleInputPassword1" placeholder="Fecha fin" value="{{$solicitud->ficha_fin}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Asunto</label>
                  <input value="{{$solicitud->asunto}}" name="asunto" type="text" class="form-control" id="exampleInputPassword1" placeholder="Solicitante" value="{{$solicitud->asunto}}">
                </div>
                
                

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Modificar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>

@stop

