
@extends('sosdif::layouts.master')
@section('content-title', 'Solicitudes de Equipo / Prestamo')
@section('content-subtitle', 'Mostrar Solicitudes de Equipo / Prestamo')

@section('other-scripts')
<script >
$('#equipo_id').select2({
        language: 'es',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('equipos.select') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      });
</script>

@endsection

@section('content')


<div class="row">
  <div class="col-md-6">
          <!-- general form elements -->
          @if (session('status'))
             <div class="alert alert-success">
             {{ session('status') }}
              </div>
            @endif
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Generar Solicitud de Equipo / Prestamo</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
              <h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{ route('spequipos') }}" method="post">
              {{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Número de solicitud</label>
                  <input value="{{++$ultimonumero}}" readonly name="numero_solicitud" type="number" class="form-control" id="exampleInputEmail1" placeholder="Número de solicitud">
                
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Solicitante</label>
                  <input value="{{auth()->user()->persona->empleado->id}}" name="solicitante_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante">
                </div>
                 <div class="form-group">
                  <label for="exampleInputPassword1">Servicio a solicitar</label>
                  <select class="form-control" name="servicio_id">
                  @foreach($tipos as $servicio)

                <!--  @if(strtolower($servicio->tipo)!="prestamo")
                  <option value="{{ $servicio->id }}">{{ $servicio->tipo }}</option>
                  @endif -->
                  <option value="{{$servicio->id}}">
                    {{$servicio->tipo}}
                  </option>
                  
                  @endforeach
                  </select>
                </div>

               <div class="form-group">
                  <label for="exampleInputPassword1">Equipo</label>
                  <select name="equipo_id" class="form-control select2" id="equipo_id" style="width: 100%;"></select>
              </div>
              
              <!--  <div class="form-group">
                  <label for="exampleInputPassword1">Tecnico</label>
                  <input name="tecnico_id" type="number" class="form-control" id="exampleInputPassword1" placeholder="Solicitante">
                </div>
              -->
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Inicio</label>
                  <input name="fecha_inicio" type="date" class="form-control" id="exampleInputPassword1" placeholder="Fecha de Inicio">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha Fin</label>
                  <input name="fecha_fin" type="date" class="form-control" id="exampleInputPassword1" placeholder="Fecha Final">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Asunto</label>
                  <input name="asunto" type="text" class="form-control" id="exampleInputPassword1" placeholder="Asunto">
                </div>
                
                

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar/Enviar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>


<section class="content-header">
      <h1>
        Solicitudes de Equipo / Prestamo
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Datos de la tabla </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Numero de Solicitud</th>
                  <th>Solicitante</th>
                  <th>Servicio</th>
                  <th>Equipo</th>
                  <th>Fecha de Inicio</th>
                  <th>Fecha final</th>
                  <th>Asunto</th>
                  <th>Acciones</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($solicitudes as $solicitud)
                <tr>
                  <td>{{$solicitud->numero_solicitud}}</td>
                  <td>{{$solicitud->solicitante_id}}</td>
                 <td>{{$solicitud->tipossoporteservicios->tipo}}</td>
                  <td>{{$solicitud->catequipos['nombre']}}</td>
                  <td>{{$solicitud->fecha_inicio}}</td>
                  <td>{{$solicitud->fecha_fin}}</td>
                  <td>{{$solicitud->asunto}}</td>
                  <td>
                     <form role="form" action="{{ route('spequipos') }}" method="post">
                     </form>

                    <a class="btn btn-success" href="editar/{{$solicitud->id}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="#" role="button" onclick="event.preventDefault(); document.getElementById('delete-solicitud').submit();"><i class="fa fa-trash-o"></i></a> <!--forma para eliminar un registro y que sea de forma segura-->
                    <form action="{{url('sosdif/solicitudes/borrar/' . $solicitud->id)}}" style="display: none;" id="delete-solicitud" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                    </form>
                   </td> 
                    <td><span class="label label-success">Iniciado</span><br>
                    <!--<span class="label label-warning">Pendiente</span><br>
                    <span class="label label-info">Proceso</span><br>
                    <span class="label label-danger">Entregado</span></td>-->
                 </tr>
                @endforeach
                </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          
@stop



