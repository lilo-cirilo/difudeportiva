@extends('vendor.admin-lte.layouts.main')


@push('head')
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
@yield('mystyles')
@endpush


@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="header">NAVEGACIÓN</li>
  <li class="active">
    <a href="{{ route('sinicio') }}">
      <i class="fa fa-home"></i>
      <span>Home</span>
    </a>
  </li>
  
    <li class="treeview">
      <a href="#">
        <i class="fa fa-lock"></i> <span>Servicios</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
      
        <li><a href="{{ route('scorrectivo') }}"><i class="fa fa-circle-o"></i> Insertar Servicios</a></li>

      </ul>

      <a href="#">
        <i class="fa fa-lock"></i> <span>Solicitudes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('solitecnico') }}"><i class="fa fa-circle-o"></i>Mis solicitudes / Técnicos</a></li>
     <li><a href="{{ route('soliequipo') }}"><i class="fa fa-circle-o"></i>Solicitudes de equipo</a></li>
        <li><a href="{{ route('soliprestamo') }}"><i class="fa fa-circle-o"></i>Solicitudes de prestamos</a></li>
      </ul>

<!--
       <a href="#">
        <i class="fa fa-lock"></i> <span>Personas</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('scorrectivo') }}"><i class="fa fa-circle-o"></i>Mis Solicitudes</a></li>
      </ul>
    -->

      <a href="#">
        <i class="fa fa-lock"></i> <span>Dictamen</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('scorrectivo') }}"><i class="fa fa-circle-o"></i>Nuevo</a></li>
      </ul>

       <a href="#">
        <i class="fa fa-lock"></i> <span>Catalogo de Equipos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('iequipo') }}"><i class="fa fa-circle-o"></i>Insertar Equipos</a></li>
       
      </ul>

      <li class="treeview">
      <a href="#">
        <i class="fa fa-lock"></i> <span>Reportes</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li class="active"><a href="javascript:void(0);" onclick="cargarlistado(3,1);"><i class="fa fa-circle-o"></i>
          Reportes </a></li>
        </ul>
        


       <a href="#">
        <i class="fa fa-lock"></i> <span>Contacto</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
    </li>
</ul>
@endsection

@section('message-url-all', '#')
@section('message-all', 'Ver todos los mensajes')
@section('message-number', 1)
@section('message-text', 'Tienes 1 mensages')
@section('message-list')
<!-- start message -->
<li>
  <a href="#">
      <div class="pull-left">
          <!-- User Image -->
          <img src="https://www.gravatar.com/avatar/?d=mm" class="img-circle" alt="User Image">
      </div>
      <!-- Message title and timestamp -->
      <h4>
          Support Team
          <small><i class="fa fa-clock-o"></i> 5 mins</small>
      </h4>
      <!-- The message -->
      <p>Why not buy a new awesome theme?</p>
  </a>
</li>
<!-- end message -->
@endsection

@section('notification-number', 1)
@section('notification-list')
<a href="#">
    <i class="fa fa-users text-aqua"></i> 5 new members joined today
</a>
@endsection

@section('task-number', 1)
@section('task-text', 'Tienes 1 tarea')
@section('task-url-all')
<a href="#">View all tasks</a>
@endsection
@section('task-list')
<li>
  <a href="#">
      <!-- Task title and progress text -->
      <h3>
          Design some buttons
          <small class="pull-right">20%</small>
      </h3>
      <!-- The progress bar -->
      <div class="progress xs">
          <!-- Change the css width attribute to simulate progress -->
          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              <span class="sr-only">20% Complete</span>
          </div>
      </div>
  </a>
</li>
@endsection

@push('body')
<script src="{{ asset('assets/js/validator.js') }}"></script>
@yield('cdn-scripts')
<script type="text/javascript" src="{{ asset('bower_components/select2/select2/select2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
@yield('other-scripts')
@endpush