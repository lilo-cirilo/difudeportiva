@extends('sosdif::layouts.master')

@section('content-title', 'Equipos')
@section('content-subtitle', 'Equipos')

@section('other-scripts')
<script>
$('#resguardo_id').select2({
        language: 'es',
        minimumInputLength: 2,
        ajax: {
          url: '{{ route('empleados.buscar') }}',
          delay: 500,
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      });
</script>
@endsection

@section('content')

@if(auth()->user()->sosdif())
<div class="row">
	<div class="col-md-6">
          <!-- general form elements -->
           @if (session('status'))
             <div class="alert alert-success">
             {{ session('status') }}
              </div>
            @endif
        <div class="box box-primary">
            <div class="box-header with-border">
            	<h3 class="box-title">AGREGAR EQUIPOS AL INVENTARIO</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <!--declaración de la ruta y el meto utilizado en este caso post-->
            @if(isset($msg))
            	<h2>{{ $msg }}</h2>
            @endif
            <form role="form" action="{{ route('iequipos') }}" method="post">
            	{{ csrf_field() }}<!--todos los formularios deben llevar un toquen para proteger del ataque de falsificacion-->

              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre</label>
                  <input name="nombre" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nombre del equipo">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Numero de serie</label>
                  <input name="num_serie" type="text" class="form-control" id="exampleInputPassword1" placeholder="Numero de serie">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Numero de inventario</label>
                  <input name="num_inventario" type="text" class="form-control" id="exampleInputPassword1" placeholder="Numero de inventario">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Direccion IP</label>
                  <input name="direccion_ip" type="text" class="form-control" id="exampleInputPassword1" placeholder="Direccion IP">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Capacidad</label>
                  <input name="capacidad" type="text" class="form-control" id="exampleInputPassword1" placeholder="Capacidad">
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Marca</label>
                  <select name="marca_id" type="number" class="form-control" id="exampleInputFile">
                  @foreach($marcas as $marca)
                  <option value="{{$marca->id}}">
                    {{$marca->nombre}}
                  </option>
                  @endforeach
                </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Modelo</label>
                  <select name="modelo_id" type="number" class="form-control" id="exampleInputPassword1">
                @foreach($modelos as $modelo)
                <option value="{{$modelo->id}}">
                  {{$modelo->nombre}}
                </option>
                @endforeach
              </select>
                </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Tipo de Equipo</label>
                  <select name="tipoequipo_id" type="number" class="form-control" id="exampleInputPassword1">
                @foreach($tipos as $tipo)
                <option value="{{$tipo->id}}">
                  {{$tipo->tipo}}
                </option>
                @endforeach
              </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Resguardo</label>
                  <select name="resguardo_id" class="form-control select2" id="resguardo_id" style="width: 100%;"></select>
                  </div>  
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
    </div>
</div>
@endif

 <!-- Content Wrapper. Contains page content -->
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tabla de Equipos existentes
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Datos de la tabla</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Numero de serie</th>
                  <th>Numero de inventario</th>
                  <th>Direccion IP</th>
                  <th>Capacidad</th>
                  <th>Marca</th>
                  <th>Modelo</th>
                  <th>Tipo de Equipo</th>
                  <th>Resguardo</th>
                  @if(auth()->user()->sosdif())
                  <th>Acciones</th>
                  @endif
                </tr>
                </thead>
                <tbody>
                  <!--foreach para guardar los datos en una tabla-->
                  @foreach($equipos as $equipo)
                <tr>
                  <td>{{$equipo->nombre}}</td>
                  <td>{{$equipo->num_serie}}</td>
                  <td>{{$equipo->num_inventario}}</td>
                  <td>{{$equipo->direccion_ip}}</td>
                  <td>{{$equipo->capacidad}}</td>
                  <td>{{$equipo->catmarcasequipos->nombre}}</td> <!-- aqui va el metodo que aparece en el modelo-->
                  <td>{{$equipo->catmodelosequipos->nombre}}</td>
                  <td>{{$equipo->cattiposequipos->tipo}}</td>
                
                  <td>{{$equipo->empleado->persona->nombre}}</td> <!--se hacen las relaciones en este caso, primero se accede a empleado, despues se accede a la persona para poder obbtener su nombre-->
                 
                  @if(auth()->user()->sosdif()) 
                   <td>
                     <form role="form" action="{{ route('iequipos') }}" method="post">
                     </form>

                    <a class="btn btn-success" href="editar/{{$equipo->id}}" role="button"><i class="fa fa-pencil-square-o"></i></a>
                    <a class="btn btn-danger" href="#" role="button" onclick="event.preventDefault(); document.getElementById('delete-equipo').submit();"><i class="fa fa-trash-o"></i></a> <!--forma para eliminar un registro y que sea de forma segura-->
                    <form action="{{url('sosdif/inventario/borrar/' . $equipo->id)}}" style="display: none;" id="delete-equipo" method="POST">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}
                    </form>
                   </td> 
                   @endif


                </tr>
                @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>


</body>
</html>
@stop <!--fin del contenido-->


       

 