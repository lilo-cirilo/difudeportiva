<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\SoporteServicios;
use \App\Models\TiposSoporteServicios;

class ServicioscController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tipos=TiposSoporteServicios::all();
        $servicios=SoporteServicios::where('tiposervicio_id', 1)->get(); //para obtener solo los servicios que pertenecen al tipo de servicio 1
        $ultimonumero=SoporteServicios::orderBy('id', 'desc')->take(1)->get();
        //dd($ultimonumero[0]->numero_servicio);
        return view('sosdif::servicios.mantenimientoc', ['servicios'=>$servicios, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_servicio]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $tipos=TiposSoporteServicios::all();
        $servicios=SoporteServicios::where('tiposervicio_id', 1)->get(); 

        //consulta para guardar el numero de servicio sin que nadie lo pueda modificar//
        $ultimonumero=SoporteServicios::orderBy('id', 'desc')->take(1)->get();

        if(($ultimonumero[0]->numero_servicio+1)!=$request->numero_servicio){
            $servicios = SoporteServicios::all();
            return view('sosdif::servicios.mantenimientoc', ['servicios' => $servicios, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_servicio, 'msg' => 'El numero de servicio es incorrecto']);
        }

        //consulta para guardar los servicios//
        $servicio= SoporteServicios::create([
            'numero_servicio' =>$request->numero_servicio,
            'nombre'=>$request->nombre,
            'tiposervicio_id'=>$request->tiposervicio_id]);
        $msg = '';
        if(isset($servicio->id)){
            $msg = 'Se guardo el servicio';
        }else{
            $msg = 'Ocurrio un error';
        }
        $servicios = SoporteServicios::all();
        return view('sosdif::servicios.mantenimientoc', ['servicios' => $servicios, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_servicio, 'msg' => 'El numero de servicio es incorrecto', 'msg' => $msg]);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('sosdif::show');
        dd('Mostrando el detalle del servicio: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
    */

    public function preEdit($id){

        //echo 'ID Servicio'.$id;
       $servicio = SoporteServicios::where('id', $id)->first();
       $tipos=TiposSoporteServicios::all();
        return view('sosdif::servicios.editar', ['servicio'=>$servicio, 'tipos' => $tipos]);
        
    }
    /*public function edit(Request $request, $id)
    {
        $nuevoNumeroservicio=$request->input('numero_servicio');
        $nuevoNombre=$request->input('nombre');
        $nuevoTiposervicio=$request->input('tiposervicio_id');

        $servicio=  SoporteServicios::find($id);

        $servicio->numero_servicio=$nuevoNumeroservicio;
        $servicio->nombre=$nuevoNombre;
        $servicio->tiposervicio_id=$nuevoTiposervicio;
        $servicio->save();

        return view('servicios.modificado');
        //$servicios=SoporteServicios::find($id);
        //dd($servicios);
       // return view('sosdif::edit');
         //return view('sosdif::servicios.mantenimientoc', ['servicios' => $servicios, 'tipos'=>$tipos, 'ultimonumero'=>$ultimonumero[0]->numero_servicio, 'msg' => 'El numero de servicio es incorrecto', 'msg' => $msg]);



    }*/

   /* public function borra($id){
        SoporteServicios::destroy($id);
        return view('servicios.borrado');
    }*/

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $nuevoNumeroservicio=$request->input('numero_servicio');
        $nuevoNombre=$request->input('nombre');
        $nuevoTiposervicio=$request->input('tiposervicio_id');

        $servicio=  SoporteServicios::findOrFail($id);

        $servicio->numero_servicio=$nuevoNumeroservicio;
        $servicio->nombre=$nuevoNombre;
        $servicio->tiposervicio_id=$nuevoTiposervicio;
        $servicio->save();

       $tipos=TiposSoporteServicios::all();
        return view('sosdif::servicios.editar', ['servicio'=>$servicio, 'tipos' => $tipos, 'status' => 'Actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    /*public function eliminar($id){
        SoporteServicios::destroy($id);
        return view('servicios.eliminado');
    }*/
    public function destroy($id)
    {

        $servicio= SoporteServicios::findOrFail($id);
        $servicio->delete();
     return redirect()->action('\Modules\Sosdif\Http\Controllers\ServicioscController@index')->with('status', 'Servicio eliminado!');
    }
}
