<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\SoporteSolicitudes;
use \App\Models\SoporteSolicitudesEquipos;
use \App\Models\SoporteSolicitudesPrestamos;
use \App\Models\SoporteServicios;
use \App\Models\TiposSoporteServicios;

class SolicitudController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $solicitudes=SoporteSolicitudes::all();
        return view('sosdif::solicitudes.solicitud', ['solicitudes'=>$solicitudes]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $solicitud =SoporteSolicitudes::create($request->all());
        $msg = '';
        if(isset($solicitud->id)){
            $msg = 'Se guardo el servicio';
        }else{
            $msg = 'Ocurrio un error';
        }
        $solicitudes = SoporteSolicitudes::all();
        return view('sosdif::solicitudes.solicitud', ['solicitudes' =>$solicitudes, 'msg' => $msg]);

        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        rdd('Mostrando el detalle de la solicitud: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sosdif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
