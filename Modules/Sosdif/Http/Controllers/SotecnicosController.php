<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\SoporteSolicitudesEquipos;
use \App\Models\SoporteSolicitudes;
use \App\Models\Inventario;
use \App\Models\TiposSoporteServicios;
use \App\Models\Empleado;
use \App\Models\TiposEquipos;
use \App\Models\Equipos;
use \App\Models\RespuestasSoporteSolicitudes;
use DB;

class SotecnicosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       // return view('sosdif::index');

        $cat_equipos=Equipos::all();
        $tipos=TiposSoporteServicios::all();
        $soliEquipos=SoporteSolicitudesEquipos::all();
        $respuestas=RespuestasSoporteSolicitudes::all();
        $solicitudes=SoporteSolicitudes::where('servicio_id','!=', 4)->get();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
        return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'respuestas'=>$respuestas, 'soliEquipos'=>$soliEquipos, 'tipos'=>$tipos,  'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'cat_equipos'=>$cat_equipos]);
   
    }
    public function buscar($id)
    {
    
       
        $solicitudes=SoporteSolicitudes::find($id);
       
        

        return response()->json($solicitudes);
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
     $fila = SoporteSolicitudesEquipos::where('solicitud_id', $request->numero_solicitud)->take(1)->get();
        if($fila->count() > 0){
            $fila[0]->tecnico_id = $request->tecnico_id;
            $fila[0]->estado_equipo = $request->estado_equipo;
            $fila[0]->observaciones = $request->observaciones;
            $fila[0]->save();
        }else{
            $SoporteSolicitudesEquipos= SoporteSolicitudesEquipos::create([
                'solicitud_id'=>$request->numero_solicitud,
                'tecnico_id'=>$request->tecnico_id,
                'estado_equipo'=>$request->estado_equipo,
                'observaciones'=>$request->observaciones,
            ]);    
        }
        
        $cat_equipos=Equipos::all();
        $tipos=TiposSoporteServicios::all();
        $solicitudes=SoporteSolicitudes::where('servicio_id','!=', 4)->get();
        $ultimonumero=SoporteSolicitudes::orderBy('id', 'desc')->take(1)->get();
         return view('sosdif::solicitudes.sequipo', ['solicitudes'=>$solicitudes, 'tipos'=>$tipos,  'ultimonumero'=>$ultimonumero[0]->numero_solicitud, 'cat_equipos'=>$cat_equipos]);
   

     }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('sosdif::show');
         dd('Mostrando el detalle de la solicitud: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sosdif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscartecnico(Request $request){

        $personas=Empleado::select('empleados.id', 'personas.nombre')->join('personas', 'persona_id', '=', 'personas.id')->where('nombre', 'LIKE', '%'.$request->search.'%')->get();
        return response()->json($personas);
        // se hace la relación de la tabla empleado y persona, el empleado con su id y la persona porque tiene los campos que son el nombre//
        // '%'. comodin se utilizan para que al momento de hacer la busqueda regrese los valores que coincidan, aunque tenga mas datos antes o despues de los valores..

    }
}
