<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\Equipos;
use \App\Models\TiposEquipos;
use \App\Models\CatMarcasEquipos;
use \App\Models\CatModelosEquipos;
use \App\Models\Empleado;

class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {

        $marcas=CatMarcasEquipos::all();
        $modelos=CatModelosEquipos::all();
        $tipos=TiposEquipos::all();
        $equipos=Equipos::all();

        return view('sosdif::inventario.equipo', ['equipos'=>$equipos, 'marcas'=>$marcas, 'modelos'=>$modelos, 'tipos'=>$tipos]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('sosdif::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $marcas=CatMarcasEquipos::all();
        $modelos=CatModelosEquipos::all();
        $tipos=TiposEquipos::all();
        $equipos =Equipos::all();
       
        $equipo= Equipos::create([
            'nombre' =>$request->nombre,
            'num_serie'=>$request->num_serie,
            'num_inventario'=>$request->num_inventario,
            'direccion_ip'=>$request->direccion_ip,
            'capacidad'=>$request->capacidad,
            'marca_id'=>$request->marca_id,
            'modelo_id'=>$request->modelo_id,
            'tipoequipo_id'=>$request->tipoequipo_id,
            'resguardo_id'=>$request->resguardo_id]);
        $msg = '';
        if(isset($equipo->id)){
            $msg = 'Se guardo el equipo';
        }else{
            $msg = 'Ocurrio un error';
        }
        $equipos = Equipos::all();
        return view('sosdif::inventario.equipo', ['equipos' => $equipos, 'marcas'=>$marcas, 'modelos'=>$modelos, 'tipos'=>$tipos, 'msg' => $msg]);
     }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        //return view('sosdif::show');
        dd('Mostrando el detalle del equipo: '.$id);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function preEdit($id)
    {
       // return view('sosdif::edit');
        $marcas=CatMarcasEquipos::all();
        $modelos=CatModelosEquipos::all();
        $tipos=TiposEquipos::all();
        $equipo=Equipos::where('id', $id)->first();
        return view('sosdif::inventario.editar', ['equipo'=>$equipo, 'marcas'=>$marcas, 'modelos'=>$modelos, 'tipos'=>$tipos]);
   

    
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $nuevoNombre=$request->input('nombre');
        $nuevonumeroSerie=$request->input('num_serie');
        $nuevonumeroInventario=$request->input('num_inventario');
        $nuevoDireccionip=$request->input('direccion_ip');
        $nuevoCapacidad=$request->input('capacidad');
        $nuevoMarca=$request->input('marca_id');
        $nuevoModelo=$request->input('modelo_id');
        $nuevoTipoequipo=$request->input('tipoequipo_id');
        $nuevoResguardo=$request->input('resguardo_id');
       

        $equipo= Equipos::findOrFail($id);

        $equipo->nombre=$nuevoNombre;
        $equipo->num_serie=$nuevonumeroSerie;
        $equipo->num_inventario=$nuevonumeroInventario;
        $equipo->direccion_ip=$nuevoDireccionip;
        $equipo->capacidad=$nuevoCapacidad;
        $equipo->marca_id=$nuevoMarca;
        $equipo->modelo_id=$nuevoModelo;
        $equipo->tipoequipo_id=$nuevoTipoequipo;
        $equipo->resguardo_id=$nuevoResguardo;
        $equipo->save();

        $marcas=CatMarcasEquipos::all();
        $modelos=CatModelosEquipos::all();
        $tipos=TiposEquipos::all();
        return view('sosdif::inventario.editar', ['equipo'=>$equipo, 'marcas' => $marcas, 'modelos' => $modelos, 'tipos'=>$tipos, 'status' => 'Actualizado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {

        $equipo= Equipos::findOrFail($id);
        $equipo->delete();
        return redirect()->action('\Modules\Sosdif\Http\Controllers\EquiposController@index')->with('status', 'Equipo eliminado!');
   


    }
    
   /*  public function buscarempleado(Request $request){

        $equipos=TiposEquipos::where('id', 'LIKE', '%'.$request->search.'%')->with('Empleado')->get();
        return response()->json($empleados);
        // '%'. comodin se utilizan para que al momento de hacer la busqueda regrese los valores que coincidan, aunque tenga mas datos antes o despues de los valores..

    }*/
}
