<?php

namespace Modules\Sosdif\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use \App\Models\TiposEquipos;
use \App\Models\CatMarcasEquipos;
use \App\Models\CatModelosEquipos;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('sosdif::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pdf.listado_reportes');

    }
    public function crearPDF($datos,$vistaurl,$tipo){
        $data=$datos;
        $date=date('Y-m-d');
        $view =\View::make($vistaurl, compact('data', 'date'))->render();
        $pdf = \App::make('dompdf.wrapper');

        if($tipo==1){ return $pdf->stream('reporte');}
        if($tipo==2){return $pdf->download('reporte.pdf');}
    }

    public function crear_reporte_equipos($tipo){
        $vistaurl="pdf.reporte_de_equipos";
        $equipos=TiposEquipos::all();

        return $this->createPDF($equipos, $vistaurl,$tipo);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('sosdif::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('sosdif::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
