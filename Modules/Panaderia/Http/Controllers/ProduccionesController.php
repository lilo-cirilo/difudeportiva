<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Produccion;
use Modules\Panaderia\Entities\Solicitud;
use Modules\Panaderia\Entities\Produccionprogramada;
use Modules\Panaderia\Entities\Produccionnormal;
use Modules\Panaderia\Entities\Produccionsolicitud;
use Modules\Panaderia\Entities\Presentacion;
use Modules\Panaderia\Entities\Producto;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;



class ProduccionesController extends Controller
{
     public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO,PRODUCCION',
        ['only' => ['store','produccionnormal']]);
    } 
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
					$producciones = Produccion::with('solicitud','normal')->paginate($request->per_page);          
        return 
        [                        
           'producciones' => $producciones,                              
         ];             
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        try {
            DB::beginTransaction();  
        $produccion = new Produccion();
        $produccion->folio='-';
        $produccion->fecha_elaboracion = date("Y-m-d");
        $produccion->usuario_id = auth()->user()->id;
        $produccion->save();
        $produccion->folio='PAN-SOL-'.$produccion->id.'-'.date("Y");
        $produccion->save();
        
        $produccionSolicitud = new Produccionsolicitud();
        $produccionSolicitud->produccion_id = $produccion->id;
        $produccionSolicitud->solicitud_id= $request->id;
        $produccionSolicitud->usuario_id =auth()->user()->id;       
        $produccionSolicitud->save();

        DB::commit();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_producciones',
                'registro' => $produccion->id . '',
                'campos' => json_encode($produccion) . '',
                'metodo' => $request->method()    
            ]);
            $usuario->bitacora($request, [
                'tabla' => 'pan_produccionessolicitudes',
                'registro' => $produccionSolicitud->id . '',
                'campos' => json_encode($produccionSolicitud) . '',
                'metodo' => $request->method()    
            ]);
        return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
        

    }

   

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {    
        $produccion=Produccion::find($id);                
        $pdf = PDF::loadView(
            'panaderia::reportes.productossolicitados',
            [                    
                "produccion"=>$produccion,
               
            ]                
        );
        $pdf->setPaper("letter");
        return $pdf->stream('productos.pdf');                
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

     public function produccionnormal(Request $request)
    {
        try {
            DB::beginTransaction();  
        $produccion = new Produccion();
        $produccion->folio='-';
        $produccion->fecha_elaboracion = date("Y-m-d");
        $produccion->usuario_id =auth()->user()->id;
        $produccion->save();
        $produccion->folio='PAN-PG-'.$produccion->id.'-'.date("Y");
        $produccion->save();
        $produccionNormal = new Produccionnormal();
        $produccionNormal->produccion_id = $produccion->id;
        $produccionNormal->produccionprogramada_id= $request->id;
        $produccionNormal->usuario_id = auth()->user()->id;
        $produccionNormal->save();        
        DB::commit();
        return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'id'=>$produccionNormal->id], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
        

    }

    public function salidasPendientes(){
        $producciones=Produccion::doesntHave('salidasInsumos')->get();        
        $presentaciones=Presentacion::get();
        return [
            'producciones'=>$producciones,
            'presentaciones'=>$presentaciones
        ];
    }    
    public function productosProduccion($id){                
        $productos=Produccion::with('normal.programada.programados')->find($id);
    }
    
    public function productosProduccionProgramada($id){ 

        $productos=Produccion::with('normal.programada.programados.receta.ingredientes')->find($id);
        /* $productos=Produccion::with('normal.programada.programados')->whereHas(
            'normal.programada',function($query){
                return $query->where('estatus_id',7);
            }
        )->find($id);    */
        return $productos;     
    }
    public function buscarProduccionsolicitud($folio){
        $resultado=Produccion::where('folio',$folio)->first();
        $resultado->solicitud->solicitud->detalles;
        return $resultado;
    } 
    public function buscarProduccionnormal($folio){
        $resultado=Produccion::where('folio',$folio)->first();
        $resultado->normal->programada->programados;
        return $resultado;
    } 
    public function showSolicitud($id)
    {
        $produccion=Produccion::findOrFail($id);
        $produccion->solicitud->solicitud->detalles;      
        return $produccion;
    }      
    public function productosProduccionSolicitud($id){
        $productos=Produccion::with('solicitud.solicitud.detalles.receta.ingredientes')->find($id);
        return $productos;
    }   
    public function getProduccion($id){
        $produccion=Produccion::findOrFail($id);
        $produccion->normal->programada->programados;      
       return $produccion;
    }

    public function finalizarProduccionNormal(Request $request,$id){
        $produccion =Produccionprogramada::findOrfail($id); 
        $produccion->estatus_id = $request->estatus; 
        $lista = $produccion->programados;
        
        foreach ($lista as $item) {
            $producto=(object)$item;
            $elaborado=Producto::find($producto->id);           
            $elaborado->stock = ($elaborado->stock+$producto->pivot->cantidad);
            $elaborado->save();

        } 
        $produccion->save();  
        return;
    }

    public function finalizarProduccionSolicitud(Request $request,$id){
        $solicitud =Solicitud::findOrfail($id); 
        $solicitud->estatus_id = $request->estatus; 
        $lista = $solicitud->detalles;
        
        foreach ($lista as $item) {
            $producto=(object)$item;
            $elaborado=Producto::find($producto->id);           
            $elaborado->stock = ($elaborado->stock+$producto->pivot->cantidad);
            $elaborado->save();

        } 
        $solicitud->save();  
        return;
    }
    
}
