<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\InsumosPanaderia;
use Modules\Panaderia\Entities\Insumo;
use Modules\Panaderia\Entities\Presentacion;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;

class InsumosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
     /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','show']]);
        $this->middleware('rolModuleV2:panaderia,ALMACEN,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store']]);
    }
    public function index(Request $request)
    {
        /* if(strlen($request->insumo)>0)        
        return $insumos=InsumosPanaderia::with(['insumo','presentacion'])->whereHas('insumo',
        function ($query)use($request){
        $query->where('producto','like','%'.$request->insumo.'%');
        })->where('cantidad','>','0')->paginate($request->per_page);
        return $insumos=InsumosPanaderia::with('insumo','presentacion')->where('cantidad','>','0')->paginate($request->per_page); */
        return $insumos=InsumosPanaderia::with('insumo','presentacion')->where('cantidad','>','0')->get();
        
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
      try{
          DB::beginTransaction();
          $insumos=$request->insumos;   
        
          foreach ($insumos as $insumo) {
               $entrada=new InsumosPanaderia();
               $entrada->insumo_id=$insumo['producto']['id'];
               $entrada->presentacion_id=$insumo['presentacion']['id'];
               $entrada->folio_ordencompra=$insumo['folio_compra'];
               $caducidad=$insumo['caducidad'];
               $entrada->fecha_caducidad=Carbon::createFromFormat('d-m-Y',$caducidad);
               $entrada->lote=$insumo['lote'];
               $entrada->cantidad_entrada=$insumo['entrada'];
               $entrada->cantidad=$insumo['entrada'];
               $entrada->precio_unitario=$insumo['precio_unitario'];            
               $entrada->usuario_id=auth()->user()->id;  
               $entrada->save();                  
           }
          DB::commit();
          $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_insumos',
                'registro' => $request->usuario_id. '',
                'campos' => json_encode($request->insumos) . '',
                'metodo' => $request->method()    
            ]);
          return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'folioCompra'=>$request['folio_compra']], 200);
      }
      catch(\Exception $e) {
        DB::rollBack();
        return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
    }
       
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($insumo)
    {
        $insumos=InsumosPanaderia::whereHas('insumo',function($query)use($insumo){
            $query->where('nombre','like',$insumo);
        });
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function existeOD($folio){
        $total=InsumosPanaderia::where('folio_ordencompra',$folio)->get();        
        $existencia=count($total);
        $presentaciones=Presentacion::where('presentacion','LITRO')
        ->OrWhere('presentacion','KILOGRAMO')
        ->Orwhere('presentacion','PIEZA')->get();
        return [
            'existencia'=>$existencia,
            'presentaciones'=>$presentaciones,
        ];
    }

    public function listaInsumos(){
       /*  $insumos=Insumo::has('entradas')->get();
        $presentaciones=Presentacion::get();       
        return [
            'insumos'=>$insumos,
            'presentaciones'=>$presentaciones,
        ] */;
        return DB::table('pan_insumos as insu')
        ->join('recm_cat_productos as pro','pro.id','=','insu.insumo_id')
        ->join('recm_cat_presentacions as pre','pre.id','=','insu.presentacion_id')
        ->select('pro.id as proID','pro.producto','pre.id as preID','pre.presentacion')
        ->groupBy('pro.id','pro.producto','pre.id','pre.presentacion')
        ->get();
    }

    public function buscarInsumoExistencia($id){
        $insumos=InsumosPanaderia::with(['insumo','presentacion'])->whereHas('insumo',
        function ($query)use($id){
        $query->where('id',$id);
        })->where('cantidad','>','0')->orderBy('fecha_caducidad')->get();       

        return $insumos;
		}  
		
		public function reportePdf(){
			$insumos=InsumosPanaderia::with('insumo','presentacion')->where('cantidad','>','0')->get();
			$pdf=PDF::loadView('panaderia::reportes.almacen.insumos',['insumos'=>$insumos]);
			$pdf->setPaper("letter");
			return $pdf->stream('Isumos.pdf');
		}
}
