<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Panaderia\Entities\Venta;
use Modules\Panaderia\Entities\Solicitud;
use App\Models\EventosTaller;
use Barryvdh\DomPDF\Facade as PDF;

class ReportesController extends Controller
{
    public function ventasMes($mes)
    {
        $anio=date("Y");
        return DB::table('pan_ventas as vta')->whereYear('vta.fecha_venta',$anio)
        ->whereMonth('vta.fecha_venta',$mes)->select('vta.fecha_venta',DB::raw('count(vta.id) as ventas'),
        DB::raw('sum(vta.total) as total'))->groupBy('vta.fecha_venta')->get();
    }
    public function masVendidosMes($mes){
        $anio=date("Y");
        $ventasMes= DB::table('pan_ventas as vta')->whereMonth('vta.fecha_venta',$mes)->whereYear('vta.fecha_venta',$anio)
        ->join('pan_detallesventa as det','vta.id','=','det.venta_id')
        ->join('pan_productos as pro','pro.id','=','det.producto_id')
        ->select('pro.nombre',DB::raw('sum(det.cantidad) as cantidad'))
        ->orderBy('cantidad','DESC')
        ->groupBY('pro.nombre')
        ->limit(10)
        ->get();
        
        return $ventasMes;
    }
    public function ventasDelDia($fecha){
        $ventas=DB::table('pan_ventas')->where('fecha_venta',$fecha)
        ->select(DB::raw('count(id) ventas'))->get();
        return $ventas;
    }
    public function reportesVentas(Request $request){        
        $anioActual=$request->anio;
        $mesInicio=0;
        $mesFin=1;        
        $tipo=$request->tipo;
        $ventas=[];
        $costoProduccion=[];
        if($tipo=='mensual'){
            $ventas= DB::table('pan_ventas')->whereMonth('fecha_venta',$request->mes)
            ->whereYear('fecha_venta',$anioActual)
            ->select(DB::raw('count(id) as ventas'),'fecha_venta',DB::raw('sum(total) total'))
            ->groupBy('fecha_venta')
            ->get();
            $costoProduccion=DB::table('pan_producciones as pro')->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
            ->where('pro.folio','like','%PAN-PG%')
            ->whereYear('fecha_elaboracion',$anioActual)
            ->whereMonth('fecha_elaboracion',$request->mes)             
            ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
            ->get();
            return ['ventas'=>$ventas,'costoProduccion'=>$costoProduccion[0]];         
        }
        if($tipo=='bimestral'){
            switch ($request->bimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=2;
                    break;
                case '2':
                $mesInicio=3;
                $mesFin=4;
                    break;
                case '3':
                $mesInicio=5;
                $mesFin=6;
                    break;
                case '4':
                $mesInicio=7;
                $mesFin=8;
                    break;
                case '5':
                $mesInicio=9;
                $mesFin=10;
                    break;
                case '6':
                $mesInicio=11;
                $mesFin=12;
                    break;                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='trimestral'){
            switch ($request->trimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=3;
                    break;
                case '2':
                $mesInicio=4;
                $mesFin=6;
                    break;
                case '3':
                $mesInicio=7;
                $mesFin=9;
                    break;
                case '4':
                $mesInicio=10;
                $mesFin=12;
                    break;
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='semestral'){
            switch ($request->semestre) {
                case '1':
                $mesInicio=1;
                $mesFin=6;
                    break;
                case '2':
                $mesInicio=7;
                $mesFin=12;
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='anual'){
            $mesInicio=1;
            $mesFin=12;
        }

        $ventas=DB::table('pan_ventas')
        ->whereYear('fecha_venta',$anioActual)
        ->whereMonth('fecha_venta','>=',$mesInicio)
        ->whereMonth('fecha_venta','<=',$mesFin)        
        ->select(DB::raw('count(id) as ventas'),'fecha_venta',DB::raw('sum(total) total'))
        ->groupBy('fecha_venta')
        ->get();
        $costoProduccion=DB::table('pan_producciones as pro')->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
        ->where('pro.folio','like','%PAN-PG%')
        ->whereYear('fecha_elaboracion',$anioActual)
        ->whereMonth('fecha_elaboracion','>=',$mesInicio)
        ->whereMonth('fecha_elaboracion','<=',$mesFin)  
        ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
        ->get();        

        return ['ventas'=>$ventas,'costoProduccion'=>$costoProduccion[0]];
    }

    public function reportesPedidos(Request $request){        
        $anioActual=$request->anio;
        $mesInicio=0;
        $mesFin=1;        
        $tipo=$request->tipo;
        $pedidos=[];
        $costoProduccion=[];
        if($tipo=='mensual'){
            $pedidos= DB::table('pan_solicitudes')
            ->where('estatus_id','5')
            ->where('folio','like','PAN-PED%')
            ->whereMonth('fecha_entrega',$request->mes)
            ->whereYear('fecha_entrega',$anioActual)
            ->select(DB::raw('count(id) as pedidos'),'fecha_entrega',DB::raw('sum(total) total'))
            ->groupBy('fecha_entrega')
            ->get();
   
            $costoProduccion=DB::table('pan_producciones as pro')
            ->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
            ->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
            ->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
            ->where('s.folio','like','%PAN-PED%')
            ->where('s.estatus_id','5')
            ->whereYear('fecha_elaboracion',$anioActual)
            ->whereMonth('fecha_elaboracion',$request->mes)             
            ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
            ->get(); 
            return ['pedidos'=>$pedidos,  'costoProduccion'=>$costoProduccion[0] ];         
        }
        if($tipo=='bimestral'){
            switch ($request->bimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=2;
                    break;
                case '2':
                $mesInicio=3;
                $mesFin=4;
                    break;
                case '3':
                $mesInicio=5;
                $mesFin=6;
                    break;
                case '4':
                $mesInicio=7;
                $mesFin=8;
                    break;
                case '5':
                $mesInicio=9;
                $mesFin=10;
                    break;
                case '6':
                $mesInicio=11;
                $mesFin=12;
                    break;                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='trimestral'){
            switch ($request->trimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=3;
                    break;
                case '2':
                $mesInicio=4;
                $mesFin=6;
                    break;
                case '3':
                $mesInicio=7;
                $mesFin=9;
                    break;
                case '4':
                $mesInicio=10;
                $mesFin=12;
                    break;
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='semestral'){
            switch ($request->semestre) {
                case '1':
                $mesInicio=1;
                $mesFin=6;
                    break;
                case '2':
                $mesInicio=7;
                $mesFin=12;
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='anual'){
            $mesInicio=1;
            $mesFin=12;
        }

        $pedidos=DB::table('pan_solicitudes')
        ->where('estatus_id','5')
        ->where('folio','like','PAN-PED%')
        ->whereYear('fecha_entrega',$anioActual)
        ->whereMonth('fecha_entrega','>=',$mesInicio)
        ->whereMonth('fecha_entrega','<=',$mesFin)        
        ->select(DB::raw('count(id) as pedidos'),'fecha_entrega',DB::raw('sum(total) total'))
        ->groupBy('fecha_entrega')
        ->get();
        $costoProduccion=DB::table('pan_producciones as pro')
        ->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
        ->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
        ->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
        ->where('s.folio','like','%PAN-PED%')
        ->where('s.estatus_id','5')
        ->whereYear('fecha_elaboracion',$anioActual)
        ->whereMonth('fecha_elaboracion','>=',$mesInicio)
        ->whereMonth('fecha_elaboracion','<=',$mesFin)       
        ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
        ->get(); 
        return ['pedidos'=>$pedidos,  'costoProduccion'=>$costoProduccion[0] ];      
        
    }

    public function reportesDonativos(Request $request){        
        $anioActual=$request->anio;
        $mesInicio=0;
        $mesFin=1;        
        $tipo=$request->tipo;
       
        if($tipo=='mensual'){
           
						$donativos= DB::table('pan_solicitudes')
            ->where('estatus_id','5')
            ->where('folio','like','PAN-DON%')
            ->whereMonth('fecha_entrega',$request->mes)
            ->whereYear('fecha_entrega',$anioActual)
            ->select(DB::raw('count(id) as donativos'),'fecha_entrega',DB::raw('sum(total) total'))
            ->groupBy('fecha_entrega')
            ->get();
   
            $costoProduccion=DB::table('pan_producciones as pro')
            ->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
            ->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
            ->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
            ->where('s.folio','like','%PAN-DON%')
            ->where('s.estatus_id','5')
            ->whereYear('fecha_elaboracion',$anioActual)
            ->whereMonth('fecha_elaboracion',$request->mes)             
            ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
            ->get(); 
            return ['donativos'=>$donativos,  'costoProduccion'=>$costoProduccion[0] ];       
        }
        if($tipo=='bimestral'){
            switch ($request->bimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=2;
                    break;
                case '2':
                $mesInicio=3;
                $mesFin=4;
                    break;
                case '3':
                $mesInicio=5;
                $mesFin=6;
                    break;
                case '4':
                $mesInicio=7;
                $mesFin=8;
                    break;
                case '5':
                $mesInicio=9;
                $mesFin=10;
                    break;
                case '6':
                $mesInicio=11;
                $mesFin=12;
                    break;                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='trimestral'){
            switch ($request->trimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=3;
                    break;
                case '2':
                $mesInicio=4;
                $mesFin=6;
                    break;
                case '3':
                $mesInicio=7;
                $mesFin=9;
                    break;
                case '4':
                $mesInicio=10;
                $mesFin=12;
                    break;
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='semestral'){
            switch ($request->semestre) {
                case '1':
                $mesInicio=1;
                $mesFin=6;
                    break;
                case '2':
                $mesInicio=7;
                $mesFin=12;
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='anual'){
            $mesInicio=1;
            $mesFin=12;
        }

        $donativos=DB::table('pan_solicitudes')
        ->where('estatus_id','5')
        ->where('folio','like','PAN-DON%')
        ->whereYear('fecha_entrega',$anioActual)
        ->whereMonth('fecha_entrega','>=',$mesInicio)
        ->whereMonth('fecha_entrega','<=',$mesFin)        
        ->select(DB::raw('count(id) as donativos'),'fecha_entrega',DB::raw('sum(total) total'))
        ->groupBy('fecha_entrega')
        ->get();
       
        $costoProduccion=DB::table('pan_producciones as pro')
        ->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
        ->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
        ->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
        ->where('s.folio','like','%PAN-DON%')
        ->where('s.estatus_id','5')
        ->whereYear('fecha_elaboracion',$anioActual)
        ->whereMonth('fecha_elaboracion','>=',$mesInicio)
        ->whereMonth('fecha_elaboracion','<=',$mesFin) 
        ->select(DB::raw('sum(sal.costo_salida) as costoProduccion'))        
        ->get(); 
        return ['donativos'=>$donativos,  'costoProduccion'=>$costoProduccion[0] ];      
        
		}
		
		public function reportesCursos(Request $request){
			$anioActual=$request->anio;
        $mesInicio=0;
        $mesFin=1;        
        $tipo=$request->tipo;        
				$costoProduccion=[];
				$eventos=[];
        if($tipo=='mensual'){
					$eventos=EventosTaller::with(['participantes','area','beneficio','estatus','localidad','municipio','tipoEvento'])
					->whereHas('area', function($q) {
					// Query the id field in area table
					$q->where('id',2); // '=' is optional
					})
					->whereYear('fechaEvento',$anioActual)
					->whereMonth('fechaEvento',$request->mes)        	
					->orderBy('id','DESC')->get();  
          return $eventos;         
        }
        if($tipo=='bimestral'){
            switch ($request->bimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=2;
                    break;
                case '2':
                $mesInicio=3;
                $mesFin=4;
                    break;
                case '3':
                $mesInicio=5;
                $mesFin=6;
                    break;
                case '4':
                $mesInicio=7;
                $mesFin=8;
                    break;
                case '5':
                $mesInicio=9;
                $mesFin=10;
                    break;
                case '6':
                $mesInicio=11;
                $mesFin=12;
                    break;                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='trimestral'){
            switch ($request->trimestre) {
                case '1':
                $mesInicio=1;
                $mesFin=3;
                    break;
                case '2':
                $mesInicio=4;
                $mesFin=6;
                    break;
                case '3':
                $mesInicio=7;
                $mesFin=9;
                    break;
                case '4':
                $mesInicio=10;
                $mesFin=12;
                    break;
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='semestral'){
            switch ($request->semestre) {
                case '1':
                $mesInicio=1;
                $mesFin=6;
                    break;
                case '2':
                $mesInicio=7;
                $mesFin=12;
                    break;
                
                default:
                    # code...
                    break;
            }
        }
        if($tipo=='anual'){
            $mesInicio=1;
            $mesFin=12;
        }     

		///------------------------
		$eventos=EventosTaller::with(['participantes','area','beneficio','estatus','localidad','municipio','tipoEvento'])
							->whereHas('area', function($q) {
							// Query the id field in area table
							$q->where('id',2); // '=' is optional
					})
					->whereYear('fechaEvento',$anioActual)
					->whereMonth('fechaEvento','>=',$mesInicio)
					->whereMonth('fechaEvento','<=',$mesFin) 					
					->orderBy('id','DESC')->get();  

			return $eventos;
		}

    public function getAnios()
    {
        return DB::table('pan_ventas as ventas')
            ->select(DB::raw('extract(YEAR FROM ventas.fecha_venta) as anio'))
            ->distinct()
            ->get();
		}
		public function reporteGeneral(Request $request){
			     
			$anioActual=$request->anio;
			$mesInicio=0;
			$mesFin=1;        
			$tipo=$request->tipo;
			
			if($tipo=='mensual'){
					$ventas= DB::table('pan_ventas')->whereMonth('fecha_venta',$request->mes)
					->whereYear('fecha_venta',$anioActual)
					->select(DB::raw('count(id) as ventas'),'fecha_venta',DB::raw('sum(total) total'))
					->groupBy('fecha_venta')
					->get();
					$costoVentas=DB::table('pan_producciones as pro')->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
					->where('pro.folio','like','%PAN-PG%')
					->whereYear('fecha_elaboracion',$anioActual)
					->whereMonth('fecha_elaboracion',$request->mes)             
					->select(DB::raw('sum(sal.costo_salida) as costoVentas'))        
					->get();
					$pedidos= DB::table('pan_solicitudes')
					->where('estatus_id','5')
					->where('folio','like','PAN-PED%')
					->whereMonth('fecha_entrega',$request->mes)
					->whereYear('fecha_entrega',$anioActual)
					->select(DB::raw('count(id) as pedidos'),'fecha_entrega',DB::raw('sum(total) total'))
					->groupBy('fecha_entrega')
					->get();   
					$costoPedidos=DB::table('pan_producciones as pro')
					->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
					->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
					->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
					->where('s.folio','like','%PAN-PED%')
					->where('s.estatus_id','5')
					->whereYear('fecha_elaboracion',$anioActual)
					->whereMonth('fecha_elaboracion',$request->mes)             
					->select(DB::raw('sum(sal.costo_salida) as costoPedidos'))        
					->get();
					$donativos= DB::table('pan_solicitudes')
					->where('estatus_id','5')
					->where('folio','like','PAN-DON%')
					->whereMonth('fecha_entrega',$request->mes)
					->whereYear('fecha_entrega',$anioActual)
					->select(DB::raw('count(id) as donativos'),'fecha_entrega',DB::raw('sum(total) total'))
					->groupBy('fecha_entrega')
					->get();   
					$costoDonativos=DB::table('pan_producciones as pro')
					->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
					->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
					->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
					->where('s.folio','like','%PAN-DON%')
					->where('s.estatus_id','5')
					->whereYear('fecha_elaboracion',$anioActual)
					->whereMonth('fecha_elaboracion',$request->mes)             
					->select(DB::raw('sum(sal.costo_salida) as costoDonativos'))        
					->get();  	
					$costos=DB::table('pan_devoluciones as dev') 
					->select(DB::raw('sum(dev.costosoperacion) as costos')) 
					->get();				
					
					return [
						'costos'=>$costos,
						'ventas'=>$ventas,
						'donativos'=>$donativos,
						'pedidos'=>$pedidos,  
						'costoVentas'=>$costoVentas[0],
						'costoDonativos'=>$costoDonativos[0],
						'costoPedidos'=>$costoPedidos[0] ];         
			}
			if($tipo=='bimestral'){
					switch ($request->bimestre) {
							case '1':
							$mesInicio=1;
							$mesFin=2;
									break;
							case '2':
							$mesInicio=3;
							$mesFin=4;
									break;
							case '3':
							$mesInicio=5;
							$mesFin=6;
									break;
							case '4':
							$mesInicio=7;
							$mesFin=8;
									break;
							case '5':
							$mesInicio=9;
							$mesFin=10;
									break;
							case '6':
							$mesInicio=11;
							$mesFin=12;
									break;                
							default:
									# code...
									break;
					}
			}
			if($tipo=='trimestral'){
					switch ($request->trimestre) {
							case '1':
							$mesInicio=1;
							$mesFin=3;
									break;
							case '2':
							$mesInicio=4;
							$mesFin=6;
									break;
							case '3':
							$mesInicio=7;
							$mesFin=9;
									break;
							case '4':
							$mesInicio=10;
							$mesFin=12;
									break;
							default:
									# code...
									break;
					}
			}
			if($tipo=='semestral'){
					switch ($request->semestre) {
							case '1':
							$mesInicio=1;
							$mesFin=6;
									break;
							case '2':
							$mesInicio=7;
							$mesFin=12;
									break;
							default:
									break;
					}
			}
			if($tipo=='anual'){
					$mesInicio=1;
					$mesFin=12;
			}
			$ventas=DB::table('pan_ventas')
			->whereYear('fecha_venta',$anioActual)
			->whereMonth('fecha_venta','>=',$mesInicio)
			->whereMonth('fecha_venta','<=',$mesFin)        
			->select(DB::raw('count(id) as ventas'),'fecha_venta',DB::raw('sum(total) total'))
			->groupBy('fecha_venta')
			->get();
			$costoVentas=DB::table('pan_producciones as pro')->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
			->where('pro.folio','like','%PAN-PG%')
			->whereYear('fecha_elaboracion',$anioActual)
			->whereMonth('fecha_elaboracion','>=',$mesInicio)
			->whereMonth('fecha_elaboracion','<=',$mesFin)  
			->select(DB::raw('sum(sal.costo_salida) as costoVentas'))        
			->get();     
			$pedidos=DB::table('pan_solicitudes')
			->where('estatus_id','5')
			->where('folio','like','PAN-PED%')
			->whereYear('fecha_entrega',$anioActual)
			->whereMonth('fecha_entrega','>=',$mesInicio)
			->whereMonth('fecha_entrega','<=',$mesFin)        
			->select(DB::raw('count(id) as pedidos'),'fecha_entrega',DB::raw('sum(total) total'))
			->groupBy('fecha_entrega')
			->get();
			$costoPedidos=DB::table('pan_producciones as pro')
			->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
			->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
			->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
			->where('s.folio','like','%PAN-PED%')
			->where('s.estatus_id','5')
			->whereYear('fecha_elaboracion',$anioActual)
			->whereMonth('fecha_elaboracion','>=',$mesInicio)
			->whereMonth('fecha_elaboracion','<=',$mesFin)       
			->select(DB::raw('sum(sal.costo_salida) as costoPedidos'))        
			->get();   
			$donativos=DB::table('pan_solicitudes')
			->where('estatus_id','5')
			->where('folio','like','PAN-DON%')
			->whereYear('fecha_entrega',$anioActual)
			->whereMonth('fecha_entrega','>=',$mesInicio)
			->whereMonth('fecha_entrega','<=',$mesFin)        
			->select(DB::raw('count(id) as donativos'),'fecha_entrega',DB::raw('sum(total) total'))
			->groupBy('fecha_entrega')
			->get();
			$costoDonativos=DB::table('pan_producciones as pro')
			->join('pan_produccionessolicitudes as ps', 'pro.id','=','ps.produccion_id')
			->join('pan_salidasinsumo as sal','pro.id','=','sal.produccion_id')
			->join('pan_solicitudes as s', 's.id','=','ps.solicitud_id')
			->where('s.folio','like','%PAN-DON%')
			->where('s.estatus_id','5')
			->whereYear('fecha_elaboracion',$anioActual)
			->whereMonth('fecha_elaboracion','>=',$mesInicio)
			->whereMonth('fecha_elaboracion','<=',$mesFin) 
			->select(DB::raw('sum(sal.costo_salida) as costoDonativos'))        
			->get(); 
			$costos=DB::table('pan_devoluciones as dev') 
			->select(DB::raw('sum(dev.costosoperacion) as costos')) 
			->get();
			return [
				'costos'=>$costos,
				'ventas'=>$ventas,
				'donativos'=>$donativos,
				'pedidos'=>$pedidos,  
				'costoVentas'=>$costoVentas[0],
				'costoDonativos'=>$costoDonativos[0],
				'costoPedidos'=>$costoPedidos[0] ];         
		/* 	$pdf = PDF::loadView(
				'panaderia::reportes.general.reporteGeneral',
				[
					'ventas'=>$ventas,
					'donativos'=>$donativos,
					'pedidos'=>$pedidos, 
					'costoVentas'=>$costoVentas[0],
					'costoDonativos'=>$costoDonativos[0],
					'costoPedidos'=>$costoPedidos[0]         
				]
			);
			return $pdf->stream(); */
	}
	public function reportePdf(Request $request)
    {
        
        $pdf = PDF::loadView(
          'panaderia::reportes.general.reporteGeneral',
          [            
						'ventas' => $request->ventas, 
						'pedidos' => $request->pedidos, 
						'donativos' => $request->donativos, 
						'costoVentas' => $request->costoVentas, 
						'costoPedidos' => $request->costoPedidos, 
						'costoDonativos' => $request->costoDonativos,
						'ingresosPedidos'=>$request->ingresosPedidos,
						'ingresosVentas'=>$request->ingresosPedidos,		
						'gananciasVentas'=>$request->gananciasVentas,
						'gananciasPedidos'=>$request->gananciasPedidos,
						'ganancias'=>$request->ganancias,
						'costoProduccion'=>$request->costoProduccion,
						'periodo' => $request->periodo,
						'costos' => $request->costos
          ]
        );

        
        return $pdf->stream();
    }
}
