<?php
namespace Modules\Panaderia\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Solicitud;
use Modules\Panaderia\Entities\Pedido;
use Modules\Panaderia\Entities\Cliente;
use Modules\Panaderia\Entities\Persona;
use Modules\Panaderia\Entities\PagoPedido;
use Modules\Panaderia\Entities\Devolucion;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;

class PedidosController extends Controller
{
 public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO,VENDEDOR',
        ['only' => ['store']]);
    } 
    public function index(Request $request)
    {
        $pedidosPendientes = Solicitud::has('pedidos')->where('estatus_id','9')->count();  
        $pedidosNoPagados = Pedido::where('pagado','0')->get();     
        $pedidos=Pedido::with('solicitud','cliente','pagos')->paginate($request->per_page);
        return [
            'pedidos'=>$pedidos,
            'pedidosPendientes'=>$pedidosPendientes,
            'pedidosNoPagados'=>$pedidosNoPagados
        ];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {      
      try {
            DB::beginTransaction();  
        $solicitud = new Solicitud();
        $solicitud->folio = '-';
        $solicitud->estatus_id = '9';
        $solicitud->fecha_solicitud = date("Y-m-d");        
        $solicitud->fecha_entrega =Carbon::createFromFormat('d-m-Y',$request->fecha_entrega);
        $solicitud->total=$request->total;
        $solicitud->usuario_id=auth()->user()->id;;
        $solicitud->save();   
        $solicitud->folio='PAN-PED-'.$solicitud->id.'-'.date("Y");
        $solicitud->save();  
        $rpedido = (object)$request->pedido;
        $pedido = new Pedido();           
        $pedido->solicitud_id=$solicitud->id;
        $pedido->cliente_id=$request->cliente; 
        $pedido->pagado=$rpedido->pagado;  
        $pedido->calle=strtoupper($rpedido->calle);
        $pedido->numero_exterior=$rpedido->numext;
        $pedido->numero_interior=$rpedido->numint;
        $pedido->colonia=strtoupper($rpedido->colonia);
        $pedido->localidad_id=$request->localidad;
        $pedido->referencia_domicilio=strtoupper($rpedido->referencia);           
        $pedido->usuario_id=auth()->user()->id;;
        $pedido->save();            
        $detalles = $request->lista;
        foreach ($detalles as $detalle) {
            $producto=(object)$detalle;
            $solicitud->detalles()->attach($producto->id,[
                'cantidad'=>$producto->cantidad,
                'precio_unitario'=>$producto->precio,
                'subtotal'=>$producto->subtotal,
                'usuario_id'=>auth()->user()->id]); 
        }         
        $pago = new PagoPedido();
        $pago->pedido_id = $pedido->id;
        $pago->fecha = date("Y-m-d"); 
        $pago->pago = $request->pago;
        $pago->save();
        DB::commit();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_solicitudes',
                'registro' => $solicitud->id. '',
                'campos' => json_encode($solicitud) . '',
                'metodo' => $request->method()    
            ]);
            $usuario->bitacora($request, [
                'tabla' => 'pan_pedidos',
                'registro' => $pedido->id. '',
                'campos' => json_encode($pedido) . '',
                'metodo' => $request->method()    
            ]);
            $usuario->bitacora($request, [
                'tabla' => 'pan_pagos_pedidos',
                'registro' => $pago->id. '',
                'campos' => json_encode($pago) . '',
                'metodo' => $request->method()    
            ]);

        return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'id'=>$pedido->id], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        } 
        
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $pedido=Pedido::findOrFail($id);
        $pedido->solicitud;
        $pedido->cliente;
        $pedido->localidad;
        $pedido->pagos;
        $pedido->devolucion;
        return $pedido;   
    }

    public function listaPedidos($id){
        //$miUsuario=Usuario::find($id);
        
        $misPedidos=Persona::find($id);
        $misPedidos->pedidos;
        return $misPedidos;
    }

    public function cancelar(Request $request){
        $devolucion =  new Devolucion();
        $devolucion->pedido_id=$request->id;
        $devolucion->fecha=date("Y-m-d");
        $devolucion->montodevolucion=$request->devolucion;
        $devolucion->costosoperacion=$request->costo;
        $devolucion->observaciones=$request->observaciones;
        $devolucion->save();
       /*  try {
            DB::beginTransaction(); 

            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'id'=>$pedido->id], 200);        
            }
            catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            } 
             */

    }
    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $pedido=Pedido::findOrfail($id); 
        $pedido->pagado= '1'; 
        $pedido->save();  
        $pago = new PagoPedido();
        $pago->pedido_id=$id;
        $pago->fecha = date("Y-m-d"); 
        $pago->pago=$request->pago;
        $pago->save();
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarCliente(Request $request){
        $columns = ['id','nombre','primer_apellido','segundo_apellido','numero_local','numero_celular','localidad_id'];
        $term = $request->curp;
        $words_search = explode(" ",$term);
        $clientes=Persona::where(function ($query) use ($columns,$words_search) {
                foreach ($words_search as $word) {
                    $query = $query->where(function ($query) use ($columns,$word) {
                        foreach ($columns as $column) {
                            $query->orWhere($column,'like',"%$word%");
                        }
                    });
                }
            });
            $clientes = $clientes->paginate(6);   
        return $clientes;
    }
    public function buscarPedido($folio){
        $solicitud=Solicitud::where('folio',$folio)->first();
        $pedido = Pedido::with('solicitud','cliente','pagos','devolucion')->where('solicitud_id',$solicitud->id)->get();
        
        
        return $pedido;
    }

    public function mostrarpdf($id)
    {
        $anticipo=0;
        $pedido=Pedido::find($id);
        $cliente=$pedido->cliente;
        $solicitud=$pedido->solicitud;
        $productos=$pedido->solicitud->detalles;
        $pagos=$pedido->pagos;
        foreach ($pagos as $pago) {
            $anticipo += $pago->pago;
        } 
            $pdf = PDF::loadView(
                'panaderia::reportes.pedidos.productossolicitados',
                [                    
                    "solicitud"=>$solicitud,
                    "productos"=>$productos,
                    "cliente"=>$cliente,
                    "pedido"=>$pedido,
                    "anticipo"=>$anticipo
                  
                ]                
            );
            $pdf->setPaper("letter");
            return $pdf->stream('productossolicitados.pdf');
    }
    public function ticket($id){
        $anticipo=0;
        $pedido=Pedido::find($id);
        $cliente=$pedido->cliente;
        $solicitud=$pedido->solicitud;
        $productos=$pedido->solicitud->detalles;
        $pagos=$pedido->pagos;
        foreach ($pagos as $pago) {
            $anticipo += $pago->pago;
        } 
        return View('panaderia::reportes.pedidos.ticketPedido',
        ["solicitud"=>$solicitud,
        "productos"=>$productos,
        "cliente"=>$cliente,
        "anticipo"=>$anticipo,
        'pedido'=>$pedido]);
        return [ 
            "solicitud"=>$solicitud,
            "productos"=>$productos,
            "cliente"=>$cliente,
            "pedido"=>$pedido
        ];
    }
    public function reportePdf(Request $request)
    {
        
        $pdf = PDF::loadView(
          'panaderia::reportes.pedidos.reportePdf',
          [
            'lineal' => $request->lineal,
            'pastel' => $request->pastel, 
            'pedidos' => $request->pedidos, 
            'costo' => $request->costo,
            'ganancia' => $request->ganancia, 
            'periodo' => $request->periodo
          ]
        );

        
        return $pdf->stream();
    }
}
