<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Modules\Panaderia\Entities\Producto;
use Modules\Panaderia\Entities\Tipoproducto;
use Illuminate\Support\Facades\DB;

class ProductosController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update' ,'show','destroy']]);
        $this->middleware('rolModuleV2:panaderia,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store','update', 'destroy']]);
    }
    //se traen todos los productos que estan activos    
    public function index()
    {   
        $tipos=Tipoproducto::all();  
        $productos=Producto::with('tipo')->where('activo','1')
        ->where('solicitado','1')
        ->has('receta')->get();               
        
        return [            
            /* 'pagination' => [
                'total'         => $productos->total(),
                'current_page'  => $productos->currentPage(),
                'per_page'      => $productos->perPage(),
                'last_page'     => $productos->lastPage(),
                'from'          => $productos->firstItem(),
                'to'            => $productos->lastItem(),
            ], */
            'productos' => $productos,
            'tipos'=>$tipos,
            
        ];               
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Request $request)
    {   
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $producto = new Producto();
            $producto->nombre = strtoupper($request->nombre);
            $producto->tipoproducto_id = $request->tipoproducto_id;
            $producto->precio = $request->precio;
            $producto->stock = $request->stock;
            $producto->activo = $request->activo;  
            $producto->solicitado=$request->solicitado;
            $producto->descripcion=strtoupper($request->descripcion);
            $producto->usuario_id =auth()->user()->id;
            $producto->save();    
            if($request->file('image')){
                $file=$request->file('image');
                $path = $file->store('public/panaderia');
                $path = str_replace('public', 'storage', $path);
                $producto->fill(['image'=>$path])->save();
            }
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_productos',
                'registro' => $producto->id . '',
                'campos' => json_encode($request) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'producto'=>$producto], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }                 
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $producto=Producto::findOrFail($id);
        $producto->tipo;
        return $producto;  
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        try{
            DB::beginTransaction();
            $producto =Producto::findOrfail($id);      
            $producto->update($request->all());
            if($request->file('image')){
                $file=$request->file('image');
                $path = $file->store('public/panaderia');
                $path = str_replace('public', 'storage', $path);
                $producto->fill(['image'=>$path])->save();                
            }
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_productos',
                'registro' => $producto->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'producto'=>$producto], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
            $producto =Producto::findOrFail($id);
            $producto->delete();
            DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }        
    }
    //metodo para traer todos los productos esten activos o inactivos
    public function getTodos(){
        $tipos=Tipoproducto::all();     
        $productos=Producto::with('tipo')->get();   
        return [            
            /* 'pagination' => [
                'total'         => $productos->total(),
                'current_page'  => $productos->currentPage(),
                'per_page'      => $productos->perPage(),
                'last_page'     => $productos->lastPage(),
                'from'          => $productos->firstItem(),
                'to'            => $productos->lastItem(),
            ], */
            'productos' => $productos,
            'tipos'=>$tipos,
            
        ];   
    }
    public function sinReceta(){
        $tipos=Tipoproducto::all();  
        $productos=Producto::with('tipo')->where('activo','1')
        ->where('solicitado','1')
        ->doesnthave('receta')
        ->get();      
              
        
        return [            
            /* 'pagination' => [
                'total'         => $productos->total(),
                'current_page'  => $productos->currentPage(),
                'per_page'      => $productos->perPage(),
                'last_page'     => $productos->lastPage(),
                'from'          => $productos->firstItem(),
                'to'            => $productos->lastItem(),
            ], */
            'productos' => $productos,
            'tipos'=>$tipos,
            
        ]; 
    }
    //productos para ventas
    public function productosVentas()
    {   
        $productos=Producto::with('tipo')->where('activo','1')
        ->where('solicitado','1')->where('stock','>','0')
        ->has('receta')->get();               
        
        return [                       
            'productos' => $productos
        ];               
    }
}