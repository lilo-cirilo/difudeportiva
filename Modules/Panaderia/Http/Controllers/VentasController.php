<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Venta;
use Modules\Panaderia\Entities\Producto;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','show']]);
        $this->middleware('rolModuleV2:panaderia,VENDEDOR,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store','ticket']]);
    }
    public function index(Request $request)
    {
       $ventas=Venta::orderBy('id', 'DESC')->paginate($request->per_page);
       return $ventas;     
       
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */    
    public function store(Request $request)
    {        
        try{
            DB::beginTransaction();
            $venta= new Venta();        
            $venta->folio='-';
            $venta->total=$request->total;
            $venta->fecha_venta=date("Y-m-d");
            $venta->cantidad_articulos=$request->totalProductos;
            $venta->usuario_id=auth()->user()->id;
            $venta->save();
            $venta->folio='PAN-VTA-'.$venta->id.'-'.date("Y");
            $venta->save();

            $lista=$request->lista;        
            //ciclo para agregar los detalles de la venta
            foreach ($lista as $key => $producto) {
                //se agregan los productos al detalle de venta
                $value=(object)$producto;
                $venta->detalles()->attach($value->id,[
                    'cantidad'=>$value->cantidad,
                    'precio_unitario'=>$value->precioU,
                    'subtotal'=>$value->subtotal,
                    'usuario_id'=>'1']); 
                //se descuentan los productos de stock de la panaderia
                $productoPan=Producto::find($value->id);
                $productoPan->stock=($productoPan->stock-$value->cantidad);
                $productoPan->save();
            }
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_ventas',
                'registro' => $venta->id . '',
                'campos' => json_encode($request->lista) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'id'=>$venta->id,'folio'=>$venta->folio], 200);
        }                        
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $venta=Venta::findOrFail($id);
        $venta->detalles;      
        return $venta;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarVenta($folio){
        $resultado=Venta::where('folio',$folio)->first();
        return $resultado;
    }
    public function ticket($id){
        $venta=Venta::with('detalles')->find($id);
        return View('panaderia::reportes.ventas.ticketVenta',['venta'=>$venta]);
        return $venta;
    }
    public function pdfReporte(Request $request){
       $pdf=PDF::loadView('panaderia::reportes.ventas.ventas',['grafica1'=>$request->grafica1,
       'grafica2'=>$request->grafica2,'ventas'=>$request->ventas,'costo'=>$request->costo,
       'ganancia'=>$request->ganancia,'periodo'=>$request->periodo]);
        $pdf->setPaper("letter");
            return $pdf->download('salidaIsumos.pdf');
    }
}
