<?php

namespace Modules\Panaderia\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Panaderia\Entities\Equipo;
use Modules\Panaderia\Entities\Statusequipos;
use Illuminate\Support\Facades\DB;

class EquiposController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    /**
     * Class constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('panaderia', ['only' => ['store','update' ,'show','destroy']]);
        $this->middleware('rolModuleV2:panaderia,ALMACEN,ADMINISTRADOR,ENCARGADO',
        ['only' => ['store','update','show', 'destroy']]);
    }
    public function index()
    {           
        $equipos=Equipo::with('estatus')->get();   
        $estatus=Statusequipos::get();
        return [                       
            'equipos' => $equipos,
            'estatus'=>$estatus,
        ];               
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('panaderia::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {       
        try {
            DB::beginTransaction();
        $equipo= new Equipo();
        $equipo->nombre=strtoupper($request->nombre);
        $equipo->marca=strtoupper($request->marca);
        $equipo->modelo=strtoupper($request->modelo);
        $equipo->descripcion=strtoupper($request->descripcion);
        $equipo->numero_serie=strtoupper($request->numero_serie);
        $equipo->precio=$request->precio;
        $equipo->estatusequipo_id=$request->estatusequipo_id;
        $equipo->inventario=strtoupper($request->inventario);
        $equipo->usuario_id=auth()->user()->id;
        $equipo->save();
        DB::commit();
        $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_equipos',
                'registro' => $equipo->id . '',
                'campos' => json_encode($request) . '',
                'metodo' => $request->method()    
            ]);
        return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'inventario'=>$equipo->inventario], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $equipo=Equipo::findOrFail($id);
        return $equipo;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('panaderia::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {      
        try{
            DB::beginTransaction();
            $equipo =Equipo::findOrfail($id);      
            $equipo->update($request->all());
            DB::commit();
            $usuario = auth()->user() ? auth()->user() : \App\Models\Usuario::find($request->usuario_id);
            $usuario->bitacora($request, [
                'tabla' => 'pan_equipos',
                'registro' => $equipo->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => $request->method()    
            ]);
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true,'inventario'=>$equipo->inventario], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        try{
            DB::beginTransaction();
        $equipo =Equipo::findOrFail($id);
        $equipo->delete();
        DB::commit();
            return response()->json(['success' => true, 'code' => 200, 'estatus' => true], 200);        
        }
        catch(\Exception $e) {
            DB::rollBack();
            return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }
}
