<?php

namespace Modules\Panaderia\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Firebase\JWT\JWT;
use Illuminate\Http\JsonResponse;

class VerifyPanaderiaToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $jwt = $request->header('Pan-Token');
        if(!$jwt && !$request->token){
            return new JsonResponse(['message' => 'Hace falta el token de panaderia'], 401);
        }
        try{
            if($jwt){
                $inf = JWT::decode($jwt, config('app.jwt_panaderia_token'), ['HS256']);
            }else{
                $inf = JWT::decode($request->token, config('app.jwt_panaderia_token'), ['HS256']);
            }
            $request['usuario_id'] = $inf->usuario_id;
            $request['modulo_id'] = $inf->modulo_id;
            $request['rol_id'] = $inf->rol_id;
            return $next($request);
        } catch(\UnexpectedValueException $e){
            return new JsonResponse(['message' => 'Token incorrecto'], 403);
        }
    }
}
