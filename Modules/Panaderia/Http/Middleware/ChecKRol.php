<?php

namespace Modules\Panaderia\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ChecKRol
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $modulo_id = array_slice(func_get_args(), 2, 1);
        $roles = array_slice(func_get_args(), 3);
        if(! Usuario::findOrFail($request->usuario_id)->hasRolesModulo($roles, $modulo_id[0])){
            return new JsonResponse(['msg' => 'No tienes acceso'], 403);
        }
        return $next($request);
    }
}
