<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statusequipos extends Model
{
    use SoftDeletes;
    protected $table = 'pan_cat_estatusequipos';

    protected $fillable = ['id','nombre'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function equipos(){
        return $this->hasMany(Equipo::class,'estatusequipo_id','id');
    }
}
