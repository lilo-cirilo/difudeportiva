<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipoempleado extends Model
{
    use SoftDeletes;
    protected $table = 'pan_cat_tiposempleados';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','nombre'];

    public function empleados(){
        return $this->hasMany('Modules\Panaderia\Entities\Empleado');
    }
}
