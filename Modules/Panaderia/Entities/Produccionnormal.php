<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produccionnormal extends Model
{
    use SoftDeletes;
    protected $table = 'pan_produccionesnormales';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','produccion_id','produccionprogramada_id','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function produccion(){
        return $this->belongsTo(Produccion::class,'produccion_id');
    }

    public function programada(){
        return $this->belongsTo(Produccionprogramada::class,'produccionprogramada_id')->with('estatus','programados');
    }
}
