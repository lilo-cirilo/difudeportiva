<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmpleadoPedido extends Model
{
    use SoftDeletes;
    protected $table = 'pan_empleados_pedidos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','empleado_id','pedido_id','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function empleado(){
        return $this->belongsTo('Modules\Panaderia\Entities\Empleado');
    }

    public function pedido(){
        return $this->belongsTo('Modules\Panaderia\Entities\Pedido');
    }

}
