<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Venta extends Model
{
    use SoftDeletes;    
    protected $table = 'pan_ventas';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','folio','total','fecha_venta','cantidad_articulos','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function empleado(){
        return $this->belongsTo('Modules\Panaderia\Empleado');
    }

    public function detalles(){
        return $this->belongsToMany(Producto::class,'pan_detallesventa','venta_id','producto_id')
        ->withPivot('cantidad','precio_unitario','subtotal','usuario_id');
    }      

}
