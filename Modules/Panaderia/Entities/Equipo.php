<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Equipo extends Model
{
    use SoftDeletes;
    protected $table = 'pan_equipos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['id','nombre','marca','modelo',
                            'descripcion','numero_serie','precio',
                            'estatusequipo_id','inventario','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }

    public function estatus(){
        return $this->belongsTo(Statusequipos::class,'estatusequipo_id');
    }

}
