<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
    use SoftDeletes;
    protected $table = 'pan_pedidos';  
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $appends = ['direccion_completa'];
    protected $fillable = ['id','solicitud_id','cliente_id','pagado',
    'calle','numero_exterior','numero_interior','colonia','localidad_id',
    'referencia_domicilio','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
		}
		public function getDireccionCompletaAttribute() 
    {  
      return $this->calle . ' ' . $this->numero_exterior . ' ' . $this->colonia;  
    }

    public function solicitud(){
        return $this->belongsTo('Modules\Panaderia\Entities\Solicitud')->with('detalles','estatus');
    }

    public function cliente(){
        return $this->belongsTo(Persona::class);
    }

    public function localidad(){
        return $this->belongsTo('App\Models\Localidad');
    }

    public function pagos(){
        return $this->hasMany('Modules\Panaderia\Entities\PagoPedido');        
    }

    public function devolucion(){
        return $this->hasOne('Modules\Panaderia\Entities\Devolucion');        
    }
}
