<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Devolucion extends Model
{
    use SoftDeletes;
    protected $table = 'pan_devoluciones';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','pedido_id','fecha','observaciones','montodevolucion','costosoperacion'];

    public function Pedido(){
        return $this->belongsTo('Modules\Panaderia\Entities\Pedido');        
    }
}
