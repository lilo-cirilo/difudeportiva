<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipoproducto extends Model
{
    use SoftDeletes;
    protected $table = 'pan_cat_tiposproductos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','nombre','usuario_id'];

    public function productos(){
        return $this->hasMany(Producto::class,'tipoproducto_id','id');
    }
}
