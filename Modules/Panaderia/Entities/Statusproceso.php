<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Statusproceso extends Model
{
    protected $table = "cat_statusprocesos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["status","descripcion"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function statussolicitudes(){
        return $this->belongsToMany(BeneficioprogramaSolicitud::class,'estados_solicitudes','statusproceso_id','beneficioprograma_solicitud_id')
        ->withPivot('usuario_id');
    }

    public function status(){
        return $this->belongsTo(SolicitudGeneral::class,'solicitud_id')->with('beneficiossolicitados');        
    }
}
