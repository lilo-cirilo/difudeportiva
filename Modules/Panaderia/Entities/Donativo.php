<?php

namespace Modules\Panaderia\Entities;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donativo extends Model
{
    use SoftDeletes;
    protected $table = 'pan_donativos';    
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    protected $fillable = ['id','solicitud_id','solicitudgeneral_id','justificacion','usuario_id'];

    public function usuario(){//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario');
    }
    
    public function solicitud(){
        return $this->belongsTo('Modules\Panaderia\Entities\Solicitud')->with('detalles','estatus');
    }

    public function solicitudGeneral(){
        return $this->belongsTo(solicitudGeneral::class, 'solicitudgeneral_id')->with('beneficiossolicitados','tiposremitente');
    }        

    public function donativoBeneficiarios(){
        return $this->belongsToMany(Persona::class,'pan_beneficiarios_donativos','donativo_id','persona_id')
        ->withPivot('usuario_id');
    }

    public function donativoDependencias(){
        return $this->belongsToMany(Dependencia::class,'pan_instituciones_donativos','donativo_id','institucion_id')
        ->withPivot('usuario_id');
    }

    public function donativoRegiones(){
        return $this->belongsToMany(Region::class,'pan_donativos_regiones','donativo_id','region_id')
        ->withPivot('usuario_id');
    }

    public function donativoMunicipios(){
        return $this->belongsToMany(Municipio::class,'pan_donativos_municipios','donativo_id','municipio_id')
        ->withPivot('usuario_id');
    }

}