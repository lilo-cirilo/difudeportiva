<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Beneficioprograma extends Model
{
    use SoftDeletes;
  
    protected $table = 'beneficiosprogramas';    
    protected $dates = ['deleted_at'];    
    protected $fillable = ['anio_programa_id', 'nombre', 'predeterminado', 'usuario_id'];              
        
    public function aniosprograma() {
      return $this->belongsTo('App\Models\AniosPrograma');
    }

    public function beneficiossolicitados(){
        return $this->belongsToMany(SolicitudGeneral::class,'beneficiosprogramas_solicitudes','beneficioprograma_id','solicitud_id')
        ->withPivot('cantidad','folio','usuario_id');
    }
    
    public function usuario() {
        return $this->belongsTo('App\Models\Usuario');
    }  
    
}
