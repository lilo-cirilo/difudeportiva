<?php

namespace Modules\Panaderia\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presentacion extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_presentacions'; 
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $fillable = ['id','presentacion'];

}
