@extends('panaderia::layouts.Admin_master')
@section('content')  
      
      <div id="app">                 
          <transition name="fade" mode="out-in">
              <router-view></router-view>
          </transition>              
      </div>        
    
@stop
@section('miscript')
@include('panaderia::js.jsToast')
@stop