<script>
  function block() {
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            backgroundColor: 'none',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .8,
            color: '#fff',
        },
        baseZ: 10000,
        message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
    });

    function unblock_error() {
        if($.unblockUI())
            alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
    }
}
function unblock() {
    $.unblockUI();
}
</script>