@extends('panaderia::layouts.master')

@section('content')
    <div id="app">
        <h1>Hello World</h1>

        <p>
            This view is loaded from module: {!! config('panaderia.name') !!}
        </p>
    </div>
@stop


@section('miscript')
<script>
console.log('hola');
</script>
@stop
