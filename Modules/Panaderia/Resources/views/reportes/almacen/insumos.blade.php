<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Existencia de insumos</title>
    <link rel="stylesheet" href="style.css" media="all" />
  </head>
  <body>
      <?php
      setlocale (LC_TIME, "es_MX");
      setlocale (LC_TIME, "es_MX");
				$meses = array("ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE");

        $fecha=date('d')." DE ".$meses[date('n')-1]. " DE ".date('Y') ;
      ?>  
    <header class="clearfix">
      <div id="logo">
         <img src={{asset('images/logo_header.png')}}>
      </div>
      <div id="company">
        <div>SISTEMA PARA EL DESARROLLO INTEGRAL DEL ESTADO DE OAXACA</div>
        <div>DIRECCIÓN DE ADMINISTRACIÓN Y FINANZAS</div>
        <div>TALLER DE PANADERÍA</div>
        <div>OAXACA DE JUÁREZ A {{$fecha}}</div>
      </div>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div style="font-weight:bold" class="invoice">INSUMOS DEL TALLER DE PANADERÍA</div>
       </div>           
        <br>
      </div>
      <table id="tablita" style="border-collapse:collapse;border-color:#ddd;" border="1" cellpadding="0" cellspacing="1" bordercolor="#000000">    
				<thead>
					<tr style="background-color: lightgray;">
								<th>INSUMO</th>
								<th>FOLIO DE COMPRA</th>
								<th>CADUCIDAD</th>
								<th>LOTE</th>
								<th>STOCK</th>
								<th>PRESENTACIÓN</th>
								<th>PRECIO UNITARIO</th>
							</tr>
				</thead>
        <tbody>    
        @foreach ($insumos as $item)        
        <tr>
					<td>{{$item->insumo['producto']}}</td>
					<td>{{$item->folio_ordencompra}}</td>
					<td>{{date("d/m/Y", strtotime($item->fecha_caducidad))}}</td>
					<td>{{$item->lote}}</td>
					<td>{{$item->cantidad}}</td>
					<td>{{$item->presentacion['presentacion']}}</td>
					<td>${{$item->precio_unitario}}</td>
        </tr>
        @endforeach         
        </tbody>
      </table>          
    </main>
    {{-- <footer style="bottom:40px; position:fixed;">
       <img class="img-fluid mt-2 footer" style="margin-bottom:50px ; width: 100%;" src={{asset('images/logo_footer.png')}}>
    </footer> --}}
  </body>
</html>
<style>
  @font-face {
  font-family: SourceSansPro;
  src: url(SourceSansPro-Regular.ttf);
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #0087C3;
  text-decoration: none;
}

body {
  position: relative;
  width: 20cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 14px; 
  font-family: Verdana, Arial, sans-serif

}

header {
  padding: 10px 0;
  margin-bottom: 20px;
  border-bottom: 1px solid #AAAAAA;
}

#logo {
  float: left;
  margin-top: 8px;
}

#logo img {
  height: 85px;
}

#company {
 /* font-size: 30px;
/*   float: right;
 */  text-align: right;
}


#details {
  margin-bottom: 20px;
}

#client {
  padding-left: 6px;
  float: left;
}

#client .to {
  color: #777777;
}

 h2.name {
  font-size: 10px;
  font-weight: normal;
  margin: 0;
} 

#invoice {
  padding-right: 6px;
   /*  float: right;*/
  text-align: right;
}

#invoice h1 {
  color: #0087C3;
  font-size: 2.4em;
  line-height: 1em;
  font-weight: normal;
  margin: 0  0 10px 0;
}

#invoice .date {
  font-size: 1.1em;
  color: #777777;
}
/*  */
#thanks{
  font-size: 2em;
  margin-bottom: 50px;
}

#notices{
  padding-left: 6px;
  border-left: 6px solid #0087C3;  
}

#notices .notice {
  font-size: 1.2em;
}
table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 20px;
}

footer {
  color: #777777;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #AAAAAA;
  padding: 8px 0;
  text-align: center;
}
#tablita td, #tablita th {
  border: 1px solid #ddd;
  padding: 8px;
}

</style>