<!DOCTYPE html>
<html>

<head>
  <style>
    
    * {
      font-size: 11px;
      font-family: 'Lucida Console';
    }
    td.producto,
    th.producto {
      width: 80px;
      max-width: 80px;
      text-align: left;
    }
    
    td.cantidad,
    th.cantidad {
      width: 80x;
      max-width: 80px;
      text-align: left;      
    }
    
    td.precio,
    th.precio {
      width: 50px;
      max-width: 50px;
      text-align: left;
     
    }
    td.sub,
    th.sub {
      width: 45px;
      max-width: 45px;
      text-align: left;
      
    }
    .centrado {
      text-align: center;
      align-content: center;
    }
    
    .ticket {
      width: 250px;
      max-width: 250px;
    }
    
    img {
      max-width: inherit;
      width: inherit;
    }
  </style>

</head>

<body>
  <div class="ticket">
    <img src="{{asset('images/miPanaderia.jpg')}}" alt="Logotipo" height="90" width="180">
    <p class="centrado">COMPROBANTE DE PEDIDO
      <br>Mi Panaderia DIF
      <br>Folio: {{$solicitud->folio}} 
      <br>Fecha: {{date("d/m/Y",strtotime($solicitud->fecha_solicitud))}}
      <br>Cliente: {{$cliente->nombre_completo}}
    </p>
    <table>
      <thead>
        <tr>
          <th class="cantidad">Cant.</th>
          <th class="producto">Producto</th>
          <th class="precio">Precio</th>
          <th class="precio">Subt.</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($productos as $item)
        <tr>
            <td class="cantidad">{{$item->pivot->cantidad}}</td>
            <td class="producto">{{$item->nombre}}</td>
            <td class="precio">${{number_format($item->pivot->precio_unitario,2)}}</td>
            <td class="sub">${{number_format($item->pivot->subtotal,2)}}</td>
        </tr>               
        @endforeach  
        <tr>
          <td class="cantidad"></td>
          <td class="producto"></td>
          <td class="precio">Anticipo:</td>
          <td class="sub">${{number_format($anticipo,2)}}</td>
        </tr> 
        <tr>
          <td class="cantidad"></td>
          <td class="producto"></td>
          <td class="precio">Restante:</td>
          <td class="sub">${{number_format(($solicitud->total)-($anticipo),2)}}</td>
        </tr>             
        <tr>
            <td class="cantidad"></td>
            <td class="producto"></td>
            <td class="precio">Total</td>
            <td class="sub">${{number_format($solicitud->total,2)}}</td>
          </tr>
      </tbody>
    </table>    
    <p class="centrado">¡GRACIAS POR SU PEDIDO! 
      <br>Recuerde mostrar su comprobante para recibir su pedido.
      <br>
      <br>Vicente Guerrero 114, Miguel Alemán
      <br>Oaxaca de Juárez</p>
  </div>
</body>
</html>