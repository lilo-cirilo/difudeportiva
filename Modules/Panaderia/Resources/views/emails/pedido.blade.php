<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Nuevo Pedido</title>
</head>
<body>
<p>Hola: {{$persona->nombre}} {{$persona->primer_apellido}}</p>
    <p>Se ha generado su pedido de pan con folio: {{$solicitud->folio}}</p>
    <p>Detalles del Pedido:</p>
    <table>
      <thead>
        <tr>
          <th>Producto</th>
          <th>Cantidad</th>
          <th>Precio</th>
          <th>Subtotal</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($detalles as $producto)
        <tr>
            <td>{{$producto['nombre']}}</td>
            <td>{{$producto['cantidad']}}</td>
            <td>{{$producto['precio']}}</td>
            <td>{{$producto['subtotal']}}</td>            
          </tr>            
        @endforeach        
      </tbody>
    </table>
    <p>Total: </p>
    <p>Gracias por su compra :)</p>
</body>
</html>