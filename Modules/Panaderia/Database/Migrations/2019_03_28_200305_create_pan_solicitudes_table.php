<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de las solicitudes de donativos y pedidos del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_solicitudes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->unsignedInteger('estatus_id');
            $table->date('fecha_solicitud');
            $table->date('fecha_entrega');
            $table->float('total',8,2);
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_solicitudes');
    }
}
