<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_productos', function (Blueprint $table) {
            $table->foreign('tipoproducto_id')->references('id')->on('pan_cat_tiposproductos');
           // $table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_productos', function (Blueprint $table) {
            $table->dropForeign(['tipoproducto_id']);
            //$table->dropForeign(['usuario_id']);            
            $table->dropIndex('pan_productos_tipoproducto_id_foreign');
            //$table->dropIndex('pan_productos_usuario_id_foreign'); 
        });
    }
}
