<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Yulissa
  MOTIVO: Registro de los productos que se vendieron de acuerdo a una venta.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanDetallesventaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_detallesventa', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('venta_id');
            $table->unsignedInteger('producto_id');
            $table->integer('cantidad');
            $table->float('precio_unitario',8,2);
            $table->float('subtotal',8,2);

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_detallesventa');
    }
}
