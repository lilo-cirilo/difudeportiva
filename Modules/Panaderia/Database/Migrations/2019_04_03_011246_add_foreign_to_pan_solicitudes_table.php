<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_solicitudes', function (Blueprint $table) {
            $table->foreign('estatus_id')->references('id')->on('pan_cat_estatus');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_solicitudes', function (Blueprint $table) {
            $table->dropForeign(['estatus_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_solicitudes_estatus_id_foreign');
            //$table->dropIndex('pan_solicitudes_usuario_id_foreign');
        });
    }
}
