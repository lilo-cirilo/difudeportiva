<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la información detallada de los pedidos Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_pedidos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solicitud_id')->unique();
            $table->unsignedInteger('cliente_id');
            $table->boolean('pagado');
            $table->string('calle');
            $table->string('numero_exterior');
            $table->string('numero_interior')->nullable();
            $table->string('colonia');
            $table->integer('localidad_id')->unsigned()->nullable();
            $table->string('referencia_domicilio');            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_pedidos');
    }
}
