<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de las producciones que se llevan a cabo en el Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanProduccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_producciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->date('fecha_elaboracion');
            $table->text('observacion')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_producciones');
    }
}
