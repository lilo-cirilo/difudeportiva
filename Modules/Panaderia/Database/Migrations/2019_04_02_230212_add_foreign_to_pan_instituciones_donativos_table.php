<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanInstitucionesDonativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_instituciones_donativos', function (Blueprint $table) {
            $table->foreign('donativo_id')->references('id')->on('pan_donativos');
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_instituciones_donativos', function (Blueprint $table) {
            $table->dropForeign(['donativo_id']);
            $table->dropForeign(['institucion_id']);            
            //$table->dropForeign(['usuario_id']);       
            $table->dropIndex('pan_instituciones_donativos_donativo_id_foreign');  
            $table->dropIndex('pan_instituciones_donativos_institucion_id_foreign');  
            //$table->dropIndex('pan_dependencias_donativos_usuario_id_foreign');  
        });
    }
}
