<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Para el registro de los insumos que se utilizan en el Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanInsumosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_insumos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('insumo_id');
            $table->unsignedInteger('presentacion_id');
            $table->string('folio_ordencompra');
            $table->float('cantidad_entrada',8,3);
            $table->date('fecha_caducidad');
            $table->string('lote');            
            $table->float('cantidad',8,3);
            $table->float('precio_unitario',8,3);
            $table->text('observacion')->nullable();

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_insumos');
    }
}
