<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_equipos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('estatusequipo_id');
            $table->string('nombre');
            $table->string('marca');
            $table->string('modelo');
            $table->text('descripcion')->nullable();
            $table->string('numero_serie')->unique();
            $table->float('precio',8,2);            
            $table->string('inventario')->unique();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_equipos');
    }
}
