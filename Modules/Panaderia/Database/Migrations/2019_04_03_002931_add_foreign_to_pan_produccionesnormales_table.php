<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProduccionesnormalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_produccionesnormales', function (Blueprint $table) {
            $table->foreign('produccion_id')->references('id')->on('pan_producciones');
            $table->foreign('produccionprogramada_id')->references('id')->on('pan_produccionesprogramadas');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_produccionesnormales', function (Blueprint $table) {
            $table->dropForeign(['produccion_id']);
            $table->dropForeign(['produccionprogramada_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_produccionesnormales_produccion_id_foreign');
            $table->dropIndex('pan_produccionesnormales_produccionprogramada_id_foreign');
            //$table->dropIndex('pan_produccionesnormales_usuario_id_foreign');
        });
    }
}
