<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_pedidos', function (Blueprint $table) {
            $table->foreign('cliente_id')->references('id')->on('personas');
            $table->foreign('solicitud_id')->references('id')->on('pan_solicitudes');
            $table->foreign('localidad_id')->references('id')->on('cat_localidades');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_pedidos', function (Blueprint $table) {
            $table->dropForeign(['solicitud_id']);
            $table->dropForeign(['cliente_id']);
            $table->dropForeign(['localidad_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_pedidos_solicitud_id_foreign');
            $table->dropIndex('pan_pedidos_cliente_id_foreign');
            $table->dropIndex('pan_pedidos_localidad_id_foreign');
            //$table->dropIndex('pan_pedidos_usuario_id_foreign');
        });
    }
}
