<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProduccionesprogramadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_produccionesprogramadas', function (Blueprint $table) {
            $table->foreign('estatus_id')->references('id')->on('pan_cat_estatus');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_produccionesprogramadas', function (Blueprint $table) {
            $table->dropForeign(['estatus_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_produccionesprogramadas_empleado_id_foreign'); 
            $table->dropIndex('pan_produccionesprogramadas_estatus_id_foreign'); 
            //$table->dropIndex('pan_produccionesprogramadas_usuario_id_foreign'); 
        });
    }
}
