<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanPagosPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_pagos_pedidos', function (Blueprint $table) {
            $table->foreign('pedido_id')->references('id')->on('pan_pedidos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_pagos_pedidos', function (Blueprint $table) {
            $table->dropForeign(['pedido_id']);       
            $table->dropIndex('pan_pagos_pedidos_pedido_id_foreign'); 

        });
    }
}
