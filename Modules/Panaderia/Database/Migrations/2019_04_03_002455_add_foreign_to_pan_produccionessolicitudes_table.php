<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignToPanProduccionessolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pan_produccionessolicitudes', function (Blueprint $table) {
            $table->foreign('produccion_id')->references('id')->on('pan_producciones');
            $table->foreign('solicitud_id')->references('id')->on('pan_solicitudes');
            //$table->foreign('usuario_id')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pan_produccionessolicitudes', function (Blueprint $table) {
            $table->dropForeign(['produccion_id']);
            $table->dropForeign(['solicitud_id']);
            //$table->dropForeign(['usuario_id']);
            $table->dropIndex('pan_produccionessolicitudes_produccion_id_foreign');
            $table->dropIndex('pan_produccionessolicitudes_solicitud_id_foreign');
            //$table->dropIndex('pan_produccionessolicitudes_usuario_id_foreign');
        });
    }
}
