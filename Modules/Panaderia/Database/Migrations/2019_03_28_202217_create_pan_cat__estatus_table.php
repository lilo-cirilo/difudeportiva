<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Edgar
  MOTIVO: Registro del catalogo de estatus disponibles para las solicitudes y producción del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanCatEstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_cat_estatus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_cat_estatus');
    }
}
