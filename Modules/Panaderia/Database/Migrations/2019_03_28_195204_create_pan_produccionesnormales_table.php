<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la produccion a la que pertenece una producción ya programada.
  ALCANCE: Panadería (Residencia)
*/

class CreatePanProduccionesnormalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_produccionesnormales', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('produccion_id')->unique();
            $table->unsignedInteger('produccionprogramada_id')->unique();
            
            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_produccionesnormales');
    }
}
