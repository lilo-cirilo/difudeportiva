<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/*
  PETICIÓN: Edgar
  MOTIVO: Registro de la información de las recetas del Taller de Panadería.
  ALCANCE: Panadería (Residencia)
*/
class CreatePanRecetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pan_recetas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('producto_id');            
            $table->string('nombre');
            $table->Integer('porciones');

            $table->unsignedInteger('usuario_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pan_recetas');
    }
}
