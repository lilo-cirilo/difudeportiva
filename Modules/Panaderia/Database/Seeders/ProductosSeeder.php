<?php

namespace Modules\Panaderia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Panaderia\Entities\Producto;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'BOLILLO','precio'=>2,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'BOLLO','precio'=>3,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'TELERA','precio'=>2,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'CUERNITO RELLENO','precio'=>4,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        //Producto::create(['tipoproducto_id'=>1,'nombre'=>'REBANADA DE PIZZA HAWAINA','precio'=>15,'stock'=>30,'activo'=>1, 'solicitado'=>0, 'usuario_id'=>1]);
        //Producto::create(['tipoproducto_id'=>1,'nombre'=>'REBANADA DE PIZZA CHORIQUESO','precio'=>15,'stock'=>30,'activo'=>1,'solicitado'=>0, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'CONCHA VAINILLA','precio'=>2,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'CONCHA CHOCOLATE','precio'=>2,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'CUERNITO','precio'=>3,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        //Producto::create(['tipoproducto_id'=>2,'nombre'=>'PAN DULCE SURTIDO','precio'=>2,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        //Producto::create(['tipoproducto_id'=>2,'nombre'=>'PAN DULCE CON PASTA SURTIDO','precio'=>2,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA VAINILLA','precio'=>4,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA NARANJA','precio'=>4,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA NUEZ','precio'=>4,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA GRANOLA','precio'=>4,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA PASAS','precio'=>4,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'PANQUÉ CHINO','precio'=>8,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'MANTECADA DE MANTECA','precio'=>4,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'GALLETAS BESITOS','precio'=>1,'stock'=>180,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'PAN PIEDRA','precio'=>3.50,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'DONA AZÚCAR','precio'=>8,'stock'=>30,'activo'=>1, 'solicitado'=>1,'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'DONA LECHECILLA','precio'=>8,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>2,'nombre'=>'DONA CHOCOLATE','precio'=>8,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'TARTALETA DE FRUTAS','precio'=>15,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'TARTALETA DE DURAZNO','precio'=>15,'stock'=>30,'activo'=>1, 'solicitado'=>1,'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'TARTALETA DE KIWI','precio'=>15,'stock'=>30,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'TARTALETA DE FRESA','precio'=>15,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'TARTALETA DE UVAS','precio'=>15,'stock'=>30,'activo'=>1, 'solicitado'=>1, 'usuario_id'=>1]);
        //Producto::create(['tipoproducto_id'=>3,'nombre'=>'REBANADA DE PASTEL','precio'=>20,'stock'=>30,'activo'=>1, 'solicitado'=>0, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>3,'nombre'=>'PASTEL 1KG','precio'=>150,'stock'=>2,'activo'=>1,'solicitado'=>1, 'usuario_id'=>1]);
        Producto::create(['tipoproducto_id'=>1,'nombre'=>'PIZZA (18 PORCIONES)','precio'=>150,'stock'=>2,'activo'=>1, 'solicitado'=>1,'usuario_id'=>1]);

    }
}
