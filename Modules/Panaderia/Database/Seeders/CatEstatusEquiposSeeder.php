<?php

namespace Modules\Panaderia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Panaderia\Entities\Statusequipos;
class CatEstatusEquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Statusequipos::create(['nombre'=>'Bueno']);
        Statusequipos::create(['nombre'=>'Malo']);
        Statusequipos::create(['nombre'=>'Regular']);

        // $this->call("OthersTableSeeder");
    }
}
