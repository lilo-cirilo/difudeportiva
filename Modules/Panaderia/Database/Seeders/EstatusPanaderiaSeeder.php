<?php

namespace Modules\Panaderia\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Panaderia\Entities\Status;

class EstatusPanaderiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        Status::create(['nombre'=>'PAGADO']);
        Status::create(['nombre'=>'NO PAGADO']);
        Status::create(['nombre'=>'CANCELADO']);
        Status::create(['nombre'=>'CANCELADO SIN DEVOLUCION']);
        Status::create(['nombre'=>'ENTREGADO']);
        Status::create(['nombre'=>'NO ENTREGADO']);
        Status::create(['nombre'=>'EN PRODUCCION']);
        Status::create(['nombre'=>'TERMINADO']);
        Status::create(['nombre'=>'PENDIENTE']);
        Status::create(['nombre'=>'LISTA DE ESPERA']);
        // $this->call("OthersTableSeeder");
    }
}
