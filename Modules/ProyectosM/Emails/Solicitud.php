<?php

namespace Modules\ProyectosM\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\MopiSolicitudes;

class Solicitud extends Mailable
{
    use Queueable, SerializesModels;

    public $datos , $area;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(MopiSolicitudes $datos, $area)
    {
        $this->datos = $datos;
        $this->area = $area;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('proyectosm::solicitud.mail');
    }
}
