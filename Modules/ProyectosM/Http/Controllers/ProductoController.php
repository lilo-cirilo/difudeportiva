<?php

namespace Modules\ProyectosM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

use App\Models\MopiCatPresentaciones;
use App\Models\MopiCatCategorias;
use App\Models\MopiCatProductos;
use App\Models\MopiPresentacionProducto;

class ProductoController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('roles:ADMINISTRADOR,DIRECTOR,PLANEACION');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('proyectosm::productos.producto');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('proyectosm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try{
            DB::beginTransaction();
            $producto = MopiCatProductos::create([
                'nombre' => $request->producto
            ]);
            foreach($request->presentaciones as $key=>$presentacion){
                if(is_string($presentacion)){
                    $producto->presentaciones()->save(MopiCatPresentaciones::create(['presentacion' => strtoupper($presentacion)]), ['precio_estimado' => $request->precios[$key], 'categoria_id' => $request->categoria['id'],'descripcion' => $request->descripcion, 'marca'=>'Producto nuevo']);
                }else{
                    $producto->presentaciones()->attach($presentacion['id'], ['precio_estimado' => $request->precios[$key], 'categoria_id' => $request->categoria['id'],'descripcion' => $request->descripcion, 'marca'=>'Producto nuevo']);
                }
            }
            DB::commit();
            auth()->user()->bitacora(request(), [
                'tabla' => 'mopi_cat_productos',
                'registro' => $producto->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            return new JsonResponse($producto);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse($e->getError(), 422);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('proyectosm::productos.lista');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('proyectosm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
         
        $producto = MopiCatProductos::find($request->id);
        $producto->nombre = $request->nombre;
        $producto->save();

        auth()->user()->bitacora(request(), [
            'tabla' => 'mopi_salida_solicitudes',
            'registro' => $producto->id . '',
            'campos' => json_encode($request->all()) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);

        return new JsonResponse($producto);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function buscarPresentacion($nombre)
    {
        return new JsonResponse(MopiCatPresentaciones::where('presentacion', 'like', "%$nombre%")->take(10)->get());
    }

    public function buscarCategoria($nombre)
    {
        return new JsonResponse(MopiCatCategorias::where('nombre', 'like', "%$nombre%")->take(10)->get());
    }

    public function list()
    {
        return new JsonResponse(MopiCatProductos::with('presentaciones','categoria')->orderBy('id')->paginate(15));
    }
    public function updatePres(Request $request)
    {
        
        $presentacion = MopiPresentacionProducto::find($request->id);
        $presentacion->precio_estimado = $request->precio;
        $presentacion->descripcion = $request->descripcion;
        $presentacion->save();

        auth()->user()->bitacora(request(), [
            'tabla' => 'mopi_presentacion_producto',
            'registro' => $presentacion->id . '',
            'campos' => json_encode($request->all()) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);

        return new JsonResponse($presentacion);
    }
    public function buscarProductos($nombre)
    {
        return new JsonResponse(MopiCatProductos::with('presentaciones','categoria')->where('nombre', 'like', "%$nombre%")->orderBy('id')->paginate(15));
    }
}
