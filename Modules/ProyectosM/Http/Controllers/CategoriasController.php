<?php

namespace Modules\ProyectosM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Models\MopiCatCategorias;
use Illuminate\Http\JsonResponse;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('proyectosm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('proyectosm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return "hola";

        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('proyectosm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('proyectosm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
        $categoria = MopiCatCategorias::find($request->id);
        $categoria->presupuesto = $request->presupuesto;
        $categoria->comprometido = $request->ejercido;
        $categoria->disponible = $request->topeCandado;
        $categoria->save();
        
        auth()->user()->bitacora(request(), [
            'tabla' => 'mopi_cat_categorias',
            'registro' => $request->id . '',
            'campos' => json_encode($request->all()) . '',
            'metodo' => request()->method(),
            'usuario_id' => auth()->user()->id
        ]);
        return new JsonResponse(['data' => $categoria, 'message'=>"Se guardo correctamente"]);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
