<?php

namespace Modules\ProyectosM\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

use App\Models\MopiEntradas;
use App\Models\MopiEntradasProductos;
use App\Models\MopiSolicitudesProductos;
use App\Models\MopiPresentacionProducto;
use App\Models\MopiHistorialSolicitudes;

use App\Models\Empleado;
use App\Models\AreasResponsable;
use App\Models\Persona;
use App\Models\Usuario;
use App\Models\Area;



class EntradaController extends Controller
{
    public function __construct() {
        $this->middleware(['auth', 'authorized']);
        $this->middleware('roles:ADMINISTRADOR,DIRECTOR,PLANEACION,USUARIO');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('proyectosm::entrada.entrada');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('proyectosm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
            $entrada = new MopiEntradas();
            $entrada->nombre_entrega = $request->nombre;
            $entrada->primer_apellido_entrega = $request->primerAP;
            $entrada->segundo_apellido_entrega = $request->segundoAP; 
            $entrada->total = $request->total;
            $entrada->fecha_recepcion =  date("Y-m-d");
            $entrada->usuario_id = auth()->user()->id;
            $entrada->solicitud_id = $request->id;
            $entrada->area_id = $request['area']['id'];
            
            $entrada->save();  
            $entrada->folio = "DOB-RI-$entrada->id-".date("Y");  
            $entrada->save();  

            // $listaProducto = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',1)->get(); // lista de productos de la solicitud
            $productosEntregados = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',7)->get(); // lista de productos de la solicitud que ya fueron entregados
            // return $productosEntregados;
            foreach ($request->productos as $value) {
                // return  $request->id.' - '.$value['presentacion_producto_id'].' - '.$value['descripcion'] ;
                
                // return $productoFind;
                // $productoFind = $listaProducto->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
                
                // return $productoFind;
                
                if($value['estatus_id'] < 7)
                {
                    $productoEntregado = $productosEntregados->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
                    $productoFind = MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',1)->where('presentacion_producto_id', $value['presentacion_producto_id'])->where('descripcion',$value['descripcion'])->firstOrFail();
                    if((int)$productoFind->cantidad == (int)$value['cantidad'])
                    {
                        if($productoEntregado)
                        {
                            $updateProducto = MopiSolicitudesProductos::find($productoEntregado->id);
                            $updateProducto->cantidad = (int)$updateProducto->cantidad + (int)$value['cantidad'];
                            $updateProducto->precio_total =((float)$value['precio_unitario'] * ((int)$value['cantidad'])+(int)$updateProducto->cantidad);
                            $updateProducto->save();
                            DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->delete();
                        }
                        else
                            DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->update(['estatus_id'=>7]);
                        
                    }
                    else
                    {
                        if($productoEntregado)
                        {
                            $updateProducto = MopiSolicitudesProductos::find($productoEntregado->id);
                            $updateProducto->cantidad = (int)$updateProducto->cantidad + (int)$value['cantidad'];
                            $updateProducto->precio_total =((float)$value['precio_unitario'] * ((int)$value['cantidad'])+(int)$updateProducto->cantidad);
                            $updateProducto->save();                            
                        }
                        else
                        {
                            $newProdSol = new MopiSolicitudesProductos();
                            $newProdSol->cantidad = (int)$value['cantidad'];
                            $newProdSol->precio_unitario = (float)$value['precio_unitario'];
                            $newProdSol->precio_total = ((float)$value['precio_unitario'] * (int)$value['cantidad']);
                            $newProdSol->descripcion = $value['descripcion'];
                            $newProdSol->estatus_id = 7;
                            $newProdSol->presentacion_producto_id = $value['presentacion_producto_id'];
                            $newProdSol->solicitud_id = $request->id;
                            $newProdSol->save(); 
                        }
                        DB::table('mopi_solicitudes_productos')->where('id',$productoFind->id)->update(['cantidad'=>(((int)$productoFind->cantidad)-((int)$value['cantidad'])),'precio_total'=>((((int)$productoFind->cantidad)-((int)$value['cantidad']))* (float)$productoFind->precio_unitario)]);
                    }
                    
                    $nuevoProducto = new MopiEntradasProductos();
                    $nuevoProducto->cantidad = (int)$value['cantidad'];
                    $nuevoProducto->precio = (float)$value['precio_unitario'];
                    $nuevoProducto->subtotal = ((float)$value['precio_unitario'] * (int)$value['cantidad']);
                    $nuevoProducto->fecha_recepcion = date("Y-m-d");
                    $nuevoProducto->entrada_id = $entrada->id;
                    $nuevoProducto->presentacion_producto_id = $value['presentacion_producto_id'];
                    $nuevoProducto->save();
                    
                    $updatePrecio = MopiPresentacionProducto::find($productoFind->presentacion_producto_id);
                    $updatePrecio->precio_estimado = (float)$value['precio_unitario'];
                    $updatePrecio->save();

                    if(MopiSolicitudesProductos::where('solicitud_id',$request->id)->count() == MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',7)->count())
                    {
                        DB::table('mopi_solicitudes')->where('id',$request->id)->update(['estatus_id'=>7]);
                        $historial = new MopiHistorialSolicitudes();
                        $historial->solicitud_id = $request->id;
                        $historial->estatus_id = 7;
                        $historial->usuario_id = auth()->user()->id;
                        $historial->save();  
                    } 
                    else if(MopiSolicitudesProductos::where('solicitud_id',$request->id)->where('estatus_id',1)->count() == 0)    
                    {
                        DB::table('mopi_solicitudes')->where('id',$request->id)->update(['estatus_id'=>6]);
                        $historial = new MopiHistorialSolicitudes();
                        $historial->solicitud_id = $request->id;
                        $historial->estatus_id = 6; // pendiente
                        $historial->usuario_id = auth()->user()->id;
                        $historial->save();  
                    }
                }                	
            };
            DB::commit();
            auth()->user()->bitacora(request(), [
                'tabla' => 'mopi_entradas_solicitudes',
                'registro' => $entrada->id . '',
                'campos' => json_encode($request->all()) . '',
                'metodo' => request()->method(),
                'usuario_id' => auth()->user()->id
            ]);
            return new JsonResponse(['success' => true, 'folio'=>$entrada->folio]);            
        }
        catch(\Exception $e) {
            DB::rollBack();
            DB::select('call reset_autoincrement(?)',['mopi_entradas']);
            \Log::debug($e->getMessage());
            return new JsonResponse(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR','PLANEACION'], 'proyectosm'))
            return new JsonResponse(MopiEntradas::with('solicitud')->paginate(10));
        else
            return new JsonResponse(MopiEntradas::with('solicitud')->where('usuario_id',auth()->user()->id)->paginate(10));
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $entrada = MopiEntradas::with('solicitud','productos')->where('id',$id)->firstOrFail();
        $prodEntradas = MopiSolicitudesProductos::where('solicitud_id',$entrada->solicitud_id)->where('estatus_id','>',6)->get();  
        foreach ($entrada->productos as $value) {
            $productoFind = $prodEntradas->firstWhere('presentacion_producto_id', $value['presentacion_producto_id']);
            $value['descProducto'] = $productoFind->descripcion;
            $productoFind->presentacion_producto_id = 0;
        }

        $empleado = Empleado::where('persona_id',auth()->user()->persona_id)->with('area','nivel')->firstOrFail();
                
        return new JsonResponse([
            // 'usuario' => Usuario::where('id',auth()->user()->id)->with('persona')->firstOrFail(),
            'usuario' => Usuario::where('id',$entrada->usuario_id)->with('persona.empleado.area','persona.empleado.nivel')->firstOrFail(),
            'areaUser' => $empleado,
            'entrada' => $entrada,
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
