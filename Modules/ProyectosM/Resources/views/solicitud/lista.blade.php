@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
<style type="text/css">
	[v-cloak] {display: none;}
</style>
@stop

@section('content-title', 'Solicitud')
@section('content-subtitle', 'Monte de Piedad')

@section('content')
<div v-cloak>
    <div id="app">        
        <div class="row">
            {{-- @if(auth()->user()->hasRoles(['PLANEACION'])) --}}
            @if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">$ Solicitudes:</span>
                            <span class="info-box-number">$ @{{parseFloat(presupuesto.comprobado + presupuesto.sincomprobar).toFixed(2)}}</span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                Presupuesto total
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Solicitudes comprobadas</span>
                            <span class="info-box-number">$ @{{parseFloat(presupuesto.comprobado).toFixed(2)}}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.comprobado)/presupuesto.total)*100)}} % Comprobado --}}
                            </span>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-hourglass-2"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Solicitudes sin comprobar</span>
                            <span class="info-box-number">$ @{{parseFloat(presupuesto.sincomprobar).toFixed(2)}}</span>
                            <div class="progress">
                                {{-- <div class="progress-bar" :style="'width: '+(((presupuesto.total-presupuesto.comprobado)/presupuesto.total)*100)+'%'"></div> --}}
                                <div class="progress-bar" style="width: 100%"></div>
                            </div>
                            <span class="progress-description">
                                {{-- @{{(((presupuesto.sincomprobar)/presupuesto.total)*100).toFixed(0)}} % Sin Comprobar --}}
                            </span>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-hand-pointer-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Solicitudes</span>
                            <span class="info-box-number"># @{{numsolicitudes}}</span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-success">
                            <div class="box-header">
                                <h3 class="box-title"> <i class="fa fa-list-ol"></i> Lista de solicitudes <small>Pendiente de validar</small></h3>
                                @if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
                                    <button class="btn btn-xs btn-outline btn-outline-primary"  @click.prevent="listEliminados()"><i class="fa fa-trash"></i> Eliminados</a>
                                    <button class="btn btn-xs btn-outline btn-outline-primary" @click.prevent="datosInicio(), solicitudesGet(1)"><i class="fa fa-refresh"></i></button>
                                @endif
                                <div class="box-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" v-model="folioBusqueda">                    
                                        <div class="input-group-btn">
                                            <button @click.prevent="searchFolio()" class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <thead>
                                        <tr class="text-muted">
                                            <th>Folio</th>
                                            <th>Fecha de creacion</th>
                                            {{-- <th>Fecha de entrega</th> --}}
                                            <th>Total</th>
                                            <th>Estatus</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(ls,index) in listaSolicitudes">
                                            <th >@{{ls.folio}}</th>
                                            <th >@{{ls.created_at}}</th>
                                            {{-- <th >@{{ls.fecha_entrega}}</th> --}}
                                            <th>$ @{{parseInt(ls.total).toLocaleString("en-US")}}</th>
                                            <th><span :class="ls.estatus.color">  @{{ls.estatus.estatus}}</span></th>
                                            <th>
                                                <template v-if="eliminado">
                                                    {{-- @if(auth()->user()->hasRoles(['USUARIO','PLANEACION'])) --}}
                                                    @if(auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm'))
                                                        <button v-if="ls.estatus_id == 1 || ls.estatus_id == 3" class="btn btn-success btn-sm" title="Enviar" @click.prevent="cambiarEstatus(ls.id,2)"><i class="fa fa-rocket"></i></button>
                                                    @endif
                                                    {{-- @if(auth()->user()->hasRoles(['CAPTURISTA']))  --}}
                                                    @if(auth()->user()->hasRolesStrModulo(['PLANEACION'], 'proyectosm'))
                                                        <button v-if="ls.estatus_id == 4" class="btn btn-info btn-sm" title="Solicitar" @click.prevent="cambiarEstatus(ls.id,5)"><i class="fa fa-truck"></i></button>
                                                        <button v-if="ls.estatus_id < 5"class="btn btn-danger btn-sm" title="Eliminar" @click.prevent="eliminarSolicitud(ls.id)"><i class="fa fa-trash"></i></button>
                                                        <button v-if="ls.estatus_id == 9" class="btn btn-info btn-sm" title="Finalizar" @click.prevent="cambiarEstatus(ls.id,10)"><i class="fa fa-clipboard"></i></button>
                                                    @endif
                                                    {{-- comprbar quien lo ve --}}
                                                    @if(auth()->user()->hasRolesStrModulo(['PLANEACION','USUARIO','DIRECTOR'], 'proyectosm'))
                                                        <button v-if="ls.estatus_id < 3" class="btn btn-primary btn-sm" title="Cancelar" @click.prevent="cancelarSolicitud(ls.id,3)"><i class="fa fa-times"></i></button>
                                                        <button v-if="ls.estatus_id > 3" class="btn btn-success btn-sm" title="Imprimir" @click.prevent="printPDF(ls.id)"><i class="fa fa-print"></i></button>
                                                    @endif
                                                </template>
                                               
                                                <button class="btn btn-warning btn-sm" title="Detalle" @click.prevent="showSolicitud(ls.id)"><i class="fa fa-eye"></i></button>
                                            </th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item" v-if="pagination.current_page > 1">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
                            </li>
                            <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
                            </li>
                            <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        {{-- Modal data-backdrop="static" data-keyboard="false"--}}
        <div class="modal modal-primary fade" id="modalSolicitud"  style="posicion:center">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">Solicitud </h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12" v-if="solicitud.estatus_id == 3">
                                    <div class="alert alert-danger alert-dismissible">
                                        <h4><i class="icon fa fa-ban"></i> Alerta!</h4>
                                        <p>
                                            @{{solicitud.motivo}}
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6 text-center">
                                    <p class="text-muted">Folio: <br> @{{solicitud.folio}}</p>
                                </div>
                                <div class="col-md-6 text-center">
                                    <p class="text-muted">Fecha de creacion: <br> @{{solicitud.created_at}}</p>
                                </div>
                                <div class="col-md-9 text-center">
                                    <p class="text-muted">Area Solicitante: <br> @{{solicitud.area.nombre}}</p>
                                </div>
                                <div class="col-md-3 text-center">
                                    <p class="text-muted">Estatus:</p>
                                    <span :class="solicitud.estatus.color">  @{{solicitud.estatus.estatus}}</span>
                                </div>
                                <div class="col-md-12">
                                    <p v-show="!editmode" class="text-muted" align="justify">Justificacion: <br> @{{solicitud.justificacion}}</p>
                                    <textarea v-show="editmode" rows="2" cols="50" ref="editor" id="editor" class="form-control input-sm" v-model="solicitud.justificacion" v-validate="'required:true'" name="justificacion"></textarea>
                                </div>
                            </div>                            
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-header">
                                            <h3 class="box-title">Lista de Productos</h3>
                                            <div class="box-tools">
                                                <div class="input-group input-group-sm" style="width: 150px;">                                                                    
                                                </div>
                                            </div>
                                            <div class="box-tools pull-right">
                                                <span data-toggle="tooltip" title="" class="badge bg-green" data-original-title="Total">Total: $ @{{parseInt(solicitud.total).toLocaleString("en-US")}}</span>
                                                @if(auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm'))
                                                    <button v-if="solicitud.estatus_id == 1 || solicitud.estatus_id == 3" title="Editar" type="button" class="btn btn-box-tool badge bg-maroon" @click.prevent="editmode = 'true'"><i class="fa fa-edit "></i></button>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr class="text-muted">
                                                        <td width="10%">Producto</td>
                                                        <td width="40%">Descripción</td>
                                                        <td width="8%">Presentacion</td>
                                                        <td width="8%">Cantidad</td>
                                                        <td width="10%">Precio Unitario</td>
                                                        <td width="10%">Precio Total</td>
                                                        <td width="10%" v-show="!editmode">Estatus</td>
                                                        <td v-show="editmode" width="10%">Opciones</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="(p,index) in productos" class="text-light-blue">
                                                        <th width="10%">@{{p.nombre}}</th>
                                                        <th width="40%">@{{p.descripcion}}</th>
                                                        <th width="8%">@{{p.presentacion}}</th>
                                                        <th width="8%">@{{p.cantidad}}</th>
                                                        <th width="10%">$ @{{p.precio_unitario.toLocaleString("en-US")}}</th>
                                                        <th width="10%">$ @{{parseInt(p.precio_unitario * p.cantidad).toLocaleString("en-US")}}</th>
                                                        <th width="10%" v-if="p.estatus_id == 7" v-show="!editmode"><small class="label label-success"> ENTREGADO</small></th>
                                                        <th width="10%" v-else-if="p.estatus_id == 9" v-show="!editmode"><small class="label label-info"> SALIDA COMPLETA</small></th>
                                                        <th width="10%" v-else v-show="!editmode"><small class="label label-warning" > Pendiente</small></th>
                                                        <th v-show="editmode" width="14%">
                                                            <button type="button" class="btn btn-box-tool label-warning" @click.prevent="opcionesEdit(1,index)"><i class="fa fa-minus"></i></button>
                                                            <button type="button" class="btn btn-box-tool label-success" @click.prevent="opcionesEdit(2,index)"><i class="fa fa-plus"></i></button>
                                                            <button  type="button" class="btn btn-box-tool label-danger" @click.prevent="opcionesEdit(3,index)"><i class="fa fa-trash"></i></button>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                               
                    </div>
                    <div class="modal-footer">
                        @if(auth()->user()->hasRolesStrModulo(['DIRECTOR'], 'proyectosm'))
                            <button v-if="solicitud.estatus_id == 2" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="cambiarEstatus(solicitud.id,4)"><i class="fa  fa-check"></i> Vo.bo</button>
                        @endif
                        <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                        @if(auth()->user()->hasRolesStrModulo(['USUARIO'], 'proyectosm'))
                            <button v-if="editmode && (solicitud.estatus_id == 1 || solicitud.estatus_id == 3)" type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="updateSolicitud()"><i class="fa fa-floppy-o"></i> Guardar y enviar</button>
                        @endif
                    </div> 
                </div>
            </div>
        </div>
        {{-- Fin modal --}}
    </div>
</div>
@stop

@section('other-scripts')
<script>
Vue.use(VeeValidate);
Vue.component('v-select', VueSelect.VueSelect);
new Vue({
    el: '#app',
    data: {
        solicitud:{
            area:{},
            productos:{},
            estatus:{}
        },
        productos:{},
        presupuesto:{
            total:33600000,
            comprobado:0,
            sincomprobar:0
        },
        numsolicitudes:0,
        listaSolicitudes:[],
        offset: 2,
        pagination:{},
        editmode:false,
        folioBusqueda:'',
        eliminado : true
    },
    computed: {
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
    },
    methods: {
        datosInicio(){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/datos";
            axios.get(url).then(function (response){
                unblock();
                me.presupuesto.sincomprobar = response.data.sincomprobar;
                me.presupuesto.comprobado = response.data.comprobado
                me.numsolicitudes = response.data.solicitudes;
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        solicitudesGet(page){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/solicitudesList?page="+page;
            axios.get(url).then(function (response){
                unblock();
                me.eliminado = true
                me.listaSolicitudes = response.data.data
                me.pagination = response.data;
                me.folioBusqueda = '';
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        cambiarPagina(page){
            this.solicitudesGet(page);
        },
        cancelarSolicitud(id,estatus){
            const swalWithBootstrapButtons = Swal.mixin({confirmButtonClass: 'btn btn-success',cancelButtonClass: 'btn btn-danger',buttonsStyling: false,})
            swalWithBootstrapButtons.fire({title: 'Estas seguro de cancelar la solicitud?',type: 'warning', showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',allowEscapeKey:false, allowOutsideClick:false, reverseButtons: true}).then((result) => {
            if (result.value) {

                Swal.mixin({
                    input: 'text',
                    confirmButtonText: 'Guardar',
                }).queue([
                    {
                    title: 'Rechazado' ,
                    text:'Motivo',
                    },
                ]).then((result) => {
                    if (result.hasOwnProperty('value') && result.value[0]){
                        let me = this;
                        var url = "/proyectosm/solicitud/changeStatus";
                        axios.post(url,{'id':id,'estatus':estatus, 'motivo':result.value[0]}).then(function (response){
                            if(response.data.success){
                                me.solicitudesGet(1);
                                swalWithBootstrapButtons.fire('Bien!','Se actualizo correctamente el folio: '+response.data.folio,'success')
                                $('#modalSolicitud').modal('hide');
                            }
                            else
                                me.notificacion('error','Ocurrio un error, intente de nuevo');
                        }).catch(function (error){
                            console.log("Error "+error)
                        }) 
                    }else
                        toast({type: 'error', title: 'Existen campos vacios'})
                })   
            } else if (result.dismiss === Swal.DismissReason.cancel) 
                swalWithBootstrapButtons.fire('Cancelado','No se realizo ningun cambio','error')
            })  
        },
        cambiarEstatus(id,estatus){
            const swalWithBootstrapButtons = Swal.mixin({confirmButtonClass: 'btn btn-success',cancelButtonClass: 'btn btn-danger',buttonsStyling: false,})
            swalWithBootstrapButtons.fire({title: 'Estas seguro?',type: 'warning', showCancelButton: true, confirmButtonText: 'Si', cancelButtonText: 'No',allowEscapeKey:false, allowOutsideClick:false, reverseButtons: true}).then((result) => {
            if (result.value) {
                let me = this;
                var url = "/proyectosm/solicitud/changeStatus";
                axios.post(url,{'id':id,'estatus':estatus}).then(function (response){
                    if(response.data.success){
                        me.solicitudesGet(1);
                        me.datosInicio();
                        swalWithBootstrapButtons.fire('Bien!','Se actualizo correctamente el folio: '+response.data.folio,'success')
                        $('#modalSolicitud').modal('hide');
                    }
                    else
                        me.notificacion('error','Ocurrio un error '+response.data.message);
                }).catch(function (error){
                    console.log("Error "+error)
                }) 
                  
            } else if (result.dismiss === Swal.DismissReason.cancel) 
                swalWithBootstrapButtons.fire('Cancelado','No se realizo ningun cambio','error')
            })            
        },
        showSolicitud(id){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/"+id;
            axios.get(url).then(function (response){
                unblock();
                $('#modalSolicitud').modal('show');
                me.editmode = false;
                me.solicitud = response.data.solicitud
                me.productos = response.data.productos
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        notificacion(tipo,texto){
            // Swal({position: 'top-end', type: tipo, title: texto, showConfirmButton: false, timer: 1500})
            const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
            toast({type: tipo, title: texto})                      
        },
        printPDF(id){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/"+id;
            axios.get(url).then(function (response){
                
                me.solicitud = response.data.solicitud
                me.solicitud.productos = response.data.productos
                fecha = response.data.solicitud.created_at.split("-");
                dia = fecha[2].split(" ")
                jsreport.serverUrl = '{{config('app.reporter_url')}}';
                var request = {
                    template:{
                        // shortid: "Ski6W0XQE"
                        shortid: "HJ9BLWKXE" // respaldo
                        // HJ9BLWKXE
                    },
                    header: {
                        Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
                    },
                    data:{
                        solicitud : me.solicitud,
                        otros: {
                            area:response.data.area[0],
                            persona:response.data.userPerson ,
                            anio:fecha[0],
                            mes:fecha[1],
                            dia:dia[0]
                        },
                       
                        
                    }
                };
                jsreport.render('_blank', request);
                unblock();
                // jsreport.renderAsync(request).then(function(res) {
                //     var url = res.toDataURI();
                //     var url_with_name = url.replace("data:application/pdf;", "data:application/pdf;name=solicitud.pdf;")
                //     var html = '<html>' +
                //         '<style>html, body { padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}  </style>' +
                //         '<body>' +
                //         '<iframe type="application/pdf" src="' + url_with_name + '"></iframe>' +
                //         '</body></html>';
                //     var a = window.open("about:blank", "solicitud");
                //     a.document.write(html);
                //     a.document.close();
                //     unblock();
                // }) 
                
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        // Edicion de solicitud *solo rol usuarios*
        opcionesEdit(tipo,index)
        {
            let me = this;
            if(tipo==1)
                me.productos[index].cantidad = (parseInt(me.productos[index].cantidad) - 1) <= 0? 1:parseInt(me.productos[index].cantidad) - 1;                
            else if(tipo == 2)
                me.productos[index].cantidad = parseInt(me.productos[index].cantidad) + 1;

            // me.productos[index].pivot.precio_total = parseInt(me.productos[index].pivot.precio_unitario) * parseInt(me.productos[index].pivot.cantidad);

            if(tipo == 3)
                me.productos.length == 1?me.notificacion('error','No puede quedar la lista vacia'):me.productos.splice(index,1)

            me.total = 0;
            for (i = 0; i < me.productos.length; i++) { 
                me.solicitud.total =parseFloat(me.total) + (parseFloat(me.productos[i].precio_unitario)*parseInt(me.productos[i].cantidad))
            }
        },
        updateSolicitud(){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/update";
            me.solicitud.productos = me.productos
            axios.post(url, me.solicitud).then(function (response) {
            unblock();
            if(response.data.success)
            {
                $('#modalSolicitud').modal('hide');
                me.editmode = false;
                me.notificacion('success','Se actualizo correctamente el folio :'+response.data.folio)
                me.datosInicio();
                me.solicitudesGet(1);
            }
            else
            {
                me.notificacion('error','Ocurrio un error durante el guardado')
            }
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        },
        eliminarSolicitud(id){
            const swalWithBootstrapButtons = Swal.mixin({
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass: 'btn btn-success',
                buttonsStyling: false,
            })

            swalWithBootstrapButtons.fire({
                title: 'Estas seguro?',
                text: "No podras revertir esta accion!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si, Eliminar!',
                cancelButtonText: 'No, cancelar!',
                reverseButtons: false
            }).then((result) => {
            if (result.value) {
                block();
                let me = this;
                var url = "/proyectosm/solicitud/eliminar/"+id;
                axios.delete(url).then(function (response){
                    unblock();
                    me.datosInicio();
                    me.solicitudesGet(1);
                    swalWithBootstrapButtons.fire('Eliminado!', 'Se elimino la solicitud.', 'success');
                }).catch(function (error){
                    unblock();
                    console.log("Error "+error)
                })
                
            } else if (
                // Read more about handling dismissals
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                'Cancelado',
                'No se realizo ninfun cambio :)',
                'error'
                )
            }
            })
        },
        searchFolio(){
            let me = this;
            if(me.folioBusqueda)
            {
                block();
                let me = this;
                var url = "/proyectosm/solicitud/busqueda/"+me.folioBusqueda;
                axios.get(url).then(function (response){
                    unblock();
                    me.eliminado = true
                    if(response.data.length)
                        me.listaSolicitudes = response.data
                    else
                        me.notificacion('info','Sin resultados');
                  
                }).catch(function (error){
                    unblock();
                    console.log("Error "+error)
                })
            }
            else
                me.notificacion('error','El campo de busqueda esta vacio');
            
        },
        listEliminados(){
            block();
            let me = this;
            var url = "/proyectosm/solicitud/list/eliminado";
            axios.get(url).then(function (response){
                me.eliminado = false
                me.listaSolicitudes = response.data.data
                me.pagination = response.data;
                unblock();
            }).catch(function (error){
                unblock();
                console.log("Error "+error)
            })
        }
    },
    mounted: function() { 
        this.datosInicio();
        this.solicitudesGet(1);
    },
    watch:{ }
});
</script>
@stop
