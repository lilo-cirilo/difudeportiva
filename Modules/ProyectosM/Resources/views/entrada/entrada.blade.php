@extends('proyectosm::layouts.master')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .25s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.bg-light-blue-active, .modal-primary .modal-header{
  background-color: #D12654 !important;
  border: 0;
}
.modal-primary .modal-body,
.modal-primary .modal-footer{
  border: 0;
  background-color: #ecf0f5 !important;
}
.btn-outline-primary{
  border-color: #D12654;
  color: #D12654;
}
.btn-outline-primary:hover, .btn-outline-primary:active, .btn-outline-primary:focus{
  background-color: #D12654;
}
.text-primary{
  color: #D12654;
}
.b-primary:focus{
  border-color: #D12654;
}
</style>
@stop

@section('content-title', 'Entrada')
@section('content-subtitle', 'Monte de Piedad')


@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>

<div id="entrada">
  <div v-cloak>	
    <section class="content">
      <div class="btn-group-vertical" style="padding-bottom: 1px;">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalEntrada" @click.prevent="solicitudSelect={},solicitudFolio=[],productos = {}, $validator.reset()"><i class="fa fa-plus"></i> Nuevo</button>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Entrada</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Folio</th>
                    <th>Fecha</th>
                    <th>Solicitud</th>
                    <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="en in listaEntradas">
                    <td>@{{en.folio}}</td>
                    <td>@{{en.fecha_recepcion}}</td>
                    <td>@{{en.solicitud.folio}}</td>
                    <td>
                        <button class="btn btn-success btn-sm" title="Detalle" @click.prevent="showSolicitud(en.id)"><i class="fa fa-eye"></i></button>
                        <button class="btn btn-info btn-sm" title="Imprimir" @click.prevent="printPDF(en.id)"><i class="fa fa-print"></i></button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <nav aria-label="Page navigation example">
            <ul class="pagination">
              <li class="page-item" v-if="pagination.current_page > 1">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page - 1)">Previous</a>
              </li>
              <li :class="['page-item', page == isActive ? 'active' : '']" v-for="page in pageNumber">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a>
              </li>
              <li class="page-item" v-if="pagination.current_page < pagination.last_page">
                <a class="page-link" href="#" @click.prevent="cambiarPagina(pagination.current_page + 1)">Next</a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="col-md-12" v-if="editmode">
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                <span class="username"><a href="#">@{{solicitudEdit.folio}}</a></span>
                <span class="description">@{{solicitudEdit.fecha_recepcion}}</span>
              </div>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" @click.prevent="editmode = false"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <p><b>Folio Solicitud</b> : <small>@{{solicitudEdit.solicitud.folio}}</small> </p>
              <p><b>Entrego</b>: <small>@{{solicitudEdit.nombre_entrega}} @{{solicitudEdit.primer_apellido_entrega}} @{{solicitudEdit.segundo_apellido_entrega}}</small> </p>
            </div>
            <div class="box-footer box-comments">
              <div class="box-comment">   
                <div class="comment-text" v-for="sp in solicitudEdit.productos">
                  <span class="username">
                    @{{sp.detalle.producto.nombre}} <b>| #</b>  @{{sp.cantidad}} <b>| $</b> @{{sp.subtotal}}
                    <span class="text-muted pull-right">@{{solicitudEdit.fecha_recepcion}}</span>
                  </span>
                  {{-- @{{sp.detalle.descripcion}} --}}
                  @{{sp.descProducto}}
                </div>
              </div>                  
            </div>              
          </div>
        </div>
      </div>
    </section>
    {{-- Modal Entrada --}}
    <div class="modal modal-primary fade" id="modalEntrada" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Generar Entrada</h4>
                </div>
                <div class="modal-body">
                  <div class="col-md-12">
                    <div class="row text-muted">
                      <div class="form-group">
                        <label for="folio">Folio</label>
                        <v-select :options="solicitudFolio" label="folio" v-model="solicitudSelect" @search="findProduct" :filterable='false'></v-select>
                        <br>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-6 text-center">
                        <div class="row text-light-blue">
                          <div class="form-group">
                            <label for="folio">Folio</label>
                            <p>
                              @{{solicitudSelect.folio}}
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6 text-center">
                        <div class="row text-light-blue">
                          <div class="form-group">
                            <label for="folio">Fecha solicitada</label>
                            <p>
                              @{{solicitudSelect.created_at}}
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12 text-center">
                        <div class="row text-light-blue">
                          <div class="form-group">
                            <label for="folio">Justificacion</label>
                            <p class="text-justify">
                              @{{solicitudSelect.justificacion}}
                            </p>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12 text-center">
                        <div class="row">
                          <div class="col-md-8">
                              <h4 class="text-light-blue">Listado de productos</h5>
                          </div>
                          <div class="col-md-4">
                              <h4 class="text-red">Sumatoria = @{{solicitudSelect.total}}</h5>
                          </div>
                        </div>   
                      </div>
                     
                      <div class="row">
                        <div class="col-xs-12">
                          <div class="box">
                            <div class="box-body table-responsive no-padding">
                              <table class="table table-hover text-muted">
                                <thead>
                                  <tr>
                                    <th class="width: 10%;">Cantidad</th>
                                    <th>Producto</th>
                                    <th>Descripcion</th>
                                    <th>Precio unitario</th>
                                    <th>Opciones</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-for="(p,index) in productos">
                                    <template v-if="p.estatus_id == 1">
                                      <td width="5%" class="text-center">@{{p.cantidad}}</td>
                                      <td width="30%">@{{p.nombre}} - @{{p.presentacion}}</td>
                                      <td width="40%">@{{p.descripcion}}</td>
                                      <td><Item :min="1" :max="p.precio_unitario" :id="index"></Item></td>
                                      <td width="10%" v-if="p.estatus_id < 7">
                                          <button type="button" class="btn btn-box-tool label-warning" @click.prevent="opcionesEdit(1,index)"><i class="fa fa-minus"></i></button>
                                          <button  type="button" class="btn btn-box-tool label-danger" @click.prevent="opcionesEdit(3,index)"><i class="fa fa-trash"></i></button>
                                      </td>
                                      <td v-else-if="p.estatus_id == 7">
                                          <span class="pull-right badge bg-green">RECIBIDO</span>
                                      </td>
                                      <td v-else-if="p.estatus_id == 9">
                                        <span class="pull-right badge bg-gray">ENTREGADO</span>
                                      </td>
                                    </template>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-4 text-muted">
                          <div :class="{ 'form-group': true, 'has-error': errors.has('nombre') }">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="nombre" v-model="solicitudSelect.nombre">
                          </div>
                        </div>
                        <div class="col-md-4 text-muted">
                          <div :class="{ 'form-group': true, 'has-error': errors.has('primerAP') }">
                            <label for="primerAP">Primer apellido</label>
                            <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="primerAP" v-model="solicitudSelect.primerAP">
                          </div>
                        </div>
                        <div class="col-md-4 text-muted">
                          <div :class="{ 'form-group': true, 'has-error': errors.has('segundoAP') }">
                            <label for="segundoAP">Segundo apellido</label>
                            <input type="text" class="form-control input-sm"  v-validate="'required:true'" name="segundoAP" v-model="solicitudSelect.segundoAP">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left btn-outline-primary" data-dismiss="modal"><i class="fa fa-close"></i> Salir</button>
                    <button  type="button" class="btn btn-outline pull-right btn-outline-primary" @click.prevent="saveEntrada()"><i class="fa fa-save"></i> Guardar Entrada</button>
                </div> 
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    {{-- Fin modal Entrada --}}
  </div>
</div>
@stop

@section('other-scripts')
<script>
  const Item = Vue.component('item', {
    template: `
    <div class="d-flex">
      <input type="text" class="form-control" v-model="quantity" @keyup="valid()">
    </div>
    `,
    props: ['min', 'max', 'id'],
    data(){
        return {
            quantity: this.max
        }
    },
    methods: {
        valid(){
          EventBus.$emit('item:change', {quantity: this.quantity, id: this.id});
        },
    }
});
  Vue.use(VeeValidate);
  Vue.component('v-select', VueSelect.VueSelect);
  const EventBus = new Vue();

  new Vue({
    el: '#entrada',
    components: {
        Item
    },
    data: {
      offset: 2,
      total :0,
      banderaSave:false,
      pagination:{},
      solicitudFolio:[],
      productos:{},
      solicitudSelect:{
        folio:'',
        productos:{},
        nombre:'',
        primerAP:'',
        segundoAP:'',
      },
      listaEntradas:[],
      solicitudEdit:{
        solicitud:{},
        producto:{
          detalle:{
            producto:{},
            presentacion:{}
          }
        }
      },
      persona:'',
      areaUser:'',
      editmode:false
    },
    computed: {
      isActive(){
        return this.pagination.current_page;
      },
      pageNumber(){
        if(!this.pagination.to){
          return [];
        }
        var desde = this.pagination.current_page - this.offset;
          if(desde < 1){
            desde = 1;
          }
        var hasta = desde + (this.offset * 2);
          if(hasta >= this.pagination.last_page) {
            hasta = this.pagination.last_page;
          }
        var pagesArray = [];
          while(desde <= hasta) {
            pagesArray.push(desde);
            desde++;
          }
        return pagesArray;
      }
    },
    methods: {
      cambiarPagina(page){
        this.entradasGet(page);
      },
      entradasGet(page){
        block();
        let me = this;
        var url = "/proyectosm/entradas/showList?page="+page;
        axios.get(url).then(function (response) {
            unblock();
            me.listaEntradas = response.data.data
            me.pagination = response.data;
        }).catch(function (error){
            unblock();
            console.log("Error "+error)
        })
      },
      notificacion(tipo,texto){
        const toast = Swal.mixin({toast: true, position: 'top-end', showConfirmButton: false, timer: 3000});
        toast({type: tipo, title: texto})                      
      },
      findProduct(search, loading) {
            loading(true);
            this.searchP(loading, search, this);
        },
      searchP: _.debounce((loading, search, vm) => {
          fetch(`/proyectosm/solicitud/folio/${escape(search)}`)
              .then(res => {
                  res.json()
                      .then(json => {
                          vm.solicitudFolio = json
                      });
                  loading(false);
              });
      }, 350),
      opcionesEdit(tipo,index)
      {
        let me = this;
        if(tipo==1)
            me.productos[index].cantidad = (parseInt(me.productos[index].cantidad) - 1) <= 0? 1:parseInt(me.productos[index].cantidad) - 1;                
       else if(tipo == 3)
            me.productos.length == 1?me.notificacion('error','No puede quedar la lista vacia'):me.productos.splice(index,1)

        me.total = 0;
        for (i = 0; i < me.productos.length; i++) { 
            if(me.productos[i].cantidad > 0)
              me.solicitudSelect.total =parseFloat(me.total) + (parseFloat(me.productos[i].precio_unitario)*parseInt(me.productos[i].cantidad))
        }
      },
      saveEntrada(){
        let me = this;
        this.$validator.validateAll().then((result) => {
          if(result) {
            block();
            me.solicitudSelect.productos = me.productos
            var prodSave = 0;
            for (i = 0; i < me.productos.length; i++) { 
                if(me.productos[i].estatus_id == 1)
                  prodSave++
            }
            if(prodSave == 0)
              return me.notificacion('error','No existen productos disponibles para realizar la entrada');

            var url = "/proyectosm/entradas/save";
            axios.post(url, me.solicitudSelect).then(function (response) {
            unblock();
            if(response.data.success)
            {
                $('#modalEntrada').modal('hide');
                me.solicitudSelect = {};
                me.productos = {};
                me.$validator.reset();
                me.notificacion('success','Se guardo correctamente con el folio :'+response.data.folio)
                me.entradasGet(1);
            }
            else
            {
                me.notificacion('error','Ocurrio un error durante el guardado')
            }
          }).catch(function (error){
              console.log("Error "+error)
              unblock();
          })
          }
          else
              me.notificacion('error','Existen campos vacios')
        });
      },
      showSolicitud(id){
        block();
        let me = this;
        var url = "/proyectosm/entradas/"+id;
        axios.get(url).then(function (response) {
          unblock();
          me.solicitudEdit = response.data.entrada
          me.editmode = true
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
      printPDF(id){
        block();
        let me = this;
        var url = "/proyectosm/entradas/"+id;
        axios.get(url).then(function (response) {
          // unblock();
          me.persona = response.data.usuario.persona
          me.areaUser = response.data.areaUser
          me.solicitudEdit = response.data.entrada
          fecha = me.solicitudEdit.fecha_recepcion.split("-");
          jsreport.serverUrl = '{{config('app.reporter_url')}}';
          var request = {
            template:{
              shortid: "r1kjlJFmV" // PMReciboInterno
            },
            header: {
              Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
            },
            data:{
              solicitud: me.solicitudEdit,
              persona:me.persona,
              areaUser: me.areaUser,
              anioo:fecha[0],
              mess:fecha[1],
              diaa:fecha[2],
              }
        };
        // jsreport.render('_blank', request);
        jsreport.renderAsync(request).then(function(res) {
          var url = res.toDataURI();
          var url_with_name = url.replace("data:application/pdf;", "data:application/pdf;name=solicitud.pdf;")
          var html = '<html>' +
              '<style>html, body { padding: 0; margin: 0; } iframe { width: 100%; height: 100%; border: 0;}  </style>' +
              '<body>' +
              '<iframe type="application/pdf" src="' + url_with_name + '"></iframe>' +
              '</body></html>';
          var a = window.open("about:blank", "solicitud");
          a.document.write(html);
          a.document.close();
          unblock();
        }) 
        }).catch(function (error){
          unblock();
          console.log("Error "+error)
        })
      },
    },
    mounted: function() {
      let me = this;
      me.entradasGet(1);
    },
    created(){
        EventBus.$on('item:change', (data) => {
            this.productos[data.id].precio_unitario = data.quantity;
        });
    },
    watch:{ 
      'solicitudSelect.folio':function(){
        let me = this;
        if(me.solicitudSelect.folio){
          var url = "/proyectosm/solicitud/listaProd/"+me.solicitudSelect.id;
          axios.get(url).then(function (response) {
            me.productos = response.data
          }).catch(function (error){
            console.log("Error "+error)
          })     
        }
      },
    }
  });
</script>
@stop


