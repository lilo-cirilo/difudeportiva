<?php

Route::group(['middleware' => 'web', 'prefix' => 'delegaciones', 'namespace' => 'Modules\Delegaciones\Http\Controllers'], function() {
    Route::get('/', 'PeticionController@index');
    Route::resource('peticiones', 'PeticionController',['as'=>'delegaciones','only'=>['index','show','update','destroy']]);   //Peticiones
    Route::resource('programas', 'ProgramaController',['as'=>'delegaciones']); //Programas (beneficios)
    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'delegaciones' ]);                                //Beneficiarios
});
