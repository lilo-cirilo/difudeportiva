@extends('orgeventos::layouts.master')

@section('content')
<style type="text/css">
	[v-cloak] {display: none;}
</style>
{{-- <link href="{{ asset('dataTables/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dataTables/2dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet"> --}}
<link href="{{ asset('dataTables/jquery.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<div id="divInicio">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12" >
			<div class="info-box bg-aqua">
				<span class="info-box-icon">
					<i class="fa fa-thumbs-o-up"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Eventos</span>
					<span class="info-box-number">@{{total}}</span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#modalDto" @click.prevent="modalTable(7,'Cancelado')">
			<div class="info-box bg-red">
				<span class="info-box-icon">
					<i class="fa fa-warning"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Cancelado</span>
					<span class="info-box-number">@{{cancelado}}</span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#modalDto" @click.prevent="modalTable(8,'Finalizado')">
			<div class="info-box bg-green">
				<span class="info-box-icon">
					<i class="fa fa-hand-o-right"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Finalizado</span>
					<span class="info-box-number">@{{finalizado}}</span>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#modalDto" @click.prevent="modalTable(5,'Lista de espera')">
			<div class="info-box bg-yellow">
				<span class="info-box-icon">
					<i class="fa fa-close"></i>
				</span>
				<div class="info-box-content">
					<span class="info-box-text">Lista de espera</span>
					<span class="info-box-number">@{{listaEspera}}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="box-body no-padding">
		<div class="table-responsive col-md-12">
			<table class="table table-striped table-bordered" style="width: 100%" id="tablaDirector">
	            <thead>
	                <th>Captura</th>
	                <th>Nombre</th>
	                <th>Fecha inicio</th>
	                {{-- <th>Estatus</th> --}}
	                <th>Opciones</th>
	                {{-- <td>Solicitar actualización</td> --}}
	            </thead>
	            <tbody>
	               @foreach ($listaSolicitudes as $solicitud)
	               		<tr>
	               			<td>{{$solicitud->userCaptura}}</td>
	               			<td>{{$solicitud->nombreEvento}}</td>
	               			<td>{{$solicitud->fechaEvento}}</td>
	               			{{-- <td>{{$solicitud->estatus->status}}</td> --}}
	               			<td><a href="#" class="btn btn-primary" onclick="validate({{$solicitud->id}})" >Validar</a></td>
	               		</tr>
	               @endforeach
	            </tbody>
	        </table>
		</div>	
	</div>


	{{-- Modal --}}

	<div class="modal fade in" id="modalDto" style="display: block; padding-right: 17px;">
	  	<div class="modal-dialog modal-lg">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#modaTitulo, #bodyDatos').empty()">
	          			<span aria-hidden="true">×</span>
	          		</button>
	        		<div id="modaTitulo"> </div>
	      		</div>
	      		<div class="modal-body">
	        		<div id="bodyDatos">
	        			
	        		</div>
	      		</div>
	      		<div class="modal-footer">
	        		<button type="button" class="btn btn-default pull-left" data-dismiss="modal" onclick="$('#modaTitulo, #bodyDatos').empty()">Cerrar</button>
	        		{{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
	      		</div>
	    	</div>
	    <!-- /.modal-content -->
	  	</div>
	  <!-- /.modal-dialog -->
	</div>

	{{-- Fin modal --}}
</div>


@stop
@push('body')

{{-- <script type="text/javascript" src="{{ asset('eventos/index.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('\dataTables\jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('\eventos\qs.js') }}"></script>


<script type="text/javascript">
	$(document).ready( function () {
		block();
	    $('#tablaDirector').DataTable({
	    	'language':{
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			}
	    });
	    unblock();

	   

		/*const params = {'jrquest':"hola"};
		 console.log("parse "+Qs.stringify({jrquest:{jrquest:"hola"}}))
		 console.log("parse params "+Qs.stringify(params))
		var url = "http://www.celjava.com:9092/service.asmx/Request_Transaction";
		axios.post(url,Qs.stringify(params)).then(function (response) {
		   console.log(response)
		  })
		  .catch(function (error) {
		    console.log(error);
		 });*/


		
		/*const options = {
		  method: 'POST',
		  headers: { 'content-type': 'application/x-www-form-urlencoded' },
		  data: Qs.stringify(data),
		  url : "http://www.celjava.com:9092/service.asmx/Request_Transaction",
		};
		axios(options).then(function (response) {
		   console.log(response)
		  })
		  .catch(function (error) {
		    console.log(error);
		 });*/


		 // ----------------------------------------------------
		/*const data = {jrquest:{var1:1, var2:2}};
		const config = {
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		var url = "http://www.celjava.com:9092/service.asmx/Request_Transaction";
		axios.post(url, Qs.stringify(data), config)
		  .then((result) => {
		    console.log("respuesta "+ result)
		  })
		  .catch((err) => {
		    console.log("error  "+ err)
		  })
*/
		  // -------------------------------------------------------
	} );

	function validate (solicitud){
		swal({
		  title: 'Estas seguro?',
		  // text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, validar!',
		  allowOutsideClick: false,
		  allowEscapeKey: 	false
		}).then((result) => {
		  if (result.value) {
		  	block();
		  	var url = "/orgeventos/eventos/updateStatus";
			axios.post(url,{id:solicitud}).then(function (response){
				// console.log(response)
				unblock();
				swal({
				  title: 'Se valido correctamente',
				  type: 'success',
				  confirmButtonColor: '#3085d6',
				  confirmButtonText: 'Bien!',
				  allowOutsideClick: false,
		  		  allowEscapeKey: 	false
				}).then((result) => {
				  if (result.value)
				  	location.reload();
				})
				
			}).catch(function (error){
				console.log(error)
				unblock();
				swal({
				  type: 'error',
				  title: 'Oops...',
				  text: 'Error de conexion',
				})
			})		  	
		  }
		})
	};
	
	new Vue({
		el 	:'#divInicio',
		data 	: {
			cancelado : 0,
			listaEspera	: 0,
			finalizado 	: 0,
			total 		: 0
		},
		methods:{
			modalTable(valor, titulo){
				var url = "/orgeventos/eventos/tableInfo";
				axios.post(url,{status:valor}).then( function (response){
					$('#bodyDatos').empty().append(response.data);
					$('#modaTitulo').empty().append(titulo);
				}).catch(function(error){
					console.log(error)
					$('#modaTitulo, #bodyDatos').empty();
				})
			},
			getDatos(){
				var url = "/orgeventos/eventos/datos";
				let me = this;
				axios.get(url).then( function (response){
					me.cancelado 		= response.data.urgentes 
					me.finalizado 		= response.data.finalizado 
					me.total	 		= response.data.eventos 
					me.listaEspera 		= response.data.listaEspera 
				}).catch(function(error){
					console.log(error)
					location.reload();
				})
			}
		},
		mounted(){
			this.getDatos();
		}
	})
</script>
@endpush
