<?php

namespace Modules\OrgEventos\Entities;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class EventosParticipantes extends Model
{
    protected $table = 'orev_eventos_participantes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['evento_id', 'persona_id','constancia','evaluado','inscrito'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
