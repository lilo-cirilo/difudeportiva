<?php

Route::group(['middleware' => 'web', 'prefix' => 'orgeventos', 'namespace' => 'Modules\OrgEventos\Http\Controllers' ,'as' => 'orgeventos.'], function()
{
    Route::get('/', 'OrgEventosController@index')->name('index');

    Route::resource('peticiones', 'PeticionController', ['only' => ['index', 'show', 'update'] ]);

    Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'] ]);
    
    Route::resource('programas','ProgramaController');

    Route::prefix('solicitudes')->name('solicitudes.')->group(function() {
	    Route::get('/', 'SolicitudesController@index')->name('index');
	    Route::get('/listEventos', 'SolicitudesController@listEventos')->name('listEventos');
	    Route::post('/show', 'SolicitudesController@show')->name('show');
	    Route::put('/actualizar', 'SolicitudesController@update')->name('actualizar');
	    Route::delete('/eliminar/{id}', 'SolicitudesController@destroy')->where('id','[0-9]+');
	    Route::get('/getCatEventos', 'SolicitudesController@tipoEventoList')->name('tipoEventoList');
	    Route::post('/registrar', 'SolicitudesController@store')->name('registrarEvento');
	    Route::post('/lista', 'SolicitudesController@listaParticipantes')->name('lista');
	    Route::post('/quitar', 'SolicitudesController@quitarParticipante')->name('quitar');
	    Route::get('/reporte/{id}', 'SolicitudesController@repParticipantes')->where('id','[0-9]+');
	    Route::get('/repEventos', 'SolicitudesController@repEventos')->name('repEventos');
	    Route::post('/busqueda', 'SolicitudesController@busquedaEvento')->name('busqueda');
	    Route::post('/busquedaCURP', 'SolicitudesController@searchPersona')->name('busquedaCURP');
	    Route::get('/search/{item}', 'SolicitudesController@search');
	});

	Route::prefix('eventos')->name('eventos.')->group(function() {
	    Route::get('/', 'OrgEventosController@index')->name('index');	    
	    Route::get('/datos', 'OrgEventosController@show')->name('datos');	    
	    Route::post('/updateStatus', 'OrgEventosController@update')->name('updateStatus');	    
	    Route::post('/tableInfo', 'OrgEventosController@table')->name('tableInfo');	    
	});

	Route::prefix('participantes')->name('participantes.')->group(function() {
		Route::get('/', 'ParticipanteController@index')->name('index');
	    Route::post('/registrar', 'ParticipanteController@store')->name('registrarParticipante');
	    Route::get('/lista', 'ParticipanteController@listaParticipantes')->name('lista');
	    Route::delete('/delete', 'ParticipanteController@destroy')->name('eliminar');
	    Route::get('/repParticipantes', 'ParticipanteController@reporteParticipantesAll')->name('repParticipantes');
	    Route::post('/busqueda', 'ParticipanteController@search')->name('busqueda');
	    Route::post('/check', 'ParticipanteController@check')->name('busqueda');
	    Route::put('/actualizar', 'ParticipanteController@update')->name('actualizar');
	    Route::get('/showPersona/{id}', 'ParticipanteController@showPersona')->name('showPersona');
	});

});