<?php

namespace Modules\ControlInterno\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Factory;
use Modules\ControlInterno\Entities\CtliMenu;
use Modules\ControlInterno\Entities\AreaUsuario;

class ControlInternoServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        view()->composer('controlinterno::layouts.master', function($view) {
            
            $area_id = AreaUsuario::where('usuario_id', auth()->user()->id)->first()->area_id;
            $padre = CtliMenu::where('id', $area_id)->first();
            $data = [];
            // dd($this->submenu($padre));
            $view->with('menus', $this->submenu($padre));
        });
    }

    public function submenu($padre)
    {
        // dd($padre);
        switch ($padre->tipo) {
            case 2: 
                $submenu = CtliMenu::where('padre', $padre->id)->get();
                // dd($submenu);
                foreach($submenu as $item){
                    $departamentos = CtliMenu::where('padre', $item->id)->get();
                    $departamentos = $departamentos->toArray();
                    for($i = 0; $i < count($departamentos); $i++){
                        $departamentos[$i]["submenu"] = [];
                        // dd($depto);
                    }
                    // dd($departamentos);
                    $item->submenu = $departamentos;
                }
                $padre->submenu = $submenu->toArray();
                return [$padre->toArray()];
            break;
            case 3: 
                $submenu = CtliMenu::where('padre', $padre->id)->get();
                foreach($submenu as $item){
                    $item->submenu = [];
                }
                $padre->submenu = $submenu->toArray();
                return [$padre->toArray()];
            break;
            case 4: 
                $padre->submenu = [];
                return [$padre->toArray()];
            break;
        }
        // $submenu = CtliMenu::where('padre', $padre->id)
        //     ->orderby('padre')
        //     ->orderby('orden')
        //     ->orderby('nombre')
        //     ->get()
        //     ->toArray();
        //     dd($submenu);
        // if($submenu == null){
        //     $padre['submenu'] = $submenu;
        //     return $padre;
        // }
        // $padre->submenu = $submenu;
        // return $this->submenu($submenu);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('controlinterno.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'controlinterno'
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/controlinterno');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ],'views');

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/controlinterno';
        }, \Config::get('view.paths')), [$sourcePath]), 'controlinterno');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/controlinterno');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'controlinterno');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'controlinterno');
        }
    }

    /**
     * Register an additional directory of factories.
     * 
     * @return void
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
