@extends('controlinterno::layouts.master')

@section('myCss')
<link rel="stylesheet" href="{{asset('plugins/dropzone/dist/dropzone.css')}}">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.text-dark{
    color: #000 !important;
}
</style>
@endsection

@section('content-subtitle', 'Documentos')

@section('li-breadcrumbs')
  <li class="active">Home</li>
@endsection

@section('content')
<section class="content" id="app">
    <h3>{{$area->nombre}}</h3>
    <template v-if="documentos.length > 0">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div :class="['box', nombre != '' ? 'box-success' : 'box-primary']">
                    <div class="box-header with-border">
                    <h3 class="box-title">Selecciona tu documento</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" @click="change('box1')"><i class="fa fa-minus"></i>
                        </button>
                        <div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                        </div>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" v-for="(documento, index) in documentos">
                        <div class="row">
                            <button :class="['btn', documento.nombre === nombre ? 'btn-primary' : 'btn-default']" @click="cambiarUrl(documento, usuario)">@{{documento.nombre}}</button>
                        </div>
                    </div>
                    <!-- ./box-body -->
                    <div class="box-footer" style="">
                        <div class="row">
                            
                        </div>
                    <!-- /.row -->
                    </div>
                <!-- /.box-footer -->
                </div>
            </div>

            <div class="col-sm-12 col-md-6">
                <div :class="['box', nombre != '' ? 'box-primary' : '']">
                    <div class="box-header with-border">
                    <h3 class="box-title">Subir Documento para @{{nombre}}</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" @click="change('box1')"><i class="fa fa-minus"></i>
                        </button>
                        <div class="btn-group">
                        <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-wrench"></i></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                        </div>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="">
                        <div class="dropzone" id="dropzone"></div>
                    </div>
                </div>
            </div>


        </div>

        <transition name="fade">
        <div class="row" v-show='url != null'>
            <div class="col-sm-12">
                <div class="box box-primary shadow">
                    <div class="box-header with-border">
                        <h3 class="box-title">PDF</h3>
                        <div class="box-tools pull-right">
                            <!-- <a :href="subida != null ? '/' + subida.url : '#'" target="_blank"><i class="fa fa-eye"></i>Abrir en otra pestaña</a> -->
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <iframe :src="url" width="100%" height="600px" style="border: none;"></iframe>
                    </div>
                </div>
            </div>
        </div>
        </transition>
    </template v-if="documentos.length > 0">

    <div class="alert alert-warning alert-dismissible text-dark" v-else>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-warning"></i> Oops !</h4>
        Lo siento, aún no tienes asignado documentos :(
    </div>
    <!-- <pre>@{{documentos}}</pre> -->
  </section>
@stop

@section('myScripts')
<script src="{{asset('plugins/dropzone/dist/dropzone.js')}}"></script>
<script>
Dropzone.autoDiscover = false;
const toast = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#71DBD4'
});
const toastW = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F5CB08'
});
const toastE = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F58EAA'
});
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
var app = new Vue({
    el: '#app',
    data: {
        documentos: @json($documentos),
        myDropzone: null,
        nombre: '',
        url: null,
        usuario: @json(['id' => auth()->user()->id, 'usuario' => auth()->user()->usuario, 'email' => auth()->user()->email])
    },
    mounted(){
        if(this.documentos.length > 0){
            this.myDropzone = new Dropzone("#dropzone", { 
                dictDefaultMessage: 'Arrastre sus archivos para subirlos',
                method: 'post',
                headers: { 'X-CSRF-TOKEN': '{{csrf_token()}}' }, 
                url: '/controlinterno/docs',
                acceptedFiles: 'application/pdf',
                success: function(file, response) {
                    console.log(file);
                    console.log(response);
                    toast({ type: 'success', title: 'Great !', text: 'Archivo subido correctamente'});
                    app.url = null;
                    app.obtenerSubida(response.documento_id);
                }
            });
        }
    },
    methods: {
        log(data){
            console.log($(data));
        },
        cambiarUrl(documento, usuario){
            // $(box).removeClass('collapsed-box');
            // console.log(documento);
            this.myDropzone.removeAllFiles();
            this.nombre = documento.nombre;
            this.url = null;
            this.myDropzone.on("sending", function(file, xhr, formData) {
                formData.append("filesize", file.size);
                formData.append("documento_id", documento.id);
                formData.append("area_id", documento.area_id);
                formData.append("usuario_id", usuario.id);
            });
            this.obtenerSubida(documento.id);
        },
        obtenerSubida(id){
            axios.get(`/controlinterno/docs/${id}/pdf`)
                .then(response => {
                    console.log(response);
                    if(response.data.length > 0){
                        this.url = `/${response.data[0].url}`;
                    }
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
});

</script>
@endsection
