@extends('controlinterno::layouts.master')

@section('myCss')
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
</style>
@endsection

@section('content-subtitle', 'Documentos')

@section('li-breadcrumbs')
  <li class="active">Home</li>
@endsection

@section('content')
<section class="content" id="app">
    <div class="row">
        <div class="col-sm-4">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar Documento</h3>
                </div>
                <div class="box-body">
                    <form @submit.prevent="guardarDocumento()">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" placeholder="Nombre del documento" v-model="documento.nombre">
                        </div>
                        <div class="form-group">
                            <label for="area_id">Area</label>
                            <select id="area_id"></select>
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    	<div class="col-sm-8">
    		<div class="box box-primary shadow">
                <div class="box-header with-border">
		            <h3 class="box-title">@{{titulo}}</h3>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    <div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Area</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="documento in pagination.data">
                                    <td>@{{documento.id}}</td>
                                    <td>@{{documento.nombre}}</td>
                                    <td>@{{documento.area.nombre}}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-warning btn-xs" @click.prevent="modalDocumento(documento)"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" @click.prevent="eliminarDocumento(documento.id)"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li v-if="pagination.current_page > 1">
                                    <a href="#" aria-label="Previous" @click.prevent="cambiarPagina(pagination.current_page - 1)">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li :class="[page == isActive ? 'active' : '']" v-for="page in pageNumber" v-if="pageNumber.length > 1"><a href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a></li>
                                <li v-if="pagination.current_page < pagination.last_page">
                                    <a href="#" aria-label="Next" @click.prevent="cambiarPagina(pagination.current_page + 1)">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
    		</div>
    	</div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modalDocumento">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Editar Documento</h4>
                </div>
                <form @submit.prevent="editarDocumento(editDocumento.id)">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" id="nombre" placeholder="Nombre del documento" v-model="editDocumento.nombre">
                        </div>
                        <div class="form-group">
                            <label for="area_id">Area</label>
                            <select id="modalarea_id"></select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </section>
@stop

@section('myScripts')
<script>
const toast = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#71DBD4'
});
const toastW = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F5CB08'
});
const toastE = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F58EAA'
});
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
var app = new Vue({
    el: '#app',
    data: {
        titulo: 'Documentos',
        documento: {
            nombre: ''
        },
        pagination: {},
        offset: 5,
        editar: false,
        editDocumento: { id: 0, nombre: '' }
    },
    computed: {
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
    },
    mounted(){
        $('#area_id').select2({
            language: 'es',
            ajax: {
                type: "GET",
                url: "/controlinterno/areas", 
                dataType: "json",
                data: params => {
                    return {
                        search: params.term
                    };
                },
                processResults: (data, params) => {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, item => {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.tipo,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        });
        this.obtenerDocumentos(1);
    },
    methods: {
        guardarDocumento(){
            axios.post('/controlinterno/documentos', {
                nombre: this.documento.nombre,
                area_id: $('#area_id').val()
            }, { 'X-CSRF-TOKEN': '{{csrf_token()}}'})
                .then(response => {
                    if(response.data.id){
                        console.log(response.data);
                        toast({
                            type: 'success',
                            title: 'Guardado !'
                        });
                        this.documento.nombre = '';
                        this.obtenerDocumentos(1);
                    }
                    else{
                        toastW({
                            type: 'warning',
                            title: 'Oops !',
                            text: 'Ocurrio un error.'
                        });
                    }
                })
                .catch(error => {
                    toastE({
                        type: 'warning',
                        title: 'Oops !',
                        text: 'Ocurrio un error.' + error
                    });

                })
        },
        obtenerDocumentos(page){
            axios.get(`/controlinterno/documentos?page=${page}`)
                .then(response => {
                    console.log(response.data);
                    this.pagination = response.data;
                })
                .catch(error => {
                    console.log(error);
                })
        },
        cambiarPagina(page){
            this.obtenerDocumentos(page);
        },
        modalDocumento(documento){
            this.editDocumento.id = documento.id;
            this.editDocumento.nombre = documento.nombre;
            $('#modalarea_id').select2({
                language: 'es',
                ajax: {
                    type: "GET",
                    url: "/controlinterno/areas", 
                    dataType: "json",
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.tipo,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            });
            $('#modalarea_id').val(1);
            $('#modalarea_id').trigger('change');
            $('#modalDocumento').modal();
        },
        editarDocumento(id){
            axios.put(`/controlinterno/documentos/${id}`, {
                nombre: this.editDocumento.nombre,
                area_id: $('#modalarea_id').val()
            })
                .then(response => {
                    if(response.data.id){
                        console.log(response.data);
                        toast({
                            type: 'success',
                            title: 'Great !',
                            text: 'Cambios guardados'
                        });
                        this.editDocumento.id = 0;
                        this.editDocumento.nombre = '';
                        this.obtenerDocumentos(1);
                        $('#modalDocumento').modal('hide');
                    }
                    else{
                        toastW({
                            type: 'warning',
                            title: 'Oops !',
                            text: 'Ocurrio un error.'
                        });
                    }
                })
                .catch(error => {
                    toastE({
                        type: 'warning',
                        title: 'Oops !',
                        text: 'Ocurrio un error.' + error
                    });

                })
        },
        eliminarDocumento(id){
            swal({
                type: 'warning',
                title: 'Estas seguro de eliminarlo ?',
                text: 'Esta acción no se puede regresar',
                showCancelButton: true,
                confirmButtonColor: '#EF426F',
                cancelButtonColor: '#71DBD4',
                confirmButtonText: 'Si, eliminalo !'
            }).then(result => {
                if(result.value){
                    axios.delete(`/controlinterno/documentos/${id}`)
                        .then(response => {
                            if(response.data.deleted){
                                toast({
                                    type: 'success',
                                    title: 'Eliminado !'});
                                let d = app.pagination.data.map((d)=>{return d.id}).indexOf(id);
                                app.pagination.data.splice(d, 1);
                            }
                        })
                        .catch(error => {
                            toastE({
                                type: 'warning',
                                title: 'Oops !',
                                text: 'Ocurrio un error.' + error
                            });
                        });
                }
            });
        }
    }
});
</script>
@endsection
