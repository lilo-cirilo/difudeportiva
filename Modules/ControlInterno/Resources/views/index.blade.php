@extends('controlinterno::layouts.master')

@section('content')
    <h1>Bievenido a Control Interno</h1>

    <p>Aquí encontrarás toda la información al respecto, así como subir tus documentos.</p>
    <p>Tu area: {{auth()->user()->area->areas->nombre}}</p>
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">PDF</h3>
                    <div class="box-tools pull-right">
                        <!-- <a :href="subida != null ? '/' + subida.url : '#'" target="_blank"><i class="fa fa-eye"></i>Abrir en otra pestaña</a> -->
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <iframe src="http://intradif.test:90/storage/cinterno/W3xuz0QxgJ1oEHu6NBUntNKhKJGsZX5uJk09Pe65.jpeg" width="100%" height="600px" style="border: none;"></iframe>
                </div>
            </div>
        </div>
    </div>
@stop
