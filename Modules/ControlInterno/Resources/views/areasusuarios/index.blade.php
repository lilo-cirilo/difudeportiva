@extends('controlinterno::layouts.master')

@section('myCss')
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
</style>
@endsection

@section('content-subtitle', 'Documentos')

@section('li-breadcrumbs')
  <li class="active">Home</li>
@endsection

@section('content')
<section class="content" id="app">
    <div class="row">
        <div class="col-sm-4">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">Asignar Documento</h3>
                </div>
                <div class="box-body">
                    <form @submit.prevent="guardarAreaUsuario()">
                        <div class="form-group">
                            <label for="nombre">Usuarios</label>
                            <select id="usuario_id"></select>
                        </div>
                        <div class="form-group">
                            <label for="area_id">Area</label>
                            <select id="area_id"></select>
                        </div>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    	<!-- <div class="col-sm-8">
    		<div class="box box-primary shadow">
                <div class="box-header with-border">
		            <h3 class="box-title">@{{titulo}}</h3>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="search" name="search" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    <div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Area</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="documento in pagination.data">
                                    <td>@{{documento.id}}</td>
                                    <td>@{{documento.nombre}}</td>
                                    <td>@{{documento.area.nombre}}</td>
                                    <td>
                                        <a class="btn btn-success btn-xs"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-warning btn-xs" @click.prevent="modalDocumento(documento)"><i class="fa fa-pencil"></i></a>
                                        <a class="btn btn-danger btn-xs" @click.prevent="eliminarDocumento(documento.id)"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation">
                            <ul class="pagination">
                                <li v-if="pagination.current_page > 1">
                                    <a href="#" aria-label="Previous" @click.prevent="cambiarPagina(pagination.current_page - 1)">
                                        <span aria-hidden="true">&laquo;</span>
                                    </a>
                                </li>
                                <li :class="[page == isActive ? 'active' : '']" v-for="page in pageNumber" v-if="pageNumber.length > 1"><a href="#" @click.prevent="cambiarPagina(page)">@{{page}}</a></li>
                                <li v-if="pagination.current_page < pagination.last_page">
                                    <a href="#" aria-label="Next" @click.prevent="cambiarPagina(pagination.current_page + 1)">
                                        <span aria-hidden="true">&raquo;</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
    		</div>
    	</div> -->
    </div>
  </section>
@stop

@section('myScripts')
<script>
const toast = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#71DBD4'
});
const toastW = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F5CB08'
});
const toastE = swal.mixin({
  toast: true,
  showConfirmButton: false,
  timer: 3000,
  width: '32rem',
  background: '#F58EAA'
});
$.fn.modal.Constructor.prototype.enforceFocus = function() {};
var app = new Vue({
    el: '#app',
    data: {
        titulo: 'Documentos',
        documento: {
            nombre: ''
        },
        pagination: {},
        offset: 5,
        editar: false,
        editDocumento: { id: 0, nombre: '' }
    },
    computed: {
        isActive(){
            return this.pagination.current_page;
        },
        pageNumber(){
            if(!this.pagination.to){
                return [];
            }
            var desde = this.pagination.current_page - this.offset;
            if(desde < 1){
                desde = 1;
            }
            var hasta = desde + (this.offset * 2);
            if(hasta >= this.pagination.last_page) {
                hasta = this.pagination.last_page;
            }
            var pagesArray = [];
            while(desde <= hasta) {
                pagesArray.push(desde);
                desde++;
            }
            return pagesArray;
        }
    },
    mounted(){
        $('#area_id').select2({
            language: 'es',
            ajax: {
                type: "GET",
                url: "/controlinterno/areas", 
                dataType: "json",
                data: params => {
                    return {
                        search: params.term
                    };
                },
                processResults: (data, params) => {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, item => {
                            return {
                                id: item.id,
                                text: item.nombre,
                                slug: item.tipo,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#usuario_id').select2({
            minimumInputLength: 3,
            ajax: {
                url: '/users/find/select',
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search: params.term
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data, function(item) {
                            return {
                                id: item.id,
                                text: item.usuario,
                                slug: item.persona_id,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        });
    },
    methods: {
        guardarAreaUsuario(){
            axios.post('/controlinterno/areausuario', {
                area_id: $('#area_id').val(),
                usuario_id: $('#usuario_id').val()
            })
                .then(response => {
                    console.log(response);
                    if(response.data.id){
                        toast({ type: 'success', title: 'Great !', text: 'Asignado Correctamente'});
                    }
                })
                .catch(error => {
                    console.log(error);
                    toastE({ type: 'danger', title: 'Oops !', text: 'Algo salio mal. ' + error });
                })
        }
    }
});
</script>
@endsection
