<?php

namespace Modules\ControlInterno\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\ControlInterno\Entities\AreaUsuario;
use Illuminate\Http\JsonResponse;

class AreausuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('rolModule:8,ADMINISTRADOR');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('controlinterno::areasusuarios.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('controlinterno::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'area_id' => 'required|numeric',
        //     'usuario_id' => 'required|numeric|unique:ctli_areas_usuarios,usuario_id
        // ]);
        $areausuario = AreaUsuario::create($request->all());
        if($areausuario){
            return new JsonResponse($areausuario);
        }
        return new JsonResponse([], 422);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('controlinterno::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('controlinterno::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
