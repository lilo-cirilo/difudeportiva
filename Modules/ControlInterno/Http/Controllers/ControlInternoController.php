<?php

namespace Modules\ControlInterno\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;
use App\Models\Area;
use Modules\ControlInterno\Entities\Documento;
use Modules\ControlInterno\Entities\CtliSubida;
use Modules\ControlInterno\Entities\AreaUsuario;
use Validator;

class ControlInternoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('controlinterno::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('controlinterno::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('controlinterno::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('controlinterno::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function documentos(){
        return view('controlinterno::documentos.index');
    }

    public function areas(Request $request){
        return Area::where('nombre', 'like', '%' . $request->search . '%')->take(10)->get();
    }

    public function verDocumentos($id){
        $documentos = Documento::where('area_id', $id)->get();
        return new JsonResponse($documentos);
    } 

    public function subida($area_id){
        if(!auth()->user()->hasArea($area_id)){
            abort(403, 'No tienes permiso de estar aquí');
        }
        $a = Area::findOrFail($area_id);
        $documentos = Documento::where('area_id', $area_id)->get();
        return view('controlinterno::documentos.subida', ['documentos' => $documentos, 'area' => $a]);
    }

    public function subirDocumento(Request $request){
        $validator = Validator::make($request->all(), [
            'file' => 'mimes:pdf'
        ]);

        if ($validator->fails()) {
            return new JsonResponse(['msg' => 'No es un archivo pdf'], 422);
        }

        $documento = Documento::findOrFail($request->documento_id);
        $area = AreaUsuario::where('usuario_id', $request->usuario_id)->first();
        if(!auth()->user()->hasArea($area->area_id)){
            return new JsonResponse(['msg' => 'No tienes permiso'], 403);
        }
        if($documento->subidas != null){
            foreach($documento->subidas as $subida){
                $path = str_replace('storage','public', $subida->url);
                Storage::delete($path);
                $subida->delete();
            }
        }
        $path = $request->file('file')->store('public/cinterno');
        $path = str_replace('public', 'storage', $path);
        $subida = CtliSubida::create(['documento_id' => $documento->id, 'usuario_id' => auth()->user()->id, 'url' => $path]);
        return new JsonResponse($subida);
    }

    public function obtenerSubida($id){
        $documento = Documento::findOrFail($id);
        return new JsonResponse($documento->subidas);
    }
}
