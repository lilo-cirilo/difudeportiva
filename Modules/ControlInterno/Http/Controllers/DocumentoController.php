<?php

namespace Modules\ControlInterno\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Modules\ControlInterno\Entities\Documento;
use Illuminate\Http\JsonResponse;

class DocumentoController extends Controller
{
    use ValidatesRequests;

    public function __construct()
    {
        $this->middleware('rolModule:8,ADMINISTRADOR');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return Documento::with(['area'])->paginate(5);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('controlinterno::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['nombre' => 'required|min:4', 'area_id' => 'required|numeric']);
        $documento = Documento::create(['nombre' => $request->nombre, 'area_id' => $request->area_id, 'tipo' => '', 'usuario_id' => auth()->user()->id]);
        if($documento){
            return new JsonResponse(['id' => $documento->id, 'nombre' => $documento->nombre, 'area_id' => $documento->area_id, 'tipo' => $documento->tipo,'area' => $documento->area]);
        }
        return new JsonResponse([], 422);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('controlinterno::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('controlinterno::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $documento = Documento::findOrFail($id);
        $documento->nombre = $request->nombre;
        $documento->area_id = $request->area_id;
        $documento->save();
        return new JsonResponse($documento);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        Documento::destroy($id);
        return new JsonResponse(['deleted' => true]);
    }
}
