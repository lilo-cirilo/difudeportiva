<?php

Route::group(['middleware' => 'web', 'prefix' => 'controlinterno', 'namespace' => 'Modules\ControlInterno\Http\Controllers'], function()
{
    Route::get('/', 'ControlInternoController@index')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::get('/areas', 'ControlInternoController@areas')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::get('/areas/{id}', 'ControlInternoController@verDocumentos')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::get('/docs', 'ControlInternoController@documentos')->middleware('rolModule:8,ADMINISTRADOR');
    Route::get('/docs/{area_id}', 'ControlInternoController@subida')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::post('/docs', 'ControlInternoController@subirDocumento')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::get('/docs/{id}/pdf', 'ControlInternoController@obtenerSubida')->middleware('rolModule:8,AREA,ADMINISTRADOR');
    Route::resource('documentos', 'DocumentoController');
    Route::resource('areausuario', 'AreausuarioController');
});
