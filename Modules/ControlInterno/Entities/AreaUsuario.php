<?php

namespace Modules\ControlInterno\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreaUsuario extends Model
{
    use SoftDeletes;

    protected $table = 'ctli_areas_usuarios';

    protected $fillable = ['area_id', 'usuario_id'];

    public function usuarios(){
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }

    public function areas(){
        return $this->belongsTo('App\Models\Area', 'area_id', 'id');
    }
}
