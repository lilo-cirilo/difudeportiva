<?php

namespace Modules\ControlInterno\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CtliSubida extends Model
{
    use SoftDeletes;

    protected $table = 'ctli_subidas';

    protected $fillable = ['documento_id', 'usuario_id', 'url', 'observaciones'];

    protected $dates = ['fecha_observacion', 'deleted_at'];

    public function documento(){
        return $this->belongsTo('Modules\ControlInterno\Entities\Documento', 'documento_id', 'id');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }

}
