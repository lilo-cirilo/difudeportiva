<?php

namespace Modules\ControlInterno\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documento extends Model
{
    use SoftDeletes;

    protected $table = 'ctli_documentos';

    protected $fillable = ['nombre', 'area_id', 'tipo', 'usuario_id'];

    protected $dates = ['deleted_at'];

    public function area(){
        return $this->belongsTo('App\Models\Area', 'area_id', 'id');
    }

    public function subidas(){
        return $this->hasMany('Modules\ControlInterno\Entities\CtliSubida', 'documento_id', 'id');
    }
}
