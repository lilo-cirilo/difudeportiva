<?php

namespace Modules\ControlInterno\Entities;

use Illuminate\Database\Eloquent\Model;

class CtliMenu extends Model
{
    protected $table = 'ctli_menus';

    protected $fillable = ['nombre', 'slug', 'padre', 'orden'];

    // protected $with = ['submenu'];

    public function getChildren($data, $line)
    {
        $children = [];
        foreach ($data as $line1) {
            if ($line['id'] == $line1['padre']) {
                $children = array_merge($children, [ array_merge($line1, ['submenu' => $this->getChildren($data, $line1) ]) ]);
            }
        }
        return $children;
    }

    public function optionsMenu()
    {
        return $this->orderby('padre')
            ->orderby('orden')
            ->orderby('nombre')
            ->get()
            ->toArray();
    }    

    public function optionsMenu2($id)
    {
        return $this->where('padre', $id)
            ->orderby('padre')
            ->orderby('orden')
            ->orderby('nombre')
            ->get()
            ->toArray();
    }
    
    // public static function submenu($padre)
    // {
    //     $submenu = CtliMenu::where('padre', $padre)
    //         ->orderby('padre')
    //         ->orderby('orden')
    //         ->orderby('nombre')
    //         ->get()
    //         ->toArray();
    //         return $submenu;
    //     if($submenu == null){
    //         return $submenu;
    //     }
    //     return submenu($submenu);
    // }

    public static function menus()
    {
        $area_id = AreaUsuario::where('usuario_id', auth()->user()->id)->first()->area_id;
        $padre = CtliMenu::where('id', $area_id)->first();
        dd(submenu($padre->id));
        dd($padre->toArray());
        return [$padre->toArray()];
    }

    public static function menus2(){
        $menus = new CtliMenu();
        $data = $menus->optionsMenu();
        $menuAll = [];
        foreach ($data as $line) {
            $item = [ array_merge($line, ['submenu' => $menus->getChildren($data, $line) ]) ];
            $menuAll = array_merge($menuAll, $item);
        }
        dd($menus->menuAll = $menuAll);
        return $menus->menuAll = $menuAll;
    }

    public function submenu(){
        return $this->where('padre', $this->id)
            ->orderby('padre')
            ->orderby('orden')
            ->orderby('nombre')
            ->get();
    }

}
