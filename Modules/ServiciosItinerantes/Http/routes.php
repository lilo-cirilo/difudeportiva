<?php

Route::group(['middleware' => 'web', 'prefix' => 'serviciositinerantes', 'namespace' => 'Modules\ServiciosItinerantes\Http\Controllers'], function()
{
		Route::resource('peticiones', 'PeticionController',['as'=>'serviciositinerantes','only'=>['index','show','update','destroy']]);   //Peticiones
  	Route::resource('programas', 'ProgramaController',['as'=>'serviciositinerantes']);                                                //Programas
  	Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['except' => ['index'], 'as' => 'serviciositinerantes' ]);  //Beneficiarios
		Route::get('/permissions', 'PeticionController@permissions');
		Route::get('/rol', 'PeticionController@roles');
	
		Route::resource('/serviciositinerantes','ServiciosItinerantesController');
			Route::get('/', 'ServiciosItinerantesController@index');
			Route::get('/searchmunicipio/{nombre?}','ServiciosItinerantesController@searchMunicipio');
			Route::get('/listunidades','ServiciosItinerantesController@listUnidades');
			Route::get('/listcaravanas','ServiciosItinerantesController@listCaravanas');
			Route::post('/store', 'ServiciosItinerantesController@store');
			Route::post('/merge', 'ServiciosItinerantesController@mergeChunks');
			Route::post('/validate', 'ServiciosItinerantesController@processFile');
			Route::get('/downloadfile','ServiciosItinerantesController@downloadExcel');
});
