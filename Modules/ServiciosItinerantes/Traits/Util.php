<?php
  namespace Modules\ServiciosItinerantes\Traits;


  use Illuminate\Support\Facades\DB;
  use App\Models\UsuariosRol;
  use App\Models\Usuario;
  use App\Models\Rol;

  trait Util
  {
    public function consultarUsuario($usuario_id){
      $User = Usuario::with([
        'usuarioroles' => function ($query){
          $query->select('usuario_id', 'rol_id', 'modulo_id', 'responsable', 'autorizado');
        },
        'usuarioroles.rol' => function ($query) {
          $query->select('id', 'nombre');
        }
        ])->find($usuario_id);
      return $User;
    }
  }

?>
