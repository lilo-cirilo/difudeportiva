<?php

namespace Modules\ServiciosItinerantes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EspecieMascota extends Model
{
	use SoftDeletes;
	protected $table = 'itn_cat_especiesmascotas';
	protected $dates = ['deleted_at'];
	protected $fillable=["nombre"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
