<?php

namespace Modules\ServiciosItinerantes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tutor extends Model
{
	use SoftDeletes;
	protected $table = 'tutoresbenefpersonas';
	protected $dates = ['deleted_at'];
	protected $fillable=["tutor_persona_id","tutorado_beneficioprog_persona_id","usuario_id"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');

	public function tutor(){
		return $this->belongsTo('App\Models\Persona', 'tutor_persona_id');
	}

	public function tutorado(){
		return $this->belongsTo('App\Models\BeneficiosPersonas', 'tutorado_beneficioprog_persona_id');
	}
}