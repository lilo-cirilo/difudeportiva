<?php

namespace Modules\ServiciosItinerantes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeneficiopersonaCaravana extends Model
{
	use SoftDeletes;
	protected $table = 'itn_beneficiospersonas_caravana';
	protected $dates = ['deleted_at'];
	protected $fillable=["caravana_id","beneficioprog_persona_id","seccion","observaciones","usuario_id"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');

	public function beneficiario(){
		return $this->belongsTo('App\Models\BeneficiosPersonas', 'beneficioprog_persona_id');
	}

	public function caravana(){
		return $this->belongsTo('Modules\ServiciosItinerantes\Entities\Caravana', 'caravana_id');
	}
}
