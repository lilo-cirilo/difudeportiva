<?php

namespace Modules\ServiciosItinerantes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Caravana extends Model
{
		use SoftDeletes;
		protected $table = 'itn_caravana';
		protected $dates = ['deleted_at'];
		protected $fillable=["fecha","localidad_id","municipio_id","evento_id","usuario_id"];
		protected $hidden = array('created_at', 'updated_at', 'deleted_at');

		public function caravanaPersonas(){
			return $this->hasMany('Modules\ServiciosItinerantes\Entities\CaravanaPersona', 'caravana_id', 'id');
		}
}
