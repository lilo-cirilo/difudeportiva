<div class="sidebar" style="z-index: 0;">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="serviciositinerantes#/home">
          <i class="nav-icon fa fa-home"></i> Home
          {{-- <span class="badge badge-primary">NEW</span> --}}
        </a>
      </li>

			<li class="nav-title">Menú</li>
          <!--Registro-->
          <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle">
              <i class="nav-icon glyphicon glyphicon-file"></i>Caravanas</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item"> 
            <a class="nav-link" href="/serviciositinerantes#/archivos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <i class="nav-icon glyphicon glyphicon-upload"></i>Cargar archivos</a>
          </li>
        </ul>
      </li>

      <li class="divider"></li>

      {{-- <li class="nav-item mt-auto">
        <a class="nav-link nav-link-success" href="https://coreui.io" target="_top">
          <i class="nav-icon icon-cloud-download"></i> Download CoreUI</a>
      </li> --}}
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
