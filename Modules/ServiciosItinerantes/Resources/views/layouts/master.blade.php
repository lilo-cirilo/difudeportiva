<!DOCTYPE html>
<html lang="es">
  <head>
	<base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
				<meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
				<meta name="author" content="Łukasz Holeczek">
				<meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
				<meta name="X-CSRF-TOKEN" content="{{csrf_token()}}">
        <title>PROGRAMA INTEGRAL DE UNIDADES MÓVILES</title>

       {{-- Laravel Mix - CSS File --}}
       <link href="{{ asset('servin/app.css') }}" rel="stylesheet">
			 <link href="{{ asset('servin/bootstrap.css') }}" rel="stylesheet">
			 <link href="{{ asset('servin/style.css') }}" rel="stylesheet">

    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
		@include('serviciositinerantes::layouts.includes.navbar')

		<div class="app-body">
				@include('serviciositinerantes::layouts.includes.sidebar')

				<main class="main">

					@include('serviciositinerantes::layouts.includes.breadcrumb')

					<div class="container-fluid">
						<div class="animated fadeIn">

							@yield('content')

						</div>
					</div>

				</main>

				@include('serviciositinerantes::layouts.includes.aside')

			</div>
			<footer class="app-footer">
				<div>
					<span>DIF Oaxaca &copy; Copyright 2019.</span>
				</div>
				<div class="ml-auto">
					<span>Unidad de Informática</span>
				</div>
			</footer>
			<script src="{{ asset('servin/app.js') }}"></script>
   	 <script src="{{ asset('servin/principal.js') }}"></script>
  	</body>
</html>
