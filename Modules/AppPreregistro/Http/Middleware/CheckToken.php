<?php

namespace Modules\AppPreregistro\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

use Illuminate\Http\JsonResponse;

use \Firebase\JWT\JWT;

class CheckToken {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next) {
        $jwt = $request->header('Authorization');

        if(!$jwt && !$request->token) {
            return new JsonResponse(['data' => null, 'message' => 'Hace falta el token.'], 401);
        }

        try {
            if($jwt) {
                $datos_token = JWT::decode($jwt, config('app.jwt_token'), ['HS256']);
            }
            else {
                $datos_token = JWT::decode($request->token, config('app.jwt_token'), ['HS256']);
            }

            $request->headers->set('usuario_id', $datos_token->id);

            //$request['usuario_id'] = $datos_token->id;
            //$request['modulo_id'] = $datos_token->modulo_id;
            //$request['rol_id'] = $datos_token->rol_id;

            return $next($request);
        }
        catch(\UnexpectedValueException $e) {
            return new JsonResponse(['data' => null, 'message' => 'Token incorrecto.'], 403);
        }

        return $next($request);
    }
}
