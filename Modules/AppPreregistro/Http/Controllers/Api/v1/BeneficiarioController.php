<?php

namespace Modules\AppPreregistro\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

//use App\Models\Beneficiariogira;
//use App\Models\BeneficiariogiraPrograma;
//use App\Models\Productosfoliados;

use Modules\AppPreregistro\Entities\GiraPreregistro;
use Modules\AppPreregistro\Entities\GiraPreregistroSolicitudPrograma;
use Modules\AppPreregistro\Entities\ProductoFoliado;

class BeneficiarioController extends Controller {
    //public function __construct() {
        //$this->middleware('\Modules\AppPreregistro\Http\Middleware\CheckToken::class')->only(['index']);
    //}
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return '';
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return '';
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request) {
        $beneficiarios = [];

        foreach($request->all() as $beneficiario) {
            
            if(GiraPreregistro::where('folio', $beneficiario['folio'])->count() > 0) {
                $beneficiario['estatus'] = 'Folio duplicado.';

                $beneficiarios[] = $beneficiario;

                continue;
            }

            $producto_foliado = ProductoFoliado::where('folio', $beneficiario['folio'])->first();

            if(!$producto_foliado) {
                $beneficiario['estatus'] = 'El folio no corresponde a ningún apoyo funcional.';

                $beneficiarios[] = $beneficiario;
                
                continue;
            }

            try {
                DB::beginTransaction();

                $image = $beneficiario['foto'];

                if($beneficiario['fecha_nacimiento']) {
                    $beneficiario['fecha_nacimiento'] = (\DateTime::createFromFormat('d-m-Y', $beneficiario['fecha_nacimiento']))->format('Y-m-d');
                }

                $beneficiario['fecha_registro'] = date('Y-m-d h:i:s', strtotime($beneficiario['fecha_registro']));

                $beneficiario['usuario_id'] = $request->header('usuario_id');

                $beneficiario['fotografia'] = null;

                if($image) {
                    $folio = str_replace('/', '_', $beneficiario['folio']);

                    Storage::makeDirectory('public/eventos_atnciudadana');

                    $image = str_replace('data:image/png;base64,', '', $image);

                    $image = str_replace(' ', '+', $image);

                    $imageName = 'image_' . time() . '_' . $folio . '.png';

                    \File::put(storage_path() . '/app/public/eventos_atnciudadana/' . $imageName, base64_decode($image));

                    $beneficiario['fotografia'] = 'storage/eventos_atnciudadana/' . $imageName;
                }

                $beneficiario_gira = GiraPreregistro::create($beneficiario);

                /*BeneficiariogiraPrograma::create([
                    'girapreregis_id' => $beneficiario_gira->id,
                    'programa_id' => $producto_foliado->detalleentradasproductos->areasproducto->programa_id
                ]);*/

                DB::commit();

                $beneficiario['estatus'] = "Sincronizado";

                $beneficiarios[] = $beneficiario;
            }
            catch(\Exception $e) {
                DB::rollBack();

                $beneficiario['estatus'] = $e->getMessage();

                $beneficiarios[] = $beneficiario;
            }
        }

        return response()->json($beneficiarios);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show() {
        return '';
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit() {
        return '';
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request) {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy() {
    }
}