<?php

namespace Modules\AppPreregistro\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\AppPreregistro\DataTables\PreSolicitudesDT;

class presolicitudes extends Controller {
    public function tequio(PreSolicitudesDT $dt){
        return $dt->render('apppreregistro::preSolicitudes.tequio');
    }
}