@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); $tipo_sol = app('request')->input('tipo_solicitud') ?>

@if(auth()->check())
  @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('content-title', 'Atención Ciudadana')

@section('breadcrumbs')
  <ol class="breadcrumb">
    {{-- <li><a href="{{ route('atnciudadana.home') }}"><i class="fa fa-dashboard"></i>AtnCiudadana</a></li> --}}
    @yield('li-breadcrumbs')
  </ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">    
</ul>
@endsection

@section('styles')
  <!-- CSS Librerias -->
  @include('atnciudadana::layouts.links')
  <!-- CSS Propios -->
  <style type="text/css">
    .row {
      margin-right: 0px;
      margin-left: 0px;
    }

    input {
      text-transform: case;
    }

    .skin-blue .sidebar-menu > li.active > a.home {
      border-left-color: transparent !important;
      color: #b8c7ce !important;
    }
  </style>
@stop

@section('scripts')
  <!-- Script Librerias  -->
  @include('atnciudadana::layouts.script')
  <!-- Script Propios -->
  <script type="text/javascript">    
    // Input en mayusculas
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });
  </script>
@stop

@push('head')
  @yield('myCSS')
@endpush

@push('body')
  @yield('myScripts')
@endpush