@extends('apppreregistro::layouts.master')

@section('content-subtitle', 'Pre-Solicitudes')

@section('li-breadcrumbs')
  <li class="active">{{ 'Pre-Solicitudes' }}</li>
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
{{--  <link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">  --}}
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style>
    .btn-info {
        background-color: #d12654;
        border-color: #842a42;
        transition: all 0.5s;
    }

    .btn-info:hover, .btn-info:active, .btn-info.hover,.btn-info.focus, .btn-info:focus {
        background-color: #92193a !important;
        border-color: #d12654 !important;
    }
</style>
@endpush

@section('content')
<section class="content">
    <div class="row">
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3 id="peticiones_nuevas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Pre-peticiones nuevas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-edit"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3 id="peticiones_proceso"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Pre-peticiones vinculadas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-caret-square-o-right"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3 id="beneficio"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Lo mas pedido</p>
                </div>
                <div class="icon">
                    <i class="fa fa-check-square-o"></i>
                </div>
            </div>
        </div>
        {{-- <div class="col-lg-3 col-xs-3">
            <div class="small-box bg-red" onclick="Peticion.filtrar_peticiones(8, 'CANCELADO')">
                <div class="inner">
                    <h3 id="peticiones_canceladas"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></h3>
                    <p>Peticiones canceladas</p>
                </div>
                <div class="icon">
                    <i class="fa fa-minus-square"></i>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="row">
        <div class="col-xs-12 no-padding-lr">
            <div class="box box-primary shadow">
                <div class="box-header with-border">
                    <h3 class="box-title">Lista de Pre-Solicitudes</h3>
                </div>
                <div class="box-body">
                    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                        <input type="text" id="buscar" class="form-control">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
                        </span>
                    </div>
                    {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiarios', 'name' => 'beneficiarios', 'style' => 'width: 100%']) !!}
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modalDetalle">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrarModal()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="">Detalles Pre-Solicitud:</h4>
            </div>
            <div class="modal-body" id="">
                <form data-toggle="validator" role="form" id="formDetalle">              
                    <div class="row">
                        
                        {{--  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="folio">Folio:</label>
                                <input type="text" class="form-control" id="folio" name="folio" value="">
                            </div>
                        </div>  --}}

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="nombre">Nombre(s):</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="primer_apellido">Primer Apellido:</label>
                                <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="segundo_apellido">Segundo Apellido:</label>
                                <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="curp">CURP:</label>
                                <input type="text" class="form-control" id="curp" name="curp" value="" disabled>
                            </div>
                        </div>

                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="municipio">Municipio:</label>
                                <input type="text" class="form-control" id="municipio" name="municipio" value="" disabled>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="localidad">Localidad:</label>
                                <input type="text" class="form-control" id="localidad" name="localidad" value="" disabled>
                            </div>
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="calle">Calle:</label>
                                <input type="text" class="form-control" id="calle" name="calle" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="colonia">Colonia:</label>
                                <input type="text" class="form-control" id="colonia" name="colonia" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="numero_exterior"># Exterior:</label>
                                <input type="text" class="form-control" id="numero_exterior" name="numero_exterior" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                            <div class="form-group">
                                <label for="numero_interior"># Interior:</label>
                                <input type="text" class="form-control" id="numero_interior" name="numero_interior" value="" disabled>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="telefono">Telefono:</label>
                                <input type="text" class="form-control" id="telefono" name="telefono" value="" disabled>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer" id="modal-footer-inactivar">
                {{-- <button type="button" class="btn btn-info" onclick="crearSolicitud()">
                    <i class="fa fa-floppy-o"></i>
                Crear Solicitud</button> --}}
                {{--  <button type="button" class="btn btn-success" onclick="">
                    <i class="fa fa-database"></i>
                Externa</button>
                <button type="button" class="btn btn-success" onclick="actualizarSolicitud()">
                    <i class="fa fa-database"></i>
                Editar</button>  --}}
                <button type="button" class="btn btn-danger" onclick="cerrarModal()">
                    <i class="fa fa-remove"></i>
                Cerrar</button>    
            </div>      
        </div>
    </div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
{!! $dataTable->scripts() !!}

<script type="text/javascript">
    var preregistro_id;
    var PreSolicitud;
    var presoli;

    $(document).ready(function() {        
        $('#btn_buscar').on('click', function() {
            presoli = $('#beneficiarios').DataTable();
            presoli.search($('#buscar').val()).draw();
        });
    });

    function cerrarModal() {
        $('#modalDetalle').modal('hide');
    }

    function mostrarSolicitud(id,beneficio) {
        preregistro_id = id;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: '/apppreregistro/api/v1/preregistro?id=' + preregistro_id,
            type: 'GET',
            data: preregistro_id,
            success: function(response) {
                {{--  response.beneficios = response.beneficios.filter((b)=> b.id == beneficio);  --}}
                console.log(response);
                PreSolicitud = response;
                PreSolicitud['beneficio_id'] = beneficio;
                {{--  $('#folio').val(response.folio);  --}}
                $('#nombre').val(response.nombre);
                $('#primer_apellido').val(response.primer_apellido);
                $('#segundo_apellido').val(response.segundo_apellido);
                $('#curp').val(response.curp);
                $('#calle').val(response.calle);
                $('#colonia').val(response.colonia);
                $('#municipio').val(response.municipio.nombre);
                $('#localidad').val(response.localidad ? response.localidad.nombre : '');
                $('#numero_exterior').val(response.numero_exterior);
                $('#numero_interior').val(response.numero_interior);
                $('#telefono').val(response.telefono);

                $('#modalDetalle').modal('show');
            },
            error: function(response) {
            }
        });
    }

    function actualizarSolicitud() {
        $.ajax({
            url: '/apppreregistro/api/v1/preregistro?id=' + preregistro_id,
            type: 'POST',
            data: $('#formDetalle').serialize(),
            success: function(response) {
                console.log(response);
            },
            error: function(response) {
            }
        });
    }

    var crearSolicitud = () => {
        block();
        $.ajax({
            url: '/apppreregistro/api/v1/crear/solicitud/interna',
            type: 'POST',
            data: PreSolicitud,
            success: function(response) {
                unblock();
                console.log(response);
                if(response.success[0].code == 200) {
                    swal(        
                        'Solicitud Creada',
                        'Folio: ' + response.success[0].solicitud.folio,
                        'success'
                    );
                    $('#beneficiarios').DataTable().ajax.reload(null, false); 
                    cerrarModal();  
                } else {
                    swal(
                        '¡Error!',
                        'Algo Salio Mal',
                        'error'
                    );
                }
            },
            error: function(response) {
                unblock();
                console.log(response);
                swal(
                    '¡Error!',
                    'Algo Salio Mal',
                    'error'
                );
            }
        });
    }

    function actualizaNumeros(params) {
        $('#peticiones_nuevas').html(window.LaravelDataTables.beneficiarios.ajax.json().nuevas)
        $('#peticiones_proceso').html(window.LaravelDataTables.beneficiarios.ajax.json().viejas)
        $('#beneficio').html(window.LaravelDataTables.beneficiarios.ajax.json().beneficio)
    }
    function imprimir(datos) {
        jsreport.serverUrl = '{{ env("REPORTER_URL", "http://187.157.97.110:3001") }}';
        var request = {
            template:{
                shortid: "SklKKlzwAN"
            },
            data: datos
        };
        jsreport.renderAsync(request).then(function(res) {
            //window.open(res.toDataURI())
            res.download('pre-solicitudes.xlsx')
        });
    }
</script>
@endpush