<?php

namespace Modules\AppPreregistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GiraPreregistro extends Model {
	use SoftDeletes;

	protected $table = 'atnc_giraspreregis';

    protected $fillable = ['evento_id', 'nombre', 'primer_apellido', 'segundo_apellido', 'fecha_nacimiento', 'curp', 'telefono', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'localidad_id', 'municipio_id', 'folio', 'fecha_registro', 'fotografia', 'usuario_id', 'observaciones'];

    public function localidad() {
        return $this->belongsTo('App\Models\Localidad');
    }

    public function municipio() {
    	return $this->belongsTo('App\Models\Municipio');
    }
}