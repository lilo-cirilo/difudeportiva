<?php

namespace Modules\AppPreregistro\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductoFoliado extends Model {
	use SoftDeletes;

	protected $table = 'productosfoliados';

	protected $fillable = ['detallesentradas_producto_id', 'folio', 'detallessalidas_producto_id'];
}