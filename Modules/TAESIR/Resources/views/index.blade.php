@extends('taesir::layouts.administrador')


@section('mystyles')


  
@section('content')
<transition name="fade" mode="out-in">
    <router-view></router-view>
</transition>
@stop

@section('miscript')
<script>
    try{
    sessionStorage.setItem('taller_token', '{{$token}}');   
    sessionStorage.setItem('rol', '{{$rol}}'); 
    console.log('{{$rol}}');
  }catch(error){
   
  }
  </script>
@endsection