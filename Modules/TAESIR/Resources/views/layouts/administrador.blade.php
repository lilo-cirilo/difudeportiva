@extends('vendor.admin-lte.layouts.main')

@if (auth()->check())
<!-- @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia())) -->
<!-- @section('user-name', auth()->user()->persona->nombre) -->
@section('user-job')
@section('user-profile', route('users.edit', auth()->user()->id))
<!-- @section('user-log', auth()->user()->created_at) -->
@endif

@push('head')
<link href="{{ asset('dist/css/amarillo.css') }}" type="text/css" rel="stylesheet">
<meta http-equiv="Expires" content="0" />
<meta http-equiv="Pragma" content="no-cache" />
<script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
@yield('mystyles')
<style>
  .fade-enter-active, .fade-leave-active {
    transition: opacity .25s;
  }
  .fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
    opacity: 0;
  }
  </style>
@endpush

@section('logo', asset('images/Serv Med/intranet.png'))
@section('breadcrumbs')
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="header">NAVEGACIÓN</li>
  <li class="active">
    <a href="/taesir">
      <i class="fa fa-home"></i>
      <span>Inicio</span>
    </a>
  </li>
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ENCARGADO','RECEPCIONISTA'],'TAESIR'))
  <li class="treeview">
    <a href="#">
      <i class="fa fa-handshake-o"></i> <span>Servicios</span>
      <span class="pull-right-container">
        <i class="fa fa-angle-left pull-right"></i>
      </span>
    </a>
    <ul class="treeview-menu">
      <li><a href="/taesir#/servicios/prestamos/lista"><i class="fa fa-list-alt"></i>Prestamos</a></li>
      <li><a href="/taesir#/diagnostico"><i class="fa fa-stethoscope"></i>Diagnostico</a></li>
      <li><a href="/taesir#/servicios/reparacion/listado"><i class="fa fa-cogs"></i>Reparaciones</a></li>
      <li><a href="/taesir#/servicios/donativos"><i class="fa fa-shopping-basket"></i>Donativos</a></li>

    </ul>
  </li>
  @endif
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ALMACENISTA'],'TAESIR'))
  <li >
    <a href="/taesir#/herramientas">
      <i class="fa fa-wrench"></i>
      <span>Herramientas</span>
    </a>
  </li>
  @endif
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ALMACENISTA'],'TAESIR'))
  <li>
    <a href="/taesir#/materiales">
      <i class="fa fa-briefcase"></i>
      <span>Materiales</span>
    </a>
  </li>
  @endif
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ALMACENISTA','RECEPCIONISTA'],'TAESIR'))
  <li>
    <a href="/taesir#/apoyofuncional">
      <i class="fa fa-wheelchair"></i>
      <span>Apoyos Funcionales</span>
    </a>
  </li>
  @endif
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ENCARGADO'],'TAESIR'))
  <li>
    <a href="/taesir#/requisiciones/lista">
      <i class="fa fa-fax"></i>
      <span>Orden de Compra</span>
    </a>
  </li>
  @endif
  @if (auth()->user()->hasRolesStrModulo(['JEFE','ENCARGADO'],'TAESIR'))
  <li>
    <a href="/taesir#/reportes">
      <i class="fa fa-bar-chart"></i>
      <span>Reportes</span>
    </a>
  </li>
  @endif
<!-- <li>
      <a href="/taesir#/donmecanicoweb">
        <i class="fa fa-ambulance"></i>
        <span>Don Mecanico</span>
      </a>
    </li>-->
</ul>
@endsection

@section('content-title','TAESIR')
@section('content-subtitle', 'TALLER DE ARMADO Y ENSAMBLADO DE SILLAS DE RUEDAS')

@section('message-url-all', '#')
@section('message-all', 'Ver todos los mensajes')
@section('message-number')
@section('message-text', 'Tienes 1 mensages')
@section('message-list')
<!-- start message -->
<li>
  <a href="#">
      <div class="pull-left">
          <!-- User Image -->
          <img src="https://www.gravatar.com/avatar/?d=mm" class="img-circle" alt="User Image">
      </div>
      <!-- Message title and timestamp -->
      <h4>
          Support Team
          <small><i class="fa fa-clock-o"></i> 5 mins</small>
      </h4>
      <!-- The message -->
      <p>Why not buy a new awesome theme?</p>
  </a>
</li>
<!-- end message -->
@endsection

@section('notification-id', 'notificaciones')
@section('notification-link', 'notificaciones-link')
@section('notification-dropdown-ul', 'notificaciones-ul')
@section('notification-number')
{{-- @section('notification-text', 'Hola mundo') --}}
@section('notification-dropdown-id', 'notificaciones-titulo')
{{-- @section('notification-list')
<a href="/recmat">
    <i class="fa fa-refresh text-yellow" id="reinicio"></i> Recargue la pagina
</a>
@endsection --}}
@section('notification-dropdown-li-id', 'notificaciones-lista')

@section('task-number')
@section('task-text', 'Tienes 1 tarea')
@section('task-url-all')

@endsection
@section('task-list')
<li>
  <a href="#">
      <!-- Task title and progress text -->
      <h3>
          Design some buttons
          <small class="pull-right">20%</small>
      </h3>
      <!-- The progress bar -->
      <div class="progress xs">
          <!-- Change the css width attribute to simulate progress -->
          <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
              <span class="sr-only">20% Complete</span>
          </div>
      </div>
  </a>
</li>
@endsection

@push('body')
<script src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="{{asset('js/sweetalert2.all.js')}}"></script>
<script src="{{ asset('taesirm/app.js') }}"></script>
<script src="{{ asset('js/vue.js') }}"></script>

<script>



  function block() {
      $.blockUI({
          css: {
              border: 'none',
              padding: '5px',
              backgroundColor: 'none',
              '-webkit-border-radius': '10px',
              '-moz-border-radius': '10px',
              opacity: .8,
              color: '#fff',
          },
          baseZ: 10000,
          message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">PROCESANDO...</p></div>',
      });
  
      function unblock_error() {
          if($.unblockUI())
              alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
      }
  }
  function unblock() {
      $.unblockUI();
  }


  </script>
  

@yield('miscript')
@endpush
