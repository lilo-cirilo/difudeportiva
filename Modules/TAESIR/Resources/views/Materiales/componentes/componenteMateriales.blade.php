<script>
const Formulario = Vue.component('formulario',{
template:`
<div>
<form action="{{url('/api/insertar')}}" method="post">
    <div class="form-group">
        <p><label for="nombre">Nombre: </label> <input class="form-control" type="text" name="nombre" /></p>
        <p><label for="unidad_medida">Unidad de Medida: </label><input type="text" class="form-control" name="unidad_medida" /></p>
        <p><label for="num_serie">Numero de Serie: </label><input type="text" class="form-control" name="num_serie" /></p>
        <p><label for="marca">Marca: </label><textarea name="marca" cols="30" rows="2" class='form-control'></textarea></p>
        <p><label for="minimo">Minimo: </label><input type="text" name="minimo" class="form-control"></p>
        <p><input type="submit" value="Aceptar" class="btn btn-success" /></p>
    </div></form></div>`
});
// componente para mostrar la tabla
const ListaMateriales = Vue.component('materiap',{
    template:`
    <div>
    <div class="col-md-12">
    <h1>@{{titulo}}</h1>
    
    <!-- tabla que muestra la informacion de los materiales en el inventario-->
    <table class="table table table-hover">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Unidad de medida</th>
            <th>Numero de serie</th>
            <th>Marca</th>
            <th>Minimo</th>
            <th>Opciones</th>
        </tr>
        
        <tr v-for="(materiap, index) in material" :key="index">
            <td>@{{materiap.id}}</td>
            <td>@{{materiap.nombre}}</td>
            <td>@{{materiap.unidad_medida}}</td>
            <td>@{{materiap.num_serie}}</td>
            <td>@{{materiap.marca}}</td>
            <td>@{{materiap.minimo}}</td>
            <td>
            <a  @click.prevent="modalEditar(materiap.id)" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></a>
            <a  @click.prevent="(eliminar(materiap.id))"class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></a>
            </td>
            
        </tr>

    </table>

    <a @click="modalCrear" class="btn btn-success">AGREGAR</i></a>
    </div>

<!--fin de la table-->

<!-- formulario Modal-->

<form method="POST" v-on:submit.prevent="enviarDatos">
        {{ csrf_field() }}
        <div class="modal fade" id="modal">
	    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
                <h4>@{{modal.titulo}}</h4>
			</div>
        <div class="modal-body">
     
        <p><label for="nombre">NOMBRE: </label>
            <input class="form-control" type="text" name="nombre"
                        v-model="material2.nombre" />
                         </p>
                <p><label for="unidad_medida">UNIDAD DE MEDIDA: </label>
                    <input type="text" class="form-control" name="unidad_medida" v-model="material2.unidad_medida" />
                </p>
                <p><label for="num_serie">NUMERO DE SERIE: </label>
                    <input type="text" class="form-control" name="num_serie" v-model="material2.num_serie" />
                </p>
                <p><label for="marca">MARCA: </label>
                    <input type="text" class="form-control" name="marca" v-model="material2.marca" />
                </p>
                <p><label for="minimo">MINIMO: </label>
                    <input type="text" class="form-control" name="minimo" v-model="material2.minimo" />
                </p>
                
			</div>

            <div class="modal-footer">
                <input type="submit" class="btn btn-success"Value="Aceptar" >
                <button class="btn btn-danger" value="Cancelar" data-dismiss="modal">Cancelar</button>

			</div>                                     
		</div>
        </div>
        </div>
        </form>
    </div>
    `
      ,
    data(){
        return {
            titulo: 'Listado de Materiales',
            material: [],
            material2:{'id':'','nombre':'','unidad_medida':'','num_serie':'','marca':'','minimo':'','usuario_id':''},
            modal:{'titulo':'','boton':''},
            metodo:'',
            pagination:{}
        }
    },
    mounted(){
        this.listado();
    },
    methods: {

        getMaterial(id){
          block();
          axios.get('/taesir/materiap/'+id).then(respuesta => {
              unblock();
              this.material2.id=respuesta.data.id;
              this.material2.nombre=respuesta.data.nombre;
              this.material2.unidad_medida=respuesta.data.unidad_medida;
              this.material2.num_serie=respuesta.data.num_serie;
              this.material2.marca=respuesta.data.marca;
              this.material2.minimo=respuesta.data.minimo;
              this.material2.usuario_id=respuesta.data.usuario_id;
            unblock();
          }).catch(error => {
              unblock();
              console.log(error);
          })
      },
      //--------------------------------------------------------
      modalCrear(){
          this.resetDatos();
            this.metodo='post';
            this.modal.titulo='Agregar Material';
            this.modal.boton='AGREGAR';
            $('#modal').modal('show');
            console.log('crear');

        },
     modalEditar(id){
            this.metodo='put';
            this.modal.titulo='Actualizar Datos';
            this.modal.boton='Actualizar';
            this.getMaterial(id);
            $('#modal').modal('show');
        },
        resetDatos(){
         this.material2.id=null;
         this.material2.nombre=null;
         this.material2.unidad_medida=null;
         this.material2.num_serie=null;
         this.material2.marca=null;
         this.material2.minimo=null;
        },
        
        listado(){
            block();
            axios.get('/taesir/materiap').then(respuesta => {
                this.material =respuesta.data.data;             
            }).catch(error => {
            })
          unblock();
        },
        eliminar(id){
          block();
          axios.delete('/taesir/materiap/'+id, {
            id: this.material.id,
            nombre: this.material2.nombre,
            unidad_medida:this.material2.unidad_medida,
            num_serie:this.material2.num_serie,
            marca:this.material2.marca,
            minimo:this.material2.minimo,
            usuario_id:1,
          }).then( respuesta => {
              console.log('Se elimino la informacion');
              console.log(respuesta.data);
              unblock();
              console.log('Resultado');
                $('#modal').modal('hide');
          }).catch( error => {
              console.log('error al Actualizar');
              console.log(error);
              unblock();
          });

        },


        //--------------------------------------------------------
        abrirModal(){
            //this.resetDatos();
            this.metodo='post';
            this.modal.titulo='Agregar Material';
            this.modal.boton='Agregar';
            $('#modal').modal('show');
            //console.log('crear');
        },
        //-----------------------------------------------------------
            enviarDatos(){
            if(this.metodo == 'post')
                this.crearNuevo();
            if(this.metodo =='put')
            this.editarMaterial(this.material2.id);
        },

        //-------------------------------------------------------------
        crearNuevo(){
            block();
         console.log('Enviando formulario');
         if(this.material2.nombre === null){
             console.log('Esta vacio');
             unblock();
             return;
         }
         axios.post('/taesir/materiap', {
             nombre: this.material2.nombre,
             unidad_medida:this.material2.unidad_medida,
             num_serie:this.material2.num_serie,
             marca:this.material2.marca,
             minimo:this.material2.minimo,
             usuario_id:1,
         }).then( respuesta => {
             console.log('El produto se creo');
             console.log(respuesta.data);
             unblock();
             this.resetDatos();
         }).catch( error => {
             console.log('Ocurrio un error');
             console.log(error);
             unblock();
         });
         console.log('Resultado');
         $('#modal').modal('hide');
        },

     editarMaterial(id){
        block();
       axios.put('/taesir/materiap/'+id, {
         id: this.material.id,
         nombre: this.material2.nombre,
         unidad_medida:this.material2.unidad_medida,
         num_serie:this.material2.num_serie,
         marca:this.material2.marca,
         minimo:this.material2.minimo,
         usuario_id:1,
       }).then( respuesta => {
           console.log('Se Actualizo la informacion');
           console.log(respuesta.data);
           unblock();
           console.log('Resultado');
             $('#modal').modal('hide');
       }).catch( error => {
           console.log('error al Actualizar');
           console.log(error);
           unblock();
       });

       console.log('Resultado');
       $('#modal').modal('hide');           
        },
            
    }
    
});

</script>
