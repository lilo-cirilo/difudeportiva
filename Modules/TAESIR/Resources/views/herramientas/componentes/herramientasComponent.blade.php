<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
const Formulario = Vue.component('formulario',{
template:`
<div>
<form action="{{url('/api/insertar')}}" method="post">
    <div class="form-group">
        <p><label for="num_serie">Numero de serie: </label><input type="text" class="form-control" name="num_serie" />
        </p>
        <p><label for="nombre">Nombre: </label> <input class="form-control" type="text" name="nombre" /></p>
        <p><label for="observaciones">Observaciones: </label><textarea name="observaciones" cols="30" rows="2" class='form-control'></textarea></p>
        <p><label for="estatus">Estatus: </label><input type="text" name="estatus" class="form-control"></p>
        <p><input type="submit" value="Aceptar" class="btn btn-success" /></p>
    </div>
</form>
</div>
`
});



// componente para mostrar la tabla
const ListaHerramientas = Vue.component('herras',{
    template:`
    <div>
    <div class="col-md-12">
    <h1>@{{titulo}}</h1>
    <br>
    <!-- tabla que muestra la informacion de las herramientas en el inventario-->
    <table class="table table-hover">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Número de Serie</th>
            <th>Estatus</th>
            <th>observaciones</th>
            <th>Opciones</th>
        </tr>

        <tr v-for="(herra, index) in herramie" :key="index">
            <td>@{{herra.id}}</td>
            <td>@{{herra.nombre}}</td>
            <td>@{{herra.num_serie}}</td>
            <td>@{{herra.estatus}}</td>
            <td>@{{herra.observaciones}}</td>
            <td>
                <a  @click.prevent="modalEditar(herra.id)" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i></a>
                <a  @click.prevent="(eliminar(herra.id))"class="btn btn-danger btn-flat"><i class="fa fa-trash"></i></a>
            </td>
        </tr>

    </table>

    <button class="btn btn-success" @click="modalCrear">Nuevo</button>
</div>

<!--fin de la table-->

<!-- formulario Modal-->

<form method="POST" v-on:submit.prevent="enviarDatos">
        {{ csrf_field() }}
        <div class="modal fade" id="modal">
	    <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span>&times;</span>
				</button>
                <h4>@{{modal.titulo}}</h4>
			</div>
			<div class="modal-body">
      <p><label for="nombre">Nombre: </label>
          <input class="form-control" type="text" name="nombre"
              v-model="herrami.nombre" />
      </p>
                <p><label for="num_serie">Número de serie: </label>
                    <input type="text" class="form-control" name="num_serie"
                        v-model="herrami.num_serie" />
                </p>

                <p><label for="estatus">Estatus </label>
                    <select v-model='seleccion' class="form-control">
                    <option value=@{{seleccion}></option>
                    <option value="Bueno">Bueno</option>
                    <option value="Regular">Regular</option>
                    <option value="Malo">Malo</option>
                    </select>
                </p>
                <p><label for="observaciones">Observaciones: </label>
                    <textarea name="observaciones" cols="30" rows="1"
                        class='form-control' v-model="herrami.observaciones"></textarea>
                </p>
							</div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-success"Value="Aceptar" >
                <button class="btn btn-danger" value="Cancelar" data-dismiss="modal">Cancelar</button>

			</div>
		</div>
        </div>
        </div>
        </form>
    </div>
    `
      ,
    data(){
        return {
            titulo: 'Listado de Herramientas',
            herramie: [],
            herrami:{'id':'','nombre':'','num_serie':'','estatus':'','observaciones':''},
            modal:{'titulo':'','boton':''},
            metodo:'',
            seleccion: 'Bueno',
        }
    },
    mounted(){
        this.listado();
    },



    methods: {
      //--------------------------------------------------------
      getHerramienta(id){
          block();
          axios.get('/taesir/herra/'+id).then(respuesta => {

              this.herrami.id=respuesta.data.id;
              this.herrami.nombre=respuesta.data.nombre;
              this.herrami.num_serie=respuesta.data.num_serie;
              this.herrami.estatus=respuesta.data.estatus;
              this.seleccion = respuesta.data.estatus;
              this.herrami.observaciones=respuesta.data.observaciones;
              unblock();
          }).catch(error => {
              unblock();
              console.log(error);
          })
      },
      //---------------------------------------------------------
      modalCrear(){
       this.resetDatos();
       this.metodo='post';
       this.modal.titulo='Agregar Herramienta';
       this.modal.boton='Crear';
       $('#modal').modal('show');
       console.log('crear');
   },
   modalEditar(id){
       this.metodo='put';
       this.modal.titulo='Actualizar Datos';
       this.modal.boton='Actualizar';
       this.getHerramienta(id);
       $('#modal').modal('show');
   },
      //---------------------------------------------------------
      resetDatos(){
    this.herrami.id=null;
    this.herrami.nombre=null;
    this.herrami.num_serie=null;
    //this.herrami.estatus='0';
    this.herrami.observaciones=null;
},
      //-----------------------------------------------------------
        listado(){
          block();
            axios.get('/taesir/herra').then(respuesta => {
                this.herramie =respuesta.data.data;
            }).catch(error => {
                //console.log(error);
            })
          unblock();
        },
        //--------------------------------------------------------

        eliminar(id){
          block();
          axios.delete('/taesir/herra/'+id, {
            id: this.herramie.id,
            nombre: this.herrami.nombre,
            num_serie:this.herrami.num_serie,
            estatus:'Bueno',
            observaciones:this.herrami.observaciones,
          }).then( respuesta => {
              unblock();
                $('#modal').modal('hide');
                swal("Elemento Eliminado!", "Click en 'OK' para continuar", "success");
                ListaHerramientas.refresh();
          }).catch( error => {
              console.log('error al Actualizar');
              console.log(error);
              unblock();
          });

        },

        //--------------------------------------------------------
        abrirModal(){
            //this.resetDatos();
            this.metodo='post';
            this.modal.titulo='Agregar Herramienta';
            this.modal.boton='Agregar';
            $('#modal').modal('show');
            //console.log('crear');
        },
        //-----------------------------------------------------------
        enviarDatos(){
            if(this.metodo == 'post'){
                this.crearNuevo();}
            else if(this.metodo =='put')
            this.editarHerramienta(this.herrami.id);
        },
        //-----------------------------------------------------------
        crearNuevo(){
         block();
         console.log('Enviando formulario');
         if(this.herrami.nombre === null){
             console.log('Esta vacio');
             unblock();
             return;
         }
         axios.post('/taesir/herra', {
             nombre: this.herrami.nombre,
             num_serie:this.herrami.num_serie,
             estatus:this.seleccion,
             observaciones:this.herrami.observaciones,
         }).then( respuesta => {
             console.log('El produto se creo');
             console.log(respuesta.data);
             unblock();
             this.resetDatos();
         }).catch( error => {
             console.log('Ocurrio un error');
             console.log(error);
             unblock();
         });
         console.log('Resultado');
         $('#modal').modal('hide');
     },
     //---------------------------------------------------------------
     editarHerramienta(id){
       block();
       axios.put('/taesir/herra/'+id, {
         id: this.herramie.id,
         nombre: this.herrami.nombre,
         num_serie:this.herrami.num_serie,
         estatus:this.seleccion,
         observaciones:this.herrami.observaciones,
       }).then( respuesta => {
           console.log('Se Actualizo la informacion');
           console.log(respuesta.data);
           unblock();
           console.log('Resultado');
             $('#modal').modal('hide');
       }).catch( error => {
           console.log('error al Actualizar');
           console.log(error);
           unblock();
       });

       console.log('Resultado');
       $('#modal').modal('hide');

     },
     //--------------------------------------------------------------
    }
});

</script>
