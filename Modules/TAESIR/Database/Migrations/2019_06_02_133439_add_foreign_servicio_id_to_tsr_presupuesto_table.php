<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignServicioIdToTsrPresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_presupuestos', function (Blueprint $table) {
            $table->foreign('servicio_id')->references('id')->on('tsr_servicios');
            $table->foreign('diagnostico_id')->references('id')->on('tsr_diagnosticos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::table('tsr_presupuestos', function (Blueprint $table) {
            $table->dropForeign(['servicio_id']);
            $table->dropForeign(['diagnostico_id']);
            $table->dropIndex('tsr_presupuestos_servicio_id_foreign');
            $table->dropIndex('tsr_presupuestos_diagnostico_id_foreign');
        });
    }
}
