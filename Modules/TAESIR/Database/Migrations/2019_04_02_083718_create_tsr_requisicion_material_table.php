<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrRequisicionMaterialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_requisiciones_materiales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cantidad');
            $table->unsignedinteger('material_id');
            $table->unsignedinteger('requisicion_id');
            
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_requisiciones_materiales');
    }
}
