<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignBeneficiosprogramasIdToTsrServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_servicios', function (Blueprint $table) {
            $table->foreign('beneficiosprogramas_id')->references('id')->on('beneficiosprogramas');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_servicios', function (Blueprint $table) {
            $table->dropForeign(['beneficiosprogramas_id']);
            $table->dropIndex('tsr_servicios_beneficiosprogramas_id_foreign');
        });
    }
}
