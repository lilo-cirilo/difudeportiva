<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignPersonaIdToTsrTrabajadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_trabajadores', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_trabajadores', function (Blueprint $table) {
            $table->dropForeign(['persona_id']);
            $table->dropIndex('tsr_trabajadores_persona_id_foreign');
        });
  
    }
}
