<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignMaterialIdToTsrMaterialesCaravanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_materiales_caravanas', function (Blueprint $table) {
            $table->foreign('material_id')->references('id')->on('tsr_materiales');
            $table->foreign('caravana_id')->references('id')->on('tsr_caravanas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_materiales_caravanas', function (Blueprint $table) {
            $table->dropForeign(['material_id']);
            $table->dropForeign(['caravana_id']);
            $table->dropIndex('tsr_materiales_caravanas_material_id_foreign');
            $table->dropIndex('tsr_materiales_caravanas_caravana_id_foreign');
        });
    }
}
