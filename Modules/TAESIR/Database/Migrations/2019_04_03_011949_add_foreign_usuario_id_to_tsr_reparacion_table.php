<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrReparacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_reparaciones', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('operativo_id')->references('id')->on('tsr_operativos');
            $table->foreign('diagnostico_id')->references('id')->on('tsr_diagnosticos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_reparaciones', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropForeign(['diagnostico_id']);
            $table->dropForeign(['operativo_id']);
            $table->dropIndex('tsr_reparaciones_usuario_id_foreign');
            $table->dropIndex('tsr_reparaciones_diagnostico_id_foreign');
            $table->dropIndex('tsr_reparaciones_operativo_id_foreign');
           
        });
    }
}
