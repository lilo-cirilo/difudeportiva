<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrTrabajadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_trabajadores', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('codigo_trabajador');
            $table->string('area');
            $table->string('horario_trabajo');
            $table->unsignedinteger('persona_id');
            
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_trabajadores');
    }
}
