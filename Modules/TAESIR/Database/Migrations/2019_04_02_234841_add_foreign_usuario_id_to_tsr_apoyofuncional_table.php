<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignUsuarioIdToTsrApoyofuncionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tsr_apoyosfuncionales', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios');
            $table->foreign('apoyo_id')->references('id')->on('tsr_cat_apoyos');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tsr_apoyosfuncionales', function (Blueprint $table) {
            $table->dropForeign(['usuario_id']);
            $table->dropIndex('tsr_apoyosfuncionales_usuario_id_foreign');
            $table->dropForeign(['apoyo_id']);
            $table->dropIndex('tsr_apoyosfuncionales_apoyo_id_foreign');
        });
    }
}
