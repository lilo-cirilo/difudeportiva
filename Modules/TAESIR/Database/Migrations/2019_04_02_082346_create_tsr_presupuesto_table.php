<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrPresupuestoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_presupuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedinteger('servicio_id');
            $table->unsignedinteger('diagnostico_id');
            $table->unsignedinteger('cantidad');
            $table->float('costo',8,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_presupuestos');
        
    }
}
