<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrBeneficiarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_beneficiarios', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('curp');
            $table->boolean('solicitud_acuse');
            $table->boolean('acta_nacimiento');
            $table->boolean('comprobante_domicilio');
            $table->boolean('tipo');
            $table->string('responsable');
            $table->unsignedinteger('persona_id');

            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_beneficiarios');
    }
}
