<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTsrRequisicionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsr_requisiciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_ordendecompra');
            $table->date('fecha');
            $table->string('observaciones')->nullable();

            $table->unsignedinteger('usuario_id');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsr_requisiciones');
    }
}
