<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donativo extends Model
{
    use  SoftDeletes;
    protected $table='tsr_donativos';
    protected $appends=['persona'];
    protected $fillable = ['fecha','nombre','observaciones','estatus','persona_id','usuario_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    protected $with = ['Persona'];

    public function Persona(){
        return $this->belongsTo(persona::class,'persona_id','id');
   } 
   
   public function getPersonaAttribute(){
    $persona=(object) $this->belongsTo(Persona::class,'persona_id','id');
    return $persona;
}
public function Materiales(){
    return $this->belongsToMany(Materiales::class,'tsr_detalledonativos','donativo_id','material_id')->withPivot('cantidad','material_id');;
}

}
