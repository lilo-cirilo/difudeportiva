<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detalledonativo extends Model
{
    use SoftDeletes;
    protected $table='tsr_detalledonativos';
    protected $fillable = ['cantidad','donativo_id','material_id','historialentrada_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
