<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class catapoyofuncional extends Model
{
    use SoftDeletes;
     protected $table='tsr_cat_apoyos';
     protected $fillable = ['tipo','nombre'];
     protected $dates =['deleted_at'];
     protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    
     public function Apoyofuncional(){
        
        return $this->hasMany('Modules\TAESIR\Entities\Apoyofuncional','apoyo_id');
    }

}
