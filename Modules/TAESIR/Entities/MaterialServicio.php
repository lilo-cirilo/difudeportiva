<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class material_servicio extends Model
{
    use SoftDeletes;
    protected $table='tsr_materiales_servicios';
    protected $fillable = ['material_id','servicio_id','cantidad'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
