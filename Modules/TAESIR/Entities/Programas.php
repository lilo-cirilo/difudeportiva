<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;

class Programas extends Model
{
    protected $table = 'programas';
    protected $fillable = [];


    public function beneficios(){ 
        return $this->hasManyThrough(beneficiosprogramas::class,aniosprogramas::class,'programa_id','anio_programa_id','id','id');
    }

}
