<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table='personas';
    protected $appends=['full_name'];
    protected $fillable = ['nombre','primer_apellido','segundo_apellido','calle','numero_exterior','numero_interior','colonia','codigopostal','curp','genero','fecha_nacimiento','numero_celular','municipio_id'];
    protected $dates= ['deleted_at'];
    protected $hiden=array('created_at', 'updated_at', 'deleted_at');

    public function Beneficiario(){
        return $this->hasOne(beneficiario::class,'persona_id');
    }

    public function Trabajador(){
        return $this->hasOne(trabajador::class,'persona_id');
    }
    public function Donativo(){
        return $this->hasMany(donativo::class,'persona_id');
    }

    public function Operativo(){
        return $this->hasManyThrough(Operativo::class,Trabajador::class,'persona_id','trabajador_id','id','id');
    }

    public function getFullNameAttribute(){
        return $this->nombre.' '.$this->primer_apellido.' '.$this->segundo_apellido;
    }
}
