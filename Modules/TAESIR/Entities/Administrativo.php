<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class administrativo extends Model
{
    use SoftDeletes;
    protected $table = 'tsr_administrativos';
    protected $fillable = ['actividad','trabajador_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
