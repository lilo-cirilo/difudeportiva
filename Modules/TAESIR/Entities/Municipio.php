<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;

class municipio extends Model
{
    protected $table = 'cat_municipios';
    protected $fillable = ['nombre'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
