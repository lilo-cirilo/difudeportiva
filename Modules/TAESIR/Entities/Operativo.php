<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class operativo extends Model
{
    use SoftDeletes;
    protected $table ='tsr_operativos';
    protected $fillable = ['actividad','trabajador_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Persona(){
        return $this->has(Persona::class,Trabajador::class,'persona_id','trabajador_id');
    }



}
