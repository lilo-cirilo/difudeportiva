<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class presupuesto extends Model
{
    use SoftDeletes;
    protected $table='tsr_presupuestos';
    protected $fillable = ['subtotal','servicio_id','diagnostico_id','reparacion_id','usuario_id'];
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
