<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;

class unidadesmedida extends Model
{
    protected $table = 'cat_unidadmedidas';
    protected $fillable = ['unidadmedida'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
