<?php

namespace Modules\TAESIR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class prestamo extends Model
{
    use SoftDeletes;
    protected $table='tsr_prestamos';
    protected $fillable = ['folio','ApoyoFuncional_id','beneficiario_id','fecha_inicio','fecha_fin','fecha_devolucion','observaciones','estatus','usuario_id'];
    protected $dates=['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function Beneficiario(){
        return $this->belongsTo(beneficiario::class,'beneficiario_id');
    }
}
