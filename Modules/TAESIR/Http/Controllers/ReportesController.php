<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\TAESIR\Entities\diagnostico;
use Barryvdh\DomPDF\Facade as PDF;

class ReportesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function getDatos(Request $request)
    {
        $mesmin = 0;
        $mesmax = 0;

        if($request->tipo=='mensual'){
            return DB::table('tsr_reparaciones')
            ->join('tsr_diagnosticos', 'tsr_reparaciones.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_presupuestos', 'tsr_presupuestos.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_servicios', 'tsr_servicios.id', '=', 'tsr_presupuestos.servicio_id')
            ->join('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'tsr_servicios.beneficiosprogramas_id')
            ->whereMonth('tsr_diagnosticos.fecha', '=', $request->mes)
            ->whereYear('tsr_diagnosticos.fecha','=',date('Y'))
            ->select('beneficiosprogramas.nombre', DB::raw('count(*) as cantidad'))
            ->groupby('beneficiosprogramas.nombre')
            ->get();
        }

        if ($request->tipo == 'semestral') {
            switch ($request->semestre) {
                case 'sem1':
                    $mesmin = 1;
                    $mesmax = 6;
                    break;
                    case 'sem2':
                    $mesmin = 7;
                    $mesmax = 12;
                    break;
                    default:
                    break;
            }
        }

        if ($request->tipo == 'anual') {

                    $mesmin = 1;
                    $mesmax = 12;
     
            
        }

        if ($request->tipo == 'trimestral') {
            switch ($request->trimestre) {
                case 't1':
                    $mesmin = 1;
                    $mesmax = 3;
                    // return ['mes1'=>$mesmin,'mes2'=>$mesmax];
                    break;
                case 't2':
                    $mesmin = 4;
                    $mesmax = 6;
                    break;
                case 't3':
                    $mesmin = 7;
                    $mesmax = 9;
                    break;
                case 't4':
                    $mesmin = 10;
                    $mesmax = 12;
                    break;
                default:
                    break;
            }
        }



        $data = DB::table('tsr_reparaciones')
            ->join('tsr_diagnosticos', 'tsr_reparaciones.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_presupuestos', 'tsr_presupuestos.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_servicios', 'tsr_servicios.id', '=', 'tsr_presupuestos.servicio_id')
            ->join('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'tsr_servicios.beneficiosprogramas_id')
            ->whereMonth('tsr_diagnosticos.fecha', '>=', $mesmin)
            ->whereMonth('tsr_diagnosticos.fecha', '<=', $mesmax)
            ->whereYear('tsr_diagnosticos.fecha','=',date('Y'))
            ->select('beneficiosprogramas.nombre', DB::raw('count(*) as cantidad'))
            ->groupby('beneficiosprogramas.nombre')
            ->get();


        return $data;
    }

    public function index(Request $request)
    {
        /*     return $request;
        $base =((Object)$request->data)->base64;
        $pdf = PDF::loadView(
            'taesir::reporteTest',
            [
                'base64'=>$base,
            ]
        );
        
        */
        //return $pdf->stream();
        //return view('taesir::reporteTest');
        /* return DB::select('select bene.nombre, count(bene.nombre) as cantidad from (tsr_diagnosticos diag 
       inner join tsr_reparaciones rep 
       on rep.diagnostico_id=diag.id 
       inner join tsr_presupuestos pres on pres.diagnostico_id=diag.id
       inner join tsr_servicios serv on serv.id=pres.servicio_id
       inner JOIN beneficiosprogramas bene on bene.id = serv.beneficiosprogramas_id)
       GROUP BY bene.nombre;');*/

        $data = DB::table('tsr_reparaciones')
            ->join('tsr_diagnosticos', 'tsr_reparaciones.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_presupuestos', 'tsr_presupuestos.diagnostico_id', '=', 'tsr_diagnosticos.id')
            ->join('tsr_servicios', 'tsr_servicios.id', '=', 'tsr_presupuestos.servicio_id')
            ->join('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'tsr_servicios.beneficiosprogramas_id')
            ->whereMonth('tsr_diagnosticos.fecha', '>=', "1", 'and', 'tsr_diagnosticos.fecha', '<=', "12")
            ->select('beneficiosprogramas.nombre', DB::raw('count(*) as cantidad'))
            ->groupby('beneficiosprogramas.nombre')
            ->get();

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //return array($request->beneficios)[0];
        $base = $request->base64;
        $mes=$request->mes;

        
        $pdf = PDF::loadView(
            'taesir::reporteTest',
            [
                'base64' => $base,
                'servicios' => $request->beneficios,
                'mes'=>$mes,
                'tipo'=>$request->tipo
            ]
        );


        return $pdf->stream();
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('taesir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    { }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    { }

    public function gananciasXservicio(Request $request)
    {
        $mesmin = 0;
        $mesmax = 0;

        //10 servicios con mas ganancias
        if ($request->tipo == 'mensual') {
            if ($request->tiposervicio != 2) {
                $data = DB::table('beneficiosprogramas as ben')
                    ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                    ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                    ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                    ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                    ->where('serv.tipo', '=', $request->tiposervicio)
                    ->where('solicitud_acuse', '=', $request->bienestar)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select('ben.nombre', DB::raw('sum(serv.precio) as ganancia'),DB::raw('count(ben.nombre) as cantidad'))
                    ->groupby('ben.nombre')
                    ->get();

                //los que presentaron acuse
                $conacuse = DB::table('beneficiosprogramas as ben')
                    ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                    ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                    ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                    ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                    ->where('serv.tipo', '=', $request->tiposervicio)
                    ->where('solicitud_acuse', '=', $request->bienestar)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('count(ben.id) as todos'))
                    ->get();

                //total de ganancia por servicios proporcionados
                $totaltaller = DB::table('tsr_presupuestos as pre')
                    ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                    ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                    ->where('serv.tipo', '=', $request->tiposervicio)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('sum(serv.precio) as total'))
                    ->get();

                //total de servicios proporcionados por donmecanico
                $totaldonmecanico = DB::table('tsr_presupuestos as pre')
                    ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                    ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                    ->where('serv.tipo', '=', $request->tiposervicio)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('sum(serv.precio) as total'))
                    ->get();

                return ['datos' => $data, 'totalTaller' => $totaltaller, 'totaldonmecanico' => $totaldonmecanico, 'conacuse' => $conacuse];
            } else {
                $data = DB::table('beneficiosprogramas as ben')
                    ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                    ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                    ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                    ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                    ->where('solicitud_acuse', '=', $request->bienestar)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select('ben.nombre', DB::raw('sum(serv.precio) as ganancia'),DB::raw('count(ben.nombre) as cantidad'))
                    ->groupby('ben.nombre')
                    ->get();

                //los que presentaron acuse
                $conacuse = DB::table('beneficiosprogramas as ben')
                    ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                    ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                    ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                    ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                    ->where('solicitud_acuse', '=', $request->bienestar)
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('count(ben.id) as todos'))
                    ->get();

                //total de ganancia por servicios proporcionados
                $totaltaller = DB::table('tsr_presupuestos as pre')
                    ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                    ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('sum(serv.precio) as total'))
                    ->get();

                //total de servicios proporcionados por donmecanico
                $totaldonmecanico = DB::table('tsr_presupuestos as pre')
                    ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                    ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                    ->whereMonth('diag.fecha', '=', $request->mes)
                    ->whereYear('diag.fecha', '=', date('Y'))
                    ->select(DB::raw('sum(serv.precio) as total'))
                    ->get();
                return ['datos' => $data, 'totalTaller' => $totaltaller, 'totaldonmecanico' => $totaldonmecanico, 'conacuse' => $conacuse];
            }
        }

        if ($request->tipo == 'semestral') {
            switch ($request->semestre) {
                case 'sem1':
                    $mesmin = 1;
                    $mesmax = 6;
                    break;
                case 'sem2':
                    $mesmin = 7;
                    $mesmax = 12;
                    break;
                default:
                    break;
            }
        }

        if ($request->tipo == 'anual') {

            $mesmin = 1;
            $mesmax = 12;
        }

        if ($request->tipo == 'trimestral') {
            switch ($request->trimestre) {
                case 't1':
                    $mesmin = 1;
                    $mesmax = 3;
                    // return ['mes1'=>$mesmin,'mes2'=>$mesmax];
                    break;
                case 't2':
                    $mesmin = 4;
                    $mesmax = 6;
                    break;
                case 't3':
                    $mesmin = 7;
                    $mesmax = 9;
                    break;
                case 't4':
                    $mesmin = 10;
                    $mesmax = 12;
                    break;
                default:
                    break;
            }
        }



        if ($request->tiposervicio != 2) {
            $data = DB::table('beneficiosprogramas as ben')
                ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                ->where('serv.tipo', '=', $request->tiposervicio)
                ->where('solicitud_acuse', '=', $request->bienestar)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select('ben.nombre', DB::raw('sum(serv.precio) as ganancia'),DB::raw('count(ben.nombre) as cantidad'))
                ->groupby('ben.nombre')
                ->get();

            //los que presentaron acuse
            $conacuse = DB::table('beneficiosprogramas as ben')
                ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                ->where('serv.tipo', '=', $request->tiposervicio)
                ->where('solicitud_acuse', '=', $request->bienestar)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('count(ben.id) as todos'))
                ->get();

            //total de ganancia por servicios proporcionados
            $totaltaller = DB::table('tsr_presupuestos as pre')
                ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                ->where('serv.tipo', '=', $request->tiposervicio)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('sum(serv.precio) as total'))
                ->get();

            //total de servicios proporcionados por donmecanico
            $totaldonmecanico = DB::table('tsr_presupuestos as pre')
                ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                ->where('serv.tipo', '=', $request->tiposervicio)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('sum(serv.precio) as total'))
                ->get();

            return ['datos' => $data, 'totalTaller' => $totaltaller, 'totaldonmecanico' => $totaldonmecanico, 'conacuse' => $conacuse];
        } else {
            $data = DB::table('beneficiosprogramas as ben')
                ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                ->where('solicitud_acuse', '=', $request->bienestar)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select('ben.nombre', DB::raw('sum(serv.precio) as ganancia'),DB::raw('count(ben.nombre) as cantidad'))
                ->groupby('ben.nombre')
                ->get();

            //los que presentaron acuse
            $conacuse = DB::table('beneficiosprogramas as ben')
                ->join('tsr_servicios as serv', 'serv.beneficiosprogramas_id', '=', 'ben.id')
                ->join('tsr_presupuestos as pres', 'pres.servicio_id', '=', 'serv.id')
                ->join('tsr_diagnosticos as diag', 'diag.id', '=', 'pres.diagnostico_id')
                ->join('tsr_beneficiarios as benefi', 'benefi.id', '=', 'diag.beneficiario_id')
                ->where('solicitud_acuse', '=', $request->bienestar)
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('count(ben.id) as todos'))
                ->get();

            //total de ganancia por servicios proporcionados
            $totaltaller = DB::table('tsr_presupuestos as pre')
                ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('sum(serv.precio) as total'))
                ->get();

            //total de servicios proporcionados por donmecanico
            $totaldonmecanico = DB::table('tsr_presupuestos as pre')
                ->join('tsr_diagnosticos as diag', 'pre.diagnostico_id', '=', 'diag.id')
                ->join('tsr_servicios as serv', 'serv.id', '=', 'pre.servicio_id')
                ->whereMonth('diag.fecha', '>=', $mesmin)
                ->whereMonth('diag.fecha', '<=', $mesmax)
                ->whereYear('diag.fecha', '=', date('Y'))
                ->select(DB::raw('sum(serv.precio) as total'))
                ->get();

            return ['datos' => $data, 'totalTaller' => $totaltaller, 'totaldonmecanico' => $totaldonmecanico, 'conacuse' => $conacuse];
        }
    }

    public function getAnios()
    {
        return DB::table('tsr_diagnosticos as diag')
            ->select(DB::raw('extract(YEAR FROM diag.fecha) as anio'))
            ->distinct()
            ->get();
    }
}
