<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\serviciosbeneficios;
use Modules\TAESIR\Entities\beneficiosprogramas;
use Modules\TAESIR\Entities\aniosprogramas;
use Illuminate\Support\Facades\DB;


class BeneficiosController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
 /*   
        $programa_id =aniosprogramas::where('programa_id','=','590')->get()[0]->id;
        $beneficios = beneficiosprogramas::where("anio_programa_id","=","$programa_id")->get();
       //return ['servicios'=>$beneficios];

        foreach($beneficios as $key => $beni){
            $service = new serviciosbeneficios();
            $service->beneficiosprogramas_id=$beni->id;
            $service->precio=0.0;
            $service->save();
        }
*/
//$benes =serviciosbeneficios::get()->where('tipo','=',0);
//return ['servicios'=>$benes];

return $benes = beneficiosprogramas::with('serviciosbeneficios')->whereHas('serviciosbeneficios', function($query){
   $query->where('tipo','=',0);
})->get();
return ['servicios'=>$benes];

$benes =beneficiosprogramas::with('serviciosbeneficios')
       ->has('serviciosbeneficios')
       ->get();
       return ['servicios'=>$benes];


       //return ['servicios'=>serviciosbeneficios::with('beneficiosprogramas')->get()];

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('taesir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
