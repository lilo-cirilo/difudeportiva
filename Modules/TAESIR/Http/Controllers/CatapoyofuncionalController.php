<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Catapoyofuncional;

class CatapoyofuncionalController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
     $apoyofuncional=Catapoyofuncional::get();
        return $apoyofuncional;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $apoyofuncional = new Catapoyofuncional();
        $apoyofuncional->nombre = $request->nombre;
        $apoyofuncional->tipo = $request->tipo;
         $apoyofuncional->save();
        return $apoyofuncional;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $Catapoyofuncional = Catapoyofuncional::find($id);
        return $Catapoyofuncional;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
