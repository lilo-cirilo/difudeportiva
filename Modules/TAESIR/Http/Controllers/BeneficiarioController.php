<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Municipio;
use Modules\TAESIR\Entities\Beneficiario;
use Modules\TAESIR\Entities\Persona;
use Modules\TAESIR\Entities\Operativo;
use Modules\TAESIR\Entities\prestamo;
use Carbon\Carbon;

class BeneficiarioController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function buscarbeneficiario(Request $request){
        
        $beneficiario = Beneficiario::
        where('persona_id','=',$request->buscar)->get();
        return ['beneficiario'=>$beneficiario];
    }

    public function buscarPersona(Request $request)
    {
        $columns = ['nombre', 'primer_apellido', 'segundo_apellido'];
        $term = $request->curp;
        $words_search = explode(" ", $term);
        $personaOption = Persona::has('Beneficiario')->where(function ($query) use ($columns, $words_search) {
            foreach ($words_search as $word) {
                $query = $query->where(function ($query) use ($columns, $word) {
                    foreach ($columns as $column) {
                        $query->orWhere($column, 'like', "%$word%");
                    }
                });
            }
        });
        $personaOption = $personaOption->paginate(6);
        return $personaOption;
    }

    public function getMunicipios()
    {
        $municipio = Municipio::get();
        return ['municipio' => $municipio];
    }

    public function postPersona(Request $request)
    {
        $fechainicio=Carbon::createFromFormat('d/m/Y', $request->fecha_nace);
        $perso = new Persona();
        $perso->nombre = strtoupper($request->nombre);
        $perso->primer_apellido = strtoupper($request->a_paterno);
        $perso->segundo_apellido = strtoupper($request->a_materno);
        $perso->calle = strtoupper($request->calle);
        $perso->numero_exterior = $request->num_ext;
        $perso->numero_interior = $request->num_int;
        $perso->colonia = strtoupper($request->colonia);
        $perso->codigopostal = strtoupper($request->cod_postal);
        $perso->curp =strtoupper($request->curp);
        $perso->fecha_nacimiento =Carbon::parse($fechainicio)->format('Y/m/d');
        $perso->genero = $request->genero;
        $perso->numero_celular = $request->numero_celular;
        $perso->municipio_id = $request->municipio;
        $perso->save();
        return ['persona' => $perso->id];
    }

    public function index()
    {
        // $beni = Beneficiario::with('Persona')->get();
        //return $beni;

        //$var = Persona::has('Beneficiario')->get()->where("full_name","like","%llave%");
        //return ['beneficiarios'=>$var];


        //$per= Persona::find(2)->Operativo;
        //return $per[0]->id;

        $operativo = Persona::has('Operativo')->get();
        //$beneficiario = Beneficiario::all();
        return $operativo;
        //return $beneficiario;
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $beneficiario = new Beneficiario();
        $beneficiario->curp = $request->curp;
        $beneficiario->solicitud_acuse = $request->acuse;
        $beneficiario->acta_nacimiento = $request->acta;
        $beneficiario->comprobante_domicilio = $request->comprobante;
        $beneficiario->responsable = strtoupper($request->responsable);
        $beneficiario->persona_id = $request->persona_id;
        $beneficiario->tipo = 0;
        $beneficiario->save();
        return ['beneficiario_id' => $beneficiario->id];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($variable)
    {
        $beneficiario = Beneficiario::where("persona_id", "=", "$variable")->get();
        /*$var = Persona::has('Beneficiario')->where("nombre","LIKE","%$variable%")
       ->orWhere("primer_apellido","LIKE","%$variable%")->get();
       return ['beneficiarios'=>$var];*/

        $id_beneficiario = $beneficiario[0]->id;
        // return ['beneficiario_id'=>$beneficiario[0]->id];
         $existe = prestamo::where("beneficiario_id", "=", "$id_beneficiario")->where('estatus','=','1')->get();

        if ($existe->isEmpty()) {
            return ['beneficiario_id' => $id_beneficiario];
        } else {
            return ['beneficiario_id' => 0];;
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    { }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    { }
}
