<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Donativo;
use Modules\TAESIR\Entities\Persona;
use Illuminate\Support\Facades\DB;



class DonativoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function buscarPersona(Request $request)
    {
        $columns = ['nombre', 'primer_apellido', 'segundo_apellido','numero_celular'];
        $term = $request->curp;
        $words_search = explode(" ", $term);
        $personaOption = Persona::has('Donativo')->where(function ($query) use ($columns, $words_search) {
            foreach ($words_search as $word) {
                $query = $query->where(function ($query) use ($columns, $word) {
                    foreach ($columns as $column) {
                        $query->orWhere($column, 'like', "%$word%");
                    }
                });
            }
        });
        $personaOption = $personaOption->paginate(6);
        return $personaOption;
    }
    public function postPersona(Request $request)
    {
        $perso = new Persona();
        $perso->nombre = $request->nombre;
        $perso->primer_apellido = $request->primer_apellido;
        $perso->segundo_apellido = $request->segundo_apellido;
        $perso->numero_celular = $request->numero_celular;
        $perso->municipio_id = $request->municipio;
        $perso->save();
        return ['persona' => $perso->id];
    }

    public function getDonativosRegistrados(){
        $donativo = Donativo::
        where("estatus","=","REGISTRADO")->get();
        return ['donativo'=>$donativo];
    }
    public function getDonativosSinRegistro(){
        $donativo = Donativo::
        where("estatus","=","SIN REGISTRO")->get();
        return ['donativo'=>$donativo];
    }
    
    public function index()
    {
       $donativo = Donativo::with('persona')->get();
        return ['donativo'=>$donativo];
    }
    public function materialdonativo($id){
       $arreglo=DB::table("tsr_materiales as mat")->join("tsr_detalledonativos as det","mat.id","=","det.material_id")->join("tsr_donativos as don","don.id","=","det.donativo_id")
       ->select("mat.nombre","mat.presentacion","det.cantidad")->where("don.id","=",$id)->get();

       return['donativo'=>$arreglo];
    }

   

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
        {
            $donativo= new Donativo();
            $donativo->fecha=$request->fecha;
            $donativo->nombre=$request->nombre;
           $donativo->observaciones=$request->observaciones;
           $donativo->estatus=$request->estatus;
           $donativo->persona_id=$request->persona_id;
           $donativo->usuario_id=auth()->user()->id;
           $donativo->save();
           return $donativo;
           
        }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
       $donativo = Donativo::find($id);
       return $donativo;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         Donativo::destroy($id);
        return"eliminado satisfactoriamente";
    }
}
