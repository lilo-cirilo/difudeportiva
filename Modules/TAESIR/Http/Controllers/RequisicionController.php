<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Requisicion;



class RequisicionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    
    public function buscarfolio(Request $request){
        
        $requisicion = Requisicion::
        where('folio_ordendecompra','=',$request->buscar)->get();
        return ['requisicion'=>$requisicion];
    }

      public function index(){
      $ordenes = Requisicion::with('Materiales')->get();
         return ['requisicion'=>$ordenes];

    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
        {
            $requisicion= new Requisicion();
            $requisicion->folio_ordendecompra=$request->folio_ordendecompra;
            $requisicion->fecha=$request->fecha;
           $requisicion->observaciones=$request->observaciones;
           $requisicion->usuario_id=auth()->user()->id;
           $requisicion->save();
           return $requisicion->id;
           
        }


    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
       $requisicion = Requisicion::find($id);
       return $requisicion;
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
         Requisicion::destroy($id);
        return"eliminado satisfactoriamente";
    }
}
