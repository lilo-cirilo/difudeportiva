<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Diagnostico;
use Modules\TAESIR\Entities\Persona;
use Modules\TAESIR\Entities\serviciosbeneficios;
use Modules\TAESIR\Entities\beneficiario;
use Modules\TAESIR\Entities\reparacion;

use Barryvdh\DomPDF\Facade as PDF;


class DiagnosticoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function generarPDF($id)
    {
        
        $reparacion = reparacion::find($id);
        $diag = Diagnostico::with('Servicios')->find($reparacion->diagnostico_id); //Diagnostico::with('Servicios')->where('diagnostico_id','=','1')->get();
        $bene_id=$diag->beneficiario_id;
        $subt =  $diag->subtotal;
        $total=$reparacion->total;
        $descuento=$reparacion->descuento;
        $diag = array($diag->servicios);
        
        $nombre=(Object)beneficiario::find($bene_id)->persona->get();;
        $nombre=array($nombre)[0][0];
        $nombre= $nombre->nombre.' '.$nombre->primer_apellido.' '.$nombre->segundo_apellido;


        $pdf    = PDF::setOptions([
            'images' => true,
            'isPhpEnabled'=>true,
            'DOMPDF_ENABLE_PHP'=>true
        ]);
        
      


        $pdf = PDF::loadView(
            'taesir::reciboreparacion',
            [
                'diagnosticos' => $diag[0],
                'subtotal' => $subt,
                'total'=>$total,
                'descuento'=>$descuento,
                'folio'=> $reparacion->folio,
                'beneficiario'=>$nombre
            ]
        );
        $pdf->setPaper("A5","landscape");


        

       // $pdf->render();
        
        return $pdf->download($reparacion->folio.'_'.$nombre.'.pdf');
    }

    public function index()
    {

     //   $nombre=(Object)beneficiario::find(14)->persona->get();;
       // $nombre=array($nombre)[0][0];
        //return $nombre->nombre.' '.$nombre->primer_apellido.' '.$nombre->segundo_apellido;
   
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $per = Persona::find($request->operativo_id)->Operativo;

        $diagnostico = new Diagnostico();
        $diagnostico->usuario_id = auth()->user()->id;
        $diagnostico->beneficiario_id = $request->beneficiario_id;
        $diagnostico->operativo_id = $per[0]->id;
        $diagnostico->fecha = $request->fecha;
        $diagnostico->subtotal = $request->subtotal;
        $diagnostico->save();

        $diagnostico_id = $diagnostico->id;

        $diag = Diagnostico::find($diagnostico_id);
        $serv = $request->servicios;
        /*
       foreach($serv as $key => $servicio){
            $servicito=(Object)$servicio;
            $diag->Servicios()->attach($servicito->id);
        }
*/        

        foreach ($serv as $key => $servicio) {
            $servicito = (Object)$servicio;
            $diag->Servicios()->attach($servicito->id, [
                'cantidad' => $servicito->cantidad, //agregar lo que llega en la request
                'costo' => $servicito->subtotalservicio, //agregar lo que llega de la request
            ]);
        }


        return ['diagnostico_id' => $diagnostico_id];
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('taesir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    { 
        $reparacion=reparacion::find($id);
        $reparacion->estatus=0;
        $reparacion->save();
        return 'ok';
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    { }
}
