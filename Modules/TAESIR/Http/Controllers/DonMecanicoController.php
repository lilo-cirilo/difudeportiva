<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use function GuzzleHttp\json_decode;
use Illuminate\Support\Facades\DB;
use Modules\TAESIR\Entities\Persona;
use App\Models\Usuario;
use Modules\TAESIR\Entities\beneficiario;
use Modules\TAESIR\Entities\diagnostico;
use Modules\TAESIR\Entities\reparacion;
use Modules\TAESIR\Entities\Caravana;
use Mockery\Exception;
use Illuminate\Support\Facades\Auth;
use App\Models\Modulo;
use \Firebase\JWT\JWT;
use Carbon\Carbon;
use App\Models\ConsultasMedicas\Diagnostico as AppDiagnostico;

class DonMecanicoController extends Controller
{
    /**
     * 
     * 
     * Display a listing of the resource.
     * @return Response
     */
    public function index()


    {

        return DB::TABLE('tsr_caravanas as cara')
            ->join('tsr_materiales_caravanas as mc', 'mc.caravana_id', '=', 'cara.id')
            ->join('tsr_materiales as mat', 'mat.id', '=', 'mc.material_id')
            ->where('cara.notificado', '=', 'false')
            ->where('operativo_id', '=', 3)
            ->where('status', '=', '1')
            ->select('cara.id as caravana_id', 'mc.material_id', 'mat.nombre', 'cantidad')
            ->get();

        $r = DB::TABLE('personas as per')
            ->join('tsr_trabajadores as tra', 'tra.persona_id', '=', 'per.id')
            ->join('tsr_operativos as op', 'op.trabajador_id', '=', 'tra.id')
            ->where('per.id', '=', '632272')
            ->select('op.id');
        // return $r->get();        //return Persona::has('Operativo')->with('Operativo')->get();

        return DB::TABLE('tsr_caravanas as cara')
            ->join('tsr_materiales_caravanas as mc', 'mc.caravana_id', '=', 'cara.id')
            ->join('tsr_materiales as mat', 'mat.id', '=', 'mc.material_id')
            ->where('cara.notificado', '=', 'false')
            ->where('operativo_id', '=', 2)
            ->where('status', '=', '1')
            ->select('cara.id as caravana_id', 'mc.material_id', 'mat.nombre', 'cantidad')
            ->get();


        $salida = array(
            'mensaje' => 'perra', 'mensaje2' => 'mensj'
        );


        //return response()->json(['prueba'=>['mensaje'=>'todo salio bien perra']]);
        //return 'Todo Salio Ok';

        return response()->json(["prueba" => ($salida["mensaje"])]);
    }

    public function store(Request $request)
    {

        $personas = json_decode($request->personas);
        $beneficiarios = json_decode($request->beneficiarios);
        $diagnosticos = json_decode($request->diagnosticos);
        $presupuestos = json_decode($request->presupuestos);
        $reparaciones = json_decode($request->reparaciones);

        //return $personas[14]->fecha_nacimiento;
        try {

            DB::beginTransaction();
            for ($i = 0; $i < sizeof($personas); $i++) {
                $perso = new Persona();
                $perso->nombre = strtoupper($personas[$i]->nombre);
                $perso->primer_apellido = strtoupper($personas[$i]->a_paterno);
                $perso->segundo_apellido = strtoupper($personas[$i]->a_materno);
                $perso->calle = strtoupper($personas[$i]->calle);
                $perso->numero_exterior = $personas[$i]->num_exterior;
                $perso->numero_interior = $personas[$i]->num_interior;
                $perso->colonia = strtoupper($personas[$i]->colonia);
                $perso->codigopostal = strtoupper($personas[$i]->codigo_postal);
                $perso->curp = strtoupper($personas[$i]->curp);
                $perso->fecha_nacimiento = $personas[$i]->fecha_nacimiento; //$request->fecha_nace;
                $perso->genero = $personas[$i]->genero;
                $perso->numero_celular = $personas[$i]->telefono;
                $perso->municipio_id = $personas[$i]->municipio_id;
                $perso->save();

                $auxbene = $this->buscaBeneficiario($beneficiarios, $personas[$i]->id);
                $bene = new beneficiario();
                $bene->curp = 0;
                $bene->solicitud_acuse = 0;
                $bene->acta_nacimiento = 0;
                $bene->comprobante_domicilio = 0;
                $bene->tipo = 0;
                $bene->responsable = strtoupper($auxbene->responsable);
                $bene->persona_id = $perso->id;
                $bene->save();

                $auxdiag = $this->buscarDiagnostico($auxbene->id, $diagnosticos);

                //return $auxdiag->fecha;
                $diagnos = new Diagnostico();
                $diagnos->beneficiario_id = $bene->id;
                $diagnos->operativo_id = 1;
                $diagnos->fecha = $auxdiag->fecha;
                $diagnos->usuario_id = 73;
                $diagnos->subtotal = $auxdiag->subtotal;
                $diagnos->save();

                foreach ($presupuestos as $key => $servicio) {
                    //$servicito = (Object)$servicio;
                    if ($servicio->diagnostico_id == $auxdiag->id) {
                        $diagnos->Servicios()->attach($servicio->servicio_id, [
                            'cantidad' => $servicio->cantidad, //agregar lo que llega en la request
                            'costo' => $servicio->costo, //agregar lo que llega de la request
                        ]);
                    }
                }

                $auxrepa = $this->buscarReparacion($reparaciones, $auxdiag->id);

                $repara = new reparacion();
                $repara->num_orden = 0;
                $repara->folio = $auxrepa->folio;
                $repara->caracteristicas = "NO HAY";
                $repara->estatus = "1";
                $repara->tipo=1;
                $repara->observaciones = "NO HAY";
                $repara->total = $auxrepa->total;
                $repara->descuento = $auxrepa->descuento;
                $repara->usuario_id = 73;
                $repara->diagnostico_id = $diagnos->id;
                $repara->operativo_id = 1;
                $repara->save();
                $repara->folio = "TSR-F" . $repara->id;
                $repara->save();


                DB::commit();
            }
        } catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
        }
        return $repara->id;


        // $personas = json_decode($personas);

    }

    public function buscaBeneficiario($beneficiarios, $persona_id_provicional)
    {
        foreach ($beneficiarios as $ben) {
            if ($ben->persona_id == $persona_id_provicional) {
                return $ben;
            }
        }
    }

    public function buscarDiagnostico($beneficiario_id, $diagnosticos)
    {
        foreach ($diagnosticos as $diag) {
            if ($diag->beneficiario_id == $beneficiario_id) {
                return $diag;
            }
        }
    }

    public function buscarReparacion($reparaciones, $diagnostico_id)
    {
        foreach ($reparaciones as $repa) {
            if ($repa->diagnostico_id == $diagnostico_id) {
                return $repa;
            }
        }
    }


    public function login(Request $request)
    {
        //return (response()->json(['Usuario' => $request->usuario]));
        if (Auth::attempt(['usuario' => $request->usuario, 'password' => $request->password])) {
            $usuario = auth()->user();
            $token = [
                'usuario_id' => $usuario->id,
                'modulo_id' => Modulo::where('nombre', 'like', 'taesir')->first()->id,
                'rol_id' => '1'
            ];
            $encoded = JWT::encode($token, config('app.jwt_taesir_token'), 'HS256');
            $usuario->token = $encoded;
            $usuario->vida_token = Carbon::now()->addMinutes(60);
            $usuario->save();
            $persona = Persona::find(auth()->user()->persona_id);

            $r = DB::TABLE('personas as per')
                ->join('tsr_trabajadores as tra', 'tra.persona_id', '=', 'per.id')
                ->join('tsr_operativos as op', 'op.trabajador_id', '=', 'tra.id')
                ->where('per.id', '=', "$persona->id")
                ->select('op.id')
                ->get();




            return ['usuario' => auth()->user()->id, 'persona' => $persona->nombre, 'operativo_id' => $r, 'token' => $encoded];
        }
        return response()->json(['error' => 'Verifique sus credenciales']);
    }


    public function getMateriales($operativo_id)
    {
        //return response()->json(['valor'=>'si entra']);
        try {
          $caravana = Caravana::where('operativo_id', '=', $operativo_id)
                ->where('status', '=', '1')
                ->where('notificado', '=', 'false')
                ->get();
                $caravana= $caravana[0];

            $datos = DB::TABLE('tsr_caravanas as cara')
                ->join('tsr_materiales_caravanas as mc', 'mc.caravana_id', '=', 'cara.id')
                ->join('tsr_materiales as mat', 'mat.id', '=', 'mc.material_id')
                ->where('cara.id','=',$caravana->id)
                ->select('cara.id as caravana_id', 'mc.material_id', 'mat.nombre', 'cantidad')
                ->get();
                return $datos;
        } catch (Exception $e) { }
    }

    public function getMunicipiosGiras(){
     return   DB::table('even_eventos as even')
     ->join('even_cat_tiposevento as teven','teven.id','=','even.tipoevento_id')
     ->join('atnc_cat_giras as gir','gir.id','=','even.gira_id')
     ->join('cat_municipios as mun','mun.id','=','even.municipio_id')
     ->where('gir.nombre','like','don mecanico')
     ->whereDay('even.fecha','=',Date('d'))
     ->whereMonth('even.fecha','=',Date('m'))
     ->whereYear('even.fecha','=',Date('Y'))
     ->select('municipio_id','mun.nombre')
     ->get();
    }
}
