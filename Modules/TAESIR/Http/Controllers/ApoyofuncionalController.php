<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Apoyofuncional;
use Modules\TAESIR\Entities\Catapoyofuncional;

class ApoyofuncionalController extends Controller
{
    public function getApoyosDisponibles(){
        $apoyofuncional = Apoyofuncional::with('catapoyofuncional')
        ->where("estatus","=","Disponible")->get();
        return ['apoyofuncional'=>$apoyofuncional];
    }

    public function getApoyosPrestados(){
        $apoyofuncional = Apoyofuncional::with('catapoyofuncional')
        ->where("estatus","=","Prestado")->get();
        return ['apoyofuncional'=>$apoyofuncional];
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $cata=Catapoyofuncional::all();
        $apoyofuncional = Apoyofuncional::with('catapoyofuncional')->get();
        return ['apoyofuncional'=>$apoyofuncional,'cata'=>$cata];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $apoyo= new Apoyofuncional();
        $apoyo->num_inventario=$request->num_inventario;
        $apoyo->marca=$request->marca;
        $apoyo->caracteristicas=$request->caracteristicas;
        $apoyo->estatus=$request->estatus;
        $apoyo->condicion=$request->condicion;
        $apoyo->apoyo_id=$request->apoyo_id;
        $apoyo->usuario_id=auth()->user()->id;
        $apoyo->save();
        return $apoyo;

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $apoyofuncional = Apoyofuncional::find($id);
        return $apoyofuncional;
       
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $apoyo = Apoyofuncional::findOrFail($id);
        $apoyo->update($request->all());
        return $apoyo;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        Apoyofuncional::destroy($id);
        return 'Eliminado satisfactoriamente';
    }
}
