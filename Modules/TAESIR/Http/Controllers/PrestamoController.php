<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Prestamo;
use Modules\TAESIR\Entities\apoyofuncional;
use Modules\TAESIR\Entities\Persona;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;

class PrestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $prestamo = Prestamo::with('Beneficiario')->where('estatus','=','1')->get();
        return ['prestamo'=>$prestamo];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
        $fechainicio=Carbon::createFromFormat('d/m/Y', $request->fecha_inicio);
        //Carbon::parse($request->fecha_inicio)->format('Y-m-d');
        $prestamo = new Prestamo();
        $prestamo->beneficiario_id=$request->beneficiario_id;
        $prestamo->apoyofuncional_id=$request->apoyofuncional_id;
        $prestamo->administrativo_id=1;
        $prestamo->fecha_inicio=date_format($fechainicio,'Y-m-d');
        $prestamo->fecha_fin=Carbon::createFromFormat('d/m/Y', $request->fecha_fin);//$request->fecha_fin;
        $prestamo->estatus=$request->estatus;
        $prestamo->usuario_id=auth()->user()->id;
        $prestamo->observaciones=strtoupper($request->observaciones);
        $prestamo->save();
        $prestamo->folio='TSR-PREST'.$prestamo->id;
        $prestamo->save();
        $apoyo = apoyofuncional::find($request->apoyofuncional_id);
        $apoyo->estatus='Prestado';
        $apoyo->save();
      return $prestamo->id;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $pres =Prestamo::findOrFail($id);
        $pres= $pres->beneficiario;
        return Persona::find($pres->persona_id);

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $prestamo=Prestamo::find($id);
        $prestamo->estatus=0;
        $prestamo->fecha_devolucion=$request->fecha_devolucion;
        $prestamo->save();

        $apoyo = apoyofuncional::find($prestamo->ApoyoFuncional_id);
        $apoyo->estatus='Disponible';
        $apoyo->save();

        return "todo salio bien";
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function reciboPrestamo($id){
       
          $pres = Prestamo::with('Beneficiario')->find($id);
         $folio=$pres->folio; 
         $persona=$pres->beneficiario->persona->get();
         $persona=$persona[0]->nombre." ".$persona[0]->primer_apellido." ".$persona[0]->segundo_apellido;
          $apoyofuncional=apoyofuncional::with('catapoyofuncional')->find($pres->ApoyoFuncional_id)->catapoyofuncional;
         $num_inventario=apoyofuncional::with('catapoyofuncional')->find($pres->ApoyoFuncional_id)->num_inventario;
        $apoyofuncional=$apoyofuncional->nombre." DE TIPO ".$apoyofuncional->tipo;
         $fecha_devolucion=$pres->fecha_fin;

        $pdf = PDF::loadView(
            'taesir::reciboprestamo',
            [
                'folio' => $folio,
                'persona' => $persona,
                'apoyo_funcional'=> $apoyofuncional,
                'fecha_devolucion'=>$fecha_devolucion,
                'numeroinventario'=>$num_inventario
            ]
        );
        $pdf->setPaper("A5","landscape");
        
        return $pdf->download($folio.'_'.$persona.'.pdf');
    }
}
