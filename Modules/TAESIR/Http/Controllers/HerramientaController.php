<?php

namespace Modules\TAESIR\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\TAESIR\Entities\Herramienta;


class HerramientaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */


    public function index()
    {
      $herramientas = Herramienta::get();
     // return Herramienta::paginate(10);
      return ['herramientas'=>$herramientas];
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('taesir::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

      $herramienta = new Herramienta();
      $herramienta->nombre = $request->nombre;
      $herramienta->num_inventario= $request->num_inventario;
      $herramienta->estatus = $request->estatus;
      $herramienta->observaciones = $request->observaciones;
      $herramienta->save();
      return $herramienta;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $herramienta = Herramienta::find($id);
        return $herramienta;
        //return view('taesir::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('taesir::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request,$id)
    {
      $herramienta = Herramienta::findOrFail($id);
      $herramienta->update($request->all());
      return $herramienta;
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
      Herramienta::destroy($id);
      return 'Eliminado satisfactoriamente';
    }
}
