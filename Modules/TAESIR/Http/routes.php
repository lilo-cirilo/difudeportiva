<?php
use Modules\TAESIR\Http\Controllers\ReparacionController;

Route::group(['prefix' => 'taesir', 'namespace' => 'Modules\TAESIR\Http\Controllers'], function () {

  Route::group(['middleware' => 'web'],function(){
    Route::get('/', 'TAESIRController@index');


    //Route::get('/herramientas', 'HerramientaController@index');
    //rutas para el controlador de herramientas y su respectiva vista
    Route::get('/herramientas/listado', 'TAESIRController@herramientas')->name('taesir.herramientas.listado');
    Route::resource('/herra', 'HerramientaController');
  
    //rutas para el controlador de materiales y su respectiva vista
    Route::get('/materiales/listado', 'TAESIRController@materiales')->name('taesir.materiales.listado');
   
    Route::resource('/materiap', 'MaterialesController');
    Route::post('/buscarfolio', 'RequisicionController@buscarfolio');
    Route::post('/ordenbeneficiario','BeneficiarioController@buscarbeneficiario');
    Route::get('/unidadesp', 'MaterialesController@getUnidades');
  
    //ruta para el controlador de apoyos funcionales
    Route::resource('/apoyosfuncionales', 'ApoyofuncionalController');
    //ruta para el controlador de apoyos funcionales disponibles
    Route::get('/apoyosfuncionalesdisponibles', 'ApoyofuncionalController@getApoyosDisponibles');
    //ruta para el controlador de apoyos funcionales prestados
    Route::get('/apoyosfuncionalesprestados', 'ApoyofuncionalController@getApoyosPrestados');
  
    Route::get('/donativosregistrados','DonativoController@getDonativosRegistrados');
    Route::get('/donativossinregistro','DonativoController@getDonativosSinRegistro');
  
     //ruta para el controlador de servicios
     Route::get('/catservicio','CatservicioController@index');
     //route for municipios
     Route::get('/munici','BeneficiarioController@getMunicipios');
     //route for personas 
     Route::post('/altapersona','BeneficiarioController@postPersona');
    //rutas para el controlador del catalogo de apoyos funcionales
    Route::resource('/cataf', 'CatapoyofuncionalController');
  
    //ruta para el controlador de servicios
    Route::get('/catservicio', 'CatservicioController@index');
    //route from beneficiarios
    Route::resource('/tsrbeneficiarios', 'BeneficiarioController');
    //route para los diagnosticos
    Route::resource('/diagcontrol', 'DiagnosticoController');
    Route::get('/reciboreparacion/{id}','DiagnosticoController@generarPDF');
    //route para las reparaciones
    Route::resource('/controlreparaciones', 'ReparacionController');
    Route::get('/controlreparacionesf/{tipo}','ReparacionController@index');
   
  
     Route::resource('/tsrtrabajador','TrabajadorController');
     //route para donativos
     Route::resource('/controldonativos','DonativoController');
     Route::resource('/controlrequisiciones','RequisicionController');
    
     Route::resource('/controlcataf','CatapoyofuncionalController');
     Route::post('/altapersonadonativo','DonativoController@postPersona');
    //route para donativos
  
    //ruta para beneficios
    Route::resource('/getbeneficios','BeneficiosController');
  
    Route::post('/buscarpersona', 'BeneficiarioController@buscarPersona');
    Route::post('/buscardonante', 'DonativoController@buscarPersona');
    Route::resource('/tsrprestamos','PrestamoController');
    Route::get('/reciboprestamo/{id}','PrestamoController@reciboPrestamo');


    //Route::put('/tsrprestamos/{id}/{id2}','PrestamoController@update');
    Route::get('/obtenerserviciosagregados/{id}','ReparacionController@agregarServicios');
  
    Route::post('/descontarmaterial','MaterialesController@descontarMaterial');
    Route::post('/descontarmaterialcaravana','MaterialesController@descontarMaterialCaravana');
    Route::resource('/reportes','ReportesController');
    Route::post('/reportespost','ReportesController@getDatos');
    Route::post('/reportesganancias','ReportesController@gananciasXservicio');
		Route::get('/aniosReparaciones','ReportesController@getAnios');



    Route::post('/agregarmaterial','MaterialesController@agregarMaterial');
    Route::post('/ordenmaterial','MaterialesController@ordenMaterial');
  
    Route::get('/reparacionesmes','DashboardController@getReparacionesMes');
    Route::get('/prestamosmes','DashboardController@getPrestamosMes');
    Route::get('/donativosmes','DashboardController@getDonacionesMes');
  
    Route::resource('/ordenescompra', 'RequisicionController');
    Route::get('/materialesdonativos/{id}','DonativoController@materialdonativo');
  });

  Route::group(['prefix'=>'donmecanico'],function(){
    Route::resource('/movil','DonMecanicoController');
    Route::get('/materiales/{id}','DonMecanicoController@getMateriales');
    Route::post('/login','DonMecanicoController@login');
    Route::get('/municipiosgiras','DonMecanicoController@getMunicipiosGiras');
  });
  

});
