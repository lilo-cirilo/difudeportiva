<?php

namespace Modules\AtnCiudadana\Entities;

use Illuminate\Database\Eloquent\Model;

class SqlSolicitudes extends Model {
    protected $connection = 'sqlsrv';

    protected $table = 'BdSICOPE.dbo.v_Peticiones as p';

    protected $fillable = [];
}