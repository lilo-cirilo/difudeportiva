@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); $tipo_sol = app('request')->input('tipo_solicitud') ?>

@if(auth()->check())
    @if(file_exists(asset(auth()->user()->persona->get_url_fotografia())))
        @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
    @else
        @section('user-avatar', asset('images/user.png'))
    @endif
  @section('user-name', auth()->user()->persona->nombre)
  @section('user-job')
  @section('user-log', auth()->user()->created_at)
@endif

@section('content-title', 'Atención Ciudadana')

@section('breadcrumbs')
  <ol class="breadcrumb">
    {{-- <li><a href="{{ route('atnciudadana.home') }}"><i class="fa fa-dashboard"></i>AtnCiudadana</a></li> --}}
    @yield('li-breadcrumbs')
  </ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu" data-widget="tree">
	<li class="header active">Atención Ciudadana</li>

	<li>
		<a href="{{ route('home') }}">
			<i class="fa fa-home"></i><span>IntraDIF</span>
		</a>
    </li>


    @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'atnciudadana'))
	<li>
		<a href="{{ route('atnciudadana.peticiones.direcciones') }}">
			<i class="fa fa-bar-chart"></i><span>Dashboard</span>
		</a>
    </li>
    @endif

	<li>
		<a href="{{ route('atnciudadana.peticiones.personas') }}">
			<i class="fa fa-search"></i><span>Búsqueda beneficiarios</span>
		</a>
	</li>

	<li class="{{ (($ruta === 'solicitudes.index' || $ruta === 'atnciudadana.peticiones.general' || $ruta === 'atnciudadana.beneficiarios' || $ruta === 'solicitudes.create') && $tipo_sol === 'i') ? 'treeview active menu-open' : 'treeview' }}">
		<a class="{{ ($ruta === 'atnciudadana.programas.index' || $ruta === 'atnciudadana.dependencias.index' ) ? 'home' : '' }}" href="#">
        <i class="fa fa-group"></i><span>Solicitudes Internas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li {{ ($ruta === 'solicitudes.create' && $tipo_sol === 'i') ? 'class=active' : '' }}><a href="{{ route('solicitudes.create', ['tipo_solicitud' => 'i']) }}"><i class="fa fa-user-plus"></i>Nueva solicitud</a></li>
            <li {{ ($ruta === 'atnciudadana.indexSolicitudes' && $tipo_sol === 'i') ? 'class=active' : '' }}><a href="{{ route('atnciudadana.indexSolicitudes', ['tipo_solicitud' => 'i']) }}"><i class="fa fa-list-ul"></i>Lista de solicitudes</a></li>
            {{--  <li {{ ($ruta === 'atnciudadana.peticiones.general' && $tipo_sol === 'i') ? 'class=active' : '' }}><a href="{{ route('atnciudadana.peticiones.general', ['tipo_solicitud' => 'i']) }}"><i class="fa fa-list-ul"></i>Lista de peticiones</a></li>  --}}
            {{--  <li {{ ($ruta === 'atnciudadana.beneficiarios' && $tipo_sol === 'i') ? 'class=active' : '' }}><a href="{{ route('atnciudadana.beneficiarios', ['tipo_solicitud' => 'i']) }}"><i class="fa fa-list-ul"></i>Lista de beneficiarios</a></li>  --}}
						{{-- <li><a href="{{ route('atnciudadana.peticiones.personas') }}"><i class="fa fa-search"></i>Búsqueda beneficiarios</a></li> --}}
		</ul>
    </li>

    @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'atnciudadana'))
    <li class="{{ (($ruta === 'solicitudes.index' || $ruta === 'atnciudadana.peticiones.general' || $ruta === 'atnciudadana.beneficiarios' || $ruta === 'solicitudes.create') && $tipo_sol === 'e') ? 'treeview active menu-open' : 'treeview' }}">
		<a class="{{ ($ruta === 'atnciudadana.programas.index' || $ruta === 'atnciudadana.dependencias.index' ) ? 'home' : '' }}" href="#">
        <i class="fa fa-share"></i><span>Solicitudes Externas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li {{ ($ruta === 'solicitudes.create' && $tipo_sol === 'e') ? 'class=active' : '' }}><a href="{{ route('solicitudes.create', ['tipo_solicitud' => 'e']) }}"><i class="fa fa-user-plus"></i>Nueva solicitud</a></li>
            <li {{ ($ruta === 'solicitudes.index' && $tipo_sol === 'e') ? 'class=active' : '' }}><a href="{{ route('solicitudes.index', ['tipo_solicitud' => 'e']) }}"><i class="fa fa-list-ul"></i>Lista de solicitudes</a></li>
            <li {{ ($ruta === 'atnciudadana.peticiones.general' && $tipo_sol === 'e') ? 'class=active' : '' }}><a href="{{ route('atnciudadana.peticiones.general', ['tipo_solicitud' => 'e']) }}"><i class="fa fa-list-ul"></i>Lista de peticiones</a></li>
            <li {{ ($ruta === 'atnciudadana.beneficiarios' && $tipo_sol === 'e') ? 'class=active' : '' }}><a href="{{ route('atnciudadana.beneficiarios', ['tipo_solicitud' => 'e']) }}"><i class="fa fa-list-ul"></i>Lista de beneficiarios</a></li>
		</ul>
    </li>
    @endif

    <li {{ ($ruta === 'presolicitudes.index') ? 'class=active' : '' }}>
        <a href="{{ route('presolicitudes.index') }}">
          <i class="fa fa-address-book"></i> <span>Pre-Solicitudes</span>
          <span class="pull-right-container"></span>
        </a>
    </li>

    @if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'atnciudadana'))
    <li {{ ($ruta === 'atnciudadana.peticiones.index' ) ? 'class=active' : '' }}>
        <a href="{{ route('atnciudadana.peticiones.index', ['tipo_solicitud' => 'i']) }}">
        <i class="fa fa-list-alt"></i> <span>Peticiones</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
      @endif
    <li>
        <a href="" data-toggle="modal" data-target="#modalReporte">
        <i class="fa fa-list-alt"></i> <span>Reporte Área</span>
          <span class="pull-right-container">
          </span>
        </a>
    </li>

    {{-- <li {{ ($ruta === 'atnciudadana.agenda.index' ) ? 'class=active' : '' }}>
        <a href="{{ route('atnciudadana.agenda.index') }}">
        <i class="fa fa-list-alt"></i> <span>Agenda de eventos</span>
          <span class="pull-right-container">
          </span>
        </a>
    </li> --}}

  {{-- @if(auth()->user()->hasRoles(['NUEVO ROL']))

  	<li class="{{ ($ruta === 'solicitudes.index' || $ruta === 'solicitudes.create') ? 'treeview active menu-open' : 'treeview active menu-open' }}">
		<a class="{{ ($ruta === 'atnciudadana.programas.index' || $ruta === 'atnciudadana.dependencias.index' ) ? 'home' : '' }}" href="#">
			<i class="fa fa-group"></i><span>Solicitudes Externas</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li {{ ($ruta === 'solicitudes.index') ? 'class=active' : '' }}><a href="{{ route('solicitudes.index') }}"><i class="fa text-yellow fa-list-ul"></i>Lista de solicitudes</a></li>
			<li {{ ($ruta === 'solicitudes.create') ? 'class=active' : '' }}><a href="{{ route('solicitudes.create') }}"><i class="fa text-green fa-user-plus"></i>Nueva solicitud</a></li>
		</ul>
	</li>
  @endif --}}


@if(auth()->user()->hasRolesStrModulo(['ADMINISTRADOR'], 'atnciudadana'))
<li class="header">Catalogos</li>
  <li {{ ($ruta === 'atnciudadana.programas.externos') ? 'class=active' : '' }}>
        <a href="{{route('atnciudadana.programas.externos')}}">
          <i class="fa fa-cubes"></i> <span>Beneficios Externos</span>
          <span class="pull-right-container"></span>
        </a>
      </li>

      <li {{ ($ruta === 'atnciudadana.dependencias.index') ? 'class=active' : '' }}>
        <a href="{{ route('atnciudadana.dependencias.index') }}">
          <i class="fa fa-university"></i> <span>Dependencias</span>
          <span class="pull-right-container"></span>
        </a>
      </li>
      @endif

      <li {{ ($ruta === 'atnciudadana.peticionesA') ? 'class=active' : '' }}>
        <a href="{{ route('atnciudadana.peticionesA') }}">
          <i class="fa fa-book"></i><span>Solicitudes Anterior</span>
        </a>
      </li>

      <li>
        <a href="{{ route('atnciudadana.telegram_bug','10' ) }}">
          <i class="fa fa-phone"></i><span>Telegram</span>
        </a>
      </li>

  </ul>
@endsection

@section('styles')
  <!-- CSS Librerias -->
  @include('atnciudadana::layouts.links')
  <!-- CSS Propios -->
  <style type="text/css">
    .row {
      margin-right: 0px;
      margin-left: 0px;
    }

    input {
      text-transform: case;
    }

    .skin-blue .sidebar-menu > li.active > a.home {
      border-left-color: transparent !important;
      color: #b8c7ce !important;
    }

    #modalReporte .modal-header {
        display: flex;
        align-items: flex-start;
        justify-content: space-between;
    }

    #modalReporte .modal-header::before, #modalReporte .modal-header::after {
        content: none;
		}
  </style>
@stop

@section('scripts')
  <!-- Script Librerias  -->
  @include('atnciudadana::layouts.script')
  <script type="text/javascript" src="{{ asset('dist/js/jsreport.min.js') }}"></script>
  <!-- Script Propios -->
  <script type="text/javascript">
    // Input en mayusculas
    var data = {
        oficio: null,
        asunto: null,
        director:  null,
        cargo: null,
        peticiones: null
    };
    $(':input').on('propertychange input', function(e) {
      var ss = e.target.selectionStart;
      var se = e.target.selectionEnd;
      e.target.value = e.target.value.toUpperCase();
      e.target.selectionStart = ss;
      e.target.selectionEnd = se;
    });

    const GenerarReporte = () => {
        block();
        data.oficio = $('#oficio').val();
        data.asunto = $('#asunto').val().toUpperCase();
        data.director = $('#responsable').val();
        if($('#tipoReporte').val() == 'folio') {
            Object.keys(data.peticiones).map(key => {
                data.peticiones[key] = data.peticiones[key].filter(e => $('#folios').val().includes(e.folio))
            });
        } else {
            var tempFI = $('#fecha_inicio').val().split('/').map(e => Number.parseInt(e));
            var tempFF = $('#fecha_fin').val().split('/').map(e => Number.parseInt(e));
            var Fi = new Date(tempFI[2],tempFI[1]-1,tempFI[0]);
            var Ff = new Date(tempFF[2],tempFF[1]-1,tempFF[0]);
            Object.keys(data.peticiones).map((key, index)=>{
                data.peticiones[key] = data.peticiones[key].map(e => {
                    var tempF = e.fecha_turnado.split('-').map(e => Number.parseInt(e));
                    e.f = new Date(tempF[2],tempF[1]-1,tempF[0]);
                    return e;
                }).filter(e => e.f >= Fi && e.f <= Ff)
            });
        }
        jsreport.serverUrl = 'http://187.157.97.110:3002';
        var request = {
            template:{
                shortid: "r1x0u9huMB"
            },
            data: data
        };
        jsreport.renderAsync(request).then(function(res) {
                res.download(`REPORTE-${$('#direccion').text()}.pdf`)
                unblock();
        });
    }

    $(document).ready(function() {
        $('#direccion').select2({
    		language: 'es',
    		ajax: {
    			url: '/atnciudadana/direcciones',
    			dataType: 'JSON',
    			type: 'GET',
    			processResults: function(data, params) {
    				params.page = params.page || 1;
    				return {
    					results: $.map(data, function(item) {
    						return {
    							id: item.id,
    							text: item.nombre,
    							slug: item.nombre,
    							results: item
    						}
    					})
    				};
    			}
    		}
    	}).change( event => {
            block();
            $.get('{{ route('atnciudadana.direccion', [":id"]) }}'.replace(':id', $('#direccion').val()), datos => {
                $('#responsable').val(datos.responsable);
                data.director = datos.responsable;
                data.cargo = datos.genero == 'F' ? 'DIRECTORA DE ' + datos.nombre : 'DIRECTOR DE ' + datos.nombre;
            });
            $.get('{{ route('atnciudadana.peticiones.area', [":id"]) }}'.replace(':id', $('#direccion').val()), datos => {
                data.peticiones = datos;
                console.log(data.peticiones);
                if($('#tipoReporte').val() == 'folio') {
                    $("#folios").empty();
                    $("#folios").select2({
                        data: Object.values(datos).flat().map(e=>e.folio),
                        width: 'resolve'
                    });
                }
                unblock();
            });
        });

        $('#fecha_inicio, #fecha_fin').datepicker({
            autoclose: true,
            language: 'es',
            startDate: '01-01-1900',
            endDate: '0d',
            orientation: 'bottom'
        })

        $('#tipoReporte').change(event => {
            $('#content-tipoReporte').empty();
            if($('#tipoReporte').val() == 'fecha') {
                $('#content-tipoReporte').append(
                    '<div class="col-sm-6">' +
                            '<div class="form-group">' +
                                    '<label for="fecha_inicio">Fecha Inicio:</label>' +
                                    '<input type="text" class="form-control" id="fecha_inicio" name="fecha_inicio">' +
                            '</div>' +
                    '</div>' +
                    '<div class="col-sm-6">' +
                            '<div class="form-group">' +
                                    '<label for="fecha_fin">Fecha Fin:</label>' +
                                    '<input type="text" class="form-control" id="fecha_fin" name="fecha_fin">' +
                            '</div>' +
                    '</div>'
                );
            } else {
                $('#content-tipoReporte').append(
                    '<div class="col-sm-12">' +
                        '<div class="form-group">' +
                                '<label for="folios">Folios:</label>' +
                                '<select id="folios" name="folios" multiple="multiple" class="form-control select2" style="width: 100%;"></select>' +
                        '</div>' +
                    '</div>'
                );
                if(data.peticiones){
                    $("#folios").select2({
                        data: Object.values(data.peticiones).flat().map(e=>e.folio),
                        width: 'resolve'
                    });
                }
            }
        })
    });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
  </script>
@stop

@push('head')
	@yield('myCSS')
	<style>
			.select2-container--default .select2-selection--multiple .select2-selection__choice {
				background-color: #D12654 !important;
				border-color: #A31E41 !important;
			}
	</style>
@endpush

@push('body')
    <div class="modal fade" id="modalReporte" tabindex="-1" role="dialog" aria-labelledby="modalReporteLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalReporteLabel">Reporte por Área</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="POST">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="direccion">Direccion:</label>
                                    <select id="direccion" name="direccion" class="form-control select2" style="width: 100%;"></select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="responsable">Responsable:</label>
                                    <input type="text" class="form-control" id="responsable" name="responsable" placeholder="Responsable" disabled>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="oficio">Oficio:</label>
                                    <input type="text" class="form-control" id="oficio" name="oficio" placeholder="Oficio">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="asunto">Asunto:</label>
                                    <input type="text" class="form-control" id="asunto" name="asunto" placeholder="Asunto">
                                </div>
                            </div>


                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tipoReporte">Tipo Reporte:</label>
                                    <select id="tipoReporte" class="form-control">
                                        <option value="fecha">Por Fecha</option>
                                        <option value="folio">Por Folio</option>
                                    </select>
                                </div>
                            </div>

                            <div id="content-tipoReporte">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fecha_inicio">Fecha Inicio:</label>
                                        <input type="text" class="form-control" id="fecha_inicio" name="fecha_inicio">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="fecha_fin">Fecha Fin:</label>
                                        <input type="text" class="form-control" id="fecha_fin" name="fecha_fin">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="GenerarReporte();">Generar</button>
                </div>
            </div>
        </div>
    </div>
  @yield('myScripts')
@endpush
