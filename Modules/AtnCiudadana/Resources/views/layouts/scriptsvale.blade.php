
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>

<script type="text/javascript">
	
var nuevaSolicitud = {};
nuevaSolicitud.beneficios = [];

$('#genero').select2();

var benefLoad = $('#beneficios').select2({
    language: 'es',
    ajax:{
        url: '{{ route('beneficio.select') }}',
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term,
                tipo: $('#tipo').val()
            };
        },
        processResults:(data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});

$('#tipo').select2({
    language: 'es',
}).change(event => {
    $('#beneficios').empty();
    benefLoad;
    $('#areaR').text('');
    $('#areaT').text('');
    $('#prog').text('');
});

$('#titular_id').select2({
    language: 'es',
    ajax: {
        url: "{{ route('titular.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term
            };
        },
        processResults: (data, params) => {
            params.page = params.page || 1;
            var r = $.map(data, item => {
                    return {
                        id: item.id,
                        text: ('C. '+item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido).toUpperCase(),
                        slug: item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido,
                        results: item
                    }
                });
                var otro = {
                    'id':0,
                    'text':'OTRO',
                    'slug': 'OTRO',
                    'results': null
                }
                r.push(otro)
                console.log(r);
            return {
                results: r
            };
        },
        cache: true
    }
}).change(e => {
    $('#otro').remove();
    if($('#titular_id').val() == 0){
        var otro = 	'<div id="otro" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
            '<div class="row">'+
                '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">'+
                    '<div class="form-group">'+
                        '<label for="curp">CURP:</label>'+
                        '<div class="input-group">'+
                            '<div class="input-group-addon">'+
                                '<i class="fa fa-file-text-o"></i>'+
                            '</div>'+
                            '<input type="text" class="form-control" id="curpT" name="curp" placeholder="CURP">'+
                            '<div class="input-group-btn">'+
                                '<button type="button" class="btn btn-info" style="padding: auto 20px;padding-left: 25px;padding-right: 25px;">'+
                                    '<i class="fa fa-search"></i>'+
                                '</button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'
        $('#div-dirigido').append(otro);
        $('#curpT').focus();
    }
});

$('#fechaoficio, #fecharecepcion, #fecha_nacimiento').datepicker({
    autoclose: true,
    language: 'es',
    endDate: '0d',
    orientation: 'bottom'
});

$('#tiporecepcion_id').select2({
    language: 'es',
    ajax: {
        url: "{{ route('recepcion.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term
            };
        },
        processResults:(data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.tipo,
                        slug: item.tipo,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});

$('#tiporemitente_id').select2({
    language: 'es',
    ajax: {
        type: "GET",
        url: "{{ route('remitente.select') }}", 
        dataType: "json",
        data: params => {
            return {
                search: params.term
            };
        },
        processResults: (data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.tipo,
                        slug: item.tipo,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});

$(document).ready(() => {
    $('#fechaoficio').datepicker('setDate', new Date().toDateString());
    $('#fecharecepcion').datepicker('setDate', new Date().toDateString());
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.extend($.validator.messages, {
			required: 'Este campo es obligatorio.',
			remote: 'Por favor, rellena este campo.',
			email: 'Por favor, escribe una dirección de correo válida.',
			url: 'Por favor, escribe una URL válida.',
			date: 'Por favor, escribe una fecha válida.',
			dateISO: 'Por favor, escribe una fecha (ISO) válida.',
			number: 'Por favor, escribe un número válido.',
			digits: 'Por favor, escribe sólo dígitos.',
			creditcard: 'Por favor, escribe un número de tarjeta válido.',
			equalTo: 'Por favor, escribe el mismo valor de nuevo.',
			extension: 'Por favor, escribe un valor con una extensión aceptada.',
			maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
			minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
			rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
			range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
			max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
			min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
			nifES: 'Por favor, escribe un NIF válido.',
			nieES: 'Por favor, escribe un NIE válido.',
			cifES: 'Por favor, escribe un CIF válido.'
		});

		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			}
		});

		$('#form_remitente').validate({
			rules: 	{
				curp: {
					required: true,
					minlength: 18,
					maxlength: 19/*,
					pattern_curp: '18 caracteres de la curp'*/
				}
			}
		});

		$('#form_solicitud').validate({
			rules: 	{
				titular_id: {
					required: true
				},
				tiporecepcion_id: {
					required: true
				},
				tiporemitente_id: {
					required: true
				},
				numoficio: {
					required: true,
					minlength: 1,
					pattern_numeros: 'solo numeros, S/N si no dispone de uno.'
				},
				fechaoficio: {
					required: true
				},
				fecharecepcion: {
					required: true
				},
				asunto: {
					required: true,
					minlength: 10,
				}
			}
		});
		$.validator.addMethod('pattern_numeros', function(value,element) {
			return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
		}, 'solo numeros, S/N si no dispone de uno.');

		
});

$('#beneficios').change(event => {
    $.ajax({
        url: '{{ route('area.find')}}',
        dataType: 'JSON',
        type: 'GET',
        data: {
            id: $('#beneficios').val()
        },
        success: r => {
            $('#areaR').text(r.area);
            $('#areaT').text(r.responsable);
            $('#prog').text(r.programa);
        }
    })
});

var agregarBeneficio = () => {
    var beneficio = crearFila({
        id:$('#beneficios').val(),
        nombre:$('#beneficios').text(),
        cantidad:$('#cantidad').val()
    });
    $('#listBeneficios').append(beneficio);
  $('#beneficios').empty();
  $('#areaR').val('');
  $('#areaT').val('');
  $('#prog').val('');
  $('#cantidad').val('')
}

var crearFila = (r) => {
  nuevaSolicitud.beneficios.push(r);
  return (
      '<tr>'+
          '<td>' + r.id + '</td>' +
          '<td>' + r.nombre + '</td>' +
          '<td>' + r.cantidad +'</td>'+
          '<td style="text-align: center;">' +
              '<button type="button" class="btn btn-warning"><i class="fa fa-pencil"></i></button>' +
              '<button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>' +
          '</td>' +
      '</tr>'
  );
}

var guardar = () => {
    nuevaSolicitud.status = 'pendiente';
    guardarSolicitud;
}

var guardar_enviar = () => {
    nuevaSolicitud.status = 'vinculado';
    guardarSolicitud;
}

var guardarSolicitud = () => {
    nuevaSolicitud.fechaoficio = $('#fechaoficio').val();
    nuevaSolicitud.fecharecepcion = $('#fecharecepcion').val();
    nuevaSolicitud.numoficio = $('#numoficio').val();
    nuevaSolicitud.asunto = $('#asunto').val();
    nuevaSolicitud.tiposrecepcion_id = $('#tiporecepcion_id').val();
    nuevaSolicitud.tiposremitente_id = $('#tiporemitente_id').val();
    nuevaSolicitud.titular_id = $('#titular_id').val();

    $.ajax({
        url: '{{ route('solicitudes.store')}}',
        type: 'POST',
        data: nuevaSolicitud,
        success: function(response) {
            console.log(response);
            var re = "{{ URL::to('atnciudadana/solicitudes') }}";
            window.location.href = re;
        },
        error: function(response) {
            unblock();
        }
    });
}

$('#agregar').click(agregarBeneficio);

function persona_script_init() {
		$('#form_persona').validate({
			rules: {
				nombre: {
					required: true,
					minlength: 3,
					pattern_nombre: 'El nombre solo puede contener letras.'
				},
				primer_apellido: {
					required: true,
					minlength: 1,
					pattern_apellidos: 'El nombre solo puede contener letras.'
				},
				segundo_apellido: {
					required: true,
					minlength: 1,
					pattern_apellidos: 'El nombre solo puede contener letras.'
				},
				calle: {
					required: true,
					minlength: 3
				},
				numero_exterior: {
					required: true,
					minlength: 1,
					pattern_numeros: 'solo numeros, S/N si no dispone de uno.'
				},
				numero_interior: {
					required: false,
					pattern_numeros: 'solo numeros, S/N si no dispone de uno.'
				},
				colonia: {
					required: true,
					minlength: 3
				},
				fecha_nacimiento: {
					required: true
				},
				referencia_domicilio: {
					required: true,
					minlength: 3
				},
				municipio_id: {
					required: true
				},
				localidad_id: {
					required: true
				},
				discapacidad_id: {
					required: true
				},
				numero_celular: {
					required: true,
					pattern_telefonos: 'completa el formato'
				},
				numero_local: {
					required: true,
					pattern_telefonos: 'completa el formato'
				},
				curp: {
					required: true,
					minlength: 18,
					maxlength: 19,
					pattern_curp: '18 caracteres de la curp'
				},
				solicitante_id: {
					required: true,
					minlength: 18,
					maxlength: 19,
					pattern_curp: '18 caracteres de la curp'
				},
				codigopostal: {
					required: true,
					pattern_numeros: 'Codigo postal solo puede contener numeros'
				},
				clave_electoral: {
					required: false,
					minlength: 13,
					pattern_numeros: 'solo numeros'
				}
			},
			messages: {
				/*nombre: {
					required: 'Por favor introdusca un nombre',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				primer_apellido: {
					required: 'Por favor introdusca un apellido o "X"',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				segundo_apellido: {
					required: 'Por favor introdusca un apellido o "X"',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				fecha_nacimiento: {
					required: 'Por favor seleccione'
				},
				calle: {
					required: 'Por favor ingrese la calle'
				},
				numero_exterior: {
					required: 'Por favor ingrese el numero ext, o S/N'
				},
				colonia:{
					required: 'Por favor ingrese la colonia'
				},
				codigopostal: {
					required: 'Por favor ingrese la codigo postal'
				},
				municipio_id: {
					required: 'Por favor seleccione'
				},
				localidad_id: {
					required: 'Por favor seleccione'
				},
				discapacidad_id: {
					required: 'Por favor seleccione'
				},
				curp: {
					required: 'Por favor ingrece la curp',
					minlength: 'La CURP consta de 18 carcateres',
					maxlength: 'La CURP consta de 18 carcateres'
				},
				referencia_domicilio: {
					required: 'Por favor rellene'
				},
				numero_celular: {
					required: 'Introduca un numero celular de contacto',
					minlength: 'Introduca celular a 10 digitor',
					maxlength: 'Introduca celular a 10 digitor'
				},
				numero_local: {
					required: 'Introdusca un numero fijo de contacto',
					minlength: 'Introdusca numero local a 7 digitos',
					maxlength: 'Introdusca numero local a 7 digitos'
				},
				clave_electoral: {
					minlength: '13 digitos de la clave electoral',
					pattern_numeros: 'La clave elecoral son solo numeros'
				}*/
			}
		});

		$.validator.addMethod('pattern_nombre', function(value, element) {
			return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
		}, 'Solo letras y espacios entre palabras');

		$.validator.addMethod('pattern_apellidos', function(value, element) {
			return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{3,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
		}, 'Solo letras y espacios entre palabras, si no dispone de uno coloque X');

		$.validator.addMethod('pattern_curp',function(value, element) {
			return this.optional(element) || /^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value);
		}, 'Revisa los caracteres y vuelve a intentarlo');

		$.validator.addMethod('pattern_numeros', function(value,element) {
			return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
		}, 'solo numeros, S/N si no dispone de uno.');

		$.validator.addMethod('pattern_telefonos', function(value,element) {
			return this.optional(element) || /^\(\d{2,3}\)\s\d+(?:-\d+)+$/.test(value);
		}, 'Siga el formato');

		$.validator.addMethod('pattern_folios', function(value,element) {
			return this.optional(element) || /\d+|(?:X)|(?:x)/.test(value);
		}, 'solo numeros, X si no dispone de uno.');


		$('#fecha_nacimiento').datepicker({
			autoclose: true,
			language: 'es',
			endDate: '0d',
			orientation: 'bottom'
		}).change(function(event) {
			$('#fecha_nacimiento').valid();
		});

		$('[data-mask]').inputmask();

	}

	function persona_create_select() {

		var municipio = $('#municipio_id');

		var localidad = $('#localidad_id');

		municipio.select2({
			language: 'es',
			minimumInputLength: 2,
			ajax: {
				url: '{{ route('municipios.select') }}',
				dataType: 'JSON',
				type: 'GET',
				data: function(params) {
					return {
						search: params.term
					};
				},
				processResults: function(data, params) {
					params.page = params.page || 1;
					return {
						results: $.map(data, function(item) {
							return {
								id: item.id,
								text: item.nombre,
								slug: item.nombre,
								results: item
							}
						})
					};
				},
				cache: true
			}
		}).change(function(event) {
			municipio.valid();
			localidad.empty();
			localidad.select2({
				language: 'es',
				ajax: {
					url: '{{ route('localidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term,
							municipio_id: municipio.val()
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				localidad.valid();
			});
		});

		localidad.select2({
			language: 'es'
		});

	}

	function persona_edit_select() {

		var municipio = $('#municipio_id');

		var localidad = $('#localidad_id');

		localidad.select2({
			language: 'es',
			//minimumInputLength: 2,
			ajax: {
				url: '{{ route('localidades.select') }}',
				dataType: 'JSON',
				type: 'GET',
				data: function(params) {
					return {
						search: params.term,
						municipio_id: municipio.val()
					};
				},
				processResults: function(data, params) {
					params.page = params.page || 1;
					return {
						results: $.map(data, function(item) {
							return {
								id: item.id,
								text: item.nombre,
								slug: item.nombre,
								results: item
							}
						})
					};
				},
				cache: true
			}
		}).change(function(event) {
			localidad.valid();
		});

	}
	var modal_persona = $('#modal-persona');


function buscarPersona() {

if($('#curp').valid()) {

	

	var modal_title_persona = $('#modal-title-persona');

	var modal_body_persona = $('#modal-body-persona');

	var modal_footer_persona = $('#modal-footer-persona');

	$.get('/personas/search/' + $('#curp').val(), function(data) {
		//console.log(data);
	})
	.done(function(data) {
		modal_title_persona.text('Resultado de la Busqueda');
		modal_body_persona.html(data.html);
		modal_persona.modal('show');
		modal_footer_persona.html(
					'<button type="submit" class="btn btn-success" onclick="persona_aceptar();">Aceptar</button>' +
					'<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'
				);
		persona_script_init();
		persona_create_select();
		persona_edit_select();

	})
	.fail(function(data) {

		//alert(data.responseJSON.errors[0].html);
		swal({
			title: 'LA CURP NO SE ENCUENTRA',
			text: '¿Desea Agregar Una Nueva Persona?',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'NO',
			confirmButtonText: 'SI'
		}).then((result) => {
			if(result.value) {

				modal_title_persona.text('Agregar Persona:');
				modal_body_persona.html(data.responseJSON.errors[0].html);
				modal_persona.modal('show');
				modal_footer_persona.html(
					'<button type="submit" class="btn btn-success" onclick="persona_create_edit();">Guardar</button>' +
					'<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'
				);
				
				persona_create_select();

			}
		});

	});

}

}

function persona_create_edit() {

block();

var form = $('#form_persona');

if(form.valid()) {

	var modal_persona = $('#modal-persona');

	var formData = new FormData(form[0]);

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url: form.attr('action'),
		type: form.attr('method'),
		data: form.serialize(),
		//processData: false,
		//contentType: false,
		success: function(response) {
			unblock();
			modal_persona.modal('hide');
		},
		error: function(response) {
			unblock();
			modal_persona.modal('hide');
		}
	});

}
else {
	unblock();
}

}

var persona_aceptar = () => {
	$('#boxDatosRemitente').remove();
	var d = '<div id="boxDatosRemitente" class="box-body">'+
    '<strong>'+
        '<i class="fa fa-user margin-r-5"></i> Nombre</strong>'+
    '<p class="text-muted" id="nombreRemitente">'+
    $('#nombre').val()+' '+$('#primer_apellido').val()+' '+$('#segundo_apellido').val()+
    '</p>'+
    '<hr>'+
    '<strong>'+
        '<i class="fa fa-map-marker margin-r-5"></i> Direccion</strong>'+
    '<p class="text-muted" id="direccionRemitente">'+$('#calle').val()+' #'+$('#numero_exterior').val()+(($('#numero_interior').val()!='')? '-'+$('#numero_interior').val() : '')+' ,Col.'+$('#colonia').val()+' ,C.P.'+$('#codigopostal').val()+'</p>'+
    '<p class="text-muted" id="ubicacionRemitente">'+$('#localidad_id').text()+', '+$('#municipio_id').text()+'</p>'+
    '<hr>'+
    '<strong>'+
        '<i class="fa fa-phone margin-r-5"></i> Contacto</strong><br>'+
	(($('#numero_local').val()!='') ? 'Telefono:<p class="text-muted" id="telefonoRemitente">'+$('#numero_local').val()+'</p>' : '' )+
	(($('#numero_celular').val()!='') ? 'Celular:<p class="text-muted" id="celularRemitente">'+$('#numero_celular').val()+'</p>' : '' )+
	(($('#email').val()!='') ? 'Correo Electronico:<p class="text-muted" id="emailRemitente">'+$('#email').val()+'</p>' : '' )+
'</div>';
$('#rowDatosRemitente').append(d);
	modal_persona.modal('hide');
}
</script>