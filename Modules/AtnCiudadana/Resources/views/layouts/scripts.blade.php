<!-- sweetalert2 -->
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
{!! $personas->html()->scripts() !!}
{!! $beneficios->html()->scripts() !!}
@include('personas.js.persona')

<script type="text/javascript">

var nuevaSolicitud = {};

//localStorage.setItem("saveSolictud", false);

nuevaSolicitud.beneficios = [];

$('#genero').select2();

var benefLoad = $('#beneficios').select2({
    language: 'es',
    ajax:{
        url: '{{ route('beneficio.select') }}',
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term
            };
        },
        processResults:(data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.nombre,
                        slug: item.nombre,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});

$('#tipo').select2({
    language: 'es',
}).change(event => {
    $('#beneficios').empty();
    benefLoad;
    $('#areaR').text('');
    $('#areaT').text('');
    $('#prog').text('');
});

$('#titular_id').select2({
    language: 'es',
    ajax: {
        url: "{{ route('titular.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term
            };
        },
        processResults: (data, params) => {
            params.page = params.page || 1;
			// data.push(
			// 	{id:'0',
			// 	nombre: 'OTRO',
			// 	primer_apellido: '',
			// 	segundo_apellido: ''}
			// );
            var r = $.map(data, item => {
                    return {
                        id: item.id,
                        text: ((item.nombre === 'OTRO' ? '':'C. ')+item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido).toUpperCase(),
                        slug: item.nombre+' '+item.primer_apellido+' '+item.segundo_apellido,
                        results: item
                    }
                });
            return {
                results: r
            };
        },
        cache: true
    }
}).change(e => {
    $('#otro').remove();
    if($('#titular_id').val() == '0'){
        var otro = 	'<div id="otro" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">'+
		'<div class="row">'+
			'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">'+
				'<div class="form-group">' +
					'<label for="otro_id">Remitente:</label>' +
					'<div class="input-group">' +
						'<select id="otro_id" name="otro_id" class="form-control select2" style="width: 100%;" readonly="readonly">' +
						'</select>' +
						'<div class="input-group-btn">' +
							'<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas(\'otro_id\');" {{ isset($candidato) ? "disabled" : "" }}>' +
								'<i class="fa fa-search"></i>' +
							'</button>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>'+
        '</div>';
        $('#div-dirigido').append(otro);
    }
});

$('#fechaoficio, #fecharecepcion, #fecha_nacimiento').datepicker({
    autoclose: true,
    language: 'es',
    endDate: '0d',
    orientation: 'bottom'
});

$('#tiporecepcion_id').select2({
    language: 'es',
    ajax: {
        url: "{{ route('recepcion.select') }}",
        dataType: 'JSON',
        type: 'GET',
        data: params => {
            return {
                search: params.term
            };
        },
        processResults:(data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.tipo,
                        slug: item.tipo,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
});

$('#tiporemitente_id').select2({
    language: 'es',
    ajax: {
        type: "GET",
        url: "{{ route('remitente.select') }}",
        dataType: "json",
        data: params => {
            return {
                search: params.term
            };
        },
        processResults: (data, params) => {
            params.page = params.page || 1;
            return {
                results: $.map(data, item => {
                    return {
                        id: item.id,
                        text: item.tipo,
                        slug: item.tipo,
                        results: item
                    }
                })
            };
        },
        cache: true
    }
}).change(e => {
	var op = '';
		if(tipor!=$('#tiporemitente_id').val()){
			tipor = $('#tiporemitente_id').val();
			$('#body_form_remitente').empty();
			$('#rowDatosRemitente').empty();
			if($('#tiporemitente_id').val()==4){
			op = '<div class="row">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">'+
					'<div class="form-group">' +
						'<label for="remitente_id">Remitente:</label>' +
						'<div class="input-group">' +
							'<select id="remitente_id" name="remitente_id" class="form-control select2" style="width: 100%;" readonly="readonly">' +
							'</select>' +
							'<div class="input-group-btn">' +
								'<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas(\'remitente_id\');" {{ isset($candidato) ? "disabled" : "" }}>' +
									'<i class="fa fa-search"></i>' +
								'</button>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';
			}
			if ($('#tiporemitente_id').val()==2) {
				op = '<div class="row">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">'+
					'<div class="form-group">' +
						'<label for="remitente_id">Remitente:</label>' +
						'<div class="input-group">' +
							'<select id="remitente_id" name="remitente_id" class="form-control select2" style="width: 100%;" readonly="readonly">' +
							'</select>' +
							'<div class="input-group-btn">' +
								'<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas(\'remitente_id\');" {{ isset($candidato) ? "disabled" : "" }}>' +
									'<i class="fa fa-search"></i>' +
								'</button>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';
				op +=
							'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">' +
								'<div class="form-group">' +
									'<label for="remitente_municipio_id">Municipio:</label>' +
									'<select id="remitente_municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">' +
									'</select>' +
								'</div>' +
							'</div>' +
						'</div>' ;
			}
			if ($('#tiporemitente_id').val()==1) {
				op = '<div class="row">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">'+
					'<div class="form-group">' +
						'<label for="remitente_id">Remitente:</label>' +
						'<div class="input-group">' +
							'<select id="remitente_id" name="remitente_id" class="form-control select2" style="width: 100%;" readonly="readonly">' +
							'</select>' +
							'<div class="input-group-btn">' +
								'<button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="candidato.abrir_modal_personas(\'remitente_id\');" {{ isset($candidato) ? "disabled" : "" }}>' +
									'<i class="fa fa-search"></i>' +
								'</button>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>';
				op +=
							'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">' +
								'<div class="form-group">' +
									'<label for="remitente_region_id">Región:</label>' +
									'<select id="remitente_region_id" name="region_id" class="form-control select2" style="width: 100%;">' +
									'</select>' +
								'</div>' +
							'</div>' +
						'</div>' ;
			}
			if ($('#tiporemitente_id').val()==3) {
				op = '<div class="row">' +
							'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">' +
								'<div class="form-group">' +
									'<label for="dependencia_id">Dependencia:</label>' +
									'<select id="dependencia_id" name="dependencia_id" class="form-control select2" style="width: 100%;">' +
									'</select>' +
								'</div>' +
							'</div>' +
						'</div>' ;
			}
			$('#body_form_remitente').append(op);
			$('#form_remitente').validate({
			rules: 	{
				municipio_id: {
					required: true
				},
				region_id: {
					required: true
				},
				dependencia_id: {
					required: true
				},
				remitente_id: {
					required: true
				}
			}
		});
			$('#btnBuscarRemitente').on('click',() => {
				buscarPersona();
			});
			$('#curpRemitente').keypress(function(e) {
				if(e.which == 13) {
					buscarPersona();
				}
			});
			$('#remitente_municipio_id').select2({
				language: 'es',
				minimumInputLength: 2,
				ajax: {
					url: '{{ route('municipios.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			});
			$('#remitente_region_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('regiones.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			});
			$('#dependencia_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('dependencias.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			});
			$("#dependencia_id").on("select2:select", function (e) {
				$('#boxDatosRemitente').remove();
				var d = '<div id="boxDatosRemitente" class="box-body">'+
							'<strong>'+
								'<i class="fa fa-map-marker margin-r-5"></i> Direccion</strong>'+
							'<p class="text-muted" id="direccionRemitente">'+e.params.data.results.calle+' #'+e.params.data.results.numero+' ,Col.'+e.params.data.results.colonia+'</p>'+
							'Tipo: <p class="text-muted" id="direccionRemitente">'+e.params.data.results.tipo+'</p>'+
							'<hr>'+
							'<strong>'+
								'<i class="fa fa-phone margin-r-5"></i> Contacto</strong><br>'+
								'Encargado: <p class="text-muted" id="direccionRemitente">'+e.params.data.results.encargado+'</p>'+
								'Cargo: <p class="text-muted" id="direccionRemitente">'+e.params.data.results.cargoencargado+'</p>'+
								'Telefono: <p class="text-muted" id="direccionRemitente">'+e.params.data.results.telefono+'</p>'+
							'</div>';
							$('#rowDatosRemitente').append(d);
			});
		}
});

$(document).ready(() => {
	$.validator.addMethod('pattern_nombre', function(value, element) {
			return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
		}, 'Solo letras y espacios entre palabras');

		$.validator.addMethod('pattern_apellidos', function(value, element) {
			return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{3,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
		}, 'Solo letras y espacios entre palabras, si no dispone de uno coloque X');

		$.validator.addMethod('pattern_curp',function(value, element) {
			return this.optional(element) || /^[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value);
		}, 'Revisa los caracteres y vuelve a intentarlo');

		$.validator.addMethod('pattern_numeros', function(value,element) {
			return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
		}, 'solo numeros, X si no dispone de uno.');

		$.validator.addMethod('pattern_telefonos', function(value,element) {
			return this.optional(element) || /^\(\d{2,3}\)\s\d+(?:-\d+)+$/.test(value);
		}, 'Siga el formato');

		$.validator.addMethod('pattern_folios', function(value,element) {
			return this.optional(element) || /\d+|(?:X)|(?:x)/.test(value);
		}, 'solo numeros, X si no dispone de uno.');
    $('#fechaoficio').datepicker('setDate', new Date().toDateString());
    $('#fecharecepcion').datepicker('setDate', new Date().toDateString());
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	$.extend($.validator.messages, {
			required: 'Este campo es obligatorio.',
			remote: 'Por favor, rellena este campo.',
			email: 'Por favor, escribe una dirección de correo válida.',
			url: 'Por favor, escribe una URL válida.',
			date: 'Por favor, escribe una fecha válida.',
			dateISO: 'Por favor, escribe una fecha (ISO) válida.',
			number: 'Por favor, escribe un número válido.',
			digits: 'Por favor, escribe sólo dígitos.',
			creditcard: 'Por favor, escribe un número de tarjeta válido.',
			equalTo: 'Por favor, escribe el mismo valor de nuevo.',
			extension: 'Por favor, escribe un valor con una extensión aceptada.',
			maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
			minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.'),
			rangelength: $.validator.format('Por favor, escribe un valor entre {0} y {1} caracteres.'),
			range: $.validator.format('Por favor, escribe un valor entre {0} y {1}.'),
			max: $.validator.format('Por favor, escribe un valor menor o igual a {0}.'),
			min: $.validator.format('Por favor, escribe un valor mayor o igual a {0}.'),
			nifES: 'Por favor, escribe un NIF válido.',
			nieES: 'Por favor, escribe un NIE válido.',
			cifES: 'Por favor, escribe un CIF válido.'
		});

		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			}
		});

		$('#form_remitente').validate({
			rules: 	{
				municipio_id: {
					required: true
				},
				region_id: {
					required: true
				},
				dependencia_id: {
					required: true
				},
				remitente_id: {
					required: true
				}
			}
		});

		$('#form_solicitud').validate({
			rules: 	{
				titular_id: {
					required: true
				},
				tiporecepcion_id: {
					required: true
				},
				tiporemitente_id: {
					required: true
				},
				numoficio: {
					required: true,
					minlength: 1,
					pattern_folios: 'solo numeros, X si no dispone de uno.'
				},
				fechaoficio: {
					required: true
				},
				fecharecepcion: {
					required: true
				},
				asunto: {
					required: true,
					minlength: 10,
				},
				otro_id: {
					required: true
				}
			}
		});

		$('#form_producto').validate({
			rules: 	{
				tipo:{
					required: true
				},
				beneficios: {
					required: true
				},
				cantidad: {
					required: true,
					minlength: 1,
					pattern_folios: 'solo numeros, X si no dispone de uno.'
				}
			}
		});
});
$('#beneficios').change(event => {
    $.ajax({
        url: '{{ route('area.find')}}',
        dataType: 'JSON',
        type: 'GET',
        data: {
            id: $('#beneficios').val()
        },
        success: r => {
            $('#areaR').text(r.area);
            $('#areaT').text(r.responsable);
            $('#prog').text(r.programa);
        }
    })
});

var agregarBeneficio = () => {
	var objE = {
        id:$('#beneficios').val(),
        nombre:$('#beneficios').select2('data')[0].text,
        cantidad:$('#cantidad').val(),
		tipo:$('#tipo').val()
    };
	var p = nuevaSolicitud.beneficios.map((b)=>{return b.id}).indexOf(objE.id);
	if( p ==  -1){
		var beneficio = crearFila(objE);
		$('#listaBeneficios').append(beneficio);
		limpiarBeneficio();
	}else{
		if($('#beneficio_'+objE.id+'_cantidad').text() == 'x'||$('#beneficio_'+objE.id+'_cantidad').text()=='X'){
			if(objE.cantidad != 'x' || objE.cantidad != 'X'){
				swal({
					title: 'El beneficio EXISTE sin cantidad',
					text: "¿Desea actualizar la cantidad?",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sí',
					cancelButtonText: 'Cancelar'
					}).then((result) => {
					if (result.value) {
						$('#beneficio_'+objE.id+'_cantidad').text(objE.cantidad);
						nuevaSolicitud.beneficios[p].cantidad =$('#beneficio_'+objE.id+'_cantidad').text();
						limpiarBeneficio();
					}
				});
			}else{
				swal(
  					'OJO',
  					'El beneficio ya existe en la lista',
  					'info'
				);
			}
		}else{
			if(objE.cantidad == 'x' || objE.cantidad == 'X'){
				swal({
					title: 'El beneficio EXISTE con cantidad',
					text: "¿Desea no establecer la cantidad?",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sí',
					cancelButtonText: 'Cancelar'
					}).then((result) => {
					if (result.value) {
						$('#beneficio_'+objE.id+'_cantidad').text(objE.cantidad);
						nuevaSolicitud.beneficios[p].cantidad =$('#beneficio_'+objE.id+'_cantidad').text();
						limpiarBeneficio();
					}
				});
			}else {
				swal({
					title: 'El beneficio EXISTE',
					text: "¿Desea sumar las cantidades?",
					type: 'question',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Sí, sumar',
					cancelButtonText: 'Cancelar'
					}).then((result) => {
					if (result.value) {
						$('#beneficio_'+objE.id+'_cantidad').text(''+(parseInt($('#beneficio_'+objE.id+'_cantidad').text())+parseInt(objE.cantidad)));
						nuevaSolicitud.beneficios[p].cantidad =$('#beneficio_'+objE.id+'_cantidad').text();
						limpiarBeneficio();
					}
				});
			}
		}
	}
}

var limpiarBeneficio = () => {
	$('#beneficios').empty();
  	$('#areaR').text('---------------------');
  	$('#areaT').text('---------------------');
  	$('#prog').text('---------------------');
  	$('#cantidad').val('');
    $('#beneficios').val(null).trigger('change');
}

var crearFila = (r) => {
  nuevaSolicitud.beneficios.push(r);
  return (
      '<tr id='+'beneficio_'+r.id+'>'+
          '<td>' + r.nombre + '</td>' +
          '<td class="text-center" id="'+'beneficio_'+r.id+'_cantidad">' + r.cantidad +'</td>'+
          '<td class="text-center">' +
              '<button type="button" class="btn btn-info" onclick="agregarBenefciarios('+r.id+')"><i class="fa fa-users"></i></button>' +
              '<button type="button" class="btn btn-warning" onclick="editBeneficio('+r.id+')"><i class="fa fa-pencil"></i></button>' +
              '<button type="button" class="btn btn-danger" onclick="deleteBeneficio('+r.id+')"><i class="fa fa-trash"></i></button>' +
          '</td>' +
      '</tr>'
  );
}

function persona_script_init() {
		$('#form_persona').validate({
			rules: {
				nombre: {
					required: true,
					minlength: 3,
					pattern_nombre: 'El nombre solo puede contener letras.'
				},
				primer_apellido: {
					required: true,
					minlength: 1,
					pattern_apellidos: 'El nombre solo puede contener letras.'
				},
				segundo_apellido: {
					required: true,
					minlength: 1,
					pattern_apellidos: 'El nombre solo puede contener letras.'
				},
				calle: {
					required: true,
					minlength: 3
				},
				numero_exterior: {
					required: true,
					minlength: 1,
					pattern_numeros: 'solo numeros, S/N si no dispone de uno.'
				},
				numero_interior: {
					required: false,
					pattern_numeros: 'solo numeros, S/N si no dispone de uno.'
				},
				colonia: {
					required: true,
					minlength: 3
				},
				fecha_nacimiento: {
					required: true
				},
				referencia_domicilio: {
					required: true,
					minlength: 3
				},
				municipio_id: {
					required: true
				},
				localidad_id: {
					required: true
				},
				discapacidad_id: {
					required: true
				},
				numero_celular: {
					required: true,
					pattern_telefonos: 'completa el formato'
				},
				numero_local: {
					required: true,
					pattern_telefonos: 'completa el formato'
				},
				curp: {
					required: true,
					minlength: 18,
					maxlength: 19,
					pattern_curp: '18 caracteres de la curp'
				},
				solicitante_id: {
					required: true,
					minlength: 18,
					maxlength: 19,
					pattern_curp: '18 caracteres de la curp'
				},
				codigopostal: {
					required: true,
					pattern_numeros: 'Codigo postal solo puede contener numeros'
				},
				clave_electoral: {
					required: false,
					minlength: 13,
					pattern_numeros: 'solo numeros'
				}
			},
			messages: {
				/*nombre: {
					required: 'Por favor introdusca un nombre',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				primer_apellido: {
					required: 'Por favor introdusca un apellido o "X"',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				segundo_apellido: {
					required: 'Por favor introdusca un apellido o "X"',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
				fecha_nacimiento: {
					required: 'Por favor seleccione'
				},
				calle: {
					required: 'Por favor ingrese la calle'
				},
				numero_exterior: {
					required: 'Por favor ingrese el numero ext, o S/N'
				},
				colonia:{
					required: 'Por favor ingrese la colonia'
				},
				codigopostal: {
					required: 'Por favor ingrese la codigo postal'
				},
				municipio_id: {
					required: 'Por favor seleccione'
				},
				localidad_id: {
					required: 'Por favor seleccione'
				},
				discapacidad_id: {
					required: 'Por favor seleccione'
				},
				curp: {
					required: 'Por favor ingrece la curp',
					minlength: 'La CURP consta de 18 carcateres',
					maxlength: 'La CURP consta de 18 carcateres'
				},
				referencia_domicilio: {
					required: 'Por favor rellene'
				},
				numero_celular: {
					required: 'Introduca un numero celular de contacto',
					minlength: 'Introduca celular a 10 digitor',
					maxlength: 'Introduca celular a 10 digitor'
				},
				numero_local: {
					required: 'Introdusca un numero fijo de contacto',
					minlength: 'Introdusca numero local a 7 digitos',
					maxlength: 'Introdusca numero local a 7 digitos'
				},
				clave_electoral: {
					minlength: '13 digitos de la clave electoral',
					pattern_numeros: 'La clave elecoral son solo numeros'
				}*/
			}
		});

		$('#fecha_nacimiento').datepicker({
			autoclose: true,
			language: 'es',
			endDate: '0d',
			orientation: 'bottom'
		}).change(function(event) {
			$('#fecha_nacimiento').valid();
		});

		$('[data-mask]').inputmask();

	}

	function persona_create_select() {

		var municipio = $('#municipio_id');

		var localidad = $('#localidad_id');

		municipio.select2({
			language: 'es',
			minimumInputLength: 2,
			ajax: {
				url: '{{ route('municipios.select') }}',
				dataType: 'JSON',
				type: 'GET',
				data: function(params) {
					return {
						search: params.term
					};
				},
				processResults: function(data, params) {
					params.page = params.page || 1;
					return {
						results: $.map(data, function(item) {
							return {
								id: item.id,
								text: item.nombre,
								slug: item.nombre,
								results: item
							}
						})
					};
				},
				cache: true
			}
		}).change(function(event) {
			municipio.valid();
			localidad.empty();
			localidad.select2({
				language: 'es',
				ajax: {
					url: '{{ route('localidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term,
							municipio_id: municipio.val()
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				localidad.valid();
			});
		});

		localidad.select2({
			language: 'es'
		});

	}

	function persona_edit_select() {

		var municipio = $('#municipio_id');

		var localidad = $('#localidad_id');

		localidad.select2({
			language: 'es',
			//minimumInputLength: 2,
			ajax: {
				url: '{{ route('localidades.select') }}',
				dataType: 'JSON',
				type: 'GET',
				data: function(params) {
					return {
						search: params.term,
						municipio_id: municipio.val()
					};
				},
				processResults: function(data, params) {
					params.page = params.page || 1;
					return {
						results: $.map(data, function(item) {
							return {
								id: item.id,
								text: item.nombre,
								slug: item.nombre,
								results: item
							}
						})
					};
				},
				cache: true
			}
		}).change(function(event) {
			localidad.valid();
		});

	}



var modal_persona = $('#modal-persona');
function buscarPersona() {

if($('#curpRemitente').valid()) {



	var modal_title_persona = $('#modal-title-persona');

	var modal_body_persona = $('#modal-body-persona');

	var modal_footer_persona = $('#modal-footer-persona');

	$.get('/personas/search?tipo=create_edit&curp=' + $('#curpRemitente').val(), function(data) {
		//console.log(data);
	})
	.done(function(data) {
		modal_title_persona.text('Resultado de la Busqueda');
		modal_body_persona.html(data.html);
		modal_persona.modal('show');
		modal_footer_persona.html(
					'<button id="btnAcpetarPersona" type="submit" class="btn btn-success">Aceptar</button>' +
					'<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'
				);
				$('#btnAcpetarPersona').on('click',() => {
					persona_aceptar(data.persona_id);
				})
		persona_script_init();
		persona_create_select();
		persona_edit_select();

	})
	.fail(function(data) {

		//alert(data.responseJSON.errors[0].html);
		swal({
			title: 'LA CURP NO SE ENCUENTRA',
			text: '¿Desea Agregar Una Nueva Persona?',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'No',
			confirmButtonText: 'Sí'
		}).then((result) => {
			if(result.value) {

				modal_title_persona.text('Agregar Persona:');
				modal_body_persona.html(data.responseJSON.errors[0].html);
				modal_persona.modal('show');
				modal_footer_persona.html(
					'<button type="submit" class="btn btn-success" onclick="persona_create_edit();">Guardar</button>' +
					'<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'
				);
				persona_script_init();
				persona_create_select();

			}
		});

	});

}

}

function persona_create_edit() {

block();

var form = $('#form_persona');

if(form.valid()) {

	var modal_persona = $('#modal-persona');

	var formData = new FormData(form[0]);

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		url: form.attr('action'),
		type: form.attr('method'),
		data: form.serialize(),
		//processData: false,
		//contentType: false,
		success: function(response) {
			unblock();
			modal_persona.modal('hide');
		},
		error: function(response) {
			unblock();
			modal_persona.modal('hide');
		}
	});

}
else {
	unblock();
}

}



var persona_aceptar = (persona_id) => {
	$('#boxDatosRemitente').remove();
	var d = '<div id="boxDatosRemitente" class="box-body">'+
    '<strong>'+
        '<i class="fa fa-user margin-r-5"></i> Nombre</strong>'+
    '<p class="text-muted" id="nombreRemitente">'+
    $('#nombre').val()+' '+$('#primer_apellido').val()+' '+$('#segundo_apellido').val()+
    '</p>'+
    '<hr>'+
    '<strong>'+
        '<i class="fa fa-map-marker margin-r-5"></i> Direccion</strong>'+
    '<p class="text-muted" id="direccionRemitente">'+$('#calle').val()+' #'+$('#numero_exterior').val()+(($('#numero_interior').val()!='')? '-'+$('#numero_interior').val() : '')+' ,Col.'+$('#colonia').val()+' ,C.P.'+$('#codigopostal').val()+'</p>'+
    '<p class="text-muted" id="ubicacionRemitente">'+$('#localidad_id').text()+', '+$('#municipio_id').text()+'</p>'+
    '<hr>'+
    '<strong>'+
        '<i class="fa fa-phone margin-r-5"></i> Contacto</strong><br>'+
	(($('#numero_local').val()!='') ? 'Telefono:<p class="text-muted" id="telefonoRemitente">'+$('#numero_local').val()+'</p>' : '' )+
	(($('#numero_celular').val()!='') ? 'Celular:<p class="text-muted" id="celularRemitente">'+$('#numero_celular').val()+'</p>' : '' )+
	(($('#email').val()!='') ? 'Correo Electronico:<p class="text-muted" id="emailRemitente">'+$('#email').val()+'</p>' : '' )+
	'</div>';
	$('#rowDatosRemitente').append(d);
	nuevaSolicitud.solicitante_id = persona_id;
	modal_persona.modal('hide');
}

$('a[data-toggle="tab"]').on('click', function(){
  if ($(this).parent('li').hasClass('disabled')) {
    return false;
  };
});
let tipor  = 0;
$('#btnNext_solicitud').on('click', function(){
	var s = $('#form_solicitud').valid();
	var r = $('#form_remitente').valid();
	if (s&&r) {
		if($('#fechaoficio').datepicker('getDate') > $('#fecharecepcion').datepicker('getDate')){
			swal(
				'¡Advertencia!',
				'La fecha de Oficio debe ser anterior a la de Recepcion',
				'warning'
			)
		}else {
			nuevaSolicitud.status = 'solicitante';
			validSolicitud();
			//$('.nav-tabs a[href="#tab_remitente"]').tab('show');
		}
	}
});

$('#bntPrevious_Remitente').on('click', function(){
    $('.nav-tabs a[href="#tab_solicitud"]').tab('show')
});

$('#bntPrevious_Peticion').on('click', function(){
    //$('.nav-tabs a[href="#tab_remitente"]').tab('show')
    $('.nav-tabs a[href="#tab_solicitud"]').tab('show')
});

$('#btnNext_Remitente').on('click', function(){
	if ($('#form_remitente').valid()) {
		if($('#tiporemitente_id').val()!=3){
			if( nuevaSolicitud.solicitante_id != undefined){
			$('.nav-tabs a[href="#tab_peticion"]').tab('show')
		}else{
			swal(
				'¡Advertencia!',
				'Busque y Seleccione un Remitente',
				'warning'
			)
		}
	}
	}
});

$('#btnAgregar').on('click',() => {
	if($('#form_producto').valid()){
		agregarBeneficio();
	}else{
		swal(
  			'¡Advertencia!',
  			'Debe seleccionar un beneficio y la cantidad',
  			'warning'
		)
	}
});




var deleteBeneficio = (id) => {
	swal({
			title: '¡Advertencia!',
			text: '¿Está seguro de eliminar este beneficio?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'No',
			confirmButtonText: 'Sí'
		}).then((result) => {
			if(result.value) {
				var p = nuevaSolicitud.beneficios.map((b)=>{return b.id}).indexOf(""+id);
				$('#beneficio_'+id).remove();
				nuevaSolicitud.beneficios.splice(p, 1);
			}
		});
}

var editBeneficio = (id) => {
	var p = nuevaSolicitud.beneficios.map((b)=>{return b.id}).indexOf(""+id);
	$('#tipo').val(nuevaSolicitud.beneficios[p].tipo);
	$('#tipo').trigger('change');
	$('#tipo').attr('disabled', 'disabled');
	var newOption = new Option(nuevaSolicitud.beneficios[p].nombre, nuevaSolicitud.beneficios[p].id, true, true);
    $('#beneficios').append(newOption).trigger('change');
	$('#beneficios').attr('disabled', 'disabled');
    $('#cantidad').val(nuevaSolicitud.beneficios[p].cantidad);
	$("#btnAgregar").off('click');
	$('#btnAgregar').attr('id','btnEditar');
	$('#btnEditar').text("Editar");
	$('#btnEditar').css('background-color','#ec971f');
	$('#btnEditar').css('border-color', '#e08e0b');
	$('#btnEditar').on('click',() => {
		if($('#form_producto').valid()){
			saveEdit(id);
		}else{
			swal(
  				'¡Advertencia!',
  				'Debe seleccionar un benificio y la cantidad',
  				'warning'
			)
		}
	});
}

var saveEdit = (id) => {
	var p = nuevaSolicitud.beneficios.map((b)=>{return b.id}).indexOf(""+id);
	var newC =  $('#cantidad').val();
	console.log(newC);
	swal({
		title: '¡Advertencia!',
		text: "¿Desea actualizar el beneficio?",
		type: 'question',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Sí',
		cancelButtonText: 'Cancelar'
		}).then((result) => {
		if (result.value) {
			$('#beneficio_'+id+'_cantidad').text(newC);
			nuevaSolicitud.beneficios[p].cantidad =$('#beneficio_'+id+'_cantidad').text();
			limpiarBeneficio();
			$('#beneficios').removeAttr('disabled');
			$('#tipo').removeAttr('disabled');
			$("#btnEditar").off('click');
			$('#btnEditar').attr('id','btnAgregar');
			$('#btnAgregar').text("Agregar");
			$('#btnAgregar').css('background-color','#00a65a');
			$('#btnAgregar').css('border-color', '#008d4c');
			$('#btnAgregar').on('click',() => {
				if($('#form_producto').valid()){
					agregarBeneficio();
				}else{
					swal(
  						'¡Advertencia!',
  						'Debe seleccionar un benificio y la cantidad',
  						'warning'
					)
				}
			});
		}else{
			limpiarBeneficio();
			$('#beneficios').removeAttr('disabled');
			$('#tipo').removeAttr('disabled');
			$("#btnEditar").off('click');
			$('#btnEditar').attr('id','btnAgregar');
			$('#btnAgregar').text("Agregar");
			$('#btnAgregar').css('background-color','#00a65a');
			$('#btnAgregar').css('border-color', '#008d4c');
			$('#btnAgregar').on('click',() => {
				if($('#form_producto').valid()){
					agregarBeneficio();
				}else{
					swal(
  						'¡Advertencia!',
  						'Debe seleccionar un benificio y la cantidad',
  						'warning'
					)
				}
			});
		}
	});
}

var guardar = () => {
    nuevaSolicitud.status = 'pendiente';
    guardarSolicitud();
}

var guardar_enviar = () => {
    nuevaSolicitud.status = 'vinculado';
    guardarSolicitud();
}

var guardarSolicitud1 = () => {
    nuevaSolicitud.fechaoficio = $('#fechaoficio').val();
    nuevaSolicitud.fecharecepcion = $('#fecharecepcion').val();
    nuevaSolicitud.numoficio = $('#numoficio').val();
    nuevaSolicitud.asunto = $('#asunto').val();
    nuevaSolicitud.tiposrecepcion_id = $('#tiporecepcion_id').val();
    nuevaSolicitud.tiposremitente_id = $('#tiporemitente_id').val();
    nuevaSolicitud.persona_id = $('#titular_id').val();
    $.ajax({
        url: '{{ route('solicitudes.store')}}',
        type: 'POST',
        data: nuevaSolicitud,
        success: function(response) {
            console.log(response);
            var re = "{{ URL::to('atnciudadana/solicitudes') }}";
            window.location.href = re;
        },
        error: function(response) {
            unblock();
        }
    });
}

var guardarSolicitud = () => {
	//Datos Solicitud
	block();
	nuevaSolicitud.tipo = "I";
	if($('#titular_id').val() != null){
		nuevaSolicitud.persona_id = $('#titular_id').val(); //Dirigido A
	}else{
		nuevaSolicitud.persona_id = $('#otro_id').val();
		nuevaSolicitud.otro = true;
	}
	nuevaSolicitud.status = 'pendiente';
	nuevaSolicitud.tiposrecepcion_id = $('#tiporecepcion_id').val(); //Recibido En
    nuevaSolicitud.tiposremitente_id = $('#tiporemitente_id').val(); //Tipo Remitente
	nuevaSolicitud.numoficio = $('#numoficio').val();
	nuevaSolicitud.fechaoficio = $('#fechaoficio').val();
    nuevaSolicitud.fecharecepcion = $('#fecharecepcion').val();
	nuevaSolicitud.asunto = $('#asunto').val();
	console.log(nuevaSolicitud.tiposremitente_id);
	if ( $('#tiporemitente_id').val() == 1){
		nuevaSolicitud.remitente = $('#remitente_region_id').val();
	}else if ($('#tiporemitente_id').val() == 2){
		nuevaSolicitud.remitente = $('#remitente_municipio_id').val();
	}else if($('#tiporemitente_id').val() == 3){
		nuevaSolicitud.remitente = $('#dependencia_id').val();
	}
	console.log(nuevaSolicitud);
	$.ajax({
        url: '{{ route('solicitud.auto.store')}}',
        type: 'POST',
        data: nuevaSolicitud,
        success: function(response) {
            //var re = "{{ URL::to('atnciudadana/solicitudes') }}";
            //window.location.href = re;
			console.log(response);
			nuevaSolicitud.solicitud_id = response.success[0].id;
			console.log(nuevaSolicitud);
			unblock();
			$('.nav-tabs a[href="#tab_peticion"]').tab('show');
        },
        error: function(response) {
            unblock();
        }
    });
}


var guardarBeneficios = () => {
	//Datos Solicitud
	block();
	nuevaSolicitud.status = 'vinculado';
	console.log(nuevaSolicitud);
	$.ajax({
        url: '{{ route('solicitud.beneficios.store')}}',
        type: 'POST',
        data: nuevaSolicitud,
        success: function(response) {
            //var re = "{{ URL::to('atnciudadana/solicitudes') }}";
            //window.location.href = re;
			console.log(response);
			nuevaSolicitud.solicitud_id = response.id;
			console.log(nuevaSolicitud);
			unblock();
			var re = "{{ URL::to('atnciudadana/solicitudes') }}";
            window.location.href = re;
			//$('.nav-tabs a[href="#tab_peticion"]').tab('show');
        },
        error: function(response) {
            unblock();
        }
    });
}

var validSolicitud = () => {
	if ($('#form_remitente').valid()) {
		if($('#tiporemitente_id').val() != 3){
			if( nuevaSolicitud.solicitante_id != undefined) {
				guardarSolicitud();
			}else{
				swal(
					'¡Advertencia!',
					'Es necesario buscar e indicar un Remitente',
					'warning'
				)
			}
		}else{
			guardarSolicitud();
		}
	}
}

$('#btnGuardarEnRemitente').on('click',() => {
	nuevaSolicitud.status = 'solicitante';
	validSolicitud();

});

$('#btnGuardarEnPeticion').on('click',() => {
	nuevaSolicitud.status = 'solicitante';
	validSolicitud();
});


$('#btnGuardarVincular').on('click',() => {
	nuevaSolicitud.status = 'vinculado';
	if (nuevaSolicitud.beneficios.length == 0) {
		swal(
  			'¡Advertencia!',
  			'No se puede turnar sin beneficios',
  			'warning'
		);
	} else {
		swal({
			title: '¿Está seguro de vincular?',
			text: 'Ya no podrá editar la solicitud después',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'No',
			confirmButtonText: 'Sí'
		}).then((result) => {
			if(result.value) {
				guardarBeneficios();
			}
		});
	}
});

var candidato = (function() {
		var modal_personas = $('#modal-personas'),
		modal_tutores = $('#modal-tutores'),
		modal_persona = $('#modal-persona'),
		personas = $('#personas'),
		datatable_personas = undefined,
		datatable_tutores = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search'),
		elemento = undefined,
		ele = undefined,
		tutor = $('#tutor'),
		fila_tutor = $('#fila_tutor'),
		discapacidad = $('#discapacidad_id'),
		estado_civil = $('#estado_civil'),
		grado_estudios = $('#grado_estudios'),
		banco = $('#banco_id'),
		form = $('#form_candidato'),
		action = form.attr('action'),
		method = form.attr('method');

		function agregar_modal_static() {
			$('.modal').modal({
				backdrop: 'static'
			});

			$('.modal').modal('hide');
		};

		function abrir_modal_personas(el) {
			elemento = $('#' + el);
			ele = el;
			if(ele === 'remitente_id') {
				modal_personas.modal('show');
			}
			if(ele === 'otro_id') {
				modal_personas.modal('show');
			}
		};

		function persona_create_edit_success(response) {
			if(elemento !== undefined && ele !== undefined) {
				elemento.html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
				modal_persona.modal('hide');
				if(ele === 'remitente_id') {
					modal_personas.modal('show');
				}
				if(ele === 'otro_id') {
					modal_personas.modal('show');
				}
			}
			unblock();
		};

		function persona_create_edit_error(response) {
			unblock();
		};

		function init_modal_persona() {
			$.fn.modal.Constructor.prototype.enforceFocus = function() {};

            var tableB = $('#beneficiosDT').dataTable();
		    datatable_beneficios = $(tableB).DataTable();

			var table = $('#personas').dataTable();

			datatable_personas = $(table).DataTable();

			search.keypress(function(e) {
				if(e.which === 13) {
					datatable_personas.search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				datatable_personas.search(search.val()).draw();
			});

			modal_personas.on('shown.bs.modal', function(event) {
				recargar_tabla_personas();
			});

			var tabla_tutores = $('#tutores').dataTable();

			datatable_tutores = $(tabla_tutores).DataTable();

			modal_personas.on('shown.bs.modal', function(event) {
				recargar_tabla_tutores();
			});
		}

		function recargar_tabla_personas() {
			if(datatable_personas !== undefined) {
				datatable_personas.ajax.reload();
			}
		};

		function recargar_tabla_tutores() {
			if(datatable_tutores !== undefined) {
				datatable_tutores.ajax.reload();
			}
		};

		function seleccionar_persona(id, nombre) {
			if(elemento !== undefined) {
				elemento.html("<option value='" + id + "'selected>" + nombre + "</option>");
				if(ele === 'remitente_id') {
					modal_personas.modal('hide');
				}
				if(ele === 'otro_id') {
					modal_personas.modal('hide');
				}
			}
		};

		function agregar_tutor() {
			tutor.change(function() {
				if($(this).val() === 'NO') {
					$('#otro_id').rules('remove', 'required');
					fila_tutor.removeAttr('style').hide();
				}
				if($(this).val() === 'SI') {
					$('#otro_id').rules('add', { required: true });
					fila_tutor.show();
				}
			});
		};

		function agregar_discapacidad_select_create() {
			discapacidad.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('discapacidades.select') }}',
					dataType: 'JSON',
					type: 'GET',
					//delay: 500,
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						var datos = [];

						for(var j = 0; j < data.length; j++){
							var found = false;

							for(var i = 0; i < datos.length; i++) {
								if(datos[i].text === data[j].padre) {
									datos[i].children.push({
										'id': data[j].id,
										'text': data[j].nombre
									});

									found = true;

									break;
								}
							}

							if(!found) {
								datos.push({
									text: data[j].padre,

									children: [{
										'id': data[j].id,
										'text': data[j].nombre
									}]
								});
							}
						}

						params.page = params.page || 1;

						return {
							results: datos
						};
					},
					cache: true
				}
			}).change(function(event) {
				discapacidad.valid();
			});

			estado_civil.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});

			grado_estudios.select2({
				language: 'es'
			});

			tutor.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});
		};

		function agregar_banco_select_create() {
			banco.select2({
				language: 'es',
				//minimumInputLength: 2,
				ajax: {
					url: '{{ route('bancos.select') }}',
					//delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				banco.valid();
			});
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					foliounico: {
						required: true,
						minlength: 5,
						maxlength: 5,
						pattern_integer: ''
					},
					remitente_id: {
						required: true
					},
					otro_id: {
						required: true
					},
					discapacidad_id: {
						required: true
					},
					estadocivil_id: {
						required: true
					},
					escolaridad_id: {
						required: true
					},
					ocupacion_id: {
						required: true
					}/*,
					banco_id: {
						required: true
					},
					num_cuenta: {
						required: true,
						minlength: 11,
						maxlength: 11,
						pattern_integer: ''
					},
					num_tarjeta: {
						required: false,
						minlength: 16,
						maxlength: 16,
						pattern_integer: ''
					}*/
				},
				messages: {
				}
			});
		};

		function create_edit() {
			if(form.valid()) {

				block();

				var formData = new FormData(form[0]);

				@if(isset($candidato))
				var old_candidato_id = {{ $candidato->id }};
				formData.append('old_candidato_id', old_candidato_id);
				@endif

				@if(isset($tutor))
				var old_tutor_id = {{ $tutor->id }};
				formData.append('old_tutor_id', old_tutor_id);
				@endif

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(response) {
						app.set_bloqueo(false);
						unblock();
						swal({
							title: 'Solicitante registrado.',
							text: '¿Agregar otro solicitante?',
							type: 'success',
							timer: 10000,
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonClass: 'btn btn-success',
							cancelButtonClass: 'btn btn-danger',
							confirmButtonText: 'Sí',
							cancelButtonText: 'No',
							allowEscapeKey: false,
							allowOutsideClick: false
						}).then((result) => {
							if(result.value) {
								location.reload();
							}

							if(result.dismiss) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}

							if(result.dismiss === swal.DismissReason.timer) {
								var re = "{{ URL::to('bienestar/solicitantes/') }}";
								window.location.href = re;// + '?n=' + new Date().getTime();
							}
						});
					},
					error: function(response) {
						unblock();
						if(response.status === 409) {
							var e = JSON.parse(response.responseText).errors[0];
							var l = e.data;
							var m = e.message;
							swal({
								title: 'Error al tratar de guardar al solicitante.',
								html: m,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Sí',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
					}
				});

			}
		};

		return {
			agregar_modal_static: agregar_modal_static,
			persona_create_edit_success: persona_create_edit_success,
			persona_create_edit_error: persona_create_edit_error,
			abrir_modal_personas: abrir_modal_personas,
			init_modal_persona: init_modal_persona,
			recargar_tabla_personas: recargar_tabla_personas,
			seleccionar_persona: seleccionar_persona,
			agregar_tutor: agregar_tutor,
			agregar_discapacidad_select_create: agregar_discapacidad_select_create,
			agregar_banco_select_create: agregar_banco_select_create,
			agregar_validacion: agregar_validacion,
			create_edit: create_edit
		};
	})();

	function agregar_persona() {
		var modal_persona = $('#modal-persona');

		var modal_title_persona = $('#modal-title-persona');

		var modal_body_persona = $('#modal-body-persona');

		var modal_footer_persona = $('#modal-footer-persona');

		$.get('/personas/search?tipo=create_edit', function(data) {
		})
		.done(function(data) {
		})
		.fail(function(data) {

			modal_title_persona.text('Agregar Persona:');
			modal_body_persona.html($.parseJSON(data.responseText).html);

			app.to_upper_case();

			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();

			modal_persona.modal('show');

			var btn_create_edit = $('#btn_create_edit');
			btn_create_edit.attr('class', 'btn btn-success');
			btn_create_edit.html('<i class="fa fa-plus"></i> Agregar');

		});
	}

	candidato.agregar_modal_static();
	candidato.init_modal_persona();
	candidato.agregar_validacion();

	function seleccionar_persona(id, nombre) {
		candidato.seleccionar_persona(id, nombre);
		nuevaSolicitud.solicitante_id = id;
	}

</script>
