@extends('atnciudadana::layouts.master')

@section('content-subtitle', (isset($solicitud) ? 'Editar' : 'Nueva') . ' Solicitud')

@section('myCSS')
    <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
    <link href="{{asset('plugins/dropzone/dist/dropzone.css')}}" type="text/css" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
    <style>
      #btnNext_solicitud,#btnPrevious_peticion,#btnPrevious_beneficiarios {
      transition: all 0.5s;
      font-size: 20px;
      padding: 3px 25px;
      border-radius: 3px;
    }

    #btnNext_solicitud:hover,#btnPrevious_peticion:hover,#btnPrevious_beneficiarios:hover {
      box-shadow: 1px 1px 5px #d12654b3;
    }
    
    i {
      -webkit-transition: all .4s ease-in-out;
    }

    #btnNext_solicitud:hover i.fa-arrow-circle-right,#btnPrevious_peticion:hover i.fa-arrow-circle-left, #btnPrevious_beneficiarios:hover i.fa-arrow-circle-left {
        transform: rotateX(360deg);
    }
        span.select2-selection__rendered {
            text-transform: uppercase;
        }

        .modal-dialog:not([role="document"]) > .modal-content > .modal-body {
            overflow-y: auto;
            max-height: calc(100vh - 210px);
        }

        #modal-body-beneficiario {
            min-height: 400px;
        }

        .modal-body {
            overflow-y: auto;
            max-height: calc(100vh - 210px);
        }

        #navFicha>li>a:hover, #navFicha>li>a:active, #navFicha>li>a:focus {
            background: #d46985;
        }
        .noeliminar{
            padding: 3px 5px;
            font-size: 12px;
            line-height: 1.5;
            font-weight: 100;
            border: 1px solid;
            border-color: #c1493b;
            border-radius: 3px;
            cursor: no-drop;
            background-color: #d67d72 !important;
            color: #ffffff !important;
        }
  </style>
@endsection

@section('li-breadcrumbs')
    <li class="active"><a href="{{ route('solicitudes.index') }}">Solicitudes</a></li>
    <li class="active">{{ isset($solicitud) ? 'Editar' : 'Nueva' }} Solicitud</li>
@endsection

@section('content')
    <div class="row shadow">
        <div class="col-xs-12 no-padding-lr">
            <div class="nav-tabs-custom no-padding-b" id="tabSolicitud">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_solicitud">Solicitud</a></li>
                    <li><a href="#tab_peticion">Peticiones</a></li>
                    <li style="display: none;"><a href="#tab_beneficiarios">Beneficiarios</a></li>
                    <li style="display: none;"><a href="#tab_anexos">Anexos</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="tab_solicitud">
                        <div class="box-body">
                            <h3 class="nmt">Datos generales de la solicitud</h3>
                            @include('atnciudadana::solicitudes.tab.solicitud2')              
                        </div>
                        <div class="box-footer">
                        <button id="btnNext_solicitud" type="button" class="btn btn-primary pull-right" title="Continuar">
                          <i class="fa fa-arrow-circle-right"></i>
                        </button>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_peticion">
                      <div class="box-body">
                        <h3>Petición</h3>
                        @include('atnciudadana::solicitudes.tab.peticion2')
                      </div>
                      <div class="box-footer">
                        <button id="btnPrevious_peticion" type="button" class="btn btn-primary pull-left" title="Regresar">
                          <i class="fa fa-arrow-circle-left"></i>
                        </button>
                        <div class="pull-right">
                          <a href="{{ route('solicitudes.index', ['tipo_solicitud' => $tipo_solicitud]) }}" class="btn btn-warning"><i class="fa fa-reply"></i> Ir a las solicitudes</a>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane" id="tab_beneficiarios">
                        <h3>Beneficiarios</h3>
                        @include('atnciudadana::solicitudes.tab.beneficiarios2')
                    </div>
                    <div class="tab-pane" id="tab_anexos">
                        <h3>Anexos</h3>
                        <form method="post" action="{{route('atnciudadana.solicitudes.anexos.store', 54)}}" enctype="multipart/form-data" class="dropzone" id="dropzone">
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('atnciudadana::solicitudes.modals')

@stop

@section('myScripts')
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
    <script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
    <script type="text/javascript" src="{{asset('plugins/dropzone/dist/dropzone.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    {!! $giras->html()->scripts() !!}
    {!! $personas->html()->scripts() !!}
    {!! $dependencias->html()->scripts() !!}
    {!! $beneficios->html()->scripts() !!}
    {!! $solicitantes->html()->scripts() !!}    
    {!! $beneficiosExt->html()->scripts() !!}    
    @include('personas.js.persona')
    @include('dependencias.js.dependencia')
    @include('atnciudadana::solicitudes.js.personas')
    @include('atnciudadana::solicitudes.js.dependencias')
    @include('atnciudadana::solicitudes.js.beneficiosExternos')
    @include('atnciudadana::solicitudes.js.solicitud')
    @include('atnciudadana::solicitudes.js.beneficiarios')
    @include('atnciudadana::solicitudes.js.giras')
	<script type="text/javascript">
	
		var cerrar_modal = (modal) => {
        	$("#"+modal).modal("hide");
    	}

        //Funciones personalizadas para cada módulo
        var seleccionar_persona = (id, nombre) => {
            Personas.seleccionar(id, nombre);
        }

        var seleccionar_dependencia = (id, nombre) => {
            Dependencias.seleccionar(id, nombre);
        }

        var seleccionar_beneficioExterno = (id, nombre) => {
            BeneficiosExt.seleccionar(id, nombre);
        }

        var abrir_modal = () => {
            Dependencias.agregar();
	    }

        var agregar_persona = () => {
            Personas.agregar();
        }

        var persona_create_edit_error = (response) => {
            Personas.create_edit_error(response);
        }

        var persona_create_edit_success = (response) => {
            Personas.seleccionar(response.id, response.nombre);
        }

        var dependencia_create_edit_success = (response) => {
		    Dependencias.seleccionar(response.id, response.nombre);
	    }

	    var dependencia_create_edit_error = (response) => {
		    Dependencias.create_edit_error(response);
        }        

        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });            
            Solicitud.inicializar();
            Personas.inicializar();
            Dependencias.inicializar();
            Beneficiarios.inicializar();
            Giras.inicializar();
            BeneficiosExt.inicializar();
        });
    </script>
@endsection