<script>
    var BeneficiosExt = (() => {

        var elemento = undefined;
        var datatable_beneficiosExt = undefined;

        var inicializar = () => {                
            datatable_dependencias = $('#beneficiosExt').DataTable();
        }

        var abrir_modal = (e) => {
            elemento = e;
            $('#modal-beneficiosExt').modal('show');
        }

        var seleccionar = (id, nombre) => {                
            var newOption = new Option(nombre, id, true, true);
            $('#'+elemento).append(newOption).trigger('change');
            $('#modal-beneficiosExt').modal('hide');
            unblock();
        }

        return {
            inicializar,
            abrir_modal,
            seleccionar
        }
    })();
</script>