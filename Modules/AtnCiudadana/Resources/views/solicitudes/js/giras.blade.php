<script>
	var Giras = ( ()=> {

        var abrir_modal = () => {
            $(".modal-gira").hide().removeClass("button-dt");
            $('#modal-giras').modal('show');
        }

        var cerrar_sin_seleccionar = () => {
            $('#tiposrecepcion_id').val(null).trigger('change');
            $('#modal-giras').modal('hide');
        }

        var inicializar = () => {
            $.fn.modal.Constructor.prototype.enforceFocus = () => {};
            var datatable_giras = $('#giras').DataTable();
            $('#search_giras').keypress(e => {
                if(e.which === 13) {
                    datatable_giras.search($('#search_giras').val()).draw();
                }
            });
            $('#btn_buscar_giras').on('click', () => {
                datatable_giras.search($('#search_giras').val()).draw();
            });
            $('#modal-giras').on('shown.bs.modal', evt => {
                if(datatable_giras !== undefined) {
                	datatable_giras.ajax.reload();
            	}
            });
        }

        var seleccionar = (id, nombre, municipio) => {
			var newOption = new Option(nombre + ' (' + municipio + ')', id, true, true);
			$('#evento_id').append(newOption).trigger('change');
            $('#evento_id').valid();
			$('#div_evento').removeClass('hide');
        	$('#modal-giras').modal('hide');
      };

        return {
            abrir_modal,
            seleccionar,
            inicializar,
            cerrar_sin_seleccionar
        }
    })();
</script>