<script>
    //Patrón auxiliar para controlar el modal y selección de dependencias
    var Dependencias = (() => {

        var elemento = undefined;
        var datatable_dependencias = undefined;

        var inicializar = () => {                
            datatable_dependencias = $('#dependencias').DataTable();
            $('#search_dependencias').keypress(e => {
                if(e.which === 13) {
                    datatable_dependencias.search($('#search_dependencias').val()).draw();
                }
            });
            $('#btn_buscar_dependencias').on('click', () => {
                datatable_dependencias.search($('#search_dependencias').val()).draw();
            });
            $('#modal-dependencias').on('shown.bs.modal', evt => {
                if(datatable_dependencias !== undefined) {
                    datatable_dependencias.ajax.reload();
                }
            });
        }

        //El parámetro "e" hace referencia al elemento que lo llamó
        var abrir_modal = (e) => {
            elemento = e;
            $('#modal-dependencias').modal('show');
        }

        var agregar = () => {
            block();
            $.get('{{route("dependencias.create")}}', function(data) {
                $('#modal-body-dependencia').html(data.html);                            
                dependencia.init();			
                $('#modal-dependencia').modal('show');            
                unblock();
            });
        }

        var seleccionar = (id, nombre) => {                
            var newOption = new Option(nombre, id, true, true);
            $('#'+elemento).append(newOption).trigger('change');
            $('#modal-dependencias, #modal-dependencia').modal('hide');
            unblock();
        }

        var mostrar = (id) => {
            //Muestra informacion del beneficiario
            if(id){

            }
            //Muestra información del remitente
            else if($('#dependencia_id').val() !== null){
                block();
                var url = '{{ route("dependencias.show", [":id"] ) }}';
                url = url.replace(':id', $('#dependencia_id').val());
                $.get(url, function(data) {
                    $('#modal-beneficiario').html(data.dependencia);
                    $('#modal-beneficiario').modal('show');
                    unblock();
                });
            }
            //No se ha seleccionado a ninguna dependecia
            else{
                swal(
                    '',
                    'Debe seleccionar una dependencia',
                    'info'
                )
            }
        }      

        var create_edit_error = (response) => {
            unblock();
            if(response.status === 422) {
                swal({
                    title: 'Error al registrar',				
                    text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
                    type: 'error',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Regresar',
                    allowEscapeKey: false,
                    allowOutsideClick: false
                });
            }else{
                swal(
                    '¡Error!',
                    'Ocurrió un error inesperado',
                    'error'
                );
            }
        }

        return {
            inicializar,
            abrir_modal,
            agregar,
            seleccionar,
            mostrar,
            create_edit_error
        }
    })();
</script>