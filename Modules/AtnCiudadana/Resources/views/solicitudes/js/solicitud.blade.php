<script>

    var Solicitud = (() => {

        var id = undefined;
        var datatable_beneficios = undefined;
        @if(isset($solicitud))
            id = {{ $solicitud->id }};
        @endif

        var guardar = (oldTab, newTab) => {
            block();
            var url = id ? '{{ route("solicitudes.update", ":id") }}' : '{{ route("solicitudes.store")}}'; 
            url = id ? url.replace(':id', id) : url; 
            var type = id ? 'PUT' : 'POST';
            var data = id ? $('#form_solicitud').serialize() : $('#form_solicitud').serialize() + '&tipo={{ $tipo_solicitud }}';
            var title = id ? 'Solicitud actualizada' : 'Solicitud registrada';

            $.ajax({
                url: url,
                type: type,
                data: data,
                success: (response) => {
                    id = response.success[0].id;
                    $(newTab).addClass('active');
                    $(oldTab).removeClass('active');
                    unblock();
                    swal({
                        title: title,
                        type: 'success',
                        timer: 3000,
                        toast : true,
                        position : 'top-end',
                        showConfirmButton: false
                    });
                },
                error: (response) => {
                    unblock();
                    swal(
                        'Error',
                        'Revise los datos',
                        'error'
                    )
                }
            });              
        }

        var inicializar = () => {
            datatable_beneficios = $('#beneficiosDT').DataTable();
            
            $('[data-toggle="tooltip"]').tooltip();

			$('#btnNext_solicitud').on('click', function(){
                LaravelDataTables.beneficiosDT.ajax.reload(null,false);
              	$( "#tabSolicitud" ).tabs({ active: 1 });          
            });

			$('#btnPrevious_peticion').on('click', function(){
              	$( "#tabSolicitud" ).tabs({ active: 0 });
            });			

            // Desactivar click en los tabs
            $('a[data-toggle="tab"]').on('click', () => {
                if ($(this).parent('li').hasClass('disabled')) {
                    return false;
                };
            });

            //Checks para el campo compromiso
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

            $('#form_solicitud').validate({
                rules: {    
                    persona_id: {
                        required: true
                    },
                    tiposrecepcion_id: {
                        required: true
                    },
                    tiposremitente_id: {
                        required: true
                    },
                    numoficio: {
                        required: true,
                        minlength: 1,
                        maxlength: 30                
                    },
                    fechaoficio: {
                        required: true
                    },
                    fecharecepcion: {
                        required: true
                    },
                    asunto: {
                        required: true,
                        minlength: 10,
                    },
                    otro_id: {
                        required: true
                    },
                    solicitud_municipio_id: {
                        required: true
                    },
                    region_id: {
                        required: true
                    },
                    cargo: {
                        required: true
                    },
                    dependencia_id: {
                        required: true
                    },
                    solicitante_id: {
                        required: true
                    },
					evento_id: {
						required: true
					}
                },
                messages: {
                    numoficio: { 
                        required: "Ingrese X si no dispone de uno.",
                        maxlength: "Por favor, no escribas más de 30 caracteres."
                    }
                }
            });

            $('#form_peticion').validate({
                rules: 	{
                    dependencia_externa:{
                        required: true
                    },
                    tag:{
                        required: true
                    },
                    programa_id: {
                        required: true
                    },
                    programa_id: {
                        required: true
                    },
                    cantidad: {
                        required: true,
                        minlength: 1,
                        pattern_folios: 'solo numeros, X si no dispone de uno.'
                    }
                }
            });

            $('#form_cancelar').validate({
                rules: {
                    motivo: {
                        required: true
                    }
                }
            });

            $("#tabSolicitud").tabs({
                beforeActivate: (event, ui) => {                        
                    if(ui.oldPanel[0].id === 'tab_solicitud' && ui.newPanel[0].id === 'tab_peticion'){
                        if($('#form_solicitud').valid()) {                            
                            guardar(ui.oldTab[0], ui.newTab[0]);
                        }else{
                            return false;
                        }
                    }else if(ui.oldPanel[0].id === 'tab_beneficiarios' && ui.newPanel[0].id === 'tab_peticion'){
                        $("[href='#tab_beneficiarios']").parent().hide();                    
                    }else{
                        $(ui.newTab[0]).addClass('active');
                        $(ui.oldTab[0]).removeClass('active');
                    }
                }
            });

            $('#fechaoficio').datepicker({
                autoclose: true,
                language: 'es',
                endDate: '0d',
                orientation: 'bottom'
            }).on('changeDate', function(selected){
                startDate = new Date(selected.date.valueOf());
                startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                startDate.setDate(startDate.getDate());
                $('#fecharecepcion').datepicker('setStartDate', startDate);
            }).change(function(event) {
                $("#fechaoficio").valid();	
            });

            $('#fecharecepcion').datepicker({
                language: 'es',
                autoclose: true,
                endDate: '0d',
                orientation: 'bottom'
            }).on('changeDate', function(selected){
                FromEndDate = new Date(selected.date.valueOf());
                FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                FromEndDate.setDate(FromEndDate.getDate());
                $('#fechaoficio').datepicker('setEndDate', FromEndDate);
            }).change(function(event) {
                $("#fechaoficio, #fecharecepcion").valid();	
            });    

            $("#tiposrecepcion_id").select2({
                language: 'es'			
            }).change(function(event) {
              if ($('#tiposrecepcion_id').valid()){
                $('#div_evento').addClass('hide');
                if($('#tiposrecepcion_id').find("option:selected").text() == "GIRA"){					
                  Giras.abrir_modal();
                }
              }else{
                $('#div_evento').addClass('hide');
              }                            
            });

            $("#persona_id").select2({
                language: 'es',		            
                minimumResultsForSearch: Infinity			
            }).change(function(event) {
                $("#persona_id").valid();
                if($('#persona_id').find("option:selected").text() == "OTRO"){
                    Personas.abrir_modal('persona_id');
                }
            });

            $("#tiposremitente_id").select2({
                language: 'es',		            
                minimumResultsForSearch: Infinity			
            }).change(function(event) {
                if( $(this).valid() ){
                    $('#sin_remitente').addClass('hide');
                    $('.remitente').addClass('hide');
                    switch($(this).val()){
                        case "1":
                            $('#divRemitente, #divCargo, #divRegion').removeClass('hide');
                        break;

                        case "2":
                            $('#divRemitente, #divCargo, #divMunicipio').removeClass('hide');
                        break;

                        case "3":
                            Dependencias.abrir_modal('dependencia_id');
                            $('#divDependencia').removeClass('hide');
                        break;

                        case "4":
                            $('#divRemitente').removeClass('hide');
                        break;
                    }
                }
            });

            $('#solicitud_municipio_id').select2({
                language: 'es',
                minimumInputLength: 2,
                allowClear : true,
                placeholder : 'SELECCIONE EL MUNICIPIO',
                ajax: {
                    url: '{{ route('municipios.select') }}',
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data:params => {
                        return {
                            search: params.term
                        };  
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(evt => {
                $('#solicitud_municipio_id').valid();
            });

            $('#region_id').select2({
                language: 'es',
                allowClear : true,
                placeholder : 'SELECCIONE LA REGIÓN',
                ajax: {
                    url: "{{ route('regiones.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.id,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(evt => {
                $('#region_id').valid();
            });

            $('#cargo').select2({
                language: 'es',
                tags: true,
                placeholder : 'SELECCIONE O INGRESE EL CARGO',
                ajax: {
                    url: "{{ route('cargos.select') }}",
                    delay: 500,
                    dataType: 'JSON',
                    type: 'GET',
                    data: params => {
                        return {
                            search: params.term
                        };
                    },
                    processResults: (data, params) => {
                        params.page = params.page || 1;
                        return {
                            results: $.map(data, item => {
                                return {
                                    id: item.nombre,
                                    text: item.nombre,
                                    slug: item.nombre,
                                    results: item
                                }
                            })
                        };
                    },
                    cache: true
                }
            }).change(evt => {
                $('#cargo').valid();
            });

            $("#motivo").select2({
		        language: 'es',
		        placeholder: 'SELECCIONE UN MOTIVO',
                minimumResultsForSearch: Infinity			
	        }).change(function(event) {
		        $("#motivo").valid();				
            });

            if('{{$tipo_solicitud}}' != 'E') {
                $('#programa_id').select2({
                    language: 'es',
                    ajax: {
                        delay: 500,
                        url: "/beneficios",
                        dataType: 'JSON',
                        type: 'GET',
                        data: params => {
                            return {
                                search: params.term,
                                type : 'query_append',
                                page : params.page || 1
                            };
                        },
                        processResults: (data, params) => {
                            params.page = params.page || 1;
                            return {
                                results: $.map(data.data, item => {
                                    return {
                                        id: item.id,
                                        text: item.nombre,
                                        slug: item.nombre,
                                        results: item
                                    }
                                }),
                                pagination: {
                                    more : data.current_page < data.last_page
                                }
                            }
                        },
                        cache: true
                    }
                }).change(evt => {
                    if($('#programa_id').val()!=null && !isNaN($('#programa_id').val()))
                        $.get('/beneficios/'+$('#programa_id').val()).done(response=>{
                            $('#areaR').text(response.area);
                            $('#areaT').text(response.responsable);
                        })                
                });
            }
            
            $('#modal-beneficiario').on('shown.bs.modal', function (event) {
                if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
                    $("#apoyos-otorgados").DataTable({
			            language: {
				            url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
			            },
			            dom: 'itp',
			            lengthMenu: [ [5], [5] ],
			            responsive: true,
			            autoWidth: true,
			            processing: true,
			            columnDefs: [
				            { className: 'text-center', 'targets': '_all' }
			            ]
		            });            
                }
	        });

            $('#btnAgregar').on('click',() => {
                if($('#form_peticion').valid()){
                    agregar_peticion();
                }else{
                    var msg = 'Debe seleccionar un beneficio y la cantidad';
                    if ('{{ $tipo_solicitud }}' == 'E'){
                        msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
                    }
                    swal(
                        '¡Advertencia!',
                        msg,
                        'warning'
                    )
                }
            });
        }

        var agregar_peticion = (programa_id,folio) => {
            block();

            var url = programa_id ? '{{ route("atnciudadana.solicitudes.programas.update", [":id",":programa"]) }}' : '{{ route("atnciudadana.solicitudes.programas.store", ":id") }}'; 
            url = programa_id ? url.replace(':id', id).replace(':programa', programa_id) : url = url.replace(':id', id);

            var type = programa_id ? 'PUT' : 'POST';            

            $.ajax({
                url: url,
                type: type,
                data: $('#form_peticion').serialize()+'&folio='+folio,
                success: function(response) {
                    datatable_beneficios.ajax.reload(null, false);
                    clear_responsable();
                    resetBoton();
                    unblock();
                },
                error: function(response) {
                    unblock();                    
                    if(response.status === 422) {
                        swal({
                            title: 'Error al registrar',				
                            text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]].message,
                            type: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Regresar',
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        });
                    } else {
                        swal(
                            '¡Error!',
                            'Ocurrió un error inesperado',
                            'error'
                        );
                    }
                }
            });        
        }

        var clear_responsable = () => {
            $('#areaR').text('---------------------');
            $('#areaT').text('---------------------');
            $('#cantidad').val('');
            $('#programa_id').val(null).trigger('change');
            if('{{$tipo_solicitud}}' === 'E') {
                $('#dependencia_externa').val(null).trigger('change');
            }
        }

        var getId = () => {
            return id;
        }

        var resetBoton = () => {
            clear_responsable();
            $('#programa_id').removeAttr('disabled');
            $("#btnEditar").off('click');
            $('#btnEditar').attr('id','btnAgregar');
            $('#btnAgregar').text("Agregar");
            $('#btnAgregar').css('background-color','#00a65a');
            $('#btnAgregar').css('border-color', '#008d4c');
            $("#btnAgregar").off('click');
            $('#btnAgregar').on('click',() => {
                if($('#form_peticion').valid()){
                    agregar_peticion();
                }else{              
                var msg = 'Debe seleccionar un beneficio y la cantidad';
                    if ('{{ $tipo_solicitud }}' == 'E'){
                        msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
                    }
                    swal(
                        '¡Advertencia!',
                        msg,
                        'warning'
                    );
                }
            });
        }

        var eliminar_peticion = (programa_id,folio) => {
            swal({
                title: '¡Advertencia!',
                text: '¿Está seguro de eliminar este beneficio?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'No',
                confirmButtonText: 'Sí'
            }).then((result) => {
                if(result.value) {
                    var url = '{{ route("atnciudadana.solicitudes.programas.destroy", [":id",":programa"]) }}';
                    url = url.replace(':id', id).replace(':programa', programa_id);          
                    block();

                    $.ajax({
                        url: url,
                        type: 'DELETE',
                        data: {folio:folio},
                        success: function(response) {                            
                            datatable_beneficios.ajax.reload(null, false);
                            resetBoton();
                            unblock();
                        },
                        error: function(response) {
                            swal({
                                title: 'Error al eliminar',				
                                text: '',
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Regresar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                            unblock();
                        }
                    });
                }
            });
        }

        var turnar_peticion = (programa_id,folio) => {
            swal({
                title: '¡Advertencia!',
                text: "No podrá editar el beneficio ni agregar beneficiarios. ¿Desea turnar la petición?",
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí',
                cancelButtonText: 'Cancelar'
            }).then(r => {
                if(r.value){
                    var url = '{{ route("atnciudadana.solicitudes.programas.estatus", [":id",":programa"]) }}';
                    url = url.replace(':id', id).replace(':programa', programa_id);
                    block();
                    $.ajax({
                        url: url,
                        data: {
                            estatus_id:3,
                            folio : folio,
                            },
                        type: 'POST',
                        success: function(response) {
                            resetBoton();
                            datatable_beneficios.ajax.reload(null, false);
                            unblock();
                        },
                        error: function(response) {
                            swal({
                                title: 'Error al turnar',
                                text: '',
                                type: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'Regresar',
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            });
                            unblock();
                        }
                    });
                }
            });
        }

        var editar_peticion = (programa_id, apoyo, cantidad, folio,dependencia = null) => {
	        var beneficio = new Option(apoyo, programa_id, true, true);
            $('#programa_id').append(beneficio).trigger('change');
            $('#programa_id').attr('disabled', 'disabled');
            if(dependencia != null){
                $('#dependencia_externa').append(dependencia).trigger('change');
                $('#dependencia_externa').attr('disabled', 'disabled');
            }
            $('#cantidad').val(cantidad);
            $('#btnAgregar').off('click');
            $('#btnAgregar').attr('id','btnEditar');
            $('#btnEditar').text("Editar");
            $('#btnEditar').css('background-color','#ec971f');
	        $('#btnEditar').css('border-color', '#e08e0b');
            $('#btnEditar').on('click',() => {
                if($('#form_peticion').valid()){
                    agregar_peticion(programa_id,folio);
                }else{
                    var msg = 'Debe seleccionar un beneficio y la cantidad';
                    if ('{{ $tipo_solicitud }}' == 'E'){
                        msg = 'Debe seleccionar una dependencia, ingresar el tag y la cantidad';
                    }
                    swal(
  				        '¡Advertencia!',
  				        msg,
  				        'warning'
			        );
                }
            });
        }

        var recargar_peticiones = () => {
            datatable_beneficios.ajax.reload(null, false);
        }

        return {
            inicializar,
            guardar,
            getId,
            eliminar_peticion,
            turnar_peticion,
            editar_peticion,
            resetBoton,
            recargar_peticiones
        }

    })();
</script>