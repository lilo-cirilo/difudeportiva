@extends("atnciudadana::layouts.master")

@section('styles')
    @include('atnciudadana::layouts.links')
    <style type="text/css">
        .card-1 {
            background-color: #289F97 !important;
            color: white !important;
        }
        .card-2 {
            background-color: #57AA65 !important;
            color: white !important;
        }
        .card-3 {
            background-color: #1B96B8 !important;
            color: white !important;
        }
        .card-4 {
            background-color: #002b59 !important;
            color: white !important;
        }
        .card-4 i {
            color: #bdc3c724 !important;
        }
    </style>
@stop

@section('content-title', 'Atención Ciudadana')
@section('content-subtitle', auth()->user()->persona->empleado->area->nombre)

@section('li-breadcrumbs')
    <li class="active">{{auth()->user()->persona->empleado->area->nombre}}</li>
@endsection

@section('content')
    <section class="content">
        <div id="app">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box card-1">
                        <div class="inner">
                            <h3>
                                <i  v-if="peticiones===null" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                <span v-else>@{{nuevos}}</span>
                            </h3>
                            <p>Peticiones Nuevas</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-edit"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box card-2">
                        <div class="inner">
                            <h3>
                                <i  v-if="peticiones===null" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                <span v-else>@{{turnados}}</span>
                            </h3>
                            <p>Peticiones Turnadas</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-share-square-o"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box card-3">
                        <div class="inner">
                            <h3>
                                <i  v-if="peticiones===null" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                <span v-else>@{{procesados}}</span>
                            </h3>
                            <p>Peticiones en Proceso</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-list-alt"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <div class="small-box card-4">
                        <div class="inner">
                            <h3>
                                <i  v-if="peticiones===null" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                                <span v-else>@{{concluidos}}</span>
                            </h3>
                            <p>Peticiones Concluídas</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-check-square-o"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-area-chart"></i>
                            <h3 class="box-title">Peticiones por Programa</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row" style="display: flex; align-items: center;">
                                <div class="col-xs-4">
                                  <v-chart ref="chartPie" type=donut height=250 :options="chartOptionsPie" :series="seriesPie"></v-chart>
                                </div>
                                <div class="col-xs-8">
                                  <v-chart ref="chartBar" type=bar height=250 :options="chartOptionsBar" :series="series"></v-chart>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                          <i class="fa fa-list"></i>
                          <h3 class="box-title">Lista de peticiones</h3>
                          <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                        </div>
                        <div class="box-body">
                          <v-client-table ref="tablapeticiones" :data="datosTabla" :columns="columns" :options="options">
                            <div slot="beforeFilter" >
                              <button class="btn button-dt tool" type="button"  @click="">
                                <i class="fa fa-file-excel-o"></i> Excel
                              </button>
                              <button class="btn button-dt tool" type="button"  @click="">
                                <i class="fa fa-ban"></i> Filtros
                              </button>
                            </div>
                          </v-client-table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@push('body')
    <script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/vue/vue.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/vue/apexcharts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/vue/vue-apexcharts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/vue/vue-tables-2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/axios/axios.min.js')}}"></script>
    <script type="text/javascript">
        Vue.component('v-chart', VueApexCharts);
        Vue.use(VueTables.ClientTable);

        const app = new Vue({
            el: '#app',
            data: {
                programas: null,
                peticiones: null,
                chartOptionsBar: {
                    colors: ['#289F97','#57AA65','#1B96B8','#002b59'],
                    chart: {
                        stacked: true,
                        stackType: '100%'
                    },
                    title: {
                      text: 'Progreso por Programa',
                      align: 'left',
                      margin: 10,
                      offsetX: 0,
                      offsetY: 0,
                      floating: false,
                      style: {
                          fontSize:  '16px',
                          color:  '#002b59'
                      },
                    },
                    responsive: [{
                        breakpoint: 480,
                        options: {
                            legend: {
                                position: 'bottom',
                                offsetX: -10,
                                offsetY: 0
                            }
                        }
                    }],
                    plotOptions: {
                        bar: {
                          horizontal: true
                        }
                    },
                    stroke: {
                        width: 1,
                        colors: ['#fff']
                    }
                },
                chartOptionsPie: {
                  colors: ['#E58BAD','#EF426F','#DB3762','#FF585D','#98579B','#F5CB08','#E99C34','#E0E721','#A0CFA8','#71DBD4','#74D1EA','#002b59'],
                  legend: {
                    show: true,
                    position: 'bottom'
                  },
                  title: {
                    text: '...',
                    align: 'center',
                    margin: 10,
                    offsetX: 0,
                    offsetY: 0,
                    floating: false,
                    style: {
                        fontSize:  '16px',
                        color:  '#002b59'
                    },
                  },
                  responsive: [{
                    breakpoint: 480,
                    options: {
                      chart: {
                        width: 200
                      },
                      legend: {
                        position: 'bottom'
                      }
                    }
                  }]
                },
                series: [],
                seriesPie: [],
                datosTabla: [],
                columns: ['folio', 'fecha_turnado', 'solicitante','tipo','municipio','producto','cantidad','beneficiario','curp','estatusG','estatus','direccion'],
                options: {
                  columnsDisplay : {
                    estatusG: 'mobile'
                  },
                  sortIcon: {
                    is: "fa-sort",
                    base: "fa",
                    up: "fa-sort-asc",
                    down: "fa-sort-desc"
                  },
                  texts: {
                    count: " {from} a {to} de {count} registros|{count} Registros|1 Registro",
                    first: "First",
                    last: "Last",
                    filter: "Filtrar:",
                    filterPlaceholder: "Buscar",
                    limit: "Registros:",
                    page: "Pagina:",
                    noResults: "No se encontraron coincidencias",
                    filterBy: "Filtrar",
                    loading: "Cargando...",
                    defaultOption: "Seleccionar {column}",
                    columns: "Columnas"
                  },
                  highlightMatches: true,
                  filterByColumn: true,
                  perPage: 5,
                  perPageValues: [5,10,25,50]
                }
            },
            mounted(){
                this.cargarPeticiones();
            },
            computed: {
                nuevos(){
                    return this.series.length>0 ? this.series[0].data.reduce((total, num)=>total + num) : 0;
                },
                turnados(){
                    return this.series.length>0 ? this.series[1].data.reduce((total, num)=>total + num) : 0;
                },
                procesados(){
                    return this.series.length>0 ? this.series[2].data.reduce((total, num)=>total + num) : 0;
                },
                concluidos(){
                    return this.series.length>0 ? this.series[3].data.reduce((total, num)=>total + num) : 0;
                }
            },
            methods: {
                cargarPeticiones(){
                    axios.get('/atnciudadana/direccionprogramas').then(response => {
                        this.programas = response.data.map(p => p.nombre);
                        axios.get('/atnciudadana/peticionesAgrupadas/').then(response => {
                            this.peticiones = response.data;
                            this.datosTabla = Object.values(this.peticiones).flat();
                            this.series = this.crearSeriesBar(this.programas, this.peticiones);
                            this.$refs.chartBar.updateOptions({
                                xaxis: {
                                    categories: this.programas
                                },
                                chart: {
                                    height: this.programas.length > 2 ? this.programas.length * 75 : 150
                                }
                            });
                            this.$refs.chartBar.updateSeries(this.series);
                            this.seriesPie = this.crearSeriesPie(this.programas, this.peticiones);
                            this.$refs.chartPie.updateOptions({
                              labels: this.programas,
                              title: {
                                text: 'Total de Peticiones: ' + this.datosTabla.length
                              },
                              chart: {
                                height: this.programas.length > 2 ? this.programas.length * 65 : 150
                              }
                          });
                            this.$refs.chartPie.updateSeries(this.seriesPie);
                        });
                    });
                },
                crearSeriesBar(programas, peticiones) {
                    let series = [
                        {name:'Nuevas', data:[], estatusG:'NUEVO'},
                        {name:'Turnadas', data:[], estatusG:'VINCULADO'},
                        {name:'En Proceso', data:[], estatusG:'PROCESO'},
                        {name:'Concluídas', data:[], estatusG:'CONCLUIDO'}
                    ];
                    series.forEach( s => {
                        programas.forEach((e, i) => {
                            s.data[i] = this.peticiones[e] ? peticiones[e].filter(e => e.estatusG == s.estatusG).length : 0;
                        });
                    })
                    return series;
                },
                crearSeriesPie(programas, peticiones) {
                    let series = [];
                    programas.forEach((e, i) => {
                      series[i] = this.peticiones[e] ? this.peticiones[e].length : 0;
                    });
                    return series;
                }
            }
        });
    </script>
@endpush