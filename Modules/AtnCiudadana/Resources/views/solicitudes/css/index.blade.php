<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
    .row {
        margin-right: 0px;
        margin-left: 0px;
    }

    input {
        text-transform: uppercase;
    }

    select{
        width: 100%;
    }
    th.dt-center, td.dt-center { 
        text-align: center; 
    }
    .small-box {
        cursor: pointer;
    }

    .noedit{
        padding: 3px 5px;
        font-size: 12px;
        line-height: 1.5;
        font-weight: 100;
        border: 1px solid;
        border-color: #57C7E6;
        border-radius: 3px;
        cursor: no-drop;
        background-color: #74D1EA !important;
        color: #177D99 !important;
    }
</style>