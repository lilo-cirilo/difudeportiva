<div class="modal fade" id="modal-personas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-personas">Tabla personas:</h4>
            </div>
            <div class="modal-body" id="modal-body-personas">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_personas" name="search_personas" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="btn_buscar_personas" name="btn_buscar_personas">Buscar</button>
                    </span>
                </div>
                {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer" id="modal-footer-personas">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dependencias">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-dependencias">Dependencias e instituciones</h4>
            </div>
            <div class="modal-body" id="modal-body-dependencias">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_dependencias" name="search_dependencias" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="btn_buscar_dependencias" name="btn_buscar_dependencias">Buscar</button>
                    </span>
                </div>
                {!! $dependencias->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'dependencias', 'name' => 'dependencias', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-beneficiosExt">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-beneficiosExt">Beneficios Externos</h4>
            </div>
            <div class="modal-body" id="modal-body-beneficiosExt">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_beneficiosExt" name="search_beneficiosExt" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="btn_buscar_beneficiosExt" name="btn_buscar_beneficiosExt">Buscar</button>
                    </span>
                </div>
                {!! $beneficiosExt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiosExt', 'name' => 'beneficiosExt', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-giras">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="Giras.cerrar_sin_seleccionar()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Lista de giras y eventos</h4>
            </div>
            <div class="modal-body" id="modal-body-giras">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_giras" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-info btn-flat" id="btn_buscar_giras">Buscar</button>
                    </span>
                </div>
                {!! $giras->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'giras', 'name' => 'giras', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer">
                <div class="pull-right">
                    <button type="button" class="btn btn-danger" onclick="Giras.cerrar_sin_seleccionar()"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-persona">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-persona')">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-persona"></h4>
            </div>
            <div class="modal-body" id="modal-body-persona"></div>
            <div class="modal-footer" id="modal-footer-persona">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" id="btn_create_edit" name="btn_create_edit" onclick="persona.create_edit();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div> 

<div class="modal fade" id="modal-beneficiario">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Registro de beneficiario</h4>
            </div>
            <div class="modal-body" id="modal-body-beneficiario"></div>
            <div class="modal-footer" id="modal-footer-beneficiario">
                {{-- <button type="button" class="btn btn-warning pull-left" data-dismiss="modal" id="btn_editar_persona"><i class="fa fa-pencil"></i> Editar</button> --}}
                <button type="button" class="btn btn-danger" onclick="Beneficiarios.cancelar()"><i class="fa fa-ban"></i> Cancelar</button>
                <button type="button" class="btn btn-success" onclick="Beneficiarios.guardar_beneficiario()"><i class="fa fa-database"></i> Aceptar</button>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dependencia">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-dependencia')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-dependencia">Registrar dependencia o institución</h4>
            </div>
            <div class="modal-body" id="modal-body-dependencia"></div>
            <div class="modal-footer" id="modal-footer-dependencia">
                <button type="button" class="btn btn-success" onclick="dependencia.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-remove"></i> Cerrar</button>
            </div>      
        </div>
    </div>
</div>