@extends('atnciudadana::layouts.master')

@push('head')
<style type="text/css">
  .timeline,
  .timeline-horizontal {
    list-style: none;
    padding: 20px;
    position: relative;
  }
  .timeline:before {
    top: 40px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 3px;
    background-color: #eeeeee;
    left: 50%;
    margin-left: -1.5px;
  }
  .timeline .timeline-item {
    margin-bottom: 20px;
    position: relative;
  }
  .timeline .timeline-item:before,
  .timeline .timeline-item:after {
    content: "";
    display: table;
  }
  .timeline .timeline-item:after {
    clear: both;
  }
  .timeline .timeline-item .timeline-badge {
    color: #fff;
    width: 54px;
    height: 54px;
    line-height: 52px;
    font-size: 22px;
    text-align: center;
    position: absolute;
    top: 18px;
    left: 50%;
    margin-left: -25px;
    background-color: #7c7c7c;
    border: 3px solid #ffffff;
    z-index: 100;
    border-top-right-radius: 50%;
    border-top-left-radius: 50%;
    border-bottom-right-radius: 50%;
    border-bottom-left-radius: 50%;
  }
  .timeline .timeline-item .timeline-badge i,
  .timeline .timeline-item .timeline-badge .fa,
  .timeline .timeline-item .timeline-badge .glyphicon {
    top: 2px;
    left: 0px;
  }

  .timeline .timeline-item .timeline-badge.pre {
    background-color: #EF426F;
  }

  .timeline .timeline-item .timeline-badge.solicitante {
    background-color: #289F97;
  }

  .timeline .timeline-item .timeline-badge.vinculado {
    background-color: #57AA65;
  }

  .timeline .timeline-item .timeline-badge.vo-bo {
    background-color: #B9BF15;
  }

  .timeline .timeline-item .timeline-badge.validando {
    background-color: #F5CB08;
  }

  .timeline .timeline-item .timeline-badge.espera {
    background-color: #1B96B8;
  }

  .timeline .timeline-item .timeline-badge.finalizado {
    background-color: #002b59;
  }

  .timeline .timeline-item .timeline-badge.otro {
    background-color: #CD0007;
  }


  .timeline .timeline-item .timeline-badge.primary {
    background-color: #1f9eba;
  }
  .timeline .timeline-item .timeline-badge.info {
    background-color: #5bc0de;
  }
  .timeline .timeline-item .timeline-badge.success {
    background-color: #59ba1f;
  }
  .timeline .timeline-item .timeline-badge.warning {
    background-color: #d1bd10;
  }
  .timeline .timeline-item .timeline-badge.danger {
    background-color: #ba1f1f;
  }
  .timeline .timeline-item .timeline-panel {
    position: relative;
    width: 46%;
    float: left;
    right: 16px;
    border: 1px solid #c0c0c0;
    background: #ffffff;
    border-radius: 2px;
    padding: 20px;
    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  }
  .timeline .timeline-item .timeline-panel:before {
    position: absolute;
    top: 26px;
    right: -16px;
    display: inline-block;
    border-top: 16px solid transparent;
    border-bottom: 16px solid transparent;
    content: " ";
  }
  .timeline .timeline-item .timeline-panel .timeline-title {
    margin-top: 0;
    color: inherit;
    font-weight: bold;
  }
  .timeline .timeline-item .timeline-panel .timeline-body > p,
  .timeline .timeline-item .timeline-panel .timeline-body > ul {
    margin-bottom: 0;
  }
  .timeline .timeline-item .timeline-panel .timeline-body > p + p {
    margin-top: 5px;
  }
  .timeline .timeline-item:last-child:nth-child(even) {
    float: right;
  }
  .timeline .timeline-item:nth-child(even) .timeline-panel {
    float: right;
    left: 16px;
  }
  .timeline .timeline-item:nth-child(even) .timeline-panel:before {
    border-left-width: 0;
    border-right-width: 14px;
    left: -14px;
    right: auto;
  }
  .timeline-horizontal {
    list-style: none;
    position: relative;
    padding: 20px 0px 20px 0px;
    display: inline-block;
  }
  .timeline-horizontal:before {
    height: 3px;
    top: auto;
    bottom: 26px;
    left: 56px;
    right: 0;
    width: 100%;
    margin-bottom: 20px;
  }
  .timeline-horizontal .timeline-item {
    display: table-cell;
    height: 200px;
    width: 20%;
    min-width: 320px;
    float: none !important;
    padding-left: 0px;
    padding-right: 20px;
    margin: 0 auto;
    vertical-align: bottom;
  }
  .timeline-horizontal .timeline-item .timeline-panel {
    top: auto;
    bottom: 64px;
    display: inline-block;
    float: none !important;
    left: 0 !important;
    right: 0 !important;
    width: 100%;
    margin-bottom: 20px;
    margin-top: 64px;
  }
  .timeline-horizontal .timeline-item .timeline-panel:before {
    top: auto;
    bottom: -16px;
    left: 28px !important;
    right: auto;
    border-right: 16px solid transparent !important;
    border-left: 16px solid transparent !important;
  }
  .timeline-horizontal .timeline-item:before,
  .timeline-horizontal .timeline-item:after {
    display: none;
  }
  .timeline-horizontal .timeline-item .timeline-badge {
    top: auto;
    bottom: 0px;
    left: 43px;
  }

  .timeline:before {
    background-color: #bdc3c7 !important;
}

  .success ~ .timeline-panel {
    border: 1px solid #59ba1f !important;
  }

  .success ~ .timeline-panel:before {
    border-top: 16px solid #59ba1f !important;
    border-bottom: 0 solid #59ba1f !important;
  }

  .success ~ .timeline-panel .timeline-title {
      color: #59ba1f !important;
  }

  .primary ~ .timeline-panel {
    border: 1px solid #1f9eba !important;
  }

  .primary ~ .timeline-panel:before {
    border-top: 16px solid #1f9eba !important;
    border-bottom: 0 solid #1f9eba !important;
  }

  .primary ~ .timeline-panel .timeline-title {
      color: #1f9eba !important;
  }

  .info ~ .timeline-panel {
    border: 1px solid #5bc0de !important;
  }

  .info ~ .timeline-panel:before {
    border-top: 16px solid #5bc0de !important;
    border-bottom: 0 solid #5bc0de !important;
  }

  .info ~ .timeline-panel .timeline-title {
      color: #5bc0de !important;
  }

  .warning ~ .timeline-panel {
    border: 1px solid #d1bd10 !important;
  }

  .warning ~ .timeline-panel:before {
    border-top: 16px solid #d1bd10 !important;
    border-bottom: 0 solid #d1bd10 !important;
  }

  .warning ~ .timeline-panel .timeline-title {
      color: #d1bd10 !important;
  }

  .danger ~ .timeline-panel {
    border: 1px solid #ba1f1f !important;
  }

  .danger ~ .timeline-panel:before {
    border-top: 16px solid #ba1f1f !important;
    border-bottom: 0 solid #ba1f1f !important;
  }

  .danger ~ .timeline-panel .timeline-title {
      color: #ba1f1f !important;
  }

  {{--  .timeline-horizontal .timeline-item {
    max-width: 130px !important;
    min-width: 100px !important;
  }  --}}

  .pre ~ .timeline-panel {
    border: 1px solid #EF426F !important;
  }

  .pre ~ .timeline-panel:before {
    border-top: 16px solid #EF426F !important;
    border-bottom: 0 solid #EF426F !important;
  }

  .pre ~ .timeline-panel .timeline-title {
    color: #EF426F !important;
  }

  .solicitante ~ .timeline-panel {
    border: 1px solid #289F97 !important;
  }

  .solicitante ~ .timeline-panel:before {
    border-top: 16px solid #289F97 !important;
    border-bottom: 0 solid #289F97 !important;
  }

  .solicitante ~ .timeline-panel .timeline-title {
    color: #289F97 !important;
  }

  .vinculado ~ .timeline-panel {
    border: 1px solid #57AA65 !important;
  }

  .vinculado ~ .timeline-panel:before {
    border-top: 16px solid #57AA65 !important;
    border-bottom: 0 solid #57AA65 !important;
  }

  .vinculado ~ .timeline-panel .timeline-title {
    color: #57AA65 !important;
  }

  .vo-bo ~ .timeline-panel {
    border: 1px solid #B9BF15 !important;
  }

  .vo-bo ~ .timeline-panel:before {
    border-top: 16px solid #B9BF15 !important;
    border-bottom: 0 solid #B9BF15 !important;
  }

  .vo-bo ~ .timeline-panel .timeline-title {
    color: #B9BF15 !important;
  }

  .validando ~ .timeline-panel {
    border: 1px solid #F5CB08 !important;
  }

  .validando ~ .timeline-panel:before {
    border-top: 16px solid #F5CB08 !important;
    border-bottom: 0 solid #F5CB08 !important;
  }

  .validando ~ .timeline-panel .timeline-title {
    color: #F5CB08 !important;
  }

  .espera ~ .timeline-panel {
    border: 1px solid #1B96B8 !important;
  }

  .espera ~ .timeline-panel:before {
    border-top: 16px solid #1B96B8 !important;
    border-bottom: 0 solid #1B96B8 !important;
  }

  .espera ~ .timeline-panel .timeline-title {
    color: #1B96B8 !important;
  }

  .finalizado ~ .timeline-panel {
    border: 1px solid #002b59 !important;
  }

  .finalizado ~ .timeline-panel:before {
    border-top: 16px solid #002b59 !important;
    border-bottom: 0 solid #002b59 !important;
  }

  .finalizado ~ .timeline-panel .timeline-title {
    color: #002b59 !important;
  }

  .otro ~ .timeline-panel {
    border: 1px solid #CD0007 !important;
  }

  .otro ~ .timeline-panel:before {
    border-top: 16px solid #CD0007 !important;
    border-bottom: 0 solid #CD0007 !important;
  }

  .otro ~ .timeline-panel .timeline-title {
      color: #CD0007 !important;
  }

  .page-header {
    padding-bottom: 0px !important;
    border-bottom-color: #bdc3c7;
  }

  .box-header h4 a {
      color: #d12654;
      font-weight: bold;
  }
</style>
@endpush

@section('content-subtitle', 'Ver Solicitud')

@section('li-breadcrumbs')
    <li class="active"><a href="{{ route('solicitudes.index') }}">Solicitudes</a></li>
    <li class="active">Ver Solicitud</li>
@endsection

@section('content')
<div class="box box-primary shadow">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:30%">
                            <i class="fa fa-id-card margin-r-5"></i>
                            Folio:
                        </th>
                        <td>{{ $generales[0]->folio }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-user margin-r-5"></i>
                            Remitente
                        </th>
                        <td>{{ $generales[0]->remitente }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-flag margin-r-5"></i>
                            Tipo:
                        </th>
                        <td>{{ $generales[0]->tipo }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-signal margin-r-5"></i>
                            Estatus general:
                        </th>
                        @if($generales[0]->status_g == "Finalizado")
                        <td><span class="label label-primary">{{ $generales[0]->status_g }}</span></td>
                        @else
                        <td><span class="label label-warning">{{ $generales[0]->status_g }}</span></td>
                        @endif
                    </tr>

                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:40%">
                            <i class="fa fa-calendar margin-r-5"></i>
                            Fecha de recepción:
                        </th>
                        <td>{{ \Carbon\Carbon::parse($generales[0]->fecharecepcion)->format('d/m/Y') }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-file-text-o margin-r-5"></i>
                            Asunto:
                        </th>
                        <td>{{ $generales[0]->asunto }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-building margin-r-5"></i>
                            Recibido en:
                        </th>
                        <td>{{ $generales[0]->recepcion }}</td>
                    </tr>
                    @if($generales[0]->status_g == "Finalizado")
                    <tr>
                        <th>
                            <i class="fa fa-calendar margin-r-5"></i>
                            Fecha finalizado:
                        </th>
                        <td>{{ $generales[0]->fecha_fin }}</td>
                    </tr>
                    @else
                    <tr>
                        <th>
                            <i class="fa fa-calendar margin-r-5"></i>
                            Dias Transcurridos:
                        </th>
                        <td>{{ $generales[0]->dias }}</td>
                    </tr>
					@endif
                </table>
            </div>
        </div>
    </div>
</div>

@foreach ($timeline as $time)
<div class="box box-primary shadow">
    <div class="box-header with-border">
        <div class="row" style="display:flex;">
            <div class="col-sm-6" style="padding: 0px !important;">
                <h4 class="box-title"><a {{-- href="../peticiones/{{$time['lineas'][0]['peticion_id']}}" --}}>{{ $time['lineas'][0]['beneficio'] }}</a></h4>
                <h6 style="font-weight: bold;">{{ $time['nombre'] }}</h6>
                <h6>{{ $time['area_responsable'] }}</h6>
            </div>
            <div class="col-sm-6" style="display:flex !important; align-items:center !important; justify-content:flex-end; min-height:100%; padding: 0px !important;">
                    <h4 class="pull-right"><a >{{ $time['folio'] }}</a></h4>
            </div>
        </div>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="display:inline-block;width:100%;overflow-y:auto;">
        <ul class="timeline timeline-horizontal" style="margin-top: 10px;">
        @foreach ($time['lineas'] as $line)
            @php
                $color = 'otro';
                $icon = 'check';
                switch ($line['status']) {
                  case 'PRE-SOLICITUD':
                    $color = 'pre';
                    $icon = 'file';
                    break;
                  case 'SOLICITANTE':
                    $color = 'solicitante';
                    $icon = 'user';
                    break;
                  case 'VINCULADO':
                    $color = 'vinculado';
                    $icon = 'link';
                    break;
                  case 'Vo. Bo.':
                    $color = 'vo-bo';
                    break;
                  case 'VALIDANDO':
                    $color = 'validando';
                    $icon = 'eye-open';
                    break;
                  case 'LISTA DE ESPERA':
                    $color = 'espera';
                    $icon = 'list-alt';
                    break;
                  case 'FINALIZADO':
                    $color = 'finalizado';
                    $icon = 'ok';
                    break;

                  default:
                    $color = 'otro';
                    $icon = 'remove';
                    break;
                }
            @endphp
            <li class="timeline-item">
              <div class="timeline-badge {{ $color }}"><i class="glyphicon glyphicon-{{$icon}}"></i></div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4 class="timeline-title">{{ $line['status'] }}</h4>
                    <div class="timeline-body">
                      <p>{{ $line['observacion'] }}</p>
                    </div>
                  <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{ $line['fecha'] }}
                    <br>{{ $line['usuario'] }}
                    </small>
                  </p>
                </div>
              </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endforeach
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
  <script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('js/d3.v2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/timeknots-min.js') }}"></script>
<script type="text/javascript">

$(document).ready(() => {

    var table = $('#solicitantesDT').dataTable();
    var datatable_beneficiarios;
      datatable_beneficiarios = $(table).DataTable();

        $('#searchBeneficiario').keypress(function(e) {
			if(e.which === 13) {
				datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
			}
		});

		$('#btn_buscarBeneficiario').on('click', function() {
			datatable_beneficiarios.search($('#searchBeneficiario').val()).draw();
		});
});

</script>
@endpush
