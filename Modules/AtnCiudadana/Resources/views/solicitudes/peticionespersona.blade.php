@extends("atnciudadana::layouts.master")

@push('head')

@endpush

@section('styles')
@include('atnciudadana::layouts.links')
  <style type="text/css">
  
    .card-color1{
      background-color: #57AA65 !important;
      color: #ffffff;
    }

    .card-color2{
      background-color: #F5CB08 !important;
      color: #ffffff;
    }

    .card-color3{
      background-color: #20AFD6 !important;
      color: #ffffff;
    }

    .card-color4{
      background-color: #FF585D !important;
      color: #ffffff;
    }

    .table-bordered {
      border: 1px solid #d12653 !important;
    }

		.box.box-solid.box-primary>.box-header {
			color: #fff;
			background: #d12654 !important;
			background-color: #d12654 !important;
		}

		.box.box-solid.box-primary {
    	border: 1px solid #d12654 !important;
		}

  </style>
@stop

@section('content-title', auth()->user()->persona->empleado->area->nombre)
@section('content-subtitle', 'Peticiones')

@section('li-breadcrumbs')
    <li class="active">Peticiones</li>
@endsection

@section('content')

    <section class="content">
        <div id="app">
                
        <div class="row">
            <div class="col-lg-3 col-xs-3">
                <div class="small-box card-color1" @click="filterStatus('SOLICITANTE')">
                    <div class="inner">
                        <h3 id="peticiones_nuevas">
                          <i  v-if="series.length<=0" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                          @{{nuevos}}
                        </h3>
                        <p>Peticiones Nuevas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-edit"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box card-color2" @click="filterStatus('VINCULADO')" >
                    <div class="inner">
                        <h3 id="peticiones_turnadas">
                          <i v-if="series.length<=0" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                          @{{turnados}}</h3>
                        <p>Peticiones Turnadas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-caret-square-o-right"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box card-color3" @click="filterStatus('Vo. Bo.')">
                    <div class="inner">
                        <h3 id="peticiones_proceso"><i  v-if="series.length<=0" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                          @{{procesados}}
                        </h3>
                        <p>Peticiones en Proceso</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-check-square-o"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-3">
                <div class="small-box card-color4" @click="filterStatus('FINALIZADO')">
                    <div class="inner">
                        <h3 id="peticiones_concluidas"><i  v-if="series.length<=0" class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                          @{{concluidos}}
                        </h3>
                        <p>Peticiones Concluídas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-minus-square"></i>
                    </div>
                </div>
            </div>
        </div>

				{{-- <div class="box box-primary box-solid" style="position: relative; left: 0px; top: 0px;">
						
					<div class="box-header">

              <div class="pull-right box-tools">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
									</button>
              </div>


              <i class="fa fa-bar-chart"></i>

              <h3 class="box-title">
                Peticiones por Dirección
							</h3>
							
						</div>
						
            <div class="box-body">
								<v-chart id="chart1" type=bar height=500 :options="chartOptions" :series="series"></v-chart>
						</div>
						
					</div> --}}
					
        
				
				<div class="row">
          <div class="col-xs-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
									<i class="fa fa-table"></i>

									<h3 class="box-title">
									  Lista de peticiones
									</h3>
								{{-- <div class="col-md-2" @click="filterStatus('')">
										<div class="icon">
											<i class="fa fa-minus-square"></i>	
											<h3 class="box-title text-right">Borrar Filtros</h3>
                    </div>
								</div> --}}
								<div class="pull-right box-tools">
										<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
										</button>
										
								</div>
              </div>
              <div class="box-body">
                {{-- <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                  <input type="text" id="buscar" class="form-control">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-info btn-flat">Buscar</button>
                  </span>
                </div> --}}
              <div>
                  <v-client-table ref="tablapeticiones" :data="peticiones" :columns="columns" :options="options">
										<div slot="beforeFilter" >
											<button class="btn btn-success" type="button"  @click="GenerarExcel()">
												<i class="fa fa-file-excel-o"></i>
												Exportar a Excel
											</button>
											<button class="btn btn-danger" type="button"  @click="filterClear('')">
												<i class="fa fa-times"></i>
												Borrar Filtros
											</button>
										</div>
									</v-client-table>
              </div>
              </div>
            </div>
          </div>
				</div>
				
    </div>
    </section>    
@stop

@push('body')

<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/vue.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/apexcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/vue-apexcharts.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/vue-tables-2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/axios/axios.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('plugins/vue/vue-multiselect.min.js')}}"></script>
<link rel="stylesheet" href="/plugins/vue/vue-multiselect.min.css">

<script type="text/javascript">

		Vue.component('vue-multiselect', window.VueMultiselect.default);
    Vue.component('v-chart', VueApexCharts);
    Vue.use(VueTables.ClientTable);

    var app = new Vue({
    el: '#app',
      data: {
        series: [],
        peticiones: [],
        areas:[],
        labels:[],
        columns: ['folio', 'fecha_turnado', 'solicitante','tipo','cve_municipio','municipio','cve_distrito','distrito','cve_region','region','cve_localidad','localidad','producto','cantidad','beneficiario','curp','estatus','direccion'],
        peticiones:[],
        options: {
          texts: {
            count:
              " {from} a {to} de {count} registros|{count} Registros|1 Registro",
            first: "First",
            last: "Last",
            filter: "Filtrar:",
            filterPlaceholder: "Buscar",
            limit: "Registros:",
            page: "Pagina:",
            noResults: "No se encontraron coincidencias",
            filterBy: "Filtrar",
            loading: "Cargando...",
            defaultOption: "Seleccionar {column}",
            columns: "Columnas"
          },
          highlightMatches: true,
          filterByColumn: true
        }
      },
      methods: {
        loadData(){
            axios.get('/atnciudadana/peticionesAgrupadas/').then(response => {
                let areasd = Object.keys(response.data);
                this.areas = areasd.map(function(element) {
                  return element.replace("DIRECCIÓN DE ","","gi");
                  //console.log(element);
                });
                this.labels = areasd;

                //console.log(this.areas);
                let nuevos=[];
                let turnados=[];
                let proceso=[];
                let concluidos=[];
                for (const key in response.data) {
                  let countN=0;
                  let countT=0;
                  let countP=0;
                  let countC=0;
                  response.data[key].forEach(element => {
                    if(element.estatus=="SOLICITANTE" || element.estatus=="PRE-SOLICITUD"){
                        countN ++;
                    }
                    
                    if(element.estatus=="VINCULADO"){
                        countT ++;
                    }
                    
                    if(element.estatus=="FINALIZADO" || element.estatus=="RECHAZADO" || element.estatus=="CANCELADO" ){
                        countC ++;
                    }
                    
                    if(element.estatus=="Vo. Bo." || element.estatus=="VALIDANDO" || element.estatus=="LISTA DE ESPERA"){
                        countP ++;
                    }
                  });

                  nuevos.push(countN);
                  turnados.push(countT);
                  proceso.push(countP);
                  concluidos.push(countC);

                }

                this.series = [
                  {
                    name: 'Nuevos',
                    data: nuevos
                  },
                  {
                    name: 'Turnados',
                    data: turnados
                  },
                  {
                    name: 'En Proceso',
                    data: proceso
                  },
                  {
                    name: 'Concluídos',
                    data: concluidos
                  } ];
                
                //console.log(this.series[0].data.reduce((total, num)=>total + num));
            }).catch(error => {
                console.log('No se pudieron cargar las peticiones.');
            });

            axios.get('/atnciudadana/peticionesDireccion/').then(response => {
                //console.log(response);
                this.peticiones = response.data.peticiones;
            }).catch(error => {
                console.log('No se pudieron cargar las peticiones.');
            });
            },

        filterStatus(filter){
					// VueTables.Event.$emit('vue-tables.filter::estatus', filter);
					this.$refs.tablapeticiones.setFilter({
						'estatus': filter
					});
				},
				filterClear(filter){
					// VueTables.Event.$emit('vue-tables.filter::estatus', filter);
					this.$refs.tablapeticiones.setFilter({
						'estatus': filter,
						'folio': filter, 
						'fecha_turnado': filter, 
						'solicitante': filter,
						'tipo': filter,
						'municipio': filter,
						'producto': filter,
						'cantidad': filter,
						'beneficiario': filter,
						'curp': filter,
						'estatus': filter,
						'direccion': filter
					});
				},
				GenerarExcel() {
					block();
					var data = {};
					data.titulo ="Reporte de Peticiones",
    			data.subtitulo= "Lista de peticiones",
					data.cabeceras=Object.keys(this.$refs.tablapeticiones.allFilteredData[0]).map(e=>e.replace("_"," ")).map(e=>e.toUpperCase());
					data.datos=this.$refs.tablapeticiones.allFilteredData.map(e=>Object.values(e));
					jsreport.serverUrl = 'http://187.157.97.110:3001';
					var request = {
							template:{
									shortid: "HJ5VttAyS"
							},
							data: data
					};
					jsreport.renderAsync(request).then(function(res) {
									res.download(`REPORTE.xlsx`)
					unblock();
					 });
		    },
      },
      mounted(){
        this.loadData();
      },
      computed:{
        chartOptions() {
          return {
            colors: ['#57AA65', '#F5CB08', '#20AFD6','#FF585D'],
            chart: {
              type: 'bar',
              stacked: true,
              stackType: '100%'
            },
            plotOptions: {
              bar: {
                horizontal: true,
              },

            },
            stroke: {
              width: 1,
              colors: ['#fff']
            },

            title: {
              //text: 'PETICIONES POR DIRECCIÓN',
              horizontalAlign: 'left',
              offsetX: 165
            },
            xaxis: {
              type: 'category',
              categories: this.areas,
              labels: {
                trim: false
              }
              
            },

            tooltip: {
              y: {
                formatter: function (val) {
                  return val + " Peticiones"
                }
              }
            },
            fill: {
              opacity: 1

            },

            legend: {
              position: 'top',
              horizontalAlign: 'left',
              offsetX: 140
            }
          }
        },
        
        nuevos(){
        return this.series.length>0 ? this.series[0].data.reduce((total, num)=>total + num) : 0;
        },
        turnados(){
        return this.series.length>0 ? this.series[1].data.reduce((total, num)=>total + num) : 0;
        },
        procesados(){
        return this.series.length>0 ? this.series[2].data.reduce((total, num)=>total + num) : 0;
        },
        concluidos(){
        return this.series.length>0 ? this.series[3].data.reduce((total, num)=>total + num) : 0;
        }



      }
    })
</script>
@endpush