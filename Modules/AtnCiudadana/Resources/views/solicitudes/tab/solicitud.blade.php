<!--div class="box-body"-->
    <form  role="form" id="form_solicitud" onsubmit="return false;">
        <div id="div-dirigido" class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                <div class="form-group">
                    <label for="titular_id">Dirigido a* :</label>
                    <select id="titular_id" name="titular_id" class="form-control select2">
                        @if(isset($solicitud))
							<option value="{{ $solicitud->persona_id }}" selected>{{ $solicitud->persona->nombreCompleto() }}</option>
						@endif
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
                <div class="form-group">
                    <label></label>
                    <div style="padding-top: 10px;">
                        <label style="margin-right: 10px;">¿Es un compromiso?</label>
                        @if(isset($solicitud) && $solicitud->compromiso)
                            <input type="checkbox" name="compromiso" id="compromiso" class="flat-red" checked>
                        @else
                            <input type="checkbox" name="compromiso" id="compromiso" class="flat-red">
                        @endif                    
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="tiporecepcion_id">Recibido en* :</label>
                    <select id="tiporecepcion_id" name="tiporecepcion_id" class="form-control select2">
                        @if(isset($solicitud))
							<option value="{{ $solicitud->tiposrecepcion_id }}" selected>{{ $solicitud->tiposrecepcion->tipo }}</option>
						@endif
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="tiporemitente_id">Tipo de remitente* :</label>
                    <select id="tiporemitente_id" name="tiporemitente_id" class="form-control select2">
                        {{-- @if(isset($solicitud))
							<option value="{{ $solicitud->tiposremitente_id }}" selected>{{ $solicitud->tiposremitente->tipo }}</option>
						@endif --}}
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Nº de oficio* :</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-file-text-o"></i>
                        </div>
                        <input type="text" id="numoficio" name="numoficio" class="form-control" placeholder="Nº de Oficio" value="{{ $solicitud->numoficio or '' }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Fecha del oficio* :</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="fechaoficio" name="fechaoficio" class="form-control pull-right" value="{{ isset($solicitud)? $solicitud->formato_fecha_oficio() : '' }}">
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Fecha de recepción* :</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="fecharecepcion" name="fecharecepcion" class="form-control pull-right" value="{{ isset($solicitud)? $solicitud->formato_fecha_recepcion() : '' }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label>Asunto* :</label>
                    <textarea id="asunto" name="asunto" placeholder="Asunto de la Solicitud" class="form-control" rows="5" value="{{ $solicitud->asunto or '' }}">{{ $solicitud->asunto or '' }}</textarea>
                </div>
            </div>
        </div>
    </form>
<!--/div-->