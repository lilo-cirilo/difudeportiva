
    <form role="form" id="form_peticion" onsubmit="return false;">
        <div class="row">
            
            @if($tipo_solicitud == 'E')
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label>Beneficio:</label>                                        
                        <div class="input-group">
                            <select id="programa_id" class="form-control input-persona" name="programa_id" readonly="readonly"></select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="BeneficiosExt.abrir_modal('programa_id');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @else 
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="programa_id">Beneficio:</label>
                        <select id="programa_id" name="programa_id" placeholder="programa_id" class="form-control select2" style="width: 100%;"></select>
                    </div>
                </div>
            @endif
			
			@if($tipo_solicitud == 'E')
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label>Dependencia:</label>                                        
                        <div class="input-group">
							<select id="dependencia_externa" class="form-control input-persona" name="dependencia_externa" readonly="readonly"></select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" style="padding-left: 25px; padding-right: 25px;" onclick="Dependencias.abrir_modal('dependencia_externa');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
			@endif
            
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="cantidad">Cantidad:</label>
                    <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="" style="color:white">Agregar:</label>
                    <button type="button" class="btn btn-block btn-success" id="btnAgregar">Agregar</button>
                </div>
            </div>
		</div>
		
		@if($tipo_solicitud == 'I')
			<div class="row">            
				<div class="row invoice-info">
					<div class="col-sm-6 invoice-col">
					<strong>Área responsable:</strong>
						<address id="areaR">
							---------------------
						</address>
					</div>
					<div class="col-sm-6 invoice-col">
						<strong>Titular del área:</strong>
						<address id="areaT">
							---------------------
						</address>
					</div>
				</div>            
			</div>
		@endif
    </form>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Lista de beneficios</h4>
                </div>
                <div class="box-body">
                    {!! $beneficios->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiosDT', 'name' => 'beneficiosDT', 'style' => 'width: 100%']) !!}                    
                </div>
            </div>
        </div>
    </div>
