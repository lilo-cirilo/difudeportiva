<div class="box-body">
    <form role="form" id="form_producto" onsubmit="return false;">
        <div class="row">
            @if($tipo_solicitud === 'I')
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div class="form-group">
                    <label for="beneficios">Beneficio:</label>
                    <select id="beneficios" name="beneficios" placeholder="Beneficios" class="form-control select2" style="width: 100%;"></select>
                </div>
            </div>
            @endif
            @if($tipo_solicitud === 'E')
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label>Dependencia</label>                                        
                        <div class="input-group">
                            <select id="dependencias" name="dependencias" placeholder="Seleccione dependencia" class="form-control select2" style="width: 100%;"></select>
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" data-toggle="tooltip" title="Nueva dependencia" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-dependencia');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="tag">Tag:</label>
                        <input id="tag" name="tag" class="form-control"/>
                    </div>
                </div>
            @endif
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="cantidad">Cantidad:</label>
                    <input type="text" class="form-control" id="cantidad" name="cantidad" placeholder="Cantidad">
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="" style="color:white">Agregar:</label>
                    <button type="button" class="btn btn-block btn-success" id="btnAgregar">Agregar</button>
                </div>
            </div>
        </div>
        <div class="row">
            @if($tipo_solicitud === 'E')
            
            @else
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                <strong>Área responsable:</strong>
                    <address id="areaR">
                        ---------------------
                    </address>
                </div>
                <div class="col-sm-4 invoice-col">
                    <strong>Titular del área:</strong>
                    <address id="areaT">
                        ---------------------
                    </address>
                </div>
                <div class="col-sm-4 invoice-col">
                    <strong>Programa:</strong>
                    <address id="prog">
                        ---------------------
                    </address>
                </div>
            </div>
            @endif
        </div>
    </form>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h4 class="box-title">Lista de beneficios</h4>
                </div>
                <div class="box-body">
                    {!! $beneficios->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiosDT', 'name' => 'beneficiosDT', 'style' => 'width: 100%']) !!}                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box-footer">
    <button id="btnPrevious_peticion" type="button" class="btn btn-primary pull-left" data-toggle="tooltip" data-placement="right" title="Regresar">
        <i class="fa fa-arrow-circle-left"></i>
    </button>
    <div class="pull-right">
        <button type="button" class="btn btn-warning" id="btnFinalizar"><i class="fa fa-reply"></i> Ir a las solicitudes</button>
    </div>
</div>