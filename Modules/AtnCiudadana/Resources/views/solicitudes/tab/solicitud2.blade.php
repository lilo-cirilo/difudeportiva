<form  role="form" id="form_solicitud" onsubmit="return false;" autocomplete="off">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="form-group">
                <label for="persona_id">Dirigido a* :</label>
                <select id="persona_id" name="persona_id" class="form-control select2">
                    <option value="" disabled selected>SELECCIONE A QUIEN VA DIRIGIDO</option>
                    @foreach($titulares as $titular)                        
                        @if(isset($solicitud))
                            <option value="{{ $solicitud->persona_id }}" selected>{{ $solicitud->persona->nombreCompleto() }}</option>
                        @else
                            <option value="{{ $titular->id }}">{{ $titular->nombre }}</option>
						@endif
                    @endforeach
                    <option value="0">OTRO</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="form-group">
                <label></label>
                <div style="padding-top: 10px;">
                    <label style="margin-right: 10px;">¿Es un compromiso?</label>
                    @if(isset($solicitud) && $solicitud->compromiso)
                        <input type="checkbox" name="compromiso" id="compromiso" class="flat-red" checked>
                    @else
                        <input type="checkbox" name="compromiso" id="compromiso" class="flat-red">
                    @endif                    
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="tiposrecepcion_id">Recibido en* :</label>
                <select id="tiposrecepcion_id" name="tiposrecepcion_id" class="form-control select2">
                    <option value="" disabled selected>SELECCIONE EL LUGAR DE RECEPCIÓN</option>
                    @foreach($recepciones as $recepcion)                        
                        <option value="{{ $recepcion->id }}" {{ (isset($solicitud) && $solicitud->tiposrecepcion_id == $recepcion->id) ? 'selected' : '' }} >{{ $recepcion->tipo }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="row {{ isset($solicitud) && $solicitud->solicitudevento ? '' : 'hide' }}" id="div_evento">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="evento_id">Nombre de la gira o del evento:</label>
                <div class="input-group">
                    <select id="evento_id" class="form-control input-persona" name="evento_id" readonly="readonly">
                        @if(isset($solicitud))
                            @if($solicitud->solicitudevento)
                                <option value="{{ $solicitud->solicitudevento->evento_id }}">{{$solicitud->solicitudevento->evento->gira->nombre . ' (' . $solicitud->solicitudevento->evento->municipio->nombre . ')' }}</option>
                            @endif
                        @endif
                    </select>
                    <div class="input-group-btn">
                        <button class="btn btn-info" type="button" onclick="Giras.abrir_modal()" style="padding-left: 25px; padding-right: 25px; border-radius: 0px;">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="tiposremitente_id">Tipo de remitente* :</label>
                <select id="tiposremitente_id" name="tiposremitente_id" class="form-control select2">
                    <option value="" disabled selected>SELECCIONE UN TIPO</option>
                    @foreach($remitentes as $remitente)
                        <option value="{{ $remitente->id }}" {{ (isset($solicitud) && $solicitud->tiposremitente_id == $remitente->id) ? 'selected' : '' }}>{{ $remitente->tipo }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label>Nº de oficio* :</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-file-text-o"></i>
                    </div>
                    <input type="text" id="numoficio" name="numoficio" class="form-control" placeholder="Nº de Oficio" value="{{ $solicitud->numoficio or '' }}">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label>Fecha del oficio* :</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" id="fechaoficio" name="fechaoficio" class="form-control pull-right" value="{{ isset($solicitud)? $solicitud->formato_fecha_oficio() : '' }}">
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label>Fecha de recepción* :</label>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" id="fecharecepcion" name="fecharecepcion" class="form-control pull-right" value="{{ isset($solicitud)? $solicitud->formato_fecha_recepcion() : '' }}">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Asunto* :</label>
                <textarea id="asunto" name="asunto" placeholder="ASUNTO DE LA SOLICITUD" class="form-control" rows="5" value="{{ $solicitud->asunto or '' }}">{{ $solicitud->asunto or '' }}</textarea>
            </div>
        </div>
    </div>    

    <h3>Datos del remitente</h3>        
    <div class="row" id="sin_remitente">
        <div class="box-body">
            @if(!isset($solicitud))
                <span class="text-red"><i class="fa fa-exclamation-triangle "></i> Seleccione un tipo de remitente </span>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 remitente {{ (isset($solicitud) && !$solicitud->solicituddependencias) ? '' : 'hide' }}" id="divRemitente">
            <div class="form-group">
                <label for="solicitante_id">Remitente:</label>
                <div class="input-group">
                    <select id="solicitante_id" class="form-control input-persona" name="solicitante_id" readonly="readonly">
                        @if(isset($solicitud))
                            @if($solicitud->solicitudmunicipales)
                                <option value="{{ $solicitud->solicitudmunicipales->persona_id }}">{{$solicitud->solicitudmunicipales->persona->nombreCompleto()}}</option>
                            @elseif($solicitud->solicitudregionales)
                                <option value="{{ $solicitud->solicitudregionales->persona_id }}">{{$solicitud->solicitudregionales->persona->nombreCompleto()}}</option>
                            @elseif($solicitud->solicitudpersonales)
                                <option value="{{ $solicitud->solicitudpersonales->persona_id }}">{{$solicitud->solicitudpersonales->persona->nombreCompleto()}}</option>
                            @endif
                        @endif
                    </select>
                    <div class="input-group-btn">
                        <button class="btn btn-info" type="button" onclick="Personas.abrir_modal('solicitante_id')" style="padding-left: 25px; padding-right: 25px; border-radius: 0px;">
                            <i class="fa fa-search"></i>
                        </button>
                        <button class="btn btn-primary" type="button" onclick="Personas.mostrar()" style="padding-left: 25px; padding-right: 25px; border-radius: 0px;">
                            <i class="fa fa-address-card"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 remitente {{ isset($solicitud) && ($solicitud->solicitudregionales || $solicitud->solicitudmunicipales) ? '' : 'hide' }}" id="divCargo">
            <div class="form-group">
                <label for="cargo">Cargo:</label>
                <select id="cargo" class="form-control select2" name="cargo">
                    @if(isset($solicitud))
                        @if($solicitud->solicitudmunicipales)
                            <option value="{{ $solicitud->solicitudmunicipales->cargo->nombre }}">{{$solicitud->solicitudmunicipales->cargo->nombre}}</option>
                        @elseif($solicitud->solicitudregionales)
                            <option value="{{ $solicitud->solicitudregionales->cargo->nombre }}">{{$solicitud->solicitudregionales->cargo->nombre}}</option>
                        @endif                       
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 remitente {{ isset($solicitud) && $solicitud->solicitudregionales ? '' : 'hide' }}" id="divRegion">
            <div class="form-group">
                <label for="region_id">Región:</label>
                <select id="region_id" class="form-control select2" name="region_id">
                    @if(isset($solicitud) && $solicitud->solicitudregionales)
                        <option value="{{ $solicitud->solicitudregionales->region_id }}">{{$solicitud->solicitudregionales->region->nombre}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6 remitente {{ isset($solicitud) && $solicitud->solicitudmunicipales ? '' : 'hide' }}" id="divMunicipio">
            <div class="form-group">
                <label for="solicitud_municipio_id">Municipio:</label>
                <select id="solicitud_municipio_id" class="form-control select2" name="solicitud_municipio_id">
                    @if(isset($solicitud) && $solicitud->solicitudmunicipales)
                        <option value="{{ $solicitud->solicitudmunicipales->municipio_id }}">{{$solicitud->solicitudmunicipales->municipio->nombre}}</option>
                    @endif
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 remitente {{ isset($solicitud) && $solicitud->solicituddependencias ? '' : 'hide' }}" id="divDependencia">
            <div class="form-group">
                <label for="dependencia_id">Institución o Dependencia:</label>
                <div class="input-group">
                    <select id="dependencia_id" class="form-control input-persona" name="dependencia_id" readonly="readonly">
                      @if(isset($solicitud) && $solicitud->solicituddependencias)
                        <option value="{{ $solicitud->solicituddependencias->dependencia->id }}">{{$solicitud->solicituddependencias->dependencia->nombre}}</option>
                      @endif
                    </select>
                    <div class="input-group-btn">
                        <button class="btn btn-info" type="button" onclick="Dependencias.abrir_modal('dependencia_id')" style="padding-left: 25px; padding-right: 25px; border-radius: 0px;">
                            <i class="fa fa-search"></i>
                        </button>
                        <!-- <button class="btn btn-primary" type="button" onclick="Dependencias.mostrar()" style="padding-left: 25px; padding-right: 25px; border-radius: 0px;">
                            <i class="fa fa-address-card"></i>
                        </button> -->
                    </div>
                </div>
            </div>
        </div>            
    </div>
</form>