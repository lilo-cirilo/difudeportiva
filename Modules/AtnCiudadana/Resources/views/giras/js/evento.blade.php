<script type="text/javascript">
    var Evento = (() => {       

        var form_evento, form_area;
        var gira_id = undefined;
        var evento_id = undefined;
        var area_id =  undefined;
        var areas = undefined;
        var giras;

        var get_giras = () => {
            app.to_upper_case();            
            giras = $('#giras').DataTable();
            $('#buscar').keypress(function(e) {
			    if(e.which === 13) {
				    giras.search($('#buscar').val()).draw();
			    }
		    });
		    $('#btn_buscar').on('click', function() {
			    giras.search($('#buscar').val()).draw();
            });
        }
        
        var inicializar = () => {
            $('#modal-evento').on('hidden.bs.modal', (e) => {
                evento_id = undefined;
                gira_id = undefined;
                area_id = undefined;
            }); 

            form_evento = $('#form_evento');
            form_area = $('#form_area');
			agregar_validacion();
            app.to_upper_case();

            agregar_fecha();
			agregar_select_create();
            agregar_select_edit();
            $("#tabs").tabs({
                beforeActivate: (event, ui) => {  
                    if(ui.newPanel[0].id === 'areas'){
                        if(form_evento.valid()) {                            
                            guardar(ui.oldTab[0], ui.newTab[0]);
                        }else{
                            return false;
                        }
                    }else{
                        $(ui.newTab[0]).addClass('active');
                        $(ui.oldTab[0]).removeClass('active');
                        $('#modal-footer-evento').show();
                    }
                }
            });
        }

        var agregar_select_create = () => {
            $('#nombre').select2({
				language: 'es',
				minimumInputLength: 2,
                tags: true,
                allowClear : true,
                placeholder : 'Seleccione el evento o la gira',
				ajax: {
					url: '/atnciudadana/agenda/search',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.nombre,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change((event) => {
                $('#nombre').valid();
            });

			$('#municipio_id').select2({
				language: 'es',
				minimumInputLength: 2,
                allowClear : true,
                placeholder : 'Seleccione un municipio',
				ajax: {
					url: '{{ route('municipios.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change((event) => {
				$('#municipio_id').valid();
				$('#localidad_id').empty();
				$('#localidad_id').select2({
					language: 'es',
                    allowClear : true,
                    placeholder : 'Seleccione una localidad',
					ajax: {
						url: '{{ route('localidades.select') }}',
						delay: 500,
						dataType: 'JSON',
						type: 'GET',
						data: (params) => {
							return {
								search: params.term,
								municipio_id: $('#municipio_id').val()
							};
						},
						processResults: (data, params) => {
							params.page = params.page || 1;
							return {
								results: $.map(data, (item) => {
									return {
										id: item.id,
										text: item.nombre,
										slug: item.nombre,
										results: item
									}
								})
							};
						},
						cache: true
					}
				}).change((event) => {
					$('#localidad_id').valid();
                    $('#localidad').valid();
				});
			});

            $('#localidad').on('input',(e) => {
                $('#localidad_id').valid();
            });

			$('#localidad_id').select2({
				language: 'es',
                allowClear : true,
                placeholder : 'Seleccione una localidad',
			});

            $('#area_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('areas.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: (params) => {
						return {
							search: params.term
						};
					},
					processResults: (data, params) => {
						params.page = params.page || 1;
						return {
							results: $.map(data, (item) => {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			});
		};

        function agregar_select_edit() {
			$('#localidad_id').select2({
				language: 'es',
				ajax: {
					url: '{{ route('localidades.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term,
							municipio_id: $('#municipio_id').val()
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				$('#localidad_id').valid();
			});
		};
        
        var agregar_fecha = () => {
			$('#fecha').datepicker({
				autoclose: true,
				language: 'es',
				startDate: '01-01-1900',
				orientation: 'bottom'
			}).change((event) => {
				$('#fecha').valid();
			});
		};

        var agregar_validacion = () => {
            form_evento.validate({
                rules: {
                    nombre: {
                        required: true
                    },
                    fecha: {
                        required: true
                    },
                    municipio_id: {
                        required: true
                    },
                    localidad_id:{
                        required:{ 
							depends: function(element) {
                                return ($('#localidad').val() == '' || $('#localidad').val() == null);
                            }
                        }
                    },
                    localidad:{
                        required:{ 
							depends: function(element) {
                                return ($('#localidad_id').val() == '' || $('#localidad_id').val() == null);
                            }
                        }
                    }
                }
            });

            form_area.validate({
                rules: {
                    area_id: {
                        required: true
                    }
                }
            });
        };
        
        var guardar = (oldTab, newTab) => {
            if(form_evento.valid()) {
                var url = evento_id ? ('/atnciudadana/eventos/' + evento_id) : ('/atnciudadana/eventos');
                var metodo = evento_id ? 'PUT' : 'POST';
                
                app.ajaxHelper( url, metodo, form_evento.serialize()).done( (response) => {
                    if(!response.success) {
                        swal(
                            '¡Error!',
                            response.message,
                            'error'
                        );
                    }else {
                        $(newTab).addClass('active');
                        $(oldTab).removeClass('active');
                        evento_id = response.evento.id;                        
                        getAreas();
                        giras.ajax.reload(null, false);
                        $('#modal-footer-evento').hide();
                        $('#tabs').tabs({ active: 1 });
                    }
                });
            }
		}

        var getAreas = () => {            
            if ($.fn.DataTable.isDataTable( '#tblAreas' ) ) {
                areas.ajax.url( '/atnciudadana/eventos/' + evento_id + '/areas' ).load();
		    }else{
                areas = $("#tblAreas").DataTable({
                    ajax: {
                        url: '/atnciudadana/eventos/' + evento_id + '/areas',
                        dataSrc: ""
                    },
                    language: {
                        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
                    },
                    columns: [
                        { "data": "area" },
                        { "data": "observaciones" },
                        {
                            data: null,
                            render: ( data, type, row ) => {
                                return "<button type='button' onclick='Evento.editar_area(" + data.area_id + ", \"" + data.area + "\", " + JSON.stringify(data.observaciones) + ")' class='btn btn-xs btn-warning'>Editar</button>";
                                
                            }
                        },
                        {
                            data: null,
                            render: ( data, type, row ) => {                                
                                return '<button type="button" onclick="Evento.eliminar_area(' + data.area_id + ', \'' + data.area + '\'' + ')" class="btn btn-xs btn-danger">Eliminar</button>';
                            }
                        }
                    ],
                    columnDefs: [
                        {className: 'text-center', targets: [2,3] }
                    ],
                    dom: 'itp',
                    lengthMenu: [ [5], [5] ],
                    responsive: true,
                    autoWidth: true,
                    processing: true
                });
            }
        }

        var crear_o_editar = (evento, nombre) => {
            evento_id = evento;
            var url = evento ? '{{ route("atnciudadana.eventos.edit", [":evento"]) }}'.replace(':evento', evento_id) : "{{ route('atnciudadana.eventos.create') }}";
            app.ajaxHelper( url, 'GET').done( (response) => {
                $('#modal-body-evento').html(response.html);
                $('#modal-evento-title').text(nombre || 'Registrar evento');
                inicializar();                
                $('#modal-footer-evento').show();
                $('#modal-evento').modal('show');
            });
        }

        var eliminar = (evento, nombre, fecha, municipio, localidad) => {
            swal({
                title: '¿Desea eliminar el evento?',
                html: nombre + '<br>' + fecha + '<br>' + municipio + '<br>' + localidad,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar',
                cancelButtonText: 'No, regresar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {     
                    var url = '/atnciudadana/eventos/' + evento;
                    app.ajaxHelper( url, 'DELETE').done( (response) => {
                        unblock();
                        if(!response.success){
                            swal(
                                '¡Error!',
                                response.message,
                                'error'
                            );
                        }else{
                            giras.ajax.reload(null, false);
                            swal(
                                '¡Correcto!',
                                response.message,
                                'success'
                            );
                        }
                    });
                }
            });           
        }

        var agregar_area = () => {
            if(form_area.valid()) {
                var url = area_id ? ('/atnciudadana/eventos/' + evento_id + '/areas/' + area_id) : ('/atnciudadana/eventos/' + evento_id + '/areas');
                var metodo = area_id ? 'PUT' : 'POST';

                app.ajaxHelper( url, metodo,  form_area.serialize()).done( (response) => {
                    if(!response.success){
                        swal(
                            '¡Error!',
                            response.message,
                            'error'
                        );
                    }else{
                        area_id = undefined;
                        $('#area_id').removeAttr('disabled');
                        $('#btnAgregar').text("Agregar");
                        $('#btnAgregar').css('background-color','##00c0ef');
                        $('#btnAgregar').css('border-color', '##00c0ef');
                        $('#area_id').val(null).trigger('change');
                        $('#observaciones').val('');
                        areas.ajax.reload(null, false);                            
                    }
                });
            }
        }

        var eliminar_area = (area, nombre) => {
            swal({
                title: '¿Desea eliminar el área?',
                text: nombre,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sí, eliminar',
                cancelButtonText: 'No, regresar',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {                    
                    url = '/atnciudadana/eventos/' + evento_id + '/areas/' + area;
                    app.ajaxHelper( url, 'DELETE').done( (response) => {
                        if(!response.success){
                            swal(
                                '¡Error!',
                                response.message,
                                'error'
                            );
                        }else{
                            areas.ajax.reload(null, false);
                            swal(
                                '¡Correcto!',
                                response.message,
                                'success'
                            );
                        }
                    });
                }
            });           
        }

        var editar_area = (area, nombre_area, observaciones) => {
            var areaOpt = new Option(nombre_area, area, true, true);
            area_id = area;
            $('#observaciones').val( observaciones === 'null' ? '' : observaciones );
            $('#area_id').append(areaOpt).trigger('change');
            $('#area_id').attr('disabled', 'disabled');
            $('#btnAgregar').text("Editar").css('background-color','#ec971f').css('border-color', '#e08e0b');
            $('#area_id').valid();
        }

        return {
            get_giras,
            crear_o_editar,
            eliminar,
            agregar_area,
            eliminar_area,
            editar_area,
            guardar
        }

    })();
</script>