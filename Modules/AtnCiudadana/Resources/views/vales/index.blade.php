@extends('atnciudadana::layouts.master')

@section('myCSS')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.slide-fade-enter-active {
  transition: all .3s ease;
}
.slide-fade-leave-active {
  transition: all .8s cubic-bezier(1.0, 0.5, 0.8, 1.0);
}
.slide-fade-enter, .slide-fade-leave-to
/* .slide-fade-leave-active below version 2.1.8 */ {
  transform: translateX(10px);
  opacity: 0;
}
</style>
@endsection

@section('content-subtitle', 'Vales')

@section('li-breadcrumbs')
    <li class="active">Vales</li>
@endsection

@section('content')
<div class="box box-primary" id="app">
    <transition name="slide-fade" mode="out-in">
        <div class="box-body" v-if="vales.index">
            <p>Solicitud de un nuevo Vale</p>
        </div>
    </transition>
    <transition name="slide-fade" mode="out-in">
        <div class="box-body" v-if="vales.create">
            <form role="form">
                <div class="box-body">
                    <div class="form-group">
                        <label>Date:</label>
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        <input type="text" class="form-control pull-right" id="fecha">
                    </div>
                    <!-- /.input group -->
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Quien Firma</label>
                        <input type="" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">File input</label>
                        <input type="file" id="exampleInputFile">

                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="checkbox">
                        <label>
                        <input type="checkbox"> Check me out
                        </label>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </transition>
    <transition name="slide-fade" mode="out-in">
        <div class="box-body" v-if="vales.list">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="tiporemitente_id">Tipo de Remitente:</label>
                        <select id="tiporemitente_id" name="tiporemitente_id" class="form-control select2">
                            @if(isset($solicitud))
                                <option value="{{ $solicitud->tiposremitente_id }}" selected>{{ $solicitud->tiposremitente->tipo }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
                    <div class="form-group">
                        <label for="tiporemitente_id">Filas:</label>
                        <select id="rows" name="tiporemitente_id" class="form-control select2">
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="75">75</option>
                        <option value="100">100</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pull-right">
                    <div class="form-group">
                        <label for="search">Buscar:</label>
                        <input class="form-control" id="search" placeholder="" type="text">
                    </div>
                </div>
            </div>
            <div class="row">
            <table id="solicitudes" class="table table-bordered table-hover data-table">
                <thead>
                    <tr>
                        <th>Folio</th>
                        <th>Recepcion</th>
                        <th>Lugar</th>
                        <th>Asunto</th>
                        <th>Consultar</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Folio</th>
                        <th>Recepcion</th>
                        <th>Lugar</th>
                        <th>Asunto</th>
                        <th>Consultar</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </tfoot>
            </table>
            </div>
            
        </div>
    </transition>
</div>
@stop

@section('myScripts')
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<!-- jQuery Validation -->
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<!-- InputMask -->
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ asset('dist/js/adminlte.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
new Vue({
    el: '#app',
    data: {
        msg: 'Vales',
        vales: {
            index: false,
            list: false,
            create: true
        }
    },
    mounted(){
        $('#fecha').datepicker({autoclose: true, language: 'es', endDate: '0d', orientation: 'bottom'});
        axios.get('{{ url("atnciudadana/vales/last/2018") }}').then(response => { console.log(response) }).catch(error => { console.log(error) });
    },
    methods: {
        go(component){
            switch (component){
                case 'new':
                    this.vales.index = false;
                    this.vales.list = false;
                    this.vales.create = true;
                    console.log(component);
                break;
                case 'list':
                    this.vales.index = false;
                    this.vales.create = false;
                    this.vales.list = true;
                    console.log(component);
                break;
            }
        }
    }
});
</script>
@endsection