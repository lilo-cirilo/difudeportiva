<?php

namespace Modules\AtnCiudadana\DataTables;

use Modules\AppPreregistro\Entities\GiraPreregistro;

use Yajra\DataTables\Services\DataTable;

class PreSolicitudesDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addIndexColumn()
        /*->editColumn('gira', function($query) {
            return $query->gira . ' (' . $query->municipio . ')';
        })*/
        ->addColumn('consultar', function($query) {
            return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="mostrarSolicitud('.
            $query->id .',' . $query->beneficio_id . ')"><i class="fa fa-address-card"></i> Consultar</button>';
        })
        ->rawColumns(['consultar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = GiraPreregistro::
        select(
            [
                'atnc_giraspreregis.id as id',
                'atnc_giraspreregis.folio as folio',
                'atnc_cat_giras.nombre as gira',
                'cat_municipios.nombre as municipio',
                'atnc_giraspreregis.nombre as nombre',
                'atnc_giraspreregis.primer_apellido as primer_apellido',
                'atnc_giraspreregis.segundo_apellido as segundo_apellido',
                'beneficiosprogramas.nombre as beneficio',
                'beneficiosprogramas.id as beneficio_id'
            ]
        )
        ->leftjoin('even_eventos', 'even_eventos.id', '=', 'atnc_giraspreregis.evento_id')
        ->leftjoin('atnc_cat_giras', 'atnc_cat_giras.id', '=', 'even_eventos.gira_id')

        ->leftjoin('cat_municipios', 'cat_municipios.id', '=', 'even_eventos.municipio_id')

        ->leftjoin('atnc_giraspreregis_beneficiosprog', 'atnc_giraspreregis_beneficiosprog.girapreregis_id', '=', 'atnc_giraspreregis.id')
        ->leftjoin('beneficiosprogramas', 'beneficiosprogramas.id', '=', 'atnc_giraspreregis_beneficiosprog.beneficioprograma_id');

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns =
        [
            //'id' => ['data' => 'id', 'name' => 'atnc_giraspreregis.id'],
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
            // 'folio' => ['data' => 'folio', 'name' => 'atnc_giraspreregis.folio'],
            'gira' => ['data' => 'gira', 'name' => 'atnc_cat_giras.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'nombre' => ['data' => 'nombre', 'name' => 'atnc_giraspreregis.nombre', 'orderable' => false, 'searchable' => false],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'atnc_giraspreregis.primer_apellido','orderable' => false, 'searchable' => false],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'atnc_giraspreregis.segundo_apellido','orderable' => false, 'searchable' => false],
            'beneficio' => ['data' => 'beneficio', 'name' => 'beneficiosprogramas.nombre','orderable' => false, 'searchable' => false],
            'detalle' => ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'rowGroup' => [
                'dataSrc' => 'gira'
            ],
            'order' => [
                1,
                'desc'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'atnciudadana/PreSolicitudes_' . date('YmdHis');
    }
}