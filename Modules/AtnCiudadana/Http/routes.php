<?php

use GuzzleHttp\Middleware;

Route::group(['middleware' => 'web', 'prefix' => 'atnciudadana', 'namespace' => 'Modules\AtnCiudadana\Http\Controllers'], function () {

  Route::get('/', 'SolicitudController@index');
  Route::get('/indexSolicitudes', 'SolicitudController@indexSolicitudes')->name('atnciudadana.indexSolicitudes');
  Route::get('peticiones/general', 'SolicitudController@peticiones')->name('atnciudadana.peticiones.general');
  Route::get('beneficiarios', 'SolicitudController@beneficiarios')->name('atnciudadana.beneficiarios');


  Route::post('crear/solicitud/interna', 'AtnCiudadanaController@crearSolicitudInterna')->name('atnciudadana.crear.solicitud.interna');;


  Route::resource('solicitudes', 'SolicitudController', ['except' => ['destroy']]);
  Route::resource('presolicitudes', 'PreSolicitudesController');
  Route::resource('solicitudes.anexos', 'SolicitudAnexoController', ['as' => 'atnciudadana']);
  Route::resource('solicitudes.programas', 'SolicitudProgramaController', ['only' => ['index', 'store', 'update', 'destroy'], 'as' => 'atnciudadana']);
  Route::resource('solicitudes.programas.beneficiarios', 'SolicitudProgramaBeneficiarioController', ['only' => ['store', 'update', 'destroy', 'show'], 'as' => 'atnciudadana']);
  Route::post('/solicitudes/{solicitud}/programas/{programa}/estatus', 'SolicitudProgramaController@estatus')->name('atnciudadana.solicitudes.programas.estatus');

  Route::get('vales/{solicitud_id}', 'ValeController@index')->where('solicitud_id', '[0-9]+');
  Route::get('vales/last/{anio}', 'ValeController@lastIDVale')->where('solicitud_id', '[0-9]+');
  Route::get('vales/whosign', 'ValeController@lastIDVale')->where('solicitud_id', '[0-9]+');

  Route::get('/dependencias', 'AtnCiudadanaController@dependencias')->name('atnciudadana.dependencias.index');

  //Peticiones genérico
  Route::get('/peticiones/direcciones', 'SolicitudController@direcciones')->name('atnciudadana.peticiones.direcciones');
  Route::get('/peticiones/personas', 'SolicitudController@peticionesPersona')->name('atnciudadana.peticiones.personas');
  Route::resource('peticiones', 'PeticionController', ['only' => ['index', 'show', 'update'], 'as' => 'atnciudadana']);
  Route::resource('peticiones.beneficiarios', 'PeticionBeneficiarioController', ['only' => ['show', 'store', 'destroy'], 'as' => 'atnciudadana']);
  Route::get('/peticionesDireccion/{direccion_id?}', 'SolicitudController@PeticionesDireccion');
  Route::get('/peticionesAgrupadas/{direccion_id?}', 'SolicitudController@peticionesAgrupadas')->name('atnciudadana.peticiones.area');
  Route::get('/peticionesPersona/{persona_nombre?}', 'SolicitudController@getPeticionesPersona');

  //Programas genérico
  Route::resource('programas', 'ProgramaController', ['as' => 'atnciudadana']);
  Route::get('programasExternos', 'ProgramaController@getExternos')->name('atnciudadana.programas.externos');

  //Direcciones
  Route::get('/direcciones', 'SolicitudController@getDirecciones')->name('atnciudadana.direcciones.select');
  Route::get('/direcciones/{direccion_id?}', 'SolicitudController@getDireccion')->name('atnciudadana.direccion');
  Route::get('/direccionprogramas', 'SolicitudController@getProgramasDireccion')->name('atnciudadana.direccion.programas');

  Route::get('/peticionesA', 'SqlSolicitudController@index')->name('atnciudadana.peticionesA');

  Route::get('/direcciones/telegram/{area} ', 'TelegramController@ResultadosDiariosParaTelegram')->name('atnciudadana.telegram_bug');
});

Route::group(['middleware' => 'web', 'prefix' => 'atnciudadana', 'namespace' => 'Modules\AtnCiudadana\Http\Controllers'], function () {
  Route::get('/direcciones/telegram/show/{area} ', 'TelegramController@ResultadosDiariosParaTelegram')->name('atnciudadana.telegram');
});