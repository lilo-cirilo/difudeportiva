<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\QueryException;
use App\Models\Persona;
use App\Models\beneficiosPersonas;
use App\Models\PeticionesPersonas;
use App\Models\PeticionesProductos;
use App\Models\BeneficiosprogramasSolicitud;
use View;

class SolicitudProgramaBeneficiarioController extends Controller {
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($peticion_id, $programa_id, Request $request) {
        if($request->ajax()) {
            try {
                $programa_solicitud = BeneficiosprogramasSolicitud::where('id',request()->get('beneficioprograma_solicitud_id'))->first();
                                    
                $persona = Persona::find($request->persona_id);
                DB::beginTransaction();
                
                
                //Comprobando que no se repita la misma persona en la misma peticion-programa
                $benefPersona = $persona->beneficiosPersonas->where('beneficioprograma_id',$programa_id)->first();
                if($benefPersona && $benefPersona->peticionesPersonas->contains("beneficioprograma_solicitud_id",$programa_solicitud->id)){
                    return response()->json(['errors' => array(['code' => 409, 'message' => 'La persona ya se encuentra registrada como beneficiario de esta petición.'])], 409);
                }
                
                //Asociando la persona con la peticion-programa
                $bp = beneficiosPersonas::Create([
                    'persona_id' => $persona->id,
                    'beneficioprograma_id' => $programa_id,
                    'usuario_id'=> Auth::user()->id]);
                /* ],
                [
                    'usuario'=> Auth::user()->id
                ]); */

                $pp = PeticionesPersonas::create([
                    'beneficiosprogramas_solicitud_id' => $programa_solicitud->id,
                    "beneficiopersona_id" =>$bp->id,
                    "entregado" => 0,
                    "observacion_entrega" => null,
                    "evaluado" => 0,
                    "observacion_evaluado" => null,
                    "usuario_id" => Auth::user()->id
                ]);

                /* $pp2 = PeticionesProductos::create([
                    "salidas_producto_id" => null,
                    "beneficioprogsol_benefpersona_id" => $pp->id,
                    "usuario_id" => Auth::user()->id
                ]); */

                auth()->user()->bitacora(request(), [
                    'tabla' => 'beneficiosprog_personas',
                    'registro' => $bp->id. '',
                    'campos' => json_encode($bp),
                    'metodo' => 'POST'
                ]);

                auth()->user()->bitacora(request(), [
                    'tabla' => 'beneficioprogsol_benefpersona',
                    'registro' => $pp->id . '',
                    'campos' => json_encode($pp),
                    'metodo' => 'POST'
                ]);

               /*  auth()->user()->bitacora(request(), [
                    'tabla' => 'afu_peticionesproductos',
                    'registro' => $pp2->id . '',
                    'campos' => json_encode($pp2),
                    'metodo' => 'POST'
                ]); */
                    
                DB::commit();
                return response()->json(['status'=>'ok'],200);

            } catch(\Exception $e) {
                DB::rollBack();
                return new JsonResponse(['data' => null, 'message' => $e->getMessage()], 500);//->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($peticion_id, $programa_id, $persona_id){
        $persona = Persona::find($persona_id);
        
        $beneficio = ProgramasSolicitud::where('solicitud_id',$peticion_id)
            ->where('programa_id', $programa_id)
            ->first();
            
        /*$db_ext = \DB::connection('sqlsrv');
        $historial = $db_ext->table('beneficiarios_funcionales')
                    ->where(function ($query) use ($persona) {
                        $query->where('curp', $persona->curp)
                            ->whereNotNull('curp')
                            ->where('curp', '!=', '');
                    })                    
                    ->orWhere(function ($query) use ($persona) {
                        $query->where('nombre', $persona->nombre)
                            ->where('primer_apellido', $persona->primer_apellido)
                            ->where('segundo_apellido', $persona->segundo_apellido)
                            ->where('fecha_nacimiento', $persona->get_formato_fecha_nacimiento());
                    })->get();*/

        $beneficiario = View::make('atnciudadana::solicitudes.personas.show2')
            ->with("persona", $persona)
            ->with("beneficio", $beneficio);
            //->with("historial", $historial);
        
        $pichon = SolicitudesPersona::where('programas_solicitud_id', $beneficio->id)
                    ->where('persona_id', $persona_id)->first();
        $estatus = null;
        if($pichon){
            $estatus = $pichon->observacionevaluado;
        }
        
        return response()->json([
            'beneficiario' =>  $beneficiario->render(),
            'estatus' => $estatus //Para saber si puede editar info del solicitante
        ], 200);
    }

    //Reactivar solicitante
    public function update($peticion_id, $programa_id, $persona_id, Request $request){
        if($request->ajax()) {
            try {                
                $programa_solicitud = ProgramasSolicitud::where("solicitud_id",$peticion_id)
                                                        ->where("programa_id",$programa_id)
                                                        ->first();

                //Comprobando que no existan mas personas que las indicadas en la solicitud
                if((SolicitudesPersona::where("programas_solicitud_id",$programa_solicitud->id)->whereNull('observacionevaluado')->count() + 1) > $programa_solicitud->cantidad && $programa_solicitud->cantidad > 0){
                    return response()->json(['errors' => array(['code' => 422, 'message' => 'No pueden haber más beneficiarios que la cantidad solicitada.'])], 422);
                }

                $solicitud_persona = SolicitudesPersona::where("programas_solicitud_id",$programa_solicitud->id)
                                                        ->where("persona_id",$persona_id)
                                                        ->first();

                DB::beginTransaction();
                $solicitud_persona->entregado = null;
                $solicitud_persona->observacionevaluado = null;
                $solicitud_persona->save();

                auth()->user()->bitacora(request(), [
                    'tabla' => 'solicitudes_personas',
                    'registro' => $solicitud_persona->id . '',
                    'campos' => json_encode($solicitud_persona) . '',
                    'metodo' => 'PUT'
                ]);
                DB::commit();
                return response()->json(['status' => '', 'persona_id' => $persona_id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($peticion_id, $programa_id, $persona_id, Request $request){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                // $programa_solicitud = ProgramasSolicitud::where("solicitud_id",$peticion_id)
                //                                         ->where("programa_id",$programa_id)
                //                                         ->first();
                // $beneficio_solicitud = BeneficiosprogramasSolicitud::where("solicitud_id",$peticion_id)
                //                                                     ->where('beneficioprograma_id',$programa_id)
                //                                                     ->first();
                $beneficio_solicitud = BeneficiosprogramasSolicitud::join("beneficiosprogsol_benefpersonas","beneficiosprogramas_solicitudes.id","=","beneficiosprogsol_benefpersonas.beneficiosprogramas_solicitud_id")
                                                                    ->join("beneficiosprog_personas","beneficiosprog_personas.id","=","beneficiosprogsol_benefpersonas.beneficiopersona_id")
                                                                    ->where("beneficiosprogramas_solicitudes.solicitud_id",$peticion_id)
                                                                    ->where("beneficiosprogramas_solicitudes.beneficioprograma_id",$programa_id)
                                                                    ->where("beneficiosprog_personas.persona_id",$persona_id)
                                                                    ->select(['beneficiosprogsol_benefpersonas.id as id'])
                                                                    ->first();
                                                                    // dd($beneficio_solicitud);
                                                                    
                $peticion_persona = PeticionesPersonas::find($beneficio_solicitud->id);

                // $beneficio_persona = BeneficiosPersonas::where("beneficioprograma_id",$)

                // $solicitud_persona = SolicitudesPersona::where("programas_solicitud_id",$programa_solicitud->id)
                //                                         ->where("persona_id",$persona_id)
                //                                         ->first();
                
                $peticion_persona->observacion_evaluado = $request->motivo;
                // dd($peticion_persona);
                $peticion_persona->save();
                auth()->user()->bitacora(request(), [
                    'tabla' => 'solicitudes_personas',
                    'registro' => $peticion_persona->id . '',
                    'campos' => json_encode($peticion_persona) . '',
                    'metodo' => 'PUT'
                ]);
                DB::commit();
                return response()->json(['status' => '', 'persona_id' => $persona_id], 200);
            } catch(\Exception $e) {
                DB::rollBack();
                return response()->json(['success' => false, 'code' => 409, 'message' => 'Error: ' . $e->getMessage()], 409);
            }
        }
    }
}