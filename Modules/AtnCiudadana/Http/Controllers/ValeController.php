<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

use App\Models\Vale;

class ValeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if($request->solicitud_id){
            return view('atnciudadana::vales.index');
        }else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('atnciudadana::vales.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                $datos = $request->except(['beneficios']);

                $datos['fecha_recepcion'] = (\DateTime::createFromFormat('d/m/Y', $datos['fecha_recepcion']))->format('Y-m-d');
                $datos['fecha_oficio'] = (\DateTime::createFromFormat('d/m/Y', $datos['fechao_oficio']))->format('Y-m-d');

                $datos['folio'] = "I/" . (Vale::max('id') + 1) . "/" . date("Y");
                
                $datos['usuario_id'] = Auth::user()->id;

                $solicitudvale = Vale::create($datos);

                foreach ($request->input('beneficios') as &$b) {
                   ProgramasSolicitud::create(
                            [
                                'solicitud_id' => $solicitudvale->id,
                                'programa_id' => $b['id'],
                                'cantidad' => $b['cantidad'],
                                'usuario_id' => Auth::user()->id
                            ]
                       );
                }

                DB::commit();

                return response()->json(array('success' => true, 'id' => $solicitudvale->id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $solicitudvale = Solicitud::findOrFail($request->id);
        return view('atnciudadana::vales.show', array('solicitudvale' => $solicitudvale));
       // return view('atnciudadana::vales.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('atnciudadana::vales.create_edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function lastIDVale($anio)
    {
        //if(request()->wantsJson()){
        $vales = DB::connection('sqlsrv')->select('SELECT MAX(Cp_FolioCaja)as ultimo_folio FROM [BdRecursosFinan].[dbo].[Tb_R_Vales] WHERE cp_ejercicio=?', [$anio]);
        return new JsonResponse($vales);
        //}
        //return new JsonResponse(['error' => 'No se pudo obtener el ultimo id insertado'], 403);
    }

    public function whoSign(){
        
    }
}

