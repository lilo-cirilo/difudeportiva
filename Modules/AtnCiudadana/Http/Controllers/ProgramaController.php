<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use App\Http\Controllers\ProgramaBaseController;
use Illuminate\Routing\Controller;

use App\DataTables\BeneficiosDataTable;

class ProgramaController extends ProgramaBaseController{
    
  function __construct(){
    parent::__construct('adminlte','ATENCIÓN CIUDADANA','atnciudadana');
  }

  public function getExternos(BeneficiosDataTable $dataTable) {
    return $dataTable
        ->with('padre_id',759)->render(
      "atnciudadana::programas.index", 
      [
        'modulo' => 'atnciudadana',
        'programa_padre' => 'ATENCIÓN CIUDADANA'
      ]
    );
	}

}