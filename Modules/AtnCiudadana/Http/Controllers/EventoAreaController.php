<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use App\Models\Evento;
use App\Models\EventoAreas;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;


use App\Http\Requests\Giras\EventoAreaRequest;


class EventoAreaController extends Controller {

    //Test:
    public function index($evento_id) {
        return EventoAreas::search()->where('evento_id', $evento_id)->get();
    }

    //Test: OK 
    //Fecha: 07/12/2018
    public function store($evento_id, Request $request) {
        try {
            DB::beginTransaction();
            $request['evento_id'] = $evento_id;
            $request['usuario_id'] = auth()->user()->id;

            $evento_area = EventoAreas::withTrashed()->where('evento_id', $evento_id)->where('area_id', $request['area_id'])->first();
            if($evento_area) {
                if($evento_area->trashed()) {
                    //Fue eliminado el evento_area pero podemos reactivar el registro en la BD
                    $evento_area->restore();
                    $evento_area->observaciones = $request['observaciones'];
                    $evento_area->usuario_id = $request['usuario_id'];
                    $evento_area->save();
                }else{
                    return response()->json(array('success' => false, 'message' => 'El área ya se encuentra registrada en el mismo evento.'));
                }
            }else {
                $evento_area = EventoAreas::create($request->all());  
            }
            DB::commit();
            return response()->json(array('success' => true, 'evento_area' => EventoAreas::search()->where('areas_eventos.id', $evento_area->id)->firstOrFail()));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }

    //Test: 
    public function show($evento_id, $area_id) {
        return EventoAreas::search()->where('areas_eventos.evento_id', $evento_id)->where('areas_eventos.area_id', $area_id)->firstOrFail();
    }

    //Test: OK 
    //Fecha: 07/12/2018
    public function update($evento_id, $area_id, Request $request) {
        $area_evento = EventoAreas::where('area_id', $area_id)->where('evento_id', $evento_id)->first();
        if(!$area_evento){
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        
        if(EventoAreas::where('area_id', $request->area_id)->where('evento_id', $evento_id)->where('id', '!=', $area_evento->id)->count() > 0){
            return response()->json(array('success' => false, 'message' => 'Ya existe una área asociada a este evento.'));
        }

        $request['usuario_id'] = auth()->user()->id;
        try {
            DB::beginTransaction();
            $area_evento->update($request->all());
            DB::commit();
            return response()->json(array('success' => true, 'evento_area' => EventoAreas::search()->where('evento_id', $evento_id)->where('areas_eventos.id', $area_evento->id)->firstOrFail()));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
    
    //Test: OK 
    //Fecha: 07/12/2018
    public function destroy($evento_id, $area_id) {
        $evento_area = EventoAreas::where('area_id', $area_id)->where('evento_id', $evento_id)->first();
        if(!$evento_area){
            return response()->json(array('success' => false, 'message' => 'Recurso no encontrado.'));
        }
        try{
            DB::beginTransaction();
            $evento_area->delete();
            DB::commit();
            return response()->json(array('success' => true, 'message' => 'Área eliminada exitosamente.'));
        } catch(\Exception $e) {
            DB::rollBack();
            return response()->json(array('success' => false, 'message' => 'Error en la BD: ' . $e->getMessage()));
        }
    }
}
