<?php


namespace Modules\AtnCiudadana\Http\Controllers;


use App\Models\BeneficiosprogramasSolicitud;
use App\Models\Area;
use Illuminate\Routing\Controller;

use Carbon\Carbon;
use Exception;

class TelegramController extends Controller
{

  public function ResultadosDiariosParaTelegram(Area $area)
  {
    $solicitudes_beneficios = BeneficiosprogramasSolicitud::with(
      'statusActual.statusproceso',
      'beneficio.anioprograma.programa.lastPadre.AreasPrograma.area'
    )->get();

    $programas_por_area = Area::with(
      'programas.programa'
    )->where('id', $area->id)->get()
      ->pluck('programas')
      ->first()
      ->filter(function ($value) {
        return $value->programa->tipo == "PROGRAMA" && $value->programa->oficial == 1;
      })->map(function ($item) {
        return $item->programa->nombre;
      });

    $resultados_por_area = collect();
    $resultados_numericos = collect();
    $resultados_detalle_programas = collect();
    $resultados_total_programas = collect();
    $resultados_numericos_ayer = collect();

    $solicitudes_una_direccion = $solicitudes_beneficios->filter(function ($solicitud) use ($area) {
      return $solicitud->beneficio->anioprograma->programa
        ->lastPadre->padre->areasPrograma->area->nombre == $area->nombre;
    });


    $nuevos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
    }, 0);

    $proceso_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $concluidos_por_area = $solicitudes_una_direccion->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);


    foreach ($programas_por_area as $programa) {

      $resultados_numericos_por_programa = collect();
      $solicitudes_de_un_programa = $solicitudes_una_direccion
        ->filter(function ($value) use ($programa) {
          return $value->beneficio->anioprograma->programa
            ->lastPadre->padre->nombre == $programa;
        });

      $nuevos_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "VINCULADO" ? 1 : 0);
      }, 0);

      $concluidos_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
          || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
      }, 0);

      $proceso_por_programa = $solicitudes_de_un_programa->reduce(function ($carry, $item) {
        return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
          || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
          || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
      }, 0);

      $resultados_numericos_por_programa->put("nuevos", $nuevos_por_programa);
      $resultados_numericos_por_programa->put("proceso", $proceso_por_programa);
      $resultados_numericos_por_programa->put("concluidos", $concluidos_por_programa);

      $resultados_detalle_programas->put($programa, $resultados_numericos_por_programa);
      $resultados_total_programas->put(
        $programa,
        $nuevos_por_programa +
          $concluidos_por_programa +
          $proceso_por_programa

      );
    }

    $solicitudes_ayer = $solicitudes_una_direccion->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::yesterday());
    });

    $solicitudes_cinco_dias = $solicitudes_una_direccion->filter(function ($value) {
      return Carbon::parse($value->estadosSolicitud->last()->created_at)
        ->lessThan(Carbon::now()->subDays(5));
    });

    $finalizados_cinco_dias = $solicitudes_cinco_dias->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $finalizados_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "FINALIZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "RECHAZADO"
        || $item->estadosSolicitud->last()->statusproceso->status == "CANCELADO" ? 1 : 0);
    }, 0);

    $proceso_ayer = $solicitudes_ayer->reduce(function ($carry, $item) {
      return $carry + ($item->estadosSolicitud->last()->statusproceso->status == "Vo. Bo."
        || $item->estadosSolicitud->last()->statusproceso->status == "VALIDANDO"
        || $item->estadosSolicitud->last()->statusproceso->status == "LISTA DE ESPERA" ? 1 : 0);
    }, 0);

    $resultados_numericos->put("nuevos", $nuevos_por_area);
    $resultados_numericos->put("proceso", $proceso_por_area);
    $resultados_numericos->put("concluidos", $concluidos_por_area);
    $resultados_numericos->put("total", $nuevos_por_area + $proceso_por_area + $concluidos_por_area);

    $resultados_numericos_ayer->put("Proceso", $proceso_ayer);
    $resultados_numericos_ayer->put("finalizados", $finalizados_ayer);
    $resultados_numericos_ayer->put("inicio_tasa_semanal", $finalizados_cinco_dias);

    $resultados_por_area->put("nombre_direccion", $area->nombre);
    $resultados_por_area->put("totales", $resultados_numericos);
    $resultados_por_area->put("detalles_programas", $resultados_detalle_programas);
    $resultados_por_area->put("total_programas", $resultados_total_programas);
    $resultados_por_area->put("resultados_estadisticos", $resultados_numericos_ayer);


    return $resultados_por_area;
  }
}
