<?php

namespace Modules\AtnCiudadana\Http\Controllers;

use App\Models\SolicitudAnexo;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;

class SolicitudAnexoController extends Controller {
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index() {
        return view('atnciudadana::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create() {
        return view('atnciudadana::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store($solicitud_id, Request $request) {
        $directorio_publico = 'public/atnciudadana/solicitudes' . '/' . $solicitud_id . '/anexos' ;
        /*$url_publico = Storage::putFile($directorio_publico, $request->file);
        $url_base = explode('public', $url_publico);
        $url_local = 'storage' . $url_base[1];*/
        $request->file->storeAs($directorio_publico . '/' . $request->file->getClientOriginalName(), $request->file->extension());

        $anexo = SolicitudAnexo::create([
            'solicitud_id' => $solicitud_id,
            'anexo' => $request->file->getClientOriginalName()
        ]);
        
        $datos['id'] = $anexo->id;
        $datos['url'] = asset($url_local);        
        return $datos;
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('atnciudadana::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('atnciudadana::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
