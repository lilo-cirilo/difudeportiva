<?php

namespace Modules\Braille\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\DataTables\Braille\Cursos;

use App\Models\Programa;
use App\Models\Braille\Curso;

use View;

class CursosController extends Controller{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Cursos $cursos){
        return $cursos->render('braille::cursos.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(){
        return response()->json([
                'body' =>  view::make('braille::cursos.create_edit')
                ->render()
            ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
        try {
            $programa_id = Programa::where('nombre','like','braille')
            ->first()->id;
            DB::beginTransaction();
            $new = Curso::firstOrCreate(
                ['nombre'=>$request->input('nombre','')],
                [
                    'descripcion'=>$request->input('descripcion',''),
                    'programa_id'=>$programa_id
                ]
            );
            DB::commit();
            return array('success' => true, 'id' => $new->id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id){
        return response()->json([
                'body' =>  view::make('braille::cursos.create_edit')
                ->with([
                    'curso'=>Curso::find($id),
                    'readonly'=>true
                ])
                ->render()
            ], 200);
    }

    /**
     * Show the form for editing a resource.
     * @return Response
     */
    public function edit($id){
        return response()->json([
                'body' =>  view::make('braille::cursos.create_edit')
                ->with('curso',Curso::find($id))
                ->render()
            ], 200);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id){
        try {
            DB::beginTransaction();
            $new = Curso::find($id);
            $new->update(
                $request->all()
            );
            DB::commit();
            return array('success' => true, 'id' => $new->id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id){
        try {
            DB::beginTransaction();
            Curso::find($id)->delete();
            DB::commit();
            return array('success' => true, 'id' => $id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }
}
