<?php

namespace Modules\Braille\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\DataTables\Braille\CursosPersonas;
use App\DataTables\Braille\Beneficiarios;
use App\DataTables\Braille\Personas;

use App\Models\Braille\AlumnosBraille;
use App\Models\DocumentosPersona;
use App\Models\PersonasPrograma;
use App\Models\AniosPrograma;
use App\Models\Discapacidad;
use App\Models\Ejercicio;
use App\Models\Programa;
use App\Models\Tutor;

class BeneficiarioController extends Controller{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Beneficiarios $tabla){
        return $tabla->render('braille::beneficiarios.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(Personas $tabla){
        $aniosPrograma = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
        ->where([
            ['programas.nombre', 'like', 'BRAILLE'],
            ['cat_ejercicios.anio',date('Y')]])
        ->first(['anios_programas.id'])->id;
        $documentos = AniosPrograma::find($aniosPrograma)->documentosprogramas;
        $discapacidades = Discapacidad::where('padre_id',73)->get();
        return $tabla->render('braille::beneficiarios.create_edit',['documentos'=>$documentos,'discapacidades'=>$discapacidades]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    /*$programa_id = Programa::firstOrCreate(
                ['nombre'=>'braille'],
                ['activo'=>1, 'descripcion'=>'CENTRO DE ATENCION A PERSONAS CIEGAS Y DEBILES VISUALES', 'tipo'=>'PROGRAMA', 'codigo'=>'BR', 'fecha_inicio'=>date('Y-m-d'),'oficial'=>1,'usuario_id'=>$request->user()->id]
            );
            $ejecicio_id = Ejercicio::firstOrCreate(['anio'=>date('Y')]);*/
    public function store(Request $request){
        try {
            $aniosPrograma_id = AniosPrograma::leftjoin('programas', 'programas.id', '=', 'anios_programas.programa_id')
            ->leftjoin('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
            ->where([
                ['programas.nombre', 'like', 'BRAILLE'],
                ['cat_ejercicios.anio',date('Y')]])
            ->first(['anios_programas.id'])->id;
            DB::beginTransaction();
            $new = AlumnosBraille::firstOrCreate(
                ['persona_id'     => $request->input('persona_id',0)],
                ['discapacidad_id'=> $request->input('discapacidad_id',0)]
            );
            $tutor = Tutor::firstOrCreate(
                [
                    'persona_id'        => $request->input('tutor_id',0),
                    'tutorado_id'       => $request->input('persona_id',0)
                ],
                [
                    'activo'            => 1,
                    'anios_programa_id' => $aniosPrograma_id,
                    'usuario_id'        => $request->user()->id
                ]);
            $reg=PersonasPrograma::firstOrCreate(
                [
                    'persona_id'       => $request->input('persona_id',0),
                    'anios_programa_id'=> $aniosPrograma_id
                ],
                [
                    'baja'             => 0,
                    'fechabaja'        => null,
                    'motivo_id'        => null,
                    'usuario_id'       => $request->user()->id
                ]
            );

            $documentos=(object)json_decode($request->input('infoDocumentos'));

            foreach ($documentos as $documento) {
                $persona;
                if($documento->nombre =="tutor")
                    $persona=$request->input('tutor_id',0);
                elseif ($documento->nombre == "beneficiario")
                    $persona=$request->input('persona_id',0);
                DocumentosPersona::firstOrCreate(
                    ['persona_id'=>$persona,'documentos_programa_id'=>$documento->id],
                    ['usuario_id'=>$request->user()->id,'presento'=>'COPIA']
                );
            }
            DB::commit();
            return array('success' => true, 'alumno' => $new->id, 'tutor'=>$tutor->id ,'perPro'=>$reg->id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id,CursosPersonas $avances){
        $alumno = AlumnosBraille::find($id);
        $tutor  = Tutor::join('anios_programas','anios_programas.id','tutores.anios_programa_id')
        ->join('programas','programas.id','anios_programas.programa_id')
        ->where('programas.nombre','like','%braille%')
        ->where('tutores.tutorado_id',$alumno->persona->id)
        ->first();//->persona;

        return $avances->with('persona_id',$alumno->persona->id)->render('braille::beneficiarios.show',['beneficiario'=>$alumno,'tutor'=>$tutor]);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('braille::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id){
        try {
            DB::beginTransaction();
            AlumnosBraille::find($id)->delete();
            DB::commit();
            return array('success' => true, 'id' => $id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }
}
