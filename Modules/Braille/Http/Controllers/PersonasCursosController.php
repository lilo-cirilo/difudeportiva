<?php

namespace Modules\Braille\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Illuminate\Support\Facades\DB;

use App\DataTables\Braille\PersonasCursos;

use App\Models\Braille\CursosPersonas;

class PersonasCursosController extends Controller{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PersonasCursos $personas){
        return $personas->render('braille::cursos.inscripciones');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(){
        return view('braille::cursos.inscripciones');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){
        try {
            DB::beginTransaction();
            $new = CursosPersonas::firstOrCreate(
                [
                    'curso_id'   => $request->input('curso_id',0),
                    'persona_id' => $request->input('persona_id',0)
                ]
            );
            DB::commit();
            return array('success' => true, 'id' => $new->id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id){
        try {
            DB::beginTransaction();
            CursosPersonas::find($id)->delete();
            DB::commit();
            return array('success' => true, 'id' => $id);
        }catch(Exeption $e) {
            DB::rollBack();
            return array('success' => false);
        }
    }
}
