<?php

Route::group(['middleware' => 'web', 'prefix' => 'braille', 'namespace' => 'Modules\Braille\Http\Controllers'], function(){
  Route::get('/', 'BrailleController@index')->name('braille.home');
  Route::resource('/beneficiarios', 'BeneficiarioController', ['as' => 'braille', 'parameters' => ['beneficiarios' => 'id']]);
  Route::resource('/cursos','CursosController',['as' => 'braille', 'parameters' => ['cursos' => 'id']]);//,'except'=>['create','edit']
  Route::resource('/notas','NotasController',['as'=>'braille.beneficiario', 'parameters'=>['notas' => 'id']]);
  Route::resource('/talleres','TalleresController',['as' => 'braille', 'parameters' => ['talleres' => 'id']]);
  Route::resource('/curso/personas','PersonasCursosController',['as'=>'braille.cursos','parameters'=>['personas'=>'id'],'except'=>['create','edit','update','show']]);
  Route::resource('peticiones', 'PeticionController',['as'=>'braille','only'=>['index','show','update','destroy']]);
  Route::resource('programas', 'ProgramaController',['as'=>'braille']);
});
