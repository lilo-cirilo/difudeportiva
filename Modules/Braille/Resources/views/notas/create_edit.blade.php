@extends('braille::layouts.master')

@push('head')
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content-subtitle', 'Nueva nota clinica')

@section('li-breadcrumbs')
<li><a href="{{route('braille.beneficiarios.index')}}">Beneficiarios</a></li>
<li class="active">Notas clinicas</li>
@endsection

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Nueva nota</h3>
    </div>

    <form id="form_nota" data-toggle="validator"
    action="{{isset($nota) ? route('braille.cursos.personas.update',$nota->id) : route('braille.cursos.personas.store')}}"
    method="{{isset($nota) ? 'PUT' : 'POST' }}">
        <div class="box-body">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label>Seleccionar curso:</label>
                    <select id="curso_id" name="curso_id" class="form-control select2">
                        <option value="{{ $nota->curso->id or '' }}" selected>{{ $nota->curso->nombre or '' }}</option>
                    </select>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                  <label for="persona_id">Seleccione persona:</label>
                  <select id="persona_id" name="persona_id" class="form-control select2">
                    <option value="{{ $nota->persona->id or '' }}" selected>{{ $nota->persona->nombre or '' }}</option>
               </select>
           </div>
       </div>

       <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
            <label>Titulo:</label>
            <input type="text" id="titulo" name="titulo" class="form-control" value="{{ $nota->titulo or '' }}" placeholder="Titulo de la nota">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="form-group">
            <label>Descripcion:</label>
            <br>
            <textarea id="descripcion" name="descripcion" class="form-control" placeholder="Contenido de la nota">{{ $nota->descripcion or '' }}</textarea>
        </div>
    </div>

</div>
<div class="box-footer">
    <div class="pull-right">
        <button type="button" class="btn btn-success" onclick="procesarNota()">GUARDAR</button>
        <a href="{{route('braille.beneficiarios.index')}}" class="btn  btn-danger">CANCELAR</a>
    </div>
</div>
</form>
</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

<script type="text/javascript">
    $("#form_nota").validate({
        rules : {
            persona_id : {
                required : true
            },
            curso_id : {
                required : true
            },
            titulo : {
                required : true,
                minlength: 3
            },
            descripcion : {
                required : true
            }
        }
    });
    $.extend($.validator.messages, {
        required: 'Este campo es obligatorio.',
        maxlength: $.validator.format('Por favor, no escribas más de {0} caracteres.'),
        minlength: $.validator.format('Por favor, no escribas menos de {0} caracteres.')
    });
    $.validator.setDefaults({
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function(error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    var curso = $('#curso_id'),
    persona = $('#persona_id');
    curso.select2({
        language: 'es',
        ajax: {
            url: '{{ route('braille.cursos.index') }}',
            delay: 500,
            dataType: 'JSON',
            type: 'GET',
            data: function(params) {
                return {
                    columns : {
                        1 : {
                            data : 'nombre',
                            name : 'cat_cursos.nombre',
                            search : {
                                value : params.term,
                                regex : false
                            }
                        }
                    }
                };
            },
            processResults: function(data, params) {
                params.page = params.page || 1;
                return {
                    results: $.map(data.data, function(item) {
                        return {
                            id: item.id,
                            text: item.nombre,
                            slug: item.nombre,
                            results: item
                        }
                    })
                };
            },
            cache: true
        }
    }).change(function(event) {
        curso.valid();
        persona.empty();
        persona.select2({
            language: 'es',
            ajax: {
                url: '{{ route('braille.cursos.personas.index') }}',
                delay: 500,
                dataType: 'JSON',
                type: 'GET',
                data: function(params) {
                    return {
                        search   : {
                           value : params.term
                        },
                        curso    : $("#curso_id").val()
                    };
                },
                processResults: function(data, params) {
                    params.page = params.page || 1;
                    return {
                        results: $.map(data.data, function(item) {
                            return {
                                id: item.persona_id,
                                text: item.nombreCompleto,
                                slug: item.nombreCompleto,
                                results: item
                            }
                        })
                    };
                },
                cache: true
            }
        }).change(function(event) {
            persona.valid();
        });
    });
    persona.select2({
        language: 'es'
    });

    function procesarNota() {
        if($("#form_nota").valid()){
            block();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $('#form_nota').attr('action'),
                type: $('#form_nota').attr('method'),
                data   : $('#form_nota').serialize(),
                dataType: 'JSON',
                success: function(response) {
                    unblock();
                    swal({
                        title: '¡Guardado!',
                        text: 'El curso se ha agregado correctamente',
                        type: 'success',
                        timer: 2500
                    })
                    $('#create_edit').modal("hide");
                    $('#cursos').DataTable().ajax.reload();
                },
                error : function(response){
                    unblock();
                    swal({
                        title: '¡Error!',
                        text: 'El curso no se ha guarado',
                        type: 'error',
                        timer: 2500
                    })
                }
            });
        }
    }
</script>
@endpush
