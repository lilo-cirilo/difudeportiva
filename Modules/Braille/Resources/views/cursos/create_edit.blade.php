<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<h4 class="modal-title">{{isset($curso)? 'Modificar' : isset($readonly)? 'Ver' : 'Crear'}} curso</h4>
</div>
<form data-toggle="validator" role="form" id="form_curso"
action="{{isset($curso) ? route('braille.cursos.update',$curso->id) : route('braille.cursos.store')}}"
method="{{isset($curso) ? 'PUT' : 'POST' }}">
	<div class="modal-body">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="form-group">
					<label for="nombre">Nombre:</label>
					<input type="text" class="form-control" id="nombre" name="nombre"
					placeholder="Nombre" value="{{ $curso->nombre or '' }}" @isset($readonly)
					readonly
					@endisset>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="form-group">
					<label for="descripcion">Descripcion:</label>
					<textarea class="form-control" id="descripcion" name="descripcion"
					placeholder="Descripcion" {{ isset($readonly) ?	'readonly' : '' }}>{{$curso->descripcion or ''}}</textarea>
				</div>
			</div>
		</div>
	</div>
<div class="modal-footer">
	<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
	@if (isset($readonly))
	''
	@else
		<button type="button" class="btn btn-primary" onclick="procesarCurso()">Guardar</button>
	@endif
</div>
</form>
