@extends('braille::layouts.master')

@section('content-subtitle', 'Tabla de Beneficiarios')

@push('head')
	<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">

	<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
	<link href="{{ asset('css/animate.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')
	<div class="box box-primary">
		<div class="box-header with-border">
			<h3 class="box-title">Tabla de Cursos:</h3>
		</div>
		<div class="box-body">
			<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
				<input type="text" id="search" name="search" class="form-control">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
				</span>
			</div>

			{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'cursos']) !!}

		</div>
	</div>

	<div class="modal fade" id="create_edit">
		<div class="modal-dialog">
			<div class="modal-content">

			</div>
		</div>
	</div>
@stop

@push('body')
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
	<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>

	<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

	{!! $dataTable->scripts() !!}

	<script type="text/javascript">

	$("#form_curso").validate({
		rules : {
			nombre : {
				required : true
			},
			descripcion : {
				required : true
			}
		}
	});

	$.extend($.validator.messages, {
		required: 'Este campo es obligatorio.',
		remote: 'Por favor, rellena este campo.'
	});

	$.validator.setDefaults({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorPlacement: function(error, element) {
			$(element).parents('.form-group').append(error);
		}
	});

	var table = (function() {
		var tabla = undefined,
		btn_buscar = $('#btn_buscar'),
		search = $('#search');

		function init() {
			search.keypress(function(e) {
				if(e.which === 13) {
					tabla.DataTable().search(search.val()).draw();
				}
			});

			btn_buscar.on('click', function() {
				tabla.DataTable().search(search.val()).draw();
			});
		};

		function set_table(valor) {
			tabla = $(valor);
		};

		function get_table() {
			return tabla;
		};

		return {
			init: init,
			set_table: set_table,
			get_table: get_table
		};
	})();

	table.set_table($('#cursos').dataTable());
	table.init();

	(function ($,DataTable){
		DataTable.ext.buttons.new = {
			className: '',
			text: function (dt) {
				return  '<i class="fa fa-plus"></i> nuevo'; //+ dt.i18n('buttons.print', 'Print');
			},
			action: function (e,dt,button,config) {
				var url = "{{route('braille.cursos.create')}}";
				cargarModal(url);
				//alert('click');
			}
		};
	})(jQuery, jQuery.fn.dataTable);

	function cargarModal(url) {
		$.get(url, function(data) {
			$('.modal-content').html(data.body);
			$('#create_edit').modal("show");
		});
	}

	function procesarCurso() {
		if($("#form_curso").valid()){
			block();
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url: $('#form_curso').attr('action'),
				type: $('#form_curso').attr('method'),
				data   : $('#form_curso').serialize(),
				dataType: 'JSON',
				success: function(response) {
					unblock();
					swal({
						title: '¡Guardado!',
						text: 'El curso se ha agregado correctamente',
						type: 'success',
						timer: 2500
					})
					$('#create_edit').modal("hide");
					$('#cursos').DataTable().ajax.reload();
				},
				error : function(response){
					unblock();
					swal({
						title: '¡Error!',
						text: 'El curso no se ha guarado',
						type: 'error',
						timer: 2500
					})
				}
			});
		}
	}

	function eliminarCurso(url) {
		swal({
			title              : '¿Está seguro?',
			text               : '¡Usted va a eliminar un curso!',
			type               : 'warning',
			focusCancel		   : true,
			showCancelButton   : true,
			confirmButtonColor : '#C51414',
			cancelButtonColor  : '#554747',
			confirmButtonText  : 'Si',
			cancelButtonText   : 'No',
			allowEscapeKey     : true,
			allowOutsideClick  : true,
			animation          : false,
			customClass        : 'animated pulse',
		}).then((result)=>{
			if(result.value){
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url: url,
					type: 'DELETE',
					success: function(response) {
						unblock();
						swal({
							title             : 'Eliminado!',
							text              : 'El curso se ha eliminado correctamente',
							type              : 'info',
							timer             : 3000,
							animation         : false,
							customClass       : 'animated jackInTheBox',
							showConfirmButton : false,
						})
						$('#cursos').DataTable().ajax.reload();
					},
					error : function(response){
						unblock();
						swal({
							title: '¡Error!',
							text: 'El curso no se ha eliminado',
							type: 'error',
							timer: 2500
						})
					}
				});
			}
		});
	}
	</script>
@endpush
