@extends('braille::layouts.master')

@section('content-subtitle')
Consultar Beneficiario
@endsection

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/buttons.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<style type="text/css">
	/*.collapsed .fa {
		transform: rotate(225deg);
	}
	button .fa {
  		transition: .3s transform ease-in-out;
	}*/
	.dir {
		width         : 100%;
		border-bottom : 1px solid black;
		margin-bottom : 0px;
	}
</style>
@endpush

@section('content')
{{-- Informacion del beneficiario --}}
<div class="box box-primary"> 
	<div class="box-header with-border">
		<h3 class="box-title">Consultar Beneficiario:</h3>
	</div>
	<div class="box-body">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
			<label>Nombre:</label> {{ isset($beneficiario->persona)? $beneficiario->persona->obtenerNombreCompleto() : '' }}
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
			<label>Edad:</label> {{ isset($beneficiario->persona)? $beneficiario->persona->get_edad() : '' }}
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8">
			<label>Télefono:</label> {{ $beneficiario->persona->numero_local or '' }}
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
			<label>Celular:</label>	{{ $beneficiario->persona->numero_celular or '' }}
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<label>Discapacidad:</label> {{ $beneficiario->discapacidad->nombre or '' }}
		</div>
	</div>
</div>
{{-- Informcion de tutor --}}
<div class="box box-warning collapsed-box">
	<div class="box-header with-border">
		<h3 class="box-title">Datos del tutor:</h3>
		<div class="pull-right box-tools">
            <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-plus"></i>
            </button>
        </div>
	</div>
	<div class="box-body">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
				<label>Nombre:</label> {{ isset($tutor->persona)? $tutor->persona->obtenerNombreCompleto() : '' }}
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
				<label>Télefono:</label> {{ $tutor->persona->numero_local or '' }}
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
				<label>Celular:</label>	{{ $tutor->persona->numero_celular or '' }}
			</div>

			<div class="col-xs-12">
				<label>Direccion:</label>
			</div>

			<div class="col-sm-12">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
					<label class="text-center dir" aria-describedby="calle">{{ $tutor->persona->calle or '' }}</label>
					<div id="calle" class="text-muted text-center text-sm" style="width: 100% !important;">calle</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-1">
					<label class="text-center dir" aria-describedby="numero">{{ $tutor->persona->numero_exterior or '' }}</label>
					<div id="numero" class="text-muted text-center text-sm" style="width: 100% !important;">numero</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
					<label class="text-center dir" aria-describedby="colonia">{{ $tutor->persona->colonia or '' }}</label>
					<div id="colonia" class="text-muted text-center text-sm" style="width: 100% !important;">colonia</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-1">
					<label class="text-center dir" aria-describedby="cp">{{ $tutor->persona->codiglopostal or 'S/CP' }}</label>
					<div id="cp" class="text-muted text-center text-sm" style="width: 100% !important;">cp</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
					<label class="text-center dir" aria-describedby="municipio">{{ $tutor->persona->municipio->nombre or '' }}</label>
					<div id="municipio" class="text-muted text-center text-sm" style="width: 100% !important;">municipio</div>
				</div>
			
			<div class="col-xs-12">
				<label class="text-center dir" aria-describedby="referencia">{{ $tutor->persona->referencia_domicilio or '' }}</label>
				<div id="referencia" class="text-muted text-center text-sm" style="width: 100% !important;">referencia de domicilio</div>
			</div>
		</div>
	</div>
</div>
{{-- Inofrmacion sobre el desarrollo del beneficiario --}}
<div class="box box-info">
	<div class="box-header with-border">
		<h3>Desarrollo del alumno</h3>
		<div class="pull-right box-tools">
              <button class="btn btn-box-tool" type="button" data-widget="collapse" data-toggle="tooltip" data-original-tittle="Cerrar">
                <i class="fa fa-minus"></i>
              </button>
        </div>
	</div>
	<div class="box-body">
		{!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'avance']) !!}
	</div>
	<div class="box-footer">
		<a href="{{ route('braille.beneficiarios.index') }}" class="btn btn-danger btn-md pull-right"><i class="fa fa-reply"></i> Regresar</a>
	</div>	
</div>
@stop
@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>

{!! $dataTable->scripts() !!}

<script type="text/javascript">

</script>
@endpush