<h4>General:</h4>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
						<img src="{{ asset(isset($persona) ? $persona->get_url_fotografia() : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-8 col-lg-10">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<label>Nombre:</label>{{ isset($persona)? $persona->obtenerNombreCompleto() : '' }}
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Género:</label>
									@isset ($persona)
									@if ($persona->genero == 'M')
									MASCULINO
									@elseif($persona->genero == 'F')
									FEMENINO
									@endif
									@endisset
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Fecha de Nacimiento:</label>
									{{ isset($persona) ? $persona->get_formato_fecha_nacimiento().' ('.floor( time() - strtotime($persona->fecha_nacimiento) / 31556926).' años)' : '' }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>CURP:</label>
									{{ $persona->curp or '' }}
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="form-group">
									<label>Clave Electoral:</label>
									{{ $persona->clave_electoral or '' }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label>Télefono:</label>
									{{ $persona->numero_local or '' }}
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
								<div class="form-group">
									<label for="numero_celular">Celular:</label>
									{{ $persona->numero_celular or '' }}
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
								<div class="form-group">
									<label>Correo Electrónico:</label>
									{{ $persona->email or '' }}
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<label>Discapacidad:</label> {{ $beneficiario->discapacidad->nombre or '' }}
							</div>
						</div>
					</div>
				</div>

				<h4>Dirección:</h4>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
						<div class="form-group">
							<label>Calle:</label>
							{{ $persona->calle or '' }}
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
						<div class="form-group">
							<label>Número Exterior:</label>
							{{ $persona->numero_exterior or '' }}
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
						<div class="form-group">
							<label>Número Interior:</label>
							{{ $persona->numero_interior or '' }}
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
						<div class="form-group">
							<label>Colonia:</label>
							{{ $persona->colonia or '' }}
						</div>
					</div>

					@if(isset($persona->codigopostal))
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-2">
						<div class="form-group">
							<label>Código Postal:</label>
							{{ $persona->codigopostal or '' }}
						</div>
					</div>
					@endif

				</div>

				<div class="row">

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>Región:</label>
							{{ $persona->localidad->municipio->distrito->region->nombre or '' }}
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
						<div class="form-group">
							<label>Distrito:</label>
							{{ $persona->localidad->municipio->distrito->nombre or '' }}
						</div>
					</div>

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="form-group">
							<label>Municipio:</label>
							{{ $persona->localidad->municipio->nombre or '' }}
						</div>
					</div>

					@if(isset($persona->localidad))
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
						<div class="form-group">
							<label>Localidad:</label>
							{{ $persona->localidad->nombre or '' }}
						</div>
					</div>
					@endif

				</div>

				<div class="row">

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>Referencias del Domicilio:</label>
							{{ $persona->referencia_domicilio or '' }}
						</div>
					</div>

				</div>
