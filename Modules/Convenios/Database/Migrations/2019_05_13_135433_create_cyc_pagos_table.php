<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cyc_pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('mes');
            $table->integer('anio');
            $table->decimal('monto', 12, 2);
            $table->unsignedInteger('convenio_id');
            $table->timestamps();
        });

        Schema::table('cyc_pagos', function (Blueprint $table) {     
            $table->foreign('convenio_id')->references('id')->on('cyc_convenios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cyc_pagos', function (Blueprint $table) {
            $table->dropForeign(['convenio_id']);
        });
        Schema::dropIfExists('cyc_pagos');
    }
}
