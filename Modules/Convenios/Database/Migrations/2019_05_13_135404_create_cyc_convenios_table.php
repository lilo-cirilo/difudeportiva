<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCycConveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cyc_convenios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('institucion_id');
            $table->unsignedInteger('area_id');
            $table->text('informe');
            $table->decimal('monto', 12, 2);
            $table->string('pdf')->nullable();
            $table->timestamps();
        });

        Schema::table('cyc_convenios', function (Blueprint $table) {     
            $table->foreign('institucion_id')->references('id')->on('cat_instituciones');
            $table->foreign('area_id')->references('id')->on('cat_areas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cyc_convenios', function (Blueprint $table) {
            $table->dropForeign(['institucion_id']);
            $table->dropForeign(['area_id']);
        });

        Schema::dropIfExists('cyc_convenios');
    }
}
