@extends('convenios::layouts.master')

@section('styles')
@endsection

@section('content')
<div class="section-top-border" id="app">
    <h2>Dirección General</h2>
    <h3 class="mb-30">Convenios y Contratos</h3>
    <p>Convenios y contratos del Sistema DIF Oaxaca.</p>
    <div class="progress-table-wrap">
        <table class="table tabel-sm table-striped table-bordered" id="dataTable">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Asociación</th>
                <th scope="col">Area</th>
                <th scope="col">Informe</th>
                <th scope="col">Convenio - Juridico</th>
                <th scope="col">Convenio - Area</th>
                <th scope="col">Convenio - Finanzas</th>
                <th scope="col">Comprobación - Área</th>
                <th scope="col">Comprobación - Finanzas</th>
                <th scope="col">Factura - Area</th>
                <th scope="col">Factura - Finanzas</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="convenio in convenios">
                    <th scope="row">@{{convenio.id}}</th>
                    <td>@{{convenio.asociacion.nombre}}</td>
                    <td>@{{convenio.direccion.nombre}}</td>
                    <td>
                        <button class="genric-btn info-border small" @click.prevent="abrirDetalles(convenio)"><span class="lnr lnr-eye"></span></button>
                        <a class="genric-btn info-border small" :href="convenio.pdf" target="_blank"><span class="lnr lnr lnr-file-empty"></span></a>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.validacion_juridico ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.validacion_juridico ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.validacion_direccion ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.validacion_direccion ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.validacion_finanzas ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.validacion_finanzas ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.comprobacion_direccion ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.comprobacion_direccion ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.comprobacion_finanzas ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.comprobacion_finanzas ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.factura_direccion ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.factura_direccion ? 'Validado' : 'No Validado'}}</span>
                    </td>
                    <td>
                        <span :class="['badge', convenio.validacion.factura_finanzas ? 'badge-success' : 'badge-danger']">@{{convenio.validacion.factura_finanzas ? 'Validado' : 'No Validado'}}</span>
                    </td>
                </tr>
            </tbody>

        </table>
    </div>
    <!-- Inicio Modal Detalle -->
    <div class="modal" tabindex="-1" role="dialog" id="modalDetalle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detalles del Convenio</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Resumen de pagos.</p>
                <table class="table">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>Anio</td>
                            <td>Mes</td>
                            <td>Monto</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="pago in pagos">
                            <td>@{{pago.id}}</td>
                            <td>@{{pago.anio}}</td>
                            <td>@{{pago.mes}}</td>
                            <td>@{{pago.monto}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td><strong>Acumulado:</strong></td>
                            <td><strong>@{{total}}</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Detalle -->
</div>
@endsection

@section('scripts')
<script>
new Vue({
    el: '#app',
    data: {
        convenios: [],
        pagos: [],
    },
    computed: {
        total() {
            let t = 0;
            this.pagos.forEach(function(pago) {
                t += parseFloat(pago.monto);
            });
            return t.toFixed(2);
        }
    },
    mounted(){
        axios.get('/convenios/convenios')
            .then(response => {
                this.convenios = response.data;
                console.log(response);
            })
            .catch(error => {
                console.log(error);
            });
    },
    methods: {
        abrirDetalles(convenio){
            axios.get(`/convenios/${convenio.id}/detalle`)
                .then(response => {
                    this.pagos = response.data.pagos;
                    this.total = this.calcularTotal(this.data.pagos);
                })
                .catch(error => {

                });
            $('#modalDetalle').modal();
        },
        calcularTotal(pagos){
            let total = 0;
            pagos.forEach(pago => {
                console.log(pago);
                total += pago.monto;
            });
            return total;
        },
    }
});

</script>
@endsection