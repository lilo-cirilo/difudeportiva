@extends('convenios::layouts.master')

@section('styles')
@endsection

@section('breadcrumbs')
<li class="breadcrumb-item active">Crear</li>
@endsection

@section('content')
<div class="section-top-border" id="app">
    <div class="modal fade" id="dangerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-danger" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@{{error.titulo}}</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h6>@{{error.estatus}}</h6>
                    <p>@{{error.contenido}}</p>
                    <p>@{{error.solucion}}</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                    {{-- <button class="btn btn-danger" type="button">Save changes</button> --}}
                </div>
            </div>
        </div>
    </div>
    <h2>Administración y Finanzas</h2>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Asociación</a>
        </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <form @submit.prevent="guardarAsociacion()">
                <div class="form-group">
                    <label for="asociacion">Asociación</label>
                    <input type="text" class="form-control" id="asociacion" v-model="asociacion.nombre" placeholder="Nueva Fundación o Asociación">
                </div>
                <div class="form-group">
                    <label for="direccion">Area responsable</label>
                    <vue-multiselect v-model="direccion" :options="areas" label="nombre" :internal-search="false" :preserve-search="false" :clear-on-select="true" @search-change="buscarArea"></vue-multiselect>
                </div>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
new Vue({
    el: '#app',
    data: {
        asociacion: {
            nombre: '',
        },
        direccion: null,
        areas: [],
        responsable: {
            titulo: '',
            nombre: '',
            primer_apellido: '',
            segundo_apellido: '',
        },
        error: {
            titulo: null,
            estatus: null,
            contenido: null,
            solucion: null
        }
    },
    methods: {
        buscarArea(val){
            if(val.length < 3) return;
            axios.get(`/convenios/areas/${val}`).then(response => {
                this.areas = response.data;
            }).catch(error => {
                this.error.titulo = 'Algo salio mal';
                this.error.estatus = `Código error: ${error.response.status}`;
                switch(error.response.status){
                    case 500:
                        this.error.contenido = 'Hubo un problema con el servidor.';
                        this.error.solucion = 'Intenta nuevamente ó contacta al área de sistemas.';
                    break;
                    default:
                        this.error.contenido = 'No sabemos que paso. Contacta al área de sistemas.';
                    break;
                }
                $('#dangerModal').modal('show');
            });
        },
        guardarAsociacion(){
            axios.post('/catalogos/asociacion', { nombre: this.asociacion.nombre.toUpperCase() })
                .then(response => {
                    if(response.data.id){
                        swal('Great !', 'Se guardo correctamente', 'success');
                        this.asociacion.nombre = '';
                    }
                    else{
                        swal('Oops !', `Ocurrio un error desconocido`, 'warning');
                    }
                })
                .catch(error => {
                    swal('Oops !', `Ocurrio un error. ${error}`, 'warning');
                    console.log(error);
                });
        },
        guardarDireccion(){
            axios.post('/catalogos/direccion', { nombre: this.direccion.nombre.toUpperCase() })
                .then(response => {
                    if(response.data.id){
                        swal('Great !', 'Se guardo correctamente', 'success');
                        this.direccion.nombre = '';
                    }
                    else{
                        swal('Oops !', `Ocurrio un error desconocido`, 'warning');
                    }
                })
                .catch(error => {
                    swal('Oops !', `Ocurrio un error. ${error}`, 'warning');
                    console.log(error);
                })
        },
        guardarResponsable(){
            axios.post('/catalogos/responsable', {
                titulo: this.responsable.titulo.toUpperCase(),
                nombre: this.responsable.nombre.toUpperCase(),
                primer_apellido: this.responsable.primer_apellido.toUpperCase(),
                segundo_apellido: this.responsable.segundo_apellido.toUpperCase()
            })
                .then(response => {
                    if(response.data.id){
                        swal('Great !', 'Se guardo correctamente', 'success');
                        titulo: this.responsable.titulo = '';
                        this.responsable.nombre = '';
                        this.responsable.primer_apellido = '';
                        this.responsable.segundo_apellido = '';
                    }
                    else{
                        swal('Oops !', `Ocurrio un error desconocido`, 'warning');
                    }
                })
                .catch(error => {
                    swal('Oops !', `Ocurrio un error. ${error}`, 'warning');
                    console.log(error);
                })
        }
    }
});
</script>
@endsection