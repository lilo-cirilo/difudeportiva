try {
  window.$ = window.jQuery = require('jquery');
  window.Vue = require('vue');
  window.axios = require('axios');
  window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
  window.VueMultiselect = require('vue-multiselect');
  Vue.component('vue-multiselect', window.VueMultiselect.default);
  let token = document.head.querySelector('meta[name="csrf-token"]');
  if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
  } else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
  }
  require('bootstrap');
  require('perfect-scrollbar/dist/perfect-scrollbar.min.js');
  require('@coreui/coreui/dist/js/coreui.min.js');
} catch (e) {
  console.log(e);
}