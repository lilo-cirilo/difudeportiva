<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use App\Convenio;
use Illuminate\Support\Facades\Storage;

class PDFController extends Controller
{
    public function subirPDF(Request $request){
        $convenio = Convenio::findOrFail($request->convenio_id);
        if($convenio){
            if($convenio->pdf != null){
                $path = str_replace('storage','public', $convenio->pdf);
                Storage::delete($path);
            }
            $path = $request->file('file')->store('public/convenios');
            $path = str_replace('public', 'storage', $path);
            $convenio->pdf = $path;
            $convenio->save();
            if($path){
                return $convenio;
            }
            return ['error' => 'Ocurrio un error'];
        }
    }


}
