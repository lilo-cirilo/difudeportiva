<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Validacion;
use App\Direccion;
use App\Responsable;
use App\Asociacion;
use App\Convenio;
use App\Pago;

class FinanzasController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        $this->middleware('rolModuleV2:convenios,FINANZAS');
    }

    public function index($id){
        $direcciones = Direccion::all();
        $responsables = Responsable::all();
        $asociaciones = Asociacion::all();
        return view('convenios::finanzas.index', ['asociaciones' => $asociaciones, 'responsables' => $responsables, 'direcciones' => $direcciones]);
    }

    public function obtenerDatos(){
        $direcciones = Direccion::all();
        $responsables = Responsable::all();
        $asociaciones = Asociacion::all();
        return new JsonResponse(['asociaciones' => $asociaciones, 'responsables' => $responsables, 'direcciones' => $responsables]);
    }

    public function guardarConvenio(Request $request){
        $this->validate($request, [
            'direccion_id' => 'required|integer',
            'responsable_id' => 'required|integer',
            'asociacion_id' => 'required|integer',
            'informe' => 'required',
            'monto' => 'required|numeric']);
        $convenio = Convenio::create($request->all());
        $validacion = Validacion::create(['convenio_id' => $convenio->id]);
        return new JsonResponse(['convenio' => $convenio, 'validacion' => $validacion]);
    }

    public function obtenerConvenios(Request $request){
        if(request()->wantsJson()){
            $convenios = Convenio::with(['direccion', 'responsable', 'asociacion', 'validacion'])->paginate(2);
            foreach($convenios as $convenio){
                $convenio->procesando = false;
            }
            return new JsonResponse($convenios);
        }
        return new JsonResponse(['error' =>  'Error']);
    }

    public function validarConvenio(Request $request, $id){
        $convenio = Validacion::findOrFail($id);
        $convenio->validacion_finanzas = ! $convenio->validacion_finanzas;
        $convenio->save();
        return new JsonResponse($convenio);
    }

    public function validarComprobacion(Request $request, $id){
        $convenio = Validacion::findOrFail($id);
        if($convenio->validacion_finanzas && !$convenio->comprobacion_finanzas) {
            $convenio->comprobacion_finanzas = ! $convenio->comprobacion_finanzas;
            $convenio->save();
            return new JsonResponse($convenio);
        }
        return new JsonResponse(['convenio' => $convenio, 'error' => ['status' => 403, 'msg' => 'Falta la validación del convenio']], 403);
    }

    public function validarFactura(Request $request, $id){
        $convenio = Validacion::findOrFail($id);
        if($convenio->comprobacion_finanzas && !$convenio->factura_finanzas){
            $convenio->factura_finanzas = ! $convenio->factura_finanzas;
            $convenio->save();
            return new JsonResponse($convenio);
        }
        return new JsonResponse(['convenio' => $convenio, 'error' => ['status' => 403, 'msg' => 'Falta la validación de la comprobación']], 403);
    }

    public function validarPago(Request $request, $id){
        $convenio = ValidacionConvenio::findOrFail($id);
        if($convenio->factura_finanzas){
            $convenio->pago_finanzas = ! $convenio->pago_finanzas;
            $convenio->save();
            return new JsonResponse($convenio);
        }
        return new JsonResponse(['convenio' => $convenio, 'error' => ['status' => 403, 'msg' => 'Falta la validación de la factura']], 403);
    }

    public function guardarPago($id, Request $request){
        $this->validate($request, [
            'anio' => 'required',
            'mes' => 'required',
            'monto' => 'required'
        ]);

        $pago = Pago::where('mes', $request->mes)->where('anio', $request->anio)->where('convenio_id', $id)->get();
        if($pago && $pago->count() > 0){
            return new JsonResponse(['pago' => ['anio' => $request->anio, 'mes' => $request->mes, 'monto' => $request->monto], 'error' => ['status' => 422, 'msg' => 'Ya existe un pago en ese mes y año']], 422);
        }

        $convenio = Convenio::findOrFail($id);
        $pago = Pago::create(['convenio_id' => $id, 'anio' => $request->anio, 'mes' => $request->mes, 'monto' => $request->monto]);
        if($pago){
            return new JsonResponse($pago);
        }
        return new JsonResponse([], 402);
    }

    public function historialPagos($id){
        $convenio = Convenio::findOrFail($id);
        $pagos = Pago::where('convenio_id', $convenio->id)->get();
        return new JsonResponse($pagos);
    }
}
