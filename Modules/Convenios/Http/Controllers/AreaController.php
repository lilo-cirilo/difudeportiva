<?php

namespace Modules\Convenios\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Modules\Convenios\Entities\Convenio;
use Modules\Convenios\Entities\Validacion;
use App\Models\Area;

class AreaController extends Controller
{
    public function index(){
        if(! auth()->user()->hasRoles(['AREA'])){
            return view('errors.403');
        }
        return view('area.index');
    }

    public function obtenerConvenios(Request $request){
        if(! auth()->user()->hasRoles(['AREA'])){
            return new JsonResponse([], 403);
        }

        if(request()->wantsJson()){
            if(auth()->user()->direccion){
                $convenios = Convenio::with(['direccion', 'asociacion', 'responsable', 'validacion'])->where('direccion_id', auth()->user()->direccion->direccion_id)->get();
                foreach($convenios as $convenio){
                    $convenio->procesando = false;
                }
                return new JsonResponse($convenios);
            }
            return new JsonResponse(['msg' => 'No tiene asignada una direccion', 'status' => 422], 422);
        }
        return new JsonResponse(['error' =>  'Error'], 403);
    }

    public function validarConvenio(Request $request, $id){
        if(! auth()->user()->hasRoles(['AREA'])){
            return new JsonResponse([], 403);
        }
        $validacion = Validacion::where('convenio_id', $id)->first();
        if($validacion && !$validacion->validacion_direccion){
            $validacion->validacion_direccion = ! $validacion->validacion_direccion;
            $validacion->save();
            return new JsonResponse($validacion);
        }
        return new JsonResponse(['msg' => 'No se encontro o ya no puede cambiar el estado'], 422);
    }

    public function validarComprobacion(Request $request, $id){
        if(! auth()->user()->hasRoles(['AREA'])){
            return new JsonResponse([], 403);
        }

        $validacion = Validacion::where('convenio_id', $id)->first();
        if($validacion && !$validacion->comprobacion_direccion) {
            if($validacion->validacion_direccion){
                $validacion->comprobacion_direccion = ! $validacion->comprobacion_direccion;
                $validacion->save();
                return new JsonResponse($validacion);
            }
            else{
                return new JsonResponse(['msg' => 'Falta la validacion del convenio'], 403);
            }
        }
        return new JsonResponse(['msg' => 'No se encontro o ya no puede cambiar el estado'], 403);
    }

    public function validarFactura(Request $request, $id){
        if(! auth()->user()->hasRoles(['AREA'])){
            return new JsonResponse([], 403);
        }

        $validacion = Validacion::where('convenio_id', $id)->first();
        if($validacion && !$validacion->factura_direccion) {
            if($validacion->comprobacion_direccion){
                $validacion->factura_direccion = ! $validacion->factura_direccion;
                $validacion->save();
                return new JsonResponse($validacion);
            }
            else{
                return new JsonResponse(['msg' => 'Falta la comprobacion del convenio'], 403);
            }
        }
        return new JsonResponse(['msg' => 'No se encontro o ya no puede cambiar el estado'], 403);
    }

    //buscar el area que sea tipo dirección
    public function buscarArea($area)
    {
        return new JsonResponse(Area::where('nombre', 'like', "%$area%")->where('tipoarea_id', 1)->get());
    }

}
