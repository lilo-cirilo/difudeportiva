<?php

namespace Modules\Convenios\Entities;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $table = 'cyc_pagos';

    protected $fillable = ['convenio_id', 'monto', 'mes', 'anio'];
}
