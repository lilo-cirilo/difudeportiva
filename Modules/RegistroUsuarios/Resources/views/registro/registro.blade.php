@extends('orgeventos::layouts.master')

@section('content-subtitle')
Lista de Participantes
@endsection

@push('head')

<link href="{{ asset('bower_components/vue/vue-multiselect.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content')

<div id="participante">
	<section class="content">

      	
    </section>
   
</div>
@stop

@push('body')

<script type="text/javascript" src="{{ asset('bower_components/vue/vue-multiselect.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/vue/vee-validate.js') }}"></script>

<script type="text/javascript" src="{{ asset('bower_components/vue/vuejs-datepicker.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('registroUsuarios/registro.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
	    $("#fechaNacimiento").addClass( "form-control" );
	});
</script>

@endpush
