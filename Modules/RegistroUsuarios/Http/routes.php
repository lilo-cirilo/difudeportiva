<?php

Route::group(['middleware' => 'web', 'prefix' => 'registrousuario', 'namespace' => 'Modules\RegistroUsuarios\Http\Controllers'], function()
{
    Route::get('/', 'RegistroUsuariosController@index');
});
