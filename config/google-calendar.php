<?php

return [

    /*
     * Path to the json file containing the credentials.
     */
    'service_account_credentials_json' => storage_path('app\google-calendar\service-account-credentials.json'),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    //'calendar_id' => env('GOOGLE_CALENDAR_ID'),
    'calendar_id' => '748090634036-d1ujc71g2ptuiv0q6osdbjtqpanvtrep.apps.googleusercontent.com',
];
