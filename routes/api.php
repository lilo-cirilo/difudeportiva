<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'Auth\LoginController@loginApi')->name('login');

//Route::group(['middleware' => ['auth:api', 'cors'], 'prefix' => 'agenda'], function() {
Route::group(['middleware' => ['cors'], 'prefix' => 'agenda'], function() {
	Route::get('eventos', '\Modules\AgendaEventos\Http\Controllers\EventoController@index');
	Route::post('beneficiarios', 'BeneficiarioController@store');
	Route::post('solicitudes', 'SolicitudController@store');
});

Route::post('/arduino/sensores', 'ArduinoController@index');

Route::post('/paginadif/denuncia', 'PaginaDIF\PaginaDIFController@denuncia');

Route::get('/organigrama', 'ArduinoController@organigrama');
// Autopull y notificacion en Telegram
Route::post('/autopull', 'Admin\GitController@autopull');
