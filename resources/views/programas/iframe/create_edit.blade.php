<form data-toggle="validator" role="form" id="form_programa" action="{{ isset($programa) ? ( $modulo !== '' ? route("$modulo.programas.update",$programa->id) : route("beneficios.update",$programa->id) ) : ( $modulo !== "" ? route("$modulo.programas.store") : route("programas.store") ) }}" method="{{ isset($programa) ? 'PUT' : 'POST' }}">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		@if($programa_padre == "")
			 <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="direccion">Dirección</label>
                        <input id="direccion" name="direccion" type="text" class="form-control" readonly value="{{ auth()->user()->persona->empleado->area->nombre }}">
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="area_id">Área responsable</label>
                        <select id="area_id" name="area_id" class="form-control select2" style="width: 100%;">
                            @if(isset($programa))
                                <option value="{{ $programa->programasresponsable->areasresponsable->area->id }}" selected>{{ $programa->programasresponsable->areasresponsable->area->nombre }}</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="programa_padre">Programa principal</label>                        
                        <select id="programa_padre" name="programa_padre" class="form-control select2" style="width: 100%;">
                            @if(isset($programa))
                                <option value="{{ $programa->padre->nombre }}" selected>{{ $programa->padre->nombre }}</option>
                            @endif
                        </select>                        
                    </div>
                </div>
			</div>
		@endif

            <div class="row" style="padding-left: 0; padding-right: 0;">                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombre">Nombre del beneficio* :</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $programa->nombre or '' }}">
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="descripcion">Descripción del beneficio* :</label>
                        <textarea class="form-control" id="descripcion" name="descripcion" rows="3" style="resize:none;">{{ $programa->descripcion or '' }}</textarea>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="tipo">Tipo* :</label>
                        <select id="tipo" name="tipo" class="form-control select2" style="width: 100%;">
                            <option value=""></option>                            
                            <option value="TRÁMITE" {{ (isset($programa) && $programa->tipo == 'TRÁMITE') ? 'selected' : '' }}>TRÁMITE</option>
                            <option value="PRODUCTO" {{ (isset($programa) && $programa->tipo == 'PRODUCTO') ? 'selected' : '' }}>PRODUCTO</option>
                            <option value="SERVICIO" {{ (isset($programa) && $programa->tipo == 'SERVICIO') ? 'selected' : '' }}>SERVICIO</option>
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_inicio">Fecha de inicio:</label>
                        <input type="text" class="form-control text-center" id="fecha_inicio" name="fecha_inicio" value="{{ (isset($programa) && $programa->fecha_inicio) ? $programa->get_formato_fecha_inicio() : '' }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label for="fecha_fin">Fecha de término:</label>
                        <input type="text" class="form-control text-center" id="fecha_fin" name="fecha_fin" value="{{ (isset($programa) && $programa->fecha_fin) ? $programa->get_formato_fecha_fin() : '' }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    <div class="form-group text-center" style="margin-top: 30px">
                        <input id="oficial" name="oficial[]" type="checkbox" {{ (isset($programa) && $programa->oficial) ? 'checked' : '' }}>
                        <label>Beneficio oficial</label>
                    </div>
                </div>
            </div>           

        </div>
    </div>
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Documentación</h3>
            <div class="box-tools pull-right">
                <button id="btn-documentacion" onclick="colapsar('div-documentacion','btn-documentacion');" type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body" id="div-documentacion" style="">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="documentos">Documentación requerida</label>
                        <div class="input-group">
                            <select id="documentos" name="documentos[]" class="form-control select2" multiple="multiple" style="width: 100%;">
                              <option value=""></option>
                                @if(isset($programa) && $programa->aniosprogramas->last())
                                    @foreach($documentos as $documento)
                                        @foreach($programa->aniosprogramas->last()->documentosprogramas as $documento_programa)
                                            @if($documento->id === $documento_programa->documento_id || $documento_programa->documento->nombre == 'SOLICITUD DIRIGIDA AL DIRECTOR DEL SISTEMA DIF OAXACA')
                                                <option selected value="{{ $documento_programa->documento_id }}">{{ $documento_programa->documento->nombre }}</option>
                                            @endif                
                                        @endforeach
                                    @endforeach
                                @endif                        
                            </select>          
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-info" data-toggle="tooltip" title="Nuevo documento" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-documento');">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>              
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>