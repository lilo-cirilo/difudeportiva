@extends("$modulo::layouts.master")

@push('head')
  
@endpush

@if($programa_padre != "")
  @section('content-title', $programa_padre)
@else
  @section('content-title', auth()->user()->persona->empleado->area->nombre)
@endif

@section('content-subtitle', 'Beneficios')

@section('content')
  <h3 >ACCIONES DE {{ Auth::user()->persona->empleado->area->nombre }}</h3>
  <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-8 col-lg-4 float-right ">
    <input type="text" id="search" name="search" class="form-control">
    <div class="input-group-append">
      <button class="btn btn-secondary" id="btn_buscar" name="btn_buscar">Buscar</button>
    </div>
  </div>
  {!! $dataTable->table(['class' => 'table table-bordered table-hover data-table dataTable dt-responsive nowrap dtr-inline', 'id' => 'programas', 'name' => 'programas', 'style' => 'width: 100%']) !!}

  <div class="modal animated zoomIn" id="modal" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          
        </div>
      </div>
  </div>

  <div class="modal animated zoomIn" id="modal-responsable" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Registrar documento</h5>
          <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form data-toggle="validator" role="form" id="form_documento" action="{{ URL::to('documentos') }}" method="POST">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">		
                <div class="row" style="padding-left: 0; padding-right: 0;">						
                  <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <div class="form-group">
                      <label for="nombre">Nombre</label>
                      <input type="text" class="form-control" id="nombre" name="nombre">
                    </div>
                  </div>
                </div>						
                <div class="row" style="padding-left: 0; padding-right: 0;">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for="descripcion">Descripción</label>
                      <input type="text" class="form-control" id="descripcion" name="descripcion">
                    </div>
                  </div>
                </div>		
              </div>
            </div>			
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-custom-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" onclick="documento.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
        </div>      
      </div>
    </div>
  </div>
@stop

@push('body')
  <script type="text/javascript" src="{{ asset('bower_components/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
  {{-- @include('programas.js.programa') --}}
  {!! $dataTable->scripts() !!}

  <script type="text/javascript">
    $("form").submit(function(e){
      e.preventDefault();
    });
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};

    var tabla = ((tablaId) => {
			$('#search').keypress(function(e) {
				if(e.which === 13) {
					$(tablaId).DataTable().search($('#search').val()).draw()
				}
			})
			$('#btn_buscar').on('click', function() {
				$(tablaId).DataTable().search($('#search').val()).draw()
			})
			function recargar(params) {
				$(tablaId).DataTable().ajax.reload(null,false); 
			}
			return {
				recargar : recargar
		}})('#programas');

    (function () {
      var modal = $(".modal.animated")

      modal.on('show.bs.modal', function () {
        var closeModalBtns = modal.find('button[data-custom-dismiss="modal"]');
        closeModalBtns.one('click', function() {
          modal.on('webkitAnimationEnd oanimationend msAnimationEnd animationend', function( evt ) {
            modal.modal('hide')
          });
          modal.removeClass('zoomIn').addClass('zoomOut');
        })
      })

      modal.on('hidden.bs.modal', function ( evt ) {
        var closeModalBtns = modal.find('button[data-custom-dismiss="modal"]');
        modal.removeClass('zoomOut').addClass('zoomIn')
        modal.off('webkitAnimationEnd oanimationend msAnimationEnd animationend')
        closeModalBtns.off('click')
      })
    })()

    function colapsar(div, btn){
      if($("#"+div).hasClass('collapse')){
        $("#"+div).removeClass('collapse');
        $("#"+btn).html('<i class="fa fa-minus"></i>');
      }else{
        $("#"+div).addClass('collapse');
        $("#"+btn).html('<i class="fa fa-plus"></i>');						
      }
    }

    function programa_create_edit_success(response, method) {
      var mensaje = (method === "POST") ? 'registrado' : 'actualizado';
        Swal.fire({
          title: '¡Correcto!',
          text: 'Beneficio ' + mensaje + '.',
          type: 'success',
          timer: 2000
        });
      table.get_table().DataTable().ajax.reload(null, false);
      app.set_bloqueo(false);
      cerrar_modal('modal-programa');
    }

    function programa_create_edit_error(response) {
      if(response.status === 422) {
        Swal.fire({
          title: 'Error al registrar el beneficio.',				
          text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
          type: 'error',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Regresar',
          allowEscapeKey: false,
          allowOutsideClick: false
        });
      }
    };

    function documento_create_edit_success(response, method) {
      var mensaje = (method === "POST") ? 'registrado' : 'actualizado';
      Swal.fire({
          title: '¡Correcto!',
          text: 'Documento ' + mensaje + '.',
          type: 'success',
          timer: 2000
      });		
      var nuevoDocumento = new Option(response.documento.nombre, response.documento.id, true, true);
        $('#documentos').append(nuevoDocumento).trigger('change');
      app.set_bloqueo(false);
      
      cerrar_modal('modal-documento');
    }

    function documento_create_edit_error(response) {};

    function abrir_modal(url) {
        var url = ;
        $("#modal-programa-title").text(title);
        $.get(url,function(data) {            
          $('#modal-body-programa').html(data.html);
          $('#modal').modal('show');            
        });
      /* else if(modal === "modal-detalle"){
        $.get(window.location.href + '/' + id, function(data) {
        $('#modal-body-detalle').html(data.html);
          var groupColumn = 0;
          $("#documentos-programa").DataTable({
          language: {
            url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
          },
          dom: 'itp',
          lengthMenu: [ [5], [5] ],
          responsive: true,
          autoWidth: true,
          processing: true,
          columnDefs: [
            { visible: false, targets: groupColumn },
            { className: 'text-center', targets: '_all' }
          ],
          order: [[ groupColumn, 'desc' ]],
          drawCallback: function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last = null; 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
              if ( last !== group ) {
                $(rows).eq( i ).before(
                  '<tr class="group"><td colspan="2">'+group+'</td></tr>'
                );
                last = group;
              }
            })
          }
        });
        $('#modal-detalle').modal('show');            
      }); */		
    }

    function cerrar_modal(modal){
      $("#"+modal).modal("hide");
    }	

    function eliminar(id, nombre){
      Swal.fire({
        title: '',
        text: '¿Está seguro de eliminar el beneficio '+nombre+'?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        cancelButtonText: 'No',
        confirmButtonText: 'Sí, eliminar'
      }).then((result) => {
        if(result.value) {
          $.ajax({
            url: window.location.href + '/' + id,
            type: 'DELETE',
            success: function(response) {
              table.get_table().DataTable().ajax.reload(null, false);
              Swal.fire({
                title: '¡Correcto!',
                text: 'El beneficio ha sido eliminado.',
                type: 'success',
                timer: 3000
              });
            },
            error: function(response) {}
          });
        }
      });
    }

    function activar(id, nombre) {
      Swal.fire({
        title: '',
        text: '¿Está seguro de activar el beneficio ' + nombre + '?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        cancelButtonText: 'No',
        confirmButtonText: 'Sí, activar'
      }).then((result) => {
        if(result.value) {
    
            abrir_modal('modal-programa', id, true);
        }else{
            table.get_table().DataTable().ajax.reload(null, false);
        }
      });
    }

    function inactivar(id, nombre){
      Swal.fire({
        title: '',
        text: '¿Está seguro de inactivar el beneficio '+nombre+'?',
        type: 'question',
        showCancelButton: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
        cancelButtonText: 'No',
        confirmButtonText: 'Sí, inactivar'
      }).then((result) => {
        if(result.value) {
          $.ajax({
            url: window.location.href + '/' + id,
            data: 'inactivar=true',
            type: 'DELETE',
            success: function(response) {
              table.get_table().DataTable().ajax.reload(null, false);
              Swal.fire({
                title: '¡Correcto!',
                text: 'El beneficio ha sido inactivado.',
                type: 'success',
                timer: 3000
              });
            },
            error: function(response) {}
          });
        }
        else
          table.get_table().DataTable().ajax.reload(null, false);
      });
    }
  </script>
@endpush