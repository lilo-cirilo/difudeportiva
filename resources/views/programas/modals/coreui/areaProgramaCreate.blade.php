<div class="modal-header">
  <h5 class="modal-title" id="modal-detalle-title">ASIGNAR AREA A {{ $programa->nombre }}</h5>
  <button type="button" class="close" data-custom-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
<div class="modal-body" id="modal-body-detalle">
  <label for="area_id"></label>
  <select name="area_id" id="area_id" required>
    <option></option>
  </select>  
</div>
<div class="modal-footer" id="modal-footer-detalle">
  <button type="button" class="btn btn-secondary" data-custom-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary" onClick="asignarArea({{ $programa->id }},$(#area_id).val())">Guardar</button>
</div>      