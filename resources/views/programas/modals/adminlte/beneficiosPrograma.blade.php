<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h3 class="modal-title">BENEFICIOS DE <strong>{{ $programa->nombre }}</strong></h3>
</div>
<div class="modal-body">
  {!! $dataTable->table(['class' => 'table table-sm table-bordered table-striped dt-responsive nowrap', 'style'=>'width:100%', 'id' => 'beneficios']) !!}
</div>
  {!! $dataTable->scripts() !!}
  <script>
    (function ($,DataTable){
      DataTable.ext.buttons.new = {
        className: '',
        text: function (dt) {
          return  '<i class="fa fa-plus"></i> Agregar'; //+ dt.i18n('buttons.print', 'Print');
        },
        action: function (e,dt,button,config) {
          var url = "{{route('programas.beneficios.create',$programa->id)}}";
          abrirModal('ceBeneficio',url);
          //alert('click');
        }
      };
    })(jQuery, jQuery.fn.dataTable);
    function eliminarBeneficio(beneficio_id){
      $.ajax({
        url : '{{ route("programas.beneficios.index",$programa->id) }}'+`/${beneficio_id}`,
        method : 'delete'
      })
      window.LaravelDataTables.beneficios.ajax.reload();
    }
    function togleMostrarAtn(url,estadoTo){
      $.ajax({
        url : url,
        method : 'put',
        data : {
          predeterminado : estadoTo
        }
      })
      window.LaravelDataTables.beneficios.ajax.reload();
    }
  </script>