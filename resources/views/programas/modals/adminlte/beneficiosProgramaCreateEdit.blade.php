<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <h3 class="modal-title">Agregar beneficio</h3>
</div>
<div class="modal-body">
  <div class="form-group">
    <label for="nombre">Nombre del beneficio</label>
    <input type="text" name="nombre" id="nombre" class="form-control" value="{{ $beneficio->nombre or '' }}">
  </div>
  <div class="form-group">
    <label for="nombre">¿Mostrar en atencion ciudadana?</label>
    <input type="checkbox" name="default" id="default" class="">
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary" onClick="{{ isset($beneficio) ? 'actualizarBeneficio()' :'asignarBeneficio()' }}">
    <i class="fa fa-save"></i> Guardar
  </button>
</div>
  <script>
    function asignarBeneficio() {
      $.post( '{{ route("programas.beneficios.store",$programa_id) }}', { nombre: $('#nombre').val(), predeterminado:$("#default").prop('checked') })
      .done(function( data ) {
        swal('Guardado','se agrego un nuevo beneficio','success')
        $("#modal-ceBeneficio").modal('hide')
        window.LaravelDataTables.beneficios.ajax.reload();
      })
      .fail(function( data ) {
        swal('Error','ocurrio un problema','error')
      });
    }
    @isset($beneficio)
      function actualizarBeneficio() {
        $.ajax({
          url: '{{ route("programas.beneficios.update",[$programa_id,$beneficio->id]) }}',
          type: 'PUT',
          data: { nombre: $('#nombre').val(),  predeterminado:$("#default").prop('checked') },
          success: function (response) {
            swal('Guardado','se agrego un nuevo beneficio','success')
        
            $("#modal-ceBeneficio").modal('hide')
            window.LaravelDataTables.beneficios.ajax.reload();
          }
        });
      }
    @endisset
  </script>