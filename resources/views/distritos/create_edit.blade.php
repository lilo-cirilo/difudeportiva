@extends('bienestar::layouts.master')

@push('head')
<style type="text/css">
  input {
    text-transform: uppercase;
  }
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($distrito) ? 'EDITAR' : 'AGREGAR' }} DISTRITO:</h3>
	</div>
	<div class="box-body">
		<form data-toggle="validator" role="form" id="form_distrito" action="{{isset($distrito) ? '/distritos'.'/'.$distrito->id :'/distritos'}}" method="{{ isset($distrito) ? 'PUT' : 'POST' }}">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
          <label for="nombre">Nombre:</label>
          <input type="text" class="form-control" id="nombre" name="nombre" placeholder="NOMBRE" value="{{ $distrito->nombre or '' }}" onkeyup="toUpperCase(this)">
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
        <div class="form-group">
          <label for="region_id">Region:</label>
          <select id="region_id" name="region_id" class="form-control select2" style="width: 100%;">
            @if(isset($distrito))
            <option value="{{ $distrito->region->id }}" selected>{{ $distrito->region->nombre }}</option>
            @endif
          </select>
        </div>
      </div>
    </form>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="procesarDistrito();"><i class="fa fa-database"></i> Guardar</button>
		<a href="{{ route('distritos.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script type="text/javascript">
  
	$(document).ready(function() {
    
		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			}
		});
    
		$('#form_distrito').validate({
			rules: {
				nombre: {
					required: true,
					minlength: 3,
					pattern_nombre: 'El nombre solo puede contener letras.'
				},
        region_id:{
          required: true
        }
			},
			messages: {
				nombre: {
					required: 'Por favor introdusca un nombre',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
        region_id:{
          required: 'Por favor selecciona una region'
        }
			},
			// submitHandler : function(form) {
        // 	procesarRegion();
        // }
      });
      
      $.validator.addMethod('pattern_nombre', function(value, element) {
        return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
      }, 'Solo letras y espacios entre palabras');
      
      $('#region_id').select2({
        language: 'es',
        ajax: {
          url: '{{ route('regiones.select') }}',
          dataType: 'JSON',
          type: 'GET',
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            params.page = params.page || 1;
            return {
              results: $.map(data, function(item) {
                return {
                  id: item.id,
                  text: item.nombre,
                  slug: item.nombre,
                  results: item
                }
              })
            };
          },
          cache: true
        }
      })
    })
    
    
    function toUpperCase(elemento) {
      javascript:elemento.value = elemento.value.toUpperCase();
    }
    function procesarDistrito() {
      if($('#form_distrito').valid()){
        block();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        
        $.ajax({
          url: $('#form_distrito').attr('action'),
          type: $('#form_distrito').attr('method'),
          data: $('#form_distrito').serialize(),
          //processData: false,
          //contentType: false,
          success: function(response) {
            unblock();
            var re = '{{route('distritos.index')}}'//"{{ URL::to('distritos/show') }}" + '/' + response.id;
            window.location.href = re;// + '?n=' + new Date().getTime();
          },
          error: function(response) {
            unblock();
          }
        });
      }
    }
  </script>
  @endpush
  