@extends('bienestar::layouts.master')

@push('head')
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($persona) ? 'EDITAR' : 'AGREGAR' }} PERSONA</h3>
	</div>
	<div class="box-body">
		@include('personas.iframe.create_edit')
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<div class="pull-right">
			<button type="button" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>
			<a href="{{ route('personas.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
		</div>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

@include('personas.js.persona')

<script type="text/javascript">
	app.to_upper_case();
	app.agregar_bloqueo_pagina();
	
	persona.editar_fotografia();
	persona.agregar_fecha_nacimiento();
	persona.agregar_inputmask();
	persona.agregar_select_create();

	@if(isset($persona))
		persona.agregar_select_edit();
	@endif
	
	persona.agregar_validacion();
</script>

<script type="text/javascript">
	function persona_create_edit_success(response) {
		app.set_bloqueo(false);
		unblock();
		//console.log(response);
		var re = "{{ URL::to('personas/show') }}" + '/' + response.id;
		window.location.href = re;// + '?n=' + new Date().getTime();
	};

	function persona_create_edit_error(response) {
		unblock();
		if(response.status === 422) {
			swal({
				title: 'Error al registrar a la persona.',				
				text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				type: 'error',
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'Regresar',
				allowEscapeKey: false,
				allowOutsideClick: false
			});
		}
	};
</script>
@endpush