<script type="text/javascript">
	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	function persona_create_edit_success(response) {};
	
	function persona_create_edit_error(response) {};

	var persona = (function() {

		var fotografia = $('#fotografia'),
		imagen = $('#imagen'),
		eliminar_imagen = false,
		form = $('#form_persona'),
		fecha_nacimiento = $('#fecha_nacimiento'),
		genero = $('#genero'),
		municipio = $('#municipio_id'),
		localidad = $('#localidad_id'),
		action = form.attr('action'),
		method = form.attr('method');

		function init() {
			fotografia = $('#fotografia'),
			imagen = $('#imagen'),
			eliminar_imagen = false,
			form = $('#form_persona'),
			fecha_nacimiento = $('#fecha_nacimiento'),
			genero = $('#genero'),
			municipio = $('#municipio_id'),
			localidad = $('#localidad_id'),
			action = form.attr('action'),
			method = form.attr('method');
		}

		function agregar_fotografia() {
			fotografia.click();
		};

		function editar_fotografia() {
			fotografia.change(function() {
				var file = this.files[0];
				var reader = new FileReader();
				reader.onloadend = function() {
					imagen.attr('src', reader.result);
				};
				reader.onerror = function() {
				};
				if(file) {
					reader.readAsDataURL(file);
				}
				else {
				}
			});
		};

		function eliminar_fotografia() {
			eliminar_imagen = true;
			fotografia.val('');
			fotografia.trigger('change');
			imagen.attr('src', '{{ asset('images/no-image.png') }}');
		};

		function agregar_fecha_nacimiento() {
			fecha_nacimiento.datepicker({
				autoclose: true,
				language: 'es',
				startDate: '01-01-1900',
				endDate: '0d',
				orientation: 'bottom'
			}).change(function(event) {
				fecha_nacimiento.valid();
			});
		};

		function agregar_inputmask() {
			$('[data-mask]').inputmask();
		};

		function agregar_select_create() {
			municipio.select2({
				language: 'es',
				minimumInputLength: 2,
				ajax: {
					url: '{{ route('municipios.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				municipio.valid();
				localidad.empty();
				localidad.select2({
					language: 'es',
					ajax: {
						url: '{{ route('localidades.select') }}',
						delay: 500,
						dataType: 'JSON',
						type: 'GET',
						data: function(params) {
							return {
								search: params.term,
								municipio_id: municipio.val()
							};
						},
						processResults: function(data, params) {
							params.page = params.page || 1;
							return {
								results: $.map(data, function(item) {
									return {
										id: item.id,
										text: item.nombre,
										slug: item.nombre,
										results: item
									}
								})
							};
						},
						cache: true
					}
				}).change(function(event) {
					localidad.valid();
				});
			});

			localidad.select2({
				language: 'es'
			});

			genero.select2({
				language: 'es',
				minimumResultsForSearch: Infinity
			});
		};

		function agregar_select_edit() {
			localidad.select2({
				language: 'es',
				ajax: {
					url: '{{ route('localidades.select') }}',
					delay: 500,
					dataType: 'JSON',
					type: 'GET',
					data: function(params) {
						return {
							search: params.term,
							municipio_id: municipio.val()
						};
					},
					processResults: function(data, params) {
						params.page = params.page || 1;
						return {
							results: $.map(data, function(item) {
								return {
									id: item.id,
									text: item.nombre,
									slug: item.nombre,
									results: item
								}
							})
						};
					},
					cache: true
				}
			}).change(function(event) {
				localidad.valid();
			});
		};

		function create_edit() {
			if(form.valid()) {

				block();

				var formData = new FormData(form[0]);

				formData.append('imagen', eliminar_imagen);

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: formData,
					processData: false,
					contentType: false,
					success: function(response) {
						persona_create_edit_success(response);
					},
					error: function(response) {
						persona_create_edit_error(response);
					}
				});

			}
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					nombre: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
					primer_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					segundo_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					fecha_nacimiento: {
						required: true
					},
					curp: {
						required: false,
						minlength: 18,
						maxlength: 19,
						pattern_curp: ''
					},
					clave_electoral: {
						required: false,
						minlength: 13,
						pattern_numero: ''
					},
					calle: {
						required: true,
						minlength: 3
					},
					numero_exterior: {
						required: true,
						minlength: 1,
						pattern_numero: ''
					},
					numero_interior: {
						required: false,
						minlength: 1,
						pattern_numero: ''
					},
					colonia: {
						required: true,
						minlength: 3
					},
					codigopostal: {
						required: false,
						minlength: 5,
						maxlength: 5,
						pattern_integer: ''
					},
					municipio_id: {
						required: true
					},
					localidad_id: {
						required: false
					},
					referencia_domicilio: {
						required: true,
						minlength: 3
					},
					numero_celular: {
						required: false,
						pattern_telefono: ''
					},
					numero_local: {
						required: false,
						pattern_telefono: ''
					},
					email: {
						required: false,
						email: true
					}
				},
				messages: {
				}
			});
		};

		function agregar_validacion_atnciudadana() {
			form.validate({
				rules: {
					nombre: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
					primer_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					segundo_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},					
					curp: {
						required: false,
						minlength: 18,
						maxlength: 19,
						pattern_curp: ''
					},
					clave_electoral: {
						required: false,
						minlength: 13,
						pattern_numero: ''
					},					
					municipio_id: {
						required: true
					},
					numero_celular: {
						required: false,
						pattern_telefono: ''
					},
					numero_local: {
						required: false,
						pattern_telefono: ''
					},
					email: {
						required: false,
						email: true
					}
				},
				messages: {
				}
			});
		};

		function agregar_validacion_asistenciaalimentaria() {
			$.validator.addMethod('pattern_curp_alim', function (value, element) {
					return this.optional(element) || /^[A-Z]{1}[AEIOUX]{1}[A-Z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[B-DF-HJ-NP-TV-Z]{3}[0-9A-Z]{1}[0-9]{1}$/.test(value.toUpperCase());
			}, 'Código alfanumérico único de identidad de 18 caracteres.')

			form.validate({
				rules: {
					nombre: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
					primer_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},
					segundo_apellido: {
						required: true,
						minlength: 1,
						pattern_apellido: ''
					},					
					curp: {
						required: true,
						minlength: 18,
						maxlength: 19,
						pattern_curp_alim: ''
					},
					clave_electoral: {
						required: false,
						minlength: 13,
						pattern_numero: ''
					},					
					municipio_id: {
						required: true
					},
					numero_celular: {
						required: false,
						pattern_telefono: ''
					},
					numero_local: {
						required: false,
						pattern_telefono: ''
					},
					email: {
						required: false,
						email: true
					}
				},
				messages: {
				}
			});
		};

		return {
			init: init,
			agregar_fotografia: agregar_fotografia,
			editar_fotografia: editar_fotografia,
			eliminar_fotografia: eliminar_fotografia,
			agregar_fecha_nacimiento: agregar_fecha_nacimiento,
			agregar_inputmask: agregar_inputmask,
			agregar_select_create: agregar_select_create,
			agregar_select_edit: agregar_select_edit,
			create_edit: create_edit,
			agregar_validacion: agregar_validacion,
			agregar_validacion_atnciudadana: agregar_validacion_atnciudadana,
			agregar_validacion_asistenciaalimentaria: agregar_validacion_asistenciaalimentaria
		};
	})();
</script>