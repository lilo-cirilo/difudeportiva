<form data-toggle="validator" role="form" id="form_persona" action="{{ isset($persona) ? URL::to('personas/update') . '/' . $persona->id : URL::to('personas/store') }}" method="{{ isset($persona) ? 'PUT' : 'POST' }}">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
            <div class="hovereffect">
                <img src="{{ asset(isset($persona) ? $persona->get_url_fotografia() : 'images/no-image.png') }}" class="img-thumbnail center-block" id="imagen" name="imagen">
                <div class="overlay">
                    <h2>Acciones</h2>
                    <button type="button" class="btn btn-success" onclick="persona.agregar_fotografia();"><i class="fa fa-database"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" onclick="persona.eliminar_fotografia();"><i class="fa fa-trash"></i> Eliminar</button>
                    <input type="file" id="fotografia" name="fotografia" style="display: none;">
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="nombre">Nombre* :</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $persona->nombre or '' }}">
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="primer_apellido">Apellido Paterno* :</label>
                        <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $persona->primer_apellido or '' }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="segundo_apellido">Apellido Materno* :</label>
                        <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $persona->segundo_apellido or '' }}">
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="genero">Género* :</label>
                        <select id="genero" name="genero" class="form-control select2" style="width: 100%;">
                            <option value="M"
                            @if(isset($persona))
                                @if($persona->genero == 'M')
                                    selected
                                @endif
                            @else
                                selected
                            @endif
                            >MASCULINO</option>
                            <option value="F" {{ (isset($persona) && $persona->genero == 'F') ? 'selected' : '' }}>FEMENINO</option>
                        </select>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="fecha_nacimiento">Fecha de Nacimiento:</label>
                        <input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ (isset($persona) && $persona->fecha_nacimiento) ? $persona->get_formato_fecha_nacimiento() : '' }}">
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 0; padding-right: 0;">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="curp">CURP:</label>
                        <input type="text" class="form-control" id="curp" name="curp" value="{{ $persona->curp or '' }}">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                    <div class="form-group">
                        <label for="clave_electoral">Clave Electoral:</label>
                        <input type="text" class="form-control" id="clave_electoral" name="clave_electoral" value="{{ $persona->clave_electoral or '' }}">
                    </div>
                </div>

            </div>

        </div>

    </div>

    <h4>Dirección:</h4>

    <hr>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label for="calle">Calle:</label>
                <input type="text" class="form-control" id="calle" name="calle" value="{{ $persona->calle or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="form-group">
                <label for="numero_exterior">Número Exterior:</label>
                <input type="text" class="form-control" id="numero_exterior" name="numero_exterior" value="{{ $persona->numero_exterior or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="form-group">
                <label for="numero_interior">Número Interior:</label>
                <input type="text" class="form-control" id="numero_interior" name="numero_interior" value="{{ $persona->numero_interior or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
            <div class="form-group">
                <label for="colonia">Colonia:</label>
                <input type="text" class="form-control" id="colonia" name="colonia" value="{{ $persona->colonia or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2">
            <div class="form-group">
                <label for="codigopostal">Código Postal:</label>
                <input type="text" class="form-control" id="codigopostal" name="codigopostal" value="{{ $persona->codigopostal or '' }}">
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="municipio_id">Municipio* :</label>
                <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
                    @if(isset($persona) && isset($persona->municipio))
                    <option value="{{ $persona->municipio->id }}" selected>({{ $persona->municipio->distrito->region->nombre }}) ({{ $persona->municipio->distrito->nombre }}) {{ $persona->municipio->nombre }}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="localidad_id">Localidad:</label>
                <select id="localidad_id" name="localidad_id" class="form-control select2" style="width: 100%;">
                    @if(isset($persona) && isset($persona->localidad))
                    <option value="{{ $persona->localidad->id }}" selected>{{ $persona->localidad->nombre }}</option>
                    @endif
                </select>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="referencia_domicilio">Referencias del Domicilio:</label>
                <textarea class="form-control" rows="3" id="referencia_domicilio" name="referencia_domicilio">{{ $persona->referencia_domicilio or '' }}</textarea>
            </div>
        </div>

    </div>

    <h4>Contacto:</h4>

    <hr>

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
                <label for="numero_celular">Celular:</label>
                <input type="text" class="form-control" id="numero_celular" name="numero_celular" data-inputmask='"mask": "(999) 999-999-9999"' data-mask value="{{ $persona->numero_celular or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="form-group">
                <label for="numero_local">Télefono:</label>
                <input type="text" class="form-control" id="numero_local" name="numero_local" data-inputmask='"mask": "(999) 999-9999"' data-mask value="{{ $persona->numero_local or '' }}">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="email">Correo Electrónico:</label>
                <input type="text" class="form-control" id="email" name="email" value="{{ $persona->email or '' }}">
            </div>
        </div>

    </div>

</form>