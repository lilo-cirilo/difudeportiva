@extends('bienestar::layouts.master')

@push('head')
<style type="text/css">
    input {
	       text-transform: uppercase;
       }
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($localidad) ? 'EDITAR' : 'AGREGAR' }} LOCALIDAD:</h3>
	</div>
	<div class="box-body">
		<form data-toggle="validator" role="form" id="form_localidad" action="{{isset($localidad) ? '/localidades'.'/'.$localidad->id :'/localidades'}}" method="{{ isset($localidad) ? 'PUT' : 'POST' }}">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="NOMBRE" value="{{ $localidad->nombre or '' }}" onkeyup="toUpperCase(this)">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="municipio_id">Municipio:</label>
                    <select id="municipio_id" name="municipio_id" class="form-control select2" style="width: 100%;">
                        @if(isset($localidad))
                        <option value="{{ $localidad->municipio->id }}" selected>{{ $localidad->municipio->nombre }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="nombre">Latitud:</label>
                    <input type="text" class="form-control" id="latitud" name="latitud" placeholder="LATITUD" value="{{ $localidad->latitud or '' }}" onkeyup="toUpperCase(this)">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="nombre">Longitud:</label>
                    <input type="text" class="form-control" id="longitud" name="longitud" placeholder="LONGITUD" value="{{ $localidad->longitud or '' }}" onkeyup="toUpperCase(this)">
                </div>
            </div>
        </form>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="procesarLocalidad();"><i class="fa fa-database"></i> Guardar</button>
		<a href="{{ route('localidades.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {

		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			}
		});

		$('#form_localidad').validate({
			rules: {
				nombre: {
					required: true,
					minlength: 3,
					pattern_nombre: 'El nombre solo puede contener letras.'
				},
                municipio_id:{
                    required: true
                },
                latitud:{
                    required: true
                },
                longitud:{
                    required: true
                }
			},
			messages: {
				nombre: {
					required: 'Por favor introdusca un nombre',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				},
                municipio_id:{
                    required: 'Por favor selecciona un distrito'
                },
                latitud:{
                    required: 'Por favor ingresa la latitud'
                },
                longitud:{
                    required: 'Por favor ingresa la longitud'
                }
			},
			// submitHandler : function(form) {
			// 	procesarMunicipio();
			// }
		});

		$.validator.addMethod('pattern_nombre', function(value, element) {
			return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
		}, 'Solo letras y espacios entre palabras');

        $('#municipio_id').select2({
    		language: 'es',
    		ajax: {
    			url: '{{ route('municipios.select') }}',
    			dataType: 'JSON',
    			type: 'GET',
    			data: function(params) {
    				return {
    					search: params.term
    				};
    			},
    			processResults: function(data, params) {
    				params.page = params.page || 1;
    				return {
    					results: $.map(data, function(item) {
    						return {
    							id: item.id,
    							text: item.nombre,
    							slug: item.nombre,
    							results: item
    						}
    					})
    				};
    			},
    			cache: true
    		}
    	})
	})


    function toUpperCase(elemento) {
		javascript:elemento.value = elemento.value.toUpperCase();
	}
    function procesarLocalidad() {
        if($('#form_localidad').valid()){
            block();
            $.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

            $.ajax({
				url: $('#form_localidad').attr('action'),
    			type: $('#form_localidad').attr('method'),
				data: $('#form_localidad').serialize(),
				//processData: false,
				//contentType: false,
				success: function(response) {
					unblock();
					var re = '{{route('localidades.index')}}'//"{{ URL::to('municipios/show') }}" + '/' + response.id;
				window.location.href = re;
				},
				error: function(response) {
					unblock();
				}
			});
        }
    }
</script>
@endpush
