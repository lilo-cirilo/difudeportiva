@extends('bienestar::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
/*.form-control.input-sm {
	width: 100%;
}*/
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">LOCALIDADES TOTALES DE OAXACA:</h3>
		<div class="pull-right">
            <a href="{{ route('localidades.create') }}" class="btn btn-success"><i class="fa fa-plus "></i> Agregar</a>
        </div>
	</div>
	<div class="box-body">
		<!-- /.box-header -->
		<div class="input-group input-group-sm col-lg-3 pull-right">
        	<input type="text" id="search" class="form-control" placeholder="Buscar" >
        	<span class="input-group-btn">
        		<button type="submit" class="btn btn-info btn-flat" id="btn_buscar">Buscar</button>
        	</span>
        </div>
		<!-- table start -->
		<table id="localidades" class="table table-bordered table-striped dataTable">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Municipio</th>
                    <th>Latitud</th>
                    <th>longitud</th>
                    <th>Acciones</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
                    <th>Nombre</th>
					<th>Municipio</th>
                    <th>Latitud</th>
                    <th>longitud</th>
                    <th>Acciones</th>
                </tr>
			</tfoot>
		</table>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
	var table = $('#localidades').DataTable({
		responsive  : true,
        autoWidth   : true,
        processing	: true,
        serverSide	: true,
        pagingType	: 'simple_numbers',
        paging      : true,
        lengthChange: true,
        ordering    : true,
        info        : true,
        scrollY		: 320,
        searching	: true,
        dom			: "ltip",
        lengthMenu	: [ 10, 25, 50, 100, 500],
        language	: {
            'url': "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
        },
		ajax        : {
			'url': "<?= route('localidades.datatable') ?>",
			'dataType': 'JSON',
			'type': 'GET',
			'data': { '_token': $('meta[name="csrf-token"]').attr('content') }
		},
		columns:[
			{ 'data': 'Nombre' },
			{ 'data': 'Municipio' },
			{ 'data': 'Latitud' },
			{ 'data': 'Longitud' },
			{ 'data': 'Acciones', 'searchable': false, 'orderable': false }
		]
	});
	$('#btn_buscar').on( 'click', function () {
		//table.search( $('#search').val(),false,false).draw();
		table.column(0).search($('#search').val(), false, false).draw();
	});
	/*function candidato_edit(persona_id) {
		console.log('Edit: ' + persona_id);
	}*/
    function localidad_delete(localidadd_id) {
		swal({
			title: '¿Estas seguro?',
			text: '¡Usted no podrá recuperar este registró!',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#28a745',
			cancelButtonColor: '#dc3545',
			cancelButtonText: 'NO',
			confirmButtonText: 'SI'
		}).then((result) => {
			if(result.value) {
				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: "{{route('localidades.index')}}" + '/' + localidadd_id,
					type: 'DELETE',
					success: function(response) {
						table.ajax.reload();

						unblock();

						swal({
							title: '¡Eliminado!',
							text: 'Su registro ha sido eliminado..',
							type: 'success',
							timer: 1500
						});

					},
					error: function(response) {

						unblock();

					}
				});

			}
		});

	}
</script>
@endpush
