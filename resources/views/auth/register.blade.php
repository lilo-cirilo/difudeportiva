@extends('admin-lte::layouts.auth')

@push('head')
<!-- Select2 -->
<!-- Select2 -->
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<!-- Theme style -->
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<style>
  .login-box{
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    width: unset;
    margin: 0;
  }
    main {
        flex: 1 0 auto;
        align-items: center;
        justify-content: center;
        display: flex;
    }
    .login-logo, .login-box-body{
        max-width: 470px;
      }


  {{--  
  .login-logo{
    width: 50%;
  }
.select2-container--default .select2-selection--single{
  border-radius: 0px;
}
.select2-container .select2-selection--single{
  height: 34px;
}
.tab-pane{
  overflow-y: scroll;
}
::-webkit-scrollbar {
    display: none;
}  --}}
</style>
@endpush

@section('content')
<div class="login-box-body" style="box-shadow: 1px 1px 15px rgba(0, 0, 0, 0.50);">
  <p class="login-box-msg" style="font-size: 16px;font-weight: bold;">Registrate a Intranet DIF</p>

  @if(isset($msg))
  <p>{{ $msg }}</p>
  @endif

  <form action="{{ route('register') }}" method="POST">
    {{ csrf_field() }}

    <div class="row">

        <div class="col-sm-12 col-md-12">
          <div class="form-group has-feedback{{ $errors->has('nombre') ? ' has-error' : '' }}">
            <input id="nombre" type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" placeholder="Nombre" required autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('nombre'))
            <span class="help-block">{{ $errors->first('nombre') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group has-feedback{{ $errors->has('primer_apellido') ? ' has-error' : '' }}">
            <input id="primer_apellido" type="text" class="form-control" name="primer_apellido" value="{{ old('primer_apellido') }}" placeholder="Primer apellido" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('primer_apellido'))
            <span class="help-block">{{ $errors->first('primer_apellido') }}</span>
            @endif
          </div>
        </div>
        <div class="col-sm-12 col-md-6">
          <div class="form-group has-feedback{{ $errors->has('segundo_apellido') ? ' has-error' : '' }}">
            <input id="segundo_apellido" type="text" class="form-control" name="segundo_apellido" value="{{ old('segundo_apellido') }}" placeholder="Segundo apellido" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('segundo_apellido'))
            <span class="help-block">{{ $errors->first('segundo_apellido') }}</span>
            @endif
          </div>
        </div>
  
      </div>

    <div class="row">

    <div class="col-sm-12 col-md-12">
        <div class="form-group has-feedback{{ $errors->has('rfc') ? ' has-error' : '' }}">
            <input id="rfc" type="text" class="form-control" name="rfc" value="{{ old('rfc') }}" placeholder="RFC" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
            @if ($errors->has('rfc'))
            <span class="help-block">{{ $errors->first('rfc') }}</span>
            @endif
        </div>
    </div>  




            <div class="col-sm-12 col-md-12">
              <div class="form-group has-feedback{{ $errors->has('etnia_id') ? ' has-error' : '' }}">
                <select id="etnia_id" class="form-control select2" name="etnia_id" required>
                  @foreach($etnias as $etnia)
                    <option value="{{$etnia->id}}">{{$etnia->nombre}}</option>
                  @endforeach
                </select>
                @if ($errors->has('etnia_id'))
                  <span class="help-block">{{ $errors->first('etnia_id') }}</span>
                @endif
              </div>
            </div>
      

      <div class="col-sm-12 col-md-12">
        <div class="form-group has-feedback{{ $errors->has('area_id') ? ' has-error' : '' }}">
          <select name="area_id" id="area_id" class="form-group select2">
            {{-- <option value="#">Busca tu área</option> --}}
          </select>
          @if ($errors->has('area_id'))
          <span class="help-block">{{ $errors->first('area_id') }}</span>
          @endif
        </div>
      </div>

      <div class="col-sm-12 col-md-12">
            <div class="form-group has-feedback{{ $errors->has('nivel_id') ? ' has-error' : '' }}">
              <select id="nivel_id" class="form-control select2" name="nivel_id" required>
                @foreach($niveles as $nivel)
                  <option value="{{$nivel->id}}">{{$nivel->nivel}} ({{$nivel->clave}})</option>
                @endforeach
              </select>
              @if ($errors->has('nivel_id'))
                <span class="help-block">{{ $errors->first('nivel_id') }}</span>
              @endif
            </div>
          </div>
  
          <div class="col-sm-12 col-md-12">
            <div class="form-group has-feedback{{ $errors->has('tiposempleado_id') ? ' has-error' : '' }}">
              <select id="tiposempleado_id" class="form-control select2" name="tiposempleado_id" required>
                @foreach($tipos as $tipo)
                  <option value="{{$tipo->id}}">{{$tipo->tipo}}</option>
                @endforeach
              </select>
              @if ($errors->has('tiposempleado_id'))
                <span class="help-block">{{ $errors->first('tiposempleado_id') }}</span>
              @endif
            </div>
          </div>
  
    </div>


    <div class="row">

      <div class="col-sm-12 col-md-12">
        <div class="form-group has-feedback{{ $errors->has('usuario') ? ' has-error' : '' }}">
          <input id="usuario" type="text" class="form-control" name="usuario" value="{{ old('usuario') }}" placeholder="Usuario" required style="text-transform: none;">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
          @if ($errors->has('usuario'))
          <span class="help-block">{{ $errors->first('usuario') }}</span>
          @endif
        </div>
      </div>

    </div>

    <div class="row">

      <div class="col-sm-12 col-md-6">
        <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
          <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required style="text-transform: none;">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          @if ($errors->has('password'))
          <span class="help-block">{{ $errors->first('password') }}</span>
          @endif
        </div>
      </div>
      
      <div class="col-sm-12 col-md-6">
        <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmar" required style="text-transform: none;">
          <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
        </div>
      </div>

      <div class="col-sm-12 col-md-12">
            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail" required style="text-transform: none;" >
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
              @if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
              @endif
            </div>
          </div>

    </div>

    <div class="row">
        <div class="col-md-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Registrarse</button>
        </div>

    </div>

  </form>

  {{-- <div class="social-auth-links text-center">
    <p>- OR -</p>
    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
    Facebook</a>
    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
    Google+</a>
  </div> --}}
<br>
  <div class="row">
      <div class="col-xs-12" style="text-align: center;">
      <a href="{{ route('login') }}" style="color: #db3762;" class="hover-dif"><i class="fa fa-address-card"></i> Ya Tengo Una Cuenta</a>
      </div>
      <!-- /.col -->
    </div>
</div>
<!-- /.form-box -->
@endsection

@push('body')
<script src="/bower_components/select2/select2/select2.min.js"></script>
<script src="/js/vue-multiselect.min.js"></script>
<script>
  $('#area_id').select2({
    placeholder: 'BUSCA TU ÁREA',
    ajax: {
      url: '{{url('/areas/select2')}}',
      dataType: 'json',
      // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
      processResults: function (data) {
      // Tranforms the top-level key of the response object from 'items' to 'results'
        return {
          results: data
        };
      }
    }
  });
  $('#etnia_id').select2({
    placeholder: 'ETNIA'
  });
  $('#etnia_id').val(null).trigger('change');
  $('#nivel_id').select2({
    placeholder: 'CARGO'
  });
  $('#nivel_id').val(null).trigger('change');
  $('#tiposempleado_id').select2({
    placeholder: 'CONTRATO'
  });
  $('#tiposempleado_id').val(null).trigger('change');
</script>
@endpush