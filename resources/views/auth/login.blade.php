@extends('admin-lte::layouts.auth')

@section('content')

<div class="login-box-body" style="box-shadow: 1px 1px 15px rgba(0, 0, 0, 0.50);">
  <p class="login-box-msg" style="font-size: 16px;font-weight: bold;">Inicia Sesión</p>
  
  @isset($status)
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box">
      <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ $msg }}</span>
        <span class="info-box-number">{{ $status }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  @endisset
  <form action="{{ route('login') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group has-feedback{{ $errors->has('usuario') ? ' has-error' : '' }}">
      <input id="usuario" type="usuario" class="form-control" name="usuario" value="{{ old('usuario') }}" placeholder="Usuario" required autofocus>
      <span class="glyphicon glyphicon-user form-control-feedback"></span>
      @if ($errors->has('usuario'))
      <span class="help-block">{{ $errors->first('usuario') }}</span>
      @endif
    </div>
    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
      <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      @if ($errors->has('password'))
      <span class="help-block">{{ $errors->first('password') }}</span>
      @endif
    </div>
    <div class="row">
      <div class="col-xs-8">
        {{--<div class="checkbox icheck">
          <label>
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} value="1"> Recordar
          </label>
        </div>--}}
        <a href="{{url('password/reset')}}" style="color: #db3762;" class="hover-dif">Olvidé mi contraseña</a>
      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar</button>
      </div>
      <!-- /.col -->
    </div>
  </form>
  

{{--
  <div class="social-auth-links text-center">
    <p>- OR -</p>
    <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
    Facebook</a>
    <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
    Google+</a>
  </div>
  <!-- /.social-auth-links -->
  --}}
  <br>
  <div class="row">
        <div class="col-xs-12 col-md-6" style="text-align: center;">
            <a href="/DT_JULIO2019.pdf" class="text-center hover-dif-2" style="font-size: 15px;color:#001d56;" download><i class="fa fa-download"></i> Directorio</a>
        </div>
        <div class="col-xs-12 col-md-6" style="text-align: center;">
          {{-- href="{{ route('register') }}"  --}}
            <a class="text-center hover-dif-2" style="font-size: 15px;color:#001d56;"><i class="fa fa-user-plus"></i> Registrarme</a>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Contacto</h4>
      </div>
      <div class="modal-body">
        <p>Comunícate con el Administrador del Sistema.</p>
        <strong><i class="fa fa-black-tie"></i>  Ing.Luis Alberto Vásquez Pérez</strong><br>
        <i class="fa fa-envelope"></i> dif.sistemas@oaxaca.gob.mx <br>
        <i class="fa fa-phone"></i> 501-50-71
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

@endsection
