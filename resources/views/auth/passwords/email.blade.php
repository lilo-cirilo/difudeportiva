@extends('admin-lte::layouts.auth')

@section('content')
@if (session('status'))
<div class="callout callout-info">
  {{ session('status') }}
</div>
@endif

<div class="login-box-body" style="box-shadow: 3px 3px 25px rgba(0, 0, 0, 0.50);">
  <p class="login-box-msg" style="font-size: 16px;font-weight: bold;">Cambiar Contraseña</p>
  <p class="text-justify" style="color: #db3762;">Ingresa tu email, te enviaremos un correo para cambiar la contraseña.</p>
  <form action="{{ route('password.email') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('email'))
      <span class="help-block">{{ $errors->first('email') }}</span>
      @endif
    </div>
    <div class="row">
      <div class="col-md-12">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Cambiar Contraseña</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

</div>
<!-- /.login-box-body -->
@endsection
