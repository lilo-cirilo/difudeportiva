<form data-toggle="validator" role="form" id="form_producto" action="{{ isset($producto) ? URL::to($modulo.'/productos') . '/' . $producto->id : URL::to($modulo.'/productos') }}" method="{{ isset($producto) ? 'PUT' : 'POST' }}">              
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label for="producto">Nombre* :</label>
                <input type="text" class="form-control" id="producto" name="producto" placeholder="producto" value="{{isset($producto) ? $producto->producto : ''}}">
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="unidadmedida_id">Unidad de medida* :</label>
                <select class="form-control select2" id="unidadmedida_id" name="unidadmedida_id" style="width: 100%;">
                    @foreach ($unidades as $unidad)
                        @if (isset($producto) && $producto->unidadmedida_id == $unidad->id)
                            <option value="{{$unidad->id}}" selected>{{$unidad->unidadmedida}}</option>
                        @else
                            <option value="{{ $unidad->id  }}">{{ $unidad->unidadmedida }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="form-group">
                <label for="vigencia">Vida útil* :</label>
                <input type="text" class="form-control" id="vigencia" name="vigencia" placeholder="VIDA ÚTIL" value="{{isset($producto) ? $producto->vigencia : ''}}">
            </div>
        </div>

        @if($modulo == 'afuncionales')
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="form-group">
                    <label for="programa_id">Programa responsable* :</label>
                    <select class="form-control select2" id="programa_id" name="programa_id" style="width: 100%;">
                    @if(isset($programa) && $programa != null)
                        <option value="{{ $programa->id }}" selected>{{ $programa->nombre }}</option>
                    @endif
                    </select>
                </div>
            </div>
        @endif
    </div>
</form>