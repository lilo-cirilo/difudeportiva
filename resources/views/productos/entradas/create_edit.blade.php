@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">

@endpush

@section('content-subtitle', 'Registrar Entrada de Productos')

@section('li-breadcrumbs')
    <li> <a href="{{ route($modulo.'.productos.entradas.index') }}">Entradas</a></li>
    @if (isset($entrada))
        <li class="active">Editar entrada</li>
    @else
        <li class="active">Nueva entrada</li>
    @endif
@endsection

@section('content')

    <div class="box box-primary shadow">            
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <form id="form-entrada">                                                
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="fechaentrada">Fecha de entrada* :</label>               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="fechaentrada" name="fechaentrada" value="{{ isset($entrada) ? $entrada->get_formato_fecha() : '' }}">
                                    </div>
                                </div>
                            </div>                                

                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                <div class="form-group">
                                    <label for="tipoentrada_id">Tipo de entrada* :</label>               
                                    <select id="tipoentrada_id" class="form-control select2"  name="tipoentrada_id" style="width: 100%;">
                                        @foreach($tipos_entrada as $tipo)
                                            <option {{ (isset($entrada) && $entrada->tiposentrada_id === $tipo->id) ? 'selected' : '' }} value="{{$tipo->id}}">{{ $tipo->tipo }}</option>
                                        @endforeach
                                    </select>                                                        
                                </div>
                            </div>       
                            
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="empleado_id">Empleado que recibe* :</label>
                                    <div class="input-group">
                                        <select id="empleado_id" name="empleado_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
                                            @if(isset($entrada))
                                                <option selected value="{{ $entrada->empleado_id }}">{{ $entrada->empleado->persona->get_nombre_completo() }}</option>
                                            @endif
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar empleado" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-empleados');">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                <div class="form-group">
                                    <label for="remitente">Tipo de remitente* :</label>               
                                    <select id="remitente" class="form-control select2"  name="remitente" style="width: 100%;">                                                                
                                        <option value="" selected></option>
                                        <option {{ (isset($entrada) && $entrada->entradasproductospersona) ? 'selected' : '' }} value="PERSONAL">PERSONAL</option>
                                        <option {{ (isset($entrada) && $entrada->entradasproductosdependencia) ? 'selected' : '' }} value="INSTITUCIONAL">INSTITUCIONAL</option>                                                                
                                    </select>                                                        
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="fila_dependencia" name="fila_dependencia" style="{{ (isset($entrada) && $entrada->entradasproductosdependencia) ? '' : 'display: none;' }}">
                                <div class="form-group">
                                    <label>Institución o dependencia* :</label>                                        
                                    <div class="input-group">
                                        <select id="dependencia_id" name="dependencia_id" class="form-control select2" style="width: 100%;">
                                            @if (isset($entrada) && $entrada->entradasproductosdependencia)
                                                <option value="{{$entrada->entradasproductosdependencia->dependencia_id}}">{{$entrada->entradasproductosdependencia->dependencia->nombre}}</option>
                                            @endif
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Nueva dependencia" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-dependencia');">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8" id="fila_persona" name="fila_persona" style="{{ (isset($entrada) && $entrada->entradasproductospersona) ? '' : 'display: none;' }}">
                                <div class="form-group">
                                    <label>Remitente* :</label>                                        
                                    <div class="input-group">
                                        <select id="persona_id" name="persona_id" class="form-control input-persona" style="width: 100%;" readonly="readonly">
                                            @if (isset($entrada) && $entrada->entradasproductospersona)
                                                <option value="{{$entrada->entradasproductospersona->persona_id}}">{{$entrada->entradasproductospersona->persona->get_nombre_completo()}}</option>
                                            @endif
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Buscar persona" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-personas');">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                <div class="form-group">
                                        <label for="areas_producto_id">Producto* :</label>
                                    <div class="form-group">
                                        <select id="areas_producto_id" class="form-control select2"  name="areas_producto_id" style="width: 100%;"></select>                                           
                                        {{--  <div class="input-group-btn">
                                            <button type="button" class="btn btn-info" data-toggle="tooltip" title="Nuevo producto" style="padding-left: 25px; padding-right: 25px;" onclick="abrir_modal('modal-producto');">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div>  --}}
                                    </div>
                                </div>
                            </div>                                        
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                                <div class="form-group">
                                    <label for="cantidad">Cantidad* :</label>  
                                    <input type="text" id="cantidad" name="cantidad" class="form-control text-center">                                                                
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
                                <div class="form-group">
                                    <label for="precio">Precio Unitario* :</label>  
                                    <input type="text" id="precio" name="precio" class="form-control text-center">                                                                
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-2">
                                <div class="form-group">
                                    <label for="" style="color:white">Agregar:</label>
                                    <button type="button" class="btn btn-block btn-success" onclick="agregar_producto()">
                                        <i class="fa fa-plus"></i> Agregar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header with-border">
                <h4 class="box-title">Lista de productos</h4>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">CANTIDAD</th>
                            <th class="text-center">PRODUCTO</th>
                            <th class="text-center">PRECIO</th>
                            <th class="text-center">OPCIONES</th>
                        </tr>
                    </thead>
                    <tbody id="tblProductos">
                        @if(isset($entrada))
                            @if($modulo === 'afuncionales')
                                @foreach($entrada->detalleentradasproductos as $detalle)
                                    <tr data-utilizados="{{ $detalle->productosfoliados->where('detallessalidas_producto_id','!=',null)->count() }}" data-disponibles="{{ $detalle->productosfoliados->where('detallessalidas_producto_id',null)->count() }}" data-area-producto="{{ $detalle->areasproducto->id }}">           
                                        <td class="text-center">{{ $detalle->cantidad }}</td>
                                        <td class="text-center">{{$detalle->areasproducto->producto->producto}}</td>
                                        <td class="text-center">{{ $detalle->precio }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-danger btn-sm" onclick="eliminarProducto(this, true,'{{ $detalle->id }}')">
                                                <i class="fa fa-remove"></i>
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @foreach($entrada->detalleentradasproductos as $detalle)
                                    <tr data-area-producto="{{ $detalle->areasproducto->id }}" data-cantidad="{{ $detalle->cantidad }}" data-precio="{{ $detalle->precio }}" data-producto="{{ $detalle->areasproducto->producto->producto }}">           
                                        <td class="text-center">{{ $detalle->cantidad }}</td>             
                                        <td class="text-center">{{ $detalle->areasproducto->producto->producto }}</td>
                                        <td class="text-center">
                                            <button class="btn btn-warning btn-sm" onclick="editarProducto(this)">
                                                <i class="fa fa-pencil"></i>
                                                Editar
                                            </button>
                                            <button class="btn btn-danger btn-sm" onclick="eliminarProducto(this)">
                                                <i class="fa fa-remove"></i>
                                                Eliminar
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endif
                    </tbody>
                </table>
            </div>
        </div>        
        <div class="box-footer">
            <div class="pull-right">
                <button type="button" class="btn btn-md btn-success pull-right" onclick="agregar_entrada()">
                    <i class="fa fa-database"></i> Guardar</button>
            </div>      
        </div>
    </div>

<div class="modal fade" id="modal-personas">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-personas')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-personas">Tabla Personas:</h4>
            </div>
            <div class="modal-body" id="modal-body-personas">            
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                    </span>
                </div>            
                {!! $pdt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
            </div>
            <div class="modal-footer" id="modal-footer-personas">
                <div class="pull-right">
                    <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>
                    <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-personas')"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>      
        </div>
    </div>
</div>


<div class="modal fade" id="modal-empleados">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-empleados')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-empleados">Tabla Empleados:</h4>
            </div>
            <div class="modal-body" id="modal-body-empleados">            
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_empleado" name="search" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_empleado" name="btn_buscar_empleado">Buscar</button>
                    </span>
                </div>            
                
                {!! $edt->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'empleados', 'name' => 'empleados', 'style' => 'width: 100%']) !!}

            </div>
            <div class="modal-footer" id="modal-footer-empleados">
                <div class="pull-right">                    
                    <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-empleados')"><i class="fa fa-remove"></i> Cerrar</button>
                </div>
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-dependencia">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-dependencia')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-dependencia">Registrar dependencia o institución</h4>
            </div>
            <div class="modal-body" id="modal-body-dependencia"></div>
            <div class="modal-footer" id="modal-footer-dependencia">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-dependencia')"><i class="fa fa-close"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="dependencia.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-producto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="cerrar_modal('modal-producto')" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Registrar producto</h4>
            </div>
            <div class="modal-body" id="modal-body-producto"></div>
            <div class="modal-footer" id="modal-footer-producto">
                <button type="button" class="btn btn-danger" onclick="cerrar_modal('modal-producto')"><i class="fa fa-close"></i> Cerrar</button>
                <button type="button" class="btn btn-success" onclick="producto.create_edit()"><i class="fa fa-database"></i> Guardar</button>    
            </div>      
        </div>
    </div>
</div>

<div class="modal fade" id="modal-persona">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-persona"></h4>
            </div>
            <div class="modal-body" id="modal-body-persona"></div>
            <div class="modal-footer" id="modal-footer-persona"></div>      
        </div>
    </div>
</div>

@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
{!! $edt->html()->scripts() !!}
{!! $pdt->html()->scripts() !!}
@include('dependencias.js.dependencia')
@include('productos.js.producto')
@include('personas.js.persona')

<script type="text/javascript">
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    app.to_upper_case();
	app.agregar_bloqueo_pagina();
    var modal_persona = $('#modal-persona');
    var modal_title_persona = $('#modal-title-persona');
    var modal_body_persona = $('#modal-body-persona');
    var modal_footer_persona = $('#modal-footer-persona');
    var personas = $('#personas');
    var btn_buscar = $('#btn_buscar');
    var search = $('#search');
    var en_edicion = false;

    function init_modal_empleado() {
		var table = $('#empleados').dataTable();
		datatable_empleados = $(table).DataTable();

		$('#search_empleado').keypress(function(e) {
			if(e.which === 13) {
				datatable_empleados.search($('#search_empleado').val()).draw();
			}
		});

		$('#btn_buscar_empleado').on('click', function() {
			datatable_empleados.search($('#search_empleado').val()).draw();
		});

		$('#modal-empleados').on('shown.bs.modal', function(event) {
			datatable_empleados.ajax.reload();
		});
	}

    function init_modal_persona() {
		var table = $('#personas').dataTable();
		datatable_personas = $(table).DataTable();

		search.keypress(function(e) {
			if(e.which === 13) {
				datatable_personas.search(search.val()).draw();
			}
		});

		btn_buscar.on('click', function() {
			datatable_personas.search(search.val()).draw();
		});

		$('#modal-personas').on('shown.bs.modal', function(event) {
			datatable_personas.ajax.reload();
		});
	}

    function seleccionar_persona(id, nombre) {
		$("#persona_id").html("<option value='" + id + "'selected>" + nombre + "</option>");
        $("#persona_id").valid();
		$('#modal-personas').modal('hide');		
	};

    function seleccionar_empleado(id, nombre) {
		$("#empleado_id").html("<option value='" + id + "'selected>" + nombre + "</option>");
        $("#empleado_id").valid();
		$('#modal-empleados').modal('hide');		
	};

    $("#remitente").change(function() {
		if($(this).val() === 'PERSONAL') {
			$('#persona_id').rules('add', { required: true });            
            $('#dependencia_id').rules('remove', 'required');
		    $("#fila_persona").show();
            $("#fila_dependencia").removeAttr('style').hide();
		}
		if($(this).val() === 'INSTITUCIONAL') {
			$('#persona_id').rules('remove', 'required');
            $('#dependencia_id').rules('add', { required: true });
			$("#fila_dependencia").show();
            $("#fila_persona").removeAttr('style').hide();
		}
	});

    function agregar_entrada(){
        $('#persona_id').rules('add', { required: true });
        $('#fechaentrada').rules('add', { required: true });
        $('#tipoentrada_id').rules('add', { required: true });
        $('#empleado_id').rules('add', { required: true });
        $('#remitente').rules('add', { required: true });
        $('#areas_producto_id').rules('remove', 'required');
        $('#cantidad').rules('remove', 'required');
        $('#precio').rules('remove', 'required');
        if($('#form-entrada').valid()){
            if($("#tblProductos tr").length == 0){              
                swal(
                    'Error',
                    'Aún no ha registrado productos',
                    'error'
                )
                return;
            }

            var url = '{{ route($modulo.'.productos.entradas.store')}}';
            var type = 'POST';
            var title = 'Entrada registrada';
            @if(isset($entrada))
                url = '{{ route($modulo.'.productos.entradas.update', $entrada->id)}}'; 
                type = 'PUT';
                title = 'Entrada actualizada';
            @endif

            block();
            $.ajax({
                url: url,
                type: type,
                data: getEntrada(),
                success: function(response) {
                    unblock();
                    swal({
                        type: 'success',
                        title: '¡Correcto!',
                        text: title,
                        showConfirmButton: false,
                        timer: 2000,
                        allowEscapeKey: false,
				        allowOutsideClick: false
                    }).then((result) => {
                        app.set_bloqueo(false);
                        if (result.dismiss === swal.DismissReason.timer) {
                            block();
                            location.href = "{{route($modulo.'.productos.entradas.index')}}";
                        }
                    })
                },
                error: function(response) {                    
                    unblock();
                    if(response.status === 500) {
                        swal(
                            'Error ' + response.status,
                            response.responseJSON.message,
                            'error'
                        )
                    }else if(response.status === 422) {
                        console.log(response);
                        
                        swal(
                            'Error',
                            response.responseJSON.errors[0].message,
                            'error'
                        )
                    }      
                }
            });
        }
    }

    function getEntrada(){
        var entrada = {};
        entrada.fechaentrada = $("#fechaentrada").val();
        entrada.tiposentrada_id = $("#tipoentrada_id").val();
        entrada.empleado_id = $("#empleado_id").val();
        entrada.productos = [];
        if($("#remitente").val() === "PERSONAL"){
            entrada.persona_id = $("#persona_id").val();
        }else{
            entrada.dependencia_id = $("#dependencia_id").val();
        }
        for(var i=0; i<$("#tblProductos tr").length; i++){
            entrada.productos.push({
                cantidad: $("#tblProductos tr").eq(i).children().eq(0).text(),
                precio: $("#tblProductos tr").eq(i).children().eq(2).text(),
                area: $("#tblProductos tr").eq(i).attr("data-area-producto")
            });
        }
        return entrada;
    }

    function seleccionar_persona(id, nombre) {
		$("#persona_id").html("<option value='" + id + "'selected>" + nombre + "</option>");
		$('#modal-personas').modal('hide');		
	};

    function dependencia_create_edit_success(response) {
		app.set_bloqueo(false);
		unblock();
        cerrar_modal('modal-dependencia');
        var option = new Option(response.nombre, response.id, true, true);
        $("#dependencia_id").append(option).trigger('change');        
	};

	function dependencia_create_edit_error(response) {
		unblock();
    };
    
    function producto_create_edit_success(response) {
		app.set_bloqueo(false);
		unblock();
        cerrar_modal('modal-producto');
        var option = new Option(response.producto.producto, response.areaproducto_id, true, true);
        $("#areas_producto_id").append(option).trigger('change');   
	};

	function producto_create_edit_error(response) {
		unblock();
	};

    function abrir_modal(modal) {
        if(modal === "modal-dependencia"){
            $.get('/dependencias/create', function(data) {            
                $('#modal-body-dependencia').html(data.html);                            
                dependencia.init();                
                $('#modal-dependencia').modal('show');            
            });
        } else if(modal === "modal-producto"){
            $.get('{{route($modulo.'.productos.create')}}', function(data) {            
                $('#modal-body-producto').html(data.html);                            
                producto.init();                
                $('#modal-producto').modal('show');            
            });
        } else{
            $("#"+modal).modal('show');
        }		
	};

    function cerrar_modal(modal){
        $("#"+modal).modal("hide");
    }

    $('[data-toggle="tooltip"]').tooltip();    

    $('#tipoentrada_id, #remitente').select2({
        language: 'es',
        placeholder: 'SELECCIONE UNA OPCIÓN',
        minimumResultsForSearch: Infinity
    }).change(function(event){
        $(this).valid();
    });


    $('#tipoentrada_id').change(function() {
		if($(this).val() === 'NO') {
			$('#persona_id').rules('remove', 'required');
			fila_dependencia.removeAttr('style').hide();
        }
		if($(this).val() === 'SI') {
			$('#persona_id').rules('add', { required: true });
			fila_dependencia.show();
		}
	});

    $('#fechaentrada').datepicker({
        autoclose: true,
        language: 'es',
        format: 'dd-mm-yyyy',
        startDate: '01-01-1900',
		endDate: '0d'
    }).change(function(event) {
		$("#fechaentrada").valid();	
    });;

    $("#areas_producto_id").select2({
		language: 'es',
        placeholder: 'BUSQUE UN PRODUCTO',
		ajax: {
            delay: 500,
			url: '{{ route($modulo.'.productos.select') }}',
			dataType: 'JSON',
			type: 'GET',
			data: function(params) {
				return {
          search: params.term,
          page : params.page || 1
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: $.map(data.data, function(item) {
						return {
							id: item.id,
							text: item.nombre.replace(/ENTREGA DE/g, '').trim(),
							slug: item.nombre.replace(/ENTREGA DE/g, '').trim(),
							results: item
						}
					}),
          pagination: {
              more : data.current_page < data.last_page
          }
				};
			},
			cache: true
		}
	}).change(function(event) {
		$("#areas_producto_id").valid();	
    });


    $("#dependencia_id").select2({
		language: 'es',
        placeholder: 'BUSQUE O AGREGUE UNA NUEVA DEPENDENCIA',
		ajax: {
            delay: 500,
			url: '{{ route('dependencias.select') }}',
			dataType: 'JSON',
			type: 'GET',
			data: function(params) {
				return {
					search: params.term
				};
			},
			processResults: function(data, params) {
				params.page = params.page || 1;
				return {
					results: $.map(data, function(item) {
						return {
							id: item.id,
							text: item.nombre,
							slug: item.nombre,
							results: item
						}
					})
				};
			},
			cache: true
		}
	}).change(function(event) {
		$("#dependencia_id").valid();	
    });

    function agregar_producto(){
        $('#fechaentrada').rules('remove', 'required');
        $('#tipoentrada_id').rules('remove', 'required');
        $('#empleado_id').rules('remove', 'required');
        $('#dependencia_id').rules('remove', 'required');
        $('#persona_id').rules('remove', 'required');
        $('#remitente').rules('remove', 'required');
        $('#cantidad').rules('add', { required: true, minlength: 1, pattern_integer: '' });
        $('#precio').rules('add', { required: true, minlength: 1, pattern_importe: '' });
        $('#areas_producto_id').rules('add', { required: true });
        if($('#form-entrada').valid()){
	    	agregarFila();
	    }else{
		    swal(
  			    '¡Advertencia!',
  			    'Debe seleccionar un producto, la cantidad y precio',
  			    'warning'
		    )
	    }
    }

    function agregarFila(){
        var tr = $('[data-area-producto="' + $("#areas_producto_id").select2('data')[0].id + '"][data-precio="' + $("#precio").val() + '"]');
        var cantidad = parseInt($("#cantidad").val());
        var precio = $("#precio").val();
        console.log(tr);
        if(en_edicion){
            tr.children().first().text(cantidad);
            tr.children().first().next().next().text(precio);
            tr.attr('data-cantidad',cantidad);
            tr.attr('data-precio',precio);
            $("#areas_producto_id").prop("disabled", false);
            en_edicion = false;
        }else if(tr.length > 0){
            swal({
				title: 'El producto ya existe',
				text: "¿Desea sumar las cantidades?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Sí, sumar',
				cancelButtonText: 'Cancelar'
				}).then((result) => {
				if (result.value) {
                    tr.children().first().text( parseInt(tr.children().first().text()) + cantidad);
                    tr.children().first().next().next().text(precio);
                    tr.attr('data-cantidad', parseInt(tr.children().first().text()) );
                    tr.attr('data-precio', tr.children().first().next().next().text() );
				}
			});
        }else{
            $("#tblProductos").append(
                '<tr data-area-producto="' + $("#areas_producto_id").select2('data')[0].id + '" data-cantidad="' + cantidad + 
                '" data-producto="' +$("#areas_producto_id").select2('data')[0].text + '" data-precio="' + precio + '">' +
                '<td class="text-center">' + cantidad + '</td>' + 
                '<td class="text-center">' + $("#areas_producto_id").select2('data')[0].text + '</td>' +
                '<td class="text-center">' + precio + '</td>' + 
                '<td class="text-center"><button class="btn btn-warning btn-sm" onclick="editarProducto(this)" style="margin-right: 4px;"><i class="fa fa-pencil"></i> Editar</button>' +
                '<button class="btn btn-danger btn-sm" onclick="eliminarProducto(this)"><i class="fa fa-remove"></i> Eliminar</button></td>' + 
                '</tr>'
            );
        }
        $("#areas_producto_id").empty();
  	    $('#cantidad').val('');
        $('#precio').val('');
    }

    function editarProducto(btn){
        var cantidad = parseInt($(btn).parent().parent().attr("data-cantidad"));
        var precio = $(btn).parent().parent().attr("data-precio");
        var producto = $(btn).parent().parent().attr("data-producto");
        var area_id = $(btn).parent().parent().attr("data-area-producto");
        var option = new Option(producto, area_id, true, true);
        $("#areas_producto_id").append(option).trigger('change');   
  	    $('#cantidad').val(cantidad);
        $('#precio').val(precio);
        $("#areas_producto_id").prop("disabled", true);
        en_edicion = true;
    }

    function eliminarProducto(btn, eliminar_en_bd, id) {
        if(eliminar_en_bd){
            var disponibles = parseInt($(btn).parent().parent().attr("data-disponibles"));
            var utilizados = parseInt($(btn).parent().parent().attr("data-utilizados"));
            var producto = $(btn).parent().prev().text().toLowerCase();
            producto = producto.charAt(0).toUpperCase() + producto.slice(1);
            var title = "¿Está seguro de eliminar este producto?";
            var text = "Los productos se eliminará definitivamente";
            if(disponibles === 0){
                swal(
                    'Error',
                    'Los productos forman parte de una salida.',
                    'error'
                )
                return;
            }
            if(utilizados > 0){
                title = 'Sólo puede eliminar ' + disponibles + " " + producto + ".";
                text = '¿Desea eliminarlo(s)?.';
            }
            swal({
                title: title,
                text: text,
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'NO',
                confirmButtonText: 'SI'
            }).then((result) => {
                if(result.value) {
                    
                }
            });
        }else{
            swal({
                title: '',
                text: '¿Esta seguro de eliminar este producto?',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#dc3545',
                cancelButtonText: 'NO',
                confirmButtonText: 'SI'
            }).then((result) => {
                if(result.value) {
                    en_edicion = false;
                    $("#areas_producto_id").prop("disabled", false);
                    $(btn).parent().parent().remove();
                }
            });
        }
    }

    

    function agregar_persona() {
		$.get('/personas/search?tipo=create_edit', function(data) {
		})
		.done(function(data) {
		})
		.fail(function(data) {			
            modal_title_persona.text('Agregar Persona:');
            modal_body_persona.html(data.responseJSON.html);
            modal_footer_persona.html(
                '<button type="button" class="btn btn-danger" onclick="cerrar_modal(\'modal-persona\')"><i class="fa fa-close"></i> Cerrar</button>' +
                '<button type="submit" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'
            );
            app.to_upper_case();
			persona.init();
			persona.editar_fotografia();
			persona.agregar_fecha_nacimiento();
			persona.agregar_inputmask();
			persona.agregar_select_create();
			persona.agregar_validacion();
			modal_persona.modal('show');
		});
	}     

        $('#form-entrada').validate({
            rules:  {
                areas_producto_id: {
                    required: true
                },
                cantidad: {
                    required: true
                },
                precio: {
                    required: true
                },
                fechaentrada: {
                    required: true
                },
                tipoentrada_id: {
                    required: true
                },
                empleado_id: {
                    required: true
                }
            }
        });    

        function persona_create_edit_success(response) {
		    app.set_bloqueo(false);
		    unblock();
            $('#modal-personas').modal("hide");
            modal_persona.modal("hide");
            swal(
                '¡Correcto!',
                'Información registrada',
                'success'
            )
            $("#persona_id").html("<option value='" + response.id + "'selected>" + response.nombre + "</option>");
	    }

	    function persona_create_edit_error(response) {
		    unblock();
            if(response.status === 422) {
			    swal({
				    title: 'Error al registrar al solicitante',				
				    text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
				    type: 'error',
				    confirmButtonColor: '#3085d6',
				    confirmButtonText: 'Regresar',
				    allowEscapeKey: false,
				    allowOutsideClick: false
			    });
		    }
	    }

        init_modal_persona();
        init_modal_empleado();
</script>
@endpush