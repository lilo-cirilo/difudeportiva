@push('head')
<link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/rowGroup.dataTables.min.css') }}" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}">

@endpush

@section('content-subtitle', 'Entradas')

@section('li-breadcrumbs')
    <li class="active">Entradas</li>
@endsection

@section('content')

    <section class="content">
    	<div class="row">
    		<div class="col-xs-12">
    			<div class="box box-primary shadow">
                    <div class="box-header with-border">
                        <h3 class="box-title">Entradas de productos</h3>
                    </div>
				    <div class="box-body">
                        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                            <input type="text" id="search" name="search" class="form-control">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-info btn-flat" id="btn_buscar" name="btn_buscar">Buscar</button>
                            </span>
                        </div>
                        {!! $dt_Entradas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'entradas', 'name' => 'entradas', 'style' => 'width: 100%']) !!}
				    </div>
    			</div>
    		</div>
    	</div>
	</section>
	
<div class="modal fade" id="modal-entrada">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Datos de la entrada</h4>
			</div>
			<div class="modal-body" id="modal-body-entrada">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <form class="form-horizontal">
                            <div class="row">
                                <label class="col-sm-2 control-label">Fecha:</label>
                                <div class="col-sm-4">
                                    <p class="form-control-static" id="fecha"></p>
                                </div>
                                <label class="col-sm-2 control-label">Tipo:</label>
                                <div class="col-sm-4">
                                    <p class="form-control-static" id="tipo"></p>
                                    </div>
                                </div>
                                <div class="row">                        
                                    <label class="col-sm-2 control-label">Remitente:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static" id="remitente"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 control-label">Recibido por:</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static" id="receptor"></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h5 class="box-title">Lista de productos</h5>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                {!! $dt_Detalle->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'detalle', 'name' => 'entradas', 'style' => 'width: 100%']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>	
			</div>      
		</div>
	</div>
</div>
            
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
{!! $dt_Entradas->html()->scripts() !!}
{!! $dt_Detalle->html()->scripts() !!}
<script type="text/javascript">

    var entrada_id = 0;

    $(document).ready(function() {
		var entradas = $('#entradas');  
		datatable_entradas = $(entradas).DataTable();

    	$('#search').keypress(function(e) {
			if(e.which === 13) {
				datatable_entradas.search($('#search').val()).draw();
			}
		});

		$('#btn_buscar').on('click', function() {
			datatable_entradas.search($('#search').val()).draw();
		});

        
	});

	function registrar_entrada(){
		location.href = "{{route($modulo.'.productos.entradas.create')}}";
	}

	function consultar(id){
		block();
        entrada_id = id;
        var table = $('#detalle').dataTable();
        datatable_detalle = $(table).DataTable();
        datatable_detalle.on( 'xhr', function () {
            var json = datatable_detalle.ajax.json();            
            $("#fecha").text(json.fecha);
            $("#tipo").text(json.tipo);
            $("#remitente").text(json.remitente);
            $("#receptor").text(json.receptor);
        	$('#modal-entrada').modal('show');
			unblock();
        });
        datatable_detalle.ajax.reload();
	}

	function cancelar(entrada_id){		
		swal({
			title: '¿Está seguro de cancelar la entrada?',
			text: "Los productos relacionados serán eliminados",
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'SI, cancelar',
			cancelButtonText: 'No'
			}).then((result) => {
			if (result.value) {
				block();
                $.ajaxSetup({
          			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        		});
        		$.ajax({
          			url: "{{ URL::to($modulo.'/productos/entradas') }}" + '/' + entrada_id,
          			type: 'POST',
          			data: {_method: 'delete'},
          			success: function(response) {
						  unblock();
            			datatable_entradas.ajax.reload(null, false);
            			swal({
              			title: '¡Correcto!',
              			text: 'La entrada ha sido cancelada.',
              			type: 'success',
              			timer: 3000
            			});
          			},
          			error: function(response) {
						unblock();						
						if(response.status === 422) {
							swal({
								title: 'Error al cancelar la entrada.',				
								text: response.responseJSON.errors[0].message,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Regresar',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}else{
							swal({
								title: 'Error al cancelar la entrada.',				
								text: 'Código de error: ' + response.status,
								type: 'error',
								confirmButtonColor: '#3085d6',
								confirmButtonText: 'Regresar',
								allowEscapeKey: false,
								allowOutsideClick: false
							});
						}
          			}
				});
			}
		});	
	}
</script>
@endpush