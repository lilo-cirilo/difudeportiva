@extends('layouts.app')

@section('content-title', 'Modulos')
@section('content-subtitle', 'Inicio')

@section('content')
<div class="row">

  @if (session('status'))
      <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
          {{ session('msg') }}
      </div>
  @endif

  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Inicio</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID Vale</th>
            <th>ID Viatico</th>
            <th>Ejercicio</th>
            <th>Folio Caja</th>
            <th>Acción</th>
          </tr>
          @foreach($vales as $vale)
          <tr>
            <td>{{ $vale->Cp_Id }}</td>
            <td>{{ $vale->Cp_IdViatico }}</td>
            <td>{{ $vale->Cp_Ejercicio }}</td>
            <td>{{ $vale->Cp_FolioCaja }}</td>
            <td>
              
            </td>
          </tr>
          @endforeach
        </table>
      </div>
    </div>
  </div>
</div>
@endsection

                    