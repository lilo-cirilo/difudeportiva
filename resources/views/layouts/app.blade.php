@extends('vendor.admin-lte.layouts.main')

<?php $ruta = \Request::route()->getName(); ?>



@if (auth()->check())
    @if(file_exists(auth()->user()->persona->get_url_fotografia()))
        @section('user-avatar', asset(auth()->user()->persona->get_url_fotografia()))
    @else
        @section('user-avatar', asset('images/user.png'))
    @endif
@section('user-name', auth()->user()->persona->nombre)
@section('user-rol', isset(auth()->user()->usuarioroles->first->id->rol->nombre) ? auth()->user()->usuarioroles->first->id->rol->nombre: '')
@section('user-job')
@section('user-profile', route('users.edit', auth()->user()->id))
@section('user-log', auth()->user()->created_at)
@endif

@push('head')
<link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/Ionicons/css/ionicons.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/AdminLTE.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('dist/css/skins/_all-skins.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('css/dif.css') }}" type="text/css" rel="stylesheet">
<style>
  .skin-blue .sidebar-menu > li.active > a.home {
      border-left-color: transparent !important;
      color: #b8c7ce !important;
  }
    .skin-blue .sidebar-menu .treeview-menu>li.active>a , .skin-blue .sidebar-menu .treeview-menu>li>a:hover i{
        color: #EF426F;
        white-space: pre-line;
    }

    .skin-blue .sidebar-menu .treeview-menu>li  {
        white-space: pre-line;
    }
    .skin-blue .sidebar-menu .treeview-menu>li> a  {
        display: inline-flex !important;
    }
</style>
@yield('mystyles')
@endpush

@section('breadcrumbs')
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Intranet</a></li>
    @yield('li-breadcrumbs')
  </ol>
@endsection

@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="header">NAVEGACIÓN</li>
  <li {{ ($ruta === 'home') ? 'class=active' : '' }}>
    <a href="{{ route('home') }}">
      <i class="fa fa-home"></i>
      <span>Home</span>
    </a>
  </li>
  
  @if(auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES','SEGUIMIENTO DE PETICIONES']))
    <li {{ ($ruta === 'peticiones.index') ? 'class=active' : '' }}>
      <a href="{{ route('peticiones.index') }}">
        <i class="fa fa-list-alt"></i>
        <span>Peticiones</span>      
        <span class="pull-right-container hidden">
            {{-- El siguiente id es muy extenso para evitar que se repita en otra vista xD --}}
          {{-- <small class="label pull-right bg-red" id="lbl_Peticiones_Nuevas_Director"></small> --}}
        </span>
      </a>
    </li>
  @endif
  @if(auth()->user()->hasRoles(['ADMINISTRADOR DE BENEFICIOS']))
    <li {{ ($ruta === 'programas.index') ? 'class=active' : '' }}>
      <a href="{{ route('programas.index') }}">
        <i class="fa fa-list-ul"></i>
        <span>Beneficios</span>      
        <span class="pull-right-container hidden">
          <small class="label pull-right bg-red" id="configurar_programas"></small>
        </span>
      </a>
    </li>
  @endif

  {{-- @if(auth()->user()->hasRoles(['ADMINISTRADOR DE BENEFICIOS']))    
  <li {{ ($ruta === 'beneficios.index') ? 'class=active' : '' }}>
    <a href="{{ route('beneficios.index') }}">
      <i class="fa fa-cubes"></i>
      <span>Beneficios</span>
    </a>
  </li>
    @endif --}}

    @if(auth()->user()->hasRoles(['ADMINISTRADOR', 'SUPERADMIN', 'COORDINADOR']))
    <li class="{{ ($ruta === 'roles.asign.create' || $ruta === 'roles.users.index' || $ruta === 'roles.index' || $ruta === 'roles.create'  ) ? 'treeview active menu-open' : 'treeview' }}">
      <a href="#">
        <i class="fa fa-lock"></i> <span>Roles</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
      @if(auth()->user()->hasRoles(['SUPERADMIN']))
        <li {{ ($ruta === 'roles.index') ? 'class=active' : '' }}><a href="{{ route('roles.index') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
        <li {{ ($ruta === 'roles.create') ? 'class=active' : '' }}><a href="{{ route('roles.create') }}"><i class="fa fa-circle-o"></i> Agregar Rol</a></li>
      @endif
      @if(auth()->user()->hasRoles(['SUPERADMIN', 'COORDINADOR']))
        <li {{ ($ruta === 'roles.asign.create') ? 'class=active' : '' }}><a href="{{ route('roles.asign.create') }}"><i class="fa fa-plus"></i>Asignar Rol a Usuario</a></li>
      @endif
        <li {{ ($ruta === 'roles.users.index') ? 'class=active' : '' }}><a href="{{ route('roles.users.index') }}"><i class="fa fa-eye"></i> Ver Usuarios/Roles</a></li>
      </ul>
    </li>
    @endif

    @if(isset(auth()->user()->usuarioroles->first->id->rol->nombre))
    <li class="{{ ($ruta === 'home' || $ruta === 'modules.index' || $ruta === 'modules.create' ) ? 'treeview active menu-open' : 'treeview' }} ">
      <a class="{{ ($ruta === 'home' ) ? 'home' : '' }}" href="#">
        <i class="fa fa-cubes"></i> <span>Módulos</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        @if(auth()->user()->hasRoles(['SUPERADMIN']))
          <li {{ ($ruta === 'modules.index') ? 'class=active' : '' }}><a href="{{ route('modules.index') }}"><i class="fa fa-cogs"></i>ADMINISTRAR MÓDULOS</a></li>
         {{-- <li><a href="{{ route('modules.create') }}"><i class="fa fa-circle-o"></i> Agregar Módulo</a></li>--}}
        @endif
        @foreach(auth()->user()->usuarioroles->unique('modulo') as $mymodule)
          @if($mymodule->autorizado && $mymodule->modulo->nombre != "INTRANET")
            <li><a href="{{url('' . $mymodule->modulo->prefix)}}"><i class="icon-dif"></i>{{ $mymodule->modulo->nombre }}</a></li>
          @endif
        @endforeach
      </ul>
    </li>
    @endif
    @if(auth()->user()->hasRoles(['SUPERADMIN', 'ADMINISTRADOR', 'DIRECTOR']))
    <li class="{{ ($ruta === 'users.index') ? 'treeview active menu-open' : 'treeview' }}">
      <a href="#">
        <i class="fa fa-user"></i> <span>Usuarios</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        @if(auth()->user()->hasRoles(['SUPERADMIN', 'DIRECTOR']))
          {{-- <li {{ ($ruta === 'users.index') ? 'class=active' : '' }}><a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i> Todos los Usuarios</a></li> --}}
        @endif
        <li><a href="{{ route('users.ficha') }}"><i class="fa fa-circle-o"></i> Ficha Registro</a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="">
        <i class="fa fa-building-o"></i> <span>Areas</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ route('areas.index') }}">Listado Areas</a></li>
      </ul>
    </li>
    <li class="{{ ($ruta === 'modules.access.create' || $ruta === 'modules.access.authorized.index') ? 'treeview active menu-open' : 'treeview' }}">
      <a href="#">
        <i class="fa fa-gear"></i> <span>Otros</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li {{ ($ruta === 'modules.access.create') ? 'class=active' : '' }}><a href="{{ route('modules.access.create') }}"><i class="fa fa-circle-o"></i> Solicitar Acceso a Módulo</a></li>
        @if(auth()->user()->hasRoles(['SUPERADMIN', 'ADMINISTRADOR']))
          <li {{ ($ruta === 'modules.access.authorized.index') ? 'class=active' : '' }}><a href="{{ route('modules.access.authorized.index') }}"><i class="fa fa-circle-o"></i> Autorizar Acceso Módulo</a></li>
        @endif
        <li><a href=""></a></li>
      </ul>
    </li>
    @endif
    
{{--
  @if(auth()->user()->hasRoles(['ADMINISTRADOR', 'SUPERADMIN']))
    <li class="treeview">
      <a href="#">
        <i class="fa fa-lock"></i> <span>No entrar</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
      @if(auth()->user()->hasRoles(['SUPERADMIN']))
        <li><a href="{{ route('home2') }}"><i class="fa fa-circle-o"></i> Home2</a></li>
        <li><a href="{{ route('home3') }}"><i class="fa fa-circle-o"></i> Home3</a></li>
        <li><a href="{{ route('home4') }}"><i class="fa fa-plus"></i>Home4</a></li>
      @endif
      </ul>
    </li>
  @endif--}}
</ul>
@endsection


@section('message-text', 'No tienes mensajes')
@section('notification-text', 'No ienes notificaciones')
@section('task-text', 'No tienes tareas')

@push('body')
<script type="text/javascript" src="{{ asset('plugins/loaders/blockui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/loaders/block.js') }}"></script>
<script type="text/javascript">
    @if(auth()->user()->hasRoles(['ADMINISTRADOR DE PETICIONES']))
        $.get("{{ route('peticiones.cantidad') }}",(data) => {
            if(data > 0){
                $('#lbl_Peticiones_Nuevas_Director').text(data);
                $('#lbl_Peticiones_Nuevas_Director').parent().removeClass('hidden');
            }else{
                $('#lbl_Peticiones_Nuevas_Director').parent().addClass('hidden');
            }
        });
    @endif

	var block = () => {
        baseZ = 1030;        
        if($('.modal[style*="display: block"]').length > 0){
            baseZ = 10000
        }

		$.blockUI({
			css: {
				border: 'none',
				padding: '5px',
				backgroundColor: 'none',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px',
				opacity: .8,
				color: '#fff'
			},
			baseZ: baseZ,
			message: '<div align="center"><img src="{{ asset('images/preloader/loading.gif') }}"><br /><p style="color:#f0f0f0; margin-top:5px;margin-left:15px; font-family:arial; font-size: larger;">Procesando...</p></div>',
		});

		var unblock_error = () => {
			if($.unblockUI())
				alert('Ocurrio un error al lanzar la petición. Intente nuevamente.');
		}

		setTimeout(unblock_error, 120000);
	}

	var unblock = () => {
		$.unblockUI();
	}
</script>
<script src="{{ asset('assets/js/validator.js') }}"></script>
@yield('cdn-scripts')
<title>Pusher Test</title>
  <!--script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('42a99bb4a3251e49fcee', {
      cluster: 'us2',
      encrypted: true
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
      alert(data.message);
    });
  </script-->
@yield('other-scripts')
@endpush