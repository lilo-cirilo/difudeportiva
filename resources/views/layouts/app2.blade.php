@extends('vendor.admin-lte.layouts.main')

@if (auth()->check())
@section('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm')
@section('user-name', auth()->user()->persona->nombre)
@section('user-job')
@section('user-log', auth()->user()->created_at)
@endif

@push('head')
@yield('mystyles')
@endpush

@section('sidebar-menu')
<ul class="sidebar-menu">
  <li class="header">NAVEGACIÓN</li>
  <li class="active">
    <a href="{{ route('home') }}">
      <i class="fa fa-home"></i>
      <span>Home</span>
    </a>
  </li>
</ul>
@endsection

@push('body')

@yield('cdn-scripts')

@yield('other-scripts')
@endpush