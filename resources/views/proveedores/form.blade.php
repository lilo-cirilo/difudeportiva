<form id="form-proveedor" action="{{isset($proveedor) ? 'POST' : 'PUT'}}">
  <div class="row">
    <div class="col-sm-12 col-md-8">
      <div class="form-group">
        <label for="nombre">*Nombre:</label>
        <input type="text" class="form-control" placeholder="Nombre de la empresa o persona fisica" id="nombre" name="nombre" value="{{$proveedor->nombre or ''}}" required>
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
          <label for="rfc">*RFC:</label>
          <input type="text" class="form-control" placeholder="RFC" id="rfc" name="rfc" value="{{$proveedor->rfc or ''}}" required>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
        <label for="inscripcion">No. Inscripción:</label>
        <input type="text" class="form-control" placeholder="Número de inscripción ante gobierno" id="inscripcion" name="inscripcion" value="{{$proveedor->inscripcion or ''}}">
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
        <div class="form-group">
            <label for="calle">Calle:</label>
            <input type="text" class="form-control" placeholder="Calle" id="calle" name="calle" value="{{$proveedor->calle or ''}}">
        </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
          <label for="numero">Número:</label>
          <input type="text" class="form-control" id="numero" name="numero" value="{{$proveedor->numero or ''}}">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-6">
      <div class="form-group">
          <label for="colonia">Colonia:</label>
          <input type="text" class="form-control" id="colonia" name="colonia" value="{{$proveedor->colonia or ''}}">
      </div>
    </div>
    <div class="col-sm-12 col-md-6">
      <div class="form-group">
          <label for="codigopostal">CP:</label>
          <input type="number" class="form-control" id="codigopostal" name="codigopostal" value="{{$proveedor->cp or ''}}">
      </div>
    </div>
    <div class="col-sm-12">
      <div class="form-group">
          <label for="ciudad">Ciudad:</label>
          <input type="text" class="form-control" id="ciudad" name="ciudad" value="{{$proveedor->ciudad or ''}}">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-6">
      <div class="form-group">
        <label for="email">Correo:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{$proveedor->email or ''}}">
      </div>
    </div>
    <div class="col-sm-12 col-md-6">
      <div class="form-group">
        <label for="telefono">Telefono:</label>
        <input type="number" class="form-control" id="telefono" name="telefono" value="{{$proveedor->telefono or ''}}">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
          <label for="banco_id">Banco:</label>
          <select class="form-control select2" id="banco_id" name="banco_id">
          </select>
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
          <label for="numero_cuenta">Num Cuenta:</label>
          <input type="text" class="form-control" id="numero_cuenta" name="numero_cuenta" value="{{$proveedor->cuenta or ''}}">
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="form-group">
          <label for="clabe">Clabe:</label>
          <input type="text" class="form-control" id="clabe" name="clabe" value="{{$proveedor->clabe or ''}}">
      </div>
    </div>
  </div>
</form>