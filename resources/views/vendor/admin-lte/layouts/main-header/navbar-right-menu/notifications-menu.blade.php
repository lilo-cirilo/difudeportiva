<!-- Notifications Menu -->
<li class="dropdown notifications-menu">
    <!-- Menu toggle button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="@yield('notification-link')">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning" id="@yield('notification-id')">@yield('notification-number')</span>
    </a>
    <ul class="dropdown-menu" id="@yield('notification-dropdown-ul')">
        <li class="header" id="@yield('notification-dropdown-id')">@yield('notification-text')</li>
        <li>
            <!-- Inner Menu: contains the notifications -->
            <ul class="menu">
                <li id="@yield('notification-dropdown-li-id')">
                    <!-- start notification -->
                    @yield('notification-list')
                </li>
                <!-- end notification -->
            </ul>
        </li>
        <li class="footer"><a href="@yield('notification-url-all', '#')">@yield('notification-all', '')</a></li>
    </ul>
</li>
