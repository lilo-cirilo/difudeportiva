<!-- Tasks Menu -->
<li class="dropdown tasks-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger">@yield('task-number')</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">@yield('task-text')</li>
        <li>
            <!-- Inner menu: contains the tasks -->
            <ul class="menu">
                @yield('task-list')
            </ul>
        </li>
        
        <li class="footer"><a href="@yield('task-url-all', '#')">@yield('task-all', '')</a></li>
        
    </ul>
</li>
