<!-- Messages: style can be found in dropdown.less-->
<li class="dropdown messages-menu">
    <!-- Menu toggle button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-envelope-o"></i>
        <span class="label label-success">@yield('message-number')</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">@yield('message-text')</li>
        <li>
            <!-- inner menu: contains the messages -->
            <ul class="menu">
                @yield('message-list')
                
            </ul>
            <!-- /.menu -->
        </li>
        <li class="footer"><a href="@yield('message-url-all', '#')">@yield('message-all', '')</a></li>
    </ul>
</li>
<!-- /.messages-menu -->
