<footer class="main-footer">
  <!-- To the right -->
  <div class="pull-right hidden-xs">
    Unidad de Informática
  </div>
  <!-- Default to the left -->
  DIF Oaxaca<strong> © Copyright {{ date('Y') }}.</strong>
</footer>