@extends('bienestar::layouts.master')

@push('head')
<link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
<link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">

<style type="text/css">
    #map{
        height: 280px;
    }
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">INFORMACION DE ESPECIFICA</h3>
	</div>
	<div class="box-body">
		<!-- /.box-header -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label>Nombre:</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label id="nombre" for="nombre">{{$municipio->nombre or ''}}</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label>Distrito:</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label id="distrito" for="nombre">{{$municipio->distrito->nombre or ''}}</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label>Region:</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label id="region" for="nombre">{{$municipio->distrito->region->nombre or ''}}</label>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <label>Ubicacion:</label>
        </div>
        <div id="map" class="col-xs-12 col-sm-12 col-md-12 col-lg-6 "></div>
	</div>
    <div class="box-footer">
		<a href="{{ route('municipios.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBDpYJe13cGrxH5Hk-9u5fu4PFQI-YMD5M&callback=initMap"></script>
<script type="text/javascript">
    function initMap() {
        var la={{$municipio->latitud}};
        var lo={{$municipio->longitud}};
        var municipio = {lat: la, lng: lo};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: municipio
        });
        var marker = new google.maps.Marker({
            position: municipio,
            map: map
        });
  }
</script>
@endpush
