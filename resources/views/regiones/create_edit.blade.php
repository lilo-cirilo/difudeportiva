@extends('bienestar::layouts.master')

@push('head')
<style type="text/css">
    input {
	       text-transform: uppercase;
       }
</style>
@endpush

@section('content')
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">{{ isset($region) ? 'EDITAR' : 'AGREGAR' }} REGION:</h3>
	</div>
	<div class="box-body">
		<form data-toggle="validator" role="form" id="form_region" action="{{isset($region) ? '/regiones'.'/'.$region->id :'/regiones'}}" method="{{ isset($region) ? 'PUT' : 'POST' }}">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                <div class="form-group">
                    <label for="nombre">Nombre:</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="NOMBRE" value="{{ $region->nombre or '' }}" onkeyup="toUpperCase(this)">
                </div>
            </div>
        </form>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<button type="button" class="btn btn-success" onclick="procesarRegion();"><i class="fa fa-database"></i> Guardar</button>
		<a href="{{ route('regiones.index') }}" class="btn btn-danger"><i class="fa fa-reply"></i> Regresar</a>
	</div>
</div>
@stop

@push('body')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script type="text/javascript">

	$(document).ready(function() {

		$.validator.setDefaults({
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorPlacement: function(error, element) {
				$(element).parents('.form-group').append(error);
			}
		});

		$('#form_region').validate({
			rules: {
				nombre: {
					required: true,
					minlength: 3,
					pattern_nombre: 'El nombre solo puede contener letras.'
				}
			},
			messages: {
				nombre: {
					required: 'Por favor introdusca un nombre',
					minlength: 'El nombre debe tener al menos 3 caracteres.'
				}
			},
			// submitHandler : function(form) {
			// 	procesarRegion();
			// }
		});

		$.validator.addMethod('pattern_nombre', function(value, element) {
			return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
		}, 'Solo letras y espacios entre palabras');
	})
    function toUpperCase(elemento) {
		javascript:elemento.value = elemento.value.toUpperCase();
	}
    function procesarRegion() {
        if($('#form_region').valid()){
            block();
            $.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

            $.ajax({
				url: $('#form_region').attr('action'),
    			type: $('#form_region').attr('method'),
				data: $('#form_region').serialize(),
				//processData: false,
				//contentType: false,
				success: function(response) {
					unblock();
					var re = '{{route('regiones.index')}}'//"{{ URL::to('regiones/show') }}" + '/' + response.id;
				window.location.href = re;// + '?n=' + new Date().getTime();
				},
				error: function(response) {
					unblock();
				}
			});
        }
    }
</script>
@endpush
