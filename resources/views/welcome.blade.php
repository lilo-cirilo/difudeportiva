<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="{{ asset('css/materialize/materialize.min.css') }}" type="text/css" rel="stylesheet">
        <style>
       body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
            background-size: cover;
        }
        
        nav {
            background-color: rgba(90, 90, 90, 0.8);
            z-index: 1;
            text-transform: uppercase;
        }
        
        nav ul a:hover,
        nav ul li.active>a:hover {
            background-color: rgba(48, 48, 48, 0.5);
            color: #ff2365 !important;
        }
        
        nav ul li.active,
        nav ul li.active>a {
            background-color: rgba(48, 48, 48, 0.5);
            color: #e61755 !important;
        }
        
        #img-logo {
            height: 52px;
            width: auto;
            margin-top: 2px;
        }
        
        main {
            flex: 1 0 auto;
        }
        
        #ayuda:hover {
            color: #ff2b6a !important;
        }
        
        .page-footer {
            padding-top: 0px;
            z-index: 1;
        }
        
        .page-footer,
        .page-footer .footer-copyright {
            background-color: rgba(51, 51, 51, 0.5);
        }
        
        #ayuda>i {
            margin-right: 5px;
            margin-top: 2px;
            font-size: 18px;
        }

        .col.grid-example {
            text-align: center;
            margin-top:20%;
        }

        @media only screen and (min-width: 960px) {
            #img-logo {
                height: 60px;
                width: auto;
                margin-top: 2px;
            }
        }
        </style>

    </head>
    <body>
    <header>
        <!--*Nav-Bar de la pagina*-->
        <div class="navbar-fixed">
            <nav>
                <div class="nav-wrapper">
                    <!--*Boton del menu mobil*-->
                    <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
                    <!--*Contenido del menu full screen*-->
                    <div class="container">
                        <a class="brand-logo"><img id="img-logo" src="{{ asset('images/DIF_HORIZONTAL_BLCO.png') }}" alt="DIF"></a>
                        <ul class="right hide-on-med-and-down">
                        @if (Route::has('login'))
                            @auth
                                <li><a href="{{ url('/home') }}"><i class="material-icons left">home</i>Home</a></li>
                            @else
                                <li><a href="{{ route('register') }}"><i class="material-icons left">person_add</i>Registrarse</a></li>
                                <li><a href="{{ route('login') }}"><i class="material-icons left">fingerprint</i>Login</a></li>
                            @endauth
                        @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <!--*Contenido del menu-movil*-->
        <ul class="side-nav" id="mobile-menu">
        @if (Route::has('login'))
            @auth
                <li><a href="{{ url('/home') }}"><i class="material-icons left">home</i>Home</a></li>
            @else
                <li><a href="{{ route('register') }}"><i class="material-icons left">person_add</i>Registrarse</a></li>
                <li><a href="{{ route('login') }}"><i class="material-icons left">fingerprint</i>Login</a></li>
            @endauth
        @endif
        </ul>
    </header>
    <main>
    <div class="row">
      <div class="grid-example col s12"><span class="flow-text">Intranet DIF <br> BIENVENIDO</span></div>
    </div>
    <!--*Modal del Directorio Unidad de Informatica*-->
    <div id="directorioUI" class="modal">
        <div class="modal-content">
            <h4>Directorio Unidad de Informatica</h4>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-light btn">
                <i class="material-icons left ">close</i> Cerrar
            </a>
        </div>
    </div>
    </main>
    <footer class="page-footer ">
        <div class="footer-copyright ">
            <div class="container ">
                <i class="icon-dif left"></i> © 2018 Copyright
                <a id="ayuda" class="grey-text text-lighten-4 right tooltipped modal-trigger" data-position="top" data-delay="50" data-tooltip="Directorio Unidad de Informatica" href="#directorioUI">
                    <i class="material-icons left">call</i> Ayuda
                </a>
            </div>
        </div>
    </footer>
    <!--
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
        -->
        <script type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('js/materialize.min.js') }}"></script>
        <script>
            $(document).ready(function() {
            $(".button-collapse").sideNav();
            $('.tooltipped').tooltip({ delay: 50 });
            $('.modal').modal();   
            });
        </script>
    </body>
</html>
