<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script type="text/javascript">
	var app = (function() {

		var bloqueo = false;

		function to_upper_case() {
			$(':input').on('propertychange input', function(e) {
				var ss = e.target.selectionStart;
				var se = e.target.selectionEnd;
				e.target.value = e.target.value.toUpperCase();
				e.target.selectionStart = ss;
				e.target.selectionEnd = se;
			});
		};

		function set_bloqueo(valor) {
			bloqueo = valor;
		};

		function get_bloqueo() {
			return bloqueo;
		};

		function agregar_bloqueo_pagina() {
			$('form :input').change(function() {
				bloqueo = true;
			});

			window.onbeforeunload = function(e) {
				if(bloqueo)
				{
					return '¿Estás seguro de salir?';
				}
			};
		};

		return {
			to_upper_case: to_upper_case,
			set_bloqueo: set_bloqueo,
			agregar_bloqueo_pagina: agregar_bloqueo_pagina
		};
	})();

	function dependencia_create_edit_success(response, method) {};
	
	function dependencia_create_edit_error(response) {};

	var dependencia = (function() {
		var form = $('#form_dependencia'),
		entidad = $('#entidad_id'),
		tipo = $('#tipo'),
		action = form.attr('action'),
		method = form.attr('method');

		function init() {
			form = $('#form_dependencia'),
            entidad = $('#entidad_id'),
		    tipo = $('#tipoinstitucion_id'),
			action = form.attr('action'),
			method = form.attr('method');
			$('[data-mask]').inputmask();			
			entidad.select2({
				language: 'es'				
			}).change(function(event) {
				entidad.valid();				
			});
			tipo.select2({
				language: 'es',		
				minimumResultsForSearch: Infinity		
			}).change(function(event) {
				tipo.valid();				
			});
			agregar_validacion();
			app.to_upper_case();
		};

		function create_edit() {
			if(form.valid()) {

				block();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					url: action + '?_method=' + method,
					type: 'POST',
					data: form.serialize(),
					success: function(response) {
						dependencia_create_edit_success(response, method);
					},
					error: function(response) {
						dependencia_create_edit_error(response);
					}
				});

			}
		};

		function agregar_validacion() {
			form.validate({
				rules: {
					nombre: {
						required: true,
						minlength: 3,
						pattern_dependencia: ''
					},
					tipo: {
						required: true
					},
					encargado: {
						required: true,
						minlength: 3,
						pattern_nombre: ''
					},
                    cargoencargado: {
						required: true,
						minlength: 3,
					},
					calle: {
						minlength: 3
					},
					numero: {
						minlength: 1,
						pattern_numero: ''
					},
					colonia: {
						minlength: 3
					},
					entidad_id: {
						required: true
					},					
					telefono: {
						pattern_telefono_dependencia: ''
					},
                    email: {
						required: false,
						email: true
					}	
				},
				messages: {
				}
			});
		};

		return {
			init: init,			
			create_edit: create_edit
		};
	})();
</script>