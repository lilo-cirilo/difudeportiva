
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>
                    @if($dependencia->tipo === "DEPENDENCIA")
                        Dependencia:
                    @else
                        Institución:
                    @endif
                </label>
                {{ $dependencia->nombre }}
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Encargado:</label>
                {{ $dependencia->encargado }}
            </div>
        </div>
 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Cargo:</label>
                {{ $dependencia->cargoencargado }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Teléfono:</label>
                {{ $dependencia->telefono }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Domicilio:</label>
                {{ $dependencia->calle . ' ' . $dependencia->numero . ' ' . $dependencia->colonia . ' ' . $dependencia->entidad->entidad }}
            </div>
        </div>       

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <label>Correo electrónico:</label>
                {{ $dependencia->email }}
            </div>
        </div>       
        
    </div>
 