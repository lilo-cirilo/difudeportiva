@extends('layouts.app')

@section('myCSS')
	@include('atnciudadana::solicitudes.css.index')
@endsection

@section('content-subtitle', 'Eventos y giras')

@section('li-breadcrumbs')
    <li class="active">{{ $evento->gira->nombre }}</li>  
@endsection

@section('content')
<section class="content">   

    <div class="row">
        <div class="col-lg-4 col-xs-4">
          <div class="small-box bg-aqua">
            <div class="inner">
                <h3 id="beneficiarios">{{ $evento->beneficiarios->where('folio','!=',null)->count() }}</h3>
              <p>Beneficiarios</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-xs-4">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="solicitudes">{{ $evento->beneficiarios->where('folio',null)->count() }}</h3>
              <p>Solicitudes</p>
            </div>
            <div class="icon">
              <i class="fa fa-file-text-o"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-xs-4">
          <div class="small-box bg-green">
            <div class="inner">
            <h3 id="inversion">${{ $evento->get_inversion() }}</h3>
              <p>Inversión</p>
            </div>
            <div class="icon">
              <i class="fa fa-dollar"></i>
            </div>
          </div>
        </div>
        
    </div>

<div class="box box-primary shadow">   

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:30%">
                            <i class="fa fa-id-card margin-r-5"></i>
                            Evento:
                        </th>
                        <td>{{ $evento->gira->nombre }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-building margin-r-5"></i>
                            Municipio:
                        </th>
                        <td>
                            {{ $evento->municipio->nombre }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:40%">
                            <i class="fa fa-calendar margin-r-5"></i>
                            Fecha:
                        </th>
                        <td>{{ $evento->get_formato_fecha() }}</td>
                    </tr>
                    <tr>
                        <th>
                            <i class="fa fa-university margin-r-5"></i>
                            Localidad:
                        </th>
                        <td>
                            @if($evento->cat_localidad)
                                {{ $evento->cat_localidad->nombre }}
                            @elseif($evento->localidad)
                                {{ $evento->localidad }}
                            @endif
                        </td>
                    </tr>                    
                </table>
            </div>
        </div>
    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_beneficiarios" data-toggle="tab" aria-expanded="true">Lista de beneficiarios</a></li>
            <li class=""><a href="#tab_solicitudes" data-toggle="tab" aria-expanded="false">Lista de solicitudes</a></li>              
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_beneficiarios">     
				<div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_beneficiarios" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_beneficiarios">Buscar</button>
                    </span>
                </div>
                {!! $dt_beneficiarios->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'dt_beneficiarios', 'name' => 'dt_beneficiarios', 'style' => 'width: 100%']) !!}
            </div>
            <div class="tab-pane" id="tab_solicitudes">
                <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
                    <input type="text" id="search_solicitudes" class="form-control">
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_solicitudes">Buscar</button>
                    </span>
                </div>
                {!! $dt_solicitantes->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'dt_solicitantes', 'name' => 'dt_solicitantes', 'style' => 'width: 100%']) !!}
            </div>
        </div>
    </div>
</div>
   
  </section>

  <div class="modal fade" id="modal-foto">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modal-title-foto">Foto de entrega</h4>
            </div>
            <div class="modal-body" id="modal-body-foto">
                <img id="foto" class="img-responsive" src="" alt="No se ha podido mostrar la foto">
            </div>
            <div class="modal-footer" id="modal-footer-foto"></div>      
        </div>
    </div>
</div>

@stop

@section('myScripts')


<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dt_beneficiarios->html()->scripts() !!}
{!! $dt_solicitantes->html()->scripts() !!}
<script>

    $(document).ready(function() {
        var beneficiarios = $('#dt_beneficiarios');  
		datatable_beneficiarios = $(beneficiarios).DataTable();

        $('#search_beneficiarios').keypress(function(e) {
			if(e.which === 13) {
				datatable_beneficiarios.search($('#search_beneficiarios').val()).draw();
			}
		});

		$('#btn_buscar_beneficiarios').on('click', function() {
			datatable_beneficiarios.search($('#search_beneficiarios').val()).draw();
        });
        
        var solicitudes = $('#dt_solicitantes');  
		datatable_solicitudes = $(solicitudes).DataTable();

        $('#search_solicitudes').keypress(function(e) {
			if(e.which === 13) {
				datatable_solicitudes.search($('#search_solicitudes').val()).draw();
			}
		});

		$('#btn_buscar_solicitudes').on('click', function() {
			datatable_solicitudes.search($('#search_solicitudes').val()).draw();
        });
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");
            if(target === '#tab_solicitudes'){
                datatable_solicitudes.ajax.reload();
            }else{
                datatable_beneficiarios.ajax.reload();
            }
        });
	});

    function mostrar_foto(url){        
        $("#foto").attr("src",'/'+url);
        $("#modal-foto").modal('show');
    }

</script>

@endsection