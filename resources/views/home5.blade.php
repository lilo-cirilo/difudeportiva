@extends('layouts.app')
<?php $ruta = \Request::route()->getName(); ?>

@section('content')
<div class="background">
 <div class="container">
	@if (session('status'))
		<div class="alert alert-warning">
			{{ session('status') }}
		</div>
	@endif
  <div class="row" style="display: flex;align-items: center;justify-content: center;">
    <h1>{{auth()->user()->hasRolesModulo(['ADMINISTRADOR'], '2')}}</h1>
   <div class="col-xs-10 col-md-8 col-lg-6">
       <div class="row" style="margin-bottom:15px">
           <div class="col-12">
               <img class="img-responsive" src="{{asset('images/logo_dif.png')}}" alt="">
           </div>
       </div>
       <div class="row fecha-hora" style="text-align: center;margin-top:15px;">
            <div class="col-sm-12 col-lg-6"><p id="day">...</p></div>
            <div class="col-sm-12 col-lg-6"><p id="time">...</p></div>
       </div>
    {{-- <h1 id="time">Hora</h1>
    <h3 id="day" class="display-5">Fecha</h3> --}}
    {{-- <h2 id="greeting">Saludo</h2> --}}
    {{-- @if(isset(auth()->user()->usuarioroles->first->id->rol->nombre))
      <nav class="cl-effect-19" id="cl-effect-19">
        @foreach(auth()->user()->usuarioroles->unique('modulo') as $mymodule)
          @if($mymodule->autorizado && $mymodule->modulo->nombre != "INTRANET")
            <a href="{{url('' . $mymodule->modulo->prefix)}}">
              <span data-hover="{{ $mymodule->modulo->nombre }}"></span>{{ $mymodule->modulo->nombre }}</a>
          @endif
        @endforeach
      </nav>
    @endif --}}
	@if(! Auth::user()->ficha)
		{{-- <button class="btn btn-primary btn-flat" id="btn-ficha" onclick="imprimirFicha()">Imprimir hoja de registro</button> --}}
	@endif
   </div>
 </div>
@endsection

@section('mystyles')
<style>
    .fecha-hora {
        color: #DB3762;
        font-family: TodaySHOP;
        font-size: 1.3em;
    }

  #cl-effect-19 a {
	position: relative;
	display: inline-block;
	margin: 15px 25px;
	outline: none;
	color: red;
	text-decoration: none;
	text-transform: uppercase;
	letter-spacing: 1px;
	font-weight: 400;
	text-shadow: 0 0 1px rgba(255,255,255,0.3);
	font-size: 1.35em;
}

#cl-effect-19 a:hover,
#cl-effect-19 a:focus {
	outline: none;
}

/* Effect 19: 3D side */
.cl-effect-19 a {
	line-height: 2em;
	margin: 15px;
	-webkit-perspective: 800px;
	-moz-perspective: 800px;
	perspective: 800px;
	width: 200px;
}

.cl-effect-19 a span {
	position: relative;
	display: inline-block;
	width: 100%;
	padding: 0 14px;
	background: #e35041;
	-webkit-transition: -webkit-transform 0.4s, background 0.4s;
	-moz-transition: -moz-transform 0.4s, background 0.4s;
	transition: transform 0.4s, background 0.4s;
	-webkit-transform-style: preserve-3d;
	-moz-transform-style: preserve-3d;
	transform-style: preserve-3d;
	-webkit-transform-origin: 50% 50% -100px;
	-moz-transform-origin: 50% 50% -100px;
	transform-origin: 50% 50% -100px;
}

.cl-effect-19 a span::before {
	position: absolute;
	top: 0;
	left: 100%;
	width: 100%;
	height: 100%;
	background: #b53a2d;
	content: attr(data-hover);
	-webkit-transition: background 0.4s;
	-moz-transition: background 0.4s;
	transition: background 0.4s;
	-webkit-transform: rotateY(90deg);
	-moz-transform: rotateY(90deg);
	transform: rotateY(90deg);
	-webkit-transform-origin: 0 50%;
	-moz-transform-origin: 0 50%;
	transform-origin: 0 50%;
	pointer-events: none;
}

.cl-effect-19 a:hover span,
.cl-effect-19 a:focus span {
	background: #b53a2d;
	-webkit-transform: rotateY(-90deg);
	-moz-transform: rotateY(-90deg);
	transform: rotateY(-90deg);
}
 .cl-effect-19 a:hover span::before,
.cl-effect-19 a:focus span::before {
	background: #ef5e50;	
}


.content {
    min-height: 250px;
    padding: 0px;
    margin-right: auto;
    margin-left: auto;
    padding-left: 0px;
    padding-right: 0px;
    margin-top:-26px;
}

.content-header {
    position: relative;
    padding: 0px;
}

body {
	font-family: 'Lato', sans-serif;
}
.background {
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none; 
    background: url("{{ asset('images/TxPGrayDIF.png') }}");
    background-color: white;
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
	min-height: calc(100vh - 101px);
	color: #DB3762 !important;
	display: flex;
	align-items: center;
	justify-content: center;
	position: relative;
	
	&:before {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: linear-gradient(to bottom, rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0));
	}
	h1 {
		font-size: 4rem;
		font-weight: 700;
	}
}
.custom-input, .btn-custom {
	border: 0;
	background: transparent;
	border-bottom: 4px solid white;
	border-radius: 0;
	margin-bottom: 0;
}
.custom-input:focus {
	border-color: white;
	background: transparent;
	color: white;
}
.btn-custom {
	color: white;
	cursor: pointer;
}
.display-5 {
	font-size: 1.5rem;
}
#greeting {
	margin-top: 2rem;
	font-size: 2rem;
}
@media (min-width: 576px) {
	.background h1 {
		font-size: 5.5rem;
	}
	.display-5 {
		font-size: 2.5rem;
	}
	#greeting {
		margin-top: 2rem;
		font-size: 2.5rem;
	}
	
}

@media (max-width: 991px){
	.background {
		min-height: calc(100vh - 151px);
	}

	.content {
	min-height: 250px;
	padding: 0px;
	margin-right: auto;
	margin-left: auto;
	padding-left: 0px;
	padding-right: 0px;
	margin-top: -62px;
	}
}

@media (min-width: 992px) {
	.background h1 {
		font-size: 6rem;
	}
	#greeting {
		font-size: 3rem;
	}
}

@media (min-width: 1200px) {
	.background h1 {
		font-size: 7.5rem;
	}
	#greeting {
		font-size: 3.6rem;
	}
}

{{--  #time {
	text-shadow: 1px 1px 3px #781b33;
}  --}}

</style>
@endsection

@section('cdn-scripts')
<script src="{{asset('/js/sweetalert2.all.js')}}"></script>
<script src="{{asset('/js/jsreport.js')}}"></script>
<script>
	function imprimirFicha()
	{
		jsreport.serverUrl = '{{env("REPORTER_URL", "http://localhost:5488")}}';
		// axios.get('/registro/{{auth()->user()->id}}/datos').then(result => {
		// 	console.log(result.data);
			
		// }).catch(error => { console.log(error); });
		axios.post('/registro/{{auth()->user()->id}}/ficha')
			.then(response => {
				swal('Great !', 'Registro correcto', 'success');
				// location.reload();
				$('#btn-ficha').hide();
				var request = {
					template:{
						shortid: "BkNUk_KlN"
					},
					header: {
						Authorization : "Basic YWRtaW46MjFxd2VydHk0Mw=="
					},
					data:{
						nombre: response.data.nombre_completo,
						cargo: response.data.cargo,
						ads:  response.data.ads,
						usuario: response.data.usuario,
						rfc: response.data.rfc,
						jefe: response.data.jefe,
						area: response.data.area,
						usuario: response.data.usuario,
						email: response.data.email,
						rfc: response.data.rfc,
						roles: response.data.roles.map(item => { return {modulo: item.modulo.nombre, rol: item.rol.nombre} })
					}
				};
				//display report in the new tab
				jsreport.render('_blank', request);
				console.log(response);
			}).catch(error => { console.log(error); });
	};
$(function() {

	
	function time() {
		
		var date = new Date(),
			hours = date.getHours(), 
			minutes = date.getMinutes().toString(), 
			greeting, 
			dd = date.getDate().toString(), 
			userName = "{{auth()->user()->persona->nombre}}"; 

		
		if (hours < 12) {
			ante = "AM";
			greeting = "Buenos días";
		} else if (hours === 12 && hours >= 3) {
			ante = "PM";
			greeting = "Buenas tardes"
		} else {
			ante = "PM";
			greeting = "Buenas tardes";
		}

		
		if (hours === 0) {
			hours = 12;
			
			
		} else if (hours !== 12) {
			hours = hours % 12;
		}

		if (minutes.length < 2) {
			minutes = "0" + minutes;
		}

		if (dd.length < 2) {
			dd = "0" + dd;
		}

		Date.prototype.monthNames = [
			"enero",
			"febrero",
			"marzo",
			"abril",
			"mayo",
			"junio",
			"julio",
			"agosto",
			"septiembre",
			"octubre",
			"noviembre",
			"diciembre"
		];

		Date.prototype.weekNames = [
			"domingo",
			"lunes",
			"martes",
			"miercoles",
			"jueves",
			"viernes",
			"sabado"
		];
		
		Date.prototype.getMonthName = function() {
			return this.monthNames[this.getMonth()];
		};
		
		Date.prototype.getWeekName = function() {
			return this.weekNames[this.getDay()];
		};

		$("#time").html(hours + ":" + minutes + " " + ante);
		$("#day").html(date.getWeekName().toUpperCase() + ", " + dd + " DE " + date.getMonthName().toUpperCase());
		$("#greeting").html(greeting + ", " + userName);
		
		setInterval(time, 1000);
	}
	time();
});

</script>
@endsection