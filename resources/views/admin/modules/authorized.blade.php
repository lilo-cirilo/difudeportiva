@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Módulo')
@section('content-subtitle',  'Nuevas Solicitudes' )

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Tus Solicitudes a Módulo</h3>
        </div>
        @if($requests->count())
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Rol</th>
                        <th>Módulo</th>
                        <th>Acción</th>
                    </tr>
                    @foreach($requests as $req)
                        <tr>
                            <td>{{ $req->id }}</td>
                            <td>{{ $req->usuario->usuario }}</td>
                            <td>{{ $req->rol->nombre }}</td>
                            <td>{{ $req->modulo->nombre }}</td>
                            <td>
                                <a href="{{ route('modules.access.authorized', $req) }}" class="btn btn-success btn-flat" 
                                    onclick="event.preventDefault(); document.getElementById('put-form' + {{ $req->id }}).submit();"><i class="fa fa-check"></i></a>
                                </a>
                                <form id="put-form{{ $req->id }}" action="{{ route('modules.access.authorized', $req) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @else
        <div class="box-body">
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Sin Pendientes !</h4>
                No tienes nuevas Solicitudes
            </div>
        </div>
        @endif
    </div>
  </div>
</div>
@endsection

@section('other-scripts')
<script>
$('#form-vic').validator();
</script>
@endsection