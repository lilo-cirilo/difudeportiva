@extends('admin-lte::layouts.auth')

@section('content')
<div class="login-box-body">
  <p class="login-box-msg">Registrate como Empleado</p>

  <form action="{{ route('empleado.store') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="persona_id" value="{{ $persona->id }}">
    @if(isset(auth()->user()->id))
        <input type="hidden" name="usuario_id" value="{{ auth()->user()->id }}">
    @else
        <input type="hidden" name="usuario_id" value="5">
    @endif

    <div class="form-group has-feedback{{ $errors->has('persona') ? ' has-error' : '' }}">
      <input disabled id="persona" type="persona" class="form-control" name="persona" value="{{ old('persona') ? old('persona') : $persona->nombre . ' ' . $persona->primer_apellido . ' ' . $persona->segundo_apellido }}" placeholder="persona" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('persona'))
      <span class="help-block">{{ $errors->first('persona') }}</span>
      @endif
    </div>

    <div class="form-group has-feedback{{ $errors->has('rfc') ? ' has-error' : '' }}">
      <input id="rfc" type="rfc" class="form-control" name="rfc" value="{{ old('rfc') }}" placeholder="RFC" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('rfc'))
      <span class="help-block">{{ $errors->first('rfc') }}</span>
      @endif
    </div>
    <div class="form-group has-feedback{{ $errors->has('num_empleado') ? ' has-error' : '' }}">
      <input id="num_empleado" type="num_empleado" class="form-control" name="num_empleado" value="{{ old('num_empleado') }}" placeholder="Numero Empleado" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      @if ($errors->has('num_empleado'))
      <span class="help-block">{{ $errors->first('num_empleado') }}</span>
      @endif
    </div>
    <div class="form-group has-feedback{{ $errors->has('segurosocial') ? ' has-error' : '' }}">
      <input id="segurosocial" type="segurosocial" class="form-control" name="segurosocial" placeholder="Seguro Social" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      @if ($errors->has('segurosocial'))
      <span class="help-block">{{ $errors->first('segurosocial') }}</span>
      @endif
    </div>

    <div class="form-group has-feedback{{ $errors->has('tiposempleado_id') ? ' has-error' : '' }}">
      <select class="form-control select2" style="width: 100%;" name="tiposempleado_id" value="{{ old('tiposempleado_id') }}">
        @foreach($tipos as $tipo)
            <option value="{{ $tipo->id }}">{{ $tipo->tipo }}</option>
        @endforeach
      </select>
      @if ($errors->has('tiposempleado_id'))
        <span class="help-block">{{ $errors->first('tiposempleado_id') }}</span>
      @endif
    </div>

    <div class="form-group has-feedback{{ $errors->has('nivel_id') ? ' has-error' : '' }}">
      <select class="form-control select2" style="width: 100%;" name="nivel_id" value="{{ old('nivel_id') }}">
        @foreach($niveles as $nivel)
            <option value="{{ $nivel->id }}">{{ $nivel->nivel }}</option>
        @endforeach
      </select>
      @if ($errors->has('nivel_id'))
        <span class="help-block">{{ $errors->first('nivel_id') }}</span>
      @endif
    </div>
    
    <div class="form-group has-feedback{{ $errors->has('area_id') ? ' has-error' : '' }}">
      <select class="form-control select2" style="width: 100%;" name="area_id" value="{{ old('area_id') }}">
        @foreach($areas as $area)
            <option value="{{ $area->id }}">{{ $area->nombre }}</option>
        @endforeach
      </select>
      @if ($errors->has('area_id'))
        <span class="help-block">{{ $errors->first('area_id') }}</span>
      @endif
    </div>

    <hr><p>En caso de Emergencia:</p>

    <div class="form-group has-feedback{{ $errors->has('persona_emergencia') ? ' has-error' : '' }}">
      <input id="persona_emergencia" type="persona_emergencia" class="form-control" name="persona_emergencia" placeholder="Nombre" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      @if ($errors->has('persona_emergencia'))
      <span class="help-block">{{ $errors->first('persona_emergencia') }}</span>
      @endif
    </div>

    <div class="form-group has-feedback{{ $errors->has('telefono_emergencia') ? ' has-error' : '' }}">
      <input id="telefono_emergencia" type="telefono_emergencia" class="form-control" name="telefono_emergencia" placeholder="Telefono" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      @if ($errors->has('telefono_emergencia'))
      <span class="help-block">{{ $errors->first('telefono_emergencia') }}</span>
      @endif
    </div>
    
    <div class="row">
      <div class="col-xs-8">
        
      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <button type="submit" class="btn btn-primary btn-block btn-flat">Guardar</button>
      </div>
      <!-- /.col -->
    </div>
  </form>

</div>
@endsection

@push('body')
<script>
    $(':input').on('propertychange input', function(e) {
        var ss = e.target.selectionStart;
        var se = e.target.selectionEnd;
        e.target.value = e.target.value.toUpperCase();
        e.target.selectionStart = ss;
        e.target.selectionEnd = se;
    });
</script>
@endpush