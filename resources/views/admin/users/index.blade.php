@extends('layouts.app')

@section('content-title', 'Usuarios')
@section('content-subtitle', 'Todos')

@section('mystyles')
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
</style>
@endsection

@section('content')
<div class="row" id="app">

  @if (session('status'))
      <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
          {{ session('msg') }}
      </div>
  @endif

  <div class="col-md-12">
    <div class="box box-primary shadow">
      <div class="box-header">
        <h3 class="box-title">Inicio</h3>
        <div class="box-tools">
          <div class="input-group input-group-sm" style="width: 150px;">
            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

            <div class="input-group-btn">
              <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Usuario</th>
            <th>Correo</th>
            <th>Estatus</th>
            <th>Cambiar Est</th>
            <th>Editar</th>
            <th>Borrar</th>
            <th>Ver Roles</th>
          </tr>
          @foreach($users as $user)
          <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->persona->nombreCompleto() ? $user->persona->nombreCompleto() : '' }}</td>
            <td>{{ $user->usuario }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->autorizado ? 'Autorizado' : 'NO Autorizado' }}</td>
            <td>
                <a href="{{ route('users.status', $user) }}" class="btn {{ ! $user->autorizado ? 'btn-success' : 'btn-warning' }} btn-flat"
                onclick="event.preventDefault(); document.getElementById('status-form' + {{ $user->id }}).submit();"><i class="fa fa-check"></i></a>
                <form id="status-form{{ $user->id }}" action="{{ route('users.status', $user) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
              </form>
            </td>
            <td>
              <a href="{{ route('users.edit', $user) }}" class="btn btn-warning btn-flat"><i class="fa fa-edit"></i>
            </td>
            <td>
              <a href="{{ route('users.destroy', $user) }}" class="btn btn-danger btn-flat" 
                onclick="event.preventDefault(); document.getElementById('delete-form' + {{ $user->id }}).submit();"><i class="fa fa-trash"></i></a>
              </a>
              <form id="delete-form{{ $user->id }}" action="{{ route('users.destroy', $user) }}" method="POST" style="display: none;">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
              </form>
            </td>
            <td><a v-on:click.prevent="getUserWithRol({{ $user->id }})" class="btn btn-success btn-flat"><i class="fa fa-eye"></i></a></td>
          </tr>
          @endforeach
        </table>
      </div>
      <transition name="fade">
        <div class="box-footer" v-if="usuario.id">
          <h3 v-text="msg + ' ' + usuario.usuario"></h3>
          <table class="table table-hover">
            <tr>
              <th>ID Módulo</th>
              <th>Módulo</th>
              <th>ID Rol</th>
              <th>Rol</th>
            </tr>
            <tr v-for="rol in usuario.roles">
              <td>@{{ rol.rol_id }}</td>
              <td>@{{ rol.rol_nombre }}</td>
              <td>@{{ rol.modulo_id }}</td>
              <td>@{{ rol.modulo_nombre }}</td>
            </tr>
          </table>
        </div>
      </transition>
    </div>
  </div>
</div>
@endsection

                    
@section('cdn-scripts')
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.0"></script>

<script>

new Vue({
  el: '#app',
  data: {
    msg: 'Roles del usuario',
    usuario: {
      id: 0,
      usuario: '',
      roles: []
    }
  },
  methods: {
    getUserWithRol(id){
      this.usuario.id = null;
      this.$http.get('users/' + id)
        .then(response => {
          this.usuario.id = response.body.id;
          this.usuario.usuario = response.body.usuario;
          this.usuario.roles = response.body.roles;
          //this.someData = response.body;
        }, response => {});
    }
  }
});

</script>
@endsection