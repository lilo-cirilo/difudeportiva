@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Usuario')
@section('content-subtitle', 'Ficha')

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="box box-primary shadow">
            <div class="box-header">
                <h3 class="box-title">Buscar Usuario</h3>
            </div>

            <div class="box-body">
                <v-select :options="usuarios" :filterable="false" label="nombre_completo" v-model="usuario" @search="buscarUsuario"></v-select>
            </div>
            
            <div class="box-footer">
                <div class="pull-right">
                    <button type="submit" class="btn btn-primary" @click.prevent="imprimirFicha" :disabled="desactivado">Imprimir Ficha</button>
                    <button type="submit" class="btn btn-primary" @click.prevent="mandarFicha" :disabled="desactivado">Mandar Ficha</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('other-scripts')
<script src="{{asset('/js/vue.js')}}"></script>
<script src="{{asset('/js/vue-select.js')}}"></script>
<script src="{{asset('/js/jsreport.js')}}"></script>
<script src="{{asset('/js/sweetalert2.all.js')}}"></script>
<script>
Vue.component("v-select", VueSelect.VueSelect);
const app = new Vue({
    el: '#app',
    data: {
        usuarios: [],
        usuario: {},
        datos: null,
        desactivado: true
    },
    mounted(){
        jsreport.serverUrl = '{{env('REPORTER_URL', 'http://192.168.0.10:3001')}}';
    },
    watch: {
        usuario(){
            if(this.usuario == null){
                this.desactivado = true;
            }
            if(this.usuario != null){
                axios.get(`/users/${this.usuario.id}/datos`).then(response => { 
                    console.log(response); 
                    this.datos = response.data;
                    this.desactivado = false;
                }).catch(error => { console.log(error); });
            }
        }
    },
    methods: {
        buscarUsuario(search, loading) {
            loading(true);
            this.buscarUsuarioV(loading, search, this);
        },
        buscarUsuarioV: _.debounce((loading, search, vm) => {
            block();
            fetch(`/users/ficha/${escape(search)}`)
                .then(res => {
                    res.json()
                        .then(json => {
                            vm.usuarios = json.map(item => { 
                                item.nombre_completo = `${item.usuario} - ${item.persona.nombre} ${item.persona.primer_apellido} ${item.persona.segundo_apellido}`; 
                                return item; 
                            });
                            unblock();
                        });
                    loading(false);
                }).catch(error => {
                    unblock();
                });
        }, 350),
        imprimirFicha(){
            var request;
            if(this.datos.empleado.nivel.nivel != 'DIRECTOR' && this.datos.empleado.nivel.nivel != 'COORDINADOR'){
                request = {
                    template:{
                        shortid: this.datos.empleado.tiposempleado.tipo != 'BECAS' ? 'rkR1rm2UN' : 'ryKPp9T8E'
                    },
                    header: {
                        Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                    },
                    data:{
                        nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                        cargo: this.datos.empleado.nivel.nivel,
                        ads: this.datos.empleado.tiposempleado.tipo,
                        usuario: this.datos.usuario.usuario,
                        jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                        area: this.datos.empleado.area.nombre,
                        email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                        rfc: this.datos.empleado.rfc,
                        roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
                    }
                };
            }else{
                request = {
                    template:{
                        shortid: 'Bko6lnTLV'
                        // shortid: 'rkR1rm2UN'
                    },
                    header: {
                        Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                    },
                    data:{
                        nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                        cargo: this.datos.empleado.nivel.nivel,
                        ads: this.datos.empleado.tiposempleado.tipo,
                        usuario: this.datos.usuario.usuario,
                        jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                        area: this.datos.empleado.area.nombre,
                        email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                        rfc: this.datos.empleado.rfc,
                        roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
                    }
                };
            }
            //display report in the new tab
            jsreport.render('_blank', request);
        },
        mandarFicha(){
            var request;
            if(this.datos.empleado.nivel.nivel != 'DIRECTOR' && this.datos.empleado.nivel.nivel != 'COORDINADOR'){
                request = {
                    template:{
                        shortid: this.datos.empleado.tiposempleado.tipo != 'BECAS' ? 'SkDRh_erE' : 'BkR2MtxHE'
                    },
                    header: {
                        Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                    },
                    data:{
                        nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                        cargo: this.datos.empleado.nivel.nivel,
                        ads: this.datos.empleado.tiposempleado.tipo,
                        usuario: this.datos.usuario.usuario,
                        jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                        area: this.datos.empleado.area.nombre,
                        email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                        rfc: this.datos.empleado.rfc,
                        roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
                    }
                };
            }else{
                request = {
                    template:{
                        // shortid: 'SkDRh_erE'
                        shortid: 'rkR1rm2UN'
                    },
                    header: {
                        Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
                    },
                    data:{
                        nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                        cargo: this.datos.empleado.nivel.nivel,
                        ads: this.datos.empleado.tiposempleado.tipo,
                        usuario: this.datos.usuario.usuario,
                        jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                        area: this.datos.empleado.area.nombre,
                        email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                        rfc: this.datos.empleado.rfc,
                        roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
                    }
                };
            }
            //display report in the new tab
            // jsreport.render('_blank', request);
            block();
            jsreport.renderAsync(request).then(function(res) {
                swal('Great!', 'Se envio al correo', 'success');
                unblock();
            });
        }
    }
});
</script>
@endsection