@extends('layouts.app')

@push('head')

@endpush

@section('content-title', 'Usuario')
@section('content-subtitle', isset($user) ? 'Editar' : 'Nuevo')

@section('content')

@if (session('status'))
    <div class="alert {{ session('status') == 'ok' ? 'alert-success' : 'alert-danger' }}">
        {{ session('msg') }}
    </div>
@endif

<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="box box-primary shadow">
        <div class="box-header">
            <h3 class="box-title">{{ isset($user) ? 'Editar' : 'Crear' }} Usuario</h3>
        </div>
        <form role="form" action="{{ isset($user) ? route('users.update', $user) : route('users.store')}}" method="POST" id="form-vic">
            <div class="box-body">
                {{ csrf_field() }}
                {{ isset($user) ? method_field('PUT') : ''}}
                <input type="hidden" name="id" value="{{ $user->id }}">
                <input type="hidden" name="usuario_id" value="{{ Auth::user()->id }}">
                <input type="hidden" name="persona_id" value="{{ $user->persona->id }}">
                <div class="form-group has-feedback{{ $errors->has('nombre') ? ' has-error' : '' }}">
                    <label for="rol">Nombre</label>
                    @if( isset($user))
                        <input type="text" name="persona" class="form-control" id="rol" placeholder="Nombre Persona" disabled minlength="2" value="{{ $user->persona->nombreCompleto() }}">
                    @endif
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('nombre'))
                    <span class="help-block">{{ $errors->first('nombre') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('usuario') ? ' has-error' : '' }}">
                    <label for="usuario">Usuario</label>
                    <input type="text" name="usuario" class="form-control" id="usuario" placeholder="Tu usuario" minlength="2" value="{{ $user->usuario or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('usuario'))
                    <span class="help-block">{{ $errors->first('usuario') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Correo Electrónico</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="Tu correo" minlength="2" value="{{ $user->email or '' }}">
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Contraseña</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Si necesitas cambiarla ingresala aqui" minlength="2" >
                    <div class="help-block with-errors"></div>
                    @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>

            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">{{ isset($user) ? 'Guardar Cambios' : 'Guardar' }}</button>
                <button type="button" class="btn btn-warning pull-right" onclick="imprimirFicha()">Imprimir ficha de registro</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('other-scripts')
<script src="{{asset('/js/jsreport.js')}}"></script>
<script src="{{asset('/js/sweetalert2.all.js')}}"></script>
<script>
axios.get(window.location.href.replace('edit','datos')).then(response => { 
                    console.log(response); 
                    this.datos = response.data;
                    this.desactivado = false;
                }).catch(error => { console.log(error); });

$('#form-vic').validator();
jsreport.serverUrl = "{{env('REPORTER_URL', 'http://192.168.0.10:3001')}}";

function imprimirFicha(){
    if(this.datos==undefined){
        swal('Error','contacte a sistemas','error');
        return false;
    }

    var request;
    if(this.datos.empleado.nivel.nivel != 'DIRECTOR' && this.datos.empleado.nivel.nivel != 'COORDINADOR'){
        request = {
            template:{
                shortid: this.datos.empleado.tiposempleado.tipo != 'BECAS' ? 'rkR1rm2UN' : 'ryKPp9T8E'
            },
            header: {
                Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
            },
            data:{
                nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                cargo: this.datos.empleado.nivel.nivel,
                ads: this.datos.empleado.tiposempleado.tipo,
                usuario: this.datos.usuario.usuario,
                jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                area: this.datos.empleado.area.nombre,
                email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                rfc: this.datos.empleado.rfc,
                roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
            }
        };
    }else{
        request = {
            template:{
                shortid: 'Bko6lnTLV'
                // shortid: 'rkR1rm2UN'
            },
            header: {
                Authorization : 'Basic YWRtaW46MjFxd2VydHk0Mw=='
            },
            data:{
                nombre: `${this.datos.usuario.persona.nombre} ${this.datos.usuario.persona.primer_apellido} ${this.datos.usuario.persona.segundo_apellido}`,
                cargo: this.datos.empleado.nivel.nivel,
                ads: this.datos.empleado.tiposempleado.tipo,
                usuario: this.datos.usuario.usuario,
                jefe: `${this.datos.jefe.nombre} ${this.datos.jefe.primer_apellido} ${this.datos.jefe.segundo_apellido}`,
                area: this.datos.empleado.area.nombre,
                email: this.datos.usuario.email != null && this.datos.usuario.email != '' ? this.datos.usuario.email : 'N/A',
                rfc: this.datos.empleado.rfc,
                roles: this.datos.roles.map(item => { return { modulo: item.modulo.nombre, rol: item.rol.nombre }; })
            }
        };
    }
    //display report in the new tab
    jsreport.render('_blank', request);
}
</script>
@endsection