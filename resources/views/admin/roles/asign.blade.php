@extends('layouts.app')

@section('mystyles')
<link href="{{ asset('css/sweetalert2.min.css') }}" rel="stylesheet"/>
<link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
<style>
.fade-enter-active, .fade-leave-active {
  transition: opacity .5s;
}
.fade-enter, .fade-leave-to /* .fade-leave-active below version 2.1.8 */ {
  opacity: 0;
}
.select2-container--default .select2-selection--single{
  border-radius: 0px;
}
.select2-container .select2-selection--single{
}
</style>
@stop

@section('li-breadcrumbs')
  <li class="active">Asignar Roles</li>
@endsection

@section('content')
    <div id="app">
        <h1>Asignar Roles</h1>
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary shadow">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form">
                <div class="box-body">

                    <div class="form-group has-feedback">
                        <label for="modulo">Modulo</label>
                        <select class="form-control" style="width: 100%;" name="tiposempleado_id" v-model="modulo.id" @change="cargarRoles()">
                            @if(isset($modulo))
                                <option value="{{ $modulo->id }}">{{ $modulo->nombre }}</option>
                            @elseif(isset($modulos))
                                @foreach($modulos as $modulo)
                                    <option selected value="{{ $modulo->id }}">{{ $modulo->nombre }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="row">

                        <div class="form-group col-sm-6">
                            <label for="rol">Rol</label>
                            <select class="form-control" style="width: 100%;" v-model="rol.id">
                                <option v-for="rol in roles" :value="rol.id">@{{ rol.nombre }}</option>
                            </select>
                        </div>
                        
                        <div class="form-group col-sm-6">
                            <label for="rol">Usuario</label>
                            <select class="form-control select2" id="usuarios" style="width: 100%;" v-model="usuario.id" @click="selectUsuario(this)">
                                <option>Selecciona...</option>
                            </select>
                        </div>

                    </div>

                    <div class="checkbox">
                        <label for="autorizado">
                            <input type="checkbox" id="autorizado" v-model="autorizado">Autorizar?
                        </label>
                    </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary" @click.prevent="guardarRol()" :disabled="onTransaction">
                    <i class="fa fa-spinner fa-spin" v-if="onTransaction"></i> Agregar Rol
                </button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
    </div>
@stop

@section('other-scripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<!-- <script src="{{ asset('js/vue-router.js') }}"></script> -->
<script src="{{ asset('js/sweetalert2.all.js') }}"></script>
<script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}"></script>
<script>
var app = new Vue({
    el: '#app',
    data: {
        modulo: {
            id: 0,
            nombre: ''
        },
        rol: {
            id: 0
        },
        usuario: {
            id: 0,
            nombre: ''
        },
        autorizado: false,
        onTransaction: false,
        roles: []
    },
    computed: {
    },
    methods: {
        cargarRoles(){
            axios.get('/roles/modulos/' + this.modulo.id)
               .then(response => {
                    this.roles = response.data;
                })
                .catch(error => { 
                    console.log(error); 
                });
        },
        selectUsuario(select){
            console.log(select);
        },
        guardarRol(){
            this.onTransaction = true;
            axios.post('/usuarios/roles/add', {
                usuario_id: this.usuario.id,
                rol_id: this.rol.id,
                modulo_id: this.modulo.id,
                autorizado: this.autorizado
            }).then(response => { console.log(response);
                this.onTransaction = false;
                this.usuario.id = 0;
                this.rol.id = 0;
                this.modulo.id = 0;
                this.autorizado = 0;
                $('#usuarios').empty();
                swal({
                    type: 'success',
                    title: 'Great...',
                    text: 'Se guardo el registro correctamente!'
                });
            }).catch(error => { 
                console.log(error);
                this.onTransaction = false;
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Algo salio mal!',
                    footer: '',
                });
            });
        }
    }
});
$('.select2').select2({
    minimumInputLength: 3,
    ajax: {
    url: '/users/find/select',
    dataType: 'JSON',
    type: 'GET',
    data: function(params) {
        return {
            search: params.term
        };
    },
    processResults: function(data, params) {
        params.page = params.page || 1;
        return {
            results: $.map(data, function(item) {
                return {
                    id: item.id,
                    text: item.usuario,
                    slug: item.persona_id,
                    results: item
                }
            })
        };
    },
    cache: true
    }
});
// Bind an event
$('.select2').on('select2:select', function (e) { 
    console.log('select event');
    console.log($('.select2').val());
    app.$data.usuario.id = $('.select2').val();
});
</script>
@stop