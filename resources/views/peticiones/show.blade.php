@php
    $layout = isset($modulo) ? "$modulo::" : '';
@endphp
@extends("{$layout}layouts.master")

@push('head')
  <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.fix.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/dataTables.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/datatables.net-responsive/css/responsive.bootstrap.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('css/personas/app.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/sweetalert2/sweetalert2.min.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('css/peticiones/timeline.css') }}" type="text/css" rel="stylesheet">
  <link href="{{ asset('plugins/iCheck/all.css') }}" type="text/css" rel="stylesheet">

  @includeIf("peticiones.custom-seguimineto-styles")

@endPush

@section('content-title', 'Petición')
@section('content-subtitle', "{$peticion->solicitud->folio}")
@section('li-breadcrumbs')
  {{-- <li><a href="{{ route($modulo.'.peticiones.index') }}">Peticiones</a></li> --}}
  <li class="active">Seguimiento</li>
@endSection

@section('content')
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Datos de la petición
        @if($peticion->estadoActual() === "SOLICITANTE")
        <a href="{{route('solicitudes.edit',$peticion->solicitud->id)}}" data-toggle="tooltip" data-placement="right" title="Editar" type="button" style="margin-left: 15px;" class="btn btn-warning pull-right btn-xs">
          <i class="fa fa-edit"></i></a>
        @endif
      </h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>        
    <div class="box-body" style="">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:30%">
                  <i class="fa fa-id-card margin-r-5"></i>
                  Folio
                </th>
                <td>{{ $peticion->solicitud->folio }}</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-user margin-r-5"></i>
                  Remitente
                </th>
                <td>
                  @if($peticion->solicitud->solicitudpersonales)
                  {{ $peticion->solicitud->solicitudpersonales->persona->nombreCompleto() }}
                  @elseif($peticion->solicitud->solicitudmunicipales)
                  {{ $peticion->solicitud->solicitudmunicipales->persona->get_nombre_completo() }}
                  <br>
                  {{ $peticion->solicitud->solicitudmunicipales->municipio->nombre }}
                  @elseif($peticion->solicitud->solicitudregionales)
                  {{ $peticion->solicitud->solicitudregionales->persona->get_nombre_completo() }}
                  <br>
                  {{ $peticion->solicitud->solicitudregionales->region->nombre }}
                  @elseif($peticion->solicitud->solicituddependencias)
                  {{ $peticion->solicitud->solicituddependencias->dependencia->nombre }}
                  @endif
                </td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-tags margin-r-5"></i>
                  Tipo
                </th>
                <td>{{ $peticion->solicitud->tiposremitente->tipo }}</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-file-text-o margin-r-5"></i>
                  Asunto
                </th>
                <td>{{ $peticion->solicitud->asunto }}</td>
              </tr>
              @if($peticion->estadoActual() == "CANCELADO" || $peticion->estadoActual() == "RECHAZADO")
              <tr>
                <th style="width:30%">
                  <i class="fa fa-id-card margin-r-5"></i>
                  @if($peticion->estadoActual() == "CANCELADO")
                  Motivo de cancelación
                  @else
                  Motivo de rechazo
                  @endif
                </th>
                <td>
                  @if($peticion->estadoActual() == "CANCELADO")
                  {{ $peticion->estadosSolicitud->where("statusproceso_id",7)->first()->motivoprograma->motivo->motivo }}
                  @else
                  {{ $peticion->estadosSolicitud->where("statusproceso_id",6)->first()->motivoprograma->motivo->motivo }}
                  @endif
                </td>
              </tr>
              @endif
            </table>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:40%">
                  <i class="fa fa-calendar margin-r-5"></i>
                  Fecha de vinculación
                </th>
                <td>
                  @if($peticion->estadosSolicitud->where("statusproceso_id",3)->first())
                  {{ $peticion->estadosSolicitud->where("statusproceso_id",3)->first()->created_at }}
                  @endif
                </td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-pencil-square-o margin-r-5"></i>
                  Apoyo Solicitado
                </th>
                <td id="programa">{{ $peticion->beneficio->nombre }}</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-list-ol margin-r-5"></i>
                  Cantidad
                </th>
                <td>{{ $peticion->cantidad }}</td>
              </tr>
              <tr>
                <th>
                  <i class="fa fa-signal margin-r-5"></i>
                  Estatus
                </th>
                <td id="lblStatus">                                    
                  {{ $peticion->estadoActual() }}    
                  @if($peticion->estadoActual() !== "FINALIZADO" && $peticion->estadoActual() !== "CANCELADO" && $peticion->estadoActual() !== "RECHAZADO")
                  <button id="btn_bitacora" type="button" class="btn btn-primary pull-right btn-sm" onclick="abrir_modal('modal-bitacora')">
                    <i class="fa fa-edit"></i>
                    BITÁCORA
                  </button>
                  @endif                                    
                </td>
              </tr>
              @if($peticion->estadoActual() == "CANCELADO" || $peticion->estadoActual() == "RECHAZADO")
              <tr>
                <th style="width:40%">
                  <i class="fa fa-calendar margin-r-5"></i>
                  Observación:
                </th>
                <td>
                  @if($peticion->estadoActual() == "CANCELADO")
                  {{ $peticion->estadosSolicitud->where("statusproceso_id",7)->first()->observacion }}
                  @else
                  {{ $peticion->estadosSolicitud->where("statusproceso_id",6)->first()->observacion }}
                  @endif
                </td>
              </tr>
              @endif
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if ($peticion->estadoActual() == 'FINALIZADO')
    @includeFirst(["peticiones.custom-show", 'peticiones.seguimiento'])
  @else
    @includeFirst(["peticiones.custom-seguimiento", 'peticiones.seguimiento'])
  @endif
    
  <div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Progreso</h3>
      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>        
    <div class="box-body" style="">      
      @foreach ($timeline as $time)
      <div class="row">
        <div class="col-md-12">        
          <div style="display:inline-block;width:100%;overflow-y:auto;">
            <ul class="timeline timeline-horizontal" style="margin-top: 10px;">
              @foreach ($time['lineas'] as $line)
              @php
              $color = 'warning';
              switch ($line['status']) {
                case 'SOLICITANTE':
                $color = 'info';
                break;
                case 'VINCULADO':
                $color = 'success';
                break;
                case 'Vo. Bo.':
                $color = 'success';
                break;
                case 'VALIDANDO':
                $color = 'warning';
                break;
                case 'LISTA DE ESPERA':
                $color = 'warning';
                break;
                case 'FINALIZADO':
                $color = 'primary';
                break;
                
                default:
                $color = 'danger';
                break;
              }
              @endphp
              <li class="timeline-item">
                <div class="timeline-badge {{ $color }}"><i class="glyphicon glyphicon-check"></i></div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 class="timeline-title">{{ $line['status'] }}</h4>
                    <div class="timeline-body">
                      <p>{{ $line['observacion'] }}</p>
                    </div>
                    <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> {{ $line['fecha'] }}
                      <br>{{ $line['usuario'] }}
                    </small></p>
                  </div>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
    
  <form id="form_bitacora" method="POST" action="#">
    <div class="modal fade" id="modal-bitacora">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header btn-danger">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Bitácora</h4>
          </div>
          <div class="modal-body">                        
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">                    
                <label>Estatus actual: </label>
                <span>{{ $peticion->estadoActual() }}</span>
                <button type="button" class="btn btn-warning pull-right btn-sm" onclick="editar_estatus();"><i class="fa fa-edit"></i></button>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="estatus">Nuevo estatus:</label>
                <select class="form-control select2" id="estatus" name="estatus" style="width: 100%;">
                  @foreach($estatus as $estado)
                  <option value="{{$estado->id}}" {{ ($peticion->estadoActual() === $estado->status) ? 'selected' : '' }}>{{ $estado->status }}</option>
                  @endforeach
                </select>
              </div>
            </div> 
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="motivo_bitacora">Motivo:</label>
                <select class="form-control select2" id="motivo_bitacora" name="motivo_bitacora" style="width: 100%;">
                  <option selected value="">SELECCIONE UNA OPCIÓN</option>
                  @foreach($motivos as $motivo)
                  <option value="{{ $motivo->id }}">{{ $motivo->motivo }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="observacion">Observación:</label>
                <textarea id="observacion" name="observacion" maxlength="255" class="form-control" rows="3" style="resize: none"></textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>
            <button type="button" class="btn btn-success" onclick="guardar_bitacora()"><i class="fa fa-check"></i> Aceptar</button>
          </div>
        </div>
      </div>
    </div>
  </form>    
  
  @if(isset($modulo) && $modulo === 'afuncionales' && $peticion->estadoActual() == "LISTA DE ESPERA")
    @include('afuncionales::peticiones.modals_entrega')
  @endif
  
  @stop

@push('body')
<script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('bower_components/select2/dist/js/es.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/jquery.validate.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/additional-methods.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Spanish.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Defaults.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/jquery-validate/1.17.0/Metodos.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/sweetalert2/sweetalert2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('plugins/iCheck/icheck.min.js') }}"></script>
  
  @includeFirst(["peticiones.custom-seguimiento-scripts",'peticiones.seguimiento-scripts'])
  
  <script>    
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')  } });
    
    
    (() => {
      $("#estatus").parent().parent().hide();
      $("#motivo_bitacora").parent().parent().hide();        
      
      $("#estatus").select2({
        language: 'es',
        placeholder: 'SELECCIONE UN MOTIVO',
        minimumResultsForSearch: Infinity
      }).change(evt => {      
        if($('#estatus').find("option:selected").text() == "CANCELADO" || $('#estatus').find("option:selected").text() == "RECHAZADO"){
          $("#motivo_bitacora").parent().parent().show();
        }else{
          $("#motivo_bitacora").parent().parent().hide();
        }
      });
      
      $("#motivo_bitacora").select2({
        language: 'es',
        minimumResultsForSearch: Infinity			
      });
      
      $("#motivo").select2({
        language: 'es',
        placeholder: 'SELECCIONE UN MOTIVO',
        minimumResultsForSearch: Infinity			
      }).change(function(event) {
        $("#motivo").valid();				
      });
    })() 
     
    var abrir_modal = (modal, persona_id, nombre) => {
      $('#estatus option:eq(0)').prop('selected',true);
      $('#estatus').trigger('change.select2');
      $("#estatus").parent().parent().hide();
      $("#motivo_bitacora").parent().parent().hide();
      $('#beneficiario').text(nombre);
      $('#motivo').val(null).trigger('change');
      $('#modal-cancelar button.btn.btn-success').off('click');
      $('#modal-cancelar button.btn.btn-success').on('click', () => {
        if($('#form_cancelar').valid()){
          cancelar_beneficiario(persona_id);
        }
      });
      $('#'+modal).modal('show');
    }
    var guardar_bitacora = () => {
      if($('#form_bitacora').valid()){
        block();
        $.ajax({
          url: window.location.href,
          type: 'PUT',
          data: $('#form_bitacora').serialize(),
          success: function(response) {
            $('#modal-bitacora').modal('hide');                
            unblock();
            location.reload();
          },
          error: function(response){
            unblock();
            if(response.status === 422) {                        
              swal({
                title: 'Error al cambiar el estatus',				
                text: response.responseJSON.errors[0].message,
                type: 'error',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Regresar',
                allowEscapeKey: false,
                allowOutsideClick: false
              });
            }else{
              swal(
              'Error',
              '',
              'error'
              )
            }                                
          }
        });
      }
    }
    var editar_estatus = () => {
      $("#estatus").parent().parent().show();
    }
    
  </script>
  
  @if(isset($modulo) && $modulo === 'afuncionales')
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js') }}"></script>    
    @include('afuncionales::peticiones.js.beneficiarios')
  @endif


@endpush