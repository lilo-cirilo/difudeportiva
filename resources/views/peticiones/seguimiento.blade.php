<div class="box box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Lista de beneficiarios</h3>
    <div class="box-tools pull-right">
      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
  </div>        
  <div class="box-body" style="">
    <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
      <input type="text" id="buscar_beneficiario" class="form-control">
      <span class="input-group-btn">
        <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_beneficiario">Buscar</button>
      </span>
    </div>
    {!! $beneficiarios->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'beneficiarios', 'name' => 'beneficiarios', 'style' => 'width: 100%']) !!}
  </div>
</div>

<div class="modal fade" id="modal-beneficiario">
  <div class="modal-dialog modal-lg"></div>
</div>

<div class="modal fade" id="modal-personas">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-title-personas">
          Buscar beneficiario
        </h4>
      </div>
      <div class="modal-body" id="modal-body-personas">    
        <div class="input-group input-group-sm col-xs-12 col-sm-12 col-md-12 col-lg-4 pull-right">
          <input type="text" id="buscar_persona" class="form-control">
          <span class="input-group-btn">
            <button type="submit" class="btn btn-info btn-flat" id="btn_buscar_persona">Buscar</button>
          </span>
        </div>
        {!! $personas->html()->table(['class' => 'table table-bordered table-hover data-table dataTable dtr-inline', 'id' => 'personas', 'name' => 'personas', 'style' => 'width: 100%']) !!}
      </div>
      <div class="modal-footer" id="modal-footer-personas">
        <div class="pull-right">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Cerrar</button>
          <button type="button" class="btn btn-success" onclick="agregar_persona();"><i class="fa fa-plus"></i> Agregar</button>                    
        </div>
      </div>      
    </div>
  </div>
</div>

<div class="modal fade" id="modal-persona">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="modal-title-persona"></h4>
      </div>
      <div class="modal-body" id="modal-body-persona"></div>
      <div class="modal-footer" id="modal-footer-persona"></div>      
    </div>
  </div>
</div>

<form id="form_cancelar" method="POST" action="#"> 
  <div class="modal fade" id="modal-cancelar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn-danger">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title">¿Está seguro de cancelar al beneficiario?</h4>
        </div>
        <div class="modal-body">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">                    
              <label>Beneficiario: </label>
              <span id="beneficiario"></span>
            </div>
          </div>                
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
              <label for="motivo">Motivo de cancelación:</label>
              <select class="form-control select2" id="motivo" name="motivo" style="width: 100%;">                    
                <option selected value="">SELECCIONE UNA OPCIÓN</option>
                @foreach($motivos as $motivo)
                <option value="{{ $motivo->id }}">{{ $motivo->motivo }}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>
          <button type="button" class="btn btn-success"><i class="fa fa-check"></i> Aceptar</button>
        </div>
      </div>
    </div>
  </div>
</form>