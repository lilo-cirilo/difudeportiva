<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/responsive.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.rowGroup.min.js') }}"></script>
<script type="text/javascript" src="/vendor/datatables/buttons.server-side.js"></script>
@include('personas.js.persona')
{!! $beneficiarios->html()->scripts() !!}
{!! $personas->html()->scripts() !!}

<script>
  var tab;
  
  var init_tabs = () => {
    $('#modal-beneficiario').on('shown.bs.modal', function (event) {
      $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        tab = $(e.target).attr("href");            
        if(tab === "#tab_Discapacidad" || tab === "#tab_Expediente"){
          $('#btn_editar_beneficiario').show();
        }else{
          $('#btn_editar_beneficiario').hide();
        }
      });
    });
  }    
  
  var datatable_beneficiarios;

  var init_modal_personas = () => {
    var table = $('#personas').dataTable();
    datatable_personas = $(table).DataTable();
    $('#buscar_persona').keypress(function(e) {
      if(e.which === 13) {
        datatable_personas.search($('#buscar_persona').val()).draw();
      }
    });
    $('#btn_buscar_persona').on('click', function() {
      datatable_personas.search($('#buscar_persona').val()).draw();
    });
    $('#modal-personas').on('shown.bs.modal', function(event) {
      datatable_personas.ajax.reload();
    });
  }
  var init_modal_beneficiarios = () => {
    
    $('#modal-beneficiario').on('shown.bs.modal', function (event) {
      if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
        init_tabla_apoyos();
      }        
    });
    
    $('#form_cancelar').validate({
      rules: {
        motivo: {
          required: true
        }				
      }
    });
    $('#form_bitacora').validate({
      rules: {
        estatus: {
          required: true
        },
        observacion: {
          required: false
        },
        motivo_bitacora: {
          required:{ 
            depends: function(element) {
              return ($('#estatus').find("option:selected").text() == "CANCELADO" || $('#estatus').find("option:selected").text() == "RECHAZADO");
            }
          }
        }
      }                
    });
    var table = $('#beneficiarios').dataTable();
    datatable_beneficiarios = $(table).DataTable();
    $('#buscar_beneficiario').keypress(function(e) {
      if(e.which === 13) {
        datatable_beneficiarios.search($('#buscar_beneficiario').val()).draw();
      }
    });
    $('#btn_buscar_beneficiario').on('click', function() {
      datatable_beneficiarios.search($('#buscar_beneficiario').val()).draw();
    });
    datatable_beneficiarios.on( 'xhr', function () {
      var data = datatable_beneficiarios.ajax.json();
      
      if(data.permiso_solo_lectura){
        $("#btn_bitacora").remove();
      }
      if(data.permiso_solo_lectura || data.estatus === 'CANCELADO' || data.estatus === 'FINALIZADO' || data.estatus === 'RECHAZADO' || data.modulo === 'afuncionales'){
        $(".nuevo-beneficiario").hide().removeClass("button-dt");              
      }else{
        $(".nuevo-beneficiario").show().addClass("button-dt");
      }
    });        
  }
  var init_tabla_apoyos = () => {
    datatable_apoyos = $("#apoyos-otorgados").DataTable({
      language: {
        url: "{{ asset('bower_components/datatables.net-responsive/js/Spanish.json') }}"
      },
      dom: 'itp',
      lengthMenu: [ [5], [5] ],
      responsive: true,
      autoWidth: true,
      processing: true,
      columnDefs: [
      { className: 'text-center', 'targets': '_all' }
      ]
    });            
  } 
  var seleccionar_persona = (id, nombre) => {
    block();
    $.ajax({
      url: window.location.href + '/beneficiarios',
      type: 'POST',
      data: "persona_id="+id,
      success: function(response) {
        $('#modal-personas').modal('hide');     
        datatable_beneficiarios.ajax.reload(null, false);
        unblock();
        swal(                            
        '¡Correcto!',
        'Beneficiario registrado',
        'success'
        )                                                  
      },
      error: function(response){
        unblock();
        swal(
        'Error',
        response.responseJSON.errors[0].message,
        'error'
        )               
      }
    });
  }
  var mostrar_persona = (persona_id, modulo, callback) => {
    block();
    var url = window.location.href + '/beneficiarios/' + persona_id;
    $.get(url, function(data) {            
      $('#modal-beneficiario').html(data.beneficiario);            
      $('#modal-beneficiario').modal('show');
      $('#btn_editar_beneficiario').off('click');
      $('#btn_editar_beneficiario').on('click',() => {
        Beneficiario.editar(persona_id);
      });
      
      $('#btn_editar_beneficiario').next().hide();
      
      if(callback){                
        callback();
      }
      unblock();
    });
  }
  var cancelar_beneficiario = (persona_id) => {
    swal({
      title: '¿Está seguro de cancelar al beneficiario?',
      text: "",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, cancelar',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        block();
        $.ajax({
          url: window.location.href + '/beneficiarios/' + persona_id,
          type: 'DELETE',
          data: 'motivo='+$('#motivo').select2('data')[0].text,
          success: function(response) {
            unblock();
            datatable_beneficiarios.ajax.reload(null, false);
            $('#modal-cancelar').modal('hide');
            swal(        
            'Beneficiario cancelado',
            '',
            'success'
            )
          },
          error: function(response) {
            unblock();
            swal(        
            'Error',
            '',
            'error'
            )
          }
        });		   
      }
    });
  }
  var agregar_persona = ()  => {
    block();
    $.get('/personas/search?tipo=create_edit', function(data) {
    })
    .done(function(data) {
    })
    .fail(function(data) {			
      $("#modal-title-persona").text('Agregar Persona:');
      $("#modal-body-persona").html(data.responseJSON.html);
      $("#modal-footer-persona").html(
      '<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i> Regresar</button>'+
      '<button type="submit" class="btn btn-success" onclick="persona.create_edit();"><i class="fa fa-database"></i> Guardar</button>'                
      );
      app.to_upper_case();
      persona.init();
      persona.editar_fotografia();
      persona.agregar_fecha_nacimiento();
      persona.agregar_inputmask();
      persona.agregar_select_create();
      persona.agregar_validacion();         
      unblock();   
      $("#modal-persona").modal('show');
    });
  }
  var persona_create_edit_success = (response) => {
    app.set_bloqueo(false);
    unblock();
    $('#modal-personas').modal("hide");
    $('#modal-persona').modal("hide");                
    seleccionar_persona(response.id);
  }
  var persona_create_edit_error = (response) => {
    unblock();
    if(response.status === 422) {
      swal({
        title: 'Error al registrar al solicitante',				
        text: response.responseJSON.errors[Object.keys(response.responseJSON.errors)[0]][0],
        type: 'error',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Regresar',
        allowEscapeKey: false,
        allowOutsideClick: false
      });
    }
  }
  var Beneficiario = (() => {
    var editar = (persona_id) => {
      
      if(tab === "#tab_Discapacidad"){
        block();
        var url = window.location.href + '/beneficiarios/' + persona_id + '/discapacidad/0/edit';
        $.get(url, function(data) {
          $("#tab_Discapacidad .row").first().append(data.discapacidad);
          $("#tab_Discapacidad .row").first().children().first().hide();
          $('#btn_editar_beneficiario').hide();                    
          $('#modal-footer-beneficiario').html(
          '<button type="submit" class="btn btn-success" onclick="Beneficiario.guardar_discapacidad();"><i class="fa fa-database"></i> Guardar</button>'+
          '<button type="button" class="btn btn-danger" onclick="Beneficiario.cancelar_discapacidad(' + persona_id + ');"><i class="fa fa-reply"></i> Regresar</button>'
          )
          discapacidad_init();
          unblock();
        });          
      } else if(tab === "#tab_Expediente"){
        block();
        var url = window.location.href + '/beneficiarios/' + persona_id + '/edit';
        $.get(url, function(data) {
          $("#tab_Expediente .row").first().append(data.expediente);
          $("#tab_Expediente .row").first().children().first().hide();
          $('#btn_editar_beneficiario').hide();                    
          $('#modal-footer-beneficiario').html(
          '<button type="submit" class="btn btn-success" onclick="Beneficiario.guardar_expediente(' + persona_id + ');"><i class="fa fa-database"></i> Guardar</button>'+
          '<button type="button" class="btn btn-danger" onclick="Beneficiario.cancelar_expediente(' + persona_id + ');"><i class="fa fa-reply"></i> Regresar</button>'
          )
          expediente_init();
          unblock();
        });
      }
    }
    
    var discapacidad_init = () => {
      $('#form-persona-funcional').validate({
        rules: 	{
          discapacidad_id: {
            required: true
          },
          limitacion_id: {
            required: true
          },
          peso: {
            required: true,
            pattern_peso: 'Ingrese sólo números'
          },
          altura: {
            required: true,
            pattern_altura: 'Ingrese sólo números'
          },
          tiempodiscapacidad: {
            required: true,
            pattern_años: 'Ingrese sólo números'
          },
          tiposilla: {
            required: true
          },
          terreno: {
            required: true
          }
        }
      });
      
      $('#limitacion_id, #tiposilla, #terreno, #motivo').select2({
        language: 'es',
        placeholder: 'SELECCIONE UNA OPCIÓN',
        minimumResultsForSearch: Infinity
      }).change(function(event) {
        $(this).valid();
      });
      
      $('#discapacidad_id').select2({
        language: 'es',
        ajax: {
          url: '{{ route('discapacidades.select') }}',
          dataType: 'JSON',
          type: 'GET',
          delay: 500,
          data: function(params) {
            return {
              search: params.term
            };
          },
          processResults: function(data, params) {
            var datos = [];
            for(var j=0; j<data.length; j++){
              var found = false;
              for(var i=0; i<datos.length; i++){
                if(datos[i].text == data[j].padre){
                  datos[i].children.push({
                    "id": data[j].id,
                    "text": data[j].nombre                                    
                  });
                  found = true;
                  break;
                }
              }
              if(!found){
                datos.push({
                  text: data[j].padre,
                  children: [{
                    "id": data[j].id,
                    "text": data[j].nombre
                  }]
                });
              }
            }
            params.page = params.page || 1;
            
            return {
              results: datos
            };
          },
          cache: true
        }
      }).change(function(event) {
        $('#discapacidad_id').valid();
      });
    }
    
    var expediente_init = () => {
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass: 'iradio_flat-green'
      });
    }
    
    var guardar_discapacidad = () => {
      var form = $('#form-persona-funcional');
      if(form.valid()) {
        block();
        $.ajax({
          url: form.attr('action'),
          type: form.attr('method'),
          data: form.serialize(),
          success: function(response) {
            mostrar_persona(response.persona_id, 'afuncionales', () => {                            
              init_tabs();                            
              if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
                init_tabla_apoyos();
                $('#modal-beneficiario .nav.navbar-nav a[href="#tab_Discapacidad"]').tab('show');
                $('#modal-footer-beneficiario').html(
                '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + response.persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
                );                            
              }
            });
            datatable_beneficiarios.ajax.reload(null, false);                        
            unblock();                    
            swal(        
            'Información registrada',
            '',
            'success'
            )          
          },
          error: () => {
            unblock();
            swal(        
            'Error',
            '',
            'error'
            )
          }	    
        });
      }
    }
    
    var guardar_expediente = (persona_id) => {
      var form = $('#form-persona-expediente');
      var url = window.location.href + '/beneficiarios/' + persona_id;
      block();
      $.ajax({
        url: url,
        type: 'PUT',
        data: form.serialize(),
        success: function(response) {
          mostrar_persona(response.persona_id, 'afuncionales', () => {                        
            init_tabs();
            if ( ! $.fn.DataTable.isDataTable( '#apoyos-otorgados' ) ) {
              init_tabla_apoyos();
            }
            $('#modal-beneficiario .nav.navbar-nav a[href="#tab_Expediente"]').tab('show');
            $('#modal-footer-beneficiario').html(
            '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + response.persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
            );
          });
          datatable_beneficiarios.ajax.reload(null, false);                        
          unblock();                    
          swal(        
          'Información registrada',
          '',
          'success'
          )          
        },
        error: () => {
          unblock();
          swal(
          'Error',
          '',
          'error'
          )
        }
      });
      
    }
    
    var cancelar_discapacidad = (persona_id) => {
      $("#tab_Discapacidad .row").first().children().first().show();
      $("#tab_Discapacidad .row").first().children().last().remove();
      $('#modal-footer-beneficiario').html(
      '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
      );
    }
    
    var cancelar_expediente = (persona_id) => {
      $("#tab_Expediente .row").first().children().first().show();
      $("#tab_Expediente .row").first().children().last().remove();
      $('#modal-footer-beneficiario').html(
      '<button type="button" style="border-radius: 24px;" class="btn btn-warning" onclick="Beneficiario.editar(' + persona_id + ')" id="btn_editar_beneficiario"><i class="fa fa-pencil"></i> Editar</button>'
      );
    }
    
    return {
      editar,
      guardar_discapacidad,
      guardar_expediente,
      cancelar_discapacidad,
      cancelar_expediente
    }
  })();
  $(document).ready(function() {
      //init();
      init_tabs();
      init_modal_personas();
      init_modal_beneficiarios();
    });
</script>