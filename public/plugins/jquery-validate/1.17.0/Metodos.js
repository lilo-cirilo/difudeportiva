$.validator.addMethod('pattern_nombre', function (value, element) {
  return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$/.test(value);
}, 'Solo palabras y espacio entre palabras.');

$.validator.addMethod('pattern_dependencia', function (value, element) {
  return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ]*)(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,10}$/.test(value);
}, 'Solo palabras y espacio entre palabras.');

$.validator.addMethod('pattern_nombre_dispersion', function (value, element) {
  return this.optional(element) || /^(?:[a-záéíóúñA-ZÁÉÍÓÚÑ0-9]+)(?:\/[a-záéíóúñA-ZÁÉÍÓÚÑ0-9]+)*$/.test(value);
}, 'Numeros y letras separados por diagonales');

$.validator.addMethod('pattern_apellido', function (value, element) {
  return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{1,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
  //return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{2,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
}, 'Solo palabras y espacio entre palabras, si no dispone del apellido coloque X.');

/*$.validator.addMethod('pattern_apellido', function(value, element) {
	return this.optional(element) || /(?:^[Xx]$)|(?:^[a-záéíóúñA-ZÁÉÍÓÚÑ]{2,}(?:\s[a-záéíóúñA-ZÁÉÍÓÚÑ]+){0,5}$)/.test(value);
}, 'Solo palabras y espacio entre palabras, si no dispone del apellido coloque X.');*/

$.validator.addMethod('pattern_curp', function (value, element) {
  return this.optional(element) || /^[A-Za-z]{1}[AEIOUXaeioux]{1}[A-Za-z]{2}[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[HMhm]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE|as|bc|bs|cc|cs|ch|cl|cm|df|dg|gt|gr|hg|jc|mc|mn|ms|nt|nl|oc|pl|qt|qr|sp|sl|sr|tc|ts|tl|vz|yn|zs|ne)[B-DF-HJ-NP-TV-Z-b-df-hj-np-tv-z]{3}[0-9A-Za-z]{1}[0-9]{1}$/.test(value);
}, 'Código alfanumérico único de identidad de 18 caracteres.');

$.validator.addMethod('pattern_numero', function (value, element) {
  return this.optional(element) || /\d+|(?:S\/N)|(?:s\/n)/.test(value);
}, 'Solo numeros, si no dispone de uno coloque S/N.');

$.validator.addMethod('pattern_telefono', function (value, element) {
  return this.optional(element) || /^\(\d{2,3}\)\s\d+(?:-\d+)+$/.test(value);
}, 'Siga el patrón.');
//El pattern importe se ocupa en Bienestar y en Apoyos Funcionales
//Antes de modificar avisen paps
$.validator.addMethod('pattern_importe', function (value, element) {
  return this.optional(element) || /^[0-9]*\.?[0-9]+$/.test(value);
}, 'Solo numeros, enteros o decimales');

$.validator.addMethod('pattern_integer', function (value, element) {
  return this.optional(element) || /^\d+$/.test(value);
}, 'Solo numeros.');

$.validator.addMethod('pattern_años', function (value, element) {
  return this.optional(element) || /^\d{1,3}$/.test(value);
}, "Ingrese un número correcto, ej. 4");

$.validator.addMethod('pattern_peso', function (value, element) {
  return this.optional(element) || /^\d+(\.\d{0,2})?$/.test(value);
}, "Ingrese un número correcto, ej. 70.00");

$.validator.addMethod('pattern_altura', function (value, element) {
  return this.optional(element) || /^\d{1}(\.\d{0,2})?$/.test(value);
}, "Ingrese un número correcto, ej. 1.70");

$.validator.addMethod('pattern_folios', function (value, element) {
  return this.optional(element) || /^\d+|(?:X)|(?:x)$/.test(value);
}, "Solo numeros, X si no dispone de uno.");

$.validator.addMethod('pattern_numoficio', function (value, element) {
  return this.optional(element) || /^\d+|(?:X)|(?:x)$/.test(value);
}, "Solo numeros, X si no dispone de uno.");

$.validator.addMethod('pattern_telefono_dependencia', function (value, element) {
  return this.optional(element) || /^\d+$/.test(value);
}, "Ingrese sólo digitos");