Vue.use(VeeValidate);
var solicitud = new Vue({
	
	el:'#dsolicitud',
	data:{
		email: '',
        name: '',
        phone: '',
        url: '',
		pendienteDatos :{
			rutaImagen  : "",
			status 	 	: "",
			idPendiente : ""

		},
		listaPendientes 	: "",
		// listaEstatus 		: ""
		modeSave			:false,
		pagination:{
            'total'         :0, 
            'current_page'  :0, 
            'per_page'      :0, 
            'last_page'     :0, 
            'from'          :0, 
            'to'            :0
        },
        offset              :3,
        busqueda 	:{
        	nombre 	: "",
        	curp 	: "", 
        }
	},
	mounted(){
		this.pendientesLista()
	},
	computed:{
        isActived:function(){
            return this.pagination.current_page;
        },
        pagesNumber: function(){
            if(!this.pagination.to){
                return[];
            }
            var from = this.pagination.current_page - this.offset;
            if(from < 1){
                from = 1;
            }
            var to = from + (this.offset*2);
            if(to >= this.pagination.last_page){
                to = this.pagination.last_page;

            }

            var pageArray = []
            while(from <= to){
                pageArray.push(from);
                from++;
            }
            return pageArray;
        }
    },
	methods:{
		 validateBeforeSubmit() {
              this.$validator.validateAll().then((result) => {
                if (result) {
                  // eslint-disable-next-line
                  alert('Form Submitted!');
                  return;
                }

                alert('Correct them errors!');
              });
            },
		changePage: function(page){
            this.pagination.current_page = page;
            this.pendientesLista(page)
        },
		resetDatos:function(){
			let me = this
			me.pendienteDatos.rutaImagen 	= "",
			me.pendienteDatos.status 		= "",
			me.pendienteDatos.idPendiente 	= "",
			me.modeSave 					= false

		},
		pendientesLista:function(page){
			block();
            let me = this
            me.busqueda.curp = "";
            var url = "solicitud/listaPendientes?page="+page;  
            axios.get(url).then(response => {
                unblock();
                me.listaPendientes 	= response.data.lista.data  
                me.pagination		= response.data.pagination              
                }).catch(error => {
                    unblock();
                     swal({
                          type: 'error',
                          title: 'Oops...',
                          text: 'Ocurrio un error!',
                          footer: 'Contacte al Administrador',
                          showConfirmButton: false,
                          timer: 1700
                        })
                })    
        },
        verImagen:function(pendiente){
        	let me = this
        	me.resetDatos()
        	me.pendienteDatos.rutaImagen = "../"+pendiente.ruta
        	$('#modalSolicitud').modal('show');

        },
        opciones:function(pendiente){
        	// block();
            let me = this
            me.resetDatos()
            me.modeSave = true
            me.pendienteDatos.status = pendiente.status_solicitud_id
            me.pendienteDatos.idPendiente = pendiente.id
            $('#modalSolicitud').modal('show');
        	
        },
        saveStatus:function(){
        	block();
            let me = this
            var url = "solicitud/saveStatus"    
            axios.post(url, me.pendienteDatos).then(response => {
                unblock();
                if (response.data.success) {
                	me.resetDatos()
                	me.pendientesLista()
                	 swal({
                          type: 'success',
                          title: 'Bien',
                          text: 'Se actualizo correctamente!',
                          showConfirmButton: false,
                          timer: 1700
                        })
                	 $('#modalSolicitud').modal('toggle');
                }else{
                	 swal({
                          type: 'error',
                          title: 'Oops...',
                          text: 'Ocurrio un error!',
                          footer: 'No se guardaron los cambio',
                          showConfirmButton: false,
                          timer: 1700
                        })
                }
            }).catch(error => {
                unblock();
	             swal({
	                  type: 'error',
	                  title: 'Oops...',
	                  text: 'Ocurrio un error!',
	                  footer: 'Contacte al Administrador',
	                  showConfirmButton: false,
	                  timer: 1700
	                })
            })  
        },
        busquedaDatos(){
        	let me = this;
        	if (me.busqueda.nombre != '' || me.busqueda.curp != '') {
        		var url = "solicitud/busqueda";
        		axios.post(url, me.busqueda).then(response => {
        			
        			if (response.data.lista.data.length > 0) {
        				me.listaPendientes 	= response.data.lista.data  
                		me.pagination		= response.data.pagination 
        			}else{
        				swal({
		                  type: 'info',
		                  title: 'Oops...',
		                  text: 'No se encontraron resultados!',
		                  // footer: 'Contacte al Administrador',
		                  showConfirmButton: false,
		                  timer: 1700
		                })
        			}
        			
        		}).catch(error => {
        			console.log("Error "+error)
        		})
        	}
        }
	}
})