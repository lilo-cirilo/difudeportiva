function inicializa() {
    var zoomI = 5;
    var latI = 23.84;
    var lngI = -102.18;
    var myOptions = {
        center: new google.maps.LatLng(latI, lngI), zoom: zoomI
    };
    for (var i = 0; i < geeServerDefs.layers.length; i++) {
        geeServerDefs.layers[i].initialState = false;
    }
    gmap = new GFusionMap("map_canvas", geeServerDefs, myOptions);
}

function enciendeApagaCapa(layerId) {
	layerId= '0-'+layerId;
    if (gmap.isFusionLayerVisible(layerId)) {
        gmap.hideFusionLayer(layerId);
    }
    else {
        gmap.showFusionLayer(layerId);
    }
}