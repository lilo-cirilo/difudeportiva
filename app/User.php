<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /* protected $fillable = [
        'name', 'email', 'password',
    ]; */

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /* protected $hidden = [
        'password', 'remember_token',
    ]; */

    protected $hidden = ["email","vida_token","remember_token","autorizado","password",'created_at', 'updated_at', 'deleted_at',"usuario_id"]; 

    public function generateToken() {
        $this->token = str_random(60);
        $this->save();
        return $this->token;
    }
}