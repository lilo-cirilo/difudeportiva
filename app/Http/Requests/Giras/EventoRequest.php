<?php

namespace App\Http\Requests\Giras;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class EventoRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'localidad' => [
                'max:255',
                'nullable'
            ],
            'localidad_id' => [
                'integer',
                'nullable'
            ],
            'municipio_id' => [
                'required',
                'integer',
                Rule::unique('atnc_eventos')->where(function($query) {
                    return $query->where('fecha', (\DateTime::createFromFormat('d/m/Y', $this->fecha))->format('Y-m-d'))
                    ->where('municipio_id', $this->municipio_id)
                    ->where('localidad_id', $this->localidad_id)
                    ->where('gira_id', $this->gira_id)
                    ->where('id', '!=', $this->evento_id);
                })
            ],
            'fecha' => [
                'required',
                'date_format:"d/m/Y"'                
            ]
        ];
    }

    public function messages() {
        return [
            'municipio_id.unique' => 'Ya se encuentra un evento registrado con la misma fecha y ubicación.'
        ];
    }

    protected function failedValidation(Validator $validator) { 
        throw new HttpResponseException(response()->json( [
            'success' => false,
            'errors' => $validator->errors()->all()
        ], 422)); 
    }
}
