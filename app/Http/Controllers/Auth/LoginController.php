<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/unidaddeportiva';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // $this->middleware('saveaction');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function username()
    {
        return 'usuario';
    }

    public function loginApi(Request $request) {
         $this->validateLogin($request);

        if ($this->attemptLogin($request)) {
            $user = $this->guard()->user();
            $user->generateToken();
            $persona = Persona::where('id', $user->persona_id)->first();
            $user->nombre = $persona->nombre . ' ' . $persona->primer_apellido . ' ' . $persona->segundo_apellido;
            try {
                $storage = Storage::disk('local')->get( str_replace('storage','public',$persona->fotografia) );
                $base64 = base64_encode($storage);
                $user->foto = $base64;
            } catch(\Exception $e){}
            return response()->json( $user );
        }
        return $this->sendFailedLoginResponse($request);
    }
}
