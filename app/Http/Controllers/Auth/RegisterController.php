<?php

namespace App\Http\Controllers\Auth;

use App\Models\Usuario;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Models\Empleado;
use App\Models\Etnia;
use App\Models\Nivel;
use App\Models\Tiposempleado;
use App\Models\Persona;
use App\Models\Modulo;
use App\Models\UsuariosRol;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/unidaddeportiva';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('saveaction');
    }

    public function showRegistrationForm()
    {
        $etnias = Etnia::all();
        $niveles = Nivel::orderBy('id', 'DESC')->get();
        $tipos = Tiposempleado::all();
        $modulos = Modulo::where('activo', 1)->where('id', '!=', 1)->with('roles')->get();
        return view('auth.register', ['etnias' => $etnias, 'niveles' => $niveles, 'tipos' => $tipos, 'modulos' => $modulos]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => 'required|string',
            'primer_apellido' => 'required|string',
            'etnia_id' => 'required|numeric',
            'rfc' => 'required|min:13|max:13|unique:empleados,rfc',
            'nivel_id' => 'required|numeric',
            'tiposempleado_id' => 'required|numeric',
            'area_id' => 'required|numeric',
            'usuario' => 'required|string|max:255|unique:usuarios,usuario',
            'password' => 'required|string|min:6|confirmed',
            'email' => 'required|email|unique:usuarios,email'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Usuario
     */
    protected function create(array $data)
    {
        try{
            DB::beginTransaction();
            $persona = Persona::create([
                'nombre' => strtoupper($data['nombre']),
                'primer_apellido' => strtoupper($data['primer_apellido']),
                'segundo_apellido' => strtoupper($data['segundo_apellido']),
                'etnia_id' => $data['etnia_id']
            ]);
            $empleado = Empleado::create([
                'persona_id' => $persona->id,
                'rfc' => strtoupper($data['rfc']),
                'nivel_id' => $data['nivel_id'],
                'tiposempleado_id' => $data['tiposempleado_id'],
                'area_id' => $data['area_id'],
                'usuario_id' => 1,
                'validado' => 1
            ]);
            $usuario = Usuario::create([
                'usuario' => $data['usuario'],
                'persona_id' => $persona->id,
                'password' => bcrypt($data['password']),
                'email' => strtolower($data['email']),
                'vida_token' => \Carbon\Carbon::now()->toDateTimeString(),
                'autorizado' => 1,
                'ficha' => 0
            ]);
            UsuariosRol::create([
                'usuario_id' => $usuario->id,
                'modulo_id' => 1,
                'rol_id' => 3,
                'responsable' => $usuario->id,
                'autorizado' => 0 
            ]);
            DB::commit();
            return $usuario;
        }catch(\Exception $e){
            DB::rollBack();
            return abort(422, $e->getError());
        }
    }

    public function tarjetas(Request $request)
    {
        $empleados = Empleado::select('persona_id', 'rfc')->where('num_empleado', 'LIKE', '%' . $request->input('search') . '%')->take(10)->get();
        $array = [];
        foreach ($empleados as $empleado) {
            array_push($array, ['persona_id' => $empleado->persona_id, 'rfc' => $empleado->rfc, 'nombre' => $empleado->persona->nombreCompleto()]);
        }
        return response()->json($array);
    }
}
