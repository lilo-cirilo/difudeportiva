<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\AreasPrograma;
use App\Models\Programa;
use View;

class AreaProgramaController extends Controller {
  public function index(Request $request, $programa_id){
    return view("programas.modals.$request->view.areaPrograma",['areaPrograma'=>AreasPrograma::where('programa_id',$programa_id)->first()]);
  }
  public function create(Request $request, $programa_id){
    return view("programas.modals.$request->view.areaProgramaCreate",['programa'=>Programa::findOrFail($programa_id)]);
  }
  public function store(Request $request, $programa_id){
    try {
      $old=AreasPrograma::where('programa_id',$programa_id)->first();
      DB::beginTransaction();
      if($old)
        $old->delete();
      $new=AreasPrograma::Create([
        'programa_id'=>$programa_id,
        'area_id'=>$request->input('area_id'),
        'usuario_id'=>Auth::user()->id
      ]);
      DB::commit();
      return new JsonResponse(['success'=>true,'new'=>$new,'old'=>$old],200);
    }catch(\Exeption $e) {
      DB::rollBack();
      abort(500,'Algo no salio bien');
    }
  }
}