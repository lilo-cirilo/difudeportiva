<?php

namespace App\Http\Controllers;

use App\Models\Programa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\Beneficiosprograma;
use App\Models\AreasPrograma;

class BeneficiosController extends Controller {
    public function beneficiosSelect(Request $request) {
        $beneficios = Beneficiosprograma::with('anioprograma.ejercicio')->wherehas('anioprograma.ejercicio',function($query){
            $query->where('anio',date('Y'));
        })
        ->where([['nombre', 'LIKE', "%$request->search%"],['predeterminado',1]])->paginate(10);//->take(20)->get()->toArray();
        return response()->json($beneficios);
    }

    public function programasPadresSelect(Request $request) {
        $beneficios = Programa::select(['programas.nombre as nombre', 'programas.id as id'])
        ->join('programas_responsables', 'programas_responsables.programa_id', 'programas.id')
        ->join('areas_responsables','areas_responsables.id','programas_responsables.areas_responsable_id')        
        ->whereNull('padre_id')
        ->where('areas_responsables.area_id', $request->input('area_id'))
        ->where('nombre', 'LIKE', '%' . $request->input('search') . '%')
        ->get()
        ->toArray();
        return response()->json($beneficios);
    }

    public function obtenerDatosBeneficio($beneficio_id) {
        //obtenemos el padre del programa asociado al beneficio
        $padre = Beneficiosprograma::find($beneficio_id)->anioprograma->programa->padre;
        //comprobamos que este en un area, en caso contrario no dirigimos al padre de este
        while ($padre->areasprograma == null){
            $padre=$padre->padre;
        }
        $area=$padre->areasprograma->area;
        return response()->json(['area'=>$area->nombre,'responsable'=>$area->areasresponsables->empleado->persona->obtenerNombreCompleto()]);
    }

}