<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ocupacion;

class OcupacionController extends Controller {

    public function select(Request $request) {
        if($request->ajax()) {
            $ocupaciones = Ocupacion::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                //->take(5)
                ->get()
                ->toArray();
            return response()->json($ocupaciones);
        }
    }
}