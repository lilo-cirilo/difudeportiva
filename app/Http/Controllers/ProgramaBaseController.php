<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Unidadmedida;
use App\Models\Programa;
use App\DataTables\BeneficiosDataTable;

class ProgramaBaseController extends Controller{
  private $programa_padre;
  private $modulo;
  private $view;
	
	function __construct($view="adminlte",$programa_padre="", $modulo="") {
		$this->programa_padre = $programa_padre;
		$this->modulo         = $modulo;      
		$this->middleware(['auth', 'authorized']);      
		/* if($modulo != "") {
			$mod = Modulo::where('prefix', $this->modulo)->orWhere('nombre',$programa_padre)->first();
			if($mod){
				//$this->middleware("rolModule:$mod->id,ADMINISTRADOR DE BENEFICIOS,ADMINISTRADOR", ['except' => 'directores']);
			} else {
				//abort(403, 'Este módulo no se encuentra registrado.');
			}
    } */
    $this->view = ".$view";
  }
  public function index(BeneficiosDataTable $dataTable,Request $request, $padre_id = 0) {
    return $dataTable
        ->with('padre_id',$padre_id)->render(
      "programas$this->view.index", 
      [
        'modulo' => $this->modulo,
        'programa_padre' => $this->programa_padre
      ]
    );
    }
    
  public function beneficiosExternos(BeneficiosDataTable $dataTable) {
    return $dataTable
        ->with('padre_id',759)->render(
      "programas$this->view.index", 
      [
        'modulo' => $this->modulo,
        'programa_padre' => $this->programa_padre
      ]
    );
  }

  public function getProgramas(Request $request) {
    return Programa::where('padre_id',$request->padre_id)
                    ->where('nombre','like',"%{$request->input('search')}%")
                    ->get()
                    ->toArray();
  }
}
