<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Telegram\Bot\Laravel\Facades\Telegram;

class ArduinoController extends Controller
{
    public function index(Request $request)
    {
        \Log::debug($request->all());
        // Notification::send(new SensorProblem());
        // Notification::send(391174766, new SensorProblem());
        if(count($request->all()) > 0)
            Telegram::sendMessage([
                // 'chat_id' => 391174766,
                'chat_id' => -1001226809060,
                'parse_mode' => 'HTML',
                'text' => "<b>Datos del servidor</b>\nVoltaje: <b>$request->voltaje</b>\nHumedad: <b>$request->humedad</b>\nTemperatura: <b>$request->temperatura</b>\nTiempo: <b>$request->tiempo</b>"
            ]);
        return new JsonResponse(['menssage' => 'Recibido', 'data' => $request->all()]);
    }

    public function updatedActivity()
    {
        $activity = Telegram::getUpdates();
        dd($activity);
    }

    public function organigrama()
    {
        return \App\Models\Area::with('listadoareas.persona:id,nombre,primer_apellido,segundo_apellido')->get();
    }
}
