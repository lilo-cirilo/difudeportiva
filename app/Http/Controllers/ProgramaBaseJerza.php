<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\DataTables\ProgramasDataTable;
use App\Models\Programa;
use App\Models\Ejercicio;
use App\Models\AniosPrograma;
use App\Models\DocumentosPrograma;
use App\Models\Documento;
use App\Models\Unidadmedida;
use App\Models\AreasResponsable;
use App\Models\AreasProducto;
use App\Models\Modulo;
use App\Models\ProgramasResponsable;
use App\Http\Requests\StorePrograma;
use View;

class ProgramaBaseJerza extends Controller {
	
	private $programa_padre;
	private $modulo;
	
	public function __construct($programa_padre="", $modulo="") {
		$this->programa_padre = $programa_padre;
		$this->modulo         = $modulo;      
		$this->middleware(['auth', 'authorized']);      
		if($modulo != "") {
			$mod = Modulo::where('prefix', $this->modulo)->orWhere('nombre',$programa_padre)->first();
			if($mod){
				$this->middleware("rolModule:$mod->id,ADMINISTRADOR DE BENEFICIOS,ADMINISTRADOR", ['except' => 'directores']);
			} else {
				//abort(403, 'Este módulo no se encuentra registrado.');
			}
		}
	}
	
	public function index(ProgramasDataTable $dataTable) {
		return $dataTable->with(['programa_padre' => $this->programa_padre, 'area_id' => auth()->user()->persona->empleado->area_id])
		->render('programas.index', 
		['unidades' => Unidadmedida::get(), 'modulo' => $this->modulo, 'programa_padre' => $this->programa_padre]);
	}
	
	public function directores(ProgramasDataTable $dataTable) {
		return $dataTable->with('area_id', auth()->user()->persona->empleado->area_id)->render('programas.directores.index',
		['unidades' => Unidadmedida::get()]);
	}
	
	public function create() {
		return response()->json([
			'html' =>  view::make('programas.iframe.create_edit')            
			->with(['documentos' => [],
			'programa_padre'=> $this->programa_padre == "" ? "" : Programa::firstOrCreate(['nombre' => $this->programa_padre], ['activo' => true, 'puede_agregar_benef' => false, 'tipo' => 'PROGRAMA', 'oficial' => true]),
			'modulo' => $this->modulo
			])
			->render()
		], 200);
	}
	
	public function store(StorePrograma $request) {
		if($request->ajax()) {
			try {
				DB::beginTransaction();
				
				$request['usuario_id'] = $request->user()->id;
				$programa_padre = $request->has('programa_padre') ? '' : Programa::where('nombre','like',$this->programa_padre)->first();
        //SE CREA EL PROGRAMA PADRE EN CASO DE NO EXISTIR
				if($request->has('programa_padre')){
					$request['programa_padre'] = strtoupper($request['programa_padre']);
					$programa_padre = Programa::join('programas_responsables','programas_responsables.programa_id','programas.id')
					->join('areas_responsables', 'areas_responsables.id', 'programas_responsables.areas_responsable_id')
					->where('areas_responsables.area_id', $request['area_id'])
					->where('programas.nombre', $request['programa_padre'])
					->whereNull('programas.padre_id')
          ->first(['programas.id']);

          //Creación del PADRE
          if(!$programa_padre){
            $programa_padre = Programa::create([
              'nombre' => $request['programa_padre'],
              'activo' => true,
              'tipo' => 'PROGRAMA',
              'oficial' => true,
              'puede_agregar_benef' => false
            ]);
            
            //Agregando a la bitácora
            auth()->user()->bitacora(request(), [
              'tabla' => 'programas',
              'registro' => $programa_padre->id . '',
              'campos' => json_encode($programa_padre) . '',
              'metodo' => 'POST'
            ]);
            
            $area_responsable = AreasResponsable::where('area_id', $request['area_id'])->first();
            if($area_responsable){
              $request['areas_responsable_id'] = $area_responsable->id;
              $programita_responsable = ProgramasResponsable::create([
                'programa_id' => $programa_padre->id,
                'areas_responsable_id' => $area_responsable->id,
                'usuario_id' => $request['usuario_id']
              ]);
              
              //Agregando a la bitácora
              auth()->user()->bitacora(request(), [
                'tabla' => 'programas_responsables',
                'registro' => $programita_responsable->id . '',
                'campos' => json_encode($programita_responsable) . '',
                'metodo' => 'POST'
              ]);
            }
          }
        }
        
        $request['padre_id'] = $programa_padre->id;
        $request['activo'] = false;
        $request['oficial'] = ($request->has('oficial')) ? true : false;        
        $request['fecha_inicio'] = ($request['fecha_inicio']) ? (\DateTime::createFromFormat('d-m-Y', $request['fecha_inicio']))->format('Y-m-d') : null;
        $request['fecha_fin'] = ($request['fecha_fin']) ? (\DateTime::createFromFormat('d-m-Y', $request['fecha_fin']))->format('Y-m-d') : null;
        $request['puede_agregar_benef'] = 0;
        $request['programa_id'] = Programa::create($request->all())->id;
        
        //Agregando a la bitácora
        auth()->user()->bitacora(request(), [
          'tabla' => 'programas',
          'registro' => $request['programa_id'] . '',
          'campos' => json_encode(Programa::find($request['programa_id'])) . '',
          'metodo' => 'POST'
        ]);
        
        //Se le asigna el área del programa padre
        $programa_responsable_padre = ProgramasResponsable::where('programa_id', $programa_padre->id)->first();
        if($programa_responsable_padre){
          $request['areas_responsable_id'] = $programa_responsable_padre->areas_responsable_id;
          $programa_responsable = ProgramasResponsable::create($request->all());
          //Agregando a la bitácora
          auth()->user()->bitacora(request(), [
            'tabla' => 'programas_responsables',
            'registro' => $programa_responsable->id . '',
            'campos' => json_encode($programa_responsable) . '',
            'metodo' => 'POST'
          ]);
        }
        
        //FIXME: como rayos se actualiza al año siguien de manera automatica
        $request['ejercicio_id'] = Ejercicio::firstOrCreate(['anio' => date('Y')])->id;
        $request['anios_programa_id'] = AniosPrograma::firstOrcreate(['ejercicio_id' => $request['ejercicio_id'], 'programa_id' => $request['programa_id']])->id;
        //Para el campo de activo en DocumentosProrama :/
        $request['activo'] = true;
        if($request->has('documentos')){
          foreach($request->documentos as $documento){
            $request['documento_id'] = $documento;
            $documentito_programa = DocumentosPrograma::create($request->all());
            //Agregando a la bitácora
            auth()->user()->bitacora(request(), [
              'tabla' => 'documentos_programas',
              'registro' => $documentito_programa->id . '',
              'campos' => json_encode($documentito_programa) . '',
              'metodo' => 'POST'
            ]);
          }
        }
        
        DB::commit();
        return response()->json(['success'=> true],200);
      } catch(\Exception $e) {
        DB::rollBack();
        return abort(500,'Error al registrar en la BD: ' . $e->getMessage());
      }
    }
  }
  
  public function edit($id, Request $request) {
    if( $request->has('activar') ) {
      try {
        DB::beginTransaction();
        $request['ejercicio_id'] = Ejercicio::firstOrCreate(['anio' => date('Y')])->id;
        AniosPrograma::firstOrcreate(['ejercicio_id' => $request['ejercicio_id'], 'programa_id' => $id]);
        Programa::find($id)->update(['activo' => true]);
        auth()->user()->bitacora(request(), [
          'tabla' => 'programas',
          'registro' => $id . '',
          'campos' => json_encode(Programa::find($id)) . '',
          'metodo' => 'PUT'
        ]);
        DB::commit();
      } catch(\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
      }
    }
    return response()->json([
      'html' =>  view::make('programas.iframe.create_edit')
      ->with(['programa' => Programa::find($id),
      'documentos'=> Documento::All(),
      'programa_padre' => $this->programa_padre,
      'modulo' => $this->modulo])
      ->render()
    ], 200);
  }
  
  public function update(StorePrograma $request, $id) {
    if($request->ajax()) {
      try {
        DB::beginTransaction();
        $request['usuario_id'] = $request->user()->id;
        $programa_padre = $request->has('programa_padre') ? '' : Programa::where('nombre','like',$this->programa_padre)->first();
        
        if($request->has('programa_padre')){
          $request['programa_padre'] = strtoupper($request['programa_padre']);
          $programa_padre = Programa::join('programas_responsables','programas_responsables.programa_id','programas.id')
          ->join('areas_responsables', 'areas_responsables.id', 'programas_responsables.areas_responsable_id')
          ->where('areas_responsables.area_id', $request['area_id'])
          ->where('programas.nombre', $request['programa_padre'])
          ->whereNull('programas.padre_id')
          ->first(['programas.id']);
          
          //Creación del PADRE
          if(!$programa_padre){
            $programa_padre = Programa::create([
              'nombre' => $request['programa_padre'],
              'activo' => true,
              'tipo' => 'PROGRAMA',
              'oficial' => true,
              'puede_agregar_benef' => false
            ]);
            
            //Agregando a la bitácora
            auth()->user()->bitacora(request(), [
              'tabla' => 'programas',
              'registro' => $programa_padre->id . '',
              'campos' => json_encode($programa_padre) . '',
              'metodo' => 'POST'
            ]);
            
            $area_responsable = AreasResponsable::where('area_id', $request['area_id'])->first();
            if($area_responsable){
                $request['areas_responsable_id'] = $area_responsable->id;
                $programita_responsable = ProgramasResponsable::create([
                  'programa_id' => $programa_padre->id,
                  'areas_responsable_id' => $area_responsable->id,
                  'usuario_id' => $request['usuario_id']
              ]);
              
              //Agregando a la bitácora
              auth()->user()->bitacora(request(), [
                'tabla' => 'programas_responsables',
                'registro' => $programita_responsable->id . '',
                'campos' => json_encode($programita_responsable) . '',
                'metodo' => 'POST'
              ]);
            }
          }
        }
        
        $request['padre_id'] = $programa_padre->id;
        $request['oficial'] = ($request->has('oficial')) ? true : false;
        $request['fecha_inicio'] = ($request['fecha_inicio']) ? (\DateTime::createFromFormat('d-m-Y', $request['fecha_inicio']))->format('Y-m-d') : null;
        $request['fecha_fin'] = ($request['fecha_fin']) ? (\DateTime::createFromFormat('d-m-Y', $request['fecha_fin']))->format('Y-m-d') : null;
        Programa::find($id)->update($request->all());
        
        auth()->user()->bitacora(request(), [
          'tabla' => 'programas',
          'registro' => $id . '',
          'campos' => json_encode(Programa::find($id)) . '',
          'metodo' => 'PUT'
        ]);
        
        $request['ejercicio_id'] = Ejercicio::firstOrCreate(['anio' => date('Y')])->id;
        $request['anios_programa_id'] = AniosPrograma::firstOrcreate(['ejercicio_id' => $request['ejercicio_id'], 'programa_id' => $id])->id;
        
        //Para el campo de activo en DocumentosProrama :/
        $request['activo'] = true;
        if($request->has('documentos')){
          foreach($request->documentos as $documento) {
            $request['documento_id'] = $documento;
            $documento_programa = DocumentosPrograma::withTrashed()->where('documento_id', $documento)->where('anios_programa_id', $request['anios_programa_id'])->first() ?: DocumentosPrograma::create($request->all());
            if($documento_programa->trashed()){
              $documento_programa->restore();
            }
          }
          
          //Eliminando aquellos documentos que no se encuentran en el request
          foreach(AniosPrograma::find($request['anios_programa_id'])->documentosprogramas as $documento_programa){            
            $existe = false;
            foreach($request->documentos as $documento){
              if($documento_programa->documento_id == $documento){
                $existe = true;
              }
            }                
            if(!$existe){
              DocumentosPrograma::destroy($documento_programa->id);
            }
          }
        }
        //En caso de que no haya checkeado quiere decir que se van a eliminar los documentos (si es que existen)
        else {            
          foreach(AniosPrograma::find($request['anios_programa_id'])->documentosprogramas as $documento_programa){
            DocumentosPrograma::destroy($documento_programa->id);
          }
        }
        
        DB::commit();
        return response()->json(['success'=>true],200);
      } catch(\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
      }
    }
  }

  public function destroy($id, Request $request) {
    if($request->ajax()) {
      try {
        DB::beginTransaction();
        $programa = Programa::findOrFail($id);
        $programa->usuario_id = $request->user()->id;
        
        if( $request->has('inactivar') ) {
          $programa->activo = false;
          auth()->user()->bitacora(request(), [
            'tabla' => 'programas',
            'registro' => $id . '',
            'campos' => json_encode($programa) . '',
            'metodo' => 'PUT'
          ]);
          $programa->save();
        } else {
          auth()->user()->bitacora(request(), [
            'tabla' => 'programas',
            'registro' => $id . '',
            'campos' => json_encode(Programa::find($id)) . '',
            'metodo' => 'DELETE'
          ]);
          
          $programa->delete();    
        }        
        DB::commit();
        return response()->json(['status'=>'ok'],200);
      } catch(\Exception $e) {
        DB::rollBack();
        return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
      }
    }
  }
  
  public function show($id) {
    return response()->json([
      'html' =>  view::make('programas.iframe.show')
      ->with(['programa' => Programa::find($id)])
      ->render()
    ], 200);
  }
  
}