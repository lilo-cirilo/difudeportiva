<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Pusher\Pusher;
use App\Notifications\NewRequestDisability;
use App\Models\Usuario;
use App\Models\UsuariosRol;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'welcome']);
        $this->middleware('authorized', ['except' => ['completar', 'welcome', 'index', 'ficha']]);
        // $this->middleware('saveaction');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome() {
        // return redirect('/home');
        return redirect('/login');
    }

    public function index()
    {
        //dd(auth()->user()->usuarioroles->first->id->rol->nombre);
        $action = (object) array('id' => 1, 'content' => auth()->user()->usuario . ' entre al Home', 'title' => 'Nueva Notificacion', 'description' => 'Hola mundo desde Laravel', 'route' => url('home'));
        //auth()->user()->notify(new NewRequestDisability($action));
        $request = request();
        $request->tabla = 'ninguna';
        $request->registro = 0;
        $mymodules = auth()->user()->usuarioroles;
        // return view('home', ['mymodules' => $mymodules]);
        return redirect('/unidaddeportiva');
        // return view('home5', ['mymodules' => $mymodules]);
    }

    public function index2()
    {
        $action = (object) array('id' => 1, 'content' => auth()->user()->usuario . ' entre al Home', 'title' => 'Nueva Notificacion', 'description' => 'Hola mundo desde Laravel', 'route' => url('home'));
        //auth()->user()->notify(new NewRequestDisability($action));
        $request = request();
        $request->tabla = 'ninguna';
        $request->registro = 0;
        $mymodules = auth()->user()->usuarioroles;
        // return view('home', ['mymodules' => $mymodules]);
        return view('home2', ['mymodules' => $mymodules]);
    }

    public function index3()
    {
        $action = (object) array('id' => 1, 'content' => auth()->user()->usuario . ' entre al Home', 'title' => 'Nueva Notificacion', 'description' => 'Hola mundo desde Laravel', 'route' => url('home'));
        //auth()->user()->notify(new NewRequestDisability($action));
        $request = request();
        $request->tabla = 'ninguna';
        $request->registro = 0;
        $mymodules = auth()->user()->usuarioroles;
        // return view('home', ['mymodules' => $mymodules]);
        return view('home3', ['mymodules' => $mymodules]);
    }
    public function index4()
    {
        $action = (object) array('id' => 1, 'content' => auth()->user()->usuario . ' entre al Home', 'title' => 'Nueva Notificacion', 'description' => 'Hola mundo desde Laravel', 'route' => url('home'));
        //auth()->user()->notify(new NewRequestDisability($action));
        $request = request();
        $request->tabla = 'ninguna';
        $request->registro = 0;
        $mymodules = auth()->user()->usuarioroles;
        // return view('home', ['mymodules' => $mymodules]);
        return view('home4', ['mymodules' => $mymodules]);
    }
    public function index5()
    {
        $action = (object) array('id' => 1, 'content' => auth()->user()->usuario . ' entre al Home', 'title' => 'Nueva Notificacion', 'description' => 'Hola mundo desde Laravel', 'route' => url('home'));
        //auth()->user()->notify(new NewRequestDisability($action));
        $request = request();
        $request->tabla = 'ninguna';
        $request->registro = 0;
        $mymodules = auth()->user()->usuarioroles;
        // return view('home', ['mymodules' => $mymodules]);
        return redirect('/unidaddeportiva');
        // return view('home5', ['mymodules' => $mymodules]);
    }

    public function completar(){
        dd(explode(',', env('DB_CONNECTIONS')));
    }

    public function ficha(Request $request, $idusuario)
    {
        try{
            DB::beginTransaction();
            $usuario = Usuario::findOrFail($idusuario);
            $usuario->ficha = 1;
            $usuario->save();
            $datos = [
                'usuario' => $usuario->usuario,
                'email' => $usuario->email,
                'nombre_completo' => $usuario->persona->nombre . ' ' . $usuario->persona->primer_apellido . ' ' . $usuario->persona->segundo_apellido,
                'rfc' => $usuario->persona->empleado->rfc,
                'cargo' => $usuario->persona->empleado->nivel->nivel,
                'ads' => $usuario->persona->empleado->tiposempleado->tipo,
                'jefe' => $usuario->persona->empleado->area->areasresponsables->empleado->persona->nombre,
                'area' => $usuario->persona->empleado->area->nombre,
                'roles' => UsuariosRol::where('usuario_id', $usuario->id)->with('rol', 'modulo')->get()
            ];
            DB::commit();
            return new JsonResponse($datos);
        }catch(Exception $e){
            DB::rollBack();
            return new JsonResponse(['error' => $e->getMessage()], 422);
        }
    }

    public function datosFicha($idusuario)
    {
        $usuario = Usuario::findOrFail($idusuario);
        $usuario->ficha = 1;
        $usuario->save();
        $datos = [
            'usuario' => $usuario->usuario,
            'email' => $usuario->email,
            'nombre_completo' => $usuario->persona->nombre . ' ' . $usuario->persona->primer_apellido . ' ' . $usuario->persona->segundo_apellido,
            'cargo' => $usuario->persona->empleado->nivel->nivel,
            'ads' => $usuario->persona->empleado->tiposempleado->tipo,
            'jefe' => $usuario->persona->empleado->area->areasresponsables->empleado->persona->nombre,
            'area' => $usuario->persona->empleado->area->nombre
        ];
        return new JsonResponse($datos);
    }

    public function colores(){
        return view('coloresDIF');
    }
    public function info(){
        return view('info');
    }
}
