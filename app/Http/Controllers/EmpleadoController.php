<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Empleado;
use App\Models\Nivel;
use App\Models\Area;
use App\Models\Tiposempleado;
use App\Models\Persona;

class EmpleadoController extends Controller {
    public function empleadosSelect(Request $request) {
        if($request->ajax()) {
            $empleados = DB::table('empleados')
            ->select([
                'empleados.id as id',
                DB::raw("CONCAT( personas.nombre, ' ', personas.primer_apellido, ' ', personas.segundo_apellido) as nombre")
            ])
            ->where('personas.nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->orWhere('personas.primer_apellido', 'LIKE', '%' . $request->input('search') . '%')
            ->orWhere('personas.segundo_apellido', 'LIKE', '%' . $request->input('search') . '%')
            ->join('personas', 'personas.id', '=', 'empleados.persona_id')
            ->take(10)
            ->get()
            ->toArray();
            return response()->json($empleados);
        }
    }

    public function index(Persona $persona){
        $empleado = Empleado::where('persona_id', $persona->id)->get();
        if($empleado->count() > 0){
            abort(403, 'Ya estas registrado :/');
        }
        $niveles = Nivel::all();
        $tipos = Tiposempleado::all();
        $areas = Area::all();
        return view('admin.empleados.index', ['persona' => $persona, 'niveles' => $niveles, 'tipos' => $tipos, 'areas' => $areas]);
    }

    public function store(Request $request){
        $empleado = Empleado::create($request->all());
        return redirect('/register');
    }
    
}
