<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Localidad;
use View;

class LocalidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('localidades.index');
    }

    public function list(Request $request){
        $columns = array(
            0 => 'cat_localidades.nombre',
            1 => 'cat_municipios.nombre',
            2 => 'latitud',
            3 => 'longitud'
        );

        $totalData = Localidad::count();
        $start = $request->input('start');
        $limit = $request->input('length');
        $limit = ($limit==-1)?$totalData:$limit;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar=$request->input('columns.0.search.value','');

        $localidades = Localidad::join('cat_municipios', 'cat_municipios.id', '=', 'cat_localidades.municipio_id')
        ->where('cat_municipios.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_localidades.nombre', 'like', '%'.$buscar.'%')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get([
          'cat_localidades.id',
          'cat_municipios.nombre as municipio',
          'cat_localidades.nombre as localidad',
          'cat_municipios.id as municipio_id',
          'cat_localidades.latitud',
          'cat_localidades.longitud'
        ]);

        $totalFiltered=Localidad::join('cat_municipios', 'cat_municipios.id', '=', 'cat_localidades.municipio_id')
        ->where('cat_municipios.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_localidades.nombre', 'like', '%'.$buscar.'%')
        ->count();

        $data = array();

        if($localidades)
        {
            foreach($localidades as $localidad)
            {
                $nestedData['id'] = $localidad->id;
                $nestedData['Nombre'] = $localidad->localidad;
                $nestedData['Municipio'] = $localidad->municipio;
                $nestedData['municipio_id'] = $localidad->municipio_id;
                $nestedData['Latitud'] = $localidad->latitud;
                $nestedData['Longitud'] = $localidad->longitud;
                /* $nestedData['Acciones'] ='
                <a href="' . route('localidades.show', $localidad->id) . '" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Ver</a>
                <a href="' . route('localidades.edit', $localidad->id) . '" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Editar</a>
                <a class="btn btn-danger btn-sm" onclick="localidad_delete(' . $localidad->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                '; */

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('localidades.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if($request->ajax()){
            try {
                DB::beginTransaction();
                $new = Localidad::create($request->all());
                DB::commit();
                return response()->json(array('success' => true, 'id' => $new->id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $localidad = Localidad::findOrFail($id);
        return view('localidades.show')->with('localidad',$localidad);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $localidad = Localidad::findOrFail($id);
        return view('localidades.create_edit')->with('localidad',$localidad);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();

                Localidad::find($id)->update($request->all());

                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        if($request->ajax()) {
            try {
                DB::beginTransaction();
                Localidad::destroy($id);
                DB::commit();

                return response()->json(array('success' => true, 'id' => $id));
            }
            catch(Exeption $e) {
                DB::rollBack();
                return response()->json(array('success' => false, 'message' => 'Error al eliminar en la BD: ' . $e->getMessage()));
            }
        }
    }

    public function localidadesSelect(Request $request)
    {
        //
        $localidades = Localidad::where('municipio_id', $request->input('municipio_id'))
        ->where('nombre', 'LIKE', '%' . $request->input('search') . '%')
        ->take(10)
        ->get()
        ->toArray();
        return response()->json($localidades);
    }
}
