<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tiposremitente;

class TipoRemitenteController extends Controller
{
    public function tiporemitenteSelect(Request $request)
    {
        if ($request->ajax())
            {
                $tiporemi = Tiposremitente::where('tipo', 'LIKE', '%' . $request->input('search') . '%')
                ->take(5)
                ->get()
                ->toArray();
                return response()->json($tiporemi);
            }
    }
}
