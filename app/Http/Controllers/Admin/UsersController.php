<?php

namespace App\Http\Controllers\Admin;

use App\Models\Usuario;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Notifications\NewRequestDisability;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Models\Persona;
use App\Models\Empleado;
use App\Models\AreasResponsable;
use App\Models\Area;
use App\Models\UsuariosRol;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR,CAPTURISTA', ['except' => ['edit', 'buscarUsuarios']]);
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR', ['only' => 'ficha']);
        $this->middleware('saveaction', ['except' => ['index', 'create', 'show', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = Usuario::all();
        return view('admin.users.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show(Usuario $user)
    {
        /*return [
            'usuario' => $user->usuario,
            'id' => $user->id,
            'email' => $user->email,
            'autorizado' => $user->autorizado,
            'nombre' => $user->persona->nombreCompleto(),
            'roles' => $user->usuarioroles];*/
        $data = $user;
        $roles = [];
        foreach($user->usuarioroles as $usuariorol){
            $rol = ['rol_id' => $usuariorol->rol->id, 'rol_nombre' => $usuariorol->rol->nombre, 'modulo_id' => $usuariorol->modulo->id, 'modulo_nombre' =>$usuariorol->modulo->nombre];
            array_push($roles, $rol);
        }
        $data['roles'] = $roles;
        return new JsonResponse($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuario $user)
    {
        $this->authorize('edit', $user);

        return view('admin.users.create', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuario $user)
    {
        $this->authorize('update', $user);

        $validator = Validator::make($request->all(), [
            'usuario' => 'required|min:3',
            'usuario_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('users.edit', $user)
                ->withErrors($validator)
                ->withInput();
        }

        $user->usuario = $request->usuario;

        $user->usuario_id = $request->usuario_id;

        if($request->password){
            $user->password = bcrypt($request->password);
        }
        try{
            DB::beginTransaction();
            $user->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('users.edit', $user)->with(['status' => 'error', 'msg' => 'Ocurrio un error !']);
        }

        return redirect()->route('users.edit', $user)->with(['status' => 'ok', 'msg' => 'Editado Correctamente !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuario $user)
    {
        $this->authorize('destroy', $user);
        try{
            DB::beginTransaction();
            $user->delete();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('users.index')->with(['status' => 'error', 'msg' => 'Ocurrio un error... intente de nuevo !']);
        }

        return redirect()->route('users.index')->with(['status' => 'ok', 'msg' => 'Eliminado Correctamente !']);
    }

    public function status(Usuario $user)
    {
        $user->autorizado = ! $user->autorizado;
        try{
            DB::beginTransaction();
            $user->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('users.index')->with(['status' => 'error', 'msg' => 'Ocurrio un error al cambiar el status !']);
        }

        return redirect()->route('users.index')->with(['status' => 'ok', 'msg' => 'Cambio de Estatus correcto !']);
    }

    public function buscarUsuarios(Request $request){
        $usuarios = Usuario::where('usuario', 'LIKE', '%' . $request->input('search') . '%')->take(10)->get();
        $request->tabla = 'usuarios';
        $request->registro = 0;
        $request->campos = 'ok';
        return new JsonResponse($usuarios);
    }

    public function buscarUsuariosV($token, $usuario)
    {
        return new JsonResponse(Usuario::where('usuario', 'like', "$usuario")->with(['persona' => function($query) {
            $query->select('id', 'nombre', 'primer_apellido', 'segundo_apellido');
        }])->take(10)->get());
    }

    public function ficha()
    {
        return view('admin.users.ficha');
    }

    public function buscarUsuarios2($usuario)
    {
        return new JsonResponse(Usuario::where('usuario', 'like', "$usuario")->with(['persona' => function($query) {
            $query->select('id', 'nombre', 'primer_apellido', 'segundo_apellido');
        }])->take(10)->get());
    }

    public function usuarioFicha($usuario)
    {
        return new JsonResponse(Usuario::where('usuario', 'like', "%$usuario%")->with(['persona' => function($query) {
            $query->select('id', 'nombre', 'primer_apellido', 'segundo_apellido');
        }])->take(10)->get());
    }

    public function obtenerDatos($idusuario)
    {
        try{
            $usuario = Usuario::with('persona')->findOrFail($idusuario);
            $empleado = Empleado::where('persona_id', $usuario->persona_id)->with('area', 'nivel', 'tiposempleado')->firstOrFail();
            $area = AreasResponsable::where('area_id', $empleado->area->id)->with('empleado','area')->firstOrFail();
            $jefe = $area;
            $roles = UsuariosRol::where('usuario_id', $usuario->id)->with('rol', 'modulo')->get();
            if($area->empleado->persona_id == $empleado->persona_id){
                $nuevaArea = Area::where('id', $area->area_id)->firstOrFail();
                $jefe = AreasResponsable::where('area_id', $nuevaArea->padre_id)->with('empleado','area')->firstOrFail();        
            }
            // dd([
            //     'usuario' => $usuario->toArray(),
            //     'empleado' => $empleado->toArray(),
            //     'area' => $area->toArray(),
            //     'jefe' => $jefe->empleado->persona->toArray(),
            //     ]);
            return new JsonResponse([
                'usuario' => $usuario,
                'empleado' => $empleado,
                'area' => $area,
                'jefe' => $jefe->empleado->persona,
                'roles' => $roles
            ]);
        }catch(\Exception $e){
            return new JsonResponse($e->getMessage());
        }
    }
}
