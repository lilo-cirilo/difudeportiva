<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Telegram\Bot\Laravel\Facades\Telegram;
use Illuminate\Support\Facades\Log;

class GitController extends Controller
{
    public function autopull(Request $request)
    {
      if ( $request->object_kind == 'push' ) {
        try{
          // $out = shell_exec( 'cd /var/www/intradif/ && git reset --hard HEAD && git pull --squash origin develop && php artisan view:clear && php artisan route:cache && php artisan cache:clear' );
          $result = '';
          system( 'cd /var/www/intradif/ && git reset --hard HEAD && git pull --squash origin develop && php artisan view:clear && php artisan route:cache && php artisan cache:clear', $result );
          Log::debug($result);
          Telegram::sendMessage([
              'chat_id' => -1001226809060,
              'parse_mode' => 'HTML',
              'text' => "<b>Auto pull hecho correctamente en el servidor!</b>\nSolicitado por: " . $request->user_name . "\nUltimo commit: " . count($request->commits) == 1 ? $request->commits[0]['message'] : $request->commits[count($request->commits) - 1]['message']
          ]);
          return new JsonResponse(['message' => 'Ok'], 200);
        }catch(\Exception $e) {
          Log::debug('--Autopull--');
          Log::debug($e);
          Telegram::sendMessage([
              'chat_id' => -1001226809060,
              'parse_mode' => 'HTML',
              'text' => "<b>Ocurrio un error!</b>\n" . $e->getMessage()
          ]);
        }
      } else {
        return new JsonResponse(['message' => 'Nothing to do'], 200);
      }
    }
}
