<?php

namespace App\Http\Controllers\Admin;

use App\Models\Modulo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Rol;
use App\Models\UsuariosRol;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('authorized');
        $this->middleware('roles:SUPERADMIN,ADMINISTRADOR', ['only' => 'index']);
        $this->middleware('saveaction', ['except' => ['index', 'create', 'show', 'edit']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Modulo::all();

        return view('admin.modules.index', ['modules' => $modules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'usuario_id' => 'required|numeric',
            'nombre' => 'required|min:3|string|unique:cat_modulos',
            'descripcion' => 'required|min:3|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('modules.create')
                ->withErrors($validator)
                ->withInput();
        }

        try{
            DB::beginTransaction();
            $module = Modulo::create($request->all());
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
        }

        if($module){
            return redirect()->route('modules.create')->with(['status' => 'ok', 'msg' => 'Módulo Agregado Correctamente!']);
        }

        return redirect()->route('modules.create')->with(['status' => 'error', 'msg' => 'Ocurrio un error! :(']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function show(Modulo $module)
    {
        return view('admin.modules.create', ['module' => $module]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function edit(Modulo $module)
    {
        return view('admin.modules.create', ['module' => $module]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modulo $module)
    {
        $validator = Validator::make($request->all(), [
            'usuario_id' => 'required|numeric',
            'nombre' => 'required|min:3|string|unique:cat_roles',
            'descripcion' => 'required|min:3|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('modules.create')
                ->withErrors($validator)
                ->withInput();
        }

        $module->nombre = $request->nombre;

        $module->descripcion = $request->descripcion;

        $module->usuario_id = $request->usuario_id;
        try{
            DB::beginTransaction();
            $module->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('modules.edit', $module)->with(['status' => 'error', 'msg' => 'Ocurrio un error... reintente !']);
        }

        return redirect()->route('modules.edit', $module)->with(['status' => 'ok', 'msg' => 'Editado Correctamente !']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Modulo  $modulo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modulo $module)
    {
        try{
            DB::beginTransaction();
            $module->delete();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('modules.index')->with(['status' => 'error', 'msg' => 'Ocurrio un error... intente de nuevo !']);
        }

        return redirect()->route('modules.index')->with(['status' => 'ok', 'msg' => 'Eliminado Correctamente !']);
    }

    public function access()
    {
        $modules = Modulo::all();

        $roles = Rol::where('id', '!=', 1)->where('id', '!=', 2)->get();

        return view('admin.modules.access', ['modules' => $modules, 'roles' => $roles]);
    }

    public function paccess(Request $request)
    {
        try{
            DB::beginTransaction();
            $usuarioRol = UsuariosRol::create($request->all());
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('modules.access.create')->with(['status' => 'error', 'msg' => 'Ocurrio un error... verifique e intentelo de nuevo']);
        }

        return redirect()->route('modules.access.create')->with(['status' => 'ok', 'msg' => 'Solicitud Generada, espere la autorización por parte del responsable']);
    }

    public function authorized()
    {
        $module = UsuariosRol::where('usuario_id', Auth::user()->id)->whereBetween('rol_id', [1,2])->take(1)->get();

        $usuariosRol = UsuariosRol::where('modulo_id', $module[0]->modulo_id)
            ->where('usuario_id', '!=', Auth::user()->id)
            ->where('autorizado', 0)->get();

        return view('admin.modules.authorized', ['requests' => $usuariosRol]);
    }

    public function pauthorized(Request $request, UsuariosRol $req)
    {
        $req->autorizado = 1;
        try{
            DB::beginTransaction();
            $req->save();
            DB::commit();
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->route('modules.access.authorized.index')->with(['status' => 'error', 'msg' => 'Ocurrio un error...']);
        }

        return redirect()->route('modules.access.authorized.index')->with(['status' => 'ok', 'msg' => 'Usuario autorizado correctamente']);
    }
}
