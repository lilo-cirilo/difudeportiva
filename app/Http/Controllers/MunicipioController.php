<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\InversionPrograma;
use App\Models\Municipio;
use View;
use Illuminate\Http\JsonResponse;

use Illuminate\Support\Facades\Auth;
use App\DataTables\InversionesProgramaDataTable;

class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('municipios.index');
    }
    public function list(Request $request){
        $columns = array(
            0 => 'cat_municipios.nombre',
            1 => 'cat_distritos.nombre',
            2 => 'latitud',
            3 => 'longitud'
        );

        $totalData = Municipio::count();
        $start = $request->input('start');
        $limit = $request->input('length');
        $limit = ($limit==-1)?$totalData:$limit;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $buscar=$request->input('columns.0.search.value','');

        $municipios = Municipio::join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->where('cat_municipios.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_distritos.nombre', 'like', '%'.$buscar.'%')
        ->offset($start)
        ->limit($limit)
        ->orderBy($order,$dir)
        ->get([
          'cat_municipios.id','cat_municipios.nombre as municipio',
          'cat_distritos.id as distrito_id',
          'cat_distritos.nombre as distrito',
          'latitud',
          'longitud'
        ]);

        $totalFiltered=Municipio::join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->where('cat_municipios.nombre', 'like','%'.$buscar.'%')
        ->orwhere('cat_distritos.nombre', 'like', '%'.$buscar.'%')
        ->count();

        $data = array();

        if($municipios)
        {
            foreach($municipios as $municipio)
            {
                $nestedData['id'] = $municipio->id;
                $nestedData['Nombre'] = $municipio->municipio;
                $nestedData['distrito_id'] = $municipio->distrito_id;
                $nestedData['Distrito'] = $municipio->distrito;
                $nestedData['Latitud'] = $municipio->latitud;
                $nestedData['Longitud'] = $municipio->longitud;
                $nestedData['Acciones'] ='
                <a href="' . route('municipios.show', $municipio->id) . '" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Ver</a>
                <a href="' . route('municipios.edit', $municipio->id) . '" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i> Editar</a>
                <a class="btn btn-danger btn-sm" onclick="municipio_delete(' . $municipio->id . ')"><i class="fa fa-trash"></i> Eliminar</a>
                ';

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return json_encode($json_data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('municipios.create_edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request){
         if($request->ajax()){
             try {
                 DB::beginTransaction();
                 $new = Municipio::create($request->all());
                 DB::commit();
                 return response()->json(array('success' => true, 'id' => $new->id));
             }
             catch(Exeption $e) {
                 DB::rollBack();
                 return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
             }
         }
     }

     public function show($id){
         $municipio = Municipio::findOrFail($id);
         return view('municipios.show')->with('municipio',$municipio);
     }
     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id){
         $municipio = Municipio::findOrFail($id);
         return view('municipios.create_edit')->with('municipio',$municipio);
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id){
         if($request->ajax()) {
             try {
                 DB::beginTransaction();

                 Municipio::find($id)->update($request->all());

                 DB::commit();

                 return response()->json(array('success' => true, 'id' => $id));
             }
             catch(Exeption $e) {
                 DB::rollBack();
                 return response()->json(array('success' => false, 'message' => 'Error al registrar en la BD: ' . $e->getMessage()));
             }
         }
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy(Request $request,$id){
         if($request->ajax()) {
             try {
                 DB::beginTransaction();
                 Municipio::destroy($id);
                 DB::commit();

                 return response()->json(array('success' => true, 'id' => $id));
             }
             catch(Exeption $e) {
                 DB::rollBack();
                 return response()->json(array('success' => false, 'message' => 'Error al eliminar en la BD: ' . $e->getMessage()));
             }
         }
     }

    public function municipiosSelect(Request $request)
    {
        if($request->ajax())
        {
        
            $municipios = DB::table('cat_municipios')
            ->select([
                'cat_municipios.id',
                DB::raw("CONCAT('(', cat_regiones.nombre, ') ', '(', cat_distritos.nombre, ') ', cat_municipios.nombre) as nombre"),
                'latitud as lat', 'longitud as lng'
            ])
            ->where('cat_municipios.nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
            ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');


            $rol_enlace = 0;
            $rol_enlace_array = [];

            if(Auth::user()->hasRolesModulo(['ENLACE CAÑADA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'CAÑADA');
                //$municipios->where('cat_regiones.nombre', '=', 'CAÑADA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE COSTA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'COSTA');
                //$municipios->where('cat_regiones.nombre', '=', 'COSTA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE ISTMO'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'ISTMO');
                //$municipios->where('cat_regiones.nombre', '=', 'ISTMO');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE MIXTECA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'MIXTECA');
                //$municipios->where('cat_regiones.nombre', '=', 'MIXTECA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE PAPALOAPAM'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'PAPALOAPAM');
                //$municipios->where('cat_regiones.nombre', '=', 'PAPALOAPAM');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE SIERRA NORTE'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'SIERRA NORTE');
                //$municipios->where('cat_regiones.nombre', '=', 'SIERRA NORTE');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE SIERRA SUR'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'SIERRA SUR');
                //$municipios->where('cat_regiones.nombre', '=', 'SIERRA SUR');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE VALLES CENTRALES'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'VALLES CENTRALES');
                //$municipios->where('cat_regiones.nombre', '=', 'VALLES CENTRALES');
            }

            if($rol_enlace === 1) {
                $municipios->whereIn('cat_regiones.nombre', $rol_enlace_array);
            }
            
            $municipios = $municipios
            ->take(10)
            ->get()
            ->toArray();

            /*$municipios = Municipio::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
            ->take(10)
            ->get()
            ->toArray();*/
            return response()->json($municipios);
        }
    }

    public function buscarMunicpio($token, $municipio)
    {
        return new JsonResponse(Municipio::select('id', 'nombre', 'distrito_id')->where('nombre', 'like', "%$municipio%")->take(10)->get());
	}
	
	public function inversiones($id, InversionesProgramaDataTable $dataTable){
		return $dataTable        
        ->with('municipio_id', $id)
        ->render('municipios.inversiones', ['municipio' => Municipio::find($id)]);		
	}
}
