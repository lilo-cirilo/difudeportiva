<?php

namespace App\Http\Controllers;

use App\DataTables\BeneficiariosDataTable;
use App\DataTables\PersonasDataTable;
use App\DataTables\PeticionesDataTable;
use App\Models\Modulo;
use App\Models\Programa;
use App\Models\BeneficiosprogramasSolicitud;
use App\Models\Statusproceso;
use App\Models\EstadosSolicitud;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeticionBaseController extends Controller {

    
  
	public function index(PeticionesDataTable $dataTable) {
		//Las peticiones se filtrarán de acuerdo al area del usuario
		return $dataTable
        ->with(['area_id' => auth()->user()->persona->empleado->area_id])
        ->render('peticiones.index');        
    }

	public function directores(PeticionesDataTable $dataTable) {
		return $dataTable->with('area_id', auth()->user()->persona->empleado->area_id)->render('peticiones.directores.index');
	}

	public function getCantidad(){
			return BeneficiosprogramasSolicitud::peticiones(null, auth()->user()->persona->empleado->area_id)->where('cat_statusprocesos.status', 'VINCULADO')->count();      
	}

	public function update($peticion_id, Request $request){ 
		$peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
		try {
				DB::beginTransaction();
				$status_solicitud = EstadosSolicitud::updateOrCreate([
					'beneficioprograma_solicitud_id' => $peticion_id,
					'statusproceso_id' => $request['estatus'],
					'motivo_programa_id' => $request['motivo_bitacora'],
					'observacion' => $request['observacion']
				],[
					'usuario_id' => auth()->user()->id
				]);

				//Agregando a la bitácora
				auth()->user()->bitacora(request(), [
						'tabla' => 'status_solicitudes',
						'registro' => $status_solicitud->id . '',
						'campos' => json_encode($status_solicitud) . '',
						'metodo' => 'POST'
				]);

				DB::commit();
		} catch(\Exception $e) {
				DB::rollBack();
				return response()->json(['errors' => array(['code' => 409, 'message' => 'Error: ' . $e->getMessage()])], 409);
		}    
	}

	public function show($peticion_id, PersonasDataTable $personas, BeneficiariosDataTable $beneficiarios) {
		$peticion = BeneficiosprogramasSolicitud::findOrFail($peticion_id);
		$motivos;

		$area_id = auth()->user()->persona->empleado->area_id;
        $areas = array_map( function($o) { return $o->id; }, DB::select('call getProgramas(?)',[$area_id]));
        $areas[$area_id] = $area_id;
        $motivos = DB::table('motivos_programas')
        ->select(['motivos_programas.id', 'cat_motivos.motivo'])
        ->join('cat_motivos', 'cat_motivos.id', '=', 'motivos_programas.motivo_id')
        ->where('programa_id',3)
        ->get();

        $permiso_solo_lectura = false;

        /* if($this->modulo === "atnciudadana"){
            $permiso_solo_lectura = true;
        }else{
            abort(403,'Permisos insuficientes');
        } */

		//Si es la primera vez que ve la peticion o le da click entonces el estatus cambia a VALIDANDO
		if($peticion->estadosSolicitud->contains('statusproceso_id', 3) && $peticion->estadosSolicitud->contains('statusproceso_id', 9) && !$peticion->estadosSolicitud->contains('statusproceso_id', 4) && !$permiso_solo_lectura){
            $peticion->estadosSolicitud()->create([
                'statusproceso_id' => 4, //VALIDANDO
                'usuario_id' => auth()->user()->id
            ]);
            auth()->user()->bitacora(request(), [
                'tabla' => 'status_solicitudes',
                'registro' => $peticion->estadosSolicitud()->latest()->first()->id . '',
                'campos' => json_encode($peticion->estadosSolicitud()->latest()->first()) . '',
                'metodo' => request()->method()
            ]);
            $peticion = BeneficiosprogramasSolicitud::find($peticion_id);
		}

		//Lista de los estatus posibles para poder cambiar la peticion menos el de CANCELADO ni el de Vo. Bo.
		$estatus = $peticion->estadosSolicitud()->latest()->first()->statusproceso_id;
		if($estatus == 6 || $estatus == 7 || $estatus == 8){
				//Si ya esta finalizada, cancelada o rechazada ya no puede cambiar de estatus paps
				$estatus = Statusproceso::where('id',$estatus)->get();
		}else{
				$estatus = Statusproceso::where('id','>=',$estatus)->where('status','!=','Vo. Bo.')->where('status','!=','PRE-SOLICITUD')->get();
		}

		$timeline = Array();
		$programas = BeneficiosprogramasSolicitud::join('beneficiosprogramas','beneficiosprogramas.id','beneficiosprogramas_solicitudes.beneficioprograma_id')
				->where('beneficiosprogramas_solicitudes.id',$peticion_id)->get(['beneficiosprogramas.id','beneficiosprogramas.nombre']);
			foreach ($programas as $programa) {
					array_push($timeline, ['nombre'=> $programa->nombre,'lineas'=>EstadosSolicitud::timelinepeticion($peticion->id)->get()->toarray()]);
			}

		if (request()->get('table') == 'beneficiarios') {
				return $beneficiarios->with([
						'beneficiosprogramas_solicitud_id' => $peticion->id, 
						'modulo' => $this->getModulo(),//$this->modulo ?? null,
						'estatus' => $peticion->statusActual(),
						'permiso_solo_lectura' => $permiso_solo_lectura
					])->render('peticiones.show', [
			'personas' => $personas, 
			'beneficiarios' => $beneficiarios, 
			'peticion' => $peticion,
			'modulo' => $this->getModulo(),//$this->modulo ?? null,
			'motivos' => $motivos,
			'timeline' => $timeline,
			'estatus' => $estatus
		]);
		}

		$beneficiarios->with([
				'beneficiosprogramas_solicitud_id' => $peticion->id, 
				'modulo' => $this->getModulo(),//$this->modulo ?? null,
				'estatus' => $peticion->estadoActual(),
				'permiso_solo_lectura' => $permiso_solo_lectura
		])->render('peticiones.show', [
				'personas' => $personas, 
				'beneficiarios' => $beneficiarios, 
				'peticion' => $peticion,
				'modulo' => $this->getModulo(),//$this->modulo ?? null,
				'motivos' => $motivos,
				'timeline' => $timeline,
				'estatus' => $estatus
		]);
        

		return $personas->with('tipo', 'beneficiarios')
					->render('peticiones.show', [
			'personas' => $personas, 
						'beneficiarios' => $beneficiarios, 
						'peticion' => $peticion,
						'modulo' => $this->getModulo(),//$this->modulo ?? null,
						'motivos' => $motivos,
						'timeline' => $timeline,
						'estatus' => $estatus
			]);
    }
    
    public function getModulo(){
        return null;
    }
}    