<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EstadoCivil;

class EstadoCivilController extends Controller {
    
    public function select(Request $request) {
        if($request->ajax()) {
            $estados = EstadoCivil::where('nombre', 'LIKE', '%' . $request->input('search') . '%')
                //->take(5)
                ->get()
                ->toArray();
            return response()->json($estados);
        }
    }
}