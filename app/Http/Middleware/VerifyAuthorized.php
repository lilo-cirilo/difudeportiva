<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class VerifyAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,  $guard = null)
    {
        if (! Auth::guard($guard)->user()->autorizado) {
            abort(403, 'No tienes permiso para acceder aqui !');
        }
        if (! Auth::guard($guard)->user()->ficha) {
            //return redirect('/home')->with('status', 'Por favor imprime tu ficha de registro para continuar');
        }
        /*if( !Auth::guard($guard)->user()->persona->empleado->validado){
            return redirect('/completar');
        }*/
        //$request->midato = 'Nuevo dato';
        return $next($request);
    }
}
