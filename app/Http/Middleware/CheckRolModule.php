<?php

namespace App\Http\Middleware;

use Closure;

class CheckRolModule
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $modulo_id = array_slice(func_get_args(), 2, 1);
        $roles = array_slice(func_get_args(), 3);
        // dd($modulo_id);
        if(auth()->user()->hasRolesModulo($roles, $modulo_id[0])) {
            return $next($request);
        }

        abort(403, 'No cuentas con los permisos necesarios.');
    }
}
