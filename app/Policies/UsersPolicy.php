<?php

namespace App\Policies;

use App\Models\Usuario;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsersPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function edit(Usuario $auth, $user)
    {
        return $auth->id === $user->id;
    }

    public function update(Usuario $auth, $user)
    {
        return $auth->id === $user->id;
    }

    public function destroy(Usuario $auth, $user)
    {
        return $auth->id === $user->id;
    }

    public function status(Usuario $auth, $user)
    {    
        dd($user);
    }
}
