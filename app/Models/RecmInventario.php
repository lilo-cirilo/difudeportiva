<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmInventario extends Model
{
    use SoftDeletes;
    protected $table = 'recm_inventarios';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['cantidad', 'presentacion_producto_id'];

    public function productos(){
        return $this->hasOne('App\Models\RecmPresentacionProducto', 'id', 'presentacion_producto_id');
    }
}
