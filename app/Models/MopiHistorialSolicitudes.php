<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiHistorialSolicitudes extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_historial_solicitudes';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['folio','subtotal','total','nombre_entrega','primer_apellido_recibe'];
}
