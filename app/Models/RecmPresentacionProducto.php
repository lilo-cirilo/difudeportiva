<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmPresentacionProducto extends Model
{
    use SoftDeletes;
    protected $table = 'recm_presentacion_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['precio', 'producto_id', 'presentacion_id', 'partida_id'];

    protected $with = ['presentacion','producto'];

    public function producto(){
        return $this->belongsTo('App\Models\RecmCatProducto');
    }

    public function presentacion(){
        return $this->belongsTo('App\Models\RecmCatPresentacion');
    }
    
    public function entradas(){
        return $this->belongsToMany('App\Models\RecmEntradas', 'recm_entradas_productos', 'presentacion_producto_id', 'entrada_id');
    }
    
    public function inventario(){
        return $this->belongsTo('App\Models\RecmInventario', 'id', 'presentacion_producto_id');
    }

    public function partida(){
        return $this->belongsTo('App\Models\RecmCatPartida');
    }
}

