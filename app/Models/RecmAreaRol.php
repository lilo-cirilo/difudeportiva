<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class RecmAreaRol extends Model
{
    use SoftDeletes;
    // protected $table = 'recm_area_rols'; // nombre anterior
    protected $table = 'recm_area_usuario_responsable'; // se cambio nombre
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    // protected $fillable = ['estatus','color'];
}