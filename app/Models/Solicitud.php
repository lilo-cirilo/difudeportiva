<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitud extends Model
{
	protected $table = 'solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["folio","fechaoficio","fecharecepcion","numoficio","asunto","compromiso","solicitante_id","tiposrecepcion_id","tiposremitente_id","persona_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeAllSolicitudes($query,$solicitud_id) {
        $query = Solicitud::select(
            DB::raw(
            "solicitudes.id,solicitudes.folio, solicitudes.fecharecepcion as fecharecepcion, solicitudes.asunto as asunto, cat_tiposrecepciones.tipo as recepcion, cat_tiposremitentes.tipo as tipo, ".
            "CASE ".
	            "WHEN cat_tiposremitentes.id = 4 THEN ".
                    "( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) ".
                        "FROM solicitudes_personales sp, personas p ".
                        "WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) ".
                "WHEN cat_tiposremitentes.id = 3 THEN ".
                    "( SELECT d.nombre ".
                        "FROM solicitudes_dependencias sd, cat_instituciones d ".
                        "WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) ".
                "WHEN cat_tiposremitentes.id = 2 THEN ".
                    "( SELECT m.nombre ".
                        "FROM solicitudes_municipios sm, cat_municipios m ".
                        "WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) ".
		        "WHEN cat_tiposremitentes.id = 1 THEN ".
                    "( SELECT r.nombre ".
                        "FROM solicitudes_regiones sr, cat_regiones r ".
                        "WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) ".
            "END AS remitente, ".
            "(SELECT COUNT(*) FROM beneficiosprogramas_solicitudes WHERE solicitud_id = solicitudes.id AND deleted_at IS NULL) AS beneficios, ".
            "(SELECT COUNT(*) FROM estados_solicitudes WHERE beneficioprograma_solicitud_id in (SELECT id FROM beneficiosprogramas_solicitudes WHERE solicitud_id = solicitudes.id AND deleted_at IS NULL) AND statusproceso_id = 3 AND deleted_at IS NULL) as turnados, ".
            "IF ((select count(*) from beneficiosprogramas_solicitudes where beneficiosprogramas_solicitudes.solicitud_id = solicitudes.id) = 0, 'PENDIENTE', (".
		    "CASE WHEN	IFNULL(".
			    "(  SELECT (SELECT count(es2.statusproceso_id) ".
							"FROM estados_solicitudes es2 ".
							"WHERE es2.beneficioprograma_solicitud_id = es.beneficioprograma_solicitud_id ".
							"AND (es2.statusproceso_id >= 6 AND es2.statusproceso_id <= 8) AND es2.deleted_at IS NULL ) AS procesos ".
			        "FROM ".
				        "beneficiosprogramas_solicitudes bps,	estados_solicitudes es ".
			        "WHERE ".
				        "bps.id = es.beneficioprograma_solicitud_id AND bps.solicitud_id = solicitudes.id AND bps.deleted_at IS NULL AND es.deleted_at IS NULL ".
			        "GROUP BY bps.solicitud_id, es.beneficioprograma_solicitud_id ".
			        "HAVING procesos = 0 ".
			        "LIMIT 1),-1) = 0 then 'PROCESANDO' ELSE 'FINALIZADO' ".
		    "END)) AS status_g, ".
		    "CASE WHEN	IFNULL(".
                "(  SELECT (SELECT count(es2.statusproceso_id) ".
                            "FROM estados_solicitudes es2 ".
                            "WHERE es2.beneficioprograma_solicitud_id = es.beneficioprograma_solicitud_id ".
                            "AND (es2.statusproceso_id >= 6 AND es2.statusproceso_id <= 8)) AS procesos ".
                    "FROM ".
                        "beneficiosprogramas_solicitudes bps,	estados_solicitudes es ".
                    "WHERE ".
				        "bps.id = es.beneficioprograma_solicitud_id AND bps.solicitud_id=solicitudes.id ".
			        "GROUP BY bps.solicitud_id, es.beneficioprograma_solicitud_id ".
			        "HAVING procesos = 0 ".
                    "LIMIT 1),-1) = 0 THEN (SELECT DATEDIFF(NOW(),solicitudes.fecharecepcion)) ".
                    "ELSE".
			             "(SELECT DATEDIFF(".
			                "(SELECT MAX(es3.created_at) ".
				            "FROM estados_solicitudes es3 ".
				            "INNER JOIN beneficiosprogramas_solicitudes bpsl ON es3.beneficioprograma_solicitud_id = bpsl.id ".
				            "WHERE bpsl.solicitud_id = solicitudes.id AND (es3.statusproceso_id >= 6 AND es3.statusproceso_id <= 8)),solicitudes.fecharecepcion)) ".
            "END AS dias, " .
		    "CASE WHEN	IFNULL(".
			    "(  SELECT (SELECT count(es2.statusproceso_id) ".
							"FROM estados_solicitudes es2 ".
							"WHERE es2.beneficioprograma_solicitud_id = es.beneficioprograma_solicitud_id ".
							"AND (es2.statusproceso_id >= 6 AND es2.statusproceso_id <= 8)) AS procesos ".
			        "FROM ".
				        "beneficiosprogramas_solicitudes bps,	estados_solicitudes es ".
			        "WHERE ".
				        "bps.id = es.beneficioprograma_solicitud_id AND bps.solicitud_id=solicitudes.id ".
			        "GROUP BY bps.solicitud_id, es.beneficioprograma_solicitud_id ".
			        "HAVING procesos = 0 ".
                    "LIMIT 1),-1) = 0 THEN NOW() ".
                    "ELSE ".
			                "DATE_FORMAT((SELECT MAX(es3.created_at) ".
				            "FROM estados_solicitudes es3 ".
				            "INNER JOIN beneficiosprogramas_solicitudes bpsl ON es3.beneficioprograma_solicitud_id = bpsl.id ".
				            "WHERE bpsl.solicitud_id = solicitudes.id),'%d-%m-%Y') ".
		    "END AS fecha_fin "
            )
        )
        ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','=','cat_tiposremitentes.id')
        ->join('cat_tiposrecepciones','solicitudes.tiposrecepcion_id','=','cat_tiposrecepciones.id');

        if($solicitud_id){
            $query->where('solicitudes.id','=',$solicitud_id);
        }

        return $query;
    }

    public function scopeTimeLine($query,$solicitud_id) {
        $query = Solicitud::select(
                DB::raw(
                        "solicitudes.id as idsolicitud,".
                        "programas_solicitudes.id as idprograma,".
                        "cat_statusprocesos.`status` as est,".
                        "status_solicitudes.statusproceso_id as idestado,".
                        "status_solicitudes.created_at as fecha"
                        )
                )
                ->join('programas_solicitudes','solicitudes.id','=','programas_solicitudes.solicitud_id')
                ->join('status_solicitudes','status_solicitudes.programas_solicitud_id','=','programas_solicitudes.id')
                ->join('cat_statusprocesos','status_solicitudes.statusproceso_id','=','cat_statusprocesos.id')
                ->where('solicitudes.id','=',$solicitud_id);

        return $query;
    }

    public function persona(){
        return $this->belongsTo('App\Models\Persona');
    }

    public function tiposrecepcion(){
    	return $this->belongsTo('App\Models\Tiposrecepcion');
    }

    public function tiposremitente(){
    	return $this->belongsTo('App\Models\Tiposremitente');
    }

    public function remitente(){

    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

    public function dependenciassolicitudes(){
        return $this->hasOne('App\Models\DependenciasSolicitud');
    }

    public function programassolicitudes(){
        return $this->hasMany('App\Models\ProgramasSolicitud');
    }   

    public function documentossolicitudes(){
        return $this->hasMany('App\Models\DocumentosSolicitud');
    }

    public function solicitudespersonas(){
        return $this->hasMany('App\Models\SolicitudesPersona');
    }

    public function solicitudespersonales(){
        return $this->hasMany('App\Models\SolicitudesPersonales');
    }

    public function formato_fecha_recepcion() {
        return (\DateTime::createFromFormat('Y-m-d', $this->fecharecepcion))->format('d/m/Y');
    }
    
    public function formato_fecha_oficio() {
        return (\DateTime::createFromFormat('Y-m-d', $this->fechaoficio))->format('d/m/Y');
    }

    public function solicitudpersonales() {
        return $this->hasOne('App\Models\SolicitudesPersonales');
    }

    public function solicitudmunicipales() {
        return $this->hasOne('App\Models\SolicitudesMunicipales');
    }

    public function solicitudregionales() {
        return $this->hasOne('App\Models\SolicitudesRegiones');
    }

    public function solicituddependencias() {
        return $this->hasOne('App\Models\SolicitudesDependencias');;
    }

    public function anexos(){
        return $this->hasMany('App\Models\SolicitudAnexo');
    }

    public function statussolicitudes() {
        return $this->hasMany('App\Models\StatusSolicitud');
    }

    public function solicitudevento() {
        return $this->hasOne('App\Models\EventosSolicitudes');
    }
}