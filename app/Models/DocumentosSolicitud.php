<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentosSolicitud extends Model
{
    protected $table = 'documentos_solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["documentos_persona_id","beneficio_solicitud_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function documentospersona(){
    	return $this->belongsTo('App\Models\DocumentosPersona', 'documentos_persona_id');
    }

    public function beneficioSolicitud(){
    	return $this->belongsTo('App\Models\beneficiosprogramasSolicitud');
    }

}
