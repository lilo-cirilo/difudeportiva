<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RespuestasAbiertas extends Model
{
  protected $table = 'respuestasabiertas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['preguntas_persona_id', 'respuesta'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function preguntaspersona(){
    return $this->belongsTo('App\Models\PreguntasPersona');
  }
}
