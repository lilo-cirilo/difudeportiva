<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TiposSoporteServicios extends Model
{
	protected $table = 'tipossoporteservicios';
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $fillable=["tipo"];
	protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    

public function soporteservicios(){
		return $this->belongsTo('App\Models\SoporteServicios', 'servicio_id', 'id');
	}

}
