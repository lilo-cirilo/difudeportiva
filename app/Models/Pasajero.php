<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pasajero extends Model
{
  use SoftDeletes;

  protected $table = 'dtll_pasajeros';
  protected $dates = ['deleted_at'];
  protected $fillable = ['tiposervicio_id', 'tipopasajero_id', 'fechaIniServ', 'fechaFinServ', 'timestapFinServ', 'numMesesServ', 'folioPago', 'lugarPago_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');


}
