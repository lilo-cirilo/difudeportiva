<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Indicador extends Model
{
    use SoftDeletes;
    protected $table = 'indicadores';
    protected $dates = ['deleted_at'];
    protected $fillable = ['indicador_id','municipio_id','poblacion_total','porcentaje','periodo_ini','periodo_fin','fuente_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function indicador(){
        return $this->belongsTo('App\Models\CatIndicador');
    }

    public function municipio(){
        return $this->belongsTo('App\Models\Municipio');
    }

    public function fuente()
    {
        return $this->belongsTo('App\Models\CatFuenteIndicador');
    }

}
