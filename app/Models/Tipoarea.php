<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipoarea extends Model
{
    protected $table = 'cat_tipoareas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function areas(){
		return $this->hasMany('App\Area');
	}
}
