<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dependencia extends Model
{
    protected $table = 'cat_instituciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","calle","numero","colonia","telefono","email","encargado","cargoencargado","tipoinstitucion_id","entidad_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query) {
        $query->join('cat_entidades', 'cat_entidades.id', '=', 'cat_instituciones.entidad_id');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function entidad(){
		return $this->belongsTo('App\Models\Entidad');
    }
    
	public function tipo(){
		return $this->belongsTo('App\Models\TipoInstitucion','tipoinstitucion_id');
	}

    public function dependenciassolicitudes(){
        return $this->hasMany('App\Models\DependenciasSolicitud');
    }


    public function entradasproductosdependencias(){
        return $this->hasMany('App\Models\EntradasproductosDependencia');
    }

    public function municipio(){
		return $this->belongsTo('App\Models\Municipio');
    }
}
