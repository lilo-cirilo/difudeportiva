<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ControlRutas extends Model
{
    use SoftDeletes;

    /* protected $connection = 'mysql_server_real'; */

    protected $table = 'controlrutas';
    protected $dates = ['deleted_at'];
    protected $fillable = ["recorrido_id", "unidad_id", "conductor_id", "fechahora_sesion_ini", "fechahora_sesion_fin"];

    public function unidad()
    {
        return $this->belongsTo('App\Models\Unidad');
    }

    public function conductor()
    {
        return $this->belongsTo('App\Models\Conductor');
    }

    public function recorrido()
    {
        return $this->belongsTo('App\Models\Recorridos');
    }

    public function registroskms()
    {
        return $this->hasMany('App\Models\DTLL\RegistroKm', 'controlruta_id');
    }
    public function servicios()
    {
        return $this->hasMany('App\Models\DTLL\Servicio', 'controlruta_id');
    }

    public function scopeHistorialConductores($query)
    {
        $query
            ->join('unidades', 'unidades.id', 'unidad_id')
            ->join('recorridos', 'recorridos.id', 'recorrido_id')
            ->join('conductores', 'conductores.id', '=', 'controlrutas.conductor_id')
            ->join('empleados', 'empleados.id', '=', 'conductores.empleado_id')
            ->join('personas', 'personas.id', '=', 'empleados.persona_id');
    }
}