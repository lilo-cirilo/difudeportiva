<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudesDependencias extends Model
{
    protected $table = 'solicitudes_dependencias';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","dependencia_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function dependencia(){
    	return $this->belongsTo('App\Models\Dependencia');
	}

	public function solicitud(){
		return $this->belongsTo('App\Models\Solicitud');
	}
}
