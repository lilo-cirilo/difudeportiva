<?php

namespace App\Models\Fotradis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pasajero extends Model
{
  protected $table = 'dtll_pasajeros';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ["tiposervicio_id","tipopasajero_id","fechaIniServ","fechaFinServ","timestampFinServ","numMesesServ","folioPago","lugarPago_id",
                          "numPasajero","numPasajeroAmbos","persona_id","persona_emergencias","telefono_emergencias","usuario_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function cattiposervicio ()
  {
    return $this->belongsTo('App\Models\Fotradis\CatTipoServicio','tiposervicio_id','id');
  }

  public function cattipopasajero()
  {
    return $this->belongsTo('App\Models\Fotradis\CatTipoPasajero','tipopasajero_id','id');
  }

  public function catlugarpago()
  {
    return $this->belongsTo('App\Models\Fotradis\CatLugarpago','lugarPago_id','id');
  }

  public function persona()
  {
    return $this->belongsTo('App\Models\Persona','persona_id','id');
  }

  public function usuario()
  {
    return $this->belongsTo('App\Models\Usuario','usuario_id','id');
  }
}
