<?php

namespace App\Models\Fotradis;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatLugarPago extends Model
{
  protected $table = 'dtll_cat_lugarespago';
  use softDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ["id","nombre"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function pasajero ()
  {
    return $this->hasMany('App\Models\Fotradis\Pasajero','id','lugarPago_id');
  }
}
