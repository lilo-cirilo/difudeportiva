<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Escuela extends Model
{
  protected $table = 'cat_escuelas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre', 'tipo', 'nivel_id', 'clave', 'servicio', 'telefono', 'correo', 'pagina_web', 'calle', 'numero', 'colonia', 'codigo_postal', 'entidad_id', 'municipio_id', 'localidad_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function entidad(){
    return $this->belongsTo('App\Models\Entidad');
  }

  public function municipio(){
    return $this->belongsTo('App\Models\Municipio');
  }

  public function localidad(){
    return $this->belongsTo('App\Models\Localidad');
  }

  public function nivel(){
    return $this->belongsTo('App\Models\NivelEscuela');
  }
}
