<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tiposentrada extends Model
{
	protected $table = 'cat_tiposentradas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function entradasproductos(){
        return $this->hasMany('App\EntradasProducto');
    }
}
