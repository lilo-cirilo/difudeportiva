<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Desempeños extends Model
{
    protected $table = 'cat_desempenios';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function soportecalificaciones(){
		return $this->belongsTo('App\Models\SoporteCalificaciones');
	}
}
