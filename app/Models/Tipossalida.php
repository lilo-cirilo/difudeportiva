<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipossalida extends Model
{
	protected $table = "cat_tipossalidas";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["tipo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function salidasproductos(){
        return $this->hasMany('App\Models\SalidasProducto');
    }
}
