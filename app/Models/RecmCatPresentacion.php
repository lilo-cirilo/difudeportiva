<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmCatPresentacion extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_presentacions';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['presentacion', 'usuario_id'];

    public function productos(){
        return $this->hasMany('App\Models\RecmCatProducto', 'recm_presentacion_productos', 'producto_id', 'presentacion_id');
    }
}
