<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Afuncionalespersona extends Model
{
    protected $table = 'afuncionalespersonas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["limitacion_id","persona_id","discapacidad_id","usuario_id","tiempodiscapacidad","otrocontacto"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function limitacion(){
    	return $this->belongsTo('App\Models\Limitacion');
    }

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function discapacidad(){
    	return $this->belongsTo('App\Models\Discapacidad');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function afuncionalessilla(){
    	return $this->hasOne("App\Models\Afuncionalessilla");
    }
}
