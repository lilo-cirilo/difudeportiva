<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramasSolicitud extends Model {
    protected $table = 'programas_solicitudes';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["programa_id","solicitud_id","cantidad","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopePeticiones($query, $programa_padre, $area_id) {

        $db_raw = "programas_solicitudes.id AS programa_solicitud_id, programas_solicitudes.cantidad as cantidad, ".
                        "solicitudes.id AS solicitud_id, programas.id AS programa_id, solicitudes.folio AS folio,".
                        "CASE ".
	                        "WHEN solicitudes.tiposremitente_id = 4 THEN ".
                                "( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) ".
                                    "FROM solicitudes_personales sp, personas p ".
                                        "WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 3 THEN ".
                                "( SELECT d.nombre ".
                                    "FROM solicitudes_dependencias sd, cat_instituciones d ".
                                        "WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 2 THEN ".
                                "( SELECT m.nombre ".
                                    "FROM solicitudes_municipios sm, cat_municipios m ".
                                        "WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) ".
		                    "WHEN solicitudes.tiposremitente_id = 1 THEN ".
                                "( SELECT r.nombre ".
                                    "FROM solicitudes_regiones sr, cat_regiones r ".
                                        "WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) ".
                        "END AS remitente, ".
                        "cat_tiposremitentes.tipo as tipo, ".
                        "programas.nombre AS apoyo, cat_statusprocesos.status AS estatus, ".
                        "CASE WHEN IFNULL((SELECT count(*) from cat_statusprocesos where programas_solicitud_id = programas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 ), -1) = 0 ".
                        "THEN (SELECT DATEDIFF(CURDATE(), (SELECT created_at FROM status_solicitudes WHERE programas_solicitud_id = programas_solicitudes.id AND statusproceso_id = 3))) ".
                        "ELSE (SELECT DATEDIFF((SELECT created_at FROM status_solicitudes WHERE	programas_solicitud_id = programas_solicitudes.id AND statusproceso_id >= 6 AND statusproceso_id != 9 AND status_solicitudes.deleted_at is null), ".
                        "(SELECT created_at FROM status_solicitudes WHERE programas_solicitud_id = programas_solicitudes.id	AND statusproceso_id = 3))) END as dias_transcurridos, ".
                        "(select created_at from status_solicitudes where programas_solicitud_id = programas_solicitudes.id and statusproceso_id = 3) as fecha";

        if($area_id) {
            $query->select(DB::raw($db_raw.", area_id, cat_areas.nombre as area_responsable"))
                    ->join('programas','programas.id','=','programas_solicitudes.programa_id')
                    ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
                    ->join('status_solicitudes as ss', 'ss.programas_solicitud_id', '=', 'programas_solicitudes.id')
                    ->join('cat_statusprocesos', 'cat_statusprocesos.id', '=', 'ss.statusproceso_id' )
                    ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','=','cat_tiposremitentes.id')
                    ->join('programas_responsables','programas_responsables.programa_id','programas.id')
                    ->join('areas_responsables','areas_responsables.id', 'programas_responsables.areas_responsable_id')
                    ->join('cat_areas','cat_areas.id', 'areas_responsables.area_id')
                    ->where(function($query) use ($area_id){
                        $areas = array_map( function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)',[$area_id]));
                        //$areas = array_map(create_function('$o', 'return $o->id_hijo;'), DB::select('call getAreasHijas(?)',[$area_id]));
                        $areas[$area_id] = $area_id;
                        $query
                        ->whereRaw('ss.created_at = (select max(sts.created_at) from status_solicitudes as sts where ss.programas_solicitud_id = sts.programas_solicitud_id and sts.deleted_at is null)')
                        ->whereIn('area_id', $areas);                        
                    });                    
        } else {
            $query->select(DB::raw($db_raw.", cat_areas.nombre as area_responsable"))
                    ->join('programas','programas.id','=','programas_solicitudes.programa_id')
                    ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
                    ->join('status_solicitudes as ss', 'ss.programas_solicitud_id', '=', 'programas_solicitudes.id')
                    ->join('cat_statusprocesos', 'cat_statusprocesos.id', '=', 'ss.statusproceso_id' )
                    ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','=','cat_tiposremitentes.id')
                    ->leftjoin('programas_responsables','programas_responsables.programa_id','programas.id')
                    ->leftjoin('areas_responsables','areas_responsables.id', 'programas_responsables.areas_responsable_id')
                    ->leftjoin('cat_areas','cat_areas.id', 'areas_responsables.area_id')
                    ->whereRaw('ss.created_at = (select max(sts.created_at) from status_solicitudes as sts where ss.programas_solicitud_id = sts.programas_solicitud_id and sts.deleted_at is null)');
            //Solo para AtnCiudadana puede ver las peticiones de todos
            if($programa_padre !== 'TODOS'){
                $query->whereRaw('(select nombre from programas as programas_padres where programas.padre_id = programas_padres.id ) = "'. $programa_padre .'"');
            }
        }
    }

    public function scopeSearch($query, $programa_padre) {
        $solicitudes = ProgramasSolicitud::
                    select(DB::raw("programas_solicitudes.id AS programa_solicitud_id, programas_solicitudes.cantidad as cantidad, ".
                        "solicitudes.id AS solicitud_id, programas.id AS programa_id, solicitudes.folio AS folio,".
                        "CASE ".
	                        "WHEN solicitudes.tiposremitente_id = 4 THEN ".
                                "( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) ".
                                    "FROM solicitudes_personales sp, personas p ".
                                        "WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 3 THEN ".
                                "( SELECT d.nombre ".
                                    "FROM solicitudes_dependencias sd, cat_instituciones d ".
                                        "WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) ".
                            "WHEN solicitudes.tiposremitente_id = 2 THEN ".
                                "( SELECT m.nombre ".
                                    "FROM solicitudes_municipios sm, cat_municipios m ".
                                        "WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) ".
		                    "WHEN solicitudes.tiposremitente_id = 1 THEN ".
                                "( SELECT r.nombre ".
                                    "FROM solicitudes_regiones sr, cat_regiones r ".
                                        "WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) ".
                        "END AS remitente, ".
                        "cat_tiposremitentes.tipo as tipo, ".
                        "programas.nombre AS apoyo, cat_statusprocesos.status AS estatus, ".
                        "CASE WHEN IFNULL((SELECT count(*) from cat_statusprocesos where programas_solicitud_id = programas_solicitudes.id AND statusproceso_id >= 6 ), -1) = 0 ".
                        "THEN (SELECT DATEDIFF(CURDATE(), (SELECT created_at FROM status_solicitudes WHERE programas_solicitud_id = programas_solicitudes.id AND statusproceso_id = 3))) ".
                        "ELSE (SELECT DATEDIFF((SELECT created_at FROM status_solicitudes WHERE	programas_solicitud_id = programas_solicitudes.id AND statusproceso_id >= 6 AND status_solicitudes.deleted_at is null), ".
                        "(SELECT created_at FROM status_solicitudes WHERE programas_solicitud_id = programas_solicitudes.id	AND statusproceso_id = 3))) END as dias_transcurridos, ".
                        "(select created_at from status_solicitudes where programas_solicitud_id = programas_solicitudes.id and statusproceso_id = 3) as fecha"))
                    ->join('programas','programas.id','=','programas_solicitudes.programa_id')
                    ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
                    ->join('status_solicitudes as ss', 'ss.programas_solicitud_id', '=', 'programas_solicitudes.id')
                    ->join('cat_statusprocesos', 'cat_statusprocesos.id', '=', 'ss.statusproceso_id' )
                    ->join('cat_tiposremitentes','solicitudes.tiposremitente_id','=','cat_tiposremitentes.id')
                    ->whereRaw('ss.created_at = (select max(sts.created_at) from status_solicitudes as sts where ss.programas_solicitud_id = sts.programas_solicitud_id and sts.deleted_at is null)');

            if($programa_padre !== 'TODOS'){
                $solicitudes = $solicitudes->whereRaw('(select nombre from programas as programas_padres where programas.padre_id = programas_padres.id ) = "'. $programa_padre .'"');
            }                    

            $query = DB::table(DB::raw("({$solicitudes->toSql()}) as x"))
                        ->select(['programa_solicitud_id', 'solicitud_id', 'programa_id', 'apoyo', 'folio', 'remitente', 'tipo', 'estatus', 'fecha', 'cantidad', 'dias_transcurridos']);
            if($programa_padre !== 'TODOS'){
                $query = $query->where(function ($query) {
                    $query->where('estatus', '!=', 'PENDIENTE')
                        ->where('estatus', '!=', 'SOLICITANTE')
                        ->where('estatus', '!=', 'CANCELADO');
                });
            }
    
        return $query;
    }

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function programa() {
    	return $this->belongsTo('App\Models\Programa');
    }

    public function solicitud() {
    	return $this->belongsTo('App\Models\Solicitud');
    }

    public function solicitudespersonas() {
        return $this->hasMany('App\Models\SolicitudesPersona');
    }

    public function statussolicitudes() {
        return $this->hasMany('App\Models\StatusSolicitud');
    }

    public function statusActual() {
        return $this->statussolicitudes->last()->statusproceso->status;
    }

    public function getAnio(){
        return (\DateTime::createFromFormat('Y-m-d H:i:s', $this->created_at))->format('Y');
    }
}
