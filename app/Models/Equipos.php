<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Equipos extends Model
{
    protected $table = 'cat_equipos'; //aqui va el nombre de la tabla de base de datos
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","num_serie","num_inventario","direccion_ip","capacidad","marca_id","modelo_id","tipoequipo_id","resguardo_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

public function catmarcasequipos(){
	return $this->belongsTo('App\Models\CatMarcasEquipos', 'marca_id', 'id');
	}

public function catmodelosequipos(){
	return $this->belongsTo('App\Models\CatModelosEquipos', 'modelo_id', 'id');
}

public function cattiposequipos(){
	return $this->belongsTo('App\Models\TiposEquipos', 'tipoequipo_id', 'id');
}
public function solicitudesequipos(){
	    return $this->belongsTo('App\Models\SoporteSolicitudes', 'equipo_id', 'id');
	}
public function empleado(){
	return $this->belongsTo('App\Models\Empleado', 'resguardo_id', 'id');
}

}
