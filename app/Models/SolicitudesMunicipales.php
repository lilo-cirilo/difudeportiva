<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudesMunicipales extends Model
{
    protected $table = 'solicitudes_municipios';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","persona_id","municipio_id", "cargo_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function municipio(){
    	return $this->belongsTo('App\Models\Municipio');
    }

    public function solicitud(){
    	return $this->belongsTo('App\Models\Solicitud');
    }

    public function cargo(){
    	return $this->belongsTo('App\Models\Cargo');
    }
}
