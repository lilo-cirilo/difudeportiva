<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model
{
	protected $connection = 'otra';

	protected $table = 'distritos';

	public function region(){
		return $this->belongsTo('App\Models\Bienestar\Region');
	}
}