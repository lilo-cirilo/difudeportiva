<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
	protected $connection = 'otra';

	protected $table = 'municipios';

	public function localidades()
	{
		return $this->hasMany('App\Models\Bienestar\Localidad');
	}

	public function distrito()
	{
		return $this->belongsTo('App\Models\Bienestar\Distrito');
	}
}