<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
	protected $connection = 'otra';

	protected $table = 'regiones';
}