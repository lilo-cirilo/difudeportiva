<?php

namespace App\Models\Bienestar;

use Illuminate\Database\Eloquent\Model;

class PersonasPrograma extends Model
{
	protected $connection = 'otra';

	protected $table = 'personas_programas';

	public function scopeSearch($query) {
		$query
		->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
		->leftjoin('localidades', 'localidades.id', '=', 'personas.localidad_id')
		->join('municipios', 'municipios.id', '=', 'personas.municipio_id')
		->join('distritos', 'distritos.id', '=', 'municipios.distrito_id')
		->join('regiones', 'regiones.id', '=', 'distritos.region_id')
		//->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
		//->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
		//->join('ejercicios', 'ejercicios.id', '=', 'anios_programas.ejercicio_id')
		//->where('programas.nombre', 'LIKE', $programa)
		//->where('ejercicios.anio', '=', $anio)
		->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
		//->join('discapacidades', 'discapacidades.id', '=', 'bienestarpersonas.discapacidad_id')
		//->whereRaw('personas_programas.anios_programa_id = (select max(pp.anios_programa_id) from personas_programas as pp where personas_programas.persona_id = pp.persona_id)')
		//->orderBy('bienestarpersonas.posicion')
		//->orderBy('personas_programas.deleted_at')
		;
	}
}