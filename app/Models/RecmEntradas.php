<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmEntradas extends Model
{
    use SoftDeletes;
    protected $table = 'recm_entradas';
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $fillable = ['nombre', 'fecha_hora', 'requisicion_id', 'usuario_id', 'folio','estatus_id','orden_compra_id'];

    // public function ordencompra(){
    //     return $this->belongsTo('App\Models\RecmOrdeneCompra','orden_compra_id','id');
    // }

    public function productos(){
        return $this->belongsToMany('App\Models\RecmPresentacionProducto', 'recm_entradas_productos', 'entrada_id', 'presentacion_producto_id')->withPivot('cantidad');
    }

    public function usuario(){
        return $this->belongsTo('App\Models\Usuario', 'usuario_id', 'id');
    }
    
    public function estatus (){
        return $this->belongsTo('App\Models\RecmCatEstatus');
    }

     public function requisicion(){
        return $this->belongsTo('App\Models\RecmRequisicion','requisicion_id','id');
    }
}
