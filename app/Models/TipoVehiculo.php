<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TipoVehiculo extends Model
{
    protected $table = 'cat_tiposvehiculos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}