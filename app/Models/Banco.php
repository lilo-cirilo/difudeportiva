<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banco extends Model
{
	use SoftDeletes;

    protected $table = 'cat_bancos';

    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre'];

    public function cuentasbancos() {
        return $this->hasMany('App\Models\CuentasBancos','banco_id','id');
    }
}