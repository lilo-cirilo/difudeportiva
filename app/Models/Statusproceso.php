<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Statusproceso extends Model
{
    protected $table = "cat_statusprocesos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["status","descripcion"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function statusbienestarpersonas(){
    	return $this->hasMany('App\Models\StatusBienestarpersona');
    }

    public function statussolicitudes(){
    	return $this->hasMany('App\Models\StatusSolicitud');
    }
    public function evento(){
        return $this->hasMany('App\Models\EventosTaller');
    }

}
