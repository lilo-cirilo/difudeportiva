<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreasPrograma extends Model{
  use SoftDeletes;

  //protected $table = 'areas_programas';
  protected $dates = ['deleted_at'];
  protected $fillable=["area_id","programa_id","usuario_id"];
  protected $hidden = array('updated_at', 'deleted_at');

  public function usuario(){//quien dio de alta el registro
    return $this->belongsTo('App\Models\Usuario');
  }
  public function area(){
    return $this->belongsTo('App\Models\Area');
  }
  public function programa(){
    return $this->belongsTo('App\Models\Programa');
  }
}
