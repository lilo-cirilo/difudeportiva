<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entidad extends Model{
  
    protected $table = 'cat_entidades';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["entidad"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function dependencias(){
    	return $this->hasMany('App\Dependencia');
    }
}
