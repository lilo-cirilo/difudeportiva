<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmCatProducto extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['producto', 'usuario_id'];

    public function presentacionProducto(){
        return $this->hasMany('App\Models\RecmPresentacionProducto','producto_id','id');
    }

    public function presentaciones(){
        return $this->belongsToMany('App\Models\RecmCatPresentacion', 'recm_presentacion_productos', 'producto_id', 'presentacion_id');
    }

    public function partida(){
        return $this->belongsToMany('App\Models\RecmCatPartida', 'recm_presentacion_productos', 'producto_id', 'partida_id');
    }
}
