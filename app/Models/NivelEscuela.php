<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NivelEscuela extends Model
{
    protected $table = 'cat_nivelesescuela';
    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['nombre'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}