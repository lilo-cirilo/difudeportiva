<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model
{
	protected $table = 'cat_modulos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","descripcion","usuario_id", 'prefix'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){//quien dio de alta el modulo
    	return $this->belongsTo('App\Models\Usuario');
	}
	
    public function bitacoras(){
        return $this->hasMany('App\Models\Bitacora');
    }

    public function usuarioroles(){
        return $this->hasMany('App\Models\UsuariosRol');
    }

    public function prefixes(){
        return $this->select('id', 'prefix', 'activo')->get();
    }

    public function isActive($modulo) {
        return $this->select('activo')->where('prefix', 'like', $modulo)->get();
    }

    public function roles(){
        return $this->hasMany('App\Models\Rol', 'modulo_id', 'id');
    }
}
