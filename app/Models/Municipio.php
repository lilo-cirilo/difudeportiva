<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    use SoftDeletes;

    protected $table = 'cat_municipios';
    
    protected $dates = ['deleted_at'];

    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'distrito_id', 'latitud', 'longitud'];

    public function personas()
    {
    	return $this->hasMany('App\Models\Persona');
    }

    public function localidades()
    {
    	return $this->hasMany('App\Models\Localidad');
    }

    public function distrito()
    {
    	return $this->belongsTo('App\Models\Distrito');
    }

    public function evento() // para el modulo orgEventos
    {
        return $this->hasMany('App\Models\EventosTaller');
    }
    
    public function inversionprograma()
    {
    	return $this->hasMany('App\Models\InversionPrograma');
    }
    
    public function solicitudMunicipal()
    {
        return $this->belongsTo('App\Models\SolicitudesMunicipales');
    }
}