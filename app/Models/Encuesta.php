<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Encuesta extends Model
{
  protected $table = 'programas_encuestas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['programa_id', 'encuesta'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function programa()
  {
    return $this->belongsTo('App\Models\Programa');
  }

}
