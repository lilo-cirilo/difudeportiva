<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Afuncionalessilla extends Model
{
    protected $table = 'afuncionales_sillas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["afuncionalespersona_id","altura","peso","terreno","tiposilla","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function afuncionalespersona(){
        return $this->belongsTo('App\Afuncionalespersona');
    }

    public function usuario(){
    	return $this->belongsTo('App\Usuario');
    }
}
