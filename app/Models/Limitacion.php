<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Limitacion extends Model
{
    
    protected $table = 'cat_limitaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["limitacion","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function usuario(){
    	return $this->belongsTo('App\Usuario');
    }

    public function afuncionalespersonas(){
    	return $this->hasMany('App\Afuncionalespersona');
    }
}
