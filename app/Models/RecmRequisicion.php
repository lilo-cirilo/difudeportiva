<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmRequisicion extends Model
{
    use SoftDeletes;
    protected $table = 'recm_requisicions';
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    // protected $appends = array('ultimo');

    protected $fillable = ['folio','justificacion','fecha_captura','fecha_plazo','estatus_id', 'observacion','areas_responsable_id'];

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario');
    }
    
    public function productos (){
        return $this->belongsToMany('App\Models\RecmPresentacionProducto','recm_producto_requisicions','requisicion_id','presentacion_producto_id')->withPivot('cantidad','precio','status_id','id','observacion')
        ->selectRaw('recm_presentacion_productos.*, sum(recm_producto_requisicions.cantidad) as cantidadT, sum(recm_producto_requisicions.precio) as precioT, recm_producto_requisicions.status_id')
        // ->withTimestamps()
        ->groupBy('recm_producto_requisicions.status_id','recm_presentacion_productos.id','recm_presentacion_productos.precio','recm_presentacion_productos.presentacion_id','recm_presentacion_productos.producto_id','recm_presentacion_productos.partida_id','recm_presentacion_productos.deleted_at','recm_presentacion_productos.created_at','recm_presentacion_productos.updated_at','recm_producto_requisicions.requisicion_id','recm_producto_requisicions.presentacion_producto_id','recm_producto_requisicions.cantidad','recm_producto_requisicions.precio','recm_producto_requisicions.id','recm_producto_requisicions.observacion');
    }

    public function estatusList(){ // historial de estatus de la requisicion
        return $this->belongsToMany('App\Models\RecmCatEstatus','recm_historial_requisicions','requisicion_id','status_id');
    }

    public function estatus(){ // ultimo estatus
        return $this->belongsTo('App\Models\RecmCatEstatus','estatus_id');
    }

    public function ordencompra(){ // lista de ordenes de compra
        return $this->hasMany('App\Models\RecmOrdeneCompra','requisicion_id', 'id');
    }

    public function productos2(){
        return $this->hasMany('App\Models\RecmProductoRequisicion', 'requisicion_id', 'id');
    }

    public function partida(){ 
        return $this->belongsTo('App\Models\RecmCatPartida');
    }

    public function tipo(){ // tipo de requisicion
        return $this->belongsTo('App\Models\RecmCatTipos','tipo');
    }
    public function area(){
        return $this->belongsTo('App\Models\Area');
    }
    
}
