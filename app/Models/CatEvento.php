<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatEvento extends Model
{
    use SoftDeletes;
    protected $table = 'orev_cat_eventos';
    protected $dates = ['deleted_at'];

    protected $fillable = ['descripcion'];

     public function eventos(){
    	return $this->hasMany('App\Models\EventosTaller');
    }
}
