<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiciosAdquiridos extends Model{
  use SoftDeletes;
  
  // protected $connection = 'mysql_server_real';

  protected $table = 'dtll_serviciosadquiridos';
  protected $dates = ['deleted_at'];
  protected $fillable=["tiposervicio_id","tipopasajero_id","numPasajero","pasajero_id","usuario_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
	public function pasajero(){
    return $this->belongsTo('App\Models\DTLL\Pasajero');
  }
  public function tipopasajero(){
      return $this->belongsTo('App\Models\DTLL\TipoPasajero');
  }
  public function tiposervicio(){
      return $this->belongsTo('App\Models\DTLL\TipoServicio');
  }
  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }
  public function pagos()
  {
      return $this->hasMany('App\Models\DTLL\Pagos','servicioadquirido_id','id');
  }
}