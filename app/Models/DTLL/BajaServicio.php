<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BajaServicio extends Model
{
    use SoftDeletes;

    // protected $connection = 'mysql_server_real';

    protected $table = 'dtll_serviciosbajadas';
    protected $dates = ['deleted_at'];
    protected $fillable = ["servicio_id", "bajada", "latitud", "longitud"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function servicio()
    {
        return $this->belongsTo('App\Models\DTLL\Servicio');
    }
}