<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LugarPago extends Model{
  use SoftDeletes;
  
  protected $table = 'dtll_cat_lugarespago';
  protected $dates = ['deleted_at'];
  protected $fillable=["nombre"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  public function pagos(){
    return $this->hasMany('App\Models\DTLL\Pagos');
	}
  
}