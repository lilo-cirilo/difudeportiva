<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pasajero extends Model{
  use SoftDeletes;
  
  // protected $connection = 'mysql_server_real';
  
  protected $table = 'dtll_pasajeros';
  protected $dates = ['deleted_at'];
  protected $fillable=["persona_id","persona_emergencias","telefono_emergencias","localidad","usuario_id","fechaInsert","pais_id","entidad_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
	public function usuario(){
    return $this->BelongsTo('App\Models\Usuario');
  }
  public function persona()
  {
    return $this->belongsTo('App\Models\Persona');
  }
  public function pais()
  {
      return $this->belongsTo('App\Models\Pais');
  }
  public function entidad()
  {
      return $this->belongsTo('App\Models\Entidad');
  }
  public function serviciosadquiridos()
  {
      return $this->hasMany('App\Models\DTLL\ServiciosAdquiridos');
  }
  public function servicios()
  {
      return $this->hasMany('App\Models\DTLL\Servicios');
  }
}