<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroGasolina extends Model
{
    use SoftDeletes;

    protected $table = 'dtll_registrogasolina';
    protected $dates = ['deleted_at'];
    protected $fillable = ["controlruta_id", "tipocarga_id", "litros", "importe", "folio", "fecha", "foto"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function controlruta()
    {
        return $this->belongsTo('App\Models\ControlRutas', 'controlruta_id');
    }
}