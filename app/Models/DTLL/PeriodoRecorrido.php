<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeriodoRecorrido extends Model{
  use SoftDeletes;
  
  protected $table = 'dtll_periodosrecorridos';
  protected $dates = ['deleted_at'];
  protected $fillable=["recorrido_id","fecha_ini","fecha_fin","usuario_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
	public function recorrido(){
    return $this->belongsTo('App\Models\Recorrido');
  }
  
	public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
  }
  
}