<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistroKm extends Model
{
    use SoftDeletes;

    // protected $connection = 'mysql_server_real';

    protected $table = 'dtll_registrokm';
    protected $dates = ['deleted_at'];
    protected $fillable = ["controlruta_id", "km", "fecha", "foto"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function controlruta()
    {
        return $this->belongsTo('App\Models\ControlRutas', 'controlruta_id');
    }
}