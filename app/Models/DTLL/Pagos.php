<?php

namespace App\Models\DTLL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pagos extends Model{
  use SoftDeletes;
  
  // protected $connection = 'mysql_server_real';

  protected $table = 'dtll_pagos';
  protected $dates = ['deleted_at'];
  protected $fillable=["servicioadquirido_id","fechaIniServ","fechaFinServ","numMesesServ","folioPago","lugarPago_id","usuario_id"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
	public function servicioadquirido(){
    return $this->belongsTo('App\Models\DTLL\ServiciosAdquiridos','servicioadquirido_id','id');
  } 

  public function usuario()
  {
      return $this->belongsTo('App\Models\Usuario');
  }

  public function lugarPago(){
    return $this->belongsTo('App\Models\DTLL\LugarPago');
  }
}