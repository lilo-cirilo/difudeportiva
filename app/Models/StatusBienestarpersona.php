<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusBienestarPersona extends Model
{
    use SoftDeletes;

    protected $table = 'status_bienestarpersonas';
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['bienestarpersona_id', 'statusproceso_id', 'motivoprograma_id', 'observacion', 'usuario_id'];
    
    public function bienestarpersona() {
    	return $this->belongsTo('App\Models\BienestarPersona');
    }

    public function statusproceso() {
    	return $this->belongsTo('App\Models\StatusProceso');
    }

    public function motivo() {
    	return $this->belongsTo('App\Models\Motivo');
    }

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
    }
}