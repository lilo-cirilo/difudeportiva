<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmHistorialOrdenCompra extends Model
{
    use SoftDeletes;
    protected $table = 'recm_historial_orden_compras';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    // protected $fillable = [];
}
