<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Programa extends Model
{
  use SoftDeletes;
  
  protected $table = 'programas';
  
  protected $dates = ['deleted_at'];
  
  protected $fillable = ['nombre', 'activo', 'descripcion', 'tipo', 'codigo', 'fecha_inicio', 'fecha_fin', 'oficial', 'usuario_id', 'padre_id', 'puede_agregar_benef'];
  
  public function scopeSearch($query, $programa_padre, $area_id) {
    if($programa_padre != '') {
      $query->whereRaw('(select nombre from programas as programas_padres where programas.padre_id = programas_padres.id ) = "'. $programa_padre .'"')
      ->whereRaw('programas.id not in (select padre_id from programas where padre_id is not null)')
      ->orderBy('programas.padre_id','desc');
    }else {
      $areas = array_map( function($o) { return $o->id_hijo; }, DB::select('call getAreasHijas(?)',[$area_id]));                        
      $areas[$area_id] = $area_id;
      $query->join('programas_responsables','programas_responsables.programa_id','programas.id')
      ->join('areas_responsables','areas_responsables.id','programas_responsables.areas_responsable_id')            
      ->whereIn('areas_responsables.area_id', $areas)
      ->whereRaw('programas.id not in (select padre_id from programas where padre_id is not null)')
      ->whereNotNull('programas.padre_id')
      ->orderBy('programas.padre_id','desc');
    }   
	}
  
  public function usuario() {
    return $this->belongsTo('App\Models\Usuario');
  }
  
  public function aniosprogramas() {
    return $this->hasMany('App\Models\AniosPrograma');
  }
  
  public function documentosprogramas() {
    return $this->hasMany('App\Models\DocumentosPrograma');
  }
  
  public function personasprogramas() {
    return $this->hasMany('App\Models\PersonasPrograma');
  }
  
  public function areasproductos() {
    return $this->hasMany('App\Models\AreasProducto');
  }
  
  public function programasresponsable() {
    return $this->hasOne('App\Models\ProgramasResponsable');
  }
  
  public function programassolicitudes() {
    return $this->hasMany('App\Models\ProgramasSolicitud');
  }
  
  public function padre() {
    return $this->hasOne('App\Models\Programa', 'id', 'padre_id');
  }
  
  public function lastPadre()
  {
    return $this->padre()->with('padre.AreasPrograma.area.areasresponsables.empleado.persona');
  }

  public function hijos() {
    return $this->hasMany('App\Models\Programa','padre_id','id');
  }

  public function programaproductos() {
    return $this->hasMany('App\Models\ProgramaProducto');
  }
  
  public function get_formato_fecha_inicio() {
    if($this->fecha_inicio) { 
      return (\DateTime::createFromFormat('Y-m-d', $this->fecha_inicio))->format('d-m-Y');
    }
    return '';        
  }
  
  public function get_formato_fecha_fin() {
    if($this->fecha_fin) {
      return (\DateTime::createFromFormat('Y-m-d', $this->fecha_fin))->format('d-m-Y');
    }
    return '';
  }
  
  public function estadoprogramas(){
    return $this->hasMany('App\Models\EstadoPrograma');
  }

  public function subprogramas(){
    return $this->hasMany('App\Models\Programa','codigo','id');
  }

  public function areasprograma(){
    return $this->belongsTo('App\Models\AreasPrograma','id','programa_id');
  }

  public function tiposAccion(){
      return $this->hasMany('\Modules\AsistenciaAlimentaria\Entities\TiposAccion');
  }
}