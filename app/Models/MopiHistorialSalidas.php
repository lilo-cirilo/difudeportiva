<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiHistorialSalidas extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_historial_salidas';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    // protected $fillable = [''];
}
