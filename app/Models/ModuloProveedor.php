<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModuloProveedor extends Model {
  use SoftDeletes;
  
  protected $table = 'modulos_proveedores';
  protected $dates = ['deleted_at'];
  protected $fillable=['modulo_id','proveedor_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function proveedor() {
    return $this->belongsTo('App\Models\Proveedor');
  }

  public function recmproveedor() {
    return $this->belongsTo('App\Models\RecmCatProveedor','proveedor_id','id');
  }

  public function licitacion () {//alimentarios
      return $this->hasMany('Modules\AsistenciaAlimentaria\Entities\Licitacion','modulo_proveedor_id','id');
  }
}