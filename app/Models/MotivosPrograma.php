<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MotivosPrograma extends Model {
    
    protected $table = 'motivos_programas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["motivo_id","programa_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function motivo(){
    	return $this->belongsTo('App\Models\Motivo');
    }

    public function programa(){
    	return $this->belongsTo('App\Models\Programa');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }
}