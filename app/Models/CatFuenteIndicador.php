<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatFuenteIndicador extends Model
{
    use SoftDeletes;
    protected $table = 'cat_fuentesindicadores';
    protected $dates = ['deleted_at'];
    protected $fillable = ['nombre'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function indicadores ()
    {
        return $this->hasMany('App\Models\Indicador','fuente_id');
    }
}
