<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Solicitudmodifdato extends Model
{
    use SoftDeletes;

    protected $table = 'solicitudesmodifdatos';
    
    protected $dates = ['deleted_at'];

    protected $fillable=["persona_id","motivo","fecha_solicitud","ruta","status_solicitud_id"];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
