<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class PeticionesPersonas extends Model {
    protected $table = 'beneficiosprogsol_benefpersonas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["beneficiosprogramas_solicitud_id","beneficiopersona_id","entregado","observacion_entrega","evaluado","observacion_evaluado","usuario_id","salidas_producto_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function beneficiopersona(){
    	return $this->belongsTo('App\Models\BeneficiosPersonas','beneficiopersona_id','id');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function beneficioprograma_solicitud(){
    	return $this->belongsTo('App\Models\BeneficiosprogramasSolicitud','beneficiosprogramas_solicitud_id','id');
    }

    public function get_fecha_updated() {
        return (\DateTime::createFromFormat('Y-m-d H:i:s', $this->updated_at))->format('d-m-Y');
    }

    public function scopeSolicitantes($query, $programas_solicitud_id) {
        $query->join('personas', 'personas.id', '=', 'solicitudes_personas.persona_id')
            ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
            ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
            ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
            ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
            ->join('programas_solicitudes', 'programas_solicitudes.id', '=', 'solicitudes_personas.programas_solicitud_id')
            ->join('solicitudes', 'solicitudes.id', '=', 'programas_solicitudes.solicitud_id')
            ->join('programas', 'programas.id', '=', 'programas_solicitudes.programa_id');
        if($programas_solicitud_id){
            $query->where('solicitudes_personas.programas_solicitud_id',$programas_solicitud_id);
        }
    }
}