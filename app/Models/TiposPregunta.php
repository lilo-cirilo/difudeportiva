<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TiposPregunta extends Model
{
  protected $table = 'cat_tipospreguntas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['tipo'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}
