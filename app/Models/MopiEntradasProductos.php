<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiEntradasProductos extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_entradas_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['cantidad','precio','subtotal','fecha_recepcion'];

    protected $with = ['detalle'];

    public function detalle(){ 
        return $this->belongsTo('App\Models\MopiPresentacionProducto','presentacion_producto_id');
    }

}
