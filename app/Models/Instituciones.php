<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instituciones extends Model
{
    use SoftDeletes;
    protected $table = 'cat_instituciones';
    protected $fillable = ['nombre'];
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
}
