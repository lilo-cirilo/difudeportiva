<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeGrupoNiveles extends Model
{
	protected $table = 'gruposniveles';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","descripcion","anios_programa_id", 'padre_id'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function padre(){
      return $this->hasOne('App\Models\vale\ValeGrupoNiveles', 'id', 'padre_id');
    }

    public function hijos(){
      return $this->hasMany('App\Models\vale\ValeGrupoNiveles', 'padre_id', 'id');
    }
}
