<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeAsistencia extends Model
{
	protected $table = 'vale_asistencias';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["grupos_alumno_id","fecha_inicio","fecha_fin","asistencias","faltas"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
