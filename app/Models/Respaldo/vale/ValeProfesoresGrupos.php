<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeProfesoresGrupos extends Model
{
	protected $table = 'vale_profesoresgrupos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["grupodisponible_id","empleado_id","activo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
