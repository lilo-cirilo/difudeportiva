<?php

namespace App\Models\vale;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ValeGruposdias extends Model
{
	protected $table = 'vale_gruposdias';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["grupodisponible_id","dia"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
