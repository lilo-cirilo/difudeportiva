<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AreasResponsable extends Model
{
    protected $table = 'areas_responsables';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["area_id","empleado_id","fecha_inicio","fecha_fin","activo","usuario_id"];
    protected $hidden = array('created_at', 'updated_at');

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
	}

	public function empleado(){
    	return $this->belongsTo('App\Models\Empleado');
	}

	public function area(){
    	return $this->belongsTo('App\Models\Area');
	}

    public function programasresponsable(){
        return $this->belongsTo('App\Models\ProgramasResponsable');
    }
}
