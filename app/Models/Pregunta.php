<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pregunta extends Model
{
  protected $table = 'preguntas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['encuesta_id', 'tipospregunta_id', 'pregunta'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function encuesta()
  {
    return $this->belongsTo('App\Models\Encuesta');
  }

  public function tipospregunta()
  {
    return $this->belongsTo('App\Models\TiposPregunta');
  }
}
