<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParadasRutas extends Model
{
    protected $table = 'paradas_rutas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["ruta_id","parada_id","tipo","numero_estacion","tipoparada"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}