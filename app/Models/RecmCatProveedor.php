<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmCatProveedor extends Model
{
    use SoftDeletes;
    protected $table = 'recm_cat_proveedors';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['nombre', 'rfc', 'calle', 'numero', 'colonia', 'numero_cuenta', 'clabe', 'no_inscripcion', 'banco', 'correo', 'telefono', 'codigopostal', 'ciudad', 'usuario_id'];

    public function entradas()
    {
        return $this->hasMany('App\Models\RecmEntradas','id','proveedor_id');
    }
    public function moduloProveedor(){//para la licitacion de alimentarios o algo asi
        return $this->hasOne('App\Models\ModuloProveedor','proveedor_id','id');
    }
}
