<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EscuelaProgramaPersonaBasica extends Model
{
  use SoftDeletes;

  protected $table = 'escuelaprograma_personasbasicas';
  protected $dates = ['deleted_at'];
  protected $fillable = ['escuela_programa_id', 'personabasica_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  public function escuelaprograma(){
    return $this->belongsTo('App\Models\EscuelaPrograma');
  }

  public function personabasica()
  {
    return $this->belongsTo('App\Models\PersonaBasica');
  }
}
