<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model
{
    protected $fillable = [
    	'id_oficio',
    	'id_estatusseguimiento',
    	'observaciones'
    ];
}
