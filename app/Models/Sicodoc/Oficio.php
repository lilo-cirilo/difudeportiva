<?php

namespace App\Models\Sicodoc;

use Illuminate\Database\Eloquent\Model;

class Oficio extends Model
{
    protected $fillable = [
    	'folio',
    	'fecha',
    	'id_tipooficio',
    	'interno',
		'nombre_destinatario',
		'puesto_destinatario',
		'asunto',
		'nombre_solicitante',
		'id_areasolicitante',
		'fecha_acuse',
		'created_at',
		'updated_at'
    ];
}
