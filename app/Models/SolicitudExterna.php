<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SolicitudExterna extends Model {
    protected $table = 'atnc_solicitudesexternas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];    
    protected $fillable=["solicitud_id","dependencia_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function dependencia(){
    	return $this->belongsTo('App\Models\Dependencia');
    }

    public function programas_solicitud(){
    	return $this->belongsTo('App\Models\ProgramasSolicitud');
    }
}
