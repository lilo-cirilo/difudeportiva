<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalidasProducto extends Model
{
    protected $table = "salidas_productos";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["fechasalida","tipossalida_id","recibo","empleado_id","persona_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeSearch($query, $programa_padre) {
        // $query->join('cat_tipossalidas','cat_tipossalidas.id','salidas_productos.tipossalida_id')
        // ->leftJoin('personas','personas.id','salidas_productos.persona_id')
        // ->join('detallessalidas_productos','detallessalidas_productos.salidas_producto_id','salidas_productos.id')
        // ->join('areas_productos','areas_productos.id','detallessalidas_productos.areas_producto_id')
        // ->join('programas','programas.id','areas_productos.programa_id')
        // ->whereRaw('(select nombre from programas as programas_padres where programas_padres.id = programas.padre_id ) = "'. $programa_padre .'"')
        // ->where('cat_tipossalidas.tipo','INTERNO');
        $query->join('cat_tipossalidas','cat_tipossalidas.id','salidas_productos.tipossalida_id')
        ->leftJoin('personas','personas.id','salidas_productos.persona_id')
        ->join('detallessalidas_productos','detallessalidas_productos.salidas_producto_id','salidas_productos.id')
        ->join('beneficiosprogramas','beneficiosprogramas.id','detallessalidas_productos.beneficioprograma_id')
        ->join('anios_programas','anios_programas.id','beneficiosprogramas.anio_programa_id')
        ->join('programas','programas.id','anios_programas.programa_id')
        ->join('programas as padre','padre.id','programas.padre_id')
        ->join('areas_programas','areas_programas.programa_id','padre.id')
        ->join('cat_areas','cat_areas.id','areas_programas.area_id')
        ->whereIn('padre.nombre',$programa_padre)
        ->where('cat_tipossalidas.tipo','INTERNO');
    }

    public function areasproducto(){
    	return $this->belongsTo('App\Models\AreasProducto','areas_producto_id');
    }

    public function detallesalidasproductos(){
    	return $this->hasMany('App\Models\DetalleSalidasproductos');
    }

    public function empleado(){
    	return $this->belongsTo('App\Models\Empleado');
    }

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function tipossalida(){
    	return $this->belongsTo('App\Models\Tipossalida');
    }

    public function usuario(){
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function get_formato_fecha() {
        return (\DateTime::createFromFormat('Y-m-d H:i:s', $this->fechasalida))->format('d-m-Y');
    }

    public function get_url_comprobante() {
        if(isset($this->recibo)) {
            return $this->recibo;
        }
        return 'images/no-image.png';
    }
}
