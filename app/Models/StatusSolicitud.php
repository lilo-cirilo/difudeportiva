<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class StatusSolicitud extends Model
{
  protected $table = "status_solicitudes";
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable=["programas_solicitud_id","statusproceso_id","motivoprograma_id","observacion","usuario_id"];
  protected $hidden = array('updated_at', 'deleted_at');
  
  public function programassolicitud(){
    return $this->belongsTo('App\Models\ProgramasSolicitud');
  }
  
  public function statusproceso(){
    return $this->belongsTo('App\Models\Statusproceso');
  }
  
  public function motivoprograma(){
    return $this->belongsTo('App\Models\MotivosPrograma','motivoprograma_id');
  }
  
  public function usuario(){
    return $this->belongsTo('App\Models\Usuario');
  }
  
  public function getCreatedAtAttribute($date){
    return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y');
  }
  public function scopeTimeLine($query,$solicitud_id,$padre_id) {
    $query->select(
      DB::raw(
        'status, p.nombre as beneficio, ps.id as peticion_id, '.
        "UPPER(observacion) as observacion, ".
        "DATE_FORMAT(status_solicitudes.created_at, '%d-%m-%Y %r') AS fecha, ".
        "pe.nombre as usuario "
      )
    )
    ->join('cat_statusprocesos as csp','csp.id','status_solicitudes.statusproceso_id')
    ->join('programas_solicitudes as ps','ps.id','status_solicitudes.programas_solicitud_id')
    ->join('programas as p','p.id','ps.programa_id')
    ->join('usuarios as u','u.id','status_solicitudes.usuario_id')
    ->join('personas as pe','pe.id','u.persona_id')
    ->where([
      ['ps.solicitud_id',$solicitud_id],
      ['padre_id',$padre_id]
    ]);
    
    return $query;
  }
  public function scopeTimeLinePeticion($query,$peticion_id) {
    $query->select(
      DB::raw(
        "status, ".
        "UPPER(observacion) as observacion, ".
        "DATE_FORMAT(status_solicitudes.created_at, '%d-%m-%Y %r') AS fecha, ".
        "pe.nombre as usuario "
      )
    )
    ->join('cat_statusprocesos as csp','csp.id','status_solicitudes.statusproceso_id')
    ->join('programas_solicitudes as ps','ps.id','status_solicitudes.programas_solicitud_id')
    ->join('programas as p','p.id','ps.programa_id')
    ->join('usuarios as u','u.id','status_solicitudes.usuario_id')
    ->join('personas as pe','pe.id','u.persona_id')
    ->where('ps.id',$peticion_id);
    
    return $query;
  }
}