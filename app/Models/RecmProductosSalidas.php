<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmProductosSalidas extends Model
{
    use SoftDeletes;
    protected $table = 'recm_productos_salidas';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['salida_id', 'presentacion_producto_id', 'cantidad'];

    public function salida(){
        $this->belongsTo('App\Models\RecmSalidas', 'id', 'salida_id');
    }
}
