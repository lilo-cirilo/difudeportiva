<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class MopiSolicitudes extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_solicitudes';
    protected $dates = ['deleted_at'];
    protected $hidden = array( 'updated_at', 'deleted_at');

    protected $fillable = ['folio','total','fecha_entrega','justificacion','created_at'];

    public function estatus(){ // Estatus actual
        return $this->belongsTo('App\Models\MopiCatEstatus');
    }
    public function area(){ 
        return $this->belongsTo('App\Models\Area');
    }
    public function productos (){
        return $this->belongsToMany('App\Models\MopiPresentacionProducto','mopi_solicitudes_productos','solicitud_id','presentacion_producto_id')->withPivot('cantidad','precio_unitario','precio_total','descripcion','estatus_id')->where('mopi_solicitudes_productos.deleted_at', null);
    }
    public function usuario(){ 
        return $this->belongsTo('App\Models\Usuario');
    }
    public function productosL (){ // revisar no los agrupa
        return $this->belongsToMany('App\Models\MopiPresentacionProducto','mopi_solicitudes_productos','solicitud_id','presentacion_producto_id')
        ->withPivot('cantidad','precio_unitario','precio_total','descripcion','estatus_id')
        // ->selectRaw('mopi_presentacion_productos.*, sum(mopi_solicitudes_productos.cantidad) as cantidadT')
        ->selectRaw('mopi_solicitudes_productos.descripcion , mopi_solicitudes_productos.presentacion_producto_id , mopi_solicitudes_productos.estatus_id , mopi_presentacion_productos.producto_id , mopi_presentacion_productos.presentacion_id , SUM(mopi_solicitudes_productos.cantidad) as cantidaT,SUM(mopi_solicitudes_productos.precio_total) as precioT')
        ->groupBy(
            'mopi_solicitudes_productos.descripcion',
            'mopi_solicitudes_productos.presentacion_producto_id',
            'mopi_solicitudes_productos.estatus_id',
            'mopi_presentacion_productos.producto_id',
            'mopi_presentacion_productos.presentacion_id',
            'mopi_solicitudes_productos.solicitud_id',
            'mopi_solicitudes_productos.cantidad',
            'mopi_solicitudes_productos.precio_unitario',
            'mopi_solicitudes_productos.precio_total'
             
           );

        // return DB::table('mopi_solicitudes_productos as msp')
        // ->select(['msp.descripcion','msp.presentacion_producto_id','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion',DB::raw("SUM(cantidad) as cantidaT,SUM(precio_total) as precioT")])
        // ->where('solicitud_id', $this->id)
        // ->join('mopi_presentacion_productos as mpp', 'mpp.id', '=', 'msp.presentacion_producto_id')
        // ->join('mopi_cat_productos as mcp', 'mcp.id', '=', 'mpp.producto_id')
        // ->join('mopi_cat_presentaciones as mcpre', 'mcpre.id', '=', 'mpp.presentacion_id')
        // ->groupBy('presentacion_producto_id','msp.descripcion','msp.estatus_id','mpp.producto_id','mpp.presentacion_id','mcp.nombre','mcpre.presentacion')
        // ->get();
    }
    
}
