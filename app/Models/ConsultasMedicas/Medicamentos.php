<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicamentos extends Model{
  
  use SoftDeletes;

  protected $table = 'cat_medicamentos';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre_generico', 'nombre_comercial','forma_farmaceutica','concentracion','indicacion_principal'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return Diagnostico[Model] Colección de instancias de los registros en diagnosticos donde aparece este registro
   */
  public function diagnosticos(){
    return $this->hasMany('App\Models\ConsultasMedicas\Diagnosticos');
  }
}
