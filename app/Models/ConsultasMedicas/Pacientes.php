<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pacientes extends Model{
  
  use SoftDeletes;
  
  protected $table = 'pacientes';
  protected $dates = ['deleted_at'];
  protected $fillable = ['id','persona_id','seguro_id','folioseguro','indigena','pais_id','entidad_id','usuario_id','rfc'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Persona[Model] Instancia del registro en personas al que hace referencia este registro
  */
  public function persona(){
    return $this->belongsTo('App\Models\Persona');
  }
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Seguros[Model] Instancia del registro en cat_seguros al que hace referencia este registro
  */
  public function seguro(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Seguros');
  }
  
  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return Consulta[Model] Colección de instancias de los registros de consulta donde aparece este registro
   */
  public function consultas(){
    return $this->hasMany('App\Models\ConsultasMedicas\Consulta','paciente_id');
  }

}
