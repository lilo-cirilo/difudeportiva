<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Receta extends Model{
  
  protected $table = 'recetas';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id', 'medicamento_id', 'dosis','intervalo','duracion'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro en consultas al que hace referencia este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
  
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Medicamento[Model] Instancia del registro en medicamentos al que hace referencia este registro
  */
  public function medicamento(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Medicamentos');
  }
  
}
