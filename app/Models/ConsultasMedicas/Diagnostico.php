<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Diagnostico extends Model{

  use SoftDeletes;

  protected $table = 'diagnosticos';
  protected $dates = ['deleted_at'];
  protected $fillable = ['consulta_id', 'primeravezpadecimiento', 'primeravezanual', 'diagnostico', 'cie10_id', 'discapacidad_id', 'suive_id', 'clasificacion_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Consulta[Model] Instancia del registro en Consulta al que hace referencia este registro
  */
  public function consulta(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Consulta');
  }
  /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return Cie10[Model] Instancia del registro en Cie10 al que hace referencia este registro
  */
  public function cie10(){
    return $this->belongsTo('App\Models\ConsultasMedicas\Cie10');
  }
   /**
  * Crea la consulta necesaria para obtener datos a partir de este registro
  *
  * @return ClasificacionEnfermedades[Model] Instancia del registro en cat_clasificaciónenfermedades al que hace referencia este registro
  */
  public function clasificacion(){
    return $this->belongsTo('App\Models\ConsultasMedicas\ClasificacionEnfermedades');
  }
}
