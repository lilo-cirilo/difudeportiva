<?php

namespace App\Models\ConsultasMedicas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClasificacionEnfermedades extends Model{
 
  use SoftDeletes;
 
  protected $table = 'cat_clasificacionenfermedades';
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre'];
  protected $hidden = array('updated_at', 'deleted_at');

  /**
   * Crea la consulta necesaria para obtener datos a partir de este registro
   *
   * @return Diagnostico[Model] Colección de instancias de los registros de consultas donde aparece este registro
   */
  public function diagnosticos(){
    return $this->hasMany('App\Models\ConsultasMedicas\Diagnostico');
  }
  
}
