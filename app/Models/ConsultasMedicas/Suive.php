<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suive extends Model{
  
  protected $table = 'cat_suive';
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $fillable = ['nombre'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
