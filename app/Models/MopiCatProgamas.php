<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiCatProgamas extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_cat_progamas';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['programa','presupuesto','descripcion'];

    public function categorias(){ // las categorias que tiene
        return $this->hasMany('App\Models\MopiCatCategorias','programa_id');
        
    }
}
