<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Motivo extends Model
{
    protected $table = 'cat_motivos';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["motivo","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


    public function usuario(){//quien dio de alta el rol
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function personasprogramas(){
    	return $this->hasMany('App\Models\PersonasPrograma');
    }

    public function statusbienestarpersonas(){
        return $this->hasMany('App\Models\StatusBienestarpersona');
    }

    public function statussolicitudes(){
        return $this->hasMany('App\Models\StatusSolicitud');
    }
}
