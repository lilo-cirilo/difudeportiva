<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discapacidad extends Model
{
    use SoftDeletes;

    protected $table = 'cat_discapacidades';
    
    protected $dates = ['deleted_at'];

    protected $fillable = ['nombre', 'usuario_id'];

    public function usuario() {
    	return $this->belongsTo('App\Models\Usuario');
    }

    public function bienestarpersonas() {
    	return $this->hasMany('App\Models\BienestarPersona');
    }

    public function afuncionalespersonas() {
        return $this->hasMany('App\Models\AfuncionalesPersona');
    }
}