<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class BeneficiosPersonas extends Model {
    protected $table = 'beneficiosprog_personas';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["beneficioprograma_id","persona_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function persona(){
    	return $this->belongsTo('App\Models\Persona');
    }

    public function peticionesPersonas(){
        return $this->hasMany('App\Models\PeticionesPersonas', 'beneficiopersona_id', 'id');
    }
}