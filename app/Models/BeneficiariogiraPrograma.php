<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BeneficiariogiraPrograma extends Model {
    protected $table = "atnc_giraspreregis_programas";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["girapreregis_id","programa_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function scopeBeneficiarios($query, $evento_id) {
        $query->join('giraspreregis', 'giraspreregis.id', '=', 'atnc_giraspreregis_programas.girapreregis_id')
              ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'giraspreregis.localidad_id')
              ->join('cat_municipios', 'cat_municipios.id', '=', 'giraspreregis.municipio_id')
              ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
              ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')                            
              ->join('programas', 'programas.id', '=', 'atnc_giraspreregis_programas.programa_id')
              
              ->leftjoin('productosfoliados','productosfoliados.folio','=','giraspreregis.folio')
              ->leftjoin('detallesentradas_productos','detallesentradas_productos.id','=','productosfoliados.detallesentradas_producto_id')
              ->leftjoin('entradas_productos','entradas_productos.id','=','detallesentradas_productos.entradas_producto_id')
              ->leftjoin('areas_productos','areas_productos.id','=','detallesentradas_productos.areas_producto_id')
              ->leftjoin('cat_productos','cat_productos.id','=','areas_productos.producto_id')
            
              ->whereNull('giraspreregis.deleted_at')
              ->where('giraspreregis.evento_id',$evento_id)
                ->whereNotNull('giraspreregis.folio');
                 
    }

    public function scopeSolicitantes($query, $evento_id) {
        $query->join('giraspreregis', 'giraspreregis.id', '=', 'atnc_giraspreregis_programas.girapreregis_id')
              ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'giraspreregis.localidad_id')
              ->join('cat_municipios', 'cat_municipios.id', '=', 'giraspreregis.municipio_id')
              ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
              ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')                            
              ->join('programas', 'programas.id', '=', 'atnc_giraspreregis_programas.programa_id')
              
              ->leftjoin('productosfoliados','productosfoliados.folio','=','giraspreregis.folio')
              ->leftjoin('detallesentradas_productos','detallesentradas_productos.id','=','productosfoliados.detallesentradas_producto_id')
              ->leftjoin('entradas_productos','entradas_productos.id','=','detallesentradas_productos.entradas_producto_id')
              ->leftjoin('areas_productos','areas_productos.id','=','detallesentradas_productos.areas_producto_id')
              ->leftjoin('cat_productos','cat_productos.id','=','areas_productos.producto_id')
            
              ->whereNull('giraspreregis.deleted_at')
              ->where('giraspreregis.evento_id',$evento_id)
             ->whereNull('giraspreregis.folio');                  
    }

    public function gira(){
    	return $this->belongsTo('App\Models\Gira','giraspreregis_id','id');
    }

    public function programa(){
    	return $this->belongsTo('App\Models\Programa','programa_id','id');
    }
}
