<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pais extends Model{
  
    protected $table = 'cat_paises';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["nombre","codigo"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

}
