<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class SoporteCalificaciones extends Model
{
    protected $table = 'soportecalificaciones';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["solicitud_id","desempenio_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');


public function soportesolicitudes(){
		return $this->hasOne('App\Models\SoporteSolicitudes');
	} 

public function desempenios(){
		return $this->hasOne('App\Models\Desempeños');
	}
}
