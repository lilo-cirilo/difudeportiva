<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CredTipoImpresion extends Model
{
    use SoftDeletes;

    protected $table = 'cred_tipoimpresion';
    
    protected $dates = ['deleted_at'];

    protected $fillable=["nombre"];

    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

     public function impresion(){
    	return $this->hasMany('App\Models\CredImpresion');
    }

}
