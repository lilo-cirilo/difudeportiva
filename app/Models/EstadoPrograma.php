<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoPrograma extends Model{
  use SoftDeletes;

  protected $table = 'estados_programas';
  protected $dates = ['deleted_at'];
  protected $fillable=['programa_id','estado_id','orden','usuario_id'];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
  protected $with = ['estado'];

  public function programa(){
    return $this->belongsTo('App\Models\Programa');
  }
  public function estado(){
    return $this->belongsTo('App\Models\Estado');
  }
}