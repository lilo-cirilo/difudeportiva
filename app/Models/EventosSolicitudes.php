<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EventosSolicitudes extends Model
{
    protected $table = "atnc_eventos_solicitudes";
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable=["evento_id","solicitud_id","usuario_id"];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    public function evento(){
    	return $this->belongsTo('Modules\AgendaEventos\Entities\Evento','evento_id','id');
    }

    public function solicitud(){
    	return $this->belongsTo('App\Models\Entities\Solicitud','solicitud_id','id');
    }
}
