<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\ListaDispersion;

class PersonasPrograma extends Model
{
    use SoftDeletes;

    protected $table = 'personas_programas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['id','persona_id', 'anios_programa_id', 'baja', 'fechabaja', 'motivo_id', 'usuario_id'];
    
    //protected $hidden = array('created_at', 'updated_at', 'deleted_at');
    
    public function beneficiarioalimentarios() {
        return $this->belongsTo('Modules\AsistenciaAlimentaria\Entities\Beneficiario','id','persona_programa_id');
    }

    public function beneficiariopeta() {
        return $this->belongsTo('Modules\DesayunosEscolaresRegistro\Entities\Beneficiario','id','persona_programa_id');
    }

    public function bienestarpersonas() {
        return $this->belongsTo('App\Models\BienestarPersona', 'persona_id', 'persona_id');
    }

    public function persona() {
        return $this->belongsTo('App\Models\Persona');
    }

    public function anios_programa() {
        return $this->belongsTo('App\Models\AniosPrograma');
    }

    public function motivo() {
        return $this->belongsTo('App\Models\Motivo');
    }

    public function usuario() {
        return $this->belongsTo('App\Models\Usuario');
    }

    public function bienestardispersiones() {
        return $this->hasMany('App\Models\BienestarDispersion');
    }

    public function scopeSearch($query, $programa, $anio) {
        /*$query
        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->leftJoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')

        ->join('personas', 'personas.id', '=', 'bienestarpersonas.persona_id')

        ->leftJoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftJoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftJoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftJoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        ->join('personas_programas', 'personas_programas.persona_id', '=', 'personas.id')
        ->where('personas_programas.anios_programa_id', '=', 2)

        ->leftJoin('tutores', 'tutores.tutorado_id', '=', 'personas.id')
        ->whereNull('tutores.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')

        ->leftJoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutor.id')
        ->whereNull('cuentasbancos.deleted_at')
        ;*/
        $query
        ->where('personas_programas.anios_programa_id', '=', 2)

        ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')

        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')

        /*->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
        ->where('programas.nombre', 'LIKE', $programa)
        ->where('cat_ejercicios.anio', '=', $anio)*/

        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->where('bienestarpersonas.ejercicio_id', '=', 8)
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'bienestarpersonas.discapacidad_id')

        ->leftjoin('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->where('tutores.programa_id', '=', 1)
        ->whereNull('tutores.deleted_at')

        //->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        //->whereNull('cuentasbancos.deleted_at')

        ->join('personas as tutor', 'tutor.id', '=', 'tutores.persona_id')

        ->leftJoin('cat_localidades as tutor_cat_localidades', 'tutor_cat_localidades.id', '=', 'tutor.localidad_id')
        ->leftJoin('cat_municipios as tutor_cat_municipios', 'tutor_cat_municipios.id', '=', 'tutor.municipio_id')
        ->leftJoin('cat_distritos as tutor_cat_distritos', 'tutor_cat_distritos.id', '=', 'tutor_cat_municipios.distrito_id')
        ->leftJoin('cat_regiones as tutor_cat_regiones', 'tutor_cat_regiones.id', '=', 'tutor_cat_distritos.region_id')

        ->leftJoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutor.id')
        ->whereNull('cuentasbancos.deleted_at')

        /*->join('tutores', 'tutores.tutorado_id', '=', 'bienestarpersonas.persona_id')
        ->whereNull('tutores.deleted_at')
        ->leftjoin('cuentasbancos', 'cuentasbancos.persona_id', 'tutores.persona_id')
        ->whereNull('cuentasbancos.deleted_at')

        ->join('personas as t', 't.id', '=', 'tutores.persona_id')*/

        //->whereRaw('personas_programas.anios_programa_id = (select max(pp.anios_programa_id) from personas_programas as pp where personas_programas.persona_id = pp.persona_id)')
        //->orderBy('bienestarpersonas.posicion')
        //->orderBy('personas_programas.deleted_at')
        ;
    }

    public function scopeMatriz($query, $programa, $anio) {
        $query
        ->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id')
        ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
        ->where('programas.nombre', 'LIKE', $programa)
        ->where('cat_ejercicios.anio', '=', $anio)
        ->leftjoin('bienestardispersiones', 'personas_programas.id', '=', 'bienestardispersiones.personas_programa_id')
        ->leftjoin('listas_dispersiones', 'listas_dispersiones.id', '=', 'bienestardispersiones.lista_id')
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        //->where('bienestarpersonas.posicion')
        ->whereNotNull('bienestarpersonas.posicion')
        //->orderBy('bienestarpersonas.posicion')
        //->orderBy('personas_programas.deleted_at')
        ->distinct();
    }

    public function scopePosicion($query, $programa, $anio) {
        $query
        ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
        ->where('programas.nombre', 'LIKE', $programa)
        ->where('cat_ejercicios.anio', '=', $anio)
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->whereNotNull('bienestarpersonas.posicion')
        ->groupBy('bienestarpersonas.posicion');
    }

    public function scopePosicionesDisponibles($query, $programa, $anio) {
        $query
        ->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'anios_programas.ejercicio_id')
        ->where('programas.nombre', 'LIKE', $programa)
        ->where('cat_ejercicios.anio', '=', $anio)
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->whereRaw('bienestarpersonas.posicion not in (select posicion from bienestarpersonas bp inner join personas_programas pp on bp.persona_id = pp.persona_id inner join anios_programas ap on ap.id = pp.anios_programa_id inner join cat_ejercicios ce on ce.id = ap.ejercicio_id inner join programas p on p.id = ap.programa_id where posicion is not null and pp.deleted_at is null and p.nombre = ' . "'" . $programa . "'" . ' and ce.anio = ' . $anio . ')')
        ->whereNotNull('personas_programas.deleted_at')
        ->groupBy('bienestarpersonas.posicion');
    }


    public function scopeFiltrarPrograma($query, $programa) {
        $query->join('anios_programas', 'anios_programas.id', '=', 'personas_programas.anios_programa_id')
        ->join('programas', 'programas.id', '=', 'anios_programas.programa_id')
        ->where('programas.nombre', 'LIKE', $programa);
    }

    public function scopeDatosPersonales($query) {
        $query->join('personas', 'personas.id', '=', 'personas_programas.persona_id')
        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')
        ->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id')
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->join('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->join('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->join('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');
    }

    public function scopeGenerarDispersion($query, $anio, $bimestre, $aniosPrograma, $dispersion_id) {
        /*$pagados = ListaDispersion::

        join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->join('personas_programas', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')

        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')

        ->where('listas_dispersiones.ejercicio_id', '=', 8)
        ->where('listas_dispersiones.bimestre', '=', $bimestre)

        ->where(function($query) use($bimestre, $dispersion_id) {
            $query
            ->orWhere(function($query) {
                $query
                ->where('bienestardispersiones.pagado', '=', 1)
                ->where('listas_dispersiones.status', '=', 'FINALIZADA')
                ;
            })
            ->orWhere(function($query) use($dispersion_id) {
                $query
                ->where('bienestardispersiones.lista_id', '!=', $dispersion_id)
                ->where('bienestardispersiones.pagado', '=', 0)
                ;
            })
            ;
        })

        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->select(['bienestarpersonas.posicion'])
        ;*/

        $pagados = ListaDispersion::

        join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->join('personas_programas', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')

        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')

        ->where('listas_dispersiones.ejercicio_id', '=', 8)
        ->where('listas_dispersiones.bimestre', '=', $bimestre)

        ->where(function($query) use($bimestre, $dispersion_id) {
            $query
            ->orWhere(function($query) {
                $query
                ->where('bienestardispersiones.pagado', '=', 1)
                ->where('listas_dispersiones.status', '=', 'FINALIZADA')
                ;
            })
            ->orWhere(function($query) use($dispersion_id) {
                $query
                ->where('bienestardispersiones.lista_id', '!=', $dispersion_id)
                ->where('bienestardispersiones.pagado', '=', 0)
                ;
            })
            ;
        })

        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->select(['bienestarpersonas.posicion'])
        ;

        $pagadoss = ListaDispersion::

        join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')

        ->join('personas_programas', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')

        ->join('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas_programas.persona_id')

        ->where('listas_dispersiones.ejercicio_id', '=', 8)
        ->where('listas_dispersiones.bimestre', '=', $bimestre - 1)

        ->where('bienestardispersiones.pagado', '=', 1)

        ->where('listas_dispersiones.status', '=', 'FINALIZADA')
        
        ->where('bienestarpersonas.ejercicio_id', '=', 8)

        ->select(['bienestarpersonas.posicion'])
        ;//dd($pagadoss->get());
        
        $query

        ->leftjoin('bienestardispersiones', 'bienestardispersiones.personas_programa_id', '=', 'personas_programas.id')

        ->leftjoin('listas_dispersiones', 'listas_dispersiones.id', '=', 'bienestardispersiones.lista_id')

        ->where('anios_programa_id', $aniosPrograma)

        

        ->whereNotIn('bienestarpersonas.posicion', $pagados)

        ->distinct('personas_programas.id')
        ;

        if($bimestre > 1) {
            $query
            ->whereIn('bienestarpersonas.posicion', $pagadoss)
            ;
        }
    }

    public function scopeCopia($query, $anio, $bimestre, $aniosPrograma, $dispersion) {
        //subconsulta para determinar las posiciones pagadas en el bimestres/año
        $posicionesPagadas=PersonasPrograma::withTrashed()
        ->leftjoin('bienestardispersiones','bienestardispersiones.personas_programa_id','=','personas_programas.id')
        ->leftjoin('bienestarpersonas','bienestarpersonas.persona_id','=','personas_programas.persona_id')
        ->leftjoin('listas_dispersiones','bienestardispersiones.lista_id','=','listas_dispersiones.id')
        ->leftjoin('cat_ejercicios','listas_dispersiones.ejercicio_id','=','cat_ejercicios.id')
        ->where([
            ['listas_dispersiones.bimestre',$bimestre],
            ['cat_ejercicios.anio',$anio],
            ['bienestardispersiones.pagado',1]
        ])->select(['bienestarpersonas.posicion']);
        //obteniendo la fecha donde finaliza la dispersion
        // $month = "{$anio}-".$bimestre*2;
        // $aux = date('Y-m-d', strtotime("{$month} + 1 month"));
        // $last_day = date('Y-m-d', strtotime("{$aux} - 1 day"));
        //cargado todos los beneficiarios validos
        $sub = PersonasPrograma::leftjoin('bienestardispersiones','bienestardispersiones.personas_programa_id','=','personas_programas.id')
        ->leftjoin('listas_dispersiones','listas_dispersiones.id','=','bienestardispersiones.lista_id')
        ->leftjoin('cat_ejercicios','listas_dispersiones.ejercicio_id','=','cat_ejercicios.id')
        ->where([
            ['anios_programa_id',$aniosPrograma],
            ['anio',$anio],
            ['bimestre',$bimestre]])
        ->select(['personas_programas.id']);

        $query->leftjoin('bienestardispersiones','bienestardispersiones.personas_programa_id','=','personas_programas.id')
        ->leftjoin('listas_dispersiones','listas_dispersiones.id','=','bienestardispersiones.lista_id')
        ->where('anios_programa_id',$aniosPrograma)
        ->where(function ($query) use ($bimestre,$dispersion,$sub) {
            $query->whereNull('lista_id')
                  ->orwhere('lista_id',$dispersion)
                  ->orwhere(function ($query) use ($bimestre,$dispersion,$sub) {
                    $query
                    ->where('bimestre','!=',$bimestre)
                    ->whereNotIn('personas_programas.id', $sub);
                  })
                  ->orwhere(function ($query) use ($bimestre,$dispersion,$sub) {
                    $query
                    ->where('pagado',2)
                    ->where('listas_dispersiones.status','FINALIZADA')
                    ;
                    //$query->where('bimestre','!=',$bimestre)->whereNotIn('personas_programas.id', $sub);
                  })
                  //->orWhere('pagado',2);
                  ;
                  //->orWhere('pagado',0);
        })
        //->whereIn('bienestarpersonas.posicion',$posicionesPagadas)//array_column($posicionesPagadas, 'posicion'))
        //->whereRaw("TIMESTAMPDIFF(YEAR,fecha_nacimiento,'$last_day') < 65")
        ->distinct('personas_programas.id');//dd($query);
        //dd();
    }
}