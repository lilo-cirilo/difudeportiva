<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recorridos extends Model {
  use SoftDeletes;

/* protected $connection = 'mysql_server_real'; */

  protected $table = 'recorridos';
  protected $dates = ['deleted_at'];
  protected $fillable=["nombre"];
  protected $hidden = array('created_at', 'updated_at', 'deleted_at');
}