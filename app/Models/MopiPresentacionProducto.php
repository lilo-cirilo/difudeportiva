<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MopiPresentacionProducto extends Model
{
    use SoftDeletes;
    protected $table = 'mopi_presentacion_productos';
    protected $dates = ['deleted_at'];
    protected $hidden = array('created_at', 'updated_at', 'deleted_at');

    protected $fillable = ['precio_estimado','descripcion','marca'];

    protected $with = ['presentacion','producto'];

    public function producto(){
        return $this->belongsTo('App\Models\MopiCatProductos');
    }

    public function presentacion(){
        return $this->belongsTo('App\Models\MopiCatPresentaciones');
    }

    public function categoria(){
        return $this->belongsTo('App\Models\MopiCatCategorias');
    }
}
