<?php

namespace App\Models\InventariosCree;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    use SoftDeletes;

    protected $table = 'cat_categorias';

    public function articulos()
    {
        return $this->hasMany('App\Models\InventariosCree\Articulo');
    }

    public function area()
    {
    	return $this->belongsTo('App\Models\Area');
    }
}
