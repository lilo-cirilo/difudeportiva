<?php

namespace App\Models\InventariosCree;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articulo extends Model
{
    use SoftDeletes;

    protected $table = 'cat_articulos';

    public function categoria()
    {
        return $this->belongsTo('App\Models\InventariosCree\Categoria');
    }

    public function area()
    {
    	return $this->belongsTo('App\Models\Area');
    }
}
