<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
    use SoftDeletes;

    protected $table = 'personas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['titulo', 'nombre', 'primer_apellido', 'segundo_apellido', 'calle', 'numero_exterior', 'numero_interior', 'colonia', 'codigopostal', 'municipio_id', 'localidad_id', 'curp', 'genero', 'fecha_nacimiento', 'clave_electoral', 'numero_celular', 'numero_local', 'email', 'referencia_domicilio', 'curpo', 'fotografia', 'usuario_id','etnia_id','num_acta_nacimiento'];

    public function scopeSearch($query) {
        $query
        ->leftjoin('cat_localidades', 'cat_localidades.id', '=', 'personas.localidad_id')
        ->leftjoin('cat_municipios', 'cat_municipios.id', '=', 'personas.municipio_id')
        ->leftjoin('cat_distritos', 'cat_distritos.id', '=', 'cat_municipios.distrito_id')
        ->leftjoin('cat_regiones', 'cat_regiones.id', '=', 'cat_distritos.region_id');
    }

    public function extradatos() {
        return $this->hasMany('Modules\DesayunosEscolaresRegistro\Entities\ExtraDatosPersona');
    }

    public function localidad() {
        return $this->belongsTo('App\Models\Localidad');
    }

    public function municipio() {
        return $this->belongsTo('App\Models\Municipio');
    }

    public function usuario() {//quien lo dio de alta
        return $this->belongsTo('App\Models\Usuario', 'id', 'usuario_id');
    }

    public function usuario2() {//una persona tipo usuario
        return $this->hasOne('App\Models\Usuario');
    }

    public function empleado() {
        return $this->hasOne('App\Models\Empleado');
    }

    public function bienestarpersona() {
        return $this->hasOne('App\Models\BienestarPersona');
    }

    public function personasprogramas() {
        return $this->hasMany('App\Models\PersonasPrograma');
    }

    public function obtenerNombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function cuentasbancos() {
        return $this->hasMany('App\Models\CuentasBancos');
    }

    public function solicitudes() {
        return $this->hasMany('App\Models\Solicitud', 'id');
    }

    public function entradasproductospersona() {
        return $this->hasMany('App\Models\EntradasproductosPersona');
    }

    public function afuncionalespersona() {
        return $this->hasOne('App\Models\Afuncionalespersona');
    }

    public function personastipos() {
        return $this->hasMany('App\Models\PersonasTipo');
    }

    public function salidasproductos() {
        return $this->hasMany('App\Models\SalidasProducto');
    }

    public function tutor() {
        return $this->hasMany('App\Models\Tutor','tutorado_id','id');
    }

    public function tutor_bienestar($programa_id) {
        return $this->hasMany('App\Models\Tutor', 'tutorado_id', 'id')->where('programa_id', '=', $programa_id);
    }

    public function beneficiosPersonas() {
        return $this->hasMany('App\Models\BeneficiosPersonas')->orderBy('updated_at','DESC');
    }

    public function detalleproductos() {
        return $this->hasMany('App\Models\Detalleproducto');
    }

    public function nombreCompleto() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function documentospersonas() {
        return $this->hasMany('App\Models\DocumentosPersona');
    }

    public function solicitudespersonales() {
        return $this->hasMany('App\Models\SolicitudesPersonales');
    }

    public function get_url_fotografia() {
        if(isset($this->fotografia)) {
            return $this->fotografia;
        }
        return 'images/no-image.png';
    }

    public function get_nombre_completo() {
        return $this->nombre . ' ' . $this->primer_apellido . ' ' . $this->segundo_apellido;
    }

    public function get_edad(){
      return with(\Carbon\Carbon::parse($this->fecha_nacimiento))->age;
    }

    public function get_formato_fecha_nacimiento() {
        return (\DateTime::createFromFormat('Y-m-d', $this->fecha_nacimiento))->format('d/m/Y');
    }

    public function etnia() {
        return $this->belongsTo('App\Models\Etnia');
    }
    public function evento(){ // para el modulo orgEventos
        // return $this->belongsToMany('App\Models\EventosTaller','participante_evento','participante_id','tipo_evento_id'); //antes
        return $this->belongsToMany('App\Models\EventosTaller','orev_eventos_participantes','persona_id','evento_id'); //despues
    }
    public function personabeneficio() {
        return $this->belongsTo('App\Models\BeneficiosPersonas','id','persona_id');
		}
		
		public function dtutor() {
			return $this->belongsTo('App\Models\Tutor','id','persona_id');
		}
		// para el modulo de credencializacion se guarda la ruta del certificado
		public function certificado() {
			return $this->belongsTo('App\Models\CertificadoMedico','id','persona_id');
		}
}
