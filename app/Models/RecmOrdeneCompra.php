<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RecmOrdeneCompra extends Model
{
    use SoftDeletes;
    protected $table = 'recm_ordenescompra';
    protected $dates = ['deleted_at'];
    protected $hidden = array('updated_at', 'deleted_at');

    protected $fillable = ['folio','subtotal','iva','total','cantidad','motivo','folio_interno','observacion','created_at'];

    public function estatus(){ // Estatus actual
        return $this->belongsTo('App\Models\RecmCatEstatus','status_id');
    }

    public function requisicion()
    {
        return $this->belongsTo('App\Models\RecmRequisicion');
    }
    public function productos()
    {
        return $this->hasMany('App\Models\RecmOrdenCompraProductos','orden_compra_id');
    }
    public function proveedor()
    {
        return $this->belongsTo('App\Models\RecmCatProveedor');
    }
    
    public function productosList (){
        return $this->belongsToMany('App\Models\RecmPresentacionProducto','recm_orden_compra_productos','orden_compra_id','presentacion_producto_id')->withPivot('cantidad','precio_unitario_real','subtotal');
    }
}
