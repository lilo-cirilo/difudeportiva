<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\Programa;
use App\Models\BeneficiosprogramasSolicitud;
use Yajra\DataTables\Services\DataTable;

class BeneficiosDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addColumn('estatus', function($beneficio) {
            $status = '';
            // dd($beneficio);
            switch ($beneficio->estatus) {
                case "SOLICITANTE":
                    // if(!$beneficio->programa_id)
                    //Comprobando que si AtnCiudadana puede agregar beneficiarios y que el numero de eneficiariarios es igual al solicitado
                    if( ($beneficio->beneficiarios == 1 && 
                        BeneficiosprogramasSolicitud::find($beneficio->beneficioprograma_solicitud_id)->petcionespersonas()->whereNull('observacion_evaluado')->count() == BeneficiosprogramasSolicitud::find($beneficio->beneficioprograma_solicitud_id)->cantidad)
                        ||
                        ($beneficio->beneficiarios == 0)){
                            $status = "<a onclick='Solicitud.turnar_peticion($beneficio->beneficio_id ,\"$beneficio->folio\");' class='btn btn-warning btn-xs'><i class='fa fa-paper-plane-o'></i> Turnar</a>";
                    } else{
                        $status = '<span class="label label-warning">SOLICITANTE</span>';
                    }
                    break;
                case "VINCULADO":
                    $status = '<span class="label label-success">VINCULADO</span>';
                    break;
                case "VALIDANDO":
                    $status = '<span class="label label-warning">VALIDANDO</span>';
                    break;
                case "LISTA DE ESPERA":
                    $status = '<span class="label label-info">LISTA DE ESPERA</span>';
                    break;
                case "CANCELADO":
                    $status = '<span class="label label-danger">CANCELADO</span>';
                    break;
                case "RECHAZADO":
                    $status = '<span class="label label-danger">RECHAZADO</span>';
                    break;
                case "FINALIZADO":
                    $status = '<span class="label label-primary">FINALIZADO</span>';
                    break;
            }
            return $status;
        })
        ->orderColumn('estatus', 'estatus $1')
        ->addColumn('beneficiarios', function($beneficio) {
            if($beneficio->beneficiarios) {
                if($beneficio->estatus === 'SOLICITANTE' || $beneficio->estatus === 'VALIDANDO') {
                    return '<a onclick="Beneficiarios.agregar(' . $beneficio->beneficio_id .','. $beneficio->beneficioprograma_solicitud_id . ', \''.$beneficio->folio.'\');" class="btn btn-info btn-xs"><i class="fa fa-users"></i> Agregar</a>';
                }else{
                    return '<a onclick="Beneficiarios.agregar(' . $beneficio->beneficio_id .','. $beneficio->beneficioprograma_solicitud_id .', \''.$beneficio->folio.'\');" class="btn btn-info btn-xs"><i class="fa fa-users"></i> Consultar</a>';
                }
            }
        })    
        ->addColumn('editar', function($beneficio) {
            if($beneficio->estatus === 'SOLICITANTE'){
                return "<a onclick='Solicitud.editar_peticion($beneficio->beneficio_id,\"$beneficio->apoyo\", $beneficio->cantidad,\"$beneficio->folio\")' class='btn btn-warning btn-xs'><i class='fa fa-pencil'></i> Editar</a>";
            }
        })
        ->addColumn('eliminar', function($beneficio) {
            if($beneficio->estatus === 'SOLICITANTE' && BeneficiosprogramasSolicitud::find($beneficio->beneficioprograma_solicitud_id)->petcionespersonas()->count() === 0){
                return "<a onclick=Solicitud.eliminar_peticion($beneficio->beneficio_id,\"$beneficio->folio\"); class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Eliminar</a>";
            }
        })
        ->rawColumns(['estatus','beneficiarios', 'editar', 'eliminar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        
        if ($this->solicitud_id == null) {
            $this->solicitud_id = 0;
        }        
        return BeneficiosprogramasSolicitud::BeneficiosXsolicitud($this->solicitud_id);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "beneficios"; d.solicitud_id = Solicitud.getId(); }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            'folio' => ['data' => 'folio', 'name' => 'folio'],
            'beneficio' => ['data' => 'apoyo', 'name' => 'apoyo'],
            'cantidad' => ['data' => 'cantidad', 'name' => 'cantidad']
        ];

        if($this->tipo_solicitud == 'E'){
            $columns['dependencia'] = ['data' => 'dependencia', 'name' => 'dependencia'];            
        }        
        
        if($this->tipo_solicitud == 'I'){
            $columns['area'] = ['data' => 'area', 'name' => 'area'];
            $columns['beneficiarios'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }

        $columns['estatus'] = ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false];

        $columns['editar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        $columns['eliminar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'order' => [
                5,
                'desc'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'AtnCiudadana/Beneficios_' . date('YmdHis');
    }
}