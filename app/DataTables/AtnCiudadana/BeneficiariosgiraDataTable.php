<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\BeneficiariogiraPrograma;
use App\Models\Gira;
use Yajra\DataTables\Services\DataTable;

class BeneficiariosgiraDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->addIndexColumn()     
        ->addColumn('foto', function($solicitud) {
            //return '<img id="foto" class="img-responsive" src="/'.$solicitud->foto .'">';
            return '<button style="margin-right: 10px;" class="btn btn-info btn-xs" data-toggle="tooltip" title="Consultar" onclick="mostrar_foto(\''.
            $solicitud->foto .'\')"><i class="fa fa-address-card"></i> Consultar</button>';
        })

        ->rawColumns(['foto']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return BeneficiariogiraPrograma::beneficiarios($this->evento_id)
        ->select(
            [                
                'giraspreregis.nombre as nombre',
                'giraspreregis.primer_apellido as primer_apellido',
                'giraspreregis.segundo_apellido as segundo_apellido',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'programas.nombre as programa',                
                'giraspreregis.folio as folio',
                'cat_productos.producto as apoyo',
                'giraspreregis.fotografia as foto'
            ]
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax(['data' => 'function(d) { d.tipo = "beneficiarios"; }'])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [        
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],    
            'nombre' => ['data' => 'nombre', 'name' => 'giraspreregis.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'giraspreregis.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'giraspreregis.segundo_apellido'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'folio' => ['data' => 'folio', 'name' => 'giraspreregis.folio'],
            'beneficio' => ['data' => 'apoyo', 'name' => 'cat_productos.producto'],
            'foto' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AtnCiudadana/Beneficiariosgira_' . date('YmdHis');
    }

    /*public function snappyPdf() {
        $data = $this->getDataForPrint();

        $snappy = app('snappy.pdf.wrapper');

        $gira = Gira::find($this->gira_id);

        $header = view('atnciudadana::reportes.header');

        $footer = view('atnciudadana::reportes.footer');

        $snappy->setOption('header-html', $header)->setOption('footer-html', $footer);

        $snappy->setOptions([
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '35',
            'margin-bottom' => '25'
        ])->setOrientation('landscape');

        $snappy->setTimeout(600);
        
        return $snappy->loadView('atnciudadana::reportes.gira', compact('data','gira'))->download($this->getFilename() . '.pdf');
    }*/
}
