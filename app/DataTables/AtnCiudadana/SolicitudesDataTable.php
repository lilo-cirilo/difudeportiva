<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\Solicitud;
use App\Models\EventosSolicitudes;
use App\Models\Usuario;
use App\User;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SolicitudesDataTable extends DataTable {

    public function dataTable($query) {
      return datatables($query)
        ->editColumn('recepcion', function($solicitud) {
          $evento_solicitud = EventosSolicitudes::where('solicitud_id',$solicitud->id)->first();
            if($evento_solicitud){
                return $solicitud->recepcion . ': ' . $evento_solicitud->evento->gira->nombre;
            }
            return $solicitud->recepcion;
        })
        ->addColumn('consultar', function($solicitud) {
          return '<a href="' . route('solicitudes.show',$solicitud->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Ver</a>';
        })
        ->addColumn('editar', function($solicitud) {
            if($solicitud->beneficios > 0 && $solicitud->beneficios == $solicitud->turnados) {
                // return '<a href="' . route('solicitudes.edit',['solicitud_id' => $solicitud->id, 'tipo_solicitud' => $this->tipo_solicitud]) . '" class="btn btn-info btn-xs" disabled><i class="fa fa-edit"></i> Editar</a>';
                return '<span class="label label-info noedit" disabled><i class="fa fa-edit"></i> Editar</span>';
            }
          return '<a href="' . route('solicitudes.edit',['solicitud_id' => $solicitud->id, 'tipo_solicitud' => $this->tipo_solicitud]) . '" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Editar</a>';
        })
        ->addColumn('turnado', function($solicitud) {
            if($solicitud->beneficios == $solicitud->turnados) {
                return '<span class="label label-success">' . $solicitud->turnados . '/' . $solicitud->beneficios . '</span>';
            }
            return '<span class="label label-warning">' . $solicitud->turnados . '/' . $solicitud->beneficios . '</span>';
        })
        ->addColumn('estatus', function($solicitud) {
          $status = '';
          switch ($solicitud->status_g) {
            case "FINALIZADO":
                $status = '<span class="label label-primary">FINALIZADO</span>';
                break;
            case "PENDIENTE":
                $status = '<span class="label label-warning">PENDIENTE</span>';
                break;
            case "PROCESANDO":
                $status = '<span class="label label-success">PROCESANDO</span>';
                break;
          }
          return $status;
        })
        ->filterColumn('folio', function($query, $keyword) {
          $query->whereRaw("solicitudes.folio like ?", ["%$keyword%"]);
        })
        ->editColumn('fecharecepcion', function($peticion) {
            return with(\Carbon\Carbon::parse($peticion->fecharecepcion))->format('d-m-Y');
        })
        ->filterColumn('fecharecepcion', function($query, $keyword) {
          $query->whereRaw("select DATE_FORMAT(fecharecepcion,'%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->orderColumn('fecharecepcion', 'fecharecepcion $1')
        ->filterColumn('recepcion', function($query, $keyword) {
          $query->whereRaw("cat_tiposrecepciones.tipo like ?", ["%$keyword%"]);
        })  
        ->filterColumn('remitente', function($query, $keyword) {
          $query->whereRaw("CASE ".
          "WHEN cat_tiposremitentes.id = 4 THEN ".
                "( SELECT CONCAT(p.nombre,' ',p.primer_apellido,' ',p.segundo_apellido) ".
                    "FROM solicitudes_personales sp, personas p ".
                    "WHERE sp.persona_id = p.id AND solicitudes.id = sp.solicitud_id) ".
            "WHEN cat_tiposremitentes.id = 3 THEN ".
                "( SELECT d.nombre ".
                    "FROM solicitudes_dependencias sd, cat_instituciones d ".
                    "WHERE sd.dependencia_id = d.id AND solicitudes.id = sd.solicitud_id) ".
            "WHEN cat_tiposremitentes.id = 2 THEN ".
                "( SELECT m.nombre ".
                    "FROM solicitudes_municipios sm, cat_municipios m ".
                    "WHERE sm.municipio_id = m.id AND solicitudes.id = sm.solicitud_id) ".
        "WHEN cat_tiposremitentes.id = 1 THEN ".
                "( SELECT r.nombre ".
                    "FROM solicitudes_regiones sr, cat_regiones r ".
                    "WHERE sr.region_id = r.id AND solicitudes.id = sr.solicitud_id) ".
        "END like ?", ["%$keyword%"]);
        })  
        ->filterColumn('tipo', function($query, $keyword) {
          $query->whereRaw("cat_tiposremitentes.tipo like ?", ["%$keyword%"]);
        })  
        ->filterColumn('asunto', function($query, $keyword) {
          $query->whereRaw("asunto like ?", ["%$keyword%"]);
        })  
        ->filterColumn('dias', function($query, $keyword) {
          $query->whereRaw("dias = ?", [$keyword]);
        })  
        // ->filterColumn('estatus', function($query, $keyword) {
        //   $query->whereRaw("CASE WHEN	IFNULL(".
		// 	    "(  SELECT (SELECT count(ss2.statusproceso_id) ".
		// 					"FROM status_solicitudes ss2 ".
		// 					"WHERE ss2.programas_solicitud_id = ss.programas_solicitud_id ".
		// 					"AND (es2.statusproceso_id >= 6 AND es2.statusproceso_id <= 8)) AS procesos ".
		// 	        "FROM ".
		// 		        "programas_solicitudes ps,	status_solicitudes ss ".
		// 	        "WHERE ".
		// 		        "ps.id = ss.programas_solicitud_id AND ps.solicitud_id=solicitudes.id ".
		// 	        "GROUP BY ps.solicitud_id, ss.programas_solicitud_id ".
		// 	        "HAVING procesos = 0 ".
		// 	        "LIMIT 1),-1) = 0 then 'Pendiente' ELSE 'Finalizado' ".
		//     "END like ?", ["%$keyword%"]);
        // })  
        ->rawColumns(['consultar','estatus','editar','turnado']);
    }
    
    public function query() {
        //AQUI IRA EL IF PARA MOSTRAR LAS SOLICITUDES POR DIRECCION O POR USUARIO DEPENDE DE LO QUE SE REQUIERA :D
        if(auth()->user()->hasRolesStrModulo(['ATNC-AREA'], 'atnciudadana')) {
            $area_id = auth()->user()->persona->empleado->area_id;
            $direccion_id = DB::select('call getPadreSiaf(?)', [$area_id])[0]->direccion_id;
            $areas_hijas = DB::select('call getAreasHijas(?)', [$direccion_id]);
            $areas_hijas = array_map(
                function($e) { return $e->id_hijo; },
                $areas_hijas
            );
            array_push($areas_hijas, $direccion_id);
            $usuarios = Usuario::join('empleados','usuarios.persona_id','empleados.persona_id')->whereIn('empleados.area_id',$areas_hijas)->get()->toArray();
            $usuarios = array_map(
                function($e) { return $e['id']; },
                $usuarios
            );
            return Solicitud::allSolicitudes(null)->whereIn('solicitudes.usuario_id',$usuarios)->where('solicitudes.folio','LIKE',$this->tipo_solicitud.'%');
        }
        return Solicitud::allSolicitudes(null)->where('solicitudes.folio','LIKE',$this->tipo_solicitud.'%');
    }

    public function html() {
      return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }
    
    protected function getColumns() {
      return [
        'Folio' => ['data' => 'folio', 'name' => 'folio'],
        'Fecha Recepción' => ['data' => 'fecharecepcion', 'name' => 'fecharecepcion'],
        'Recepción' => ['data' => 'recepcion', 'name' => 'recepcion'],
        'Remitente' => ['data' => 'remitente', 'name' => 'remitente'],
        'Tipo' => ['data' => 'tipo', 'name' => 'tipo'],
        'Asunto' => ['data' => 'asunto', 'name' => 'asunto'],
        'Dias Transcurridos' => ['data' => 'dias', 'name' => 'dias','searchable' => false],
        'Estatus' => ['data' => 'estatus', 'name' => 'estatus','searchable' => false],
        'turnado' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
        'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
        'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
      ];
    }

    protected function getBuilderParameters() {
      $builderParameters =
        [
          'language' => [
            'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
          ],
          'dom' => 'Btip',
          'preDrawCallback' => 'function() { block(); }',
          'drawCallback' => 'function() { unblock(); }',
          'buttons' => [
            [
              'extend' => 'excel',
              'text' => '<i class="fa fa-file-excel-o"></i> Excel',
              'className' => 'button-dt tool'
            ],
            [
              'extend' => 'pdf',
              'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
              'className' => 'button-dt tool'
            ],
            [
              'extend' => 'reset',
              'text' => '<i class="fa fa-undo"></i> Reiniciar',
              'className' => 'button-dt tool'
            ],
            [
              'extend' => 'reload',
              'text' => '<i class="fa fa-refresh"></i> Recargar',
              'className' => 'button-dt tool'
            ]
          ],
          'lengthMenu' => [ [10], [10] ],
          'responsive' => true,
          'columnDefs' => [
            [ 'className' => 'text-center', 'targets' => [0,1,4,6,7,8,9] ]
          ],
          'order' => [1,'desc']          
        ];
        return $builderParameters;
    }

    protected function filename() {
        return 'Solicitudes_' . date('YmdHis');
    }
}
