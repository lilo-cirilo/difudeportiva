<?php

namespace App\DataTables\AtnCiudadana;

use App\Models\BeneficiariogiraPrograma;
use Yajra\DataTables\Services\DataTable;

class SolicitantesGiraDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        ->addIndexColumn();
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return BeneficiariogiraPrograma::solicitantes($this->evento_id)
        ->select(
            [                
                'giraspreregis.nombre as nombre',
                'giraspreregis.primer_apellido as primer_apellido',
                'giraspreregis.segundo_apellido as segundo_apellido',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',
                'programas.nombre as programa',                
                'giraspreregis.folio as folio',
                'cat_productos.producto as apoyo',
                'giraspreregis.fotografia as foto'
            ]
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax(['data' => 'function(d) { d.tipo = "solicitantes"; }'])
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [        
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],    
            'nombre' => ['data' => 'nombre', 'name' => 'giraspreregis.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'giraspreregis.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'giraspreregis.segundo_apellido'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'apoyo solicitado' => ['data' => 'programa', 'name' => 'programas.nombre']
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AtnCiudadana/Beneficiariosgira_' . date('YmdHis');
    }
}
