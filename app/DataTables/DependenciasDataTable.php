<?php

namespace App\DataTables;

use App\Models\Dependencia;
use Yajra\DataTables\Services\DataTable;

class DependenciasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
      if($this->tipo === 'modal'){
        return datatables($query)          
            ->addColumn('seleccionar', function($dependencia) {
                return '<a class="btn btn-success btn-xs" onclick="seleccionar_dependencia(' . "'" . $dependencia->id . "'" . ', ' . "'" .  $dependencia->nombre . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
            })
            ->rawColumns(['seleccionar']);
      } 
        return datatables($query)
            ->addColumn('consultar', function($dependencia) {
                return '<a onclick="abrir_modal(\'/dependencias/show/' . $dependencia->id . '\',\'Datos de la '. strtolower($dependencia->tipo->nombre) . '\', true)" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
            })
            ->addColumn('editar', function($dependencia) {
                return '<a onclick="abrir_modal(\'/dependencias/edit/' . $dependencia->id . '\', \'Editar dependencia o institución\')" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
            })
            ->addColumn('eliminar', function($dependencia) {
                return '<a class="btn btn-danger btn-xs" onclick="dependencia_delete(' . $dependencia->id . ', \'' . $dependencia->tipo->nombre . '\');"><i class="fa fa-trash"></i> Eliminar</a>';
            })
            ->rawColumns(['consultar','editar', 'eliminar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return Dependencia::search()
        ->select(['cat_instituciones.id as id', 
                'cat_instituciones.nombre as nombre', 
                'cat_instituciones.encargado as encargado', 
                'cat_instituciones.cargoencargado as cargo', 
                'cat_instituciones.telefono as telefono',
                'cat_instituciones.tipoinstitucion_id'
        ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->ajax(['data' => 'function(d) { d.table = "dependencias"; }'])                    
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            'nombre' => ['data' => 'nombre', 'name' => 'nombre'],
            'titular' => ['data' => 'encargado', 'name' => 'encargado'],
            'cargo' => ['data' => 'cargo', 'name' => 'cargoencargado'],
            'telefono' => ['data' => 'telefono', 'name' => 'telefono']
        ];
        if($this->tipo === 'modal'){
            $columns['seleccionar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }else{
            $columns['consultar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            $columns['editar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            $columns['eliminar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }
        return $columns;
    }

    protected function getBuilderParameters() {
        if($this->tipo === 'modal'){
            $builderParameters = [
                'language' => [
                    'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
                ],
                'dom' => 'Btip',
                'preDrawCallback' => 'function() { block(); }',
                'drawCallback' => 'function() { unblock(); }',
                'buttons' => [
                    [
                        'className' => 'button-dt tool',
                        'text' => '<i class="fa fa-plus"></i> Nuevo',
                        'action' => 'function ( e, dt, node, config ) { abrir_modal("/dependencias/create","Registrar dependencia o institución"); }'
                    ]
                ],
                'lengthMenu' => [ [10], [10] ],
                'responsive' => true,
                'columnDefs' => [
                    [ 'className' => 'text-center', 'targets' => [3,4] ]
                ]
            ];
        }else{     
            $builderParameters = [
                'language' => [
                    'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
                ],
                'dom' => 'Btip',
                'preDrawCallback' => 'function() { block(); }',
                'drawCallback' => 'function() { unblock(); }',
                'buttons' => [
                    [
                        'className' => 'button-dt tool',
                        'text' => '<i class="fa fa-plus"></i> Nuevo',
                        'action' => 'function ( e, dt, node, config ) { abrir_modal("/dependencias/create","Registrar dependencia o institución"); }'
                    ]
                ],
                'lengthMenu' => [ [10], [10] ],
                'responsive' => true,
                'columnDefs' => [
                    [ 'className' => 'text-center', 'targets' => [3,4,5] ]
                ]
            ];
        }
        
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Dependencias_' . date('YmdHis');
    }
}