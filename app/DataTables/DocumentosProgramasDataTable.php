<?php

namespace App\DataTables;

use App\User;
use App\Models\DocumentosPrograma;
use App\Models\AniosPrograma;
use App\Models\Ejercicio;
use Yajra\DataTables\Services\DataTable;

class DocumentosProgramasDataTable extends DataTable{
  
  protected $actions = ['new'];
  /**
  * Build DataTable class.
  *
  * @param mixed $query Results from query() method.
  * @return \Yajra\DataTables\DataTableAbstract
  */
  
  public function dataTable($query){
    return datatables($query)
    ->addColumn('documento', function ($DocProg){
      return $DocProg->documento->nombre;
    })
    ->addColumn('eliminar',function($DocProg){
      return "<button class='btn btn-warning btn-sm' onClick='eliminarDocumento($DocProg->id)'><i class='fa fa-trash'></i> Eliminar</button>";
    })->rawColumns(['eliminar']);
  }
  
  /**
  * Get query source of dataTable.
  *
  * @param \App\User $model
  * @return \Illuminate\Database\Eloquent\Builder
  */
  public function query(){
    $apaux = AniosPrograma::where([['programa_id',$this->programa_id],['ejercicio_id',Ejercicio::where('anio',date('Y'))->first()->id]])->first()->id;
    return DocumentosPrograma::where('anios_programa_id',$apaux);
  }
  
  /**
  * Optional method if you want to use html builder.
  *
  * @return \Yajra\DataTables\Html\Builder
  */
  public function html(){
    return $this->builder()
    ->columns($this->getColumns())
    ->ajax([
      'url' => route('programas.documentos.index',$this->programa_id),
      'type' => 'GET',
    ])
    //->addAction(['width' => '80px'])
    ->parameters($this->getBuilderParameters());
  }
  
  /**
  * Get columns.
  *
  * @return array
  */
  protected function getColumns(){
    return [
      'documento','eliminar'
    ];
  }
  protected function getBuilderParameters() {
    $builderParameters = [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      'buttons' => [
        'new',
        'reload'                
      ],
      'lengthMenu' => [ [10], [10] ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
        // [ 'visible' => false, 'targets' => 0 ]
        ]
      ];
      
      return $builderParameters;
    }
    /**
    * Get filename for export.
    *
    * @return string
    */
    protected function filename(){
      return 'Programas asociados';
    }
  }
  