<?php

namespace App\DataTables\ConsultasMedicas;

use Yajra\DataTables\Services\DataTable;

use App\Models\ConsultasMedicas\Receta;

class RecetaTabla extends DataTable{

    /**
     * Construye el dataTable
     * @param mixed $query Resultado obtenido a partir del método query()
     * @return \Yajra\DataTables\DataTableAbstract El dataTable
     */
    public function dataTable($query){
        return datatables($query)
        ->addColumn('show',function($paciente){
            return '<a href="'.route('consultasmedicas.pacientes.show',$paciente->id).'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </i></a>';
        })
        ->addColumn('edit',function($paciente){
            if ($paciente->estado == null)
                return '<a href="'.route('consultasmedicas.pacientes.edit',$paciente->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</i></a>';
            return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
        })
        ->addColumn('servicio',function($paciente){
            if ($paciente->estado == null)
                return '<a class="btn btn-xs btn-danger" onclick="eliminarRegistro('."'".route('consultasmedicas.pacientes.destroy',$paciente->id)."','paciente'".')"><i class="fa fa-remove"> Dar de baja</i></a>';
            return '<a class="btn btn-xs btn-success"><i class="fa fa-check"> Reactivar</i></a>';
        })
        // ->editColumn('estado',function($paciente){
        //     if ($paciente->estado == null)
        //         return 'Activo';
        //     return 'Baja';
        // })
        ->editColumn('edad',function($paciente){
            $fecha = time() - strtotime($paciente->edad);
            return floor($fecha / 31556926);
        })
        ->rawColumns(['show','edit','destroy']);
    }

    /**
     * Genera la fuente de datos para el dataTable
     * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
*/
    public function query(){

        $model;

        $model = Receta::all();

        return $model;
    }

    /**
     * Método pata configurar el constructor html
     * @return \Yajra\DataTables\Html\Builder Constructor de html
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Define las columnas que mostrara el html
     * @return array Arreglo con la configuración de columnas
     */
    protected function getColumns(){
        $columns =
        [
            'id'                => ['data' => 'id', 'name' => 'pacientes.id'],
            // 'estado'            => ['data' => 'estado', 'name' => 'pacientes.deleted_at'],
            'nombre'            => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido'   => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'edad'              => ['data' => 'edad', 'name' => 'personas.fecha_nacimiento'],
            'curp'              => ['data' => 'curp', 'name' => 'personas.curp'],
            'municipio'         => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'consultar'         => ['data' => 'show', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'servicio'            => ['data' => 'servicio', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false]
            //'Eliminar'          => ['data' => 'destroy', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
        ];
        if($this->request->input('tipo')==='select'){
            $columns =[
                'nombre'            => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer_apellido'   => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'completo'          => ['data' => 'nombreCompleto']
            ];
        }
        return $columns;
    }

    /**
 * Define los parámetros de configuración para la librería de jquery dataTables
 * @return Array Parametros de inicializacion para la librería
 */
protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'sEmptyTable' => 'No hay tratamientos disponibles'
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'create',
                    'text'   => '<i class="fa fa-plus"></i> Agregar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Define un nombre para el archivo que se exportara
     * @return string Nombre del archivo, sin extension
     */
    protected function filename(){
        return 'beneficiarios braile al ' . date('Ymd');
    }
}
