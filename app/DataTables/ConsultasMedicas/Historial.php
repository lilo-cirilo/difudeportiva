<?php

namespace App\DataTables\ConsultasMedicas;

use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use App\Models\ConsultasMedicas\Consulta;

class Historial extends DataTable{
    /**
     * Construye el dataTable
     * @param mixed $query Resultado obtenido a partir del método query()
     * @return \Yajra\DataTables\DataTableAbstract El dataTable
     */

    public function dataTable($query){
        return datatables($query);
        //  ->addColumn('nombre',function($consulta){
        //    return $consulta->nombre.' '.$consulta->primer_apellido.' '.$consulta->segundo_apellido;
        //  });
        // ->addColumn('tipo',function($consulta){
        //   if ($consulta->odontologia == null && $consulta->psicologia == null)
        //     return 'General';
        //   if ($consulta->odontologia == null)
        //     return 'Psicologia';
        //   return 'Odontologia';
        // });//->rawColumns(['edit','destroy']);
    }

    /**
     * Genera la fuente de datos para el dataTable
     * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
    */
    public function query(){
      return Consulta::select([DB::raw('distinct(consultas.id)'),
      DB::raw('CASE WHEN isnull( cie.nombre ) THEN CONCAT("TRATAMIENTO ",so.tipo) ELSE cie.nombre END AS diagnostico'),
      DB::raw('CASE WHEN isnull(me.nombre_generico) THEN so.nombre ELSE me.nombre_generico END as tratamiento'),
      DB::raw('CONCAT( p.nombre," ",p.primer_apellido," ",p.segundo_apellido ) as medico'),
      DB::raw('consultas.created_at as fecha')])
      ->LEFTJOIN('consulta_psicologica AS cp','cp.consulta_id','consultas.id')   
      ->LEFTJOIN('consulta_odontologica AS co','co.consulta_id','consultas.id')   
      ->LEFTJOIN('diagnosticos AS d','d.consulta_id','consultas.id')   
      ->LEFTJOIN('recetas AS r','r.consulta_id','consultas.id')   
      ->LEFTJOIN('cat_servicios_odontologicos AS so','so.id','co.servicio_id')   
      ->LEFTJOIN('cat_medicamentos AS me','me.id','r.medicamento_id')   
      ->LEFTJOIN('cat_cie10 AS cie','cie.id','d.cie10_id')
      ->JOIN('empleados AS e','e.id','consultas.empleado_id')   
      ->JOIN('personas AS p','p.id','e.persona_id')
      ->where('paciente_id',$this->paciente);//->get();
    }

    /**
     * Método pata configurar el constructor html
     * @return \Yajra\DataTables\Html\Builder Constructor de html
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Define las columnas que mostrara el html
     * @return array Arreglo con la configuración de columnas
     */
    protected function getColumns(){
        // $columns =
        // [
        //     'id'                => ['data' => 'id', 'name' => 'consultas.id'],
        //     'tipo'              => ['data' => 'tipo', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
        //     'doctor'            => ['data' => 'doc'],
        //     'fecha de consuta'  => ['data' => 'created_at', 'name' => 'consultas.created_at']
        // ];
        return [
          //'doctor' => ['data' => '', 'name' => ''],
          'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
          'diagnostico' => ['data' => 'diagnostico', 'name' => 'diagnostico'],
          'tratamiento' => ['data' => 'tratamiento', 'name' => 'tratamiento'],
        ];
    }

    /**
   * Define los parámetros de configuración para la librería de jquery dataTables
   * @return Array Parametros de inicializacion para la librería
   */
  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'sEmptyTable' => 'Este paciente no ha recibido consultas'
      ],
      'dom' => 'Btip',
      'buttons' => [
        [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => 'button-dt tool'
        ]
      ],
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ],
        [ 'visible' => false, 'targets' => 0 ]
      ],
      'rowGroup' => [
        'startRender' => 'function (rows,group) { return group.substring(0,9) + " " + rows.context[0].json.data[rows[0][0]].medico + "<button class=\"btn btn-info btn-xs pull-right\" onClick=\"muestraCita("+rows.context[0].json.data[rows[0][0]].id+")\"><i class=\"fa fa-eye\"></i> Ver</button>" ; }',
        'dataSrc' => 'fecha'
      ]
    ];

    return $builderParameters;
  }

    /**
     * Define un nombre para el archivo que se exportara
     * @return string Nombre del archivo, sin extension
     */
    protected function filename(){
        return 'expediente' . date('Ymd');
    }
}
