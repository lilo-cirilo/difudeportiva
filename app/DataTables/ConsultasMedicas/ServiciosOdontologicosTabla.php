<?php

namespace App\DataTables\ConsultasMedicas;

use Yajra\DataTables\Services\DataTable;

use App\Models\ConsultasMedicas\ServiciosOdontologicos;

class ServiciosOdontologicosTabla extends DataTable{
    /**
     * Construye el dataTable
     * @param mixed $query Resultado obtenido a partir del método query()
     * @return \Yajra\DataTables\DataTableAbstract El dataTable
     */

    public function dataTable($query){
        return datatables($query)
        // ->addColumn('show',function($curso){
        //     return '<a onClick="cargarModal('."'".route('consultasmedicas.odontologia.servicios.show',$curso->id)."')".'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </a>';
        // })
        ->addColumn('edit',function($curso){
            if ($curso->estado == null)
                return '<a onClick="cargarModal('."'".route('consultasmedicas.odontologia.servicios.edit',$curso->id)."')".'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</a>';
            return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
        })
        ->addColumn('destroy',function($curso){
            if ($curso->estado == null)
                return '<a class="btn btn-xs btn-danger" onclick="eliminarRegistro('."'".route('consultasmedicas.odontologia.servicios.destroy',$curso->id)."')".'"><i class="fa fa-remove"> Eliminar</a>';
            return '<a class="btn btn-xs btn-success"><i class="fa fa-check"> Reactivar</a>';
        })->rawColumns(['edit','destroy']);
    }

    /**
     * Genera la fuente de datos para el dataTable
     * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
*/
    public function query(){
      return ServiciosOdontologicos::select([
          'cat_servicios_odontologicos.id',
          'cat_servicios_odontologicos.nombre',
          'cat_servicios_odontologicos.tipo',
          'cat_servicios_odontologicos.created_at'
      ]);
    }

    /**
     * Método pata configurar el constructor html
     * @return \Yajra\DataTables\Html\Builder Constructor de html
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Define las columnas que mostrara el html
     * @return array Arreglo con la configuración de columnas
     */
    protected function getColumns(){
        $columns =
        [
            'id'                => ['data' => 'id', 'name' => 'cat_servicios_odontologicos.id'],
            'nombre'            => ['data' => 'nombre', 'name' => 'cat_servicios_odontologicos.nombre'],
            'tipo'              => ['data' => 'tipo', 'name' => 'cat_servicios_odontologicos.tipo'],
            'fecha de creacion' => ['data' => 'created_at', 'name' => 'cat_servicios_odontologicos.created_at'],
            // 'consultar'         => ['data' => 'show', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'Editar'            => ['data' => 'edit', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'Eliminar'          => ['data' => 'destroy', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false]
        ];
        return $columns;
    }

    /**
 * Define los parámetros de configuración para la librería de jquery dataTables
 * @return Array Parametros de inicializacion para la librería
 */
protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
                'sEmptyTable' => 'No se han registrado servicios'
            ],
            'dom' => 'Btip',
            'buttons' => [
              [
                'extend' => 'create',
                'text' => '<i class="fa fa-plus"></i> Nuevo',
                'className' => 'button-dt tool',
                'action' => '
                  function (e, dt, button, config) {
                    var url = "'.route('consultasmedicas.odontologia.servicios.create').'";
                    cargarModal(url);
                  }
                '
            ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Define un nombre para el archivo que se exportara
     * @return string Nombre del archivo, sin extension
     */
    protected function filename(){
        return 'cursos braile al ' . date('Ymd');
    }
}
