<?php

namespace App\DataTables\Braille;

use Yajra\DataTables\Services\DataTable;

use App\Models\Braille\Curso;

class Cursos extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    protected $actions = ['new'];

    public function dataTable($query){
        return datatables($query)
        ->addColumn('show',function($curso){
            return '<a onClick="cargarModal('."'".route('braille.cursos.show',$curso->id)."')".'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </a>';
        })
        ->addColumn('edit',function($curso){
            if ($curso->estado == null)
                return '<a onClick="cargarModal('."'".route('braille.cursos.edit',$curso->id)."')".'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Editar</a>';
            return '<a href="#" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Editar</a>';
        })
        ->addColumn('destroy',function($curso){
            if ($curso->estado == null)
                return '<a class="btn btn-xs btn-danger" onclick="eliminarCurso('."'".route('braille.cursos.destroy',$curso->id)."')".'"><i class="fa fa-remove"> Eliminar</a>';
            return '<a class="btn btn-xs btn-success"><i class="fa fa-check"> Reactivar</a>';
        })->rawColumns(['show','edit','destroy']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return Curso::join('programas','programas.id','=','cat_cursos.programa_id')
        ->where('programas.nombre','like','BRAILLE')
        ->select([
            'cat_cursos.id',
            'cat_cursos.nombre',
            'cat_cursos.descripcion',
            'cat_cursos.created_at'
        ]);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'id'                => ['data' => 'id', 'name' => 'cat_cursos.id'],
            'nombre'            => ['data' => 'nombre', 'name' => 'cat_cursos.nombre'],
            'descripcion'       => ['data' => 'descripcion', 'name' => 'cat_cursos.descripcion'],
            'fecha de creacion' => ['data' => 'created_at', 'name' => 'cat_cursos.created_at'],
            'consultar'         => ['data' => 'show', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'Editar'            => ['data' => 'edit', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false],
            'Eliminar'          => ['data' => 'destroy', 'orderable' => false, 'searcheable' => false, 'exportable' => false, 'printable' => false]
        ];
        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                'new',
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'cursos braile al ' . date('Ymd');
    }
}
