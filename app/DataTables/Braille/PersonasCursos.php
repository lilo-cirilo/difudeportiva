<?php

namespace App\DataTables\Braille;

/*

  Tabla para mostrar las personas dentro de un determinado curso.

*/

use Yajra\DataTables\Services\DataTable;
use App\Models\Persona;

class PersonasCursos extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables()->eloquent($query)
        ->addColumn('nombreCompleto',function ($persona){
            return $persona->nombreCompleto();  
        })
        ->addColumn('destroy',function($beneficiario){
            return '<a class="btn btn-xs btn-danger" onclick="eliminarRegistro('."'".route('braille.beneficiarios.destroy',$beneficiario->id)."','beneficiario'".')"><i class="fa fa-remove"> Dar de baja</i></a>';
        })   
        ->rawColumns(['destroy']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $model = Persona::join('braillealumnos', 'braillealumnos.persona_id', '=', 'personas.id')
		    ->join('cursos_personas', 'cursos_personas.persona_id', '=', 'personas.id')
        ->where('cursos_personas.curso_id',$this->request->input('curso',1))
		    ->select(
            [
                'cursos_personas.id as id',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido'/*,
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp'*/
            ]
        );
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this->builder()
        ->columns($this->getColumns())
        ->ajax([
            'data'=>'function(d){
                if($("#curso_id").val() !== undefined){
                    d.curso = $("#curso_id").val();
                }
            }'
          ])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'apellido_paterno' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'apellido_materno' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'Eliminar' => ['data' => 'destroy', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language'        => ['url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')],
            'dom'             => 'Btip',
            // 'preDrawCallback' => 'function() { block(); }',
            // 'drawCallback'    => 'function() { unblock(); }',
            //'buttons'         => [],
            'lengthMenu'      => [ [10], [10] ],
            'responsive'      => true,
            'columnDefs'      => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'personas_' . date('YmdHis');
    }
}
