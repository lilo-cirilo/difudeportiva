<?php

namespace App\DataTables\Braille;

/*

  Tabla para mostrar los cursos que tiene una persona

*/

use Yajra\DataTables\Services\DataTable;
use App\Models\Braille\Curso;
use App\Models\Braille\NotasClinicas;

class CursosPersonas extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables()->eloquent($query)
        ->addColumn('sesiones',function ($curso){
            return NotasClinicas::where([
                ['persona_id',$this->persona_id],
                ['curso_id',$curso->id]
            ])->count();  
        })
        ->addColumn('avance',function($curso){
            $ultimaNota= NotasClinicas::where([
                ['persona_id',$this->persona_id],
                ['curso_id',$curso->id]
            ])->latest()->first();  
            if(isset($ultimaNota))
                return $ultimaNota->descripcion;
            return 'No se han registrado avances';
        });
        /*->rawColumns(['destroy'])*/;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return Curso::join('cursos_personas','cursos_personas.curso_id','cat_cursos.id')
        ->where('cursos_personas.persona_id',$this->persona_id)
		    ->select(
            [
                'cat_cursos.id as id',
                'cat_cursos.nombre as nombre',
            ]
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this->builder()
        ->columns($this->getColumns())
        ->ajax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'nombre'             => ['data' => 'nombre', 'name' => 'cat_cursos.nombre'],
            'numero de sesiones' => ['data' => 'sesiones', 'searchable' => false, 'orderable' => false],
            'ultimo avance'      => ['data' => 'avance', 'searchable' => false, 'orderable' => false]
            // 'ver'                => ['data' => 'see', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language'        => ['url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')],
            'dom'             => 'Btip',
            // 'preDrawCallback' => 'function() { block(); }',
            // 'drawCallback'    => 'function() { unblock(); }',
            //'buttons'         => [],
            'lengthMenu'      => [ [10], [10] ],
            'responsive'      => true,
            'columnDefs'      => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'personas_' . date('YmdHis');
    }
}
