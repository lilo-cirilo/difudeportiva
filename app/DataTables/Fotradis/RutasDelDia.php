<?php

namespace App\DataTables\Fotradis;
use App\Models\ControlRutas;
use App\Models\Rutas;
use App\Models\Unidad;
use App\Models\Conductor;
use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Services\DataTable;

class RutasDelDia extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
      $table = dataTables($query)
                ->editColumn('unidad_id', function($rutas) {
                    return strtoupper($rutas->unidad->numero_unidad);
                })
                ->editColumn('matricula', function($rutas) {
                  return strtoupper($rutas->unidad->matricula);
                })
                ->editColumn('conductor', function($rutas) {
                    return strtoupper($rutas->conductor->empleado->persona->nombreCompleto());
                })
                ->addColumn('clave_conductor', function($rutas) {
                    return '<span id="clave_conductor'. $rutas->conductor_id .'" class="label label-default"></span>';
                })
                ->addColumn('estatus_sesion_app', function($rutas) {
                    return '<span id="sesionapp'. $rutas->conductor_id .'" class="label label-info"></span>';
                })
                ->addColumn('servicio', function($rutas) {                    
                    if ($rutas->unidad->tipo_servicio == 'taxi') {
                        // return '<span class="label label-default"><i class="fa fa-fw fa-taxi text-green"></i>TAXI</span>';
                        return '<input id="'.$rutas->unidad->numero_unidad.'" checked data-toggle="toggle" data-on='."'".'<i class="fa fa-fw fa-taxi"></i>'."'".' data-off='."'".'<i class="fa fa-fw fa-bus"></i>'."'".' data-onstyle="warning" data-offstyle="primary" data-size="mini" type="checkbox" class="myToggle">';
                    } elseif ($rutas->unidad->tipo_servicio == 'urban') {
                        // return '<span class="label label-default"><i class="fa fa-fw fa-bus text-red"></i>URBAN</span>';
                        return '<input id="'.$rutas->unidad->numero_unidad.'" data-toggle="toggle" data-on='."'".'<i class="fa fa-fw fa-taxi"></i>'."'".' data-off='."'".'<i class="fa fa-fw fa-bus"></i>'."'".' data-onstyle="warning" data-offstyle="primary" data-size="mini" type="checkbox" class="myToggle">';
                    }
                })
                ->addColumn('estatus', function($rutas) {
                    return '<span id="estatusservicio'. $rutas->conductor_id .'" class="label label-info" estatusservicio></span>';
                })
                ->addColumn('ruta', function($rutas) {
                    if ($rutas->unidad->tipo_servicio == 'taxi') {
                        return '<span class="label label-default text-green">TAXI</span>';
                    } elseif ($rutas->unidad->tipo_servicio == 'urban') {
                        if($rutas->recorrido->id != 100) {
                          switch ($rutas->recorrido->id) {
                            case 1:
                            return '<span class="label label-r01">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 2:
                            return '<span class="label label-r02">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 3:
                            return '<span class="label label-r03">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 4:
                            return '<span class="label label-r04">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 5:
                            return '<span class="label label-r05">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 6:
                            return '<span class="label label-r06">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                            case 7:
                            return '<span class="label label-r06">'. strtoupper($rutas->recorrido->nombre) .'</span>';
                                break;
                          }
                        } else {
                            return '<span class="label label-default"></span>';
                        }
                    }
                })
                // ->addColumn('editar', function($rutas) {
                //     return '<a class="btn btn-warning btn-xs" onclick="controlrutas.boton(' . "'" . $rutas->id . "','" . "edit". "'" . ');"><i class="fa fa-fw fa-edit"></i> Asignación</a>';
                // })
                ->addColumn('eliminar', function($rutas) {
                  if(Auth::user()->hasRolesModulo(['ADMINISTRADOR'], 6)) {
                    return '<a id="eruta'.$rutas->conductor_id.'" class="btn btn-danger btn-xs" onclick="controlrutas.eliminar(' . "'" . $rutas->id . "','" . "eliminar". "'" . ');"><i class="fa fa-trash-o"></i></a>';
                  }else {
                    return '<a class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></a>';
                  }

                })
                ->rawColumns(['clave_conductor', 'estatus_sesion_app', 'servicio', 'estatus', 'ruta', 'eliminar']);
                return $table;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        $rutas = ControlRutas::all();
        return $rutas;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            '# Unidad'    => ['data' => 'unidad_id', 'name' => 'unidad_id'],
            'Matricula' => ['data' => 'matricula', 'name' => 'matricula'],
            'Conductor' => ['data' => 'conductor', 'name' => 'conductor'],
            'Clave' => ['data' => 'clave_conductor', 'searchable' => false, 'orderable' => false],
            'Sesión'  => ['data' => 'estatus_sesion_app', 'name' => 'estatus_sesion_app'],
            'Servicio' => ['data' => 'servicio', 'name' => 'servicio'],
            'Estatus servicio'      => ['data' => 'estatus', 'name' => 'estatus'],
            'Recorrido' => ['data' => 'ruta', 'name' => 'ruta'],
            // 'Editar' => ['data' => 'editar', 'searchable' => false, 'orderable' => false],
            'Eliminar' => ['data' => 'eliminar', 'searchable' => false, 'orderable' => false]
        ];
        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { initMap();
             fb(); table.init();}',
            //'buttons' => null,
            //'autoWidth' => true,
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        
    }
}
