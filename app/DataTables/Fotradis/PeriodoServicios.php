<?php

namespace App\DataTables\Fotradis;

use App\Models\Rutas;
use App\Models\Recorridos;
use Yajra\DataTables\Services\DataTable;

class PeriodoServicios extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
        /* ->addColumn('editar_periodo', function($recorrido) {
            return '<a class="btn btn-warning btn-sm" onclick="editarPeriodo('. "'" . $recorrido->nombre . "'," . "'" . $recorrido->periodo_id . "'" . ');"><i class="fa fa-fw fa-edit"></i></a>';
        }) */
        ->addColumn('eliminar_recorrido', function($recorrido) {
            if($recorrido->deleted_at == null)
                return '<a class="btn btn-success btn-sm" onclick="habilitarRecorrido(' . "'" . $recorrido->id . "'" . ',true);"><i class="fa fa-sort-desc"></i> Habilitado</a>';
            else
                return '<a class="btn btn-danger btn-sm" onclick="habilitarRecorrido(' . "'" . $recorrido->id . "'" . ',false);"><i class="fa fa-sort-up"> Inhabilitado</i></a>';
        })
        ->rawColumns([/* 'editar_periodo', */'eliminar_recorrido']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        return Recorridos::leftJoin('dtll_periodosrecorridos','dtll_periodosrecorridos.recorrido_id','recorridos.id')
                          ->withTrashed()
                          ->get(['nombre','fecha_ini','fecha_fin','dtll_periodosrecorridos.id as periodo_id','recorridos.id as id','recorridos.deleted_at as deleted_at']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'Nombre'    => ['data' => 'nombre', 'name' => 'nombre'],
		    //'Fecha inicio'    => ['data' => 'fecha_ini', 'name' => 'fecha_ini'],
            //'Fecha fin'    => ['data' => 'fecha_fin', 'name' => 'fecha_fin'],
            //'Editar Periodo' => ['data' => 'editar_periodo', 'searchable' => false, 'orderable' => false],
            'Habilitar/Inhabilitar'    => ['data' => 'eliminar_recorrido', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }
}
