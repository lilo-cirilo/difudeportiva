<?php

namespace App\DataTables\Fotradis;

use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;

use App\Models\DTLL\Pasajero;
use App\Models\DTLL\ServiciosAdquiridos;

class PasajerosDataTable extends DataTable
{
  /**
   * Build DataTable class.
   *
   * @param mixed $query Results from query() method.
   * @return \Yajra\DataTables\DataTableAbstract
   */
  public function dataTable($query)
  {
    /* $tabla = datatables($query)
        ->addColumn('action', 'pasajeros.action'); */
    return datatables()->eloquent($query)
      ->addColumn('numPasajero', function($pasajero) {
        $aux = "";
        foreach ($pasajero->serviciosadquiridos as $servicio ) {
            if (!(strpos($servicio->numPasajero,'G') !== false) )
                $aux  = $aux.($aux==""?"":", ").$servicio->numPasajero; 
        }
        return $aux;
      })
      ->filterColumn('numPasajero', function($query, $keyword){
            $pasajerosIds = ServiciosAdquiridos::Where('numPasajero','like',"%$keyword%")->get(['pasajero_id']);
            $query->whereIn('dtll_pasajeros.id',$pasajerosIds);
        })
      ->addColumn('servicio', function($pasajero) {
          if ($pasajero->serviciosadquiridos->count() > 1)
            return "AMBOS";
          else {
             if ($pasajero->serviciosadquiridos->first() !== null)
               return $pasajero->serviciosadquiridos->first()->tiposervicio->nombre;
          }
      })
      ->filterColumn('servicio', function($query, $keyword){
            $pasajerosIds = ServiciosAdquiridos::join('dtll_cat_tiposservicio','dtll_cat_tiposservicio.id','dtll_serviciosadquiridos.tiposervicio_id')
                                                ->Where('dtll_cat_tiposservicio.nombre','like',"%$keyword%")->get(['pasajero_id']);
            $query->whereIn('dtll_pasajeros.id',$pasajerosIds);
        })
      ->addColumn('tipo_pasajero', function($pasajero) {
            return $pasajero->serviciosadquiridos->first()->tipopasajero->nombre;
      })
      ->addColumn('fechaIniServ', function($pasajero) {
          foreach ($pasajero->serviciosadquiridos as $servicio ) {
            if($servicio->tiposervicio->nombre === "TAXI") {
              if ($servicio->pagos->last() !== null) 
                return ($servicio->Pagos->last()->fechaIniServ);
            }
          }
      })
      ->addColumn('fechaFinServ', function($pasajero) {
          foreach ($pasajero->serviciosadquiridos as $servicio ) {
            if($servicio->tiposervicio->nombre === "TAXI")
              if ($servicio->Pagos->last() !== null)
                return $servicio->Pagos->last()->fechaFinServ;
          }
      })
      ->addColumn('pagar', function($pasajero) {
          foreach ($pasajero->serviciosadquiridos as $servicio ) {
            if($servicio->tiposervicio->nombre === "TAXI")          //Si no tiene la letra B
                return '<a class="btn btn-success btn-sm" onclick="abrir_modal(\'modal-pagarServicio\', ' . $pasajero->id . ');" ><i class="fa fa-dollar"></i> Pagar </a>';
          }

        //if ($pasajero->servicio == 'TAXI')
        //    return '<a class="btn btn-success btn-xs" onclick="abrir_modal(\'modal-regispasajero\', ' . $pasajero->id . ');" ><i class="fa fa-pencil"></i> Pagar </a>';
      })
      ->addColumn('ver', function($pasajero) {
        //return '<a class="btn btn-warning btn-xs" onclick="abrir_modal(\'modal-regispasajero\', ' . $pasajero->id . ');"><i class="fa fa-pencil"></i> Editar</a>';
        //return '<a href="' . route('registrobenef.editpasajero',['pasajero_id' => $pasajero->id]) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        
        return '<a class="btn btn-info btn-sm" onclick="abrir_modal(\'modal-verPasajero\', ' . $pasajero->id . ');" ><i class="fa fa-fw fa-eye"></i> Ver </a>';
        
        
        //return '<button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="Beneficiarios.abrir_eliminar_beneficiario('.$solicitud->persona->id.',\''.$solicitud->persona->get_nombre_completo().'\')"> <i class="fa fa-ban"></i> Eliminar</button>';
        //return '<a href="' . route('solicitudes.edit',['solicitud_id' => $solicitud->id, 'tipo_solicitud' => $this->tipo_solicitud]) . '" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Editar</a>';
        //return '<a href="' . route('afuncionales.productos.entradas.edit', $entradas->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        // return '<a href="' . route('registrobenef.reptrim') . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
      })
			->addColumn('editar', function($pasajero) {
        // return '<a class="btn btn-success btn-sm" onclick="abrir_modal(\'modal-editarPasajero\', ' . $pasajero->id . ');" ><i class="fa fa-fw fa-pencil"></i> Editar </a>';							   //<----- Para usarse como modal
        return '<a class="btn btn-success btn-sm" href="'.route("registrobenef.geteditpasajero",["pasajero_id" => $pasajero->id]).'" ><i class="fa fa-fw fa-pencil"></i> Editar </a>';				 //<----- Para usarse como página
      })
      ->addColumn('eliminar', function($pasajero) {
        return '<a class="btn btn-danger btn-sm" onclick="abrir_modal(\'modal-EliminarServicio\', ' . $pasajero->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
      })
      ->rawColumns(['pagar','ver', 'editar','eliminar']);
  }

  //Regresa true o false si una subcadena se encuentra en una cadena.
  public function contains ($stringEnLaQueSeBusca, $cadenaABuscar)
  {
    return strpos($stringEnLaQueSeBusca, $cadenaABuscar) === false ? false : true;
  }

  /**
   * Get query source of dataTable.
   *
   * @param \App\User $model
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function query(Pasajero $model)
  {
      // Extraemos una lista de usuarios que sólo recibieron servicio en Guelaguetza
      $pasajerosConUnServicio = ServiciosAdquiridos::withTrashed()
                                ->select('pasajero_id')
                                ->groupBy('pasajero_id')
                                ->having(DB::raw('count(pasajero_id)'),'1')
                                ->get();
      $pasajerosConUnSoloServicioGuelaguetza = ServiciosAdquiridos::withTrashed()
                                                        ->select('pasajero_id')
                                                        ->Where('numPasajero','like','G%')
                                                        ->whereIn('pasajero_id',$pasajerosConUnServicio->pluck('pasajero_id'))
                                                        ->get();

      // Hacemos la consulta general
      return Pasajero::select('dtll_pasajeros.id as id','pe.nombre', 'pe.primer_apellido', 'pe.segundo_apellido', 'pe.curp', 'pe.numero_celular', 'pe.numero_local')
                      ->join('personas AS pe', 'dtll_pasajeros.persona_id','pe.id')
                      ->whereNotIn ('dtll_pasajeros.id', $pasajerosConUnSoloServicioGuelaguetza->pluck('pasajero_id'))
                      ->orderBy('pe.nombre');
  }

  /**
   * Optional method if you want to use html builder.
   *
   * @return \Yajra\DataTables\Html\Builder
   */
  public function html()
  {
      return $this->builder()
                  ->columns($this->getColumns())
                  ->minifiedAjax()
                  //->addAction(['width' => '80px'])
                  ->parameters($this->getBuilderParameters());
  }

  /**
   * Get columns.
   *
   * @return array
   */
  protected function getColumns()
  {
      return [
          //'numPasajero','tiposervicio_id','tipopasajero_id','fechaIniServ','fechaFinServ',
          'numPasajero',
          'nombre de pers'  => ['data' => 'nombre', 'name' => 'pe.nombre'],
          'primer_apellido'  => ['data' => 'primer_apellido', 'name' => 'pe.primer_apellido'],
          'segundo_apellido'  => ['data' => 'segundo_apellido', 'name' => 'pe.segundo_apellido'],
          'curp'  => ['data' => 'curp', 'name' => 'pe.curp'],
          'servicio',
          'tipo_pasajero', 
          'fechaIniServ', 
          'fechaFinServ',
          'número_celular' => ['data' => 'numero_celular', 'name' => 'pe.numero_celular'],
          'número_local'=> ['data' => 'numero_local', 'name' => 'pe.numero_local'],
          // 'producto' => ['data' => 'producto', 'name' => 'producto'],

          'pagar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
          'ver' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
					'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
          'eliminar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
      ];
  }

	protected function decirHolita ()
	{
		alert('hola');
	}

  /**
   * Get columns.
   *
   * @return array
   */
  protected function getBuilderParameters()
  {
		// return '<a class="btn btn-info btn-sm" onclick="abrir_modal(\'modal-verPasajero\', ' . $pasajero->id . ');" ><i class="fa fa-fw fa-eye"></i> Ver </a>';
        
        
        //return '<button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="Beneficiarios.abrir_eliminar_beneficiario('.$solicitud->persona->id.',\''.$solicitud->persona->get_nombre_completo().'\')"> <i class="fa fa-ban"></i> Eliminar</button>';
        //return '<a href="' . route('solicitudes.edit',['solicitud_id' => $solicitud->id, 'tipo_solicitud' => $this->tipo_solicitud]) . '" class="btn btn-info btn-xs"><i class="fa fa-edit"></i> Editar</a>';
        //return '<a href="' . route('afuncionales.productos.entradas.edit', $entradas->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        
				// return '<a href="' . route('registrobenef.reptrim') . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';

    $builderParameters =
      [
        'language' => [
            'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
          ],
        'dom' => 'Btip',
        'buttons' => [
          [
            'extend' => 'reset',
            'text' => '<i class="fa fa-undo"></i> Reiniciar',
            'className' => 'button-dt tool'
          ],
          [
            'extend' => 'reload',
            'text' => '<i class="fa fa-refresh"></i> Recargar',
            'className' => 'button-dt tool'
          ],
          [
            'extend' => 'pdf',
            'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
            'className' => 'button-dt tool'
          ],
					[
						'text' => 'Reporte Inscritos',
            'className' => 'button-dt tool',
						'action' => "function (e, dt, button, config) {
							abrir_modal('modal-repInscritos', 0, e, dt)
						}"
					]
        ],
        //'autoWidth' => true,
        'lengthMenu' => [ 10 ],
        'responsive' => true,
        'columnDefs' => [
            [ 'className' => 'text-center', 'targets' => '_all' ]
          ],
        'processing' => true
      ];

    return $builderParameters;
  }

  /**
   * Get filename for export.
   *
   * @return string
   */
  protected function filename()
  {
      return 'Fotradis\Pasajeros_' . date('YmdHis');
  }
}
