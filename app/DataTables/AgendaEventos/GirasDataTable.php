<?php

namespace App\DataTables\AgendaEventos;

use Modules\AgendaEventos\Entities\Gira;
use Yajra\DataTables\Services\DataTable;

use Illuminate\Support\Facades\Auth;

class GirasDataTable extends DataTable {

    public function dataTable($query) {
			return datatables($query)
			->addIndexColumn()
			->editColumn('fecha', function($gira) {
				if($gira->evento_id) {
					return with(\Carbon\Carbon::parse($gira->fecha))->format('d-m-Y');
				}
			})
			/*->editColumn('fecha', function($gira) {
				if($gira->evento_id) {
					return with(\Carbon\Carbon::parse($gira->fecha))->format('d-m-Y');
				}
			})*/
			->editColumn('estado', function($gira) {
				if($gira->estado) {
					if($gira->estado === 'NUEVA') {
						return '<small class="label label-info">' . $gira->estado . '</small">';
					}
					if($gira->estado === 'EN PROCESO') {
						return '<small class="label label-warning">' . $gira->estado . '</small">';
					}
					if($gira->estado === 'FINALIZADA') {
						return '<small class="label label-success">' . $gira->estado . '</small">';
					}
				}
			})
			->orderColumn('fecha', 'fecha $1')
			->filterColumn('fecha', function($query, $keyword) {
				$query->whereRaw("DATE_FORMAT(fecha, '%d-%m-%Y') like ?", ["%$keyword%"]);
			})
			->filterColumn('municipio', function($query, $keyword) {
				$query->whereRaw("cat_municipios.nombre like ?", ["%$keyword%"]);
			})
			->filterColumn('localidad', function($query, $keyword) {
				$query->whereRaw("cat_localidades.nombre like ? or even_eventos.localidad like ?", ["%$keyword%", "%$keyword%"]);
			})
			->filterColumn('region', function($query, $keyword) {
				$query->whereRaw("cat_regiones.nombre like ?", ["%$keyword%"]);
			})
			->addColumn('seleccionar', function($gira) {
				return '<a class="btn btn-success btn-xs" onclick="Giras.seleccionar(' . "'" . $gira->evento_id . "'" . ', ' . "'" . addslashes( $gira->nombre ) . "', '" . $gira->municipio .  "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
			})
			->addColumn('ficha', function($gira) {
				return '<a class="btn btn-info btn-xs" href="'.route('agenda.fichas.edit', $gira->evento_id). '"><i class="fa fa-sticky-note-o"></i> Generar</a>';
			})
			->addColumn('inversion', function($gira) {
				return '<a class="btn btn-success btn-xs" href="'.route('agenda.inversiones', $gira->municipio_id). '"><i class="fa fa-dollar"></i> Consultar</a>';
			})
			->addColumn('editar', function($gira) {
				if($gira->evento_id) {
					return '<a class="btn btn-warning btn-xs" onclick="Evento.crear_o_editar(' . "'" . $gira->evento_id . "', '" . addslashes( $gira->nombre ) . "', '" . $gira->id_calendario_google ."', '" . $gira->id_evento_google ."'" . ');"><i class="fa fa-pencil"></i> Editar</a>';
				}
			})
			->addColumn('eliminar', function($gira) {
				if($gira->evento_id) {
					$fecha = with(\Carbon\Carbon::parse($gira->fecha))->format('d-m-Y');
					return '<a class="btn btn-danger btn-xs" onclick="Evento.eliminar(' . "'" . $gira->evento_id . "', '" . addslashes( $gira->nombre ) . "', '" .  $fecha . "', '" . $gira->municipio . "', '" . $gira->localidad . "', '" . $gira->id_calendario_google ."', '" . $gira->id_evento_google . "'" . ');"><i class="fa fa-trash"></i> Eliminar</a>';
				}
			})
			->addColumn('consultar', function($gira) {
				if($gira->evento_id) {
					return "<a class='btn btn-info btn-xs' href='" .route('agenda.eventos.show', ['id' => $gira->evento_id])."'><i class='fa fa-eye'></i> Consultar</a>";
				}
			})
			->addColumn('descargarFicha', function($gira) {
				if($gira->estado === 'FINALIZADA') {
					return '<a class="btn bg-navy btn-xs" onclick="imprimirFichaI(' . "'" . $gira->ficha_id . "'" . ');"><i class="fa fa-newspaper-o"></i> Ficha</a>';
				}
			})
			->addColumn('descargarExcel', function($gira) {
				if($gira->evento_id) {
					return '<a class="btn bg-purple btn-xs" onclick="eventoMunicipioExcel(' . "'" . $gira->evento_id . "', '" . addslashes( $gira->nombre ) . "', '" . $gira->id_calendario_google . "', '" . $gira->id_evento_google . "'" . ');"><i class="fa fa-file-excel-o"></i> Excel</a>';
				}
			})
			->rawColumns(['estado', 'seleccionar', 'editar', 'eliminar', 'ficha', 'inversion', 'descargarFicha', 'descargarExcel']);
    }
    
    public function query() {
			//return Auth::user()->persona->empleado->area->id;
			$model = Gira::search(Auth::user()->persona->empleado->area->id)
			//->orderBy('atnc_cat_giras.id','desc')
			;
      return $model;
    }

    public function html() {
        return $this->builder()
					->columns($this->getColumns())
					->minifiedAjax()
                    ->ajax(['data' => 'function(d) { d.table = "giras"; }'])                    
                    ->parameters($this->getBuilderParameters());
    }

    protected function getColumns() {
			$columns = [
					'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
					'hora' => ['data' => 'hora', 'name' => 'even_eventos.hora'],
					'evento' => ['data' => 'nombre', 'name' => 'atnc_cat_giras.nombre'],
					'calendario' => ['data' => 'tipoevento', 'name' => 'even_cat_tiposevento.nombre'],
					'estado' => ['data' => 'estado', 'name' => 'cat_estados.nombre'],
					'municipio' => ['data' => 'municipio', 'name' => 'municipio'],
					'localidad' => ['data' => 'localidad', 'name' => 'localidad'],
                    'región' => ['data' => 'region', 'name' => 'region'],
                    'numero ficha' => ['data' => 'numero_ficha', 'name' => 'even_fichasinfo.numero_ficha']
			];
			
			if($this->tipo === 'modal') {
				$columns['seleccionar'] = ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
			}
			else if(auth()->user()->hasRolesModulo(['ADMINISTRADOR DE EVENTOS'], $this->request->modulo_id)) {
				$columns['editar'] = ['title' => 'Editar Evento', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['eliminar'] = ['title' => 'Eliminar Evento', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['ficha'] = ['title' => 'Generar Ficha', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['inversion'] = ['title' => 'Consultar Inversion', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['descargarFicha'] = ['title' => 'Descargar Ficha', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['descargarExcel'] = ['title' => 'Descargar Excel', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
			}
			else if(auth()->user()->hasRolesModulo(['CAPTURISTA DE FICHAS'], $this->request->modulo_id)) {
				$columns['ficha'] = ['title' => 'Generar Ficha', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['inversion'] = ['title' => 'Consultar Inversion', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['descargarFicha'] = ['title' => 'Descargar Ficha', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['descargarExcel'] = ['title' => 'Descargar Excel', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
			}
			else if(auth()->user()->hasRolesModulo(['ASESOR PRESIDENCIA'], $this->request->modulo_id)) {
				$columns['descargarFicha'] = ['title' => 'Descargar Ficha', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
				$columns['descargarExcel'] = ['title' => 'Descargar Excel', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
			}
			
			return $columns;
	}

    protected function getBuilderParameters() {
		//dd($this->request);
		$builderParameters;
		if( $this->request && auth()->user()->hasRolesModulo(['ADMINISTRADOR DE EVENTOS'], $this->request->modulo_id) ) {
			$builderParameters = [
				'language' => [
					'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
				],
				'dom' => 'Btip',
				//'preDrawCallback' => 'function() { block(); }',
				//'drawCallback' => 'function() { unblock(); }',
				'buttons' => [
					[
						'text' => '<i class="fa fa-plus"></i> Nuevo',
						'action' => 'function ( e, dt, node, config ) { Evento.crear_o_editar(); }',
						'className' => 'button-dt tool modal-gira'
					],
					[
						'extend' => 'excel',
						'text' => '<i class="fa fa-file-excel-o"></i> Excel',
						'className' => 'button-dt tool modal-gira'
					],
					[
						'extend' => 'pdf',
						'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
						'className' => 'button-dt tool modal-gira'
					],
					[
						'extend' => 'reset',
						'text' => '<i class="fa fa-undo"></i> Reiniciar',
						'className' => 'button-dt tool'
					],
					[
						'extend' => 'reload',
						'text' => '<i class="fa fa-refresh"></i> Recargar',
						'className' => 'button-dt tool'
					]
				],
				'lengthMenu' => [ [10], [10] ],
				'responsive' => true,
				'columnDefs' => [
					[ 'className' => 'text-center', 'targets' => '_all' ],
					[ 'visible' => false, 'targets' => 2 ]
				],
				'rowGroup' => [
					//'startRender' => 'function (rows,group) { return group + " <button onclick=\"Evento.crear_o_editar("+rows.context[0].json.data[rows[0][0]].gira_id+",undefined,\'"+rows.context[0].json.data[rows[0][0]].nombre+"\'"+")\" type=\"button\" class=\"btn btn-xs btn-success pull-right\"><i class=\"fa fa-plus\"></i> Agregar</buton>" ; }',
					'dataSrc' => 'nombre'
				],
				'order' => [
						2,
						'asc'
				]
			];
		}else {
			$builderParameters = [
				'language' => [
					'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
				],
				'dom' => 'Btip',
				//'preDrawCallback' => 'function() { block(); }',
				//'drawCallback' => 'function() { unblock(); }',
				'buttons' => [
					[
						'extend' => 'excel',
						'text' => '<i class="fa fa-file-excel-o"></i> Excel',
						'className' => 'button-dt tool modal-gira'
					],
					[
						'extend' => 'pdf',
						'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
						'className' => 'button-dt tool modal-gira'
					],
					[
						'extend' => 'reset',
						'text' => '<i class="fa fa-undo"></i> Reiniciar',
						'className' => 'button-dt tool'
					],
					[
						'extend' => 'reload',
						'text' => '<i class="fa fa-refresh"></i> Recargar',
						'className' => 'button-dt tool'
					]
				],
				'lengthMenu' => [ [10], [10] ],
				'responsive' => true,
				'columnDefs' => [
					[ 'className' => 'text-center', 'targets' => '_all' ],
					[ 'visible' => false, 'targets' => 2 ]
				],
				'rowGroup' => [
					//'startRender' => 'function (rows,group) { return group + " <button onclick=\"Evento.crear_o_editar("+rows.context[0].json.data[rows[0][0]].gira_id+",undefined,\'"+rows.context[0].json.data[rows[0][0]].nombre+"\'"+")\" type=\"button\" class=\"btn btn-xs btn-success pull-right\"><i class=\"fa fa-plus\"></i> Agregar</buton>" ; }',
					'dataSrc' => 'nombre'
				],
				'order' => [
						2,
						'asc'
				]
			];
		}
        
        return $builderParameters;
    }

    protected function filename() {
        return 'Giras_' . date('YmdHis');
    }
}
