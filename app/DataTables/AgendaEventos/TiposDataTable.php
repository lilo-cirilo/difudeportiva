<?php

namespace App\DataTables\AgendaEventos;

use Modules\AgendaEventos\Entities\TipoEvento;
use Yajra\DataTables\Services\DataTable;

class TiposDataTable extends DataTable {
    
    public function dataTable($query) {
        return datatables($query)
            ->addColumn('compartir', function($tipo) {
                return '<a class="btn btn-success btn-xs" onclick="TipoEvento.compartirCalendario(' . "'" . $tipo->id . "', '" . addslashes( $tipo->nombre ) . "', '" . $tipo->id_calendario_google . "'" . ');"><i class="fa fa-share"></i> Compartir</a>';
            })
            ->addColumn('editar', function($tipo) {
                return '<a class="btn btn-warning btn-xs" onclick="TipoEvento.crear_o_editar(' . "'" . $tipo->id . "', '" . addslashes( $tipo->nombre ) . "', '" . $tipo->id_calendario_google . "'" . ');"><i class="fa fa-eye"></i> Editar</a>';    
            })
            ->addColumn('eliminar', function($tipo) {
                return '<a class="btn btn-danger btn-xs" onclick="TipoEvento.eliminar(' . "'" . $tipo->id . "', '" . addslashes( $tipo->nombre ) . "', '" . $tipo->id_calendario_google . "'" . ');"><i class="fa fa-trash"></i> Eliminar</a>';
            })
            ->rawColumns(['compartir','editar','eliminar']);
    }

    public function query() {
        return TipoEvento::where('id', '!=', 0)->whereNotNull('id_calendario_google')->orderBy('nombre','asc');        
    }

    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    protected function getColumns() {
        return [
            'nombre' => ['data' => 'nombre', 'name' => 'nombre'],
            'descripcion' => ['data' => 'descripcion', 'name' => 'descripcion'],
            'compartir' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { TipoEvento.crear_o_editar(); }',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool modal-gira'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        
        return $builderParameters;
    }

    protected function filename() {
        return 'Tipos_' . date('YmdHis');
    }
}
