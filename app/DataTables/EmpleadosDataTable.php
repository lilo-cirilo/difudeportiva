<?php

namespace App\DataTables;

use App\Models\Empleado;
use Yajra\DataTables\Services\DataTable;

class EmpleadosDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)        
        ->addColumn('seleccionar', function($empleado) {
            return '<a class="btn btn-success btn-xs" onclick="seleccionar_empleado(' . "'" . $empleado->id . "'" . ', ' . "'" . $empleado->persona->get_nombre_completo() . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
        })
        ->rawColumns(['seleccionar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = Empleado::search()
        ->select(
            [                
                'empleados.id as id',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'empleados.num_empleado as numero',
                'empleados.rfc as rfc',
                'cat_tiposempleados.tipo as modalidad',
                'cat_areas.nombre as area'
            ]
        );
        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->ajax(['data' => 'function(d) { d.table = "empleados"; }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns = [
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'numero' => ['data' => 'numero', 'name' => 'empleados.num_empleado'],
            'rfc' => ['data' => 'rfc', 'name' => 'empleados.rfc'],
            'modalidad' => ['data' => 'modalidad', 'name' => 'cat_tiposempleados.tipo'],
            'area' => ['data' => 'area', 'name' => 'cat_areas.nombre'],
            //'jefe inmediato' => ['data' => 'area', 'name' => 'jefe'],            
            'seleccionar' => ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
        return $columns;
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => 'http://intradif.oo/bower_components/datatables.net-responsive/js/Spanish.json'
            ],
            'dom' => 'Btip',
            'buttons' => [
                /*[
                    'extend' => 'csv',
                    'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                    'className' => ''
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => ''
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => ''
                ],
                [
                    'extend' => 'print',
                    'text' => '<i class="fa fa-print"></i> Imprimir',
                    'className' => ''*//*,
                    'key' => [
                        'key' => 'p',
                        'ctrlKey' => true
                    ]*/
                //],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            //'autoWidth' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        /*if($this->tipo === 'bienestarcandidatos') {
            $builderParameters['buttons'] =
            [
                'extend' => 'csv',
                'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                'className' => ''
            ];
        }*/

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'empleados_' . date('YmdHis');
    }
}