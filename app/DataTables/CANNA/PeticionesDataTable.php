<?php

namespace App\DataTables\Canna;

use App\Models\StatusSolicitud;
use App\Models\ProgramasSolicitud;
use App\Models\Programa;

use App\DataTables\Afuncionales\PeticionesDataTable as AfuncionalesPeticionesDataTable;
use App\User;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PeticionesDataTable extends AfuncionalesPeticionesDataTable
{
  public function dataTable($query) {
    return datatables($query)
    ->addIndexColumn()
    ->editColumn('fecha', function($peticiones) {
      return with(\Carbon\Carbon::parse($peticiones->fecha))->format('d-m-Y');
    })
    ->filterColumn('fecha', function($query, $keyword) {
      $query->whereRaw("select DATE_FORMAT(fecha,'%d-%m-%Y') like ?", ["%$keyword%"]);
    })
    ->orderColumn('dias transcurridos', 'dias_transcurridos $1')
    ->filterColumn('estatus', function($query, $keyword) {
      if(strtoupper($keyword) === 'NUEVO'){
        $keyword = 'VINCULADO';
      }
      $query->whereRaw("estatus like ?", ["%$keyword%"]);
    })
    ->addColumn('estatus', function($peticiones) {
      $status = '';
      switch ($peticiones->estatus) {
        case "VINCULADO":
        $status = '<span class="label label-success">NUEVO</span>';
        break;
        case "VALIDANDO":
        $status = '<span class="label label-warning">VALIDANDO</span>';
        break;
        case "LISTA DE ESPERA":
        $status = '<span class="label label-info">LISTA DE ESPERA</span>';
        break;
        case "CANCELADO":
        $status = '<span class="label label-danger">CANCELADO</span>';
        break;
        case "RECHAZADO":
        $status = '<span class="label label-danger">RECHAZADO</span>';
        break;
        case "FINALIZADO":
        $status = '<span class="label label-primary">FINALIZADO</span>';
        break;
      }
      return $status;
    })
    ->orderColumn('estatus', 'estatus $1')
    ->addColumn('seguimiento', function($peticiones) {
      return "<a class='btn btn-primary btn-xs' href='".route('canna.peticiones.show', ['id' => $peticiones->programa_solicitud_id])."'><i class='fa fa-arrow-right'></i> Seguimiento</a>";
    })
    ->addColumn('cancelar', function($peticiones) {
      if($peticiones->estatus != "CANCELADO" && $peticiones->estatus != "FINALIZADO" && $peticiones->estatus != "LISTA DE ESPERA"){
        return "<a class='btn btn-danger btn-xs' onclick='cancelar($peticiones->programa_solicitud_id)'><i class='fa fa-ban'></i> Cancelar</a>";
      }
    })
    ->rawColumns(['estatus','seguimiento','cancelar']);
  }

  protected function getColumns() {
      return [
          '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
          'folio' => ['data' => 'folio', 'name' => 'folio'],
          'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
          'remitente' => ['data' => 'remitente', 'name' => 'remitente'],
          'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
          'apoyo' => ['data' => 'apoyo', 'name' => 'apoyo'],
          'cantidad' => ['data' => 'cantidad', 'name' => 'cantidad'],
          'días transcurridos' => ['data' => 'dias_transcurridos', 'name' => 'dias_transcurridos'],
          'estatus' => ['searchable' => true, 'orderable' => true, 'exportable' => true, 'printable' => false],
          'seguimiento' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
      ];
  }
    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
      return ProgramasSolicitud::search('CANNA');
    }

}
