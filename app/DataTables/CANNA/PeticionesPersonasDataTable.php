<?php

namespace App\DataTables\Canna;

use App\Models\SolicitudesPersona;

use App\User;
use Yajra\DataTables\Services\DataTable;

// use App\DataTables\Afuncionales\SolicitantesDataTable;

class PeticionesPersonasDataTable extends DataTable
{
  /**
  * Build DataTable class.
  *
  * @param mixed $query Results from query() method.
  * @return \Yajra\DataTables\DataTableAbstract
  */
  public function dataTable($query)
  {
    return datatables($query)
    ->addIndexColumn()
    ->addColumn('edad', function($persona) {
      return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
    })
    ->orderColumn('edad', '-fecha_nacimiento $1')
    ->filterColumn('edad', function($query, $keyword){
      $edad = strtoupper($keyword);
      if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)){
        preg_match_all('!\d+!', $edad, $valores);
        preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);
        if(count($valores[0]) == 2){
          $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]." AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][1]." ".$valores[0][1]);
        }else{
          $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ".$comparadores[0][0]." ".$valores[0][0]);
        }
      }
    })
    ->addColumn('cancelar', function($solicitud) {
      if($solicitud->programas_solicitud->statusActual() == "VINCULADO" || $solicitud->programas_solicitud->statusActual() == "VALIDANDO") {
        if($solicitud->evaluado === null || $solicitud->evaluado === 1) {
          return '<button class="btn btn-danger btn-xs" data-toggle="tooltip" title="Cancelar" onclick="eliminarBeneficiario('.
          $solicitud->persona->id.',\''.$solicitud->persona->get_nombre_completo().'\')"> <i class="fa fa-ban"></i> Cancelar</button>';
        }
      } elseif($solicitud->programas_solicitud->statusActual() == "LISTA DE ESPERA"){
        if($solicitud->entregado === null) {
          return '<button type="button" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Rechazar" data-target="#rechazar" onclick="setPersona('.
          $solicitud->persona->id .', \''.$solicitud->persona->get_nombre_completo().'\',\'rechazar\')"><i class="fa fa-ban"></i> Cancelar</button>';
        }
      }
    })
    ->rawColumns(['cancelar']);
  }

  /**
  * Get query source of dataTable.
  *
  * @param \App\User $model
  * @return \Illuminate\Database\Eloquent\Builder
  */
  public function query()
  {
    return SolicitudesPersona::join('personas', 'personas.id', '=', 'solicitudes_personas.persona_id')
    ->where('solicitudes_personas.programas_solicitud_id',$this->programas_solicitud_id)
    ->select(
      [
        'solicitudes_personas.id as solicitud_persona',
        'solicitudes_personas.programas_solicitud_id as programas_solicitud_id',
        'personas.id as persona_id',
        'personas.nombre as nombre',
        'personas.primer_apellido as primer_apellido',
        'personas.segundo_apellido as segundo_apellido',
        'personas.fecha_nacimiento as fecha_nacimiento',
      ]
    );
  }


  /**
  * Optional method if you want to use html builder.
  *
  * @return \Yajra\DataTables\Html\Builder
  */
  public function html() {
    return $this->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->ajax(['data' => 'function(d) { d.table = "solicitantes"; }'])
    ->parameters($this->getBuilderParameters());
  }

  /**
  * Get columns.
  *
  * @return array
  */
  protected function getColumns()
  {
    $columns =
    [
      '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
      'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
      'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
      'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
      'edad' => ['data' => 'edad', 'name' => 'edad'],
      'cancelar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
    ];

    return $columns;
  }

  protected function getBuilderParameters(){
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
      ],
      'dom' => 'Btip',
      'preDrawCallback' => 'function() { block(); }',
      'drawCallback' => 'function() { $(\'[data-toggle="tooltip"]\').tooltip(); unblock(); }',
      'buttons' => [
        [
          'extend' => 'reset',
          'text' => '<i class="fa fa-undo"></i> Reiniciar',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'reload',
          'text' => '<i class="fa fa-refresh"></i> Recargar',
          'className' => 'button-dt tool'
        ],
        [
            'extend' => 'excel',
            'text' => '<i class="fa fa-file-excel-o"></i> Excel',
            'className' => 'button-dt tool'
        ],
        [
            'extend' => 'pdf',
            'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
            'className' => 'button-dt tool'
        ]
      ],
      //'autoWidth' => true,
      'lengthMenu' => [ 10 ],
      'responsive' => true,
      'columnDefs' => [
        [ 'className' => 'text-center', 'targets' => '_all' ]
      ]
    ];

    return $builderParameters;
  }

  /**
  * Get filename for export.
  *
  * @return string
  */
  protected function filename()
  {
    return 'Canna/PeticionesPersonas_' . date('YmdHis');
  }
}
