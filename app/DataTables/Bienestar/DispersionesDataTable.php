<?php

namespace App\DataTables\Bienestar;

use App\Traits\Bienestar;

use App\Models\ListaDispersion;

use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Services\DataTable;

class DispersionesDataTable extends DataTable {
    use Bienestar;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha', function($dispersion) {
            return date('d/m/Y h:i:s', strtotime($dispersion->fecha));
        })
        ->filterColumn('listas_dispersiones.fecha', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha, '%d/%m/%Y %h:%i:%s') like ?", ["%$keyword%"]);
        })
        ->addColumn('accion', function($dispersion) {
            $btn = '<a href="' . route('bienestar.dispersiones.show', $dispersion->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-server"></i> Continuar</a >';
            if($dispersion->status === 'EN REVISION' || $dispersion->status === 'ENVIADA' || $dispersion->status === 'CONFIRMADA' || $dispersion->status === 'CANCELADA' || $dispersion->status === 'FINALIZADA') {
                $btn = str_replace(['warning', 'server', 'Continuar'], ['primary', 'eye', 'Consultar'], $btn);
            }
            return $btn;
        })
        ->rawColumns(['accion']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        $model = ListaDispersion::
        
        join('usuarios', 'usuarios.id', '=', 'listas_dispersiones.usuario_id')

        ->join('cat_ejercicios', 'cat_ejercicios.id', '=', 'ejercicio_id')

        ->select(
            [
                'listas_dispersiones.id as id',
                'cat_ejercicios.anio as anio',
                'listas_dispersiones.bimestre as bimestre',
                'listas_dispersiones.status as status',
                'listas_dispersiones.nombre_listado as nombre_listado',
                'usuarios.usuario as usuario',
                'listas_dispersiones.fecha as fecha',
                'listas_dispersiones.importe as importe'
            ]
        );

        if(Auth::user()->hasRolesModulo(['FINANCIERO VALIDADOR'], 2)) {
            $model
            ->orWhere('listas_dispersiones.status', '=', 'ENVIADA')
            ->orWhere('listas_dispersiones.status', '=', 'EN REVISION FINANCIERO')
            ->orWhere('listas_dispersiones.status', '=', 'CONFIRMADA')
            ->orWhere('listas_dispersiones.status', '=', 'RETENIDA')
            ->orWhere('listas_dispersiones.status', '=', 'FINALIZADA')
            ;
        }

        if(Auth::user()->hasRolesModulo(['FINANCIERO DISPERSOR'], 2)) {
            $model
            ->orWhere('listas_dispersiones.status', '=', 'CONFIRMADA')
            ->orWhere('listas_dispersiones.status', '=', 'FINALIZADA')
            ;
        }

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        $columns =
        [
            'año' => ['data' => 'anio', 'name' => 'cat_ejercicios.anio'],
            'bimestre' => ['data' => 'bimestre', 'name' => 'listas_dispersiones.bimestre'],
            'estado' => ['data' => 'status', 'name' => 'listas_dispersiones.status'],
            'oficio' => ['data' => 'nombre_listado', 'name' => 'listas_dispersiones.nombre_listado'],
            'fecha' => ['data' => 'fecha', 'name' => 'listas_dispersiones.fecha'],
            'usuario' => ['data' => 'usuario', 'name' => 'usuarios.usuario'],
            'importe' => ['data' => 'importe', 'name' => 'listas_dispersiones.importe'],
            'accion' => ['data' => 'accion', 'searchable' => false, 'orderable' => false]
        ];

        return $columns;
    }

    protected function getBuilderParameters() {
        $builderParameters =
        [
            'processing' => true,
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'dispersiones_' . date('YmdHis');
    }
}