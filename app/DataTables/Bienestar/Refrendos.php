<?php

namespace App\DataTables\Bienestar;

use App\Models\AniosPrograma;
use App\Models\PersonasPrograma;
use Yajra\DataTables\Services\DataTable;

class Refrendos extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        if ($this->tipo=='detalles') {
            return datatables($query)
            ->editColumn('fecha_nacimiento', function($candidato) {
                return $candidato->fecha_nacimiento ? date("d/m/Y", strtotime($candidato->fecha_nacimiento)) : '';
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                    $sql = "DATE_FORMAT(personas.fecha_nacimiento, '%d/%m/%Y') like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->addColumn('status',function($beneficiario){
                $estado = '<div class="sin_refrendar col-sm-12 col-md-12 col-lg-12 col-xl-12">SIN REFRENDAR</div>';
                if ($beneficiario->motivo_id!=null && $beneficiario->motivo_id!=3)
                    $estado='<div class="baja col-sm-12 col-md-12 col-lg-12 col-xl-12">BAJA</div>';
                if($beneficiario->motivo_id==3)
                    $estado='<div class="refrendado col-sm-12 col-md-12 col-lg-12 col-xl-12">REFRENDADO</div>';
                return $estado;
            })
            ->filterColumn('status', function($query, $keyword) {
                    $sql=["personas_programas.motivo_id","null"];
                    if ($keyword=='BAJA')
                        $sql=[['personas_programas.motivo_id','!=',null],['personas_programas.motivo_id','!=','3']];
                    if ($keyword=='REFRENDADO')
                        $sql=['personas_programas.motivo_id',3];
                    $query->where($sql);
            })->rawColumns(['status']);
        }

        return datatables($query)
        ->addColumn('informacion',function($ejercicio){
            return '<a href="'.route('bienestar.refrendos.show',$ejercicio->id).'" class="btn btn-info btn-xs"><i class="fa fa-eye"> Informacion </a>';
        })
        ->addColumn('continuar',function($ejercicio){
            if ($ejercicio->anio<date('Y'))
                return '<a href="'.route('bienestar.refrendos.edit',$ejercicio->id).'" class="btn btn-xs btn-warning disabled"><i class="fa fa-pencil"> Continuar</a>';
            return '<a href="'.route('bienestar.refrendos.edit',$ejercicio->id).'" class="btn btn-xs btn-warning"><i class="fa fa-pencil"> Continuar</a>';
        })
        ->addColumn('finalizar',function($ejercicio){
            if ($ejercicio->anio<date('Y'))
                return '<a class="btn btn-xs btn-danger disabled"><i class="fa fa-check"> Finalizar</a>';
            return '<a class="btn btn-xs btn-danger"><i class="fa fa-check"> Finalizar</a>';
        })
        ->addColumn('estado',function($ejercicio){
            if ($ejercicio->anio<date('Y'))
                return 'Finaliada';
            return 'En proceso';
        })
        ->rawColumns(['informacion','continuar','finalizar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(){
        if($this->tipo=='detalles'){
            return PersonasPrograma::withTrashed()->datosPersonales()->where('anios_programa_id',$this->aniosPrograma)
            ->select(['personas_programas.id','personas_programas.motivo_id','personas_programas.persona_id','foliounico',
            'personas.nombre as nombre','primer_apellido','segundo_apellido','fecha_nacimiento','curp',
            'cat_discapacidades.nombre as discapacidad','cat_regiones.nombre as region','cat_distritos.nombre as distrito',
            'cat_municipios.nombre as municipio','cat_localidades.nombre as localidad']);
        }
        return AniosPrograma::join('cat_ejercicios','cat_ejercicios.id','=','anios_programas.ejercicio_id')
        ->where('anios_programas.programa_id',5)
        ->select(['anios_programas.id as id','anio']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'Id'        => ['data' => 'id', 'name' => 'id' ],
            'Año'      => ['data' => 'anio', 'name' => 'cat_ejercicios.anio'],
            'Estado'    => ['data' => 'estado', 'name' => 'estado'],
            'Consultar' => ['data' => 'informacion', 'orderable' => false, 'searchable' => false],
            'Continuar' => ['data' => 'continuar','orderable' => false, 'searchable' => false],
            'Finalizar' => ['data' => 'finalizar','orderable' => false, 'searchable' => false]
        ];
        if($this->tipo === 'detalles') {
            $columns =[
                'Folio unico'         => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
    			'Discapacidad'        => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
    			'Nombre'              => ['data' => 'nombre', 'name' => 'personas.nombre'],
    			'Primer apellido'     => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
    			'Segundo apellido'    => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
    			'Fecha de nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
    			'CURP'                => ['data' => 'curp', 'name' => 'personas.curp'],
    			'Region'              => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
    			'Distrito'            => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
    			'Municipio'           => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
    			//'Locialidad'          => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                'Estatus'             => ['data' => 'status']
            ];
        }
        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => 'http://intradif.oo:90/bower_components/datatables.net-responsive/js/Spanish.json'
            ],
            'dom' => 'Btip',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            //'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'ejercicios_' . date('YmdHis');
    }
}
