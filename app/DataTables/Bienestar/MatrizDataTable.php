<?php

namespace App\DataTables\Bienestar;

use App\Traits\Bienestar;

use Yajra\DataTables\Services\DataTable;

use Illuminate\Support\Facades\DB;

use App\Models\PersonasPrograma;

use Illuminate\Support\Facades\Auth;

class MatrizDataTable extends DataTable {
    use Bienestar;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->addColumn('edad', function($persona) {
          return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->orderColumn('edad', '-fecha_nacimiento $1')
        ->filterColumn('edad', function($query, $keyword) {
          $edad = strtoupper($keyword);
          if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)) {
            preg_match_all('!\d+!', $edad, $valores);
            preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);
            if(count($valores[0]) === 2) {
              $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0] . " AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][1] . " " . $valores[0][1]);
            }
            else {
              $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0]);
            }
          }
        })
        ->editColumn('tutor_fecha_nacimiento', function($persona) {
            return $persona->tutor_fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->tutor_fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('tutor.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(tutor.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })

        ->editColumn('estatus', function($personaprograma) {
            if($personaprograma->trashed()) {
                return '<span class="label label-danger">INACTIVO</span>';
            }
            else {
                return '<span class="label label-success">ACTIVO</span>';
            }
        })
        ->filterColumn('personas_programas.deleted_at', function($query, $keyword) {
            if(strtoupper($keyword) === 'ACTIVO') {
                $query->whereRaw("personas_programas.deleted_at is null");
            }
            if(strtoupper($keyword) === 'INACTIVO') {
                $query->whereRaw("personas_programas.deleted_at is not null");
            }
        })
        ->addColumn('total', function($personaprograma) {
            return $personaprograma->bimestre1 + $personaprograma->bimestre2 + $personaprograma->bimestre3 + $personaprograma->bimestre4 + $personaprograma->bimestre5 + $personaprograma->bimestre6;
        })
        ->rawColumns(['estatus']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $model = PersonasPrograma::withTrashed()
        ->search($this->programa, $this->anio)
        ->select(
            [
                'bienestarpersonas.posicion as posicion',
                'bienestarpersonas.foliounico as foliounico',
                'cat_discapacidades.nombre as discapacidad',
                'personas_programas.deleted_at as estatus',
                //'cat_ejercicios.anio as anio',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',
                'personas.genero as genero',
                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',

                'tutor.nombre as tutor_nombre',
                'tutor.primer_apellido as tutor_primer_apellido',
                'tutor.segundo_apellido as tutor_segundo_apellido',
                'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                'tutor.curp as tutor_curp',
                'tutor.genero as tutor_genero',

                'cuentasbancos.num_cuenta as num_cuenta',

                'tutor_cat_regiones.nombre as tutor_region',
                'tutor_cat_distritos.nombre as tutor_distrito',
                'tutor_cat_municipios.nombre as tutor_municipio',
                'tutor_cat_localidades.nombre as tutor_localidad',

                'personas_programas.deleted_at as deleted_at',
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 1 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre1'),
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 2 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre2'),
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 3 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre3'),
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 4 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre4'),
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 5 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre5'),
                DB::raw('IFNULL((select ld.importe from personas_programas pp left join bienestardispersiones bienestarpersonas on pp.id = bienestarpersonas.personas_programa_id left join listas_dispersiones ld on bienestarpersonas.lista_id = ld.id where ld.bimestre = 6 and personas_programas.id = pp.id and bienestarpersonas.pagado = 1), 0) as bimestre6')
            ]
        )
        ->whereNotNull('bienestarpersonas.posicion');

        if(Auth::user()->hasRolesModulo(['ENLACE CAÑADA'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'CAÑADA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE COSTA'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'COSTA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE ISTMO'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'ISTMO');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE MIXTECA'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'MIXTECA');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE PAPALOAPAM'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'PAPALOAPAM');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE SIERRA NORTE'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'SIERRA NORTE');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE SIERRA SUR'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'SIERRA SUR');
        }
        if(Auth::user()->hasRolesModulo(['ENLACE VALLES CENTRALES'], 2)) {
            $model->where('cat_regiones.nombre', '=', 'VALLES CENTRALES');
        }

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        //->addAction(['width' => '80px'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns =
        [
            'foliounico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
            'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion'],
            'discapacidad' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
            'estatus' => ['data' => 'estatus', 'name' => 'personas_programas.deleted_at'],
            'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
            'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
            'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
            'fecha_nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
            'edad' => ['data' => 'edad', 'name' => 'edad'],
            'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
            'genero' => ['data' => 'genero', 'name' => 'personas.genero'],

            'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
            'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
            'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
            'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],

            'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
            'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
            'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
            'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
            'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
            'genero del tutor' => ['data' => 'tutor_genero', 'name' => 'tutor.genero'],

            'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],

            'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
            'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
            'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
            'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre'],

            'bimestre1' => ['data' => 'bimestre1', 'name' => 'bimestre1', 'searchable' => false, 'orderable' => true],
            'bimestre2' => ['data' => 'bimestre2', 'name' => 'bimestre2', 'searchable' => false, 'orderable' => true],
            'bimestre3' => ['data' => 'bimestre3', 'name' => 'bimestre3', 'searchable' => false, 'orderable' => true],
            'bimestre4' => ['data' => 'bimestre4', 'name' => 'bimestre4', 'searchable' => false, 'orderable' => true],
            'bimestre5' => ['data' => 'bimestre5', 'name' => 'bimestre5', 'searchable' => false, 'orderable' => true],
            'bimestre6' => ['data' => 'bimestre6', 'name' => 'bimestre6', 'searchable' => false, 'orderable' => true],
            'total' => ['data' => 'total', 'name' => 'total', 'searchable' => false, 'orderable' => true]
        ];

        if($this->tipo === 'normal') {
            //$columns['convertir'] = ['data' => 'convertir', 'name' => 'convertir', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            //$columns['consultar'] = ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            //$columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }

        /*if($this->tipo === 'bienestarcandidatos') {
            $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            $columns['seleccionar'] = ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }*/

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters()
    {
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'csv',
                    'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                    'className' => ''
                ],
                /*[
                    'extend' => 'csv',
                    'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                    'className' => ''
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => ''
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => ''
                ],
                [
                    'extend' => 'print',
                    'text' => '<i class="fa fa-print"></i> Imprimir',
                    'className' => ''*//*,
                    'key' => [
                        'key' => 'p',
                        'ctrlKey' => true
                    ]*/
                //],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'scrollX' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'rowGroup' => [
                'dataSrc' => 'posicion'
            ]
        ];

        /*if($this->tipo === 'bienestarcandidatos') {
            $builderParameters['buttons'] =
            [
                'extend' => 'csv',
                'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                'className' => ''
            ];
        }*/

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'matriz_' . date('YmdHis');
    }
}