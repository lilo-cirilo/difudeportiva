<?php

namespace App\DataTables\Bienestar;

use App\Traits\Bienestar;

use Illuminate\Support\Facades\Auth;

use App\Models\ListaDispersion;
use App\Models\BienestarDispersion;
use Yajra\DataTables\Services\DataTable;

class Dispersiones extends DataTable {
    use Bienestar;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
    */
    
    protected $actions = ['print', 'excel', 'pdf', 'MyPdf', 'MyExcel', 'MyPrint'];

    // public function __construct(Datatables $datatables, Factory $viewFactory)
    // {
    //   $this->datatables  = $datatables;
    //   $this->viewFactory = $viewFactory;
    // }

    public function dataTable($query) {
        if($this->tipo === 'detalles') {
            return datatables($query)
            ->addColumn('accion',function($beneficiario) {
                //return '<a onclick="pago_exito(' . $beneficiario->idp . ')" class="btn btn-success btn-xs"><i class="fa fa-check"> Exitoso</i></a><a onclick="pago_fallo(' . $beneficiario->idp . ')" class="btn btn-danger btn-xs"><i class="fa fa-warning"></i> Fallido</a>';
            })
            /*->editColumn('pagado', function($beneficiario) {
                if($beneficiario->pagado === 0) {
                    return '<span class="label label-warning"> Pago pendiente</span>';
                }
                if($beneficiario->pagado === 1) {
                    return '<span class="label label-success"> Pago exitoso</span>';
                }
                if($beneficiario->pagado === 2) {
                    return '<span class="label label-danger"> Error en el pago</span>';
                }
            })*/

            ->editColumn('pagado', function($beneficiario) {
                if($beneficiario->pagado === 0) {
                    return '<span class="label label-warning"> PENDIENTE</span>';
                }
    
                if($beneficiario->pagado === 1) {
                    return '<span class="label label-success"> EXITOSO</span>';
                }
    
                if($beneficiario->pagado === 2) {
                    return '<span class="label label-danger"> ERROR</span>';
                }
            })
            ->filterColumn('bienestardispersiones.pagado', function($query, $keyword) {
                $pagado = strtoupper($keyword);
    
                if($pagado === 'PENDIENTE') {
                    $query
                    ->where('bienestardispersiones.pagado', '=', '0');
                }
    
                if($pagado === 'EXITOSO') {
                    $query
                    ->where('bienestardispersiones.pagado', '=', '1');
                }
    
                if($pagado === 'ERROR') {
                    $query
                    ->where('bienestardispersiones.pagado', '=', '2');
                }
            })

            ->editColumn('num_cuenta', function($beneficiario) {
                return '000000000' . $beneficiario->num_cuenta;
            })
            ->addColumn('tutor_nombre_completo', function($beneficiario) {
                return $beneficiario->tutor_nombre . ',' . $beneficiario->tutor_primer_apellido . '/' .  $beneficiario->tutor_segundo_apellido;
            })
            ->editColumn('fecha_nacimiento', function($persona) {
                return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
            })
            ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->editColumn('tutor_fecha_nacimiento', function($persona) {
                return $persona->tutor_fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->tutor_fecha_nacimiento))->format('d-m-Y') : '';
            })
            ->filterColumn('tutor.fecha_nacimiento', function($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(tutor.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->rawColumns(['pagado']);
        }

        return datatables($query)
        ->editColumn('fecha', function($dispersion) {
            return date("d/m/Y h:i:s", strtotime($dispersion->fecha));//with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y');
        })
        ->filterColumn('fecha', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(fecha, '%d/%m/%Y %h:%i:%s') like ?", ["%$keyword%"]);
        })
        ->addColumn('accion', function($dispersion) {
            $btn = '<a href="'.route('bienestar.dispersiones.show',$dispersion->id).'" class="btn btn-warning btn-xs"><i class="fa fa-server"></i> Continuar</a >';
            if($dispersion->status === 'EN REVISION' || $dispersion->status === 'ENVIADA' || $dispersion->status === 'CONFIRMADA' || $dispersion->status === 'CANCELADA' || $dispersion->status === 'FINALIZADA') {
                $btn = str_replace(['warning', 'server', 'Continuar'], ['primary', 'eye', 'Consultar'], $btn);
            }
            return $btn;
        })
        ->rawColumns([/*'editar', 'enviar',*/ 'accion']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        if ($this->tipo === 'detalles') {

            /*return ListaDispersion::where('id', '=', 21)
            ->join('bienestardispersiones', 'bienestardispersiones.lista_id', '=', 'listas_dispersiones.id')
            ;*/

            return BienestarDispersion::Search($this->programa, $this->anio)
            ->join('listas_dispersiones','listas_dispersiones.id','=','lista_id')
            //->leftjoin('cat_discapacidades', 'cat_discapacidades.id', '=', 'discapacidad_id' )
            //->join('cuentasbancos','cuentasbancos.id','=','cuenta_id')
            ->where('lista_id', $this->lista->id)
            ->distinct('personas_programas.id')
            ->select(
                /*['bienestardispersiones.id as idp','personas_programas.id',
            'foliounico','personas.nombre','primer_apellido','segundo_apellido',
            'observacion','curp','cat_discapacidades.nombre as discapacidad',
            'cat_regiones.nombre as region','pagado','cat_municipios.nombre as municipio','cuentasbancos.num_cuenta']*/
            [
                //'personas_programas.id as personas_programa_id',
                //'cuentasbancos.id as cuenta_id',

                'personas_programas.id as id',
                'bienestarpersonas.id as bienestarpersonas_id',
                'bienestarpersonas.posicion as posicion',
                'bienestarpersonas.foliounico as foliounico',
                'cat_discapacidades.nombre as discapacidad',
                //'personas_programas.deleted_at as estatus',
                //'cat_ejercicios.anio as anio',
                'personas.id as persona_id',
                'personas.nombre as nombre',
                'personas.primer_apellido as primer_apellido',
                'personas.segundo_apellido as segundo_apellido',
                'personas.fecha_nacimiento as fecha_nacimiento',
                'personas.curp as curp',

                'cat_regiones.nombre as region',
                'cat_distritos.nombre as distrito',
                'cat_municipios.nombre as municipio',
                'cat_localidades.nombre as localidad',

                /*'t.nombre as tutor_nombre',
                't.primer_apellido as tutor_primer_apellido',
                't.segundo_apellido as tutor_segundo_apellido',
                'cuentasbancos.num_cuenta as num_cuenta',*/
                
                'tutor.nombre as tutor_nombre',
                'tutor.primer_apellido as tutor_primer_apellido',
                'tutor.segundo_apellido as tutor_segundo_apellido',
                'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                'tutor.curp as tutor_curp',

                'cuentasbancos.num_cuenta as num_cuenta',

                'tutor_cat_regiones.nombre as tutor_region',
                'tutor_cat_distritos.nombre as tutor_distrito',
                'tutor_cat_municipios.nombre as tutor_municipio',
                'tutor_cat_localidades.nombre as tutor_localidad',

                'bienestardispersiones.pagado as pagado',
                'bienestardispersiones.observacion as observacion'
            ]
            )
            //->where('cat_regiones.nombre', '=', 'COSTA')
            ;
        }

        return ListaDispersion::search('')
        ->join('cat_ejercicios','cat_ejercicios.id','=','ejercicio_id')
        ->select(['listas_dispersiones.id','anio','nombre_listado','bimestre','importe','fecha','usuarios.usuario','status']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(){
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(){
        $columns =
        [
            'Nombre'    => ['data' => 'nombre_listado', 'name' => 'nombre_listado'],
            'Año'       => ['data' => 'anio', 'name' => 'cat_ejercicios.anio'],
			'Bimestre'  => ['data' => 'bimestre', 'name' => 'bimestre'],
			'Importe'   => ['data' => 'importe', 'name' => 'importe'],
			'Fecha'     => ['data' => 'fecha', 'name' => 'fecha'],
			'Usuario'    => ['data' => 'usuario', 'name' => 'usuarios.usuario'],
			'Estado'    => ['data' => 'status', 'name' => 'status'],
			'Accion'    => ['data' => 'accion', 'searchable' => false, 'orderable' => false]
        ];

        if ($this->tipo=='detalles') {
            /*$columns =
            [
                'Folio unico'      => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
    			'Discapacidad'     => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
    			'Nombre'           => ['data' => 'nombre', 'name' => 'personas.nombre'],
    			'Primer apellido'  => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
    			'Segundo apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
    			'CURP'             => ['data' => 'curp', 'name' => 'personas.curp' ],
    			'Region'           => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
    			'Municipio'        => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'Numero de Cuenta' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],
    			'Observacion'      => ['data' => 'observacion', 'name' => 'observacion']
    			'Pago'             => ['data' => 'accion', 'orderable' => false, 'searchable' => false]
            ];*/

            $columns =
            [
                //'ppid' => ['data' => 'personas_programa_id', 'name' => 'bienestarpersonas.foliounico'],
                //'cid' => ['data' => 'cuenta_id', 'name' => 'bienestarpersonas.foliounico'],

                //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
                'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
                'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
                'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
                //'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
                'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
                'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
    
                'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre', 'printable' => false],
                'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre', 'printable' => false],
                'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre', 'printable' => false],
                
                'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
                'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
                'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
                'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
                'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
    
                'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],
    
                'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
                'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre', 'printable' => false],
                'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre', 'printable' => false],
                'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre', 'printable' => false],

                'pagado' => ['data' => 'pagado', 'name' => 'bienestardispersiones.pagado', 'exportable' => false, 'printable' => false],
                'observacion' => ['data' => 'observacion', 'name' => 'bienestardispersiones.observacion', 'printable' => false]
            ];

            if(Auth::user()->hasRolesModulo(['FINANCIERO DISPERSOR', 'FINANCIERO VALIDADOR'], 2)) {
                $columns =
                [
                    //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
                    'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
                    //'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
                    //'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
                    //'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
                    'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                    'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                    'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                    'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
                    /*'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
        
                    'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                    'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                    'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                    'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],*/
                    
                    'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
                    'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
                    'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
                    'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
                    'tutor_nombre_completo'    => ['data' => 'tutor_nombre_completo', 'searchable' => false, 'orderable' => false],
                    //'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
        
                    'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],
        
                    'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
                    'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                    /*'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
                    'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
                    'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre'],*/
    
                    'pagado' => ['data' => 'pagado', 'name' => 'bienestardispersiones.pagado', 'exportable' => false, 'printable' => false],
                    'observacion' => ['data' => 'observacion', 'name' => 'bienestardispersiones.observacion', 'exportable' => false, 'printable' => false]
                ];
            }
        }

        return $columns;
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters(){
        $builderParameters =
        [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ],
            'autoWidth' => true,
            'lengthMenu' => [ 10 ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
        if ($this->tipo === 'detalles') {
            $builderParameters['buttons'] = [
                /*[
                    'extend' => 'export',
                    'text'    => 'Exportar actual',
                    'fade' => true,
                    'autoclose' => true
                ],*/
                // [
                //     'extend' => 'collection',
                //     'text'    => 'Exportar todo',
                //     'buttons' => [
                //         ['extend' => 'MyExcel','text' => '<i class="fa fa-file-excel-o"></i> Excel','className'=>'col-md-12'],
                //         ['extend' => 'MyPdf','text' => '<i class="fa fa-file-pdf-o"></i> PDF','className'=>'col-md-12'],
                //         ['extend' => 'MyPrint','text' => '<i class="fa fa-print"></i> Imprimir todo','className'=>'col-md-12']
                //     ],
                //     'fade' => true,
                //     'autoclose' => true
                // ],
                //'MyPrint',
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => ''
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => ''
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => ''
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => ''
                ]
            ];
        }
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        if($this->tipo === 'detalles') {
            return 'listadispersion_' . date('YmdHis');
            //return "Lista dispersion {$this->lista->nombre}";
        }
        
        return 'listas de dispersion_' . date('YmdHis');
    }
    
    public function render($view, $data = [], $mergeData = []) {
        
        if($this->request()->ajax() && $this->request()->wantsJson()) {
            return app()->call([$this, 'ajax']);
        }
        
        if($action = $this->request()->get('action') and in_array($action, $this->actions)) {
            if($action === 'print' || $action === 'MyPrint') {
                return app()->call([$this, 'printPreview']);
            }
            
            return app()->call([$this, $action]);
        }
        
        return view($view, $data, $mergeData)->with($this->dataTableVariable, $this->getHtmlBuilder());
    }
    
    protected function getAjaxResponseData() {
        $this->request()->merge(['length' => -1]);
        
        //if($action = $this->request()->get('action') === 'MyPrint') {
            $this->request()->merge(['search' => '']);
        //}
        
        $response = app()->call([$this, 'ajax']);
        
        $data = $response->getData(true);
        
        return $data['data'];
    }

    public function snappyPdf() {
        $data = $this->getDataForPrint();

        $snappy = app('snappy.pdf.wrapper');

        $dispersion = ListaDispersion::where('id', '=', $this->lista->id)->first();

        $header = view('bienestar::reportes.header', compact('dispersion'));

        $footer = view('bienestar::reportes.footer');

        $snappy
        ->setOption('header-html', $header)
        ->setOption('footer-html', $footer)
        ;

        //$snappy->setOption('footer-center', 'Page [page]');

        $snappy->setOptions([
            'margin-left' => '10mm',
            'margin-right' => '10mm',
            'margin-top' => '35',
            'margin-bottom' => '25'
        ])
        ->setOrientation('landscape')
        ;

        $snappy->setTimeout(600);
        
        return $snappy
        //->loadView($this->printPreview, compact('data'))
        ->loadView('bienestar::reportes.lista_dispersion', compact('data'))
        ->download($this->getFilename() . '.pdf');
    }
}