<?php

namespace App\DataTables\Bienestar;

use Yajra\DataTables\Services\DataTable;

class PersonasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
    }

    protected function getBuilderParameters() {
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
    }
}