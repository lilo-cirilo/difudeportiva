<?php

namespace App\DataTables\Bienestar;

use App\Models\BienestarPersona;

use Illuminate\Support\Facades\Auth;

use Yajra\DataTables\Services\DataTable;

class SolicitantesDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha_nacimiento', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->editColumn('tutor_fecha_nacimiento', function($persona) {
            return $persona->tutor_fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->tutor_fecha_nacimiento))->format('d-m-Y') : '';
        })
        ->filterColumn('tutor.fecha_nacimiento', function($query, $keyword) {
            $query->whereRaw("DATE_FORMAT(tutor.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
        })
        ->editColumn('genero', function($persona) {
            if($persona->genero === 'M') {
                return 'MASCULINO';
            }
            
            if($persona->genero === 'F') {
                return 'FEMENINO';
            }
        })

        /*->addColumn('edad', function($persona) {
            return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
        })
        ->orderColumn('edad', '-personas.fecha_nacimiento $1')
        ->filterColumn('edad', function($query, $keyword) {
            $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) ", ["%$keyword%"]);
        })*/

        ->filterColumn('personas.genero', function($query, $keyword) {
            if($keyword === 'MASCULINO') {
                $query
                ->where('personas.genero', '=', 'M');
            }
            
            if($keyword === 'FEMENINO') {
                $query
                ->where('personas.genero', '=', 'F');
            }
        })
        ->editColumn('tutor_genero', function($persona) {
            if($persona->tutor_genero === 'M') {
                return 'MASCULINO';
            }
            
            if($persona->tutor_genero === 'F') {
                return 'FEMENINO';
            }
        })
        ->filterColumn('tutor.genero', function($query, $keyword) {
            if($keyword === 'MASCULINO') {
                $query
                ->where('tutor.genero', '=', 'M');
            }
            
            if($keyword === 'FEMENINO') {
                $query
                ->where('tutor.genero', '=', 'F');
            }
        })
        ->addColumn('convertir', function($solicitante) {
            return '<a class="btn btn-success btn-xs" onclick="solicitante_convertir(' . "'" . $solicitante->id . "'" . ", '" . $solicitante->foliounico . "'" . ", '" . $solicitante->persona->nombrecompleto() . "'" . ');"><i class="fa fa-mail-forward"></i> Convertir a Beneficiario</a>';
        })
        ->addColumn('consultar', function($solicitante) {
            return '<a href="' . route('bienestar.solicitantes.show', $solicitante->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
        })
        ->addColumn('editar', function($solicitante) {
            return '<a href="' . route('bienestar.solicitantes.edit', $solicitante->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('cuenta', function($solicitante) {
            return '<a class="btn btn-warning btn-xs" onclick="abrir_modal_cuenta(\'' .route('bienestar.bancos.edit', ['candidato_id' => $solicitante->id]) . '\');"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->rawColumns(['convertir', 'consultar', 'editar', 'cuenta']);
    }
    
    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        if($this->tipo === 'normal') {
            $model = BienestarPersona::Solicitantes()
            ->select(
                [
                    'bienestarpersonas.id as id',
                    'bienestarpersonas.foliounico as foliounico',
                    'cat_discapacidades.nombre as discapacidad',

                    'personas.id as persona_id',
                    'personas.nombre as nombre',
                    'personas.primer_apellido as primer_apellido',
                    'personas.segundo_apellido as segundo_apellido',
                    'personas.fecha_nacimiento as fecha_nacimiento',
                    'personas.curp as curp',
                    'personas.genero as genero',

                    'cat_regiones.nombre as region',
                    'cat_distritos.nombre as distrito',
                    'cat_municipios.nombre as municipio',
                    'cat_localidades.nombre as localidad',

                    'tutor.nombre as tutor_nombre',
                    'tutor.primer_apellido as tutor_primer_apellido',
                    'tutor.segundo_apellido as tutor_segundo_apellido',
                    'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                    'tutor.curp as tutor_curp',
                    'tutor.genero as tutor_genero',

                    'cuentasbancos.num_cuenta as num_cuenta',

                    'tutor_cat_regiones.nombre as tutor_region',
                    'tutor_cat_distritos.nombre as tutor_distrito',
                    'tutor_cat_municipios.nombre as tutor_municipio',
                    'tutor_cat_localidades.nombre as tutor_localidad'
                ]
            );

            $rol_enlace = 0;
            $rol_enlace_array = [];
    
            if(Auth::user()->hasRolesModulo(['ENLACE CAÑADA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'CAÑADA');
                //$model->where('cat_regiones.nombre', '=', 'CAÑADA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE COSTA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'COSTA');
                //$model->where('cat_regiones.nombre', '=', 'COSTA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE ISTMO'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'ISTMO');
                //$model->where('cat_regiones.nombre', '=', 'ISTMO');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE MIXTECA'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'MIXTECA');
                //$model->where('cat_regiones.nombre', '=', 'MIXTECA');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE PAPALOAPAM'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'PAPALOAPAM');
                //$model->where('cat_regiones.nombre', '=', 'PAPALOAPAM');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE SIERRA NORTE'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'SIERRA NORTE');
                //$model->where('cat_regiones.nombre', '=', 'SIERRA NORTE');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE SIERRA SUR'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'SIERRA SUR');
                //$model->where('cat_regiones.nombre', '=', 'SIERRA SUR');
            }
            if(Auth::user()->hasRolesModulo(['ENLACE VALLES CENTRALES'], 2)) {
                $rol_enlace = 1;
                array_push($rol_enlace_array, 'VALLES CENTRALES');
                //$model->where('cat_regiones.nombre', '=', 'VALLES CENTRALES');
            }
    
            if($rol_enlace === 1) {
                $model->whereIn('cat_regiones.nombre', $rol_enlace_array);
            }
        }

        if($this->tipo === 'bancos') {            
            $model = BienestarPersona::Bancos($this->ejercicio_id)
            ->select(
                [
                    'bienestarpersonas.id as id',
                    'bienestarpersonas.foliounico as foliounico',
                    'cat_discapacidades.nombre as discapacidad',

                    'personas.id as persona_id',
                    'personas.nombre as nombre',
                    'personas.primer_apellido as primer_apellido',
                    'personas.segundo_apellido as segundo_apellido',
                    'personas.fecha_nacimiento as fecha_nacimiento',
                    'personas.curp as curp',

                    'cat_regiones.nombre as region',
                    'cat_distritos.nombre as distrito',
                    'cat_municipios.nombre as municipio',
                    'cat_localidades.nombre as localidad',
    
                    'tutor.nombre as tutor_nombre',
                    'tutor.primer_apellido as tutor_primer_apellido',
                    'tutor.segundo_apellido as tutor_segundo_apellido',
                    'tutor.fecha_nacimiento as tutor_fecha_nacimiento',
                    'tutor.curp as tutor_curp',
    
                    'cuentasbancos.num_cuenta as num_cuenta',
    
                    'tutor_cat_regiones.nombre as tutor_region',
                    'tutor_cat_distritos.nombre as tutor_distrito',
                    'tutor_cat_municipios.nombre as tutor_municipio',
                    'tutor_cat_localidades.nombre as tutor_localidad'
                ]
            );
        }
        
        return $model;
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this
        ->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        if($this->tipo === 'normal') {
            $columns =
            [
                //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
                'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
                //'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
                'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
                //'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
                'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
                //'edad' => ['data' => 'edad', 'name' => 'edad'],
                'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
                'genero del beneficiario' => ['data' => 'genero', 'name' => 'personas.genero'],
    
                'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                
                'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
                'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
                'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
                'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
                'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
                'genero del tutor' => ['data' => 'tutor_genero', 'name' => 'tutor.genero'],
    
                'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],
    
                'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
                'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
                'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
                'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre']
            ];
        }

        if($this->tipo === 'bancos') {
            $columns =
            [
                //'id' => ['data' => 'id', 'name' => 'bienestarpersonas.id'],
                'folio unico' => ['data' => 'foliounico', 'name' => 'bienestarpersonas.foliounico'],
                //'posicion' => ['data' => 'posicion', 'name' => 'bienestarpersonas.posicion', 'exportable' => false, 'printable' => false],
                'discapacidad del beneficiario' => ['data' => 'discapacidad', 'name' => 'cat_discapacidades.nombre'],
                //'estatus del beneficiario' => ['data' => 'estatus', 'name' => 'bienestarpersonas.deleted_at'],
                'nombre del beneficiario' => ['data' => 'nombre', 'name' => 'personas.nombre'],
                'primer apellido del beneficiario' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
                'segundo apellido del beneficiario' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
                'fecha de nacimiento del beneficiario' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
                'curp del beneficiario' => ['data' => 'curp', 'name' => 'personas.curp'],
    
                'region del beneficiario' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
                'distrito del beneficiario' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
                'municipio del beneficiario' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
                'localidad del beneficiario' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre'],
                
                'nombre del tutor' => ['data' => 'tutor_nombre', 'name' => 'tutor.nombre'],
                'primer apellido del tutor' => ['data' => 'tutor_primer_apellido', 'name' => 'tutor.primer_apellido'],
                'segundo apellido del tutor' => ['data' => 'tutor_segundo_apellido', 'name' => 'tutor.segundo_apellido'],
                'fecha de nacimiento del tutor' => ['data' => 'tutor_fecha_nacimiento', 'name' => 'tutor.fecha_nacimiento'],
                'curp del tutor' => ['data' => 'tutor_curp', 'name' => 'tutor.curp'],
    
                'numero de cuenta del tutor' => ['data' => 'num_cuenta', 'name' => 'cuentasbancos.num_cuenta'],
    
                'region del tutor' => ['data' => 'tutor_region', 'name' => 'tutor_cat_regiones.nombre'],
                'distrito del tutor' => ['data' => 'tutor_distrito', 'name' => 'tutor_cat_distritos.nombre'],
                'municipio del tutor' => ['data' => 'tutor_municipio', 'name' => 'tutor_cat_municipios.nombre'],
                'localidad del tutor' => ['data' => 'tutor_localidad', 'name' => 'tutor_cat_localidades.nombre']
            ];
        }

        if($this->tipo === 'normal') {
            if(Auth::user()->hasRolesModulo(['SUPERADMIN', 'OPERATIVO'], 2)) {
                $columns['convertir'] = ['data' => 'convertir', 'name' => 'convertir', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }
            $columns['consultar'] = ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            if(Auth::user()->hasRolesModulo(['SUPERADMIN', 'ADMINISTRADOR', 'COORDINADOR', 'OPERATIVO', 'ADMINISTRATIVO', 'APOYO OPERATIVO'], 2)) {
                $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
            }
            
        }

        if($this->tipo === 'bancos') {
            $columns['cuenta'] = ['data' => 'cuenta', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
        }
        
        return $columns;
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    protected function getBuilderParameters() {
        if($this->tipo === 'normal') {
            $builderParameters =
            [
                'language' => [
                    'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
                ],
                'dom' => 'Btip',
                'preDrawCallback' => 'function() { block(); }',
                'drawCallback' => 'function() { unblock(); }',
                'buttons' => [
                    [
                        'extend' => 'excel',
                        'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                        'className' => 'button-dt tool'
                    ],
                    [
                        'extend' => 'reset',
                        'text' => '<i class="fa fa-undo"></i> Reiniciar',
                        'className' => 'button-dt tool'
                    ],
                    [
                        'extend' => 'reload',
                        'text' => '<i class="fa fa-refresh"></i> Recargar',
                        'className' => 'button-dt tool'
                    ]
                ],
                'lengthMenu' => [ [10], [10] ],
                'scrollX' => true,
                'columnDefs' => [
                    [ 'className' => 'text-center', 'targets' => '_all' ]
                ]
            ];
        }

        if($this->tipo === 'bancos') {
            $builderParameters =
            [
                'language' => [
                    'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
                ],
                'dom' => 'Btip',
                'preDrawCallback' => 'function() { block(); }',
                'drawCallback' => 'function() { unblock(); }',
                'buttons' => [
                    [
                        'extend' => 'excel',
                        'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                        'className' => 'button-dt tool'
                    ],
                    [
                        'extend' => 'reset',
                        'text' => '<i class="fa fa-undo"></i> Reiniciar',
                        'className' => 'button-dt tool'
                    ],
                    [
                        'extend' => 'reload',
                        'text' => '<i class="fa fa-refresh"></i> Recargar',
                        'className' => 'button-dt tool'
                    ]
                ],
                'lengthMenu' => [ [10], [10] ],
                'responsive' => true,
                'columnDefs' => [
                    [ 'className' => 'text-center', 'targets' => '_all' ]
                ]
            ];
        }
        
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'solicitantes_' . date('YmdHis');
    }
}