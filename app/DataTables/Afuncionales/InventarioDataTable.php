<?php

namespace App\DataTables\Afuncionales;

use App\User;
use Yajra\DataTables\Services\DataTable;
use App\Models\Producto;
use App\Models\BeneficiosPrograma;
use Illuminate\Support\Facades\DB;

class InventarioDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addIndexColumn()
        ->editColumn('producto', function($inventario) {
          return str_replace("ENTREGA DE ","",$inventario->producto);
        })
        ->filterColumn('producto', function($query, $keyword) {
            $query->whereRaw("beneficiosprogramas.nombre like ?", ["%$keyword%"]);
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        // $model = Producto::withTrashed()
        // ->select(DB::raw("cat_productos.producto as producto, cat_unidadmedidas.unidadmedida as unidad, IFNULL(dpe.entradas,0) as entradas, ".
        //                 "IFNULL(dps.salidas, 0) as salidas, ".
        //                 "IFNULL(((IFNULL(dpe.entradas, 0)) - (IFNULL(dps.salidas, 0))), 0) as stock"))
        // ->join('areas_productos as ap','ap.producto_id','=','cat_productos.id')
        // ->join('programas','programas.id','ap.programa_id')
        // ->join('cat_unidadmedidas','cat_unidadmedidas.id','cat_productos.unidadmedida_id')
        // ->leftJoin(DB::raw('(select areas_producto_id, sum(format(cantidad,0)) as entradas '.
        //             'from detallesentradas_productos '.
        //             'where detallesentradas_productos.deleted_at is null '.
        //             'group by areas_producto_id) as dpe'), 'dpe.areas_producto_id', '=', 'ap.id')
        // ->leftJoin(DB::raw('(select areas_producto_id, sum(format(cantidad,0)) as salidas '.
        //             'from detallessalidas_productos '.
        //             'where detallessalidas_productos.deleted_at is null '.
        //             'group by areas_producto_id) as dps'), 'dps.areas_producto_id', '=', 'ap.id')
        // ->whereRaw('(select nombre from programas as programas_padres where programas_padres.id = programas.padre_id ) = "APOYOS FUNCIONALES" ')
        // ->whereNull('ap.deleted_at');

        $model = BeneficiosPrograma::select(DB::raw('beneficiosprogramas.nombre as producto, '.
                                                    'IFNULL(dpe.entradas,0) as entradas,'.
                                                    'IFNULL(dps.salidas, 0) as salidas,'.
                                                    'IFNULL(((IFNULL(dpe.entradas, 0)) - (IFNULL(dps.salidas, 0))), 0) as stock'))
          ->join('anios_programas as ap','ap.id','beneficiosprogramas.anio_programa_id')
          ->join('programas as p','p.id','ap.programa_id')
          ->join('programas as pp','pp.id','p.padre_id')
          ->join('areas_programas as app','app.id','pp.id')
          ->join('cat_areas as a','a.id','app.area_id')
          ->leftJoin(DB::raw('(SELECT beneficioprograma_id, sum(format(cantidad,0)) as entradas FROM detallesentradas_productos '.
                              'WHERE detallesentradas_productos.deleted_at IS NULL '.
                              'GROUP BY beneficioprograma_id) AS dpe'), 'dpe.beneficioprograma_id', '=', 'beneficiosprogramas.id')
          ->leftJoin(DB::raw('(SELECT beneficioprograma_id, sum(format(cantidad,0)) as salidas FROM detallessalidas_productos '.
                              'WHERE detallessalidas_productos.deleted_at IS NULL '.
                              'GROUP BY beneficioprograma_id) AS dps'), 'dps.beneficioprograma_id', '=', 'beneficiosprogramas.id')
          ->whereIn('pp.nombre',['APOYOS FUNCIONALES','TRABAJO SOCIAL']);                    

        return $model;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
            'producto' => ['data' => 'producto', 'name' => 'producto'],
            // 'unidad de medida' => ['data' => 'unidad', 'name' => 'cat_unidadmedidas.unidadmedida', 'searchable' => false],
            'entradas' => ['data' => 'entradas', 'name' => 'dpe.entradas', 'searchable' => false],
            'salidas' => ['data' => 'salidas', 'name' => 'dps.salidas', 'searchable' => false],
            'stock' => ['data' => 'stock', 'name' => 'stock', 'searchable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [                
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Inventario_' . date('YmdHis');
    }
}