<?php

namespace App\DataTables\Afuncionales;

use App\Models\SalidasProducto;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SalidasDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
            ->addColumn('fecha', function($salidas) {
                return SalidasProducto::withTrashed()->find($salidas->id)->get_formato_fecha();
            })
            ->addColumn('solicitante', function($salidas) {
                return $salidas->persona->get_nombre_completo();
            })
            ->addColumn('consultar', function($salidas) {
                if(SalidasProducto::find($salidas->id)){
                    return '<a onclick="consultar(\'' . route('afuncionales.productos.salidas.show', $salidas->id) . '\')" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
                }
            })
            ->addColumn('editar', function($salidas) {
                if(SalidasProducto::find($salidas->id)){
                    return '<a href="' . route('afuncionales.productos.salidas.edit', $salidas->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
                }          
            })
            ->addColumn('cancelar', function($salidas) {
                if(SalidasProducto::find($salidas->id)){
                    return '<a class="btn btn-danger btn-xs" onclick="cancelar(' . $salidas->id . ');"><i class="fa fa-trash"></i> Cancelar</a>';          
                }        
            })
            ->addColumn('observacion', function($salidas){
                if(!SalidasProducto::find($salidas->id)){
                    return '<span class="label label-danger">CANCELADO</span>';
                }
            })
            ->rawColumns(['consultar', 'editar', 'cancelar','observacion']);
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return SalidasProducto::search(['APOYOS FUNCIONALES','TRABAJO SOCIAL'])
        // ->distinct('entradas_productos.id')      
        ->select(["salidas_productos.id as id", "cat_tipossalidas.tipo as tipo", 'personas.id as persona_id'])
        ->groupby(DB::raw("1,2,3"));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [            
            'fecha' => ['data' => 'fecha', 'name' => 'fecha'],
            'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
            'solicitante' => ['data' => 'solicitante', 'name' => 'solicitante'],
            'consultar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'cancelar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { registrar_salida(); }',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ],
            'order' => [
                2,
                'desc'
            ]
        ];
        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Afuncionales/Salidas_' . date('YmdHis');
    }
}