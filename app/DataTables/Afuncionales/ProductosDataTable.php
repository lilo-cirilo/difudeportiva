<?php

namespace App\DataTables\Afuncionales;

use App\Models\Producto;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Services\DataTable;

class ProductosDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
            ->addIndexColumn()
            ->addColumn('editar', function($producto) {
                return '<a onclick="abrir_modal(\'modal-producto\', ' . $producto->id . ');" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
            })
            ->addColumn('eliminar', function($producto) {
                return '<a class="btn btn-danger btn-xs" onclick="producto_delete(' . $producto->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
            })
            ->rawColumns(['consultar', 'editar', 'eliminar']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return $model = Producto::search('APOYOS FUNCIONALES')
        ->select(
            DB::raw('DISTINCT cat_productos.id as id, 
            cat_productos.producto as producto, 
            cat_productos.vigencia as vigencia, 
            cat_unidadmedidas.unidadmedida as unidad,
            programas.nombre as programa_responsable')
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()                    
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            '#' => ['data' => 'DT_RowIndex', 'orderable' => false, 'searchable' => false],
            'producto' => ['data' => 'producto', 'name' => 'producto'],
            'vida util' => ['data' => 'vigencia', 'name' => 'vigencia'],
            'unidad' => ['data' => 'unidad', 'name' => 'cat_unidadmedidas.unidadmedida'],
            'programa_responsable' => ['data' => 'programa_responsable', 'name' => 'programas.nombre'],
            'editar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            'buttons' => [
                [
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { abrir_modal("modal-producto"); }',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'pdf',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Afuncionales/Productos_' . date('YmdHis');
    }
}
