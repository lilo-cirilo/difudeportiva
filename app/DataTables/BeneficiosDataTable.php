<?php

namespace App\DataTables;

use App\Models\Programa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Services\DataTable;

class BeneficiosDataTable extends DataTable{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query){
        return datatables($query)
            ->addColumn('area',function($programa){
              return $programa->padre->areasprograma? $programa->padre->areasprograma->area->nombre : null;
            })
            ->addColumn('responsable',function($programa){
              if(isset($programa->padre->areasprograma))
                if(isset($programa->padre->areasprograma->area->areasresponsables))
                  return $programa->padre->areasprograma->area->areasresponsables->empleado->persona->obtenerNombreCompleto();
              return null;
            })
            ->addColumn('acciones',function ($programa){
              $rutaBen=route('programas.beneficios.index',$programa->id);
              $rutaDoc=route('programas.documentos.index',$programa->id);
              $btnBeneficios = "<button class='btn text-white btn-sm btn-warning' data-toggle='modal' onClick='abrirModal(\"beneficio\",\"$rutaBen\")'><i class='fa fa-gear'></i> Configurar beneficios</button>";
              $btnDocumentos = "<button class='btn text-white btn-sm btn-warning' data-toggle='modal' onClick='abrirModal(\"documento\",\"$rutaDoc\")'><i class='fa fa-gear'></i> Configurar documentos</button>";
              return $btnBeneficios.$btnDocumentos;
            })->rawColumns(['acciones']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {

      if ($this->padre_id == 0) {
        $areas=array_map( function($o) { return $o->id; }, DB::select('call getProgramas(?)',[Auth::user()->persona->empleado->area_id]));
        return Programa::join('programas as padre','programas.padre_id','padre.id')->whereIn('programas.id',$areas)->where(function($q){
            $q->orWhere('programas.tipo','servicio')->orWhere('programas.tipo','producto');
        })->select('programas.id','programas.nombre','padre.nombre as padre_nombre','programas.padre_id');
      } else {
        $areas=array_map( function($o) { return $o->programa_id; }, DB::select('call getProgramasHijos(?)',[$this->padre_id]));
        return Programa::join('programas as padre','programas.padre_id','padre.id')->whereIn('id',$areas)->where(function($q){
            $q->orWhere('programas.tipo','servicio')->orWhere('progarmas.tipo','producto');
        })->select('programas.id','programas.nombre','padre.nombre as padre_nombre','programas.padre_id');
      } 

     
      //return Programa::where('id','>=',205);//with('hijos.hijos')
      //->find(205);
      /* ->select([
        'hijos.hijos.id as id',
        'hijos.hijos.nombre as nombre',
        'hijos.nombre as subprograma'
      ]); */
    }
    
    /**
      * Optional method if you want to use html builder.
      *
      * @return \Yajra\DataTables\Html\Builder
      */
    public function html() {
      return $this->builder()
      ->columns($this->getColumns())
      ->minifiedAjax()
      ->parameters($this->getBuilderParameters());
    }
    
    /**
      * Get columns.
      *
      * @return array
      */
    protected function getColumns() {
      return [
        ['data'=>'padre_nombre', 'name'=>'padre.nombre'],
        ['title'=>'Nombre de la acción','data'=>'nombre'],
        ['title'=>'Area','data'=>'area'],
        ['title'=>'Responsable','data'=>'responsable'],
        ['title'=>'Beneficios','data'=>'acciones'],
        // 'programa principal' => ['data' => 'padre', 'name' => 'padre', 'searchable' => false, 'orderable' => false],
        // 'nombre' => ['data' => 'nombre', 'name' => 'nombre'],
        // 'descripción' => ['data' => 'descripcion', 'name' => 'descripcion'],
        // 'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
        // 'oficial' => ['data' => 'oficial', 'name' => 'oficial', 'searchable' => false],
        // 'fecha de inicio' => ['data' => 'fecha_inicio', 'name' => 'fecha_inicio'],
        // 'fecha de término' => ['data' => 'fecha_fin', 'name' => 'fecha_fin'],
        // 'estatus' => ['data' => 'estatus', 'name' => 'estatus'],
        // 'consultar' => ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
        // 'editar' => ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
        // 'eliminar' => ['data' => 'eliminar', 'name' => 'eliminar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
      ];
    }

    protected function getBuilderParameters() {
      $builderParameters = [
          'language' => [
              'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
          ],
          'dom' => 'Btlip',
          'buttons' => [
              /* [
                  'extend' => 'create',
                  'action' => 'function ( e, dt, node, config ) { abrir_modal("modal-programa"); }'
              ], */
              'excel',
              'pdf',
              ['extend'=>'reset','text'=>'Reiniciar'],
              ['extend'=>'reload','text'=>'Recargar']
          ],
          'lengthMenu' => [ [10,20,-1], [10,20,'TODO'] ],
          'responsive' => true,
          'columnDefs' => [
              [ 'className' => 'text-center', 'targets' => [1,2] ],
              [ 'visible' => false, 'targets' => 0 ]
          ],
          'rowGroup' => [
            'startRender' => 'function (rows,group) {
              var datos=rows.context[0].json.data[rows[0][0]], btn1,btn2;
              var btn1 = datos.area==null ? "ASIGNAR AREA" : "CAMBIAR AREA";
              var btn2 = datos.responsable==null ? "ASIGNAR RESPONSABLE" : "CAMBIAR RESPONSABLE";
              var ruta = `/programas/${datos.padre_id}/area`;
              var color= datos.area ? \'warning\' : \'info\';
              if(datos.area==null)
                ruta+=\'/create\'
              return group+`<div class="btn-group btn-group-sm float-right pull-right" role="group">
              <button class="btn btn-sm btn-${color}" data-toggle="modal" onClick="abrirModal(\'area\',\'${ruta}\')">${btn1}</button>`
              //<button class="btn btn-sm btn-info" data-toggle="modal" onClick="abrilModal(\'responsable\',\'\')">${btn2}</button>
              +`</div>`;
            }',
            'dataSrc' => 'padre_nombre'
          ]
      ];

      return $builderParameters;
    }
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(){
        return 'Beneficios_' . date('YmdHis');
    }
}
