<?php

namespace App\DataTables;

use App\Models\Programa;
use Yajra\DataTables\Services\DataTable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ProgramasDataTable extends DataTable {
    
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->editColumn('fecha_inicio', function($programa){
            return $programa->fecha_inicio ? with(\Carbon\Carbon::parse($programa->fecha_inicio))->format('d-m-Y') : '';
        })
        ->editColumn('fecha_fin', function($programa){
            return $programa->fecha_fin ? with(\Carbon\Carbon::parse($programa->fecha_fin))->format('d-m-Y') : '';
        })
        ->editColumn('oficial', function($programa) {
            if($programa->oficial){
                return '<div><i class="fa fa-fw fa-check"></i></div>';
            }else{
                return '<div><i class="fa fa-fw fa-close"></i></div>';
            }
        })
        ->editColumn('estatus', function($programa) {

			if(auth()->user()->hasRoles(['VALIDADOR DE BENEFICIOS'])){
				if($programa->aniosprogramas->contains('ejercicio.anio', date('Y')) && $programa->activo){
                	return '<div class="checkbox"><input id="' . $programa->id . '" name="'. $programa->nombre .'" class="toogle" type="checkbox" checked data-size="mini" data-onstyle="success"  data-offstyle="danger" data-width="95" data-on="Activo" data-off="Inactivo"></div>';                
            	}else {
                	return '<div class="checkbox"><input id="' . $programa->id . '" name="'. $programa->nombre .'" class="toogle" type="checkbox" data-size="mini" data-onstyle="success"  data-offstyle="danger" data-width="95" data-on="Activo" data-off="Inactivo"></div>';                
            	}
			}else{
				if($programa->aniosprogramas->contains('ejercicio.anio', date('Y')) && $programa->activo){
					return '<span class="label label-success">ACTIVO</span>';
				}else{
					return '<span class="label label-danger">INACTIVO</span>';
				}
				
			}
        })      
        ->filterColumn('estatus', function($query, $keyword) {
            if(strtoupper($keyword) === 'ACTIVO'){
                $keyword = 1;
                $query->whereRaw("programas.activo = ?", ["$keyword"]);
            }else if(strtoupper($keyword) === 'INACTIVO'){
                $keyword = 0;
                $query->whereRaw("programas.activo = ?", ["$keyword"]);
            }
        })
        ->addColumn('editar', function($programa) {
            return '<a class="btn btn-warning btn-xs" onclick="abrir_modal(\'modal-programa\','.$programa->id.');"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('consultar', function($programa) {
            return '<a class="btn btn-info btn-xs" onclick="abrir_modal(\'modal-detalle\','.$programa->id.');"><i class="fa fa-eye"></i> Consultar</a>';
        })
        ->addColumn('eliminar', function($programa) {
            return '<a class="btn btn-danger btn-xs" onclick="eliminar(' . $programa->id . ',\''. $programa->nombre .'\');"><i class="fa fa-trash"></i> Eliminar</a>';

            if($programa->activo){
                return '<a class="btn btn-danger btn-xs" onclick="cancelar(' . $programa->id . ',\''. $programa->nombre .'\');"><i class="fa fa-trash"></i> Cancelar</a>';
            }else{
                return '<a class="btn btn-success btn-xs" onclick="activar(' . $programa->id . ',\''. $programa->nombre .'\');"><i class="fa fa-check"></i> Activar</a>';
            }
        })        
        ->rawColumns(['editar','eliminar','oficial','activo','estatus','consultar']);;
    }

    /**
     * Get query source of dataTable.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return Programa::search($this->programa_padre, $this->area_id)
        ->select('programas.id as id', 'programas.padre_id as padre_id','programas.codigo as codigo','programas.nombre as nombre',
        'programas.descripcion as descripcion', 'programas.oficial as oficial', 'programas.activo as activo',
        'programas.tipo as tipo','programas.fecha_inicio as fecha_inicio','programas.fecha_fin as fecha_fin',
        DB::raw('IFNULL((select nombre from programas pr where programas.padre_id = pr.id), nombre) as padre'));
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'programa principal' => ['data' => 'padre', 'name' => 'padre', 'searchable' => false, 'orderable' => false],
            'nombre' => ['data' => 'nombre', 'name' => 'nombre'],
            'descripción' => ['data' => 'descripcion', 'name' => 'descripcion'],
            'tipo' => ['data' => 'tipo', 'name' => 'tipo'],
            'oficial' => ['data' => 'oficial', 'name' => 'oficial', 'searchable' => false],
            'fecha de inicio' => ['data' => 'fecha_inicio', 'name' => 'fecha_inicio'],
            'fecha de término' => ['data' => 'fecha_fin', 'name' => 'fecha_fin'],
            'estatus' => ['data' => 'estatus', 'name' => 'estatus'],
            'consultar' => ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'editar' => ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['data' => 'eliminar', 'name' => 'eliminar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        $builderParameters = [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btip',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); $(".toogle").bootstrapToggle(); $(".toogle").change(function(event) { if( $(this).prop("checked") ) { activar($(event.target).attr("id"), $(event.target).attr("name")); } else { inactivar($(event.target).attr("id"), $(event.target).attr("name")); } }) }',
            'buttons' => [
                [
                    'className' => 'button-dt tool',
                    'text' => '<i class="fa fa-plus"></i> Nuevo',
                    'action' => 'function ( e, dt, node, config ) { abrir_modal("modal-programa"); }'
                ],
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'excel',
                    'className' => 'button-dt tool',
                    'text' => '<i class="fa fa-file-excel-o"></i> Excel'
                ],                
                [
                    'extend' => 'pdf',
                    'className' => 'button-dt tool',
                    'text' => '<i class="fa fa-file-pdf-o"></i> PDF'
                ]                
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => [3,4,5,6,7,8,9] ],
                [ 'visible' => false, 'targets' => 0 ]
            ],
            'rowGroup' => [
                'dataSrc' => 'padre'
            ]
        ];

        return $builderParameters;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'Programas_' . date('YmdHis');
    }
}