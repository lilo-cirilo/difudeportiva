<?php

namespace App\DataTables\Vale;

use Modules\Vale\Entities\Alumno;
use Yajra\DataTables\Services\DataTable;

class AlumnosDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addColumn('editar', function($grupo) {
            return '<a href="#/edit/' . $grupo->id . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('eliminar', function($grupo) {
            return '<a href="#/destroy/' . $grupo->id . '" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Eliminar</a>';
        })
        ->rawColumns(['editar', 'eliminar'])
        ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return Alumno::
        select(
            'vale_alumnos.id as id',
            'vale_alumnos.matricula as matricula',
            'vale_alumnos.persona_id as persona_id'
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->ajax(['data' => 'function(d) { d.table = "alumnos"; }'])
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'id' => ['data' => 'id', 'name' => 'vale_alumnos.id'],
            'matricula' => ['data' => 'matricula', 'name' => 'vale_alumnos.matricula'],
            'persona_id' => ['data' => 'persona_id', 'name' => 'vale_alumnos.persona_id'],
            'editar' => ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['data' => 'eliminar', 'name' => 'eliminar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        return [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            /*'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],*/
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'alumnos_' . date('YmdHis');
    }
}