<?php

namespace App\DataTables\Vale;

use Modules\Vale\Entities\Grupo;
use Yajra\DataTables\Services\DataTable;

class GruposDataTable extends DataTable {
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query) {
        return datatables($query)
        ->addColumn('editar', function($grupo) {
            return '<a href="#/edit/' . $grupo->id . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
        })
        ->addColumn('eliminar', function($grupo) {
            return '<a href="#/destroy/' . $grupo->id . '" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Eliminar</a>';
        })
        ->rawColumns(['editar', 'eliminar'])
        ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query() {
        return Grupo::
        select(
            'vale_grupos.id as id',
            'vale_grupos.nombre as nombre',
            'vale_grupos.descripcion as descripcion',
            'vale_grupos.capacidad as capacidad'
        );
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html() {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns() {
        return [
            'id' => ['data' => 'id', 'name' => 'vale_grupos.id'],
            'nombre' => ['data' => 'nombre', 'name' => 'vale_grupos.nombre'],
            'descripcion' => ['data' => 'descripcion', 'name' => 'vale_grupos.descripcion'],
            'capacidad' => ['data' => 'capacidad', 'name' => 'vale_grupos.capacidad'],
            'editar' => ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false],
            'eliminar' => ['data' => 'eliminar', 'name' => 'eliminar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false]
        ];
    }

    protected function getBuilderParameters() {
        return [
            'language' => [
                'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json')
            ],
            'dom' => 'Btipr',
            'preDrawCallback' => 'function() { block(); }',
            'drawCallback' => 'function() { unblock(); }',
            /*'buttons' => [
                [
                    'extend' => 'reset',
                    'text' => '<i class="fa fa-undo"></i> Reiniciar',
                    'className' => 'button-dt tool'
                ],
                [
                    'extend' => 'reload',
                    'text' => '<i class="fa fa-refresh"></i> Recargar',
                    'className' => 'button-dt tool'
                ]
            ],*/
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            'columnDefs' => [
                [ 'className' => 'text-center', 'targets' => '_all' ]
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename() {
        return 'grupos_' . date('YmdHis');
    }
}