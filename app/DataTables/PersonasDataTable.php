<?php



namespace App\DataTables;

use App\Models\Persona;
use Yajra\DataTables\Services\DataTable;

use Illuminate\Support\Facades\Auth;

class PersonasDataTable extends DataTable
{
  /**
  * Construye el dataTable
  * @param mixed $query Resultado obtenido a partir del método query()
  * @return \Yajra\DataTables\DataTableAbstract El dataTable
  */
  public function dataTable($query){
    if($this->request && $this->request->tipo=='select') {
      return datatables($query)
      ->addColumn('url',function ($persona){
        return route('personas.show', $persona->id);
      })
      ->addColumn('edit',function ($persona){
        return route('personas.edit', $persona->id);
      });
    }
    
    return datatables($query)
    ->editColumn('fecha_nacimiento', function($persona) {
      return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->format('d-m-Y') : '';
    })
    ->filterColumn('personas.fecha_nacimiento', function($query, $keyword) {
      $query->whereRaw("DATE_FORMAT(personas.fecha_nacimiento, '%d-%m-%Y') like ?", ["%$keyword%"]);
    })
    ->addColumn('edad', function($persona) {
      return $persona->fecha_nacimiento ? with(\Carbon\Carbon::parse($persona->fecha_nacimiento))->age : '';
    })
    ->orderColumn('edad', '-fecha_nacimiento $1')
    ->filterColumn('edad', function($query, $keyword) {
      $edad = strtoupper($keyword);
      if(preg_match("/EDAD(=|>|>=|<|<=)\d+((=|>|>=|<|<=)\d+)?$/", $edad)) {
        preg_match_all('!\d+!', $edad, $valores);
        preg_match_all('!(>=|<=|=|>|<)!', $edad, $comparadores);
        if(count($valores[0]) === 2) {
          $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0] . " AND ((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][1] . " " . $valores[0][1]);
        }
        else {
          $query->whereRaw("((YEAR(now()) - YEAR(personas.fecha_nacimiento)) - ((DATE_FORMAT(now(), '00-%m-%d') < DATE_FORMAT(personas.fecha_nacimiento, '00-%m-%d')))) " . $comparadores[0][0] . " " . $valores[0][0]);
        }
      }
    })
    ->addColumn('consultar', function($persona) {
      return '<a href="' . route('personas.show', $persona->id) . '" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i> Consultar</a>';
    })
    ->addColumn('editar', function($persona) {
      if($this->tipo === 'normal') {
        return '<a href="' . route('personas.edit', $persona->id) . '" class="btn btn-warning btn-xs"><i class="fa fa-pencil"></i> Editar</a>';
      }
      if($this->tipo === 'bienestarsolicitantes' || $this->tipo === 'bienestartutores') {
        return '<a class="btn btn-warning btn-xs" onclick="editar_persona(' . $persona->id . ');"><i class="fa fa-pencil"></i> Editar</a>';
      }
    })
    ->addColumn('eliminar', function($persona) {
      return '<a class="btn btn-danger btn-xs" onclick="persona_delete(' . $persona->id . ');"><i class="fa fa-trash"></i> Eliminar</a>';
    })
    ->addColumn('seleccionar', function($persona) {
      return '<a class="btn btn-success btn-xs" onclick="seleccionar_persona(' . "'" . $persona->id . "'" . ', ' . "'" . $persona->get_nombre_completo() . "'" . ');"><i class="fa fa-check"></i> Seleccionar</a>';
    })
    ->rawColumns(['consultar', 'editar', 'eliminar', 'seleccionar']);
  }
  
  /**
  * Genera la fuente de datos para el dataTable
  * @return \Illuminate\Database\Eloquent\Builder Constructor de consultas
  */
  public function query()
  {
    $model = Persona::search()
    ->select(
      [
        'personas.id as id',
        'personas.nombre as nombre',
        'personas.primer_apellido as primer_apellido',
        'personas.segundo_apellido as segundo_apellido',
        'personas.fecha_nacimiento as fecha_nacimiento',
        'personas.curp as curp',
        'cat_regiones.nombre as region',
        'cat_distritos.nombre as distrito',
        'cat_municipios.nombre as municipio',
        'cat_localidades.nombre as localidad'
      ]
    );
    
    if($this->tipo === 'bienestarsolicitantes') {
      $model
      ->leftjoin('empleados', 'empleados.persona_id', '=', 'personas.id')
      ->whereNull('empleados.persona_id')
      ->leftjoin('bienestarpersonas', 'bienestarpersonas.persona_id', '=', 'personas.id')
      ->whereNull('bienestarpersonas.persona_id');
    }
    
    if($this->tipo === 'bienestartutores') {
      $model
      ->leftjoin('empleados', 'empleados.persona_id', '=', 'personas.id')
      ->whereNull('empleados.persona_id');
    }
    if($this->request && $this->request->tipo=='select') {
      $model = Persona::select(['id','curp','curp as label'])->take(10);
      if($this->request->input('filtro') === 'mayor') {
        $model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') >= 18");
    	}
    	elseif ($this->request->input('filtro') === 'menor') {
        $model->whereRaw("TIMESTAMPDIFF(YEAR,personas.fecha_nacimiento,'".date('Y-m-d')."') < 18");
    	}
    }
    return $model;
  }
  
  /**
  * Método pata configurar el constructor html
  * @return \Yajra\DataTables\Html\Builder Constructor de html
  */
  public function html()
  {
    return $this
    ->builder()
    ->columns($this->getColumns())
    ->minifiedAjax()
    ->ajax(['data' => 'function(d) { d.table = "personas"; }'])
    ->parameters($this->getBuilderParameters());
  }
  
  /**
  * Define las columnas que mostrara el html
  * @return array Arreglo con la configuración de columnas
  */
  protected function getColumns()
  {
    $columns =
    [
      'id' => ['data' => 'id', 'name' => 'personas.id'],
      'nombre' => ['data' => 'nombre', 'name' => 'personas.nombre'],
      'primer_apellido' => ['data' => 'primer_apellido', 'name' => 'personas.primer_apellido'],
      'segundo_apellido' => ['data' => 'segundo_apellido', 'name' => 'personas.segundo_apellido'],
      'fecha_nacimiento' => ['data' => 'fecha_nacimiento', 'name' => 'personas.fecha_nacimiento'],
      'edad' => ['data' => 'edad', 'name' => 'edad'],
      'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
      'region' => ['data' => 'region', 'name' => 'cat_regiones.nombre'],
      'distrito' => ['data' => 'distrito', 'name' => 'cat_distritos.nombre'],
      'municipio' => ['data' => 'municipio', 'name' => 'cat_municipios.nombre'],
      'localidad' => ['data' => 'localidad', 'name' => 'cat_localidades.nombre']
    ];
    
    if($this->tipo === 'normal') {
      /*$columns['consultar'] = ['data' => 'consultar', 'name' => 'consultar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
      $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
      $columns['eliminar'] = ['data' => 'eliminar', 'name' => 'eliminar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];*/
    }
    
    if($this->tipo === 'bienestarsolicitantes' || $this->tipo === 'bienestartutores') {
      if(Auth::user()->hasRoles(['SUPERADMIN', 'COORDINADOR', 'OPERATIVO', 'APOYO OPERATIVO'])) {
        $columns['editar'] = ['data' => 'editar', 'name' => 'editar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
      }
      $columns['seleccionar'] = ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
    }
    
    if($this->tipo === 'afuncionalessolicitantes') {
      $columns['seleccionar'] = ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
    }
    if($this->tipo === 'beneficiarios') {
      $columns['seleccionar'] = ['data' => 'seleccionar', 'name' => 'seleccionar', 'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false];
    }
    if($this->request && $this->request->tipo=='select') {
      return [
        'curp' => ['data' => 'curp', 'name' => 'personas.curp'],
        'label' => ['data' => 'curp', 'name' => 'personas.curp'],
        'id'  => ['data'=>'id', 'name'=>'personas.id'],
        'url'  => ['data' => 'url']
      ];
    }
    
    return $columns;
  }
  
  /**
  * Define los parámetros de configuración para la librería de jquery dataTables
  * @return Array Parametros de inicializacion para la librería
  */
  protected function getBuilderParameters()
  {
    $builderParameters =
    [
      'language' => [
        'url' => asset('bower_components/datatables.net-responsive/js/Spanish.json'),
        'sEmptyTable' => 'No hay personas registradas'
      ],
      'dom' => 'Btip',
      'preDrawCallback' => 'function() { block(); }',
      'drawCallback' => 'function() { unblock(); }',
      'buttons' => [
        /*[
          'extend' => 'csv',
          'text' => '<i class="fa fa-file-excel-o"></i> CSV',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'excel',
          'text' => '<i class="fa fa-file-excel-o"></i> Excel',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'pdf',
          'text' => '<i class="fa fa-file-pdf-o"></i> PDF',
          'className' => 'button-dt tool'
        ],
        [
          'extend' => 'print',
          'text' => '<i class="fa fa-print"></i> Imprimir',
          'className' => 'button-dt tool'*//*,
          'key' => [
            'key' => 'p',
            'ctrlKey' => true
            ]*/
            //],
            [
              'extend' => 'reset',
              'text' => '<i class="fa fa-undo"></i> Reiniciar',
              'className' => 'button-dt tool'
            ],
            [
              'extend' => 'reload',
              'text' => '<i class="fa fa-refresh"></i> Recargar',
              'className' => 'button-dt tool'
              ]
            ],
            'lengthMenu' => [ [10], [10] ],
            'responsive' => true,
            //'autoWidth' => true,
            'columnDefs' => [
              [ 'className' => 'text-center', 'targets' => '_all' ]
              ]
            ];
            
            /*if($this->tipo === 'bienestarcandidatos') {
              $builderParameters['buttons'] =
              [
                'extend' => 'csv',
                'text' => '<i class="fa fa-file-excel-o"></i> CSV',
                'className' => 'button-dt tool'
              ];
            }*/
            
            return $builderParameters;
          }
          
          /**
          * Define un nombre para el archivo que se exportara
          * @return string Nombre del archivo, sin extension
          */
          protected function filename()
          {
            return 'personas_' . date('YmdHis');
          }
        }
        